/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;
import uk.ac.cam.ch.hunter.ssip.core.graph.VF2;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;

public class SsipMoleculeTest {

	private SsipMolecule ssipMolecule;
	private CmlMolecule cmlMolecule;
	private SsipContainer ssipContainer;
	private SsipSurfaceInformation ssipSurfaceInformation;
	
	@Before
	public void setUp() throws Exception {
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomList = new ArrayList<>();
		atomList.add(new Atom(AtomDescriptor.O, new Point3D(-0.197781963448, -7.67944186155e-36, 0.346659831728), "a1"));
		atomList.add(new Atom(AtomDescriptor.H, new Point3D(0.760536914211, -1.03517266068e-35, 0.204548524816), "a2"));
		atomList.add(new Atom(AtomDescriptor.H, new Point3D(-0.561754950762, 3.39295529441e-35, -0.551208356544), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomList, bondMap);
		
		ssipSurfaceInformation = new SsipSurfaceInformation(new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000));
		
		ArrayList<Ssip> ssipList = new ArrayList<>();
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(-0.784,   0.048,  -1.589, 2.943));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(1.714,   0.000,  -0.277, 2.927));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(-0.865,   1.043,   1.440, "a1",  -6.393));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(-0.904,  -0.928,   1.505, "a1",  -6.541));
		ssipList.add(ssip1);
		ssipList.add(ssip2);
		ssipList.add(ssip3);
		ssipList.add(ssip4);
		ssipContainer = new SsipContainer(ssipList);
		
		ssipMolecule = new SsipMolecule(cmlMolecule, ssipContainer, ssipSurfaceInformation);
	}

	@Test
	public void testConstructor() {
		assertNotNull(ssipMolecule);
		assertEquals(ssipContainer.getSSIPs(), ssipMolecule.getSsipContainer().getSSIPs());
		assertEquals("a3", ssipMolecule.getSsipContainer().getSSIPs().get(0).getNearestAtomID());
		assertEquals("a2", ssipMolecule.getSsipContainer().getSSIPs().get(1).getNearestAtomID());
		CmlMolecule actualCmlMolecule = ssipMolecule.getCmlMolecule();
		assertEquals(cmlMolecule.getMoleculeid(), actualCmlMolecule.getMoleculeid());
		assertEquals(cmlMolecule.getStdinchikey(), actualCmlMolecule.getStdinchikey());
		assertEquals(cmlMolecule.getAtomArrayList(), actualCmlMolecule.getAtomArrayList());
		assertEquals(cmlMolecule.getBondMap(), actualCmlMolecule.getBondMap());
		assertEquals(cmlMolecule.getAdjacencyRepresentation(), actualCmlMolecule.getAdjacencyRepresentation());
		assertTrue(cmlMolecule.getMolTopology().getVertices().size() == actualCmlMolecule.getMolTopology().getVertices().size()
				&& !(new VF2<AtomDescriptor, BondDescriptor>(cmlMolecule.getMolTopology(),
						actualCmlMolecule.getMolTopology()).findIsomorphisms().isEmpty()));
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> expectedMolPositions = new HashMap<>();
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(0),
				new Point3D(-0.197781963448, -7.67944186155e-36, 0.346659831728));
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(1),
				new Point3D(0.760536914211, -1.03517266068e-35, 0.204548524816));
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(2),
				new Point3D(-0.561754950762, 3.39295529441e-35, -0.551208356544));
		assertEquals(expectedMolPositions, actualCmlMolecule.getMolPositions());
		
		assertEquals(ssipSurfaceInformation, ssipMolecule.getSsipSurfaceInformation());
	}

}
