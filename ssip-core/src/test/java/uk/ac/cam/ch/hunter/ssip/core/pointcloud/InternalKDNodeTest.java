/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class InternalKDNodeTest {

	/**
	 * Constant used for comparison of doubles.
	 */
	private static final double TOLERANCE = 1e-7;

	@Test
	public void testConstructor() {

		// ----Drawing a picture is highly recommended-----

		final Point3D nearLeftEdgeOfBoundingBox = new Point3D();
		final Point3D farLeftEdgeOfBoundingBox = new Point3D(1, 2, 1);
		final ArrayList<Point3D> pointsInLeftBoundingBox = new ArrayList<>(
				Arrays.asList(new Point3D(0.5, 1, 0.5)));

		final Point3D nearRightEdgeOfBoundingBox = new Point3D(0, 2, 0);
		final Point3D farRightEdgeOfBoundingBox = new Point3D(1, 4, 1);
		final ArrayList<Point3D> pointsInRightBoundingBox = new ArrayList<>(
				Arrays.asList(new Point3D(0.5, 3, 0.5)));

		final LeafKDNode<Point3D> left = new LeafKDNode<>(
				nearLeftEdgeOfBoundingBox, farLeftEdgeOfBoundingBox,
				pointsInLeftBoundingBox);
		LeafKDNode<Point3D> right = new LeafKDNode<>(
				nearRightEdgeOfBoundingBox, farRightEdgeOfBoundingBox,
				pointsInRightBoundingBox);

		InternalKDNode<Point3D> node = new InternalKDNode<>(left, right);

		assertNotNull(node);
		assertNotNull(node.getLeft());
		assertEquals(left, node.getLeft());
		assertNotNull(node.getRight());
		assertEquals(right, node.getRight());
		assertEquals(Axis.Y_AXIS, node.getSplitAxis()); // along y
		assertEquals(2.0, node.getSplitPositionAlongSplitAxis(), TOLERANCE);

		// test for failure

		// test for null values
		try {
			node = new InternalKDNode<>(left, null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		try {
			node = new InternalKDNode<>(null, right);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// 'left' and 'right' do not share a plane
		try {
			// moved point along 'y' from 2 to 3 (see
			// 'nearRightEdgeOfBoundingBox')
			Point3D movedNearRightEdgeOfBoundingBox = new Point3D(0, 3, 0);

			right = new LeafKDNode<>(movedNearRightEdgeOfBoundingBox,
					farRightEdgeOfBoundingBox, pointsInRightBoundingBox);
			node = new InternalKDNode<>(left, right);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// near edge of 'left' and near edge of 'right' do not lie on an
		// axis-parallel line
		try {
			// moved point along 'x' from 0 to -1 (see
			// 'nearRightEdgeOfBoundingBox')
			Point3D movedNearRightEdgeOfBoundingBox = new Point3D(-1, 2, 0);

			right = new LeafKDNode<>(movedNearRightEdgeOfBoundingBox,
					farRightEdgeOfBoundingBox, pointsInRightBoundingBox);
			node = new InternalKDNode<>(left, right);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// far edge of 'left' and far edge of 'right' do not lie on an
		// axis-parallel line
		try {
			// moved point along 'z' from 1 to 2 (see
			// 'farRightEdgeOfBoundingBox')
			Point3D movedFarRightEdgeOfBoundingBox = new Point3D(1, 4, 2);

			right = new LeafKDNode<>(nearRightEdgeOfBoundingBox,
					movedFarRightEdgeOfBoundingBox, pointsInRightBoundingBox);
			node = new InternalKDNode<>(left, right);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}
}