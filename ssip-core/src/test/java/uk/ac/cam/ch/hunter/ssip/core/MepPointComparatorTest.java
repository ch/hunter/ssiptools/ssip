/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.*;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;

import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class MepPointComparatorTest {

	private MepPointComparator mepPointComparator;
	private AlphaPoint alphaPoint1;
	private AlphaPoint alphaPoint2;
	private BetaPoint betaPoint1;
	private BetaPoint betaPoint2;
	
	@Before
	public void setUp() throws Exception {
		mepPointComparator = new MepPointComparator();
		alphaPoint1 = new AlphaPoint(0.0, 1.0, 1.0, 0.3);
		alphaPoint2 = new AlphaPoint(1.0, 1.0, 1.0, 0.5);
		Atom atom = new Atom(O, new Point3D(), "a1");
		betaPoint1 = new BetaPoint(0.0, 0.0, 1.0, -0.5, atom);
		betaPoint2 = new BetaPoint(1.0, 0.0, 0.0, -0.6, atom);
	}

	@Test
	public void testconstructor() {
		assertNotNull(mepPointComparator);
	}
	
	@Test
	public void testCompare() {
		assertEquals(-1, mepPointComparator.compare(alphaPoint1, alphaPoint2));
		assertEquals(-1, mepPointComparator.compare(betaPoint1, alphaPoint1));
		assertEquals(-1, mepPointComparator.compare(betaPoint1, betaPoint2));
		assertEquals(0, mepPointComparator.compare(alphaPoint2, alphaPoint2));
		assertEquals(1, mepPointComparator.compare(alphaPoint1, betaPoint2));		
	}

}
