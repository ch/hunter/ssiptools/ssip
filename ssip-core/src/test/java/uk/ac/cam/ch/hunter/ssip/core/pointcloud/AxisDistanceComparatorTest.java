/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.X_AXIS;

import org.junit.Test;

public class AxisDistanceComparatorTest {
	@Test
	public void testConstructor() {
		Axis axis = X_AXIS;
		AxisDistanceComparator<Point3D> comparator = new AxisDistanceComparator<>(
				axis);

		assertNotNull(comparator);
		assertEquals(axis, comparator.getAxis());
	}

	@Test
	public void testCompare() {

		Axis axis = X_AXIS;
		AxisDistanceComparator<Point3D> comparator = new AxisDistanceComparator<>(
				axis);

		// distant point
		Point3D point1 = new Point3D(3, 4, 0);

		// closer point
		Point3D point2 = new Point3D(-6, 10, 0);

		// mep1 > mep2
		assertEquals(1, comparator.compare(point1, point2));

		// mep2 < mep1
		assertEquals(-1, comparator.compare(point2, point1));

		// test equality
		point1 = new Point3D(-6, -1, 0);

		// mep1 == mep2
		assertEquals(0, comparator.compare(point1, point1));
		
	}
}