/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.*;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.add;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

public class SurfaceTest {

	@Test
	public void testConstructor() {

		ArrayList<Point3D> pointsOnSurface = new ArrayList<>(Arrays.asList(
				new Point3D(0, 0, 1), new Point3D(1, 0, 0),
				new Point3D(0, 1, 0), new Point3D(1, 1, 1)));

		Surface<Point3D> surface = new Surface<>(pointsOnSurface);

		assertNotNull(surface);
		assertNotNull(surface.getPointsOnSurface());

		// test for failure
		try {
			new Surface<>(null);
			fail("Should not go here!");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testFindArea() {
		// TODO:
	}

	@Test
	public void testFindPatches() {
		/*
		 * Generates a rectangular box made out of points 1 unit apart from each
		 * other. Dimensions of the box : near edge (0, 0 ,0) ; far edge
		 * (sizeOnX - 1, sizeOnY -1 , sizeOnZ - 1).
		 */
		int sizeOnX = 2;
		int sizeOnY = 3;
		int sizeOnZ = 4;

		int sizeOfRactangularBox = sizeOnX * sizeOnY * sizeOnZ;
		ArrayList<Point3D> grid1 = new ArrayList<>(sizeOfRactangularBox);

		// generates points using row major order
		// " index = z + y * sizeOnZ + x * sizeOnZ * sizeOnY "
		for (int i = 0; i < sizeOfRactangularBox; i++) {
			int x = i / (sizeOnZ * sizeOnY);
			int y = (i - x * sizeOnZ * sizeOnY) / sizeOnZ;
			int z = i - y * sizeOnZ - x * sizeOnY * sizeOnZ;
			grid1.add(new Point3D(x, y, z));
		}

		ArrayList<Point3D> grid2 = new ArrayList<>(sizeOfRactangularBox);
		Point3D position = new Point3D(10, 10, 10);

		// generates points using row major order
		// " index = z + y * sizeOnZ + x * sizeOnZ * sizeOnY "
		for (int i = 0; i < sizeOfRactangularBox; i++) {
			int x = i / (sizeOnZ * sizeOnY);
			int y = (i - x * sizeOnZ * sizeOnY) / sizeOnZ;
			int z = i - y * sizeOnZ - x * sizeOnY * sizeOnZ;
			grid2.add(add(position, new Point3D(x, y, z)));
		}

		ArrayList<Point3D> cloudOfPoints = new ArrayList<>(
				2 * sizeOfRactangularBox);
		cloudOfPoints.addAll(grid1);
		cloudOfPoints.addAll(grid2);

		Collections.shuffle(cloudOfPoints);

		Surface<Point3D> surface = new Surface<>(cloudOfPoints);

		// order of elements is irrelevant
		Surface<Point3D> expectedGrid1 = new Surface<>(grid1);
		Surface<Point3D> expectedGrid2 = new Surface<>(grid2);

		ArrayList<Surface<Point3D>> patches = surface.findPatches();

		// checks if there are two surfaces recognized
		assertEquals(2, patches.size());

		Surface<Point3D> resultingGrid1 = patches.get(0);
		Surface<Point3D> resultingGrid2 = patches.get(1);

		assertTrue((resultingGrid1.equals(expectedGrid1) && resultingGrid2
				.equals(expectedGrid2))
				|| (resultingGrid2.equals(expectedGrid1) && resultingGrid1
						.equals(expectedGrid2)));
	}

	@Test
	public void testEquals() {
		// TODO:
	}

	@Test
	public void testHashCode() {
		// TODO:
	}

	/*
	 * // Points approximately uniformly distributed on a sphere centered // at
	 * the origin
	 * 
	 * ArrayList<Point3D> sphere1; int numberOfPoints = 100; sphere1 = new
	 * ArrayList<>(numberOfPoints); Random randomGen = new Random(); //
	 * generates Normal distribution double x, y, z; // coordinates of point on
	 * the hemisphere double d; // distance to origin while (sphere1.size() <=
	 * numberOfPoints) { // generates normally distributed random number with
	 * mean 0 and // variance 1 along x, y and z z = randomGen.nextGaussian(); x
	 * = randomGen.nextGaussian(); y = randomGen.nextGaussian(); d = Math.sqrt(x
	 * * x + y * y + z * z); sphere1.add(new Point3D(x / d, y / d, z / d)); }
	 * 
	 * // Points approximately uniformly distributed on a sphere centered // at
	 * the origin numberOfPoints = 100; ArrayList<Point3D> sphere2 = new
	 * ArrayList<>(numberOfPoints); Point3D centerOfSphere2 = new Point3D(5, 0,
	 * 0); while (sphere2.size() <= numberOfPoints) { // generates normally
	 * distributed random number with mean 0 and // variance 1 along x, y and z
	 * z = randomGen.nextGaussian(); x = randomGen.nextGaussian(); y =
	 * randomGen.nextGaussian(); d = Math.sqrt(x * x + y * y + z * z);
	 * sphere2.add(add(centerOfSphere2, new Point3D(x / d, y / d, z / d))); }
	 */
}