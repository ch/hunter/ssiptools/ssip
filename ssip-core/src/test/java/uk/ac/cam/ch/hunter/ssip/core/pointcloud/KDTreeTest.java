/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

/**
 * Reference test:
 * 
 * *
 * 
 * <pre>
 * {@code
 * #!/usr/bin/python
 * 
 * # http://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.spatial.KDTree.query.html
 * # https://github.com/scipy/scipy/blob/master/scipy/spatial/kdtree.py
 * 
 * from scipy import spatial as ss
 * import numpy as np
 * 
 * def main():
 *     x, y, z = np.mgrid[0.0:2.0, 0.0:3.0, 0.0:4.0]
 *     points  = zip(x.ravel(), y.ravel(), z.ravel())
 * 
 *     tree = ss.KDTree(points)
 *     pts = np.array([[0.0, 0.0, 0.0]])
 *     # get k nearest neighbours
 *     a = tree.query(pts,1)
 *     print tree.data[a[1]]
 * 
 * if __name__ == "__main__":
 *     main()
 * 
 *  }
 * </pre>
 * 
 * @author mw529
 * 
 */
public class KDTreeTest {

	private KDTree<Point3D> tree;
	private ArrayList<Point3D> cloudOfPoints;
	private ArrayList<Point3D> tmpCloudOfPoints;

	// declaring variables
	private int numberOfNeighbours;
	private Point3D referencePoint;
	private HashSet<Point3D> expectedNeighbours;
	private HashSet<Point3D> resultingNeighbours;
	private double searchRadius;

	@Before
	public void setUp() {
		// ----Drawing a picture is highly recommended-----

		/*
		 * Generates a rectangular box made out of points 1 unit apart from each
		 * other. Dimensions of the box : near edge (0, 0 ,0) ; far edge
		 * (sizeOnX - 1, sizeOnY -1 , sizeOnZ - 1).
		 */
		int sizeOnX = 2;
		int sizeOnY = 3;
		int sizeOnZ = 4;
		int sizeOfRactangularBox = sizeOnX * sizeOnY * sizeOnZ;
		cloudOfPoints = new ArrayList<Point3D>(sizeOfRactangularBox);

		// generates points using row major order
		// " index = z + y * sizeOnZ + x * sizeOnZ * sizeOnY "
		for (int i = 0; i < sizeOfRactangularBox; i++) {
			int x = i / (sizeOnZ * sizeOnY);
			int y = (i - x * sizeOnZ * sizeOnY) / sizeOnZ;
			int z = i - y * sizeOnZ - x * sizeOnY * sizeOnZ;
			cloudOfPoints.add(new Point3D(x, y, z));
		}

		tmpCloudOfPoints = new ArrayList<Point3D>(cloudOfPoints);

		tree = new KDTree<>(cloudOfPoints);
	}

	@Test
	public void testConstructor() {
		// Test for failure
		assertNotNull(tree);
		assertNotNull(tree.getRoot());
	}

	@Test
	public void testInputParameterOrderRemainsUnchanged() {
		new KDTree<>(cloudOfPoints);
		assertEquals(tmpCloudOfPoints, cloudOfPoints);
	}

	@Test
	public void testConstructorNull() {
		// if list is null
		try {
			ArrayList<Point3D> dummy = null;
			new KDTree<>(dummy);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testConstructorEmpty() {
		// if list is empty
		try {
			ArrayList<Point3D> cloudOfPoints;
			cloudOfPoints = new ArrayList<>(0);
			new KDTree<>(cloudOfPoints);
			fail("Should throw an exception");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testConstructorOneElement() {
		// if list has one element
		try {
			ArrayList<Point3D> cloudOfPoints;
			cloudOfPoints = new ArrayList<>(Arrays.asList(mock(Point3D.class)));
			new KDTree<>(cloudOfPoints);
			fail("Should throw an exception");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testNearestNeighboursEdgePointZero() {
		// test edge point
		referencePoint = new Point3D(0, 0, 0);
		numberOfNeighbours = 1;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(0, 0, 0)));
		resultingNeighbours = new HashSet<>(tree.nearestNeighbours(
				referencePoint, numberOfNeighbours));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

	@Test
	public void testNearestNeighboursEdgePoint() {
		// test edge point
		referencePoint = new Point3D(0, 2, 3);
		numberOfNeighbours = 3;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(0, 2, 3),
				new Point3D(0, 2, 2), new Point3D(1, 2, 3)));
		resultingNeighbours = new HashSet<>(tree.nearestNeighbours(
				referencePoint, numberOfNeighbours));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

	@Test
	public void testNearestNeighboursPointBetweenPlanes() {
		// test point between planes
		referencePoint = new Point3D(1, 1, 1);
		numberOfNeighbours = 5;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(1, 1, 1),
				new Point3D(0, 1, 1), new Point3D(1, 0, 1),
				new Point3D(1, 1, 0), new Point3D(1, 2, 1)));
		resultingNeighbours = new HashSet<>(tree.nearestNeighbours(
				referencePoint, numberOfNeighbours));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

	@Test
	public void testNearestNeighboursPointOnOneOfTheSides() {
		// test point on one of the sides
		referencePoint = new Point3D(1, 0, 1);
		numberOfNeighbours = 4;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(1, 0, 1),
				new Point3D(0, 0, 1), new Point3D(1, 0, 0),
				new Point3D(1, 1, 1)));
		resultingNeighbours = new HashSet<>(tree.nearestNeighbours(
				referencePoint, numberOfNeighbours));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

	@Test
	public void testNearestNeighboursPointInsideBox() {
		// test at point inside box
		referencePoint = new Point3D(0.5, 1, 0.9);
		numberOfNeighbours = 2;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(1, 1, 1),
				new Point3D(0, 1, 1)));
		resultingNeighbours = new HashSet<>(tree.nearestNeighbours(
				referencePoint, numberOfNeighbours));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

	@Test
	public void testBallSearchEdgePoint() {
		referencePoint = new Point3D(0, 2, 3);
		searchRadius = 3;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(0.0, 0.0,
				1.0), new Point3D(0.0, 1.0, 1.0), new Point3D(1.0, 0.0, 1.0),
				new Point3D(1.0, 1.0, 1.0), new Point3D(0.0, 2.0, 0.0),
				new Point3D(0.0, 2.0, 1.0), new Point3D(1.0, 2.0, 1.0),
				new Point3D(0.0, 0.0, 2.0), new Point3D(0.0, 0.0, 3.0),
				new Point3D(0.0, 1.0, 2.0), new Point3D(0.0, 1.0, 3.0),
				new Point3D(1.0, 0.0, 2.0), new Point3D(1.0, 0.0, 3.0),
				new Point3D(1.0, 1.0, 2.0), new Point3D(1.0, 1.0, 3.0),
				new Point3D(0.0, 2.0, 2.0), new Point3D(0.0, 2.0, 3.0),
				new Point3D(1.0, 2.0, 2.0), new Point3D(1.0, 2.0, 3.0)));
		resultingNeighbours = new HashSet<>(tree.ballSearch(searchRadius,
				referencePoint));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

	@Test
	public void testBallSearchPointBetweenPlanes() {
		// test point between planes
		referencePoint = new Point3D(1, 1, 1);
		searchRadius = 5;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(0.0, 0.0,
				0.0), new Point3D(0.0, 0.0, 1.0), new Point3D(0.0, 1.0, 0.0),
				new Point3D(0.0, 1.0, 1.0), new Point3D(1.0, 0.0, 0.0),
				new Point3D(1.0, 0.0, 1.0), new Point3D(1.0, 1.0, 0.0),
				new Point3D(1.0, 1.0, 1.0), new Point3D(0.0, 2.0, 0.0),
				new Point3D(0.0, 2.0, 1.0), new Point3D(1.0, 2.0, 0.0),
				new Point3D(1.0, 2.0, 1.0), new Point3D(0.0, 0.0, 2.0),
				new Point3D(0.0, 0.0, 3.0), new Point3D(0.0, 1.0, 2.0),
				new Point3D(0.0, 1.0, 3.0), new Point3D(1.0, 0.0, 2.0),
				new Point3D(1.0, 0.0, 3.0), new Point3D(1.0, 1.0, 2.0),
				new Point3D(1.0, 1.0, 3.0), new Point3D(0.0, 2.0, 2.0),
				new Point3D(0.0, 2.0, 3.0), new Point3D(1.0, 2.0, 2.0),
				new Point3D(1.0, 2.0, 3.0)));
		resultingNeighbours = new HashSet<>(tree.ballSearch(searchRadius,
				referencePoint));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

	@Test
	public void testBallSearchPointOnOneOfTheSides() {
		// test point on one of the sides
		referencePoint = new Point3D(1, 0, 1);
		searchRadius = 4;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(0.0, 0.0,
				0.0), new Point3D(0.0, 0.0, 1.0), new Point3D(0.0, 1.0, 0.0),
				new Point3D(0.0, 1.0, 1.0), new Point3D(1.0, 0.0, 0.0),
				new Point3D(1.0, 0.0, 1.0), new Point3D(1.0, 1.0, 0.0),
				new Point3D(1.0, 1.0, 1.0), new Point3D(0.0, 2.0, 0.0),
				new Point3D(0.0, 2.0, 1.0), new Point3D(1.0, 2.0, 0.0),
				new Point3D(1.0, 2.0, 1.0), new Point3D(0.0, 0.0, 2.0),
				new Point3D(0.0, 0.0, 3.0), new Point3D(0.0, 1.0, 2.0),
				new Point3D(0.0, 1.0, 3.0), new Point3D(1.0, 0.0, 2.0),
				new Point3D(1.0, 0.0, 3.0), new Point3D(1.0, 1.0, 2.0),
				new Point3D(1.0, 1.0, 3.0), new Point3D(0.0, 2.0, 2.0),
				new Point3D(0.0, 2.0, 3.0), new Point3D(1.0, 2.0, 2.0),
				new Point3D(1.0, 2.0, 3.0)));
		resultingNeighbours = new HashSet<>(tree.ballSearch(searchRadius,
				referencePoint));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

	@Test
	public void testBallSearchPointInBox() {
		// test at point in box
		referencePoint = new Point3D(0.5, 1, 0.9);
		searchRadius = 2;
		expectedNeighbours = new HashSet<>(Arrays.asList(new Point3D(0.0, 0.0,
				0.0), new Point3D(0.0, 0.0, 1.0), new Point3D(0.0, 1.0, 0.0),
				new Point3D(0.0, 1.0, 1.0), new Point3D(1.0, 0.0, 0.0),
				new Point3D(1.0, 0.0, 1.0), new Point3D(1.0, 1.0, 0.0),
				new Point3D(1.0, 1.0, 1.0), new Point3D(0.0, 2.0, 0.0),
				new Point3D(0.0, 2.0, 1.0), new Point3D(1.0, 2.0, 0.0),
				new Point3D(1.0, 2.0, 1.0), new Point3D(0.0, 0.0, 2.0),
				new Point3D(0.0, 1.0, 2.0), new Point3D(1.0, 0.0, 2.0),
				new Point3D(1.0, 1.0, 2.0), new Point3D(0.0, 2.0, 2.0),
				new Point3D(1.0, 2.0, 2.0)));
		resultingNeighbours = new HashSet<>(tree.ballSearch(searchRadius,
				referencePoint));
		assertEquals(expectedNeighbours, resultingNeighbours);
	}

}