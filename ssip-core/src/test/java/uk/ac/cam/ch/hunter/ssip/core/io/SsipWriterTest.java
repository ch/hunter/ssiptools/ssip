/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.io;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;

public class SsipWriterTest {
	
	static final String namespace = "http://www-hunter.ch.cam.ac.uk/SSIP";
	Ssip alphaSSIP;
	Ssip betaSSIP;
	
	@Before
	public void setUp(){

		AlphaPoint mep1 = new AlphaPoint(1.0, 2.1, 0.5, 1.0);
		alphaSSIP = Ssip.fromMepPoint(mep1);
		betaSSIP = Ssip.fromMepPoint(new BetaPoint(1.0, 2.1, 0.5, "a1", -4.5));
	}

	@Test
	public void testWriteSSIP() {
		String expectedString = "  1.000   2.100   0.500   0.008";
		String actualString = SsipWriter.writeSSIP(alphaSSIP);
		assertEquals(expectedString, actualString);
	}

}
