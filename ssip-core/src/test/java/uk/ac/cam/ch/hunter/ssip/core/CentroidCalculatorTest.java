/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class CentroidCalculatorTest {

	private CentroidCalculator centroidCalculator;
	private Point3D expectedCentroid;
	private HashSet<Point3D> point3dHashSet;
	private ArrayList<Point3D> point3dList;
	private HashSet<Ssip> ssipSet;
	private ArrayList<Ssip> ssipList;
	
	@Before
	public void setUp() throws Exception {
		centroidCalculator = new CentroidCalculator();
		expectedCentroid = new Point3D(0.5, 1.0, 0.0);
		Point3D point1 = new Point3D(0.0, 0.0, 0.0);
		Point3D point2 = new Point3D(1.0, 2.0, -1.0);
		Point3D point3 = new Point3D(1.0, 1.0, 1.0);
		Point3D point4 = new Point3D(0.0, 1.0, 0.0);
		point3dList = new ArrayList<>();
		point3dList.add(point1);
		point3dList.add(point2);
		point3dList.add(point3);
		point3dList.add(point4);
		point3dHashSet = new HashSet<>();
		point3dHashSet.add(point1);
		point3dHashSet.add(point2);
		point3dHashSet.add(point3);
		point3dHashSet.add(point4);
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 0.0, 0.0, 1.0));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 2.0, -1.0, 1.0));
		Ssip ssip3 = Ssip.fromMepPoint(new AlphaPoint(1.0, 1.0, 1.0, 1.0));
		Ssip ssip4 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, 1.0));
		ssipSet = new HashSet<>();
		ssipSet.add(ssip1);
		ssipSet.add(ssip2);
		ssipSet.add(ssip3);
		ssipSet.add(ssip4);
		ssipList = new ArrayList<>();
		ssipList.add(ssip1);
		ssipList.add(ssip2);
		ssipList.add(ssip3);
		ssipList.add(ssip4);
	}

	@Test
	public void testCentroidCalculator() {
		assertNotNull(centroidCalculator);
	}

	@Test
	public void testCalculateCentroid() {
		Point3D actualCentroidFromList = centroidCalculator.calculateCentroid(point3dList);
		assertEquals(expectedCentroid, actualCentroidFromList);
		
		Point3D actualCentroidFromSet = centroidCalculator.calculateCentroid(point3dHashSet);
		assertEquals(expectedCentroid, actualCentroidFromSet);
	}

	@Test
	public void testCalculateSSIPCentroid() {
		Point3D actualCentroidFromList = centroidCalculator.calculateSSIPCentroid(ssipList);
		assertEquals(expectedCentroid, actualCentroidFromList);
		
		Point3D actualCentroidFromSet = centroidCalculator.calculateSSIPCentroid(ssipSet);
		assertEquals(expectedCentroid, actualCentroidFromSet);
	}

}
