/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;

import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class AlphaPointTest {

	public static final double TOLERANCE = 1E-7;
	public static final double LOWER_TOLERANCE = 1E-5;
	public static AlphaPoint alpha1 = null;
	public static MepPointComparator mepPointComparator;

	@BeforeClass
	public static void setUp() {
		double x1 = 1;
		double y1 = 2;
		double z1 = 3;
		double ep1 = 1;
		alpha1 = new AlphaPoint(x1, y1, z1, ep1);
		mepPointComparator = new MepPointComparator();
	}

	@Test
	public void testConstructor() {
		double x = 1;
		double y = 2;
		double z = 3;
		double ep = 222.673873771;
		double alphaVal = 2.9493123280352274;
		AlphaPoint alpha = new AlphaPoint(x, y, z, ep);

		assertEquals(x, alpha.getX(), TOLERANCE);
		assertEquals(y, alpha.getY(), TOLERANCE);
		assertEquals(z, alpha.getZ(), TOLERANCE);
		assertEquals(alphaVal, alpha.getValue(), LOWER_TOLERANCE);
		assertEquals(ep, alpha.getEp(), TOLERANCE);
		assertEquals("2.95", alpha.toFormattedString());

		// test for fail
		ep = -5;
		try {
			alpha = new AlphaPoint(x, y, z, ep);
			fail("Should not go here!");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testCompareToInequality() {
		// Testing inequality
		double x2 = 1.4;
		double y2 = 2.2;
		double z2 = 3.1;
		double ep2 = 1.5;
		AlphaPoint alpha2 = new AlphaPoint(x2, y2, z2, ep2);

		assertTrue(mepPointComparator.compare(alpha1, alpha2) < 0);
		assertTrue(mepPointComparator.compare(alpha2, alpha1) > 0);
	}

	@Test
	public void testCompareToEquality() {
		// Testing equality
		double x3 = 1.6;
		double y3 = 2.4;
		double z3 = 3.3;
		double ep3 = 1 + TOLERANCE / 2;
		AlphaPoint alpha3 = new AlphaPoint(x3, y3, z3, ep3);

		assertEquals(0, mepPointComparator.compare(alpha1, alpha3));
	}

	@Test
	public void testCompareToBetaPoint() {
		// Testing BetaPoint comparison
		double closestAtomCorrectionFactor = 1.5;
		Atom closestAtom = new Atom(O, new Point3D(),
				closestAtomCorrectionFactor, "a1");

		double x4 = 1.6;
		double y4 = 2.4;
		double z4 = 3.3;
		double ep4 = -1;
		BetaPoint beta = new BetaPoint(x4, y4, z4, ep4, closestAtom);

		assertTrue(mepPointComparator.compare(alpha1, beta) > 0);
	}
}
