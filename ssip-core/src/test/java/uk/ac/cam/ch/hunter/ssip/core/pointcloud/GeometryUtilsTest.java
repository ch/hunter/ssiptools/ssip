/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.areCollinear;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.areCollinearToAxis;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.areCoplanar;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.areCoplanarToAxis;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.findBoundingBox;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class GeometryUtilsTest {

	@Test
	public void testFindBoundingBox() {

		// ----Drawing a picture is highly recommended-----

		Point3D[] meps = new Point3D[] { new Point3D(1, 1, 7),
				new Point3D(1, 3, 5), new Point3D(2, 4, 2),
				new Point3D(7, 1, 5), new Point3D(4, 0, 0) };
		ArrayList<Point3D> cloudOfPoints = new ArrayList<>(Arrays.asList(meps));
		ArrayList<Point3D> expectedBoundingBox = new ArrayList<>(Arrays.asList(
				new Point3D(1, 0, 0), new Point3D(7, 4, 7)));

		ArrayList<Point3D> boundingBox = GeometryUtils
				.findBoundingBox(cloudOfPoints);

		assertEquals(expectedBoundingBox, boundingBox);

		// test for failure

		// if list is empty
		try {
			cloudOfPoints = new ArrayList<>(0);
			findBoundingBox(cloudOfPoints);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// if list is null
		try {
			findBoundingBox(null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// if list has one element
		try {
			cloudOfPoints = new ArrayList<>(Arrays.asList(new Point3D()));
			findBoundingBox(cloudOfPoints);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testAreCoplanar() {

		ArrayList<Point3D> points = new ArrayList<>(Arrays.asList(
				new Point3D(), new Point3D(1, 1, 1), new Point3D(1, 2, 3)));

		// true since 3 points always lie on a plane
		assertTrue(areCoplanar(points));

		points.add(new Point3D(1, 6, 4));

		// false since points do not lie on a plane
		assertTrue(!areCoplanar(points));

		points = new ArrayList<>(Arrays.asList(new Point3D(0, 0, -1),
				new Point3D(1, 1, 1), new Point3D(1, 0, 0),
				new Point3D(0, 1, 0)));
		assertTrue(areCoplanar(points));

		// test for null
		try {
			areCoplanar(null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testAreCoplanarToAxisArrayArg() {

		// lie on a plane orthogonal to the 'x' axis
		ArrayList<Point3D> points = new ArrayList<>(Arrays.asList(new Point3D(
				1, 5, 6), new Point3D(1, 1, 1), new Point3D(1, 2, 3),
				new Point3D(1, -3, -5)));
		assertTrue(areCoplanarToAxis(points));

		// points lie on a plane but plane is not orthogonal to any axis
		points = new ArrayList<>(Arrays.asList(new Point3D(0, 0, -1),
				new Point3D(1, 1, 1), new Point3D(1, 0, 0),
				new Point3D(0, 1, 0)));
		assertTrue(!areCoplanarToAxis(points));
	}

	@Test
	public void testAreCoplanarToAxis() {
		Point3D point1 = new Point3D(-1, 4, 2);
		Point3D point2 = new Point3D(5, 2, 2);

		assertTrue(areCoplanarToAxis(point1, point2));

		point2 = new Point3D(5, 2, 1);
		assertTrue(!areCoplanarToAxis(point1, point2));
	}

	@Test
	public void testAreCollinear() {

		ArrayList<Point3D> points = new ArrayList<>(Arrays.asList(
				new Point3D(), new Point3D(1, 1, 1)));

		// true since 2 points always lie on a plane
		assertTrue(areCollinear(points));

		points.add(new Point3D(4, 4, 4));

		// true since points lie on a line
		assertTrue(areCollinear(points));

		// false not all points are collinear
		points.add(new Point3D(1, 5, 5));
		assertTrue(!areCollinear(points));

		// test for points too close to each other
		try {
			areCollinear(new ArrayList<>(Arrays.asList(new Point3D(),
					new Point3D(), new Point3D(), new Point3D())));
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// test for null
		try {
			areCollinear(null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testAreCollinearToAxisArrayArg() {

		// lie on a line parallel to the 'z' axis
		ArrayList<Point3D> points = new ArrayList<>(Arrays.asList(new Point3D(
				1, 2, 6), new Point3D(1, 2, 1), new Point3D(1, 2, 3),
				new Point3D(1, 2, -5)));
		assertTrue(areCollinearToAxis(points));

		// don't lie on a line parallel to any of the axis
		points = new ArrayList<>(Arrays.asList(new Point3D(-1, -1, -1),
				new Point3D(), new Point3D(1, 1, 1), new Point3D(10, 10, 10)));
		assertTrue(!areCollinearToAxis(points));
	}

	@Test
	public void testAreCollinearToAxis() {

		// lie on a line parallel to the 'x' axis
		final Point3D point1 = new Point3D(1, 1, 1);
		Point3D point2 = new Point3D(2, 1, 1);

		assertTrue(areCollinearToAxis(point1, point2));

		// don't lie on a line parallel to any of the axis
		point2 = new Point3D(1, 3, 4);
		assertTrue(!areCollinearToAxis(point1, point2));
	}
}