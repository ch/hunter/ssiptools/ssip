/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

public class EuclideanDistanceComparatorTest {

	@Test
	public void testConstructor() {
		Point3D referencePoint = new Point3D();
		EuclideanDistanceComparator comparator = new EuclideanDistanceComparator(
				referencePoint);

		assertNotNull(comparator);
		assertNotNull(comparator.getReferencePoint());
		assertEquals(referencePoint, comparator.getReferencePoint());

		// test for null
		try {
			new EuclideanDistanceComparator(null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testCompare() {

		// centered at origin
		Point3D referencePoint = new Point3D();

		EuclideanDistanceComparator comparator = new EuclideanDistanceComparator(
				referencePoint);

		// distant point
		Point3D point1 = new Point3D(1, -2, 0);

		// closer point
		final Point3D point2 = new Point3D(1, 1, 0);

		// mep1 > mep2
		assertEquals(1, comparator.compare(point1, point2));

		// mep2 > mep1
		assertEquals(-1, comparator.compare(point2, point1));

		// test equality
		point1 = new Point3D(1, -1, 0);

		// mep1 == mep2
		assertEquals(1, comparator.compare(point1, point1));
	}

	@Test
	public void testCompareEqual() {

		Point3D referencePoint = new Point3D(0.0, 0.0, 0.0);

		EuclideanDistanceComparator comparator = new EuclideanDistanceComparator(
				referencePoint);

		Point3D point1 = new Point3D(0.0, 1.0, 0.0);

		Point3D point2 = new Point3D(1.0, 0.0, 0.0);

		assertEquals(1, comparator.compare(point1, point2));

	}
}