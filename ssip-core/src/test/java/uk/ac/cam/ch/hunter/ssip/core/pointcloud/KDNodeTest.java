/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;


public class KDNodeTest {

	static KDNode<Point3D> root;

	@BeforeClass
	public static void setUp() {

		ArrayList<Point3D> pointsInBox1 = new ArrayList<>(Arrays.asList(new Point3D(0.3, 0.3, 0.3),
				new Point3D(0.4, 0.6, 0.1)));
		Point3D nearEdgeBox1 = new Point3D();
		Point3D farEdgeBox1 = new Point3D(1, 1, 1);
		KDNode<Point3D> box1 = new LeafKDNode<>(nearEdgeBox1, farEdgeBox1, pointsInBox1);

		ArrayList<Point3D> pointsInBox2 = new ArrayList<>(Arrays.asList(
				new Point3D(1.5, 1.3, 0.25), new Point3D(0.7, 1.2, 0.34)));
		Point3D nearEdgeBox2 = new Point3D(0, 1, 0);
		Point3D farEdgeBox2 = new Point3D(1, 2, 1);
		KDNode<Point3D> box2 = new LeafKDNode<>(nearEdgeBox2, farEdgeBox2, pointsInBox2);

		ArrayList<Point3D> pointsInBox3 = new ArrayList<>(Arrays.asList(new Point3D(1.3, 0.1, 0.2),
				new Point3D(1.9, 0.14, 0.19)));
		Point3D nearEdgeBox3 = new Point3D(1, 0, 0);
		Point3D farEdgeBox3 = new Point3D(2, 1, 1);
		KDNode<Point3D> box3 = new LeafKDNode<>(nearEdgeBox3, farEdgeBox3, pointsInBox3);

		ArrayList<Point3D> pointsInBox4 = new ArrayList<>(Arrays.asList(new Point3D(1.3, 1.5, 0.1),
				new Point3D(1.9, 1.2, 0.4)));
		Point3D nearEdgeBox4 = new Point3D(1, 1, 0);
		Point3D farEdgeBox4 = new Point3D(2, 2, 1);
		KDNode<Point3D> box4 = new LeafKDNode<>(nearEdgeBox4, farEdgeBox4, pointsInBox4);

		KDNode<Point3D> box12 = new InternalKDNode<>(box1, box2);
		KDNode<Point3D> box34 = new InternalKDNode<>(box3, box4);

		root = new InternalKDNode<>(box12, box34);
	}

	@Test
	public void testKDNode() {

		// ----Drawing a picture is highly recommended-----

		final Point3D nearEdgeOfBoundingBox = new Point3D();

		Point3D farEdgeOfBoundingBox = new Point3D(1, 1, 1);

		final ArrayList<Point3D> pointsInBoundingBox = new ArrayList<>(Arrays.asList(new Point3D()));

		KDNode<Point3D> node = new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox,
				pointsInBoundingBox);

		assertNotNull(node);
		assertEquals(nearEdgeOfBoundingBox, node.getNearEdgeOfBoundingBox());
		assertEquals(farEdgeOfBoundingBox, node.getFarEdgeOfBoundingBox());

		// test for failure

		// does not span a box due to equal 'x' values
		try {
			farEdgeOfBoundingBox = new Point3D(0, 1, 1);
			new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox, pointsInBoundingBox);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// does not span a box due to equal 'y' values
		try {
			farEdgeOfBoundingBox = new Point3D(1, 0, 1);
			new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox, pointsInBoundingBox);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// does not span a box due to equal 'z' values
		try {
			farEdgeOfBoundingBox = new Point3D(1, 1, 0);
			new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox, pointsInBoundingBox);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// does not span a box due to far edge having smaller 'x' value
		try {
			farEdgeOfBoundingBox = new Point3D(-1, 1, 1);
			new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox, pointsInBoundingBox);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// does not span a box due to far edge having smaller 'y' value
		try {
			farEdgeOfBoundingBox = new Point3D(1, -1, 1);
			new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox, pointsInBoundingBox);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// does not span a box due to far edge having smaller 'z' value
		try {
			farEdgeOfBoundingBox = new Point3D(1, 1, -1);
			new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox, pointsInBoundingBox);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// null values

		try {
			new LeafKDNode<>(nearEdgeOfBoundingBox, null, pointsInBoundingBox);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		try {
			new LeafKDNode<>(null, farEdgeOfBoundingBox, pointsInBoundingBox);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testContains() {

		// ----Drawing a picture is highly recommended-----

		final Point3D nearEdgeOfBoundingBox = new Point3D();

		final Point3D farEdgeOfBoundingBox = new Point3D(1, 1, 1);

		final ArrayList<Point3D> pointsInBoundingBox = new ArrayList<>(Arrays.asList(new Point3D()));

		final KDNode<Point3D> node = new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox,
				pointsInBoundingBox);

		Point3D point;
		// point on one of the sides
		point = new Point3D(0.5, 0.3, 0);
		assertTrue(node.containsPoint(point));

		// point on one of the edges
		point = new Point3D(0, 1, 0);
		assertTrue(node.containsPoint(point));

		// point inside
		point = new Point3D(0.2, 0.3, 0.4);
		assertTrue(node.containsPoint(point));

		// point outside
		point = new Point3D(0.2, 0.2, -3);
		assertTrue(!node.containsPoint(point));

		// test for null
		try {
			node.containsPoint(null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testBelongsTo() {

		// ----Drawing a picture is highly recommended-----

		final Point3D nearLeftEdgeOfBoundingBox = new Point3D();
		final Point3D farLeftEdgeOfBoundingBox = new Point3D(1, 2, 1);
		final Point3D leftPoint = new Point3D(0.5, 1, 0.5);
		final ArrayList<Point3D> pointsInLeftBoundingBox = new ArrayList<>(Arrays.asList(leftPoint));
		final Point3D nearRightEdgeOfBoundingBox = new Point3D(0, 2, 0);
		final Point3D farRightEdgeOfBoundingBox = new Point3D(1, 4, 1);

		final Point3D rightPoint = new Point3D(0.5, 3, 0.5);
		final ArrayList<Point3D> pointsInRightBoundingBox = new ArrayList<>(
				Arrays.asList(rightPoint));

		final LeafKDNode<Point3D> left = new LeafKDNode<>(nearLeftEdgeOfBoundingBox,
				farLeftEdgeOfBoundingBox, pointsInLeftBoundingBox);
		LeafKDNode<Point3D> right = new LeafKDNode<>(nearRightEdgeOfBoundingBox,
				farRightEdgeOfBoundingBox, pointsInRightBoundingBox);

		KDNode<Point3D> node = new InternalKDNode<>(left, right);

		assertTrue(node.belongsTo(leftPoint));
		assertTrue(node.belongsTo(rightPoint));

		// test for failure

		// position of point outside bounding box
		Point3D outsidePoint = new Point3D(3, 0, 0);
		assertTrue(!node.belongsTo(outsidePoint));

		// position of point inside bounding box but MEP not contained in the
		// list of points in the box.
		Point3D insidePoint = new Point3D(1, 0, 0);
		assertTrue(!node.belongsTo(insidePoint));
	}

	@Test
	public void testNumberOfPoints() {
		assertEquals(8, root.numberOfPointsInBoundingBox());
	}

	@Test
	public void testGetPointsInBoundingBox() {
		HashSet<Point3D> expectedPoints = new HashSet<>(Arrays.asList(new Point3D(0.3, 0.3, 0.3),
				new Point3D(0.4, 0.6, 0.1), new Point3D(1.5, 1.3, 0.25),
				new Point3D(0.7, 1.2, 0.34), new Point3D(1.3, 0.1, 0.2), new Point3D(1.9, 0.14,
						0.19), new Point3D(1.3, 1.5, 0.1), new Point3D(1.9, 1.2, 0.4)));

		assertEquals(8, root.getPointsInBoundingBox().size());
		HashSet<Point3D> resultingPoints = new HashSet<>(root.getPointsInBoundingBox());

		assertEquals(expectedPoints, resultingPoints);
	}
}