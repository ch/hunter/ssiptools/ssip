/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.B;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.C;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.H;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.N;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.S;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.SINGLE;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BondDescriptor;


/**
 * The graph used for testing can be found in [1], figure 2b. The labels
 * assigned to the graph are completely random.
 * 
 * [1] W.-S. Han, R. Kasperovics, J.Lee, J.-H. Lee. An In-depth Comparison of
 * Subgraph Isomorphism Algorithms in Graph Databases. PVLDB, 2012.
 * DOI:10.14778/2535568.2448946
 * 
 */
public class VF2Test {

	static VF2<AtomDescriptor, BondDescriptor> algorithm;

	static Graph<AtomDescriptor, BondDescriptor> parentGraph;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex v1, v2, v3, v4, v5, v6, v7, v8, v9;

	static Graph<AtomDescriptor, BondDescriptor> subgraph;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex n1, n2, n3;

	@BeforeClass
	public static void setUp() {
		// Parent graph initialization
		parentGraph = new Graph<>();

		v1 = parentGraph.addVertex(C);
		v2 = parentGraph.addVertex(N);
		v3 = parentGraph.addVertex(O);
		v4 = parentGraph.addVertex(H);
		v5 = parentGraph.addVertex(C);
		v6 = parentGraph.addVertex(N);
		v7 = parentGraph.addVertex(O);
		v8 = parentGraph.addVertex(H);
		v9 = parentGraph.addVertex(B);

		BondDescriptor value = SINGLE;
		parentGraph.addEdge(v1, v4, value);
		parentGraph.addEdge(v2, v4, value);
		parentGraph.addEdge(v2, v5, value);
		parentGraph.addEdge(v3, v5, value);
		parentGraph.addEdge(v3, v6, value);
		parentGraph.addEdge(v4, v5, value);
		parentGraph.addEdge(v4, v8, value);
		parentGraph.addEdge(v5, v6, value);
		parentGraph.addEdge(v5, v9, value);
		parentGraph.addEdge(v7, v8, value);

		// Subgraph initialization
		subgraph = new Graph<>();

		n1 = subgraph.addVertex(C);
		n2 = subgraph.addVertex(H);
		n3 = subgraph.addVertex(N);

		subgraph.addEdge(n1, n2, value);
		subgraph.addEdge(n2, n3, value);
	}

	@Before
	public void init() {
		// Creates and instance of subgraph isomorphism algorithm
		// the method is called before each test method to ensure that the state
		// of the object is set to initial.
		algorithm = new VF2<>(subgraph, parentGraph);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testConstructor() {
		Graph<AtomDescriptor, BondDescriptor> testSubgraph, testParentGraph;
		testSubgraph = (Graph<AtomDescriptor, BondDescriptor>) Mockito.mock(Graph.class);
		testParentGraph = (Graph<AtomDescriptor, BondDescriptor>) Mockito.mock(Graph.class);

		algorithm = new VF2<>(testSubgraph, testParentGraph);

		assertNotNull(algorithm);

		try {
			algorithm = new VF2<>(null, parentGraph);
			fail("Should throw an exception!");
		} catch (IllegalArgumentException e) {
		}
		;

		try {
			algorithm = new VF2<>(subgraph, null);
			fail("Should throw an exception!");
		} catch (IllegalArgumentException e) {
		}
		;
	}

	@Test
	public void testInitializeCompatibilityMap() throws Exception {
		// ---builds the expected compability map---
		LinkedHashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, 
					  HashSet<Graph<AtomDescriptor, BondDescriptor>.Vertex>> 
					  expectedCompabilityMap = new LinkedHashMap<>();

		// n1
		HashSet<Graph<AtomDescriptor, BondDescriptor>.Vertex> compatiblePVerticesToSVertex1 = 
				new HashSet<>(Arrays.asList(v1, v5));
		expectedCompabilityMap.put(n1, compatiblePVerticesToSVertex1);

		// n2
		HashSet<Graph<AtomDescriptor, BondDescriptor>.Vertex> compatiblePVerticesToSVertex2 = 
				new HashSet<>(Arrays.asList(v4, v8));
		expectedCompabilityMap.put(n2, compatiblePVerticesToSVertex2);
		
		// n3
		HashSet<Graph<AtomDescriptor, BondDescriptor>.Vertex> compatiblePVerticesToSVertex3 = 
				new HashSet<>(Arrays.asList(v2, v6));
		expectedCompabilityMap.put(n3, compatiblePVerticesToSVertex3);

		VF2<AtomDescriptor, BondDescriptor> testAlgorithm = new VF2<>(subgraph, parentGraph);
		LinkedHashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, 
					  HashSet<Graph<AtomDescriptor, BondDescriptor>.Vertex>> 
					  resultingCompabilityMap = testAlgorithm.getCompatibilityMap();

		assertEquals(expectedCompabilityMap, resultingCompabilityMap);
	}

	@Test
	public void testSubgraphSearch() {
		HashSet<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, 
					Graph<AtomDescriptor, BondDescriptor>.Vertex>> expectedIsomorphisms = new HashSet<>();
		
		BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, 
				Graph<AtomDescriptor, BondDescriptor>.Vertex> expectedIsomorphism1, expectedIsomorphism2;
		
		expectedIsomorphism1 = HashBiMap.create();
		expectedIsomorphism1.put(n1, v1);
		expectedIsomorphism1.put(n2, v4);
		expectedIsomorphism1.put(n3, v2);

		expectedIsomorphism2 = HashBiMap.create();
		expectedIsomorphism2.put(n2, v4);
		expectedIsomorphism2.put(n1, v5);
		expectedIsomorphism2.put(n3, v2);

		expectedIsomorphisms.add(expectedIsomorphism1);
		expectedIsomorphisms.add(expectedIsomorphism2);

		Set<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, 
				  Graph<AtomDescriptor, BondDescriptor>.Vertex>> isomorphisms = algorithm
				.findIsomorphisms();

		assertEquals(expectedIsomorphisms,isomorphisms);
	}
	
	@Test
	public void testEqualGraphs() {
		Graph<AtomDescriptor, BondDescriptor> subgraph = new Graph<>();
		Graph<AtomDescriptor, BondDescriptor>.Vertex n1, n2, n3, n4, n5, n6, n7, n8, n9;
		
		n1 = subgraph.addVertex(C);
		n2 = subgraph.addVertex(N);
		n3 = subgraph.addVertex(O);
		n4 = subgraph.addVertex(H);
		n5 = subgraph.addVertex(C);
		n6 = subgraph.addVertex(N);
		n7 = subgraph.addVertex(O);
		n8 = subgraph.addVertex(H);
		n9 = subgraph.addVertex(B);

		BondDescriptor value = SINGLE;
		subgraph.addEdge(n1, n4, value);
		subgraph.addEdge(n2, n4, value);
		subgraph.addEdge(n2, n5, value);
		subgraph.addEdge(n3, n5, value);
		subgraph.addEdge(n3, n6, value);
		subgraph.addEdge(n4, n5, value);
		subgraph.addEdge(n4, n8, value);
		subgraph.addEdge(n5, n6, value);
		subgraph.addEdge(n5, n9, value);
		subgraph.addEdge(n7, n8, value);
		
		BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, 
		  Graph<AtomDescriptor, BondDescriptor>.Vertex> expectedIsomorphism = HashBiMap.create();

		expectedIsomorphism.put(n1, v1);
		expectedIsomorphism.put(n2, v2);
		expectedIsomorphism.put(n3, v3);
		expectedIsomorphism.put(n4, v4);
		expectedIsomorphism.put(n5, v5);
		expectedIsomorphism.put(n6, v6);
		expectedIsomorphism.put(n7, v7);
		expectedIsomorphism.put(n8, v8);
		expectedIsomorphism.put(n9, v9);
		
		HashSet<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, 
		  Graph<AtomDescriptor, BondDescriptor>.Vertex>> expectedIsomorphisms = new HashSet<>();
		expectedIsomorphisms.add(expectedIsomorphism);
		
		VF2<AtomDescriptor, BondDescriptor> algorithm = new VF2<>(subgraph, parentGraph);
		
		Set<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, 
		  Graph<AtomDescriptor, BondDescriptor>.Vertex>> actualIsomorphisms = algorithm.findIsomorphisms();
		
		assertEquals(expectedIsomorphisms, actualIsomorphisms);
	}

	@Test
	public void testFindIsomorphisms() {
		// tests the method logic preceding subgraphSearch()

		// Builds a subgraph containing a label that is not present in
		// parentGraph defined in setUp()
		BondDescriptor value = SINGLE;
		Graph<AtomDescriptor, BondDescriptor> subgraph = new Graph<>();
		Graph<AtomDescriptor, BondDescriptor>.Vertex n1, n2, n3;
		n1 = subgraph.addVertex(C);
		n2 = subgraph.addVertex(H);
		n3 = subgraph.addVertex(S);

		subgraph.addEdge(n1, n2, value);
		subgraph.addEdge(n2, n3, value);

		// Creates and instance of subgraph isomorphism algorithm
		algorithm = new VF2<>(subgraph, parentGraph);

		// There should be no isomorphisms of 'subgraph' in the 'parentGraph'
		assertTrue(algorithm.findIsomorphisms().isEmpty());
	}
}
