/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class SsipComparatorTest {
	
	private Ssip ssip1;
	private Ssip ssip2;
	private Ssip ssip3;
	private Ssip ssip4;
	private Ssip ssip5;
	private Ssip ssip6;
	private Ssip ssip7;
	private SsipComparator ssipComparator;
	
	@Before
	public void setUp() {
		ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 1.0, 1.0, 0.1));
		ssip2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, 0.2));
		Atom betaAtom = Mockito.mock(Atom.class);
		ssip3 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, -0.1, betaAtom));
		ssip4 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, -0.2, betaAtom));
		ssip5= Ssip.fromMepPoint(new AlphaPoint(1.0, 1.0, 0.0, 0.1));
		ssip6 = Ssip.fromMepPoint(new AlphaPoint(1.0, 1.0, 2.0, 0.1));
		ssip7 = Ssip.fromMepPoint(new AlphaPoint(1.0, -2.0, 1.0, 0.1));
		Point3D centroid = new Point3D(0.0, 0.0, 0.0); 
		ssipComparator = new SsipComparator(centroid);
	}

	@Test
	public void sSIPComparison() {
		assertEquals(1, ssipComparator.compare(ssip1, ssip2));
		assertEquals(0, ssipComparator.compare(ssip3, ssip4));
		assertEquals(-1, ssipComparator.compare(ssip1, ssip4));
		
		assertEquals(1, ssipComparator.compare(ssip1, ssip5));
		assertEquals(-1, ssipComparator.compare(ssip1, ssip6));
		assertEquals(1, ssipComparator.compare(ssip5, ssip2));
		assertEquals(-1, ssipComparator.compare(ssip6, ssip7));
	}

}
