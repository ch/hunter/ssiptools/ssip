/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.cml.io;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BondDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;
import uk.ac.cam.ch.hunter.ssip.core.graph.VF2;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;



public class CmlReaderTest {
	
	private static final Logger LOG = LogManager.getLogger(CmlReaderTest.class);
	
	static URI waterURI;
	static CmlMolecule cmlMolecule;
	
	@Before
	public void setUp() throws Exception {
		waterURI = CmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/core/XLYOFNOQVPJJNP-UHFFFAOYSA-N.cml").toURI();
		
		List<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(-0.197781963448, -7.67944186155e-36, 0.346659831728), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(0.760536914211, -1.03517266068e-35, 0.204548524816), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-0.561754950762, 3.39295529441e-35, -0.551208356544), "a3"));
		Map<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		cmlMolecule = new CmlMolecule("XLYOFNOQVPJJNP-UHFFFAOYSA-N", "XLYOFNOQVPJJNP-UHFFFAOYSA-N", atomlist, bondMap);
	}

	@Test
	public void testDemarshal() throws JAXBException, MalformedURLException, SAXException{
		CmlMolecule actualCmlMolecule = CmlReader.demarshal(waterURI);
		
		assertEquals(cmlMolecule.getMoleculeid(), actualCmlMolecule.getMoleculeid());
		assertEquals(cmlMolecule.getStdinchikey(), actualCmlMolecule.getStdinchikey());
		assertEquals(cmlMolecule.getAtomArrayList(), actualCmlMolecule.getAtomArrayList());
		assertEquals(cmlMolecule.getBondMap(), actualCmlMolecule.getBondMap());
		assertEquals(cmlMolecule.getAdjacencyRepresentation(), actualCmlMolecule.getAdjacencyRepresentation());
		LOG.debug("expectedMolPositions");
		LOG.debug(cmlMolecule.getMolPositions().toString());
		LOG.debug("actualMolPostions");
		LOG.debug(actualCmlMolecule.getMolPositions().toString());
		LOG.debug("expectedMolTopology");
		LOG.debug(cmlMolecule.getMolTopology().toString());
		LOG.debug("actualMolTopology");
		LOG.debug(actualCmlMolecule.getMolTopology().toString());
		assertTrue(cmlMolecule.getMolTopology().getVertices().size() == actualCmlMolecule.getMolTopology().getVertices().size()
				&& !(new VF2<AtomDescriptor, BondDescriptor>(cmlMolecule.getMolTopology(),
						actualCmlMolecule.getMolTopology()).findIsomorphisms().isEmpty()));
		
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> expectedMolPositions = new HashMap<>();
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(0),
				new Point3D(-0.197781963448, -7.67944186155e-36, 0.346659831728));
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(1),
				new Point3D(0.760536914211, -1.03517266068e-35, 0.204548524816));
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(2),
				new Point3D(-0.561754950762, 3.39295529441e-35, -0.551208356544));
		assertEquals(expectedMolPositions, actualCmlMolecule.getMolPositions());
		
	}

}
