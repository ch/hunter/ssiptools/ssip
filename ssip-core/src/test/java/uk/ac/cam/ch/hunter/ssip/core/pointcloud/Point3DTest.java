/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.X_AXIS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.Y_AXIS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.Z_AXIS;

import org.junit.Test;

public class Point3DTest {

	/**
	 * Constant used for comparison of doubles.
	 */
	public static final double TOLERANCE = 1e-7;

	@Test
	public void testConstructor() {
		Point3D point = new Point3D(1, 2, 3);

		// first constructor
		assertNotNull(point);
		assertEquals(1, point.getX(), TOLERANCE);
		assertEquals(2, point.getY(), TOLERANCE);
		assertEquals(3, point.getZ(), TOLERANCE);

		// second constructor
		Point3D origin = new Point3D();
		assertNotNull(origin);
		assertEquals(0, origin.getX(), TOLERANCE);
		assertEquals(0, origin.getY(), TOLERANCE);
		assertEquals(0, origin.getZ(), TOLERANCE);

		// test for failure

		// if x == NAN
		try {
			new Point3D(Double.NaN, 0, 0);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// if x = Double.NEGATIVE_INFINITY
		try {
			new Point3D(Double.NEGATIVE_INFINITY, 0, 0);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		// if x = Double.POSITIVE_INFINITY
		try {
			new Point3D(Double.POSITIVE_INFINITY, 0, 0);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testDistanceSquaredTo() {
		final Point3D point1 = new Point3D(0, 0, 0);
		Point3D point2 = new Point3D(1, 1, 1);

		assertEquals(3.0, point1.distanceSquaredTo(point2), TOLERANCE);

		// same points
		point2 = new Point3D(0, 0, 0);
		assertEquals(0, point1.distanceSquaredTo(point2), TOLERANCE);

		// test failure

		try {
			point1.distanceSquaredTo(null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testDistanceTo() {

		final Point3D point1 = new Point3D(0, 0, 0);
		Point3D point2 = new Point3D(1, 1, 1);

		assertEquals(Math.sqrt(3.0), point1.distanceTo(point2), TOLERANCE);

		// same points
		point2 = new Point3D(0, 0, 0);
		assertEquals(0, point1.distanceTo(point2), TOLERANCE);

		// test failure

		try {
			point1.distanceTo(null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testMinCoord() {

		Point3D point = new Point3D(1, 3, 5);
		assertEquals(1.0, point.minCoord(), TOLERANCE);

		point = new Point3D(1, 1, 1);
		assertEquals(1.0, point.minCoord(), TOLERANCE);

	}

	@Test
	public void testMinCoordAxis() {

		Point3D point = new Point3D(1, 3, 5);
		assertSame(X_AXIS, point.minCoordAxis());

		point = new Point3D(1, 1, 1);
		assertSame(X_AXIS, point.minCoordAxis());

		point = new Point3D(4, 1, 1);
		assertSame(Y_AXIS, point.minCoordAxis());

		point = new Point3D(4, 1, -1);
		assertSame(Z_AXIS, point.minCoordAxis());

	}

	@Test
	public void testMaxCoord() {
		Point3D point = new Point3D(1, 3, 5);
		assertSame(Z_AXIS, point.maxCoordAxis());

		point = new Point3D(1, 1, 1);
		assertSame(X_AXIS, point.maxCoordAxis());

		point = new Point3D(0, 1, 1);
		assertSame(Y_AXIS, point.maxCoordAxis());

		point = new Point3D(-4, 1, 1.5);
		assertSame(Z_AXIS, point.maxCoordAxis());
	}

	@Test
	public void testCoord() {
		Point3D point = new Point3D(1, 3, 5);
		assertEquals(1, point.getCoord(X_AXIS), TOLERANCE);
		assertEquals(3, point.getCoord(Y_AXIS), TOLERANCE);
		assertEquals(5, point.getCoord(Z_AXIS), TOLERANCE);

		// test for null
		try {
			point.getCoord(null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testNorm() {
		Point3D point = new Point3D(1, 1, 1);
		assertEquals(Math.sqrt(3.0), point.getNorm(), TOLERANCE);
	}

	@Test
	public void testNormSquared() {
		Point3D point = new Point3D(1, 1, 1);
		assertEquals(3.0, point.getNormSquared(), TOLERANCE);
	}

	@Test
	public void testNeg() {
		Point3D point = new Point3D(1, 2, 3);
		Point3D expected = new Point3D(-1, -2, -3);

		assertEquals(expected, point.neg());
	}

	@Test
	public void testNormalize() {
		Point3D point = new Point3D(1, -1, 1);
		Point3D expected = new Point3D(1.0 / Math.sqrt(3), -1.0 / Math.sqrt(3), 1.0 / Math.sqrt(3));

		assertEquals(expected, point.normalize());

		// test for failure
		double epsilon = Point3D.ERROR_IN_POSITIONS / 2;
		point = new Point3D(epsilon, epsilon, epsilon);
		try {
			point.normalize();
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testAdd() {
		Point3D point1 = new Point3D(-1, -2, 5);
		Point3D point2 = new Point3D(1, 3, 2);
		Point3D expected = new Point3D(0, 1, 7);

		assertEquals(expected, Point3D.add(point1, point2));

		// test for null
		try {
			Point3D.add(point1, null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		try {
			Point3D.add(null, point2);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testSub() {
		Point3D point1 = new Point3D(-1, -2, 5);
		Point3D point2 = new Point3D(1, 3, 2);
		Point3D expected = new Point3D(-2, -5, 3);

		assertEquals(expected, Point3D.sub(point1, point2));

		// test for null
		try {
			Point3D.sub(point1, null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		try {
			Point3D.sub(null, point2);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testDot() {
		Point3D point1 = new Point3D(-1, -2, 5);
		Point3D point2 = new Point3D(1, 3, 2);
		double expected = -1 - 6 + 10;

		assertEquals(expected, Point3D.dot(point1, point2), TOLERANCE);
		// test for null
		try {
			Point3D.dot(point1, null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		try {
			Point3D.dot(null, point2);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testCross() {
		final Point3D point1 = new Point3D(1, 0, 0);

		Point3D point2 = new Point3D(0, 1, 0);
		Point3D expected1 = new Point3D(0, 0, 1);
		assertEquals(expected1, Point3D.cross(point1, point2));

		// collinear vectors
		point2 = new Point3D(1, 0, 0);
		Point3D expected2 = new Point3D();
		assertEquals(expected2, Point3D.cross(point1, point2));

		// test for null
		try {
			Point3D.cross(point1, null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}

		try {
			Point3D.cross(null, point2);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testEquals() {

		final double epsilon = Point3D.ERROR_IN_POSITIONS / 2;

		Point3D point = new Point3D(1, 1, 1);
		Point3D other = new Point3D();

		// should not be equal
		assertNotEquals(other, point);

		// should be equal
		other = new Point3D(epsilon + 1, epsilon + 1, epsilon + 1);
		assertEquals(other, point);

	}

	@Test
	public void testToString() {
		Point3D point = new Point3D(1, 3, 5);
		String expectedString = "(1.0, 3.0, 5.0)";

		assertEquals(expectedString, point.toString());
	}
}