/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;

import org.junit.Test;
import org.junit.BeforeClass;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class BetaPointTest {

	public static final double TOLERANCE = 1E-7;
	public static final double LOWER_TOLERANCE = 1E-5;
	public static BetaPoint beta = null;
	public static Atom closestAtom = null;
	public static MepPointComparator mepPointComparator;

	@BeforeClass
	public static void setUp() {
		closestAtom = new Atom(O, new Point3D(), "a1");

		double x = 1;
		double y = 2;
		double z = 3;
		double ep = -1;
		beta = new BetaPoint(x, y, z, ep, closestAtom);
		mepPointComparator = new MepPointComparator();
	}

	@Test
	public void testConstructor() {

		double closestAtomCorrectionFactor = 1;
		Atom closestAtom = new Atom(O, new Point3D(),
				closestAtomCorrectionFactor, "a1");

		double x = 1;
		double y = 2;
		double z = 3;
		double ep = -74.695464189;
		double betaVal = -1 * closestAtomCorrectionFactor
				* (1.5542612811011052);
		BetaPoint beta = new BetaPoint(x, y, z, ep, closestAtom);

		assertEquals(x, beta.getX(), TOLERANCE);
		assertEquals(y, beta.getY(), TOLERANCE);
		assertEquals(z, beta.getZ(), TOLERANCE);
		assertEquals(betaVal, beta.getValue(), LOWER_TOLERANCE);
		assertEquals(ep, beta.getEp(), TOLERANCE);
		assertEquals("-1.55", beta.toFormattedString());

		// test for fail
		ep = 0;
		try {
			beta = new BetaPoint(x, y, z, ep, closestAtom);
			fail("Should not go here!");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testCompareToInequality() {
		// Testing inequality
		double x1 = 1.4;
		double y1 = 2.2;
		double z1 = 3.1;
		double ep1 = -1.5;
		BetaPoint beta1 = new BetaPoint(x1, y1, z1, ep1, closestAtom);

		assertTrue(mepPointComparator.compare(beta, beta1) < 0);
		assertTrue(mepPointComparator.compare(beta1, beta) > 0);
	}

	@Test
	public void testCompareToEquality() {

		// Testing equality
		double x2 = 1.6;
		double y2 = 2.4;
		double z2 = 3.3;
		double ep2 = -1 + TOLERANCE / 2;
		BetaPoint beta2 = new BetaPoint(x2, y2, z2, ep2, closestAtom);

		assertEquals(0, mepPointComparator.compare(beta, beta2));
	}

	@Test
	public void testCompareToBetaPoint() {

		// Testing BetaPoint comparison
		double x4 = 1.6;
		double y4 = 2.4;
		double z4 = 3.3;
		double ep4 = 1;
		AlphaPoint alpha = new AlphaPoint(x4, y4, z4, ep4);

		assertTrue(mepPointComparator.compare(beta, alpha) < 0);
	}
}
