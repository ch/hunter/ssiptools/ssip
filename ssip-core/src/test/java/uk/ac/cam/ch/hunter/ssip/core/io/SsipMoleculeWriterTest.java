/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;
import org.w3c.dom.Attr;
import org.xml.sax.SAXException;
import org.xmlunit.matchers.CompareMatcher;
import org.xmlunit.util.Nodes;
import org.xmlunit.util.Predicate;

import uk.ac.cam.ch.hunter.ssip.core.SsipContainer;
import uk.ac.cam.ch.hunter.ssip.core.SsipMolecule;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;

public class SsipMoleculeWriterTest {

	private static final Logger LOG = LogManager.getLogger(SsipMoleculeWriterTest.class);
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private URI outputURI;
	private URI comparisonFileURI;
	private SsipContainer sSIPList;
	private SsipMolecule ssipMolecule;
	private CmlMolecule cmlMolecule;
	private SsipContainer ssipContainer;
	private SsipSurfaceInformation ssipSurfaceInformation;

	@Before
	public void setUp() throws Exception {
		File outFile = tempFolder.newFile("ssipMolecule.xml");
		outputURI = outFile.toURI();
		comparisonFileURI = SsipMoleculeWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/core/expectedSsipMolecule.xml").toURI();
		
		HashSet<Ssip> ssipList1 = new HashSet<>();
		Ssip ssip1a = Ssip.fromMepPoint(new AlphaPoint(1.0, 1.0, 1.0, 0.1));
		Ssip ssip2a = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, 0.2));
		Atom betaAtom = Mockito.mock(Atom.class);
		Ssip ssip3a = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, -0.1, betaAtom));
		Ssip ssip4a = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 0.0, -0.2, betaAtom));
		ssipList1.add(ssip1a);
		ssipList1.add(ssip2a);
		ssipList1.add(ssip3a);
		ssipList1.add(ssip4a);

		sSIPList = new SsipContainer(ssipList1);
		
		
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomList = new ArrayList<>();
		atomList.add(new Atom(AtomDescriptor.O, new Point3D(-0.197781963448, -7.67944186155e-36, 0.346659831728), "a1"));
		atomList.add(new Atom(AtomDescriptor.H, new Point3D(0.760536914211, -1.03517266068e-35, 0.204548524816), "a2"));
		atomList.add(new Atom(AtomDescriptor.H, new Point3D(-0.561754950762, 3.39295529441e-35, -0.551208356544), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomList, bondMap);
		
		ssipSurfaceInformation = new SsipSurfaceInformation(new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000, 0.0));
		
		ArrayList<Ssip> ssipList = new ArrayList<>();
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(-0.784,   0.048,  -1.589,  "a3", 2.943));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(1.714,   0.000,  -0.277, "a2",  2.927));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(-0.865,   1.043,   1.440, "a1",  -6.393));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(-0.904,  -0.928,   1.505, "a1",  -6.541));
		ssipList.add(ssip1);
		ssipList.add(ssip2);
		ssipList.add(ssip3);
		ssipList.add(ssip4);
		ssipContainer = new SsipContainer(ssipList);
		
		ssipMolecule = new SsipMolecule(cmlMolecule, ssipContainer, ssipSurfaceInformation);
	}

	@After
	public void tearDown() throws Exception {
		// use this to flush file contents.
		new FileOutputStream(new File(outputURI)).close();
	}
	
	@Test
	public void testWriteSSIPList() {
		String expectedString = "  1.000   0.000   0.000   0.002\n  1.000   1.000   1.000   0.001\n  0.000   1.000   1.000  -0.000\n  0.000   1.000   0.000  -0.000\n";
		String actualString = SsipMoleculeWriter.writeSSIPList(sSIPList);
		assertEquals(expectedString, actualString);
	}
	
	@Test
	public void testMarshal() throws MalformedURLException, SAXException{
		try {
			SsipMoleculeWriter.marshal(ssipMolecule, outputURI);
		} catch (JAXBException e) {
			LOG.error(e);
		}

		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(outputURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(comparisonFileURI)));
			LOG.debug(actualFileContents);

			// Ignore the "dynamic" value of ssipSoftwareVersion in the testing
		    final Predicate<Attr> attributeFilter = new Predicate<Attr>(){
	            @Override
	            public boolean test(Attr attrName) {
	            	return (!Nodes.getQName(attrName).toString().contains("ssipSoftwareVersion"));
	            }
		    };

			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter));
		} catch (IOException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	@Test
	public void testMarshalRoundTrip(){
		try {
			SsipMolecule cmlMolecule = SsipMoleculeReader.demarshal(comparisonFileURI);
			SsipMoleculeWriter.marshal(cmlMolecule, outputURI);
		} catch (JAXBException e) {
			LOG.error(e);
		}
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(outputURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(comparisonFileURI)));
			LOG.debug(actualFileContents);
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents));
		} catch (IOException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
}
