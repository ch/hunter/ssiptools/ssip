/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.io;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.xml.sax.SAXException;

import uk.ac.cam.ch.hunter.ssip.core.SsipContainer;
import uk.ac.cam.ch.hunter.ssip.core.SsipMolecule;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BondDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;
import uk.ac.cam.ch.hunter.ssip.core.graph.VF2;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;

public class SsipMoleculeReaderTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
		
	static URI outputURI;
	static URI comparisonFileURI;
	private CmlMolecule cmlMolecule;
	private SsipContainer ssipContainer;
	private SsipSurfaceInformation ssipSurfaceInformation;

	@Before
	public void setUp() throws Exception {
		File outFile = tempFolder.newFile("ssipMolecule.xml");
		outputURI = outFile.toURI();
		comparisonFileURI = SsipMoleculeReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/core/expectedSsipMolecule.xml").toURI();
		
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomList = new ArrayList<>();
		atomList.add(new Atom(AtomDescriptor.O, new Point3D(-0.197781963448, -7.67944186155e-36, 0.346659831728), "a1"));
		atomList.add(new Atom(AtomDescriptor.H, new Point3D(0.760536914211, -1.03517266068e-35, 0.204548524816), "a2"));
		atomList.add(new Atom(AtomDescriptor.H, new Point3D(-0.561754950762, 3.39295529441e-35, -0.551208356544), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomList, bondMap);
		
		ssipSurfaceInformation = new SsipSurfaceInformation(new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000, 0.0));
		
		ArrayList<Ssip> ssipList = new ArrayList<>();
		Ssip ssip1 = new Ssip(-0.784,   0.048,  -1.589,  2.943);
		ssip1.setNearestAtomID("a3");
		Ssip ssip2 = new Ssip(1.714,   0.000,  -0.277, 2.927);
		ssip2.setNearestAtomID("a2");
		Ssip ssip3 = new Ssip(-0.865,   1.043,   1.440, -6.393);
		ssip3.setNearestAtomID("a1");
		Ssip ssip4 = new Ssip(-0.904,  -0.928,   1.505, -6.541);
		ssip4.setNearestAtomID("a1");
		ssipList.add(ssip1);
		ssipList.add(ssip2);
		ssipList.add(ssip3);
		ssipList.add(ssip4);
		ssipContainer = new SsipContainer(ssipList);
		
	}

	@Test
	public void testDemarshal() throws JAXBException, MalformedURLException, SAXException{
		SsipMolecule actualSsipMolecule = SsipMoleculeReader.demarshal(comparisonFileURI);
		
		CmlMolecule actualCmlMolecule = actualSsipMolecule.getCmlMolecule();
		assertEquals(cmlMolecule.getMoleculeid(), actualCmlMolecule.getMoleculeid());
		assertEquals(cmlMolecule.getStdinchikey(), actualCmlMolecule.getStdinchikey());
		assertEquals(cmlMolecule.getAtomArrayList(), actualCmlMolecule.getAtomArrayList());
		assertEquals(cmlMolecule.getBondMap(), actualCmlMolecule.getBondMap());
		assertEquals(cmlMolecule.getAdjacencyRepresentation(), actualCmlMolecule.getAdjacencyRepresentation());
		assertTrue(cmlMolecule.getMolTopology().getVertices().size() == actualCmlMolecule.getMolTopology().getVertices().size()
				&& !(new VF2<AtomDescriptor, BondDescriptor>(cmlMolecule.getMolTopology(),
						actualCmlMolecule.getMolTopology()).findIsomorphisms().isEmpty()));
		
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> expectedMolPositions = new HashMap<>();
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(0),
				new Point3D(-0.197781963448, -7.67944186155e-36, 0.346659831728));
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(1),
				new Point3D(0.760536914211, -1.03517266068e-35, 0.204548524816));
		expectedMolPositions.put(actualCmlMolecule.getMolTopology().getVertices().get(2),
				new Point3D(-0.561754950762, 3.39295529441e-35, -0.551208356544));
		assertEquals(expectedMolPositions, actualCmlMolecule.getMolPositions());
		
		SsipSurfaceInformation actualSsipSurfaceInformation = actualSsipMolecule.getSsipSurfaceInformation();
		
		assertEquals(ssipSurfaceInformation.getSsipSurfaces(), actualSsipSurfaceInformation.getSsipSurfaces());
		
		assertEquals(ssipContainer.getSSIPs(), actualSsipMolecule.getSsipContainer().getSSIPs());
		
	}

}
