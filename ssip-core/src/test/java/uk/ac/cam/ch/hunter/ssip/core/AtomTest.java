/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.C;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.H;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.N;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;

import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class AtomTest {

	static Atom atom;
	static AtomDescriptor symbol;
	static int correctFactor;
	static Point3D position;
	static String atomId;

	@BeforeClass
	public static void setUp() {
		symbol = H;
		correctFactor = 2;
		position = new Point3D(1, 2, 3);
		atomId = "a1";
		atom = new Atom(symbol, position, correctFactor, atomId);
	}

	@Test
	public void testConstructors() {
		
		//First constructor
		Atom testAtom = new Atom(symbol, position, correctFactor, atomId);
		assertNotNull(testAtom);
		assertEquals(testAtom.getCorrectFactor(), correctFactor, 1E-3);
		assertEquals(position, testAtom.getPosition());
		assertEquals(symbol, testAtom.getSymbol());
		assertEquals(atomId, testAtom.getAtomId());
		
		//Second constructor
		testAtom = new Atom(symbol, position, atomId);
		assertNotNull(testAtom);
		assertEquals(1, testAtom.getCorrectFactor(), 1E-3);
		assertEquals(position, testAtom.getPosition());
		assertEquals(symbol, testAtom.getSymbol());
		assertEquals(atomId, testAtom.getAtomId());
		
		//test for failure constructor 1
		try {
			testAtom = new Atom(null,position,correctFactor, atomId);
			fail("Should not go here!");
		} catch (IllegalArgumentException e) {};
		
		//test for failure constructor 2
		try {
			testAtom = new Atom(symbol,null,correctFactor, atomId);
			fail("Should not go here!");
		} catch (IllegalArgumentException e) {};
	}

	@Test
	public void testFindAtomClosestToPoint() {
		Point3D point = new Point3D(0.3, 0.2, 0);

		Atom carbon = new Atom(C, new Point3D(1, 1, 0), "a1");
		Atom hydrogen = new Atom(H, new Point3D(-1, 1, 0), "a2");
		Atom nitrogen = new Atom(N, new Point3D(-1, -1, 0), "a3");
		Atom oxygen = new Atom(O, new Point3D(1, -1, 0), "a4");

		List<Atom> atoms = Arrays.asList(carbon, hydrogen, nitrogen, oxygen);

		Atom closestAtom = Atom.findAtomClosestToPoint(atoms, point);

		assertEquals(carbon, closestAtom);
	}
}