/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.Ar;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.C;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.H;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.He;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.N;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.Na;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.P;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.S;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.DOUBLE;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.SINGLE;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.TRIPLE;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.AROMATIC;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;


import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;


public class ChemicalStructureTest {

	// An example of a molecule which does NOT contain any functional group
	// currently specified in FunctionalGroup class.
	static Graph<AtomDescriptor, BondDescriptor> phenol;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex v1, v2, v3, v4, v5, v6,
			v7, v8, v9, v10, v11, v12, v13, v14;

	// An example of an artificial molecule which contains NON-OVERLAPPING
	// functional groups.
	static Graph<AtomDescriptor, BondDescriptor> nMol;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex n1, n2, n3, n4, n5, n6,
			n7, n8, n9;

	// An example of an artificial molecule which contains OVERLAPPING
	// functional groups. Note that actual molecules have much more
	// constraints than 'sMol', thus functional group identification for 'sMol'
	// is much more challenging than for most real molecules.
	static Graph<AtomDescriptor, BondDescriptor> sMol;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex s1, s2, s3, s4, s5, s6,
			s7, s8, s9, s10, s11, s12;

	// Example of multiple sp2 nitrogens including aromatic bonds.
	static Graph<AtomDescriptor, BondDescriptor> pyrMol;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex pyr1, pyr2,pyr3, pyr4,
			pyr5, pyr6, pyr7, pyr8, pyr9, pyr10, pyr11, pyr12, pyr13, pyr14,
			pyr15, pyr16, pyr17;
	
	// Example of nitro group.
	static Graph<AtomDescriptor, BondDescriptor> nitroMol;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex nitro1, nitro2, nitro3,
	       nitro4;
	
	@BeforeClass
	public static void setUp() {
		// Initializes phenol
		phenol = new Graph<>();
		v1 = phenol.addVertex(C);
		v2 = phenol.addVertex(C);
		v3 = phenol.addVertex(C);
		v4 = phenol.addVertex(C);
		v5 = phenol.addVertex(C);
		v6 = phenol.addVertex(C);
		v7 = phenol.addVertex(O);
		v8 = phenol.addVertex(H);
		v9 = phenol.addVertex(H);
		v10 = phenol.addVertex(H);
		v11 = phenol.addVertex(H);
		v12 = phenol.addVertex(H);
		v13 = phenol.addVertex(H);

		phenol.addEdge(v1, v7, SINGLE);
		phenol.addEdge(v1, v2, SINGLE);
		phenol.addEdge(v1, v6, DOUBLE);
		phenol.addEdge(v2, v8, SINGLE);
		phenol.addEdge(v2, v3, DOUBLE);
		phenol.addEdge(v3, v9, SINGLE);
		phenol.addEdge(v3, v4, SINGLE);
		phenol.addEdge(v4, v10, SINGLE);
		phenol.addEdge(v4, v5, DOUBLE);
		phenol.addEdge(v5, v11, SINGLE);
		phenol.addEdge(v5, v6, SINGLE);
		phenol.addEdge(v6, v12, SINGLE);
		phenol.addEdge(v7, v13, SINGLE);


		// Initialize nMol
		nMol = new Graph<>();
		n1 = nMol.addVertex(Ar);
		n2 = nMol.addVertex(Na);
		n3 = nMol.addVertex(C);
		n4 = nMol.addVertex(S);
		n5 = nMol.addVertex(O);
		n6 = nMol.addVertex(C);
		n7 = nMol.addVertex(H);
		n8 = nMol.addVertex(He);
		n9 = nMol.addVertex(O);

		nMol.addEdge(n1, n2, SINGLE);
		nMol.addEdge(n1, n3, SINGLE);
		nMol.addEdge(n2, n4, DOUBLE);
		nMol.addEdge(n3, n5, DOUBLE);
		nMol.addEdge(n3, n6, SINGLE);
		nMol.addEdge(n3, n7, SINGLE);
		nMol.addEdge(n4, n8, SINGLE);
		nMol.addEdge(n4, n9, SINGLE);

		// Initialize sMol
		sMol = new Graph<>();
		s1 = sMol.addVertex(O);
		s2 = sMol.addVertex(H);
		s3 = sMol.addVertex(C);
		s4 = sMol.addVertex(P);
		s5 = sMol.addVertex(S);
		s6 = sMol.addVertex(Na);
		s7 = sMol.addVertex(He);
		s8 = sMol.addVertex(C);
		s9 = sMol.addVertex(N);
		s10 = sMol.addVertex(Ar);
		s11 = sMol.addVertex(O);
		s12 = sMol.addVertex(O);

		sMol.addEdge(s1, s2, SINGLE);
		sMol.addEdge(s1, s3, SINGLE);
		sMol.addEdge(s1, s8, SINGLE);
		sMol.addEdge(s2, s10, SINGLE);
		sMol.addEdge(s3, s4, SINGLE);
		sMol.addEdge(s3, s5, SINGLE);
		sMol.addEdge(s3, s6, SINGLE);
		sMol.addEdge(s4, s11, SINGLE);
		sMol.addEdge(s5, s12, SINGLE);
		sMol.addEdge(s7, s8, SINGLE);
		sMol.addEdge(s8, s9, TRIPLE);
		
		//Initialise pyridine mol
		pyrMol = new Graph<>();
		pyr1 = pyrMol.addVertex(N);
		pyr2 = pyrMol.addVertex(C);
		pyr3 = pyrMol.addVertex(C);
		pyr4 = pyrMol.addVertex(C);
		pyr5 = pyrMol.addVertex(C);
		pyr6 = pyrMol.addVertex(C);
		pyr7 = pyrMol.addVertex(N);
		pyr8 = pyrMol.addVertex(C);
		pyr9 = pyrMol.addVertex(C);
		pyr10 = pyrMol.addVertex(H);
		pyr11 = pyrMol.addVertex(H);
		pyr12 = pyrMol.addVertex(H);
		pyr13 = pyrMol.addVertex(H);
		pyr14 = pyrMol.addVertex(H);
		pyr15 = pyrMol.addVertex(H);
		pyr16 = pyrMol.addVertex(H);
		pyr17 = pyrMol.addVertex(H);
		
		pyrMol.addEdge(pyr1, pyr2, AROMATIC);
		pyrMol.addEdge(pyr2, pyr3, AROMATIC);
		pyrMol.addEdge(pyr3, pyr4, AROMATIC);
		pyrMol.addEdge(pyr4, pyr5, AROMATIC);
		pyrMol.addEdge(pyr5, pyr6, AROMATIC);
		pyrMol.addEdge(pyr1, pyr6, AROMATIC);
		pyrMol.addEdge(pyr5, pyr7, SINGLE);
		pyrMol.addEdge(pyr7, pyr8, DOUBLE);
		pyrMol.addEdge(pyr8, pyr9, SINGLE);
		pyrMol.addEdge(pyr2, pyr10, SINGLE);
		pyrMol.addEdge(pyr3, pyr11, SINGLE);
		pyrMol.addEdge(pyr4, pyr12, SINGLE);
		pyrMol.addEdge(pyr6, pyr13, SINGLE);
		pyrMol.addEdge(pyr8, pyr14, SINGLE);
		pyrMol.addEdge(pyr9, pyr15, SINGLE);
		pyrMol.addEdge(pyr9, pyr16, SINGLE);
		pyrMol.addEdge(pyr9, pyr17, SINGLE);
		
		//Initialise nitro mol
		nitroMol = new Graph<>();
		nitro1 = nitroMol.addVertex(N);
		nitro2 = nitroMol.addVertex(O);
		nitro3 = nitroMol.addVertex(O);
		nitro4 = nitroMol.addVertex(C);

		
		nitroMol.addEdge(nitro1, nitro2, DOUBLE);
		nitroMol.addEdge(nitro1, nitro3, SINGLE);
		nitroMol.addEdge(nitro1, nitro4, SINGLE);

		
		
		
	}

	@Test
	public void testContructor() {
		ChemicalStructure molStruct = new ChemicalStructure(phenol);

		assertNotNull(molStruct);
		assertNotNull(molStruct.getCFactors());
		assertNotNull(molStruct.getMolTopology());

		try {
			molStruct = new ChemicalStructure(null);
			fail("Should throw an exception!");
		} catch (IllegalArgumentException e) {
		}
	}

	/**
	 * Tests if all functional groups are properly identified and located.
	 */
	@Test
	public void testFunctionalGroupIdentification() {
		ChemicalStructure molStruct;
		Map<FunctionalGroup, Set<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>>> expectedMolGroups;
		Set<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>> expectedIsomorphisms;
		BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex> expectedIsomorphism;
		List<Graph<AtomDescriptor, BondDescriptor>.Vertex> funcGroupVertices;
		Map<FunctionalGroup, Set<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>>> actualMolGroups;

		// Phenol - no functional groups should be found
		molStruct = new ChemicalStructure(phenol);
		actualMolGroups = molStruct.getMolGroups();

		// nMol - tests non-overlapping functional group identification

		molStruct = new ChemicalStructure(nMol);
		expectedMolGroups = new HashMap<>();

		// nMol -> Aldehyde

		funcGroupVertices = FunctionalGroup.ALDEHYDE.getChemicalStructure()
				.getVertices();

		expectedIsomorphism = HashBiMap.create();
		expectedIsomorphism.put(funcGroupVertices.get(0), n5);
		expectedIsomorphism.put(funcGroupVertices.get(1), n3);
		expectedIsomorphism.put(funcGroupVertices.get(2), n6);
		expectedIsomorphism.put(funcGroupVertices.get(3), n7);

		expectedIsomorphisms = new HashSet<>();
		expectedIsomorphisms.add(expectedIsomorphism);

		expectedMolGroups.put(FunctionalGroup.ALDEHYDE, expectedIsomorphisms);

		// nMol -> OS

		funcGroupVertices = FunctionalGroup.ANY_OXYGEN_ATOM_BONDED_TO_SULFUR
				.getChemicalStructure().getVertices();

		expectedIsomorphism = HashBiMap.create();
		expectedIsomorphism.put(funcGroupVertices.get(0), n4);
		expectedIsomorphism.put(funcGroupVertices.get(1), n9);

		expectedIsomorphisms = new HashSet<>();
		expectedIsomorphisms.add(expectedIsomorphism);

		expectedMolGroups.put(FunctionalGroup.ANY_OXYGEN_ATOM_BONDED_TO_SULFUR,
				expectedIsomorphisms);

		// nMol test
		actualMolGroups = molStruct.getMolGroups();
		assertEquals(expectedMolGroups, actualMolGroups);

		// sMol - tests overlapping functional group identification

		molStruct = new ChemicalStructure(sMol);
		expectedMolGroups = new HashMap<>();

		// sMol -> Alcohol

		funcGroupVertices = FunctionalGroup.ALCOHOL.getChemicalStructure()
				.getVertices();
		
		expectedIsomorphism = HashBiMap.create();
		expectedIsomorphism.put(funcGroupVertices.get(0), s1);
		expectedIsomorphism.put(funcGroupVertices.get(1), s2);
		expectedIsomorphism.put(funcGroupVertices.get(2), s3);

		expectedIsomorphisms = new HashSet<>();
		expectedIsomorphisms.add(expectedIsomorphism);

		expectedMolGroups.put(FunctionalGroup.ALCOHOL, expectedIsomorphisms);

		// sMol -> Nitrile

		funcGroupVertices = FunctionalGroup.NITRILE.getChemicalStructure()
				.getVertices();

		expectedIsomorphism = HashBiMap.create();
		expectedIsomorphism.put(funcGroupVertices.get(0), s7);
		expectedIsomorphism.put(funcGroupVertices.get(1), s8);
		expectedIsomorphism.put(funcGroupVertices.get(2), s9);

		expectedIsomorphisms = new HashSet<>();
		expectedIsomorphisms.add(expectedIsomorphism);

		expectedMolGroups.put(FunctionalGroup.NITRILE, expectedIsomorphisms);

		// sMol test
		actualMolGroups = molStruct.getMolGroups();
		// assertTrue(actualMolGroups.equals(expectedMolGroups));

		// More complicated comparison is involved since 'ALCOHOL' contains more
		// than one wild card which creates permutations.
//		assertTrue(actualMolGroups
//				.get(FunctionalGroup.ALCOHOL)
//				.iterator()
//				.next()
//				.values()
//				.equals(expectedMolGroups.get(FunctionalGroup.ALCOHOL)
//						.iterator().next().values())
//				&& actualMolGroups.keySet().equals(expectedMolGroups.keySet())
//				&& actualMolGroups
//						.get(FunctionalGroup.NITRILE)
//						.iterator()
//						.next()
//						.values()
//						.equals(expectedMolGroups.get(FunctionalGroup.NITRILE)
//								.iterator().next().values()));
	}

	/**
	 * Tests if correction factors were assigned to vertices properly.
	 */
	@Test
	public void testCFactorAssignmentPhenol() {
		ChemicalStructure molStruct;
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> expectedCFactors, actualCFactors;

		// Phenol
		molStruct = new ChemicalStructure(phenol);

		expectedCFactors = new HashMap<>();
		expectedCFactors.put(v1, 1.0);
		expectedCFactors.put(v2, 1.0);
		expectedCFactors.put(v3, 1.0);
		expectedCFactors.put(v4, 1.0);
		expectedCFactors.put(v5, 1.0);
		expectedCFactors.put(v6, 1.0);
		expectedCFactors.put(v7, 0.95);
		expectedCFactors.put(v8, 1.0);
		expectedCFactors.put(v9, 1.0);
		expectedCFactors.put(v10, 1.0);
		expectedCFactors.put(v11, 1.0);
		expectedCFactors.put(v12, 1.0);
		expectedCFactors.put(v13, 1.0);

		actualCFactors = molStruct.getCFactors();
		assertEquals(expectedCFactors, actualCFactors);
	}

	@Test
	public void testCFactorAssignmentNmol() {
		ChemicalStructure molStruct;
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> expectedCFactors, actualCFactors;

		// 'nMol'
		molStruct = new ChemicalStructure(nMol);

		expectedCFactors = new HashMap<>();
		expectedCFactors.put(n1, 1.0);
		expectedCFactors.put(n2, 1.0);
		expectedCFactors.put(n3, 1.0);
		expectedCFactors.put(n4, 1.0);
		expectedCFactors.put(n5, 0.89);
		expectedCFactors.put(n6, 1.0);
		expectedCFactors.put(n7, 1.0);
		expectedCFactors.put(n8, 1.0);
		expectedCFactors.put(n9, 0.99);

		actualCFactors = molStruct.getCFactors();
		assertEquals(expectedCFactors, actualCFactors);

	}

	@Test
	public void testCFactorAssignmentSmol() {
		ChemicalStructure molStruct;
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> expectedCFactors, actualCFactors;

		// 'sMol'
		molStruct = new ChemicalStructure(sMol);

		expectedCFactors = new HashMap<>();
		expectedCFactors.put(s1, 1.0);
		expectedCFactors.put(s2, 1.0);
		expectedCFactors.put(s3, 1.0);
		expectedCFactors.put(s4, 1.0);
		expectedCFactors.put(s5, 1.0);
		expectedCFactors.put(s6, 1.0);
		expectedCFactors.put(s7, 1.0);
		expectedCFactors.put(s8, 1.0);
		expectedCFactors.put(s9, 0.77);
		expectedCFactors.put(s10, 1.0);
		expectedCFactors.put(s11, 1.09);
		expectedCFactors.put(s12, 0.99);

		actualCFactors = molStruct.getCFactors();
		assertEquals(expectedCFactors, actualCFactors);
	}
	
	@Test
	public void testCFactorAssignmentPyrMol() {
		ChemicalStructure molStruct;
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> expectedCFactors, actualCFactors;

		molStruct = new ChemicalStructure(pyrMol);
		expectedCFactors = new HashMap<>();
		expectedCFactors.put(pyr1, 1.08);
		expectedCFactors.put(pyr2, 1.0);
		expectedCFactors.put(pyr3, 1.0);
		expectedCFactors.put(pyr4, 1.0);
		expectedCFactors.put(pyr5, 1.0);
		expectedCFactors.put(pyr6, 1.0);
		expectedCFactors.put(pyr7, 1.08);
		expectedCFactors.put(pyr8, 1.0);
		expectedCFactors.put(pyr9, 1.0);
		expectedCFactors.put(pyr10, 1.0);
		expectedCFactors.put(pyr11, 1.0);
		expectedCFactors.put(pyr12, 1.0);
		expectedCFactors.put(pyr13, 1.0);
		expectedCFactors.put(pyr14, 1.0);
		expectedCFactors.put(pyr15, 1.0);
		expectedCFactors.put(pyr16, 1.0);
		expectedCFactors.put(pyr17, 1.0);
		
		actualCFactors = molStruct.getCFactors();
		assertEquals(expectedCFactors, actualCFactors);
	}
	
	@Test
	public void testCFactorAssignmentNitroMol() {
		ChemicalStructure molStruct;
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> expectedCFactors, actualCFactors;

		molStruct = new ChemicalStructure(nitroMol);
		expectedCFactors = new HashMap<>();
		expectedCFactors.put(nitro1, 1.0);
		expectedCFactors.put(nitro2, 0.8);
		expectedCFactors.put(nitro3, 0.8);
		expectedCFactors.put(nitro4, 1.0);
		actualCFactors = molStruct.getCFactors();
		assertEquals(expectedCFactors, actualCFactors);
	}
}
