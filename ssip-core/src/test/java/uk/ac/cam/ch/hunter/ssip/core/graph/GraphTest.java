/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.B;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.C;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.H;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.N;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.SINGLE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BondDescriptor;

/**
 * @author tn300
 * 
 *         The graph used for testing can be found in [1], figure 2b. The labels
 *         assigned to the graph are completely random.
 * 
 *         [1] DOI:10.14778/2535568.2448946 W.-S. Han, R. Kasperovics, J.Lee,
 *         J.-H. Lee. An In-depth Comparison of Subgraph Isomorphism Algorithms
 *         in Graph Databases. PVLDB, 2012.
 */
public class GraphTest {

	static Graph<AtomDescriptor, BondDescriptor> graph;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex v1, v2, v3, v4, v5, v6, v7, v8, v9;

	@BeforeClass
	public static void setUp() {

		graph = new Graph<>();

		v1 = graph.addVertex(C);
		v2 = graph.addVertex(N);
		v3 = graph.addVertex(O);
		v4 = graph.addVertex(H);
		v5 = graph.addVertex(C);
		v6 = graph.addVertex(N);
		v7 = graph.addVertex(O);
		v8 = graph.addVertex(H);
		v9 = graph.addVertex(B);

		BondDescriptor value = SINGLE;
		graph.addEdge(v1, v4, value);
		graph.addEdge(v2, v4, value);
		graph.addEdge(v2, v5, value);
		graph.addEdge(v3, v5, value);
		graph.addEdge(v3, v6, value);
		graph.addEdge(v4, v5, value);
		graph.addEdge(v4, v8, value);
		graph.addEdge(v5, v6, value);
		graph.addEdge(v5, v9, value);
		graph.addEdge(v7, v8, value);
	}

	@Test
	public void testConstructor() {
		Graph<AtomDescriptor, BondDescriptor> testGraph = new Graph<>();
		assertNotNull(testGraph);
		assertNotNull(testGraph.getVertices());
	}

	@Test
	public void testFindNumberOfAdjacentVertices() {
		assertEquals(4, graph.findNumberOfAdjacentVertices(v4));
		assertEquals(5, graph.findNumberOfAdjacentVertices(v5));
		assertEquals(1, graph.findNumberOfAdjacentVertices(v9));
	}

	@Test
	public void testFindSetAdjacentVertices() {

		Set<Graph<AtomDescriptor, BondDescriptor>.Vertex> inputSetOfVertices1 = new HashSet<>(Arrays.asList(v4, v5));
		Set<Graph<AtomDescriptor, BondDescriptor>.Vertex> predictedSetOfVertices1 = new HashSet<>(
				Arrays.asList(v8, v9, v6, v2, v1, v3));

		assertEquals(predictedSetOfVertices1, graph.findSetAdjacentVertices(inputSetOfVertices1));

		Set<Graph<AtomDescriptor, BondDescriptor>.Vertex> inputSetOfVertices2 = new HashSet<>(Arrays.asList(v3, v4));
		Set<Graph<AtomDescriptor, BondDescriptor>.Vertex> predictedSetOfVertices2 = new HashSet<>(
				Arrays.asList(v1, v2, v5, v6, v8));

		assertEquals(predictedSetOfVertices2, graph.findSetAdjacentVertices(inputSetOfVertices2));

		Set<Graph<AtomDescriptor, BondDescriptor>.Vertex> inputSetOfVertices3 = new HashSet<>(Arrays.asList(v8));
		Set<Graph<AtomDescriptor, BondDescriptor>.Vertex> predictedSetOfVertices3 = new HashSet<>(
				Arrays.asList(v4, v7));

		assertEquals(predictedSetOfVertices3, graph.findSetAdjacentVertices(inputSetOfVertices3));
	}

	@Test
	public void testFindAdjacentVertices() {

		Set<Graph<AtomDescriptor, BondDescriptor>.Vertex> adjacentVertices1 = new HashSet<>(
				Arrays.asList(v1, v2, v5, v8));
		assertEquals(adjacentVertices1, graph.findAdjacentVertices(v4));

		Set<Graph<AtomDescriptor, BondDescriptor>.Vertex> adjacentVertices2 = new HashSet<>(
				Arrays.asList(v4, v7));
		assertEquals(adjacentVertices2, graph.findAdjacentVertices(v8));
	}

	@Test
	public void testHasEdge() {
		assertTrue(graph.hasEdge(v3, v6));
		assertTrue(graph.hasEdge(v6, v3));
		assertFalse(graph.hasEdge(v3, v1));
	}

	@Test
	public void testGetEdgeLabel() {
		assertEquals(SINGLE, graph.getEdgeLabel(v3, v6));
	}

	@Test
	public void testGetVertexLabel() {
		assertEquals(H, graph.getVertexLabel(v4));
	}

	@Test
	public void testGetNumberOfVertices() {
		assertEquals(9, graph.getNumberOfVertices());
	}

	@Test
	public void testGetVertices() {
		ArrayList<Graph<AtomDescriptor, BondDescriptor>.Vertex> expectedVertices = new ArrayList<>(
				Arrays.asList(v1, v2, v3, v4, v5, v6, v7, v8, v9));
		assertEquals(expectedVertices, graph.getVertices());
	}

	@Test
	public void testToString() {
		String graphRepresentation = "1C -> 4S \n" + "2N -> 4S 5S \n" + "3O -> 5S 6S \n" + "4H -> 1S 2S 5S 8S \n"
				+ "5C -> 2S 3S 4S 6S 9S \n" + "6N -> 3S 5S \n" + "7O -> 8S \n" + "8H -> 4S 7S \n" + "9B -> 5S \n";

		assertEquals(graphRepresentation, graph.toString());
	}

	// TODO: test addEdge
	@Test
	public void testAddEdge() {
		Graph<AtomDescriptor, BondDescriptor> testGraph = new Graph<>();
		Graph<AtomDescriptor, BondDescriptor>.Vertex v1, v2;
		v1 = testGraph.addVertex(C);
		v2 = testGraph.addVertex(H);
		testGraph.addEdge(v1, v2, SINGLE);

		// TODO: access private member 'edges'.
	}

	// TODO: test private constructor 'Vertex'

	@Test
	public void testAddVertex() {
		Graph<AtomDescriptor, BondDescriptor> testGraph = new Graph<>();
		Graph<AtomDescriptor, BondDescriptor>.Vertex v = testGraph.addVertex(C);
		assertTrue(v.equals(testGraph.getVertices().iterator().next()));
	}
}