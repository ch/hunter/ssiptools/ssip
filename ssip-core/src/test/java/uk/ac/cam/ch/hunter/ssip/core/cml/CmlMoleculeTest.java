/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.cml;

import static org.junit.Assert.*;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.H;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.DOUBLE;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.SINGLE;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.TRIPLE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BondDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;
import uk.ac.cam.ch.hunter.ssip.core.graph.VF2;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class CmlMoleculeTest {

	private static final Logger LOG = LogManager.getLogger(CmlMoleculeTest.class);
	
	private String stdinchikey;
	private String moleculeid;
	private ArrayList<Atom> atomlist;
	private HashMap<String, String> bondMap;
	private HashMap<Integer, HashMap<Integer, String>> adjacenyRepresentation;
	private CmlMolecule cmlMolecule;
	
	@Before
	public void setUp() throws Exception {
		stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		adjacenyRepresentation = new HashMap<>();
		HashMap<Integer, String> oxyMap = new HashMap<>();
		oxyMap.put(1, "1");
		oxyMap.put(2, "1");
		HashMap<Integer, String> h1Map = new HashMap<>();
		h1Map.put(0, "1");
		HashMap<Integer, String> h2Map = new HashMap<>();
		h2Map.put(0, "1");
		adjacenyRepresentation.put(0, oxyMap);
		adjacenyRepresentation.put(1, h1Map);
		adjacenyRepresentation.put(2, h2Map);
		
		cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
	}

	@Test
	public void testCmlMolecule() {
		assertEquals(moleculeid, cmlMolecule.getMoleculeid());
		assertEquals(stdinchikey, cmlMolecule.getStdinchikey());
		assertEquals(atomlist, cmlMolecule.getAtomArrayList());
		assertEquals(bondMap, cmlMolecule.getBondMap());
		assertEquals(adjacenyRepresentation, cmlMolecule.getAdjacencyRepresentation());
	}

	@Test
	public void testcreateBondMapFromAdjacencyRepresentation() throws Exception {
		Map<String, String> actualBondMap = cmlMolecule.createBondMapFromAdjacencyRepresentation();
		assertEquals(bondMap, actualBondMap);		
	}
	
	@Test
	public void testcreateAtomRefsFromIndices() throws Exception {
		String[] expectedAtomRefs = {"a1 a2", "a2 a1"};
		String[] actualAtomRefs = cmlMolecule.createAtomRefsFromIndices(0, 1);
		assertArrayEquals(expectedAtomRefs, actualAtomRefs);
	}

	@Test
	public void testcreateAdjacencyRepresentationFromBondMap() throws Exception {
		Map<Integer, HashMap<Integer, String>> actualAdjRep = cmlMolecule.createAdjacencyRepresentationFromBondMap();
		assertEquals(adjacenyRepresentation, actualAdjRep);
	}
	
	@Test
	public void testparseAtomIDs() throws Exception {
		String[] expectedAtomIds = {"a1", "a2"};
		String[] actualAtomIds = cmlMolecule.parseAtomIDs("a1 a2");
		assertArrayEquals(expectedAtomIds, actualAtomIds);
	}
	
	@Test
	public void testgetAtomIdList() throws Exception {
		ArrayList<String> expectedArrayList = new ArrayList<>();
		expectedArrayList.add("a1");
		expectedArrayList.add("a2");
		expectedArrayList.add("a3");
		List<String> actualAtomIds = cmlMolecule.getAtomIdList();
		assertEquals(expectedArrayList, actualAtomIds);
	}
	
	@Test
	public void testupdateAdjacencyRepresentation() throws Exception {
		Map<Integer, HashMap<Integer, String>> expectedMap = new HashMap<>();
		HashMap<Integer, String> innerMap1 = new HashMap<>();
		innerMap1.put(1, "1");
		expectedMap.put(0, innerMap1);
		HashMap<Integer, String> innerMap2 = new HashMap<>();
		innerMap2.put(0, "1");
		expectedMap.put(1, innerMap2);
		Map<Integer, HashMap<Integer, String>> actualMap = new HashMap<>();
		cmlMolecule.updateAdjacencyRepresentation(actualMap, 0, 1, "1");
		assertEquals(expectedMap, actualMap);
	}
	
	@Test
	public void testupdateAdjacencyRepresentationFirstAtom() throws Exception {
		Map<Integer, HashMap<Integer, String>> expectedMap = new HashMap<>();
		HashMap<Integer, String> innerMap1 = new HashMap<>();
		innerMap1.put(1, "1");
		expectedMap.put(0, innerMap1);
		Map<Integer, HashMap<Integer, String>> actualMap = new HashMap<>();
		cmlMolecule.updateAdjacencyRepresentationFirstAtom(actualMap, 0, 1, "1");
		assertEquals(expectedMap, actualMap);
	}
	
	@Test
	public void testGetMolTopologyWater() {
		
		Graph<AtomDescriptor, BondDescriptor> expectedMolStruct = new Graph<>();
		Graph<AtomDescriptor, BondDescriptor>.Vertex v1, v2, v3;
		v1 = expectedMolStruct.addVertex(O);
		v2 = expectedMolStruct.addVertex(H);
		v3 = expectedMolStruct.addVertex(H);
		
		expectedMolStruct.addEdge(v1, v2, SINGLE);
		expectedMolStruct.addEdge(v1, v3, SINGLE);
		
		Graph<AtomDescriptor, BondDescriptor> actualMolStruct = cmlMolecule.getMolTopology();
		
		assertTrue(expectedMolStruct.getVertices().size() == actualMolStruct
				.getVertices().size()
				&& !(new VF2<AtomDescriptor, BondDescriptor>(expectedMolStruct,
						actualMolStruct).findIsomorphisms().isEmpty()));
	}

	@Test
	public void testGetMolPositionsWater() {
		
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> expectedMolPositions = new HashMap<>();
		
		Graph<AtomDescriptor, BondDescriptor> actualMolStruct = cmlMolecule.getMolTopology();
		
		expectedMolPositions.put(actualMolStruct.getVertices().get(0),
				new Point3D(0.0, 1.0, 0.0));
		expectedMolPositions.put(actualMolStruct.getVertices().get(1),
				new Point3D(1.0, 1.0, 0.0));
		expectedMolPositions.put(actualMolStruct.getVertices().get(2),
				new Point3D(-1.0, 1.0, 0.0));
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> actualMolPositions = cmlMolecule
				.getMolPositions();
		LOG.debug("expectedMolPositions");
		LOG.debug(expectedMolPositions.toString());
		LOG.debug("actualMolPostions");
		LOG.debug(actualMolPositions.toString());
		assertEquals(expectedMolPositions, actualMolPositions);
	}
	
	@Test
	public void testBondMapper() throws Exception {
		BondDescriptor b;

		b = CmlMolecule.bondMapper("1");
		assertSame(SINGLE, b);

		b = CmlMolecule.bondMapper("2");
		assertSame(DOUBLE, b);

		b = CmlMolecule.bondMapper("3");
		assertSame(TRIPLE, b);

		b = CmlMolecule.bondMapper("A");
		assertSame(BondDescriptor.AROMATIC, b);
		
		try {
			b = CmlMolecule.bondMapper("-1");
			fail("Should not go here!");
		} catch (IllegalArgumentException e) {
		}
	}
	
	@Test
	public void testGetMoleculeCentroid(){
		Point3D expectedCentroid = new Point3D(0.0, 1.0, 0.0);
		assertEquals(expectedCentroid, cmlMolecule.getMoleculeCentroid());
	}
}
