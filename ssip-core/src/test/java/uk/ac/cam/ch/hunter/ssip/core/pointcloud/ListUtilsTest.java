/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import org.junit.Test;

import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.X_AXIS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.ListUtils.findPartition;

public class ListUtilsTest {

	@Test
	public void testSelect() {

		// ordered along x : 0.1 , 0.3 , 1 , 1 , 1.1, 1.2, 1.45 , 2, 7
		ArrayList<Point3D> points = new ArrayList<>(Arrays.asList(new Point3D(1, 0, 0),
				new Point3D(2, 0, 0), new Point3D(7, 0, 0), new Point3D(1, 1, 0), new Point3D(1.45,
						0, 0), new Point3D(1.2, 0, 0), new Point3D(0.3, 0, 0), new Point3D(0.1, 0,
						0), new Point3D(1.1, 0, 0)));
		AxisDistanceComparator<Point3D> comparator = new AxisDistanceComparator<>(X_AXIS);
		int k;

		// should be either (1,0,0) or (1,1,0)
		k = 3;
		ListUtils.quickSelect(points, comparator, 0, points.size() - 1, k);
		assertTrue(points.get(k).equals(new Point3D(1, 0, 0))
				|| points.get(k).equals(new Point3D(1, 1, 0)));

		// should be (1.1, 0, 0)
		k = 4;
		Collections.shuffle(points);
		ListUtils.quickSelect(points, comparator, 0, points.size() - 1, k);
		assertEquals(new Point3D(1.1, 0, 0), points.get(k));

		// 'left' == 'k' == 'right' - does nothing
		k = 3;
		Collections.shuffle(points);
		ArrayList<Point3D> shuffled = new ArrayList<>(points);
		ListUtils.quickSelect(points, comparator, k, k, k);
		assertEquals(shuffled, points);

		// test for failure

		// 'left' is out of bounds
		try {
			ListUtils.quickSelect(points, comparator, points.size(), points.size() - 1,
					points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// 'right' is out of bounds
		try {
			ListUtils.quickSelect(points, comparator, 0, points.size(), points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// 'k' is not between 'left' and 'right'
		try {
			ListUtils.quickSelect(points, comparator, points.size() / 2, points.size() - 1, 0);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// test for null
		try {
			ListUtils.quickSelect(null, comparator, 0, points.size() - 1, points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// test for null
		try {
			ListUtils.quickSelect(points, null, 0, points.size() - 1, points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// list is empty
		try {
			ListUtils.quickSelect(new ArrayList<Point3D>(0), comparator, 0, points.size() - 1,
					points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}
	}

	@Test
	public void testFindPartition() {

		// ordered along x : 0.1 , 0.3 , 1 , 1 , 1.1, 1.2, 1.45 , 2, 7
		ArrayList<Point3D> points = new ArrayList<>(Arrays.asList(new Point3D(1, 0, 0),
				new Point3D(2, 0, 0), new Point3D(7, 0, 0), new Point3D(1, 1, 0), new Point3D(1.45,
						0, 0), new Point3D(1.2, 0, 0), new Point3D(0.3, 0, 0), new Point3D(0.1, 0,
						0), new Point3D(1.1, 0, 0)));
		AxisDistanceComparator<Point3D> comparator = new AxisDistanceComparator<>(X_AXIS);

		// pivot value is (1.45, 4 ,5) at initial position 4 in the list

		int pivotIndex = 4;
		Point3D pivotValue = points.get(pivotIndex);
		findPartition(points, comparator, 0, points.size() - 1, pivotIndex);

		// tests if pivot is at expected position

		assertEquals(6, points.indexOf(pivotValue));

		// tests if points on left and right of pivot are smaller and bigger

		HashSet<Point3D> pointsLeftOfPivotIndex = new HashSet<>(points.subList(0,
				points.indexOf(pivotValue)));
		HashSet<Point3D> pointsRightOfPivotIndex = new HashSet<>(points.subList(
				points.indexOf(pivotValue) + 1, points.size()));

		// left of pivot : points with 'x' coords 0.1 , 0.3 , 1 , 1 , 1.1, 1.2
		HashSet<Point3D> expectedPointsLeftOfPivotIndex = new HashSet<>(Arrays.asList(new Point3D(
				0.1, 0, 0), new Point3D(0.3, 0, 0), new Point3D(1, 1, 0), new Point3D(1, 0, 0),
				new Point3D(1.1, 0, 0), new Point3D(1.2, 0, 0)));

		// right of pivot : points with 'x' coords 2, 7
		HashSet<Point3D> expectedPointsRightOfPivotIndex = new HashSet<>(Arrays.asList(new Point3D(
				7, 0, 0), new Point3D(2, 0, 0)));

		assertEquals(expectedPointsRightOfPivotIndex, pointsRightOfPivotIndex);
		assertEquals(expectedPointsLeftOfPivotIndex, pointsLeftOfPivotIndex);

		// test for failure
		// TODO: tests

		// 'left' is out of bounds
		try {
			findPartition(points, comparator, points.size(), points.size() - 1, points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// 'right' is out of bounds
		try {
			findPartition(points, comparator, 0, points.size(), points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// 'k' is not between 'left' and 'right'
		try {
			findPartition(points, comparator, points.size() / 2, points.size() - 1, 0);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// test for null
		try {
			findPartition(null, comparator, 0, points.size() - 1, points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// test for null
		try {
			findPartition(points, null, 0, points.size() - 1, points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}

		// list is empty
		try {
			findPartition(new ArrayList<Point3D>(0), comparator, 0, points.size() - 1,
					points.size() / 2);
			fail("Should not go here.");
		} catch (IllegalArgumentException e) {
		}
	}
}