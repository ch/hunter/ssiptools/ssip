/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;
import static org.junit.Assert.assertEquals;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.ALCOHOL;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.ALDEHYDE;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.ANY_OXYGEN_ATOM_BONDED_TO_PHOSPHORUS;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.ANY_OXYGEN_ATOM_BONDED_TO_SULFUR;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.CARBONATE;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.ESTER;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.ETHER;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.NITRILE;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.NITRO;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.PRIMARY_AMINE;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.PYRIDINE;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.SECONDARY_AMINE;
import static uk.ac.cam.ch.hunter.ssip.core.FunctionalGroup.TERTIARY_AMINE;

import org.junit.Test;

//The line below tells Eclipse to switch off automatic formatting.
//@formatter:off

public class FunctionalGroupTest {
	
		@Test
		public void nitrile() {
		String expectedNitrileStructure = "1R -> 2S \n" +
								  		  "2C -> 1S 3T \n" +
								  		  "3N -> 2T \n";
		String resultingNitrileStructure = NITRILE.getChemicalStructure().toString();
		assertEquals(expectedNitrileStructure, resultingNitrileStructure);
		assertEquals(3, FunctionalGroup.NITRILE.getNumberOfAtom());
		assertEquals(1, FunctionalGroup.NITRILE.getNumberOfWildcards());
		}
		
		@Test
		public void primaryAmine(){
		String expectedPrimaryAmineStructure = "1N -> 2S 3S 4S \n" +
											   "2H -> 1S \n" +
											   "3H -> 1S \n" +
											   "4C -> 1S \n";
		String resultingPrimaryAmineStructure = PRIMARY_AMINE.getChemicalStructure().toString();
		assertEquals(expectedPrimaryAmineStructure, resultingPrimaryAmineStructure);
		assertEquals(4, FunctionalGroup.PRIMARY_AMINE.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.PRIMARY_AMINE.getNumberOfWildcards());
		}
		
		@Test
		public void secondaryAmine(){
		String expectedSecondaryAmineStructure = "1N -> 2S 3S 4S \n" +
				   								 "2H -> 1S \n" +
				   								 "3C -> 1S \n" +
				   								 "4C -> 1S \n";

		String resultingSecondaryAmineStructure = SECONDARY_AMINE.getChemicalStructure().toString();
		assertEquals(expectedSecondaryAmineStructure, resultingSecondaryAmineStructure);
		assertEquals(4, FunctionalGroup.SECONDARY_AMINE.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.SECONDARY_AMINE.getNumberOfWildcards());
		}
		
		@Test
		public void tertiaryAmine(){
		String expectedTertiaryAmineStructure = "1N -> 2S 3S 4S \n" +
					 							"2C -> 1S \n" +
					 							"3C -> 1S \n" +
					 							"4C -> 1S \n";
		
		String resultingTertiaryAmineStructure = TERTIARY_AMINE.getChemicalStructure().toString();
		assertEquals(expectedTertiaryAmineStructure, resultingTertiaryAmineStructure);
		assertEquals(4, FunctionalGroup.TERTIARY_AMINE.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.TERTIARY_AMINE.getNumberOfWildcards());
		}
		
		@Test
		public void pyridine(){
		String expectedPyridineStructure = "1N -> 2S 3D \n" +
										   "2R -> 1S \n" +
										   "3R -> 1D \n";
		String resultingPyridineStructure = PYRIDINE.getChemicalStructure().toString();
		assertEquals(expectedPyridineStructure, resultingPyridineStructure);
		assertEquals(3, FunctionalGroup.PYRIDINE.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.PYRIDINE.getNumberOfWildcards());
		}
		
		@Test
		public void ether(){
		String expectedEtherStructure = "1O -> 2S 3S \n" + 
										"2C -> 1S \n" +
										"3C -> 1S \n";
		String resultingEtherStructure = ETHER.getChemicalStructure().toString();
		assertEquals(expectedEtherStructure, resultingEtherStructure);
		assertEquals(3, FunctionalGroup.ETHER.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.ETHER.getNumberOfWildcards());
		}
		
		@Test
		public void alcohol(){
		String expectedAlcoholStructure = "1O -> 2S 3S \n" +
										  "2H -> 1S \n" +
										  "3C -> 1S \n";
		String resultingAlcoholStructure = ALCOHOL.getChemicalStructure().toString();
		assertEquals(expectedAlcoholStructure, resultingAlcoholStructure);
		assertEquals(3, FunctionalGroup.ALCOHOL.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.ALCOHOL.getNumberOfWildcards());
		}
		
		@Test
		public void aldehyde(){
		String expectedAldehydeStructure = "1O -> 2D \n"
										 + "2C -> 1D 3S 4S \n"
										 + "3C -> 2S \n"
										 + "4H -> 2S \n";
		String resultingAldehydeStructure = ALDEHYDE.getChemicalStructure().toString();
		assertEquals(expectedAldehydeStructure, resultingAldehydeStructure);
		assertEquals(4, FunctionalGroup.ALDEHYDE.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.ALDEHYDE.getNumberOfWildcards());
		}
		
		@Test
		public void ester(){
		String expectedEsterStructure = "1O -> 2D \n"
									  + "2C -> 1D 3S 4S \n"
									  + "3C -> 2S \n"
									  + "4O -> 2S 5S \n"
									  + "5C -> 4S \n";
		String resultingEsterStructure = ESTER.getChemicalStructure().toString();
		assertEquals(expectedEsterStructure, resultingEsterStructure);
		assertEquals(5, FunctionalGroup.ESTER.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.ESTER.getNumberOfWildcards());
		}
		
		@Test
		public void carbonate(){
		String expectedCarbonateStructure = "1O -> 2D \n"
										  + "2C -> 1D 3S 4S \n"
										  + "3O -> 2S 5S \n"
										  + "4O -> 2S 6S \n"
										  + "5C -> 3S \n"
										  + "6C -> 4S \n";
		String resultingCarbonateStructure = CARBONATE.getChemicalStructure().toString();
		assertEquals(expectedCarbonateStructure, resultingCarbonateStructure);
		assertEquals(6, FunctionalGroup.CARBONATE.getNumberOfAtom());
		assertEquals(0, FunctionalGroup.CARBONATE.getNumberOfWildcards());
		}
		
		@Test
		public void nitro(){
		String expectedNitroStructure = "1N -> 2W 3W 4S \n"
									  + "2O -> 1W \n"
									  + "3O -> 1W \n"
									  + "4R -> 1S \n";
		String resultingNitroStructure = NITRO.getChemicalStructure().toString();
		assertEquals(expectedNitroStructure, resultingNitroStructure);
		assertEquals(4, FunctionalGroup.NITRO.getNumberOfAtom());
		assertEquals(3, FunctionalGroup.NITRO.getNumberOfWildcards());
		}
		
		@Test
		public void anyOxygenAtomBondedToSulfur(){
		String expectedSulfurStructure = "1S -> 2W \n"
									   + "2O -> 1W \n";
		String resultingSulfurStructure = ANY_OXYGEN_ATOM_BONDED_TO_SULFUR.getChemicalStructure().toString();
		assertEquals(expectedSulfurStructure, resultingSulfurStructure);
		assertEquals(2, FunctionalGroup.ANY_OXYGEN_ATOM_BONDED_TO_SULFUR.getNumberOfAtom());
		assertEquals(1, FunctionalGroup.ANY_OXYGEN_ATOM_BONDED_TO_SULFUR.getNumberOfWildcards());
		}
		
		@Test
		public void anyOxygenAtomBondedToPhosphorus(){
		String expectedPhosphorusStructure = "1P -> 2W \n"
										   + "2O -> 1W \n";
		String resultingPhosphorusStructure = ANY_OXYGEN_ATOM_BONDED_TO_PHOSPHORUS.getChemicalStructure().toString();
		assertEquals(expectedPhosphorusStructure, resultingPhosphorusStructure);
		assertEquals(2, FunctionalGroup.ANY_OXYGEN_ATOM_BONDED_TO_PHOSPHORUS.getNumberOfAtom());
		assertEquals(1, FunctionalGroup.ANY_OXYGEN_ATOM_BONDED_TO_PHOSPHORUS.getNumberOfWildcards());
		}
}