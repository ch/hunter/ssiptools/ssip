/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class LeafKDNodeTest {

	@Test
	public void testConstructor() {

		// ----Drawing a picture is highly recommended-----

		final Point3D nearEdgeOfBoundingBox = new Point3D();
		final Point3D farEdgeOfBoundingBox = new Point3D(1, 1, 1);
		ArrayList<Point3D> pointsInBoundingBox = new ArrayList<>(Arrays.asList(new Point3D()));
		LeafKDNode<Point3D> node = new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox,
				pointsInBoundingBox);

		assertNotNull(node);
		assertEquals(pointsInBoundingBox, node.getPointsInBoundingBox());
		assertEquals(nearEdgeOfBoundingBox, node.getNearEdgeOfBoundingBox());
		assertEquals(farEdgeOfBoundingBox, node.getFarEdgeOfBoundingBox());

		// test for failure

		// if list is null
		try {
			new LeafKDNode<>(nearEdgeOfBoundingBox, farEdgeOfBoundingBox, null);
			fail("Should throw an exception.");
		} catch (IllegalArgumentException e) {
		}
	}
}