/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class SsipContainerTest {

	private HashSet<Ssip> ssipList = new HashSet<Ssip>();
	private Ssip ssip1, ssip2, ssip3, ssip4;

	@Before
	public void setUp() {
		AlphaPoint mep1 = new AlphaPoint(1.0, 1.0, 1.0, 0.1);
		ssip1 = Ssip.fromMepPoint(mep1);
		AlphaPoint mep2 = new AlphaPoint(1.0, 0.0, 0.0, 0.2);
		ssip2 = Ssip.fromMepPoint(mep2);
		Atom betaAtom = Mockito.mock(Atom.class);
		BetaPoint mep3 = new BetaPoint(0.0, 1.0, 1.0, -0.1, betaAtom);
		ssip3 = Ssip.fromMepPoint(mep3);
		BetaPoint mep4 = new BetaPoint(0.0, 1.0, 0.0, -0.2, betaAtom);
		ssip4 = Ssip.fromMepPoint(mep4);
		ssipList.add(ssip1);
		ssipList.add(ssip2);
		ssipList.add(ssip3);
		ssipList.add(ssip4);
	}

	@Test
	public void testSSIPList() {

		SsipContainer sSIPList = new SsipContainer(ssipList);

		assertNotNull(sSIPList);
		assertEquals(4, sSIPList.getSSIPs().size());
		assertTrue(sSIPList.getSSIPs().containsAll(ssipList));

	}

	@Test
	public void testToString() {
		SsipContainer sSIPList = new SsipContainer(ssipList);

		String expectedString = "[SSIP Value: 0.001501032(1.0, 0.0, 0.0), SSIP Value: 7.502580000000001E-4(1.0, 1.0, 1.0), SSIP Value: -0.0(0.0, 1.0, 1.0), SSIP Value: -0.0(0.0, 1.0, 0.0)]";

		String actualString = sSIPList.toString();
		assertEquals(expectedString, actualString);
	}

	@Test
	public void testSortSSIPS() {
		ArrayList<Ssip> expectedSortedSSIPs = new ArrayList<>();
		expectedSortedSSIPs.add(ssip2);
		expectedSortedSSIPs.add(ssip1);
		expectedSortedSSIPs.add(ssip3);
		expectedSortedSSIPs.add(ssip4);
		SsipContainer sSIPList = new SsipContainer(ssipList);
		ArrayList<Ssip> actualSortedSSIPS = (ArrayList<Ssip>) sSIPList
				.getSSIPs();
		assertEquals(expectedSortedSSIPs, actualSortedSSIPS);
	}

}
