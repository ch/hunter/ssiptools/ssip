/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.cml.io;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class CmlWriterTest {

	private static final Logger LOG = LogManager.getLogger(CmlWriterTest.class);
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	static URI outputURI;
	static URI comparisonFileURI;
	private String stdinchikey;
	private String moleculeid;
	private ArrayList<Atom> atomlist;
	private HashMap<String, String> bondMap;
	private HashMap<Integer, HashMap<Integer, Integer>> adjacenyRepresentation;
	private CmlMolecule cmlMolecule;
	
	
	@Before
	public void setUp() throws Exception {
		File outfile = tempFolder.newFile("water.cml");
		outputURI = outfile.toURI();
		comparisonFileURI = CmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/core/XLYOFNOQVPJJNP-UHFFFAOYSA-N.cml").toURI();

		stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(-0.197781963448, -7.67944186155e-36, 0.346659831728), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(0.760536914211, -1.03517266068e-35, 0.204548524816), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-0.561754950762, 3.39295529441e-35, -0.551208356544), "a3"));
		bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		adjacenyRepresentation = new HashMap<>();
		HashMap<Integer, Integer> oxyMap = new HashMap<>();
		oxyMap.put(1, 1);
		oxyMap.put(2, 1);
		HashMap<Integer, Integer> h1Map = new HashMap<>();
		h1Map.put(0, 1);
		HashMap<Integer, Integer> h2Map = new HashMap<>();
		h2Map.put(0, 1);
		adjacenyRepresentation.put(0, oxyMap);
		adjacenyRepresentation.put(1, h1Map);
		adjacenyRepresentation.put(2, h2Map);
		
		cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
	}

	@Test
	public void testMarshall() throws IOException {
		try {
			CmlWriter.marshal(cmlMolecule, outputURI);
		} catch (JAXBException e) {
			LOG.error(e);
		}
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(outputURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(comparisonFileURI)));
			LOG.debug(actualFileContents);
			assertEquals(expectedFileContents, actualFileContents);
		} catch (IOException e) {
			LOG.debug(e);
		}
		
	}
	
	@Test
	public void testMarshalRoundTrip(){
		try {
			CmlMolecule cmlMolecule = CmlReader.demarshal(comparisonFileURI);
			CmlWriter.marshal(cmlMolecule, outputURI);
		} catch (JAXBException e) {
			LOG.error(e);
		}
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(outputURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(comparisonFileURI)));
			LOG.debug(actualFileContents);
			assertEquals(expectedFileContents, actualFileContents);
		} catch (IOException e) {
			LOG.debug(e);
		}
	}

}
