/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.surfaceinformation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

/**
 * Class for storing information on electron density isosurface.
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ElectronDensityIsoSurface {

	@XmlTransient
	static final double ERROR_IN_DENSITY = 1e-5;
	
	@XmlValue
	private double density;
	
	@XmlAttribute(name="unit", namespace="http://www-hunter.ch.cam.ac.uk/SSIP")
	private String unit;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;
	
	/**
	 * JAXB constructor
	 */
	public ElectronDensityIsoSurface() {
	}

	/**
	 * Electron density isosurface.
	 * 
	 * @param density electron density.
	 */
	public ElectronDensityIsoSurface(double density) {
		this.density = density;
		this.unit = "e bohr^-3";
	}
	
	public double getDensity() {
		return density;
	}

	public void setDensity(double density) {
		this.density = density;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(density);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			if (unit != null) {
				temp = unit.hashCode();
				result = prime * result + (int) (temp ^ (temp >>> 32));
			}
			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		ElectronDensityIsoSurface othereDensityIsoSurface = (ElectronDensityIsoSurface) obj;
		return Math.abs(this.density - othereDensityIsoSurface.getDensity()) < ERROR_IN_DENSITY && this.getUnit().equals(othereDensityIsoSurface.getUnit());
	}
	
	
	
	

}
