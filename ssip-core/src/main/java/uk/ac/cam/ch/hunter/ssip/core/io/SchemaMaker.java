/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.io;

import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import uk.ac.cam.ch.hunter.ssip.core.DefaultNamespacePrefixMapper;

/**
 * Class for generating XML Schema.
 * 
 * @author mdd31
 *
 */
public final class SchemaMaker {

	private static final Logger LOG = LogManager.getLogger(SchemaMaker.class);
	
	private static DefaultNamespacePrefixMapper defaultNamespacePrefixMapper = makeNamespaceMapper();
	
	private SchemaMaker(){

	}
	
	private static DefaultNamespacePrefixMapper makeNamespaceMapper() {
		try {
			return new DefaultNamespacePrefixMapper();
		} catch (JAXBException e) {
			LOG.error(e);
			return null;
		}
	}
	
	/**
	 * Generates XML Schema.
	 * 
	 * @param schemaURL URL of the schema.
	 * @return schema Schema
	 * @throws SAXException SAXException
	 */
	public static Schema createSchema(URL schemaURL) throws SAXException{
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		return schemaFactory.newSchema(schemaURL);
	}
	
	public static Schema createCMLSchema() {
		try {
			return createSchema(defaultNamespacePrefixMapper.getCmlSchemaLocationURL());
		} catch (SAXException e) {
			LOG.error(e);
			return null;
		}
	}
	
	public static Schema createSsipSchema() {
		try {
			return createSchema(defaultNamespacePrefixMapper.getSsipSchemaLocationURL());
		} catch (SAXException e) {
			LOG.error(e);
			return null;
		}
	}
	
	public static Schema createPhaseSchema() {
		try {
			return createSchema(defaultNamespacePrefixMapper.getPhaseSchemaLocationURL());
		} catch (SAXException e) {
			LOG.error(e);
			return null;
		}
	}
}
