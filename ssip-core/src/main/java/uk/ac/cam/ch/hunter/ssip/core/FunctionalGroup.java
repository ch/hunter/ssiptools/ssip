/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.C;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.H;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.N;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.O;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.P;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.R;
import static uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor.S;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.DOUBLE;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.SINGLE;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.TRIPLE;
import static uk.ac.cam.ch.hunter.ssip.core.BondDescriptor.WILDCARD;

import java.util.HashMap;
import java.util.Map;

import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;

/**
 * Represents a chemical functional group.
 * 
 * @author tn300
 * 
 */
public enum FunctionalGroup {

	CARBONATE() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v4;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v5;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v6;
		transient Graph<AtomDescriptor, BondDescriptor> carbonateStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> carbonateCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return carbonateStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return carbonateCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 6;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> carbonateStruct = new Graph<>();
			
			v1 = carbonateStruct.addVertex(O);
			v2 = carbonateStruct.addVertex(C);
			v3 = carbonateStruct.addVertex(O);
			v4 = carbonateStruct.addVertex(O);
			v5 = carbonateStruct.addVertex(C);
			v6 = carbonateStruct.addVertex(C);

			carbonateStruct.addEdge(v1, v2, DOUBLE);
			carbonateStruct.addEdge(v2, v3, SINGLE);
			carbonateStruct.addEdge(v2, v4, SINGLE);
			carbonateStruct.addEdge(v3, v5, SINGLE);
			carbonateStruct.addEdge(v4, v6, SINGLE);
			return carbonateStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> carbonateFactors = new HashMap<>();
			carbonateFactors.put(v1, 0.96);
			carbonateFactors.put(v2, 1.0);
			carbonateFactors.put(v3, 1.0);
			carbonateFactors.put(v4, 1.0);
			carbonateFactors.put(v5, 1.0);
			carbonateFactors.put(v6, 1.0);
			return carbonateFactors;
		}
	},

	ESTER() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v4;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v5;
		transient Graph<AtomDescriptor, BondDescriptor> esterCarbonylStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> esterCarbonylCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return esterCarbonylStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return esterCarbonylCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 5;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> esterCarbonylStruct = new Graph<>();
			v1 = esterCarbonylStruct.addVertex(O);
			v2 = esterCarbonylStruct.addVertex(C);
			v3 = esterCarbonylStruct.addVertex(C);
			v4 = esterCarbonylStruct.addVertex(O);
			v5 = esterCarbonylStruct.addVertex(C);

			esterCarbonylStruct.addEdge(v1, v2, DOUBLE);
			esterCarbonylStruct.addEdge(v2, v3, SINGLE);
			esterCarbonylStruct.addEdge(v2, v4, SINGLE);
			esterCarbonylStruct.addEdge(v4, v5, SINGLE);
			return esterCarbonylStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> esterCarbonylFactors = new HashMap<>();
			esterCarbonylFactors.put(v1, 0.96);
			esterCarbonylFactors.put(v2, 1.0);
			esterCarbonylFactors.put(v3, 1.0);
			esterCarbonylFactors.put(v4, 1.0);
			esterCarbonylFactors.put(v5, 1.0);
			return esterCarbonylFactors;
		}
	},

	CYANAMIDE() {
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor> cyanamideStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> cyanamideCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return cyanamideStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return cyanamideCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 3;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> cyanamideStruct = new Graph<>();
			
			v1 = cyanamideStruct.addVertex(N);
			v2 = cyanamideStruct.addVertex(C);
			v3 = cyanamideStruct.addVertex(N);

			cyanamideStruct.addEdge(v1, v2, SINGLE);
			cyanamideStruct.addEdge(v2, v3, TRIPLE);
			return cyanamideStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> cyanamideFactors = new HashMap<>();
			cyanamideFactors.put(v1, 1.41);
			cyanamideFactors.put(v2, 1.0);
			cyanamideFactors.put(v3, 0.77);
			return cyanamideFactors;
		}
	},

	NITRILE() {
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor> nitrileStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> nitrileCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return nitrileStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return nitrileCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 3;
		}

		@Override
		public int getNumberOfWildcards() {
			return 1;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> nitrileStruct = new Graph<>();
			
			v1 = nitrileStruct.addVertex(R);
			v2 = nitrileStruct.addVertex(C);
			v3 = nitrileStruct.addVertex(N);

			nitrileStruct.addEdge(v1, v2, SINGLE);
			nitrileStruct.addEdge(v2, v3, TRIPLE);
			return nitrileStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> nitrileFactors = new HashMap<>();
			nitrileFactors.put(v1, 1.0);
			nitrileFactors.put(v2, 1.0);
			nitrileFactors.put(v3, 0.77);
			return nitrileFactors;
		}
	},

	ALDEHYDE() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v4;
		transient Graph<AtomDescriptor, BondDescriptor> aldehydeCorbonylStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> aldehydeCarbonylCorrectionFactors = generateCorrectionFactors();

		

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return aldehydeCorbonylStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return aldehydeCarbonylCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 4;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> aldehydeCorbonylStruct = new Graph<>();
			
			v1 = aldehydeCorbonylStruct.addVertex(O);
			v2 = aldehydeCorbonylStruct.addVertex(C);
			v3 = aldehydeCorbonylStruct.addVertex(C);
			v4 = aldehydeCorbonylStruct.addVertex(H);

			aldehydeCorbonylStruct.addEdge(v1, v2, DOUBLE);
			aldehydeCorbonylStruct.addEdge(v2, v3, SINGLE);
			aldehydeCorbonylStruct.addEdge(v2, v4, SINGLE);
			return aldehydeCorbonylStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> aldehydeCarbonylFactors = new HashMap<>();
			aldehydeCarbonylFactors.put(v1, 0.89);
			aldehydeCarbonylFactors.put(v2, 1.0);
			aldehydeCarbonylFactors.put(v3, 1.0);
			aldehydeCarbonylFactors.put(v4, 1.0);
			return aldehydeCarbonylFactors;
		}
	},

	ALCOHOL() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor> alcoholStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> alcoholCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return alcoholStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return alcoholCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 3;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> alcoholStruct = new Graph<>();
			
			v1 = alcoholStruct.addVertex(O);
			v2 = alcoholStruct.addVertex(H);
			v3 = alcoholStruct.addVertex(C);

			alcoholStruct.addEdge(v1, v2, SINGLE);
			alcoholStruct.addEdge(v1, v3, SINGLE); 
			return alcoholStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> alcoholFactors = new HashMap<>();
			alcoholFactors.put(v1, 0.95);
			alcoholFactors.put(v2, 1.0);
			alcoholFactors.put(v3, 1.0);
			return alcoholFactors;
		}
	},

	PRIMARY_AMINE() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v4;
		transient Graph<AtomDescriptor, BondDescriptor> primaryAmineStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> primaryAmineCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return primaryAmineStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return primaryAmineCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 4;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> primaryAmineStruct = new Graph<>();

			v1 = primaryAmineStruct.addVertex(N);
			v2 = primaryAmineStruct.addVertex(H);
			v3 = primaryAmineStruct.addVertex(H);
			v4 = primaryAmineStruct.addVertex(C);

			primaryAmineStruct.addEdge(v1, v4, SINGLE);
			primaryAmineStruct.addEdge(v1, v3, SINGLE);
			primaryAmineStruct.addEdge(v1, v2, SINGLE);
			return primaryAmineStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> primaryAmineFactors = new HashMap<>();
			primaryAmineFactors.put(v1, 1.0);
			primaryAmineFactors.put(v2, 1.0);
			primaryAmineFactors.put(v3, 1.0);
			primaryAmineFactors.put(v4, 1.0);
			return primaryAmineFactors;
		}

	},

	SECONDARY_AMINE() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v4;
		transient Graph<AtomDescriptor, BondDescriptor> secondaryAmineStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> secondaryAmineCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return secondaryAmineStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return secondaryAmineCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 4;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> secondaryAmineStruct = new Graph<>();

			v1 = secondaryAmineStruct.addVertex(N);
			v2 = secondaryAmineStruct.addVertex(H);
			v3 = secondaryAmineStruct.addVertex(C);
			v4 = secondaryAmineStruct.addVertex(C);

			secondaryAmineStruct.addEdge(v1, v4, SINGLE);
			secondaryAmineStruct.addEdge(v1, v3, SINGLE);
			secondaryAmineStruct.addEdge(v1, v2, SINGLE);

			secondaryAmineCorrectionFactors = new HashMap<>();
			secondaryAmineCorrectionFactors.put(v1, 1.25);
			secondaryAmineCorrectionFactors.put(v2, 1.0);
			secondaryAmineCorrectionFactors.put(v3, 1.0);
			secondaryAmineCorrectionFactors.put(v4, 1.0);
			return secondaryAmineStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> secondaryAmineFactors = new HashMap<>();
			secondaryAmineFactors.put(v1, 1.25);
			secondaryAmineFactors.put(v2, 1.0);
			secondaryAmineFactors.put(v3, 1.0);
			secondaryAmineFactors.put(v4, 1.0);
			return secondaryAmineFactors;
		}
	},

	TERTIARY_AMINE() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v4;
		transient Graph<AtomDescriptor, BondDescriptor> tertiaryAmineStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> tertiaryAmineCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return tertiaryAmineStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return tertiaryAmineCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 4;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> tertiaryAmineStruct = new Graph<>();

			v1 = tertiaryAmineStruct.addVertex(N);
			v2 = tertiaryAmineStruct.addVertex(C);
			v3 = tertiaryAmineStruct.addVertex(C);
			v4 = tertiaryAmineStruct.addVertex(C);

			tertiaryAmineStruct.addEdge(v1, v4, SINGLE);
			tertiaryAmineStruct.addEdge(v1, v3, SINGLE);
			tertiaryAmineStruct.addEdge(v1, v2, SINGLE);
			return tertiaryAmineStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> tertiaryAmineFactors = new HashMap<>();
			tertiaryAmineFactors.put(v1, 1.41);
			tertiaryAmineFactors.put(v2, 1.0);
			tertiaryAmineFactors.put(v3, 1.0);
			tertiaryAmineFactors.put(v4, 1.0);
			return tertiaryAmineFactors;
		}
	},

	ANY_OXYGEN_ATOM_BONDED_TO_SULFUR() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor> sulfurStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> sulfurCorrectionFactors = generateCorrectionFactors();


		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return sulfurStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return sulfurCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 2;
		}

		@Override
		public int getNumberOfWildcards() {
			return 1;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> sulfurStruct = new Graph<>();
			v1 = sulfurStruct.addVertex(S);
			v2 = sulfurStruct.addVertex(O);

			sulfurStruct.addEdge(v1, v2, WILDCARD);
			return sulfurStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> sulfurFactors = new HashMap<>();
			sulfurFactors.put(v1, 1.0);
			sulfurFactors.put(v2, 0.99);
			return sulfurFactors;
		}
	},

	NITRO() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v4;
		transient Graph<AtomDescriptor, BondDescriptor> nitroStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> nitroCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return nitroStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return nitroCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 4;
		}

		@Override
		public int getNumberOfWildcards() {
			return 3;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> nitroStruct = new Graph<>();
			v1 = nitroStruct.addVertex(N);
			v2 = nitroStruct.addVertex(O);
			v3 = nitroStruct.addVertex(O);
			v4 = nitroStruct.addVertex(R); // wildcard

			nitroStruct.addEdge(v1, v2, WILDCARD);
			nitroStruct.addEdge(v1, v3, WILDCARD);
			nitroStruct.addEdge(v1, v4, SINGLE);
			return nitroStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> nitroFactors = new HashMap<>();
			nitroFactors.put(v1, 1.0);
			nitroFactors.put(v2, 0.8);
			nitroFactors.put(v3, 0.8);
			nitroFactors.put(v4, 1.0);
			return nitroFactors;
		}
	},
	
	PYRIDINE() {
		
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor> pyridineStructure= generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> pyridineCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return pyridineStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return pyridineCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 3;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> pyridineStruct = new Graph<>();

			v1 = pyridineStruct.addVertex(N);
			v2 = pyridineStruct.addVertex(R);
			v3 = pyridineStruct.addVertex(R);


			pyridineStruct.addEdge(v1, v2, SINGLE);
			pyridineStruct.addEdge(v1, v3, DOUBLE);

			return pyridineStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> pyridineFactors = new HashMap<>();
			pyridineFactors.put(v1, 1.08);
			pyridineFactors.put(v2, 1.0);
			pyridineFactors.put(v3, 1.0);
			return pyridineFactors;
		}
	},

	ANY_OXYGEN_ATOM_BONDED_TO_PHOSPHORUS() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor> phosphorusStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> phosphorusCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return phosphorusStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return phosphorusCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 2;
		}

		@Override
		public int getNumberOfWildcards() {
			return 1;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> phosphorusStruct = new Graph<>();
			
			v1 = phosphorusStruct.addVertex(P);
			v2 = phosphorusStruct.addVertex(O);

			phosphorusStruct.addEdge(v1, v2, WILDCARD);
			return phosphorusStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> phosphorusFactors = new HashMap<>();
			phosphorusFactors.put(v1, 1.0);
			phosphorusFactors.put(v2, 1.09);
			return phosphorusFactors;
		}
	},

	// TODO: ask if this is the correct structure
	ETHER() {

		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v1;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v2;
		transient Graph<AtomDescriptor, BondDescriptor>.Vertex v3;
		transient Graph<AtomDescriptor, BondDescriptor> etherStructure = generateChemicalStructure();
		transient Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> etherCorrectionFactors = generateCorrectionFactors();

		@Override
		public Graph<AtomDescriptor, BondDescriptor> getChemicalStructure() {
			return etherStructure;
		}

		@Override
		public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors() {
			return etherCorrectionFactors;
		}

		@Override
		public int getNumberOfAtom() {
			return 3;
		}

		@Override
		public int getNumberOfWildcards() {
			return 0;
		}

		@Override
		protected Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure() {
			Graph<AtomDescriptor, BondDescriptor> etherStruct = new Graph<>();
			
			v1 = etherStruct.addVertex(O);
			v2 = etherStruct.addVertex(C);
			v3 = etherStruct.addVertex(C);

			etherStruct.addEdge(v1, v2, SINGLE);
			etherStruct.addEdge(v1, v3, SINGLE);
			return etherStruct;
		}

		@Override
		protected Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors() {
			HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> etherFactors = new HashMap<>();
			etherFactors.put(v1, 1.16);
			etherFactors.put(v2, 1.0);
			etherFactors.put(v3, 1.0);
			return etherFactors;
		}
	};

	
	/**
	 * @return A graph structure representation of the group.
	 */
	public abstract Graph<AtomDescriptor, BondDescriptor> getChemicalStructure();

	/**
	 * @return A map between the correction factor and the vertex to which it
	 * is associated.
	 */
	public abstract Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCorrectionFactors();

	/**
	 * 
	 * @return The number of atoms present in the group.
	 */
	public abstract int getNumberOfAtom();

	/**
	 * @return The number of wild cards in the group. The number of wild cards
	 * is a sum of both the atom wild cards and the bond wild cards.
	 */
	public abstract int getNumberOfWildcards();
	
	/**
	 * Initialise transient variable containing chemical structure representation.
	 * 
	 * @return A graph structure representation of the group.
	 */
	protected abstract Graph<AtomDescriptor, BondDescriptor> generateChemicalStructure();
	
	/**
	 * Initialise map containing correction factors.
	 * 
	 * @return A map containing correction factors.
	 */
	protected abstract Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> generateCorrectionFactors();

}
