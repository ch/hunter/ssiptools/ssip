/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.cml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.AtomComparator;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BondDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.CentroidCalculator;
import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

/**
 * This class is a Java equivalent to a CML molecule. This acts as an
 * intermediary from which the molecule topology can be extracted and then used in calculation.
 *  It can also be generated from a molecule topology.
 * 
 * @author mdd31
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "molecule", namespace="http://www.xml-cml.org/schema")
public class CmlMolecule {

	@XmlTransient
	private static final Logger LOG = LogManager.getLogger(CmlMolecule.class);
	
	@XmlElementWrapper(name = "atomArray", namespace="http://www.xml-cml.org/schema")
	@XmlElement(name = "atom", namespace = "http://www.xml-cml.org/schema")
	private ArrayList<Atom> atomArrayList;
	
	@XmlAttribute(name = "stdInChIKey", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private String stdinchikey;
	
	@XmlAttribute(name = "id", namespace = "http://www.xml-cml.org/schema")
	private String moleculeid;
	
	/**
	 * This is a simple map- where the key is the atomRefs2 attribute, and the value is the bond order.
	 * 
	 * The atomRefs2 element contains the atom ids for the atoms involved in the bond, separated by a space.
	 * 
	 * For writing out the molecule this is needed in this form. 
	 */
	@XmlJavaTypeAdapter(BondMapAdapter.class)
	@XmlElement(name = "bondArray", namespace = "http://www.xml-cml.org/schema")
	private Map<String, String> bondMap;
	
	/**
	 * This represents the molecules as an adjacency map.
	 * 
	 * A map between vertex index, which serves as a key, and adjacent vertex
	 * indices. Each adjacent vertex index is a key in the inner map, the
	 * corresponding value in the inner map is the order of the bond in the
	 * corresponding bond element in the CML. This provides information
	 * necessary to infer the type of bond that is made between the adjacent
	 * index and the key index in the outer map.
	 * <p>
	 * An example of this can be found <a
	 * href="https://en.wikipedia.org/wiki/Adjacency_list">here</a>, but here,
	 * there is an extra piece of information pertaining to the bond order.
	 * 
	 */
	@XmlTransient
	private Map<Integer, HashMap<Integer, String>> adjacencyRepresentation;
	
	@XmlTransient
	private Graph<AtomDescriptor, BondDescriptor> molStructure;

	@XmlTransient
	private Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> molPositions;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;
	
	/**
	 * Empty constructor generates new CmlMolecule with no values initialised.
	 */
	public CmlMolecule() {

	}
	
	/**
	 * Constructor for representing a CML Molecule.
	 * 
	 * @param stdinchikey Standard InChIKey for the molecule.
	 * @param moleculeid Molecule ID.
	 * @param atomList List of atoms in the molecule.
	 * @param bondMap Bond Map of atoms in the molecule.
	 */
	public CmlMolecule(String stdinchikey, String moleculeid, List<Atom> atomList, Map<String, String> bondMap) {
		setStdinchikey(stdinchikey);
		setMoleculeid(moleculeid);
		Collections.sort(atomList, new AtomComparator());
		setAtomArrayList(atomList);
		setBondMap(bondMap);
		initialiseData();
	}
	
	public Graph<AtomDescriptor, BondDescriptor> getMolTopology() {
		return molStructure;
	}

	public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> getMolPositions() {
		return molPositions;
	}
	
	/**
	 * This returns the list of AtomIDs for the atoms in the molecule.
	 * 
	 * @return atom AtomIDs.
	 */
	public List<String> getAtomIdList(){
		List<String> atomIDList = new ArrayList<>(getAtomArrayList().size());
		for (int i = 0; i < getAtomArrayList().size(); i++) {
			atomIDList.add(getAtomArrayList().get(i).getAtomId());
		}
		return atomIDList;
	}

	public List<AtomDescriptor> getAtomDescriptors(){
		List<AtomDescriptor> atomDescriptors = new ArrayList<>();
		for (int i = 0; i < getAtomArrayList().size(); i++) {
			atomDescriptors.add(getAtomArrayList().get(i).getSymbol());
		}
		return atomDescriptors;
	}
	
	/**
	 *This is used during demarshaling to set up the adjacency representation and the Molecule structure and positions.  
	 * 
	 */
	public void initialiseData(){
		ArrayList<Atom> atomList = (ArrayList<Atom>) getAtomArrayList();
		Collections.sort(atomList, new AtomComparator());
		setAtomArrayList(atomList);
		Map<Integer, HashMap<Integer, String>> adjRep;
		try {
			LOG.trace("bond map: {}", getBondMap());
			adjRep = createAdjacencyRepresentationFromBondMap();
			LOG.trace("adjacency rep from bond map: {}", adjRep);
			setAdjacencyRepresentation(adjRep);
		} catch (IllegalAccessException e) {
			LOG.warn(e);
		}
		molPositions = new HashMap<>();
		molStructure = new Graph<>();
		initMolData();
	}
	/**
	 * Initialises 'molStructure' and 'molPositions'.
	 */
	private void initMolData() {

		
		List<AtomDescriptor> atomDescriptors = getAtomDescriptors();
		// Populates the graph with vertices.
		Graph<AtomDescriptor, BondDescriptor>.Vertex v;
		for (int i = 0; i < atomDescriptors.size(); i++) {
			LOG.trace("Vertex");
			v = molStructure.addVertex(atomDescriptors.get(i));
			if (LOG.isTraceEnabled()) {
				LOG.trace(v.toString());
			}
			molPositions.put(v, getAtomArrayList().get(i).getPosition());
		}

		// Establishes bonds between vertices.
		List<Graph<AtomDescriptor, BondDescriptor>.Vertex> vertices = molStructure
				.getVertices();
		Map<Integer, String> adjacentVertexToBondMap;
		LOG.debug("adjacency representation: {}", adjacencyRepresentation);
		for (int i = 0; i < atomDescriptors.size(); i++) {
			
			adjacentVertexToBondMap = adjacencyRepresentation.get(i);
			LOG.debug("adjVertex to bond Map: {}", adjacentVertexToBondMap);
			for (Entry<Integer, String> entry : adjacentVertexToBondMap
					.entrySet()) {
				int j = entry.getKey();
				molStructure.addEdge(vertices.get(i), vertices.get(j),
						bondMapper(adjacentVertexToBondMap.get(j)));
			}
		}
	}
	
	/**
	 * This generates a bond map from the adjacency representation of the molecule.
	 * 
	 * @return bond Bond map.
	 * @throws IllegalAccessException IllegalAccessException.
	 */
	public Map<String, String> createBondMapFromAdjacencyRepresentation() throws IllegalAccessException{
		Map<String, String> bMap = new HashMap<>();
		if (getAdjacencyRepresentation() == null) {
			throw new IllegalAccessException("null adjaceny representation");
		}
		for (Map.Entry<Integer, HashMap<Integer, String>> adRepEntry : getAdjacencyRepresentation().entrySet()) {
			int atomIdOneIndex = adRepEntry.getKey();
			for (Map.Entry<Integer, String> atomAdRepEntry : adRepEntry.getValue().entrySet()) {
				int atomIdTwoIndex = atomAdRepEntry.getKey();
				String order = atomAdRepEntry.getValue();
				String[] atomRefsStrings = createAtomRefsFromIndices(atomIdOneIndex, atomIdTwoIndex);
				if (!bMap.containsKey(atomRefsStrings[0]) && !bMap.containsKey(atomRefsStrings[1])) {
					bMap.put(atomRefsStrings[0], order);
				}
			}
		}
		return bMap;
	}
	
	String[] createAtomRefsFromIndices(int atomIdOneIndex, int atomIdTwoIndex){
		String atomIdOne = getAtomIdList().get(atomIdOneIndex);
		String atomIdTwo = getAtomIdList().get(atomIdTwoIndex);
		String[] atomRefsStrings = new String[2];
		atomRefsStrings[0] = atomIdOne + " " + atomIdTwo;
		atomRefsStrings[1] = atomIdTwo + " " + atomIdOne;
		return atomRefsStrings;
	}
	
	Map<Integer, HashMap<Integer, String>> createAdjacencyRepresentationFromBondMap() throws IllegalAccessException{
		Map<Integer, HashMap<Integer, String>> adjRep = new HashMap<>();
		if (getBondMap() == null) {
			throw new IllegalAccessException("null bond map");
		} else {
			ArrayList<String> atomIdList = (ArrayList<String>) getAtomIdList();
			LOG.debug("Atom ID Array List: {}", atomIdList);
			for (Map.Entry<String, String> bond : getBondMap().entrySet()) {
				String[] atomsInBond = parseAtomIDs(bond.getKey());
				int atomIdOneIndex = atomIdList.indexOf(atomsInBond[0]);
				int atomIdTwoIndex = atomIdList.indexOf(atomsInBond[1]);
				LOG.debug("Atoms in bond: {}, {}", atomsInBond[0], atomsInBond[1]);
				LOG.debug("atomIDOne index: {} atomIDTwo index: {}", atomIdOneIndex, atomIdTwoIndex);
				updateAdjacencyRepresentation(adjRep, atomIdOneIndex, atomIdTwoIndex, bond.getValue());
			}
		}
		return adjRep;
	}
	
	String[] parseAtomIDs(String bondAtomRefs){
		return bondAtomRefs.split("\\s+");
	}
	
	/**
	 * This updates the adjacency representation for both atoms.
	 * 
	 * @param atomIDOneIndex Index of atom 1.
	 * @param atomIDTwoIndex Index of atom 2.
	 * @param bondOrder Bond Order.
	 */
	void updateAdjacencyRepresentation(Map<Integer, HashMap<Integer, String>> adjacencyRepresentation, int atomIDOneIndex, int atomIDTwoIndex, String bondOrder){
		//do first atom index first.
		updateAdjacencyRepresentationFirstAtom(adjacencyRepresentation, atomIDOneIndex, atomIDTwoIndex, bondOrder);
		//do second atom index next.
		updateAdjacencyRepresentationFirstAtom(adjacencyRepresentation, atomIDTwoIndex, atomIDOneIndex, bondOrder);
	}

	/**
	 * 
	 * This updates the Adjacency representation, adding the information to the entry for atomIDOneIndex for the connection to atomIDTwoIndex, with bond order of bondOrder.
	 * 
	 * @param atomIDOneIndex Index of atom 1.
	 * @param atomIDTwoIndex Index of atom 2.
	 * @param bondOrder Bond Order.
	 */
	void updateAdjacencyRepresentationFirstAtom(Map<Integer, HashMap<Integer, String>> adjacencyRepresentation, int atomIDOneIndex, int atomIDTwoIndex, String bondOrder){
		// Is this atom already in our list?
				if (adjacencyRepresentation.containsKey(atomIDOneIndex)) {
					LOG.debug("Existing atom {} found in adjacencyRepresentation",
							atomIDOneIndex);
	
					// OK, it is, lets pull out the map of what it is bonded to
					Map<Integer, String> existingAdjacentIndicesToBondTypeMap = adjacencyRepresentation
							.get(atomIDOneIndex);
	
					LOG.debug("\tatom {}'s existingAdjacentIndicesToBondTypeMap: {}",
							atomIDOneIndex, existingAdjacentIndicesToBondTypeMap);
	
					existingAdjacentIndicesToBondTypeMap.put(atomIDTwoIndex,
								bondOrder);
					
				} else {
					LOG.debug("We have a new index atom {}", atomIDOneIndex);
					
					HashMap<Integer, String> adjacentIndicesToBondTypeMap = new HashMap<>();
					adjacentIndicesToBondTypeMap.put(atomIDTwoIndex, bondOrder);
					LOG.debug(
							"Adding the following adjacentIndicesToBondTypeMap {} to adjacencyReperesentation key {}",
							adjacentIndicesToBondTypeMap, atomIDOneIndex);
					adjacencyRepresentation.put(atomIDOneIndex,
							adjacentIndicesToBondTypeMap);
				}
	}
	
	static BondDescriptor bondMapper(String bondType) {
		BondDescriptor bond;
		switch (bondType) {
		case "1":
		case "S":
			bond = BondDescriptor.SINGLE;
			break;
		case "2":
		case "D":
			bond = BondDescriptor.DOUBLE;
			break;
		case "A":
			bond = BondDescriptor.AROMATIC;
			break;
		case "3":
		case "T":
			bond = BondDescriptor.TRIPLE;
			break;
		default:
			throw new IllegalArgumentException(
					"Method input must be '1', 'S', '2', 'D', 'A', 'T' or '3'.");
		}

		return bond;
	}

	public Point3D getMoleculeCentroid(){
		CentroidCalculator centroidCalculator = new CentroidCalculator();
		return centroidCalculator.calculateCentroid(getAtomArrayList());
	}
	
	public List<Atom> getAtomArrayList() {
		return atomArrayList;
	}

	public void setAtomArrayList(List<Atom> atomArrayList) {
		this.atomArrayList = (ArrayList<Atom>) atomArrayList;
	}

	public String getStdinchikey() {
		return stdinchikey;
	}

	public void setStdinchikey(String stdinchikey) {
		this.stdinchikey = stdinchikey;
	}

	public String getMoleculeid() {
		return moleculeid;
	}

	public void setMoleculeid(String moleculeid) {
		this.moleculeid = moleculeid;
	}

	public Map<String, String> getBondMap() {
		return bondMap;
	}

	public void setBondMap(Map<String, String> bondMap) {
		this.bondMap = bondMap;
	}

	public Map<Integer, HashMap<Integer, String>> getAdjacencyRepresentation() {
		return adjacencyRepresentation;
	}

	public void setAdjacencyRepresentation(
			Map<Integer, HashMap<Integer, String>> adjacencyRepresentation) {
		this.adjacencyRepresentation = adjacencyRepresentation;
	}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CmlMolecule other = (CmlMolecule) obj;
		
		return this.getAtomArrayList().equals(other.getAtomArrayList()) && this.getBondMap().equals(other.getBondMap());
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = 1;
			long temp;
			temp = atomArrayList.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			if (moleculeid != null) {
				temp = moleculeid.hashCode();
				result = prime * result + (int) (temp ^ (temp >>> 32));
			}
			if (stdinchikey != null) {
				temp = stdinchikey.hashCode();
				result = prime * result + (int) (temp ^ (temp >>> 32));
			}
			// TODO include graphs in hash.
			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
	
}
