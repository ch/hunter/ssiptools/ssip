/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.graph;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Sets;


public class VF2<A extends LabelComparator<A>, B extends LabelComparator<B>> {

	private static final Logger LOG = LogManager.getLogger(VF2.class);

	/**
	 * TODO:javadoc
	 */
	private boolean foundIsomorphisms;

	/**
	 * A subgraph of <code>parentGraph</code>.
	 * 
	 * Intuition: This can represent the structure of a functional group.
	 */
	private final Graph<A, B> subgraph;

	/**
	 * A graph which contains <code>subgraph</code>.
	 * 
	 * Intuition: This often represents the chemical structure of a molecule.
	 */
	private final Graph<A, B> parentGraph;

	/**
	 * Maps a <code>subgraph</code> vertex to a set set of
	 * <code>parentGraph</code> vertices with the same label
	 */
	final LinkedHashMap<Graph<A, B>.Vertex, HashSet<Graph<A, B>.Vertex>> compatibilityMap;

	/**
	 * Maps each <code>subgraph</code> vertex (LHS) to corresponding
	 * <code>parentGraph</parentGraph> vertex (RHS). The structure represents
	 * the mathematical idea of an isomorphism between two objects.
	 */
	private BiMap<Graph<A, B>.Vertex, Graph<A, B>.Vertex> isomorphism;

	/**
	 * List of isomorphisms representing the different ways
	 * <code>subgraph</code> is embedded in <code>parentGraph</code>.
	 */
	private HashSet<BiMap<Graph<A, B>.Vertex, Graph<A, B>.Vertex>> isomorphisms;

	/**
	 * Contains all <code>subgraph</code> vertices that are already matched.
	 */
	private final HashSet<Graph<A, B>.Vertex> matchedSubgraphVertices;

	/**
	 * Contains all <code>subgraph</code> vertices adjacent to a set of matched
	 * vertices that are not yet matched.
	 */
	private final HashSet<Graph<A, B>.Vertex> nonMatchedSetAdjacentSubgraphVertices;

	/**
	 * Contains all <code>parentGraph</code> vertices that are matched.
	 */
	private final HashSet<Graph<A, B>.Vertex> matchedParentGraphVertices;

	/**
	 * Contains all <code>parentGraph</code> vertices adjacent to a set of
	 * matched vertices that are not yet matched.
	 */
	private final HashSet<Graph<A, B>.Vertex> nonMatchedSetAdjacentParentGraphVertices;

	/**
	 * An implementation of VF2 algorithm for graph ismorphism testing.
	 * 
	 * @param subgraph
	 *            Subgraph to search for.
	 * @param parentGraph
	 *            Parent graph to search against.
	 * 
	 * @see <a href="http://dx.doi.org/10.1109/TPAMI.2004.75">A (sub)graph isomorphism algorithm for matching large graphs</a>
	 * 
	 */
	public VF2(Graph<A, B> subgraph, Graph<A, B> parentGraph) {
		if (subgraph == null || parentGraph == null)
			throw new IllegalArgumentException(
					"Null arguments are not allowed!");

		this.subgraph = subgraph;
		this.parentGraph = parentGraph;
		isomorphism = HashBiMap.create();

		compatibilityMap = new LinkedHashMap<>();
		initializeCompatibilityMap();

		isomorphisms = new HashSet<>();
		foundIsomorphisms = false;

		matchedSubgraphVertices = new HashSet<>();
		nonMatchedSetAdjacentSubgraphVertices = new HashSet<>();

		nonMatchedSetAdjacentParentGraphVertices = new HashSet<>();
		matchedParentGraphVertices = new HashSet<>();
	}

	/**
	 * Finds all vertex indices belonging to <code>parentGraph</code> with
	 * identical labels to the label of the specified <code>subgraph</code>
	 * vertex index.
	 */
	private void initializeCompatibilityMap() {

		// Defines a set of 'parentGraph' vertices compatible to a given
		// 'subgraph' vertex
		HashSet<Graph<A, B>.Vertex> compatiblePVertices;

		// Iterates over all 'subgraph' vertices
		for (Graph<A, B>.Vertex sVertex : subgraph.getVertices()) {
			compatiblePVertices = new HashSet<>();

			// Iterates over all 'parentGraph' vertices
			for (Graph<A, B>.Vertex pVertex : parentGraph.getVertices()) {

				// Checks if the 'subgraph' vertex label matches the label of
				// the 'parentGraph' vertex and if so adds it to the
				// compatibility set associated to that vertex.
				if (parentGraph.getVertexLabel(pVertex).consideredEqual(
						subgraph.getVertexLabel(sVertex))) {
					compatiblePVertices.add(pVertex);
				}
			}
			compatibilityMap.put(sVertex, compatiblePVertices);
		}
	}

	/**
	 * Finds pairs of <code>subgraph</code> and <code>parentGraph</code> vertex
	 * indices to construct the isomorphism.
	 * 
	 * @return
	 */
	private void subgraphSearch() {

		if (isomorphism.size() == subgraph.getNumberOfVertices()) {
			// When all vertices are mapped, the isomorphism is stored in
			// isomorphisms and the recursive tree spawned by subgraphSearch()
			// reaches the bottom.
			isomorphisms.add(HashBiMap.create(isomorphism));
		} else {
			// Retrieves the next non matched subgraph vertex
			Graph<A, B>.Vertex sVertex = nextSubgraphVertex();
			LOG.debug("subgraphVertexIndex : {}", sVertex);

			// Refines the number of compatible parent vertices to the given
			// subgraph index
			Set<Graph<A, B>.Vertex> refinedCompatibilityMap = refineCompatibility(sVertex);
			LOG.debug("refinedCompatibilityMap : {}", refinedCompatibilityMap);

			// Iterates over the refined set of parent vertices starting from
			// the one with lowest index
			for (Graph<A, B>.Vertex pVertex : refinedCompatibilityMap) {

				if (isJoinable(sVertex, pVertex)) {

					// New points to explore
					HashSet<Graph<A, B>.Vertex> newNonMatchedSetAdjacentSubgraphVertices = new HashSet<>(
							Sets.difference(Sets.difference(
									subgraph.findAdjacentVertices(sVertex),
									matchedSubgraphVertices),
									nonMatchedSetAdjacentSubgraphVertices));

					HashSet<Graph<A, B>.Vertex> newNonMatchedSetAdjacentParentGraphVertices = new HashSet<>(
							Sets.difference(Sets.difference(
									parentGraph.findAdjacentVertices(pVertex),
									matchedParentGraphVertices),
									nonMatchedSetAdjacentParentGraphVertices));

					// Updates isomorphism with the newly matched pair of
					// vertices
					updateState(sVertex, pVertex,
							newNonMatchedSetAdjacentSubgraphVertices,
							newNonMatchedSetAdjacentParentGraphVertices);

					// Checks the remaining graph to find an embedding.
					subgraphSearch();

					// Restores isomorphism to a state before executing
					// subgraphSearch() by removing the matched pair of
					// vertices. This ensures that all possible isomorphism are
					// found and the algorithm proceeds regardless of whether a
					// subgraphSearch() on the line before was successful in
					// finding an isomorphism.
					restoreState(sVertex, pVertex,
							newNonMatchedSetAdjacentSubgraphVertices,
							newNonMatchedSetAdjacentParentGraphVertices);
				}
			}
		}
	}

	/**
	 * Reduces the number of possible candidate <code>parentGraph</code> vertex
	 * indices which correspond to the specified <code>subgraph</code> vertex
	 * index.
	 * 
	 * @param subgraphVertexIndex
	 * @return
	 */
	private Set<Graph<A, B>.Vertex> refineCompatibility(
			Graph<A, B>.Vertex sVertex) {

		// Executes at start of algorithm
		if (isomorphism.isEmpty())
			return compatibilityMap.get(sVertex);

		Set<Graph<A, B>.Vertex> connectedCandidateVertices = new HashSet<>(
				Sets.intersection(nonMatchedSetAdjacentParentGraphVertices,
						Sets.difference(compatibilityMap.get(sVertex),
								matchedParentGraphVertices)));

		// Represents the number of non-matched adjacent subgraph vertices
		// to 'sVertex' that are also adjacent to an already matched
		// vertex index.
		int numberOfNonMatchedLocalSubgraphVertices = 0;

		// Represents the number of non-matched adjacent subgraph vertices
		// to 'sVertex' that are NOT adjacent to an already matched
		// vertex.
		int numberOfNonMatchedRemoteSubgraphVertices = 0;

		// The number of non-matched adjacent 'parentGraph' vertices
		// that are also adjacent to an already matched vertex.
		int numberOfNonMatchedLocalParentGraphVertices = 0;

		// The number of non-matched adjacent 'parentGraph' vertices
		// that are NOT adjacent to an already matched vertex.
		int numberOfNonMatchedRemoteParentGraphVertices = 0;

		for (Graph<A, B>.Vertex v : subgraph.findAdjacentVertices(sVertex)) {
			if (nonMatchedSetAdjacentSubgraphVertices.contains(v)) {
				numberOfNonMatchedLocalSubgraphVertices++;
			} else if (!matchedSubgraphVertices.contains(v)) {
				numberOfNonMatchedRemoteSubgraphVertices++;
			}
		}

		for (Iterator<Graph<A, B>.Vertex> iterator = connectedCandidateVertices
				.iterator(); iterator.hasNext();) {
			for (Graph<A, B>.Vertex apVertex : parentGraph
					.findAdjacentVertices(iterator.next())) {
				if (nonMatchedSetAdjacentParentGraphVertices.contains(apVertex)) {
					numberOfNonMatchedLocalParentGraphVertices++;
				} else if (!matchedParentGraphVertices.contains(apVertex)) {
					numberOfNonMatchedRemoteParentGraphVertices++;
				}
			}

			// This condition must be satisfied in order to ensure that
			// "parentGraph" has enough adjacent vertices to accommodate all
			// the adjacent "subgraph" vertices. The below condition is a
			// more complicated version of the observation that a subgraph
			// vertex having 'x' adjacent vertices can't be mapped to a
			// "parentGraph" vertex having less number of vertices 'y'.
			if (numberOfNonMatchedLocalParentGraphVertices < numberOfNonMatchedLocalSubgraphVertices
					|| numberOfNonMatchedRemoteParentGraphVertices < numberOfNonMatchedRemoteSubgraphVertices) {
				iterator.remove();
			}
		}

		return connectedCandidateVertices;
	}

	/**
	 * Determines whether a bond exists between <code>parentGraph</code> vertex
	 * such that it matches a bond between <code>subgraph</code> vertex.
	 * 
	 * @param subgraphVertexIndex
	 * @param parentGraphVertexIndex
	 * @return
	 */
	private boolean isJoinable(Graph<A, B>.Vertex sVertex,
			Graph<A, B>.Vertex pVertex) {

		B parentGraphEdgeLabel = null;
		B subgraphEdgeLabel = null;
		A adjacentParentGraphLabel = null;
		A adjacentSubgraphLabel = null;

		// Executes at start of algorithm.
		if (isomorphism.isEmpty()) {

			// Selects an arbitrary adjacent vertex to 'sVertex'.
			Graph<A, B>.Vertex asVertex = subgraph
					.findAdjacentVertices(sVertex).iterator().next();

			// Checks if there is a compatible edge in 'parentGraph' at
			// 'pVertex'.
			Set<Graph<A, B>.Vertex> adjacentParentGraphVertices = parentGraph
					.findAdjacentVertices(pVertex);
			adjacentSubgraphLabel = subgraph.getVertexLabel(asVertex);
			subgraphEdgeLabel = subgraph.getEdgeLabel(asVertex, sVertex);
			for (Graph<A, B>.Vertex apVertex : adjacentParentGraphVertices) {
				parentGraphEdgeLabel = parentGraph.getEdgeLabel(apVertex,
						pVertex);
				adjacentParentGraphLabel = parentGraph.getVertexLabel(apVertex);

				// Checks if labels and edges are compatible
				if (adjacentSubgraphLabel
						.consideredEqual(adjacentParentGraphLabel)
						&& parentGraphEdgeLabel
								.consideredEqual(subgraphEdgeLabel)) {
					return true;
				}
			}
			return false;
		}

		Set<Graph<A, B>.Vertex> matchedAdjacentVertices = Sets
				.intersection(subgraph.findAdjacentVertices(sVertex),
						matchedSubgraphVertices);
		if (matchedAdjacentVertices.isEmpty())
			return false;

		// Iterates over all vertices adjacent to "sVertex" which are already
		// matched to determine if corresponding edges exist in "pVertex".
		for (Graph<A, B>.Vertex asVertex : matchedAdjacentVertices) {
			// If vertex is matched, retrieves corresponding parent vertex
			// and finds if there is an edge and 'cpVertex'
			Graph<A, B>.Vertex ipVertex = isomorphism.get(asVertex);

			if (parentGraph.hasEdge(ipVertex, pVertex)) {
				// Retrieves labels attached to the edges
				parentGraphEdgeLabel = parentGraph.getEdgeLabel(ipVertex,
						pVertex);
				subgraphEdgeLabel = subgraph.getEdgeLabel(asVertex, sVertex);

				// if edge labels do not match returns
				if (!parentGraphEdgeLabel.consideredEqual(subgraphEdgeLabel))
					return false;
			} else {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns the next <code>subgraph</code> vertex to be matched to a
	 * <code>parentGraph</code> vertex.
	 * 
	 * If no vertices are matched yet, it returns a <code>subgraph</code> vertex
	 * starting the algorithm. Afterwards the non matched vertex that is
	 * adjacent to an already matched one is returned.
	 * 
	 * Note: This is not optimal, future implementation might incorporate a
	 * different strategy.
	 * 
	 * @return
	 */
	private Graph<A, B>.Vertex nextSubgraphVertex() {
		if (isomorphism.isEmpty()) {
			// executes at start of algorithm
			return subgraph.getVertices().iterator().next();
		} else {
			// returns a non matched subgraph vertex that is adjacent to an
			// already matched vertex
			return nonMatchedSetAdjacentSubgraphVertices.iterator().next();
		}
	}

	/**
	 * Updates all 'parentGraph' structures necessary for correct execution of
	 * the algorithm. For example this might include an update of
	 * <code>isomorphism</code> to include a new mapping between
	 * <code>subgraph</code> and <code>parentGraph</code> vertices.
	 * 
	 * @param sVertex
	 * @param pVertex
	 */
	private void updateState(Graph<A, B>.Vertex sVertex,
			Graph<A, B>.Vertex pVertex,
			Set<Graph<A, B>.Vertex> newNonMatchedSetAdjacentSubgraphVertices,
			Set<Graph<A, B>.Vertex> newNonMatchedSetAdjacentParentGraphVertices) {

		// includes both VertexIndices in the isomorphism, modifies
		// matchedParentGraphVertexIndices and matchedSubgraphVertexIndices in
		// the process
		LOG.debug("subgraphIndex: {}  candidateParentGraphIndex: {}", sVertex,
				pVertex);

		isomorphism.put(sVertex, pVertex);

		matchedSubgraphVertices.add(sVertex);
		matchedParentGraphVertices.add(pVertex);

		nonMatchedSetAdjacentSubgraphVertices.remove(sVertex);
		nonMatchedSetAdjacentParentGraphVertices.remove(pVertex);

		nonMatchedSetAdjacentSubgraphVertices
				.addAll(newNonMatchedSetAdjacentSubgraphVertices);
		nonMatchedSetAdjacentParentGraphVertices
				.addAll(newNonMatchedSetAdjacentParentGraphVertices);
	}

	/**
	 * Restores the state of all parentGraph structures before the execution of
	 * <code>subStructureSearch</code> in {@link #findIsomorphicSubgraphs()}.
	 * The method ensures that all possible isomorphisms will be found.
	 * 
	 */
	private void restoreState(Graph<A, B>.Vertex sVertex,
			Graph<A, B>.Vertex pVertex,
			Set<Graph<A, B>.Vertex> newNonMatchedSetAdjacentSubgraphVertices,
			Set<Graph<A, B>.Vertex> newNonMatchedSetAdjacentParentGraphVertices) {
		isomorphism.remove(sVertex);

		matchedSubgraphVertices.remove(sVertex);
		matchedParentGraphVertices.remove(pVertex);

		nonMatchedSetAdjacentSubgraphVertices.add(sVertex);

		// If there are no points left in the isomorphism,
		// then 'pVertex' was the first point to be mapped by
		// the algorithm and may not be in the set of adjacent points
		// to the next 'pVertex' selected.
		if (!isomorphism.isEmpty())
			nonMatchedSetAdjacentParentGraphVertices.add(pVertex);

		nonMatchedSetAdjacentSubgraphVertices
				.removeAll(newNonMatchedSetAdjacentSubgraphVertices);
		nonMatchedSetAdjacentParentGraphVertices
				.removeAll(newNonMatchedSetAdjacentParentGraphVertices);
	}

	/**
	 * Finds all the isomorphisms of a given <code>subgraph</code> in
	 * <code>this</code>
	 * 
	 * @return A set of maps between vertices of <code>this</code> and
	 *         <code>subgraph</code> vertices. The maps define all isomorphisms
	 *         of <code>subgraph</code> in <code>this</code>. By convention
	 *         vertices belonging to <code>subgraph</code> are on LHS!
	 */
	public Set<BiMap<Graph<A, B>.Vertex, Graph<A, B>.Vertex>> findIsomorphisms() {

		if (!foundIsomorphisms) {
			// Checks if compatible parent indices were found for a particular
			// subgraph index to ensure that it is possible to find an embedding
			// of the subgraph in the parent graph.
			for (Graph<A, B>.Vertex sVertex : subgraph.getVertices()) {
				if (compatibilityMap.get(sVertex).isEmpty())
					return isomorphisms;
			}

			// performs the actual calculations and populates the
			// 'isomorphisms' map
			subgraphSearch();
			foundIsomorphisms = true;
		}
		LOG.debug("isomorphisms: {}", isomorphisms);
		return isomorphisms;
	}

	LinkedHashMap<Graph<A, B>.Vertex, HashSet<Graph<A, B>.Vertex>> getCompatibilityMap() {
		return compatibilityMap;
	}
}