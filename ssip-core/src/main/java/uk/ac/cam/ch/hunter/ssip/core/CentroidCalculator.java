/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.Collection;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

/**
 * Class for calculating the Point3D centroid of a collection of Point3D positions.  
 * 
 * @author mdd31
 *
 */
public class CentroidCalculator {
	
	/**
	 * Constructor
	 */
	public CentroidCalculator(){
		/**
		 * Initialise Calculator.
		 */
	}
	/**
	 * method returns the centroid for a collection of point3D.
	 * 
	 * @param point3dCollection Collection of point3D points.
	 * @return centroid
	 */
	public Point3D calculateCentroid(Collection<? extends Point3D> point3dCollection){
		Point3D point3DSum = new Point3D();
		int numberOfPoints = point3dCollection.size();
		for (Point3D point3d : point3dCollection) {
			point3DSum = Point3D.add(point3DSum, point3d);
		}
		return new Point3D(point3DSum.getX()/numberOfPoints, point3DSum.getY()/numberOfPoints, point3DSum.getZ()/numberOfPoints);
	}

	/**
	 * method returns the centroid for a collection of SSIPs.
	 * 
	 * @param ssipCollection Collection of SSIPs.
	 * @return centroid 
	 */
	public Point3D calculateSSIPCentroid(Collection<? extends Ssip> ssipCollection){
		Point3D point3DSum = new Point3D();
		int numberOfPoints = ssipCollection.size();
		for (Ssip ssip : ssipCollection) {
			point3DSum = Point3D.add(point3DSum, ssip);
		}
		return new Point3D(point3DSum.getX()/numberOfPoints, point3DSum.getY()/numberOfPoints, point3DSum.getZ()/numberOfPoints);
	}
}
