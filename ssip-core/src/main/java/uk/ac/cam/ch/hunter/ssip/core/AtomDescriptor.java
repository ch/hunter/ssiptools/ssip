/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import uk.ac.cam.ch.hunter.ssip.core.graph.LabelComparator;

public enum AtomDescriptor implements LabelComparator<AtomDescriptor> {

	/**
	 * Represents any element of the periodic table
	 */
	R(0, "R"),

	/**
	 * Elements from the periodic table.
	 */
	H(1, "H"), He(2, "He"), Li(3, "Li"), Be(4, "Be"), B(5, "B"), C(6, "C"), N(
			7, "N"), O(8, "O"), F(9, "F"), Ne(10, "Ne"), Na(11, "Na"), Mg(12,
			"Mg"), Al(13, "Al"), Si(14, "Si"), P(15, "P"), S(16, "S"), Cl(17,
			"Cl"), Ar(18, "Ar"), K(19, "K"), Ca(20, "Ca"), Sc(21, "Sc"), Ti(22,
			"Ti"), V(23, "V"), Cr(24, "Cr"), Mn(25, "Mn"), Fe(26, "Fe"), Co(27,
			"Co"), Ni(28, "Ni"), Cu(29, "Cu"), Zn(30, "Zn"), Ga(31, "Ga"), Ge(
			32, "Ge"), As(33, "As"), Se(34, "Se"), Br(35, "Br"), Kr(36, "Kr"), Rb(
			37, "Rb"), Sr(38, "Sr"), Y(39, "Y"), Zr(40, "Zr"), Nb(41, "Nb"), Mo(
			42, "Mo"), Tc(43, "Tc"), Ru(44, "Ru"), Rh(45, "Rh"), Pd(46, "Pd"), Ag(
			47, "Ag"), Cd(48, "Cd"), In(49, "In"), Sn(50, "Sn"), Sb(51, "Sb"), Te(
			52, "Te"), I(53, "I"), Xe(54, "Xe"), Cs(55, "Cs"), Ba(56, "Ba"), La(
			57, "La"), Ce(58, "Ce"), Pr(59, "Pr"), Nd(60, "Nd"), Pm(61, "Pm"), Sm(
			62, "Sm"), Eu(63, "Eu"), Gd(64, "Gd"), Tb(65, "Tb"), Dy(66, "Dy"), Ho(
			67, "Ho"), Er(68, "Er"), Tm(69, "Tm"), Yb(70, "Yb"), Lu(71, "Lu"), Hf(
			72, "Hf"), Ta(73, "Ta"), W(74, "W"), Re(75, "Re"), Os(76, "Os"), Ir(
			77, "Ir"), Pt(78, "Pt"), Au(79, "Au"), Hg(80, "Hg"), Tl(81, "Tl"), Pb(
			82, "Pb"), Bi(83, "Bi"), Po(84, "Po"), At(85, "At"), Rn(86, "Rn"), Fr(
			87, "Fr"), Ra(88, "Ra"), Ac(89, "Ac"), Th(90, "Th"), Pa(91, "Pa"), U(
			92, "U"), Np(93, "Np"), Pu(94, "Pu"), Am(95, "Am"), Cm(96, "Cm"), Bk(
			97, "Bk"), Cf(98, "Cf"), Es(99, "Es"), Fm(100, "Fm"), Md(101, "Md"), No(
			102, "No"), Lr(103, "Lr");

	/**
	 * The name of the atom in the Periodic table.
	 */
	private final String name;

	/**
	 * The atomic number of the atom in the Periodic table.
	 */
	private final int atomicNumber;

	private AtomDescriptor(int atomicNumber, String name) {
		this.atomicNumber = atomicNumber;
		this.name = name;
	}

	public int getAtomicNumber() {
		return atomicNumber;
	}

	public String getName() {
		return name;
	}

	/**
	 * Given an atomic number, the method returns the corresponding
	 * <code>AtomDescriptor</code> object.
	 * 
	 * @param atomicNumber Z value of the Element.
	 * @return AtomDescriptor with corresponding atomic number
	 */
	public static AtomDescriptor getAtom(int atomicNumber) {
		for (AtomDescriptor a : AtomDescriptor.values()) {
			if (a.getAtomicNumber() == atomicNumber) {
				return a;
			}
		}

		// normally should not go here
		throw new IllegalArgumentException(
				"The provided atomic number does not correspond "
						+ "to an element from the periodic table!");
	}

	/**
	 * Given a name of an atom, the method returns the corresponding
	 * <code>AtomDescriptor</code> object.
	 * 
	 * @param name The chemical symbol of the atom as given in the periodic table.
	 * @return AtomDescriptor with corresponding atom name
	 */
	public static AtomDescriptor getAtom(String name) {
		for (AtomDescriptor a : AtomDescriptor.values()) {
			if (a.getName().equals(name)) {
				return a;
			}
		}

		// normally should not go here
		throw new IllegalArgumentException("The provided atomic symbol: "
				+ name + " does not correspond "
				+ "to an element from the periodic table!");
	}

	@Override
	public boolean consideredEqual(AtomDescriptor other) {
		if (other == null)
			return false;

		if (this.equals(R) || other.equals(R))
			return true;
		else
			return equals(other);
	}
}