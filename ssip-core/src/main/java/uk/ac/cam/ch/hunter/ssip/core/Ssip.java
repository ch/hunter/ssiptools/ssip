/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.Formatter;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

import uk.ac.cam.ch.hunter.ssip.core.interfaces.INearestAtom;
import uk.ac.cam.ch.hunter.ssip.core.interfaces.IPointProperty;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

/**
 * Represents a surface site interaction point (SSIP) on the molecular surface.
 * 
 * @author tn300
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Ssip extends Point3D implements INearestAtom, IPointProperty{
	/**
	 * Molecular electrostatic potential point on which the SSIP is placed
	 */
	@XmlAttribute(name = "value", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private double value;
	
	/**
	 * This is an ID for the closest atom.
	 */
	@XmlAttribute(name = "nearestAtomID", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	protected String nearestAtomID;
	
	
	/**
	 * Electrostatic potential in kJ/mol. Used only in OriginalSSIPSelector
	 */
	@XmlTransient
	private double ep;

	/**
	 * Null constructor for JAXB.
	 */
	public Ssip() {
		super(0.0, 0.0, 0.0);
		this.value = 0.0;
	}

	/**
	 * Constructor takes double values for position and value.
	 * 
	 * @param x x component of the Cartesian.
	 * @param y x component of the Cartesian.
	 * @param z x component of the Cartesian.
	 * @param value value.
	 */
	public Ssip(double x, double y, double z, double value){
		this.x = x;
		this.y = y;
		this.z = z;
		this.value = value;
	}
	
	/**
	 * Creates a new {@link Ssip} from an input {@link MepPoint}.
	 * 
	 * @param <T> This is the type parameter.
	 * @param mepPoint takes an {@link MepPoint} and generates an {@link Ssip} representation of it.
	 * @return new {@link Ssip}.
	 */
	public static <T extends MepPoint> Ssip fromMepPoint(T mepPoint) {
		Ssip ssip = new Ssip(mepPoint.getX(), mepPoint.getY(), mepPoint.getZ(), mepPoint.getValue());
		ssip.setNearestAtomID(mepPoint.getNearestAtomID());
		ssip.setEp(mepPoint.getEp());
		return ssip;
	}

	public <T extends Ssip> Ssip(T ssip){
		if (ssip == null) {
			throw new IllegalArgumentException("Null values are not allowed!");
		}
		this.x = ssip.getX();
		this.y = ssip.getY();
		this.z = ssip.getZ();
		this.nearestAtomID = ssip.getNearestAtomID();
		this.value = ssip.getValue();
	}

	public double getValue() {
		return value;
	}

	/**
	 * Returns a truncated this.getValue() for use in Jmol visualisation.
	 *
	 * @return truncated this.getValue()
	 */
	public String toFormattedString() {
		
		try (Formatter formatter = new Formatter()){
			return formatter.format("%.2f", this.getValue()).toString();
		}
	}
	
	@Override
	public String toString() {
		return "SSIP Value: " + getValue() + super.toString();
	}

	/**
	 * Provides the energy associated to <code>this</code> SSIP in the given
	 * configuration of SSIPs. The energy is calculated on the basis of the
	 * alpha/beta value of that point and the energy derived from the spatial
	 * configuration of SSIPs on the surface. If <code>this</code> SSIP is
	 * within distance squared as specified by
	 * <code>effectiveSSIPDistanceSquared</code> of another SSIP, the energy
	 * associated to that SSIP will be negative. If that is not the case the
	 * energy is given simply by the alpha/beta value of the MEP on which the
	 * SSIP is centered, and is positive.
	 * 
	 * @param ssipDistribution
	 *            configuration of SSIPs which might include <code>this</code>
	 *            SSIP
	 * @param effectiveSSIPDistanceSquared
	 *            specifies the distance between <code>this</code> SSIP and
	 *            other SSIPs in <code>ssipDistribution</code> at which the
	 *            energy becomes negative.
	 * @return energy of <code>this</code> SSIP object
	 */
	public double getEnergy(CopyOnWriteArrayList<Ssip> ssipDistribution,
			double effectiveSSIPDistanceSquared) {

		// Distance energy of SSIP configuration
		double distanceEnergy = 0;

		// Squared distance between SSIPs
		double distanceSquared;

		// Iterates over all SSIPs
		for (Ssip otherSSIP : ssipDistribution) {

			// if the SSIP is not the one being moved
			if (!otherSSIP.equals(this)) {

				// calculates the distance squared between that SSIP and the
				// moved SSIP
				distanceSquared = this.distanceSquaredTo(otherSSIP);

				// if distance between the two SSIPs is smaller than
				// 'effectiveSSIPDistance'
				if (effectiveSSIPDistanceSquared > distanceSquared) {

					// decreases energy
					distanceEnergy -= 1 / distanceSquared;
				}
			}
		}

		// electrostatic energy is positive or equal to zero
		// it corresponds to either the alpha or beta values
		double electrostaticEnergy = this.getValue();

		return distanceEnergy < 0 ? distanceEnergy : electrostaticEnergy;
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 31;
			long temp = Double.doubleToLongBits(value);
			hash  = prime * super.hashCode() + (int) (temp ^ (temp >>> 32)) ;
			hashCodeFound = true;
		}
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ssip other = (Ssip) obj;

		return Math.abs(this.value - other.getValue()) < Constants.ERROR_IN_ELECTROSTATIC_POTENTIAL;
	}

	@Override
	public Point3D getPosition(){
		return new Point3D(getX(), getY(), getZ());
	}
	
	@Override
	public String getNearestAtomID() {
		return nearestAtomID;
	}

	@Override
	public void setNearestAtomID(String nearestAtomID) {
		this.nearestAtomID = nearestAtomID;
	}

	public double getEp() {
		return ep;
	}

	public void setEp(double ep) {
		this.ep = ep;
	}
}
