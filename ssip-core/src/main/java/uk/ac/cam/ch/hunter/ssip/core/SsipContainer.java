/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

/**
 * Represents a group of Surface Site interaction Points (SSIPs) for a molecule.
 * 
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "SSIPContainer", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
public class SsipContainer {

	/*
	 * Hash Set of SSIP contains all the SSIPs related to the molecule.
	 */
	
	
	@XmlElement(name = "SSIP", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private ArrayList<Ssip> sSIPs;

	/**
	 * Constructor for JAXB.
	 */
	public SsipContainer() {
		this.sSIPs = new ArrayList<>();
	}

	/**
	 * Constructor to initialise {@link SsipContainer} with collection of SSIPs.
	 * 
	 * @param ssipList Collection of SSIPs.
	 */
	public SsipContainer(Collection<Ssip> ssipList) {
		this.sSIPs = (ArrayList<Ssip>) getSortedSSIPS(ssipList);
	}

	@Override
	public String toString() {
		ArrayList<Ssip> sSIPList = (ArrayList<Ssip>) this.getSSIPs();
		return sSIPList.toString();
	}

	/**
	 * Store the canonicalised List of SSIPs rather than a set. This ensures the same write order every time.
	 * 
	 * @param ssips List of SSIPs
	 * @return Canonicalised list of SSIPs
	 */
	private List<Ssip> getSortedSSIPS(Collection<Ssip> ssips) {
		ArrayList<Ssip> sortedSsips = new ArrayList<>(ssips);
		CentroidCalculator centroidCalculator = new CentroidCalculator();
		Point3D centroid = centroidCalculator
				.calculateSSIPCentroid(sortedSsips);
		Comparator<Ssip> comparator = new SsipComparator(centroid);
		Collections.sort(sortedSsips, comparator);
		return sortedSsips;
	}
	
	public List<Ssip> getSSIPs() {
		return sSIPs;
	}

	public void setSSIPs(Collection<Ssip> sSIPList) {
		this.sSIPs = (ArrayList<Ssip>) getSortedSSIPS(sSIPList);
	}

}
