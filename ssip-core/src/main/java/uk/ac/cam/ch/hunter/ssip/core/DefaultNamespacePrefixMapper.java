/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

/**
 * Implementation of {@link NamespacePrefixMapper} that maps the schema
 * namespaces more to readable names. Used by the jaxb marshaller. Requires
 * setting the property "com.sun.xml.bind.namespacePrefixMapper" to an instance
 * of this class.
 * 
 * This class also contains the Schema locations for the CML and SSIP namespaces.
 * 
 * <p>
 * Requires dependency on JAXB implementation jars
 * </p>
 * 
 * @author mark
 *
 */
public class DefaultNamespacePrefixMapper extends NamespacePrefixMapper {

	private static final Logger LOG = LogManager.getLogger(DefaultNamespacePrefixMapper.class);

	private Map<String, String> namespaceMap = makeNamespaceMap();
	
	private static URL ssipSchemaLocationURL = makeUrl("http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd");
	
	private static URL cmlSchemaLocationURL = makeUrl("http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd");
	
	private static URL phaseSchemaLocationURL = makeUrl("http://www-hunter.ch.cam.ac.uk/schema/PhaseSchema.xsd");
	
	private static final String SCHEMA_LOCATION_SSIP = "http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd";
	
	private static final String SCHEMA_LOCATION_PHASE = "http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd http://www-hunter.ch.cam.ac.uk/PhaseSchema http://www-hunter.ch.cam.ac.uk/schema/PhaseSchema.xsd";
	
	private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	
	private static final String SSIP_XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<?xml-stylesheet type=\"text/xsl\" href=\"http://www-hunter.ch.cam.ac.uk/xlst/ssip.xsl\"?>";
	
	private static final String PHASE_XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<?xml-stylesheet type=\"text/xsl\" href=\"http://www-hunter.ch.cam.ac.uk/xlst/phase.xsl\"?>";

	/**
	 * Create mappings.
	 * @throws JAXBException JAXBException
	 */
	public DefaultNamespacePrefixMapper()  throws JAXBException {
		/**
		 * Static methods used.
		 */
	}
 
	/* (non-Javadoc)
	 * Returning null when not found based on spec.
	 * @see com.sun.xml.bind.marshaller.NamespacePrefixMapper#getPreferredPrefix(java.lang.String, java.lang.String, boolean)
	 */
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		/**
		 * getOrDefault is available from Java 8. This would clean up this. 
		 */
		if (namespaceMap.containsKey(namespaceUri)) {
			return namespaceMap.get(namespaceUri);
		} else {
			return suggestion;
		}
		
	}

	/**
	 * This creates {@link URL} for given string.
	 * 
	 * @param urlString
	 * @return URL
	 */
	private static URL makeUrl(String urlString) {
		try {
			return new URL(urlString);
		} catch (MalformedURLException e) {
			LOG.error(e);
			return null;
		}
	}
	
	private static Map<String, String> makeNamespaceMap(){
		Map<String, String> namespaceMap = new HashMap<>();
		namespaceMap.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");
		namespaceMap.put("http://www.xml-cml.org/schema", "cml");
		namespaceMap.put("http://www-hunter.ch.cam.ac.uk/SSIP", "ssip");
		namespaceMap.put("http://www-hunter.ch.cam.ac.uk/PhaseSchema", "phase");
		return namespaceMap;
	}
	
	public URL getSsipSchemaLocationURL() {
		return ssipSchemaLocationURL;
	}

	public URL getCmlSchemaLocationURL() {
		return cmlSchemaLocationURL;
	}

	public static String getSchemalocationstringssip() {
		return SCHEMA_LOCATION_SSIP;
	}

	public URL getPhaseSchemaLocationURL() {
		return phaseSchemaLocationURL;
	}

	public static String getSchemaLocationPhase() {
		return SCHEMA_LOCATION_PHASE;
	}

	public static String getXmlHeader() {
		return XML_HEADER;
	}

	public static String getSsipXmlHeader() {
		return SSIP_XML_HEADER;
	}

	public static String getPhaseXmlHeader() {
		return PHASE_XML_HEADER;
	}

}
