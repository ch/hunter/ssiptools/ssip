/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class for getting the project properties including version.
 * 
 * @author mdd31
 *
 */
public class ProjectProperties {
	
	private static final Logger LOG = LogManager.getLogger(ProjectProperties.class);
	
	final String projectVersion;
	
	final String projectArtifactID;
	
	/**
	 * Constructor.
	 */
	public ProjectProperties(){
		InputStream inputstream = null;
		Properties properties = new Properties();
		try {
			inputstream = this.getClass().getResourceAsStream("/project.properties");
			LOG.debug("input stream:");
			LOG.debug(inputstream);
			
			LOG.debug("project properties pre load:");
			LOG.debug(properties);
			properties.load(inputstream);
			LOG.debug("project properties post load:");
			LOG.debug(properties);
		} catch (IOException e) {
			LOG.error(e);
		} finally {
			if (inputstream != null) {
				try {
					inputstream.close();
				} catch (IOException e) {
					LOG.error(e);
				}
			}
			projectArtifactID = properties.getProperty("artifactId");
			projectVersion = properties.getProperty("version");
		}
		
		
	}
	
	public String getVersion(){
		return projectVersion;
	}
	
	public String getArtifactID(){
		return projectArtifactID;
	}
}
