/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import java.util.Comparator;

import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.ERROR_IN_POSITIONS;

/**
 * A Comparator class used to compare the distance of MEPPoints with respect to
 * a reference point.
 * 
 * @author tn300
 */
public class EuclideanDistanceComparator implements Comparator<Point3D> {

	/**
	 * This error constant was calculated based on the following assumptions:
	 * <p>
	 * 1) that errors in each coordinate are the same
	 * <p>
	 * 2) that errors in each coordinates are independent
	 * <p>
	 * Refer to wikipedia: <a href="https://en.wikipedia.org/wiki/Propagation_of_uncertainty">
	 * Propagation of uncertainty</a> for more information.
	 * 
	 */
	public static final double ERROR = 2 * ERROR_IN_POSITIONS;

	/**
	 * The reference point of the comparator. All comparisons are made with
	 * respect to that point.
	 */
	private final Point3D referencePoint;

	public EuclideanDistanceComparator(Point3D referencePoint) {
		if (referencePoint == null)
			throw new IllegalArgumentException("Null values are not allowed!");
		this.referencePoint = referencePoint;
	}

	@Override
	public int compare(Point3D o1, Point3D o2) {
		if (o1 == null || o2 == null)
			throw new IllegalArgumentException("Null values are not allowed!");

		// used distance squared for faster calculations
		double o1distanceSquared = o1.distanceSquaredTo(referencePoint);
		double o2distanceSquared = o2.distanceSquaredTo(referencePoint);

		if (o1distanceSquared == o2distanceSquared) {
			return 1;
		} else if (o1distanceSquared > o2distanceSquared) {
			return 1;
		} else {
			return -1;
		}
	}

	public Point3D getReferencePoint() {
		return referencePoint;
	}
}