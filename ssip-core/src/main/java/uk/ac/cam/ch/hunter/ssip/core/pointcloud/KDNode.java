/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.ERROR_IN_POSITIONS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.sub;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Represents a node of a kd-tree. There are two subclasses of
 * <code>KDNode</code>: {@link InternalKDNode} and {@link LeafKDNode}
 * implementing leaf nodes and internal nodes. Internal nodes provide references
 * to other nodes, leaf nodes provide reference to all MEPs contained in that
 * node.
 * 
 * @author tn300
 */
public abstract class KDNode<T extends Point3D> {

	/**
	 * The edge of the bounding box that is closer to the origin of the
	 * coordinate system and is diagonally opposite to farEdgeOfBoundingBox.
	 */
	private final Point3D nearEdgeOfBoundingBox;

	/**
	 * The edge of the bounding box that is further from the origin of the
	 * coordinate system and is diagonally opposite to nearEdgeOfBoundingBox.
	 */
	private final Point3D farEdgeOfBoundingBox;

	private boolean hashCodeFound = false;
	private int hash;

	/**
	 * Represents a node of a KDTree. The node is a rectangular box spanned by
	 * <code>nearEdgeOfBoundingBox</code> and <code>farEdgeOfBoundingBox</code>.
	 * <code>nearEdgeOfBoundingBox</code> is the edge of the box with minimum
	 * 'x', 'y' and 'z' coordinates, <code>farEdgeOfBoundingBox</code> is the
	 * edge of the box with maximum 'x', 'y' and 'z' coordinates of all the
	 * edges of the box. The edges are diagonally opposite along one of the
	 * longest diagonals of the box (there are two such diagonals).
	 * <code>nearEdgeOfBoundingBox</code> and <code>farEdgeOfBoundingBox</code>
	 * may have one or two of its coordinates equal corresponding respectively
	 * to a rectangle in an axis-orthogonal plane and an axis perpendicular
	 * line.
	 * 
	 * @param nearEdgeOfBoundingBox
	 *            edge of the rectangular box with minimum 'x', 'y' and 'z'
	 *            components
	 * @param farEdgeOfBoundingBox
	 *            edge of the rectangular box with maximum 'x', 'y' and 'z'
	 *            components
	 */
	public KDNode(Point3D nearEdgeOfBoundingBox, Point3D farEdgeOfBoundingBox) {
		if (nearEdgeOfBoundingBox == null || farEdgeOfBoundingBox == null)
			throw new IllegalArgumentException("Null values are not allowed.");
		if (nearEdgeOfBoundingBox.equals(farEdgeOfBoundingBox))
			throw new IllegalArgumentException(
					"Near and far edges must be different "
							+ "within the tolerance specified in the tree.");

		Point3D boxDiagonal = sub(farEdgeOfBoundingBox, nearEdgeOfBoundingBox);
		if (boxDiagonal.minCoord() < ERROR_IN_POSITIONS)
			throw new IllegalArgumentException(
					"The specified edges do not span a box.");

		this.nearEdgeOfBoundingBox = nearEdgeOfBoundingBox;
		this.farEdgeOfBoundingBox = farEdgeOfBoundingBox;
	}

	/**
	 * Checks if <code>point</code> is within the bounding box of the node.
	 * 
	 * @param point Point in question.
	 * @return True if point is within the bounding box of the node.
	 */
	public boolean containsPoint(Point3D point) {
		if (point == null)
			throw new IllegalArgumentException("Null is not allowed.");

		boolean containedInX = nearEdgeOfBoundingBox.getX() <= point.getX()
				&& point.getX() <= farEdgeOfBoundingBox.getX();
		boolean containedInY = nearEdgeOfBoundingBox.getY() <= point.getY()
				&& point.getX() <= farEdgeOfBoundingBox.getY();
		boolean containedInZ = nearEdgeOfBoundingBox.getZ() <= point.getZ()
				&& point.getX() <= farEdgeOfBoundingBox.getZ();

		return containedInX && containedInY && containedInZ;
	}

	/**
	 * Finds the 'k' closest MEPs to <code>queryPoint</code>. Implemented as a
	 * recursive method. <code>closestPoints</code> is build up by adding the
	 * closest points to <code>queryPoint</code>. <code>closestPoints</code> is
	 * sorted at all times with the last element being the most distant from
	 * <code>queryPoint</code>. The size of <code>queryPoint</code> stays less
	 * than or equal 'k' in between function calls.
	 * 
	 * @param closestPoints
	 *            set of MEPs closest to <code>queryMEP</code>
	 * @param queryPoint
	 *            reference MEP
	 * @param k
	 *            number of nearest neighbours
	 */
	protected abstract void searchNode(final TreeSet<T> closestPoints,
			Point3D queryPoint, int k);

	/**
	 * Finds the closest MEPs to <code>queryPoint</code> within a distance of
	 * ballRadius. Implemented as a recursive method. <code>closestPoints</code>
	 * is build up by adding the closest points to <code>queryPoint</code>.
	 * <code>closestPoints</code> is sorted at all times with the last element
	 * being the most distant from <code>queryPoint</code>.
	 * 
	 * @param closestPoints
	 *            set of MEPs closest to <code>queryMEP</code> within a distance
	 *            of ballradius.
	 * @param queryPoint
	 *            reference MEP.
	 * @param ballRadius
	 *            distance to reference MEP of required points.
	 */
	protected abstract void searchNode(final TreeSet<T> closestPoints,
			Point3D queryPoint, double ballRadius);

	/**
	 * Given a MEP, it returns true if the Node contains the MEP.
	 * 
	 * @param queryPoint Point being queried.
	 * @return True if node contains the MEP
	 */
	public abstract boolean belongsTo(Point3D queryPoint);

	/**
	 * Returns the number of points in the bounding box.
	 * 
	 * @return Number of points in the bounding box.
	 */
	public abstract int numberOfPointsInBoundingBox();

	/**
	 * Returns all points contained in the bounding box.
	 * 
	 * @return All points contained in the bounding box.
	 */
	public abstract ArrayList<T> getPointsInBoundingBox();

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 31;
			int result = 1;
			result = prime * result + farEdgeOfBoundingBox.hashCode();
			result = prime * result + nearEdgeOfBoundingBox.hashCode();
			hash = result;
			hashCodeFound = true;
		}
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KDNode<?> other = (KDNode<?>) obj;
		if (!farEdgeOfBoundingBox.equals(other.farEdgeOfBoundingBox))
			return false;
		if (!nearEdgeOfBoundingBox.equals(other.nearEdgeOfBoundingBox))
			return false;
		return true;
	}

	public Point3D getNearEdgeOfBoundingBox() {
		return nearEdgeOfBoundingBox;
	}

	public Point3D getFarEdgeOfBoundingBox() {
		return farEdgeOfBoundingBox;
	}
}