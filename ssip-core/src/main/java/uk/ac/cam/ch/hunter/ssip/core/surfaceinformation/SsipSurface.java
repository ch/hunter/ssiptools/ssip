/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.surfaceinformation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Class containing information about the molecule surface used to generate the SSIP description.
 * This is a POJO, and is used purely as a store of the information.
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SsipSurface {
	
	@XmlTransient
	static final double ERROR_IN_DENSITY = 1e-5;
	@XmlElement(name = "TotalSurfaceArea", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private SurfaceArea totalSurfaceArea;
	@XmlElement(name = "NegativeSurfaceArea", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private SurfaceArea negativeSurfaceArea;
	@XmlElement(name = "PositiveSurfaceArea", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private SurfaceArea positiveSurfaceArea;
	@XmlElement(name = "ElectronDensityIsosurface", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private ElectronDensityIsoSurface electronDensityIsosurface;
	@XmlElement(name = "NumberOFMEPSPoints", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private int numberOfMEPSPoints;
	@XmlElement(name = "VdWVolume", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP", required=true)
	private SurfaceVolume volumeVdW;
	@XmlElement(name = "ElectrostaticPotentialMax", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private ElectrostaticPotential maxElectrostaticPotential;
	@XmlElement(name = "ElectrostaticPotentialMin", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private ElectrostaticPotential minElectrostaticPotential;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;
	
	public SsipSurface() {
		/**
		 * Null constructor for JAXB. 
		 */
	}

	/**
	 * Constructor initialising all values except volume and electrostatic potentials.
	 * 
	 * @param totalSurfaceArea Total surface area of molecule in Ang^2.
	 * @param negativeSurfaceArea Negative surface area of molecule in Ang^2. 
	 * @param positiveSurfaceArea Positive surface area of molecule in Ang^2.
	 * @param electronDensityIsosurface  Electron DensityIsosurface.
	 * @param numberOfMEPSPoints Number of MEP points.
	 */
	public SsipSurface(double totalSurfaceArea,
			double negativeSurfaceArea, double positiveSurfaceArea,
			double electronDensityIsosurface, int numberOfMEPSPoints) {
		this.setTotalSurfaceArea(totalSurfaceArea);
		this.setNegativeSurfaceArea(negativeSurfaceArea);
		this.setPositiveSurfaceArea(positiveSurfaceArea);
		this.setElectronDensityIsosurface(electronDensityIsosurface);
		this.setNumberOfMEPSPoints(numberOfMEPSPoints);
	}

	/**
	 * Constructor initialising all non electrostatic potential values.
	 * 
	 * @param totalSurfaceArea Total surface area of molecule in Ang^2.
	 * @param negativeSurfaceArea Negative surface area of molecule in Ang^2. 
	 * @param positiveSurfaceArea Positive surface area of molecule in Ang^2.
	 * @param electronDensityIsosurface  Electron DensityIsosurface.
	 * @param numberOfMEPSPoints Number of MEP points.
	 * @param volumeVdW Volume of the molecule in Ang^3.
	 */
	public SsipSurface(double totalSurfaceArea,
			double negativeSurfaceArea, double positiveSurfaceArea,
			double electronDensityIsosurface, int numberOfMEPSPoints, double volumeVdW) {
		this.setTotalSurfaceArea(totalSurfaceArea);
		this.setNegativeSurfaceArea(negativeSurfaceArea);
		this.setPositiveSurfaceArea(positiveSurfaceArea);
		this.setElectronDensityIsosurface(electronDensityIsosurface);
		this.setNumberOfMEPSPoints(numberOfMEPSPoints);
		this.setVolumeVdW(volumeVdW);
	}
	
	/**
	 * Constructor initialising all non electrostatic potential values.
	 * 
	 * @param totalSurfaceArea Total surface area of molecule in Ang^2.
	 * @param negativeSurfaceArea Negative surface area of molecule in Ang^2. 
	 * @param positiveSurfaceArea Positive surface area of molecule in Ang^2.
	 * @param electronDensityIsosurface  Electron DensityIsosurface.
	 * @param numberOfMEPSPoints Number of MEP points.
	 * @param volumeVdW Volume of the molecule in Ang^3.
	 * @param maxElectrostaticPotential on surface in kJmol^-1.
	 * @param minElectrostaticPotential on surface in kJmol^-1.
	 */
	public SsipSurface(double totalSurfaceArea,
			double negativeSurfaceArea, double positiveSurfaceArea,
			double electronDensityIsosurface, int numberOfMEPSPoints, double volumeVdW,
			double maxElectrostaticPotential, double minElectrostaticPotential) {
		this.setTotalSurfaceArea(totalSurfaceArea);
		this.setNegativeSurfaceArea(negativeSurfaceArea);
		this.setPositiveSurfaceArea(positiveSurfaceArea);
		this.setElectronDensityIsosurface(electronDensityIsosurface);
		this.setNumberOfMEPSPoints(numberOfMEPSPoints);
		this.setVolumeVdW(volumeVdW);
		this.setMaxElectrostaticPotential(new ElectrostaticPotential(maxElectrostaticPotential));
		this.setMinElectrostaticPotential(new ElectrostaticPotential(minElectrostaticPotential));
	}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SsipSurface otherSsipSurfaceInformation = (SsipSurface) obj;
		
		if (getNumberOfMEPSPoints() == otherSsipSurfaceInformation.getNumberOfMEPSPoints() && getElectronDensityIsosurface().equals(otherSsipSurfaceInformation.getElectronDensityIsosurface())) {
			return getTotalSurfaceArea().equals(otherSsipSurfaceInformation.getTotalSurfaceArea()) && getPositiveSurfaceArea().equals(otherSsipSurfaceInformation.getPositiveSurfaceArea()) && getNegativeSurfaceArea().equals(otherSsipSurfaceInformation.getNegativeSurfaceArea());
		} else {
			return false;
		}
	}
	
	
	
	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = 1;
			long temp;
			temp = totalSurfaceArea.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = negativeSurfaceArea.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = positiveSurfaceArea.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = electronDensityIsosurface.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = numberOfMEPSPoints;
			result = prime * result + (int) (temp ^ (temp >>> 32));
			if (volumeVdW != null) {
				temp = volumeVdW.hashCode();
				result = prime * result + (int) (temp ^ (temp >>> 32));
			}
			if (maxElectrostaticPotential != null) {
				temp = maxElectrostaticPotential.hashCode();
				result = prime * result + (int) (temp ^ (temp >>> 32));
			}
			if (minElectrostaticPotential != null) {
				temp = minElectrostaticPotential.hashCode();
				result = prime * result + (int) (temp ^ (temp >>> 32));
			}
			
			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}

	public SurfaceArea getTotalSurfaceArea() {
		return totalSurfaceArea;
	}

	public void setTotalSurfaceArea(double totalSurfaceArea) {
		if (this.totalSurfaceArea != null) {
			this.totalSurfaceArea.setSurfaceArea(totalSurfaceArea);
		} else {
			this.totalSurfaceArea = new SurfaceArea(totalSurfaceArea);
		}
		
	}

	public SurfaceArea getNegativeSurfaceArea() {
		return negativeSurfaceArea;
	}

	public void setNegativeSurfaceArea(double negativeSurfaceArea) {
		if (this.negativeSurfaceArea != null) {
			this.negativeSurfaceArea.setSurfaceArea(negativeSurfaceArea);
		} else {
			this.negativeSurfaceArea = new SurfaceArea(negativeSurfaceArea);
		}
	}

	public SurfaceArea getPositiveSurfaceArea() {
		return positiveSurfaceArea;
	}

	public void setPositiveSurfaceArea(double positiveSurfaceArea) {
		if (this.positiveSurfaceArea != null) {
			this.positiveSurfaceArea.setSurfaceArea(positiveSurfaceArea);
		} else {
			this.positiveSurfaceArea = new SurfaceArea(positiveSurfaceArea);
		}
	}

	public ElectronDensityIsoSurface getElectronDensityIsosurface() {
		return electronDensityIsosurface;
	}

	public void setElectronDensityIsosurface(double electronDensityIsosurface) {
		if (this.electronDensityIsosurface != null) {
			this.electronDensityIsosurface.setDensity(electronDensityIsosurface);
		}
		this.electronDensityIsosurface = new ElectronDensityIsoSurface(electronDensityIsosurface);
	}

	public int getNumberOfMEPSPoints() {
		return numberOfMEPSPoints;
	}

	public void setNumberOfMEPSPoints(int numberOfMEPSPoints) {
		this.numberOfMEPSPoints = numberOfMEPSPoints;
	}

	public SurfaceVolume getVolumeVdW() {
		return volumeVdW;
	}

	public void setVolumeVdW(double volumeVdW) {
		if (this.volumeVdW != null) {
			this.volumeVdW.setVolume(volumeVdW);
		} else {
			this.volumeVdW = new SurfaceVolume(volumeVdW);
		}
		
	}

	public ElectrostaticPotential getMaxElectrostaticPotential() {
		return maxElectrostaticPotential;
	}

	public void setMaxElectrostaticPotential(ElectrostaticPotential maxElectrostaticPotential) {
		this.maxElectrostaticPotential = maxElectrostaticPotential;
	}

	public ElectrostaticPotential getMinElectrostaticPotential() {
		return minElectrostaticPotential;
	}

	public void setMinElectrostaticPotential(ElectrostaticPotential minElectrostaticPotential) {
		this.minElectrostaticPotential = minElectrostaticPotential;
	}
}