/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.X_AXIS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.Y_AXIS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.Z_AXIS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.ERROR_IN_POSITIONS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.cross;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.dot;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.sub;
import static java.lang.Math.abs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * The class provides a set of methods used to find the geometric properties of
 * a cloud of points.
 * 
 * @author tn300
 */
public final class GeometryUtils {
	private static final Logger LOG = LogManager.getLogger(GeometryUtils.class);

	// TODO: correct error propagation with asymptotic estimates

	private GeometryUtils() {

	}

	/**
	 * Checks if the specified array of points lies on the same line. Returns
	 * true if the number of points specified is less than 3 or the list is
	 * empty.
	 * 
	 * @param <T> This is the type parameter.
	 * @param points A set of Points.
	 * @return True if any of the points lie on the same line.
	 */
	public static <T extends Point3D> boolean areCollinear(ArrayList<T> points) {
		if (points == null){
			throw new IllegalArgumentException("Null values are not allowed.");
		}
		if (points.size() < 3){
			return true;
		}

		// Takes the two first points to create a vector along the line
		Point3D unitVectorAlongLine = null;
		int index = 1;
		for (int i = 1; i < points.size(); i++) {
			if (!points.get(0).equals(points.get(i))) {
				unitVectorAlongLine = sub(points.get(i), points.get(0))
						.normalize();
				index = i;
				break;
			}
		}

		Point3D unitVector;
		boolean areParallel;

		for (int i = index + 1; i < points.size(); i++) {

			unitVector = sub(points.get(i), points.get(0)).normalize();

			// checks if vectors are collinear to each other.
			areParallel = abs(abs(dot(unitVector, unitVectorAlongLine)) - 1.0) < ERROR_IN_POSITIONS;
			if (!areParallel)
				return false;
		}

		return true;
	}

	/**
	 * Returns true if the points lie on a line parallel to one of the axis.
	 * 
	 * @param <T> This is the type parameter.
	 * @param points A set of points.
	 * @return True if the points lie on a line parallel to one of the axis.
	 */
	public static <T extends Point3D> boolean areCollinearToAxis(
			ArrayList<T> points) {
		if (points == null)
			throw new IllegalArgumentException(
					"Null arguments are not allowed.");
		if (!areCollinear(points))
			return false;

		Point3D unitVectorAlongLineOfPoints = null;

		for (int i = 0; i < points.size(); i++) {
			if (!points.get(0).equals(points.get(i))) {
				unitVectorAlongLineOfPoints = sub(points.get(i), points.get(0))
						.normalize();
				break;
			}
		}

		boolean parallelToX = abs(abs(dot(unitVectorAlongLineOfPoints,
				new Point3D(1, 0, 0))) - 1) < ERROR_IN_POSITIONS;
		boolean parallelToY = abs(abs(dot(unitVectorAlongLineOfPoints,
				new Point3D(0, 1, 0))) - 1) < ERROR_IN_POSITIONS;
		boolean parallelToZ = abs(abs(dot(unitVectorAlongLineOfPoints,
				new Point3D(0, 0, 1))) - 1) < ERROR_IN_POSITIONS;

		return parallelToX || parallelToY || parallelToZ;
	}

	/**
	 * Returns true if the two points lie on a line parallel to one of the axis.
	 * 
	 * @param point1 Point 1.
	 * @param point2 Point 2.
	 * @return True if the two points lie on a line parallel to one of the axis.
	 */
	public static boolean areCollinearToAxis(Point3D point1, Point3D point2) {
		return areCollinearToAxis(new ArrayList<>(Arrays.asList(point1, point2)));
	}

	/**
	 * Checks if the specified array of points lies on the same plane. Returns
	 * true if the number of points specified is less than 4 or the list is
	 * empty.
	 * 
	 * @param <T> This is the type parameter.
	 * @param points A set of points.
	 * @return True if points lies on the same plane.
	 */
	public static <T extends Point3D> boolean areCoplanar(ArrayList<T> points) {
		if (points == null)
			throw new IllegalArgumentException("Null values are not allowed.");
		if (points.size() < 4)
			return true;

		Point3D baseVector1 = null;
		int index = 1;
		// The first base vector
		for (int i = 1; i < points.size(); i++) {
			if (!points.get(0).equals(points.get(i))) {
				baseVector1 = sub(points.get(i), points.get(0)).normalize();
				index = i;
				break;
			}
		}

		// The second base vector
		Point3D baseVector2 = null;

		// normal to the plane
		Point3D normalToPlane = null;

		Point3D vectorBetweenPoints;
		boolean areCoplanar;
		boolean areCollinear;
		for (int i = index + 1; i < points.size(); i++) {
			if (!points.get(i).equals(points.get(0))) {
				vectorBetweenPoints = sub(points.get(i), points.get(0))
						.normalize();

				if (baseVector2 == null) {

					// checks if vectors are collinear to each other.
					areCollinear = abs(abs(dot(vectorBetweenPoints, baseVector1)) - 1) < ERROR_IN_POSITIONS;

					if (!areCollinear) {
						baseVector2 = vectorBetweenPoints;
						normalToPlane = cross(baseVector1, baseVector2)
								.normalize();
					}
				} else {

					// checks if vectors are perpendicular to each other.
					areCoplanar = Math.abs(dot(vectorBetweenPoints,
							normalToPlane)) < ERROR_IN_POSITIONS;
					if (!areCoplanar)
						return false;
				}
			}
		}

		return true;
	}

	/**
	 * Given <code>point1</code> and <code>point2</code>, the method returns
	 * true if the points lie on a plane that is orthogonal to one of the axes.
	 * Equivalently, it returns true if the points lie on a plane parallel to
	 * one of the planes defined by the axis.
	 * 
	 * @param point1 Point 1.
	 * @param point2 Point 2.
	 * @return True if the points lie on a plane that is orthogonal to one of the axes.
	 */
	public static boolean areCoplanarToAxis(Point3D point1, Point3D point2) {
		if (point1 == null || point2 == null)
			throw new IllegalArgumentException("Null values are not allowed.");
		Point3D vec = sub(point1, point2);

		boolean liesOnX = Math.abs(vec.getX()) < ERROR_IN_POSITIONS;
		boolean liesOnY = Math.abs(vec.getY()) < ERROR_IN_POSITIONS;
		boolean liesOnZ = Math.abs(vec.getZ()) < ERROR_IN_POSITIONS;

		return liesOnX || liesOnY || liesOnZ;
	}

	/**
	 * Given an array of points, the method returns true if the points lie on a
	 * plane that is orthogonal to one of the axes. Equivalently, it returns
	 * true if the points lie on a plane parallel to one of the planes defined
	 * by the axis.
	 * 
	 * @param <T> This is the type parameter.
	 * @param points A set of Points.
	 * @return True if the points lie on a plane that is orthogonal to one of the axes.
	 */
	public static <T extends Point3D> boolean areCoplanarToAxis(
			ArrayList<T> points) {
		if (points == null)
			throw new IllegalArgumentException("Null is not allowed!");
		if (!areCoplanar(points))
			return false;

		Point3D unitVectorInPlane = null;

		for (int i = 1; i < points.size(); i++) {
			if (!points.get(0).equals(points.get(i))) {
				LOG.debug("points.get(i): {}", points.get(i));
				LOG.debug("points.get(0): {}", points.get(0));
				unitVectorInPlane = sub(points.get(i), points.get(0))
						.normalize();
				break;
			}
		}

		LOG.debug("unitVectorInPlane: {}", unitVectorInPlane);

		boolean orthogonalToX = Math.abs(dot(unitVectorInPlane, new Point3D(1,
				0, 0))) < ERROR_IN_POSITIONS;
		boolean orthogonalToY = Math.abs(dot(unitVectorInPlane, new Point3D(0,
				1, 0))) < ERROR_IN_POSITIONS;
		boolean orthogonalToZ = Math.abs(dot(unitVectorInPlane, new Point3D(0,
				0, 1))) < ERROR_IN_POSITIONS;

		LOG.debug("orthogonalToX: {}", orthogonalToX);
		LOG.debug("orthogonalToY: {}", orthogonalToY);
		LOG.debug("orthogonalToZ: {}", orthogonalToZ);

		return orthogonalToX || orthogonalToY || orthogonalToZ;
	}

	/**
	 * Given a cloud of points, the method calculates its minimum bounding box
	 * and returns the two diagonally opposite edges, with minimum and maximum
	 * coordinates, which span the box.
	 * 
	 * @param <T> This is the type parameter.
	 * 
	 * @param cloudOfPoints
	 *            cloud of points whose bounding box is to be determined
	 * @return A list containing two elements. The first element is the edge
	 *         with minimum coordinates. The second element is the edge with
	 *         maximum coordinate.
	 */
	public static <T extends Point3D> ArrayList<Point3D> findBoundingBox(
			List<T> cloudOfPoints) {
		if (cloudOfPoints == null)
			throw new IllegalArgumentException("Null values are not allowed!");
		if (cloudOfPoints.size() < 2)
			throw new IllegalArgumentException(
					"There must be at least two points in the list.");

		// Minimum x,y and z coordinates of the point cloud
		double minX = Double.MAX_VALUE;
		double minY = Double.MAX_VALUE;
		double minZ = Double.MAX_VALUE;

		// Maximum x,y and z coordinates of the point cloud
		double maxX = Double.MIN_VALUE;
		double maxY = Double.MIN_VALUE;
		double maxZ = Double.MIN_VALUE;

		// The x, y and z component of each MEP
		double tmpX;
		double tmpY;
		double tmpZ;

		// Iterates over all MEPs in space
		for (Point3D point : cloudOfPoints) {

			tmpX = point.getCoord(X_AXIS);
			tmpY = point.getCoord(Y_AXIS);
			tmpZ = point.getCoord(Z_AXIS);

			// Finds maximum coordinates.
			if (tmpX > maxX)
				maxX = tmpX;
			if (tmpY > maxY)
				maxY = tmpY;
			if (tmpZ > maxZ)
				maxZ = tmpZ;

			// Finds minimum coordinates.
			if (tmpX < minX)
				minX = tmpX;
			if (tmpY < minY)
				minY = tmpY;
			if (tmpZ < minZ)
				minZ = tmpZ;
		}

		return new ArrayList<>(Arrays.asList(new Point3D(minX, minY, minZ),
				new Point3D(maxX, maxY, maxZ)));
	}
}
