/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Represents a leaf node of a kd-tree.
 * 
 * @author tn300
 */
public class LeafKDNode<T extends Point3D> extends KDNode<T> {

	/**
	 * Points contained in the bounding box associated to that node. The node is
	 * null if it is a leaf node.
	 */
	private final ArrayList<T> pointsInBoundingBox;

	private boolean hashCodeFound = false;
	private int hash;

	/**
	 * Constructs a leaf node of a kd-tree. The node maintains its own copy of
	 * the points to ensure that it is unaffected by changes made to the
	 * <code>pointsInBoundingBox</code> outside this class. It is common for
	 * <code>pointsInBoundingBox</code> to be a sublist providing a "view" to a
	 * larger list.
	 * 
	 * @param nearEdgeOfBoundingBox
	 *            edge of the rectangular box with minimum 'x', 'y' and 'z'
	 *            components
	 * @param farEdgeOfBoundingBox
	 *            edge of the rectangular box with maximum 'x', 'y' and 'z'
	 *            components
	 * @param pointsInBoundingBox
	 *            points contained in the bounding box represented by this leaf
	 *            node
	 */
	public LeafKDNode(Point3D nearEdgeOfBoundingBox,
			Point3D farEdgeOfBoundingBox, List<T> pointsInBoundingBox) {
		super(nearEdgeOfBoundingBox, farEdgeOfBoundingBox);
		if (pointsInBoundingBox == null)
			throw new IllegalArgumentException("No null values allowed.");

		this.pointsInBoundingBox = new ArrayList<>(pointsInBoundingBox);
	}

	@Override
	protected void searchNode(TreeSet<T> closestPoints, Point3D queryPoint,
			int k) {

		// Iterates over all points contained in the node
		for (T point : pointsInBoundingBox) {

			// Adds and sorts the MEP
			closestPoints.add(point);

			// if there are 'k+1' elements in the list
			if (closestPoints.size() == k + 1) {

				// remove the most distant
				closestPoints.pollLast();
			}

		}
	}

	@Override
	protected void searchNode(TreeSet<T> closestPoints, Point3D queryPoint,
			double ballradius) {

		// Iterates over all points contained in the node
		for (T point : pointsInBoundingBox) {

			if (point.distanceTo(queryPoint) <= ballradius) {
				// Adds and sorts the MEP
				closestPoints.add(point);
			}

		}
	}

	@Override
	public boolean belongsTo(Point3D queryPoint) {
		if (!containsPoint(queryPoint))
			return false;

		return pointsInBoundingBox.contains(queryPoint);
	}

	@Override
	public ArrayList<T> getPointsInBoundingBox() {
		return pointsInBoundingBox;
	}

	@Override
	public int numberOfPointsInBoundingBox() {
		return pointsInBoundingBox.size();
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + pointsInBoundingBox.hashCode();
			hash = result;
			hashCodeFound = true;
		}
		return hash;
	}

	/**
	 * Note that the number of points is only used to determine equality. It is
	 * expensive and unnecessary to test whether the points in the box are the
	 * same. In practice they always are.
	 * 
	 * @see uk.ac.cam.ch.hunter.ssip.core.pointcloud.KDNode#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeafKDNode<?> other = (LeafKDNode<?>) obj;
		if (pointsInBoundingBox.size() != other.pointsInBoundingBox.size())
			return false;

		return true;
	}
}
