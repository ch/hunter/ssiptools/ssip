/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.Comparator;

/**
 * Comparator class for MEPPoint. This replaces the comparable implementation on the {@link MepPoint}, which was overridden by {@link AlphaPoint} and {@link BetaPoint}. This is to provide greater consistency in behaviour with the {@link Ssip} class.
 * 
 * @author mdd31
 *
 */
public class MepPointComparator implements Comparator<MepPoint>{

	public static final double ERROR_IN_ELECTROSTATIC_POTENTIAL = 1E-7;
	
	@Override
	public int compare(MepPoint o1, MepPoint o2) {
		if (o1 instanceof BetaPoint && o2 instanceof AlphaPoint)
			return -1;
		if (o2 instanceof BetaPoint && o1 instanceof AlphaPoint)
			return 1;
		if (Math.abs(o2.getValue() - o1.getValue()) < ERROR_IN_ELECTROSTATIC_POTENTIAL)
			return 0;
		else if (Math.abs(o1.getValue()) - Math.abs(o2.getValue()) > 0)
			/**
			 * Use absolute values to ensure that BetaPoints are ordered with largest magnitude first. 
			 */
			return 1;
		else
			return -1;
	}

}
