/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.areCollinearToAxis;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.areCoplanarToAxis;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.sub;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Stack;
import java.util.TreeSet;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Represents binary tree implementing an efficient data structure for point
 * cloud processing in 3D space. The structure provides faster shape recognition
 * and nearest neighbour lookups. More information can be found <a
 * href="https://en.wikipedia.org/wiki/K-d_tree">here</a>. The tree lives in 3D
 * space and is constructed using a median splitting rule as described in [1].
 * <p>
 * Throughout the javaDoc, the rectangular box associated to each node is
 * referred to as 'bounding box'. The coordinate axis are labelled 'x', 'y' and
 * 'z' with corresponding indices 1, 2 and 3.
 * 
 * [1] S. Maneewongvatana, D. M. Mount. It’s okay to be skinny, if your friends
 * are fat. Center for Geometric Computing 4th Annual Workshop on Computational
 * Geometry, 1999.
 * 
 * @author tn300
 */
public class KDTree<T extends Point3D> {

	private static final Logger LOG = LogManager.getLogger(KDTree.class);

	/**
	 * Maximum number of points contained in every leaf node of the tree.
	 */
	private final int maxNumberOfPointsInBoundingBox;

	/**
	 * The node representing the root of the tree.
	 */
	private final KDNode<T> root;

	/**
	 * Number of points contained in the tree.
	 */
	private final int numberOfPoints;

	/**
	 * Points contained in the tree.
	 */
	private final ArrayList<T> cloudOfPoints;

	private boolean hashCodeFound = false;
	private int hash;

	public KDTree(KDNode<T> root) {
		if (root == null)
			throw new IllegalArgumentException("Null is not allowed.");
		this.root = root;
		cloudOfPoints = root.getPointsInBoundingBox();
		numberOfPoints = cloudOfPoints.size();
		maxNumberOfPointsInBoundingBox = findMaxNumberOfPointsInBoundingBox(root);
	}

	public KDTree(int maxNumberOfPointsInBoundingBox, ArrayList<T> cloudOfPoints) {
		if (cloudOfPoints == null)
			throw new IllegalArgumentException("Null values are not allowed!");
		if (cloudOfPoints.size() < 2)
			throw new IllegalArgumentException(
					"Not enough points in the list! There must be at least "
							+ "two points in the list.");

		LOG.debug("cloudOfPoints: {}", cloudOfPoints);

		if (areCollinearToAxis(cloudOfPoints))
			throw new IllegalArgumentException(
					"A tree can not be constructed from collinear "
							+ "points parallel to an axis.");
		if (areCoplanarToAxis(cloudOfPoints))
			throw new IllegalArgumentException(
					"A tree can not be constructed from coplanar "
							+ "points parallel to an axis plane.");
		if (maxNumberOfPointsInBoundingBox > cloudOfPoints.size())
			throw new IllegalArgumentException(
					"'maxNumberOfPointsInBoundingBox' should be less than the total number of points.");
		if (maxNumberOfPointsInBoundingBox <= 0)
			throw new IllegalArgumentException(
					"'maxNumberOfPointsInBoundingBox' must be a positive integer.");

		numberOfPoints = cloudOfPoints.size();
		LOG.debug("numberOfPoints: {}", numberOfPoints);
		this.maxNumberOfPointsInBoundingBox = maxNumberOfPointsInBoundingBox;
		LOG.debug("maxNumberOfPointsInBoundingBox: {}",
				maxNumberOfPointsInBoundingBox);

		this.cloudOfPoints = new ArrayList<>(cloudOfPoints);

		// Finds the rectangular bounding box of the point cloud
		ArrayList<Point3D> boundingBox = GeometryUtils
				.findBoundingBox(cloudOfPoints);

		// near edge
		Point3D nearEdgeOfBoundingBox = boundingBox.get(0);
		LOG.debug("root nearEdgeOfBoundingBox: {}", nearEdgeOfBoundingBox);

		// far edge
		Point3D farEdgeOfBoundingBox = boundingBox.get(1);
		LOG.debug("root farEdgeOfBoundingBox: {}", farEdgeOfBoundingBox);

		// Constructs the tree
		root = constructTree(this.cloudOfPoints, nearEdgeOfBoundingBox,
				farEdgeOfBoundingBox);

		LOG.info("Done initialisation");
	}

	/**
	 * Constructs a KDTree from the given list of points. The number of points
	 * per bounding box is set to the square root of the total number of points.
	 * 
	 * @param cloudOfPoints Input set of Points.
	 */
	public KDTree(ArrayList<T> cloudOfPoints) {
		this((int) Math.sqrt(listSizeRetriever(cloudOfPoints)), cloudOfPoints);
	}

	/**
	 * Retrieves the size of the array in a safe way. The method is used in the
	 * constructor accepting only one argument. Since <code>this()</code> has to
	 * be the first statement in the constructor, the method provides a way to
	 * test if null has been passed before calling <code>size()</code>.
	 * 
	 * @param cloudOfPoints
	 * @return
	 */
	private static <T extends Point3D> int listSizeRetriever(
			ArrayList<T> cloudOfPoints) {
		if (cloudOfPoints == null)
			throw new IllegalArgumentException("Null values are not allowed!");

		return cloudOfPoints.size();
	}

	/**
	 * Implements a bottom-up recursive algorithm to construct the KDTree. The
	 * node splitting rule is implemented using the median rule.
	 * 
	 * @param cloudOfPoints
	 * @return
	 */
	private KDNode<T> constructTree(List<T> cloudOfPoints,
			Point3D nearEdgeOfBoundingBox, Point3D farEdgeOfBoundingBox) {

		/*
		 * Creates a leaf node if number of points in the bounding box is
		 * smaller than the maximum number of points allowed.
		 */
		if (cloudOfPoints.size() <= maxNumberOfPointsInBoundingBox) {
			return new LeafKDNode<>(nearEdgeOfBoundingBox,
					farEdgeOfBoundingBox, cloudOfPoints);
		}

		// Finds the axis with greatest spread of points in space
		Point3D boxDiagonal = sub(farEdgeOfBoundingBox, nearEdgeOfBoundingBox);
		Axis splitAxis = boxDiagonal.maxCoordAxis();

		LOG.info("");
		LOG.info("splitAxis: {}", splitAxis);

		// Central splitting rule

		// Compares all points in the list along the give axis
		AxisDistanceComparator<T> comparator = new AxisDistanceComparator<>(
				splitAxis);

		Collections.sort(cloudOfPoints, comparator);
		LOG.debug("axis sorted cloudOfPoints: {}", cloudOfPoints);

		// sets the split position to the middle of the longest side of the
		// box
		double splitPositionAlongSplitAxis = nearEdgeOfBoundingBox
				.getCoord(boxDiagonal.maxCoordAxis())
				+ boxDiagonal.maxCoord()
				/ 2;

		LOG.info("splitPositionAlongSplitAxis: {}",
				splitPositionAlongSplitAxis);
		// Determines the split index

		// if all points are smaller than the split index
		int splitIndex = cloudOfPoints.size();
		for (T point : cloudOfPoints) {
			if (point.getCoord(splitAxis) > splitPositionAlongSplitAxis) {
				LOG.trace("cloud of points size: {}", cloudOfPoints.size());
				splitIndex = cloudOfPoints.indexOf(point);
				LOG.trace("point: {}", point);
				LOG.trace("split index: {}", splitIndex);
				break;
			}
		}

		
		/*
		 * TODO: fix Finds the median.
		 * 
		 * AxisDistanceComparator<T> comparator = new AxisDistanceComparator<>(
		 * splitAxis); Point3D mepOnSplittingPlane = quickSelect(cloudOfPoints,
		 * comparator, 0, cloudOfPoints.size() - 1, cloudOfPoints.size() / 2);
		 * int splitIndex = cloudOfPoints.indexOf(mepOnSplittingPlane); double
		 * splitPositionAlongSplitAxis = mepOnSplittingPlane
		 * .getCoord(splitAxis);
		 */

		/*
		 * Calculates farEdgeOfLeftBoundingBox and nearEdgeRightOfBoundingBox
		 * for each child. These are calculated by observing that both points
		 * lie on the splitting plane. In addition farEdgeOfLeftBoundingBox and
		 * farEdgeOfBoundingBox lie on a line parallel to the splitting axis.
		 * Finally nearEdgeOfRightBoundingBox and nearEdgeOfBoundingBox lie on
		 * the splitting axis.
		 */
		Point3D farEdgeOfLeftBoundingBox;
		Point3D nearEdgeOfRightBoundingBox;
		switch (splitAxis) {
		case X_AXIS: {
			farEdgeOfLeftBoundingBox = new Point3D(splitPositionAlongSplitAxis,
					farEdgeOfBoundingBox.getY(), farEdgeOfBoundingBox.getZ());
			nearEdgeOfRightBoundingBox = new Point3D(
					splitPositionAlongSplitAxis, nearEdgeOfBoundingBox.getY(),
					nearEdgeOfBoundingBox.getZ());
			break;
		}
		case Y_AXIS: {
			farEdgeOfLeftBoundingBox = new Point3D(farEdgeOfBoundingBox.getX(),
					splitPositionAlongSplitAxis, farEdgeOfBoundingBox.getZ());
			nearEdgeOfRightBoundingBox = new Point3D(
					nearEdgeOfBoundingBox.getX(), splitPositionAlongSplitAxis,
					nearEdgeOfBoundingBox.getZ());
			break;

		}
		case Z_AXIS: {
			farEdgeOfLeftBoundingBox = new Point3D(farEdgeOfBoundingBox.getX(),
					farEdgeOfBoundingBox.getY(), splitPositionAlongSplitAxis);
			nearEdgeOfRightBoundingBox = new Point3D(
					nearEdgeOfBoundingBox.getX(), nearEdgeOfBoundingBox.getY(),
					splitPositionAlongSplitAxis);
			break;
		}
		default: {
			throw new IllegalArgumentException("Must have a valid splitAxis");
		}
		}

		LOG.debug("farEdgeOfLeftBoundingBox: {}", farEdgeOfLeftBoundingBox);
		LOG.debug("nearEdgeOfRightBoundingBox: {}", nearEdgeOfRightBoundingBox);

		/*
		 * Retrieves a view of sublist of cloudOfPoints.
		 * 
		 * NOTE: The split point is always assigned to the 'right' child node.
		 * All other points lying on the split plane can either be assigned to
		 * the 'left' or to the 'right' nodes depending on where they end up
		 * after sorting. It is generally quite hard to predict that.
		 */
		List<T> leftCloudOfPoints = cloudOfPoints.subList(0, splitIndex);
		List<T> rightCloudOfPoints = cloudOfPoints.subList(splitIndex,
				cloudOfPoints.size());

		LOG.debug("");
		LOG.debug("left InternalKDNode: ");
		LOG.debug("  leftCloudOfPoints: {}", leftCloudOfPoints);
		LOG.debug("  nearEdgeOfBoundingBox: {}", nearEdgeOfBoundingBox);
		LOG.debug("  farEdgeOfLeftBoundingBox: {}", farEdgeOfLeftBoundingBox);
		LOG.debug("");

		LOG.debug("right InternalKDNode: ");
		LOG.debug("  rightCloudOfPoints: {}", rightCloudOfPoints);
		LOG.debug("  nearEdgeOfRightBoundingBox: {}",
				nearEdgeOfRightBoundingBox);
		LOG.debug("  farEdgeOfBoundingBox: {}", farEdgeOfBoundingBox);
		LOG.debug("");

		LOG.debug("Creating left node");
		KDNode<T> left = constructTree(leftCloudOfPoints,
				nearEdgeOfBoundingBox, farEdgeOfLeftBoundingBox);

		LOG.debug("Creating right node");
		KDNode<T> right = constructTree(rightCloudOfPoints,
				nearEdgeOfRightBoundingBox, farEdgeOfBoundingBox);

		return new InternalKDNode<>(left, right);
	}

	public static <T extends Point3D> int findMaxNumberOfPointsInBoundingBox(
			KDNode<T> root) {

		// used to traverse the tree
		Stack<KDNode<T>> stack = new Stack<>();

		// push the root to top of stack
		stack.push(root);

		// Node in the tree
		KDNode<T> node;

		// Maximum number of points contained in a leaf node
		int maxNumberOfPointsInBoundingBox = Integer.MIN_VALUE;

		// Number of points contained in a leaf node
		int numberOfPointsInLeafNode;

		// Internal node of the tree
		InternalKDNode<T> iNode;

		// Leaf node of the tree
		LeafKDNode<T> lNode;

		// Iterate until all elements of the tree are traversed
		while (!stack.isEmpty()) {

			// get node
			node = stack.pop();

			if (node instanceof LeafKDNode<?>) {
				// if it is a leaf node

				// retrieves the number of points
				lNode = (LeafKDNode<T>) node;
				numberOfPointsInLeafNode = lNode.numberOfPointsInBoundingBox();

				// if more than the current maximum
				if (numberOfPointsInLeafNode > maxNumberOfPointsInBoundingBox) {

					// sets the current maximum to the number of points in the
					// leaf
					maxNumberOfPointsInBoundingBox = numberOfPointsInLeafNode;
				}
			} else {
				// if it is a leaf node
				iNode = (InternalKDNode<T>) node;

				// push right first so that left is on top and is
				// traversed first
				stack.push(iNode.getRight());
				stack.push(iNode.getLeft());
			}
		}

		return maxNumberOfPointsInBoundingBox;
	}

	/**
	 * Provides the 'k' nearest neighbours to the specified MEPPoint.
	 * Implementation is based on description of the algorithm as found in <a
	 * href="https://en.wikipedia.org/wiki/K-d_tree">wiki</a>.
	 * 
	 * @param queryPoint
	 *            the point whose neighbours are sought
	 * @param k
	 *            number of nearest neighbours requested
	 * @return a list containing the 'k' nearest neighbours
	 */
	public ArrayList<T> nearestNeighbours(Point3D queryPoint, int k) {

		// compares points with Euclidean distance metric
		EuclideanDistanceComparator comparator = new EuclideanDistanceComparator(
				queryPoint);

		// Provides fast sorting facilities
		TreeSet<T> closestPoints = new TreeSet<>(comparator);

		// Finds and inserts the closest points to queryMEP
		root.searchNode(closestPoints, queryPoint, k);

		// ArrayLists provides faster access to elements
		return new ArrayList<>(closestPoints);
	}

	public KDNode<T> getRoot() {
		return root;
	}

	/**
	 * Find all points within distance ballRadius of the point centerOfBall.
	 * 
	 * @param ballRadius
	 *            The radius of points to return.
	 * @param centerOfBall
	 *            The point to search for neighbours of.
	 * @return a list containing the nearest neighbours within the distance
	 *         ballRadius of the point centerOfBall.
	 */
	public ArrayList<T> ballSearch(double ballRadius, Point3D centerOfBall) {
		// compares points with Euclidean distance metric
		EuclideanDistanceComparator comparator = new EuclideanDistanceComparator(
				centerOfBall);

		// Provides fast sorting facilities
		TreeSet<T> closestPoints = new TreeSet<>(comparator);

		// Finds and inserts the closest points to queryMEP
		root.searchNode(closestPoints, centerOfBall, ballRadius);

		// ArrayLists provides faster access to elements
		return new ArrayList<>(closestPoints);
	}


	/**
	 * Returns the exact average distance of the '4' nearest neighbours of each
	 * point. Beware when using the method with lists containing large number of
	 * entries as the algorithm is O(n * log(n)). 
	 * 
	 * @param numberOfNearestNeighbours Number of nearest neighbours.
	 * @return Approximate distance
	 */
	public double averageDistanceBetweenPoints(int numberOfNearestNeighbours) {

		// nearest neighbours of a point
		ArrayList<T> nearestNeighbours;

		// average distance between neighbours
		double averageDistance = 0;

		// Iterates over all points
		for (T point : cloudOfPoints) {

			// Retrieve nearest neighbours
			nearestNeighbours = nearestNeighbours(point,
					numberOfNearestNeighbours);

			// Iterates over all nearest neighbours
			for (T nearestNeighbour : nearestNeighbours) {

				// add the distance of each neighbour
				averageDistance += point.distanceTo(nearestNeighbour);
			}
		}

		// divide by total number of distance computed to get average
		averageDistance /= (numberOfNearestNeighbours * numberOfPoints);

		return averageDistance;
	}

	/**
	 * Returns the approximate average distance between elements in the tree.
	 * The method should be used if the number of elements in the tree is large.
	 * An appropriate sample size must be provided depending on the total number
	 * of points, refer to <a
	 * href="https://en.wikipedia.org/wiki/Sample_size_determination"></a> for
	 * more information. The elements comprising the sample are drawn randomly
	 * from a uniform distribution.
	 * 
	 * @param sampleSize Sample size.
	 * @param numberOfNearestNeighbours  Number of nearest neighbours.
	 * @return Approximate average distance between elements in the tree.
	 */
	public double approximateAverageDistanceBetweenPoints(int sampleSize,
			int numberOfNearestNeighbours) {

		// nearest neighbours of a point
		ArrayList<T> nearestNeighbours;

		// average distance between neighbours
		double averageDistance = 0;

		// Number of sampled points
		int numberOfSampledPoints = 0;

		// sampled point from the tree
		T samplePoint;

		// index of point in cloudOfPoints
		int index;

		// generates random numbers.
		Random randomGen = new Random();

		// Iterates until the number of sampled points is equal to the sample
		// size
		while (numberOfSampledPoints != sampleSize) {

			// retrieves a random number between 0 and cloudOfPoints.size()
			index = randomGen.nextInt(cloudOfPoints.size());

			// retrieves the sample point
			samplePoint = cloudOfPoints.get(index);

			// retrieves all nearest neighbours of the sample point
			nearestNeighbours = nearestNeighbours(samplePoint,
					numberOfNearestNeighbours);

			// iterates over all nearest neighbours
			for (T nearestNeighbour : nearestNeighbours) {

				// add the distance of each neighbour
				averageDistance += samplePoint.distanceTo(nearestNeighbour);
			}

			// increases number of sampled points by 1
			numberOfSampledPoints++;
		}

		averageDistance /= sampleSize;
		return averageDistance;
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 31;
			int result = 1;
			result = prime * result + cloudOfPoints.hashCode();
			result = prime * result + maxNumberOfPointsInBoundingBox;
			result = prime * result + numberOfPoints;
			result = prime * result + root.hashCode();
			hash = result;
			hashCodeFound = true;
		}
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KDTree<?> other = (KDTree<?>) obj;
		if (maxNumberOfPointsInBoundingBox != other.maxNumberOfPointsInBoundingBox)
			return false;
		if (numberOfPoints != other.numberOfPoints)
			return false;
		if (!root.equals(other.root))
			return false;
		return true;
	}
}