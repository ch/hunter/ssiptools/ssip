/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Holder class for the Git information of the current build.
 * 
 * @author mw529
 * 
 */
public class GitRepositoryState {

	private static final Logger LOG = LogManager.getLogger(GitRepositoryState.class);
	
	private final String branch;
	private final String describe;
	private final String commitId;
	private final String buildUserName;
	private final String buildUserEmail;
	private final String buildTime;
	private final String commitUserName;
	private final String commitUserEmail;
	private final String commitMessageShort;
	private final String commitMessageFull;
	private final String commitTime;
	private final String describeShort;

	public GitRepositoryState() {
		this.branch = null;
		this.describe = null;
		this.commitId = null;
		this.buildUserName = null;
		this.buildUserEmail = null;
		this.buildTime = null;
		this.commitUserName = null;
		this.commitUserEmail = null;
		this.commitMessageShort = null;
		this.commitMessageFull = null;
		this.commitTime = null;
		this.describeShort = null;
	}

	public GitRepositoryState(Properties properties) {
		this.branch = properties.get("git.branch").toString();
		this.describe = properties.get("git.commit.id.describe").toString();
		this.commitId = properties.get("git.commit.id").toString();
		this.buildUserName = properties.get("git.build.user.name").toString();
		this.buildUserEmail = properties.get("git.build.user.email").toString();
		this.buildTime = properties.get("git.build.time").toString();
		this.commitUserName = properties.get("git.commit.user.name").toString();
		this.commitUserEmail = properties.get("git.commit.user.email")
				.toString();
		this.commitMessageShort = properties.get("git.commit.message.short")
				.toString();
		this.commitMessageFull = properties.get("git.commit.message.full")
				.toString();
		this.commitTime = properties.get("git.commit.time").toString();
		this.describeShort = properties.get("git.commit.id.describe-short").toString();
	}

	public static GitRepositoryState getGitRepositoryState(Class<?> clazz) {
		GitRepositoryState repositoryState = new GitRepositoryState(); 	
		Properties properties = new Properties();
		try {
			LOG.debug("properties path: {}", clazz.getClassLoader().getResource(
						"git.properties").toURI());
			properties.load(clazz.getClassLoader().getResourceAsStream(
						"git.properties"));
			repositoryState = new GitRepositoryState(properties);
		} catch (IOException e) {
			LOG.info("Problem with GitRepositoryState", e);
		} catch (URISyntaxException e) {
			LOG.info("Problem with URISyntax", e);
		}

			
		return repositoryState;
	}

	public String getBranch() {
		return branch;
	}

	public String getDescribe() {
		return describe;
	}

	public String getCommitId() {
		return commitId;
	}

	public String getBuildUserName() {
		return buildUserName;
	}

	public String getBuildUserEmail() {
		return buildUserEmail;
	}

	public String getBuildTime() {
		return buildTime;
	}

	public String getCommitUserName() {
		return commitUserName;
	}

	public String getCommitUserEmail() {
		return commitUserEmail;
	}

	public String getCommitMessageShort() {
		return commitMessageShort;
	}

	public String getCommitMessageFull() {
		return commitMessageFull;
	}

	public String getCommitTime() {
		return commitTime;
	}

	public String getDescribeShort() {
		return describeShort;
	}

}
