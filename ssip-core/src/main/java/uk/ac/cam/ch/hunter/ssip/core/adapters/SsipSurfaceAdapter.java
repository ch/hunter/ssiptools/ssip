/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.adapters;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;

public class SsipSurfaceAdapter extends XmlAdapter<SsipSurfaceAdapter.AdaptedSurfaceMap, Map<String, SsipSurface>> {

	/**
	 * {@link AdaptedSurfaceMap} contains {@link ArrayList} of {@link SsipSurface} objects, ordered by formatted string of isosurface density.
	 * 
	 * @author mdd31
	 *
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class AdaptedSurfaceMap {
		/**
		 * {@link ArrayList} of {@link SsipSurface} objects, ordered by formatted string of isosurface density
		 */
		@XmlElement(name="Surface", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
		public List<SsipSurface> ssipSurfaces = new ArrayList<>();
	}

	@Override
	public Map<String, SsipSurface> unmarshal(AdaptedSurfaceMap adaptedSurfaceMap) throws Exception {
		HashMap<String, SsipSurface> surfaceMap = new HashMap<>();
		for (SsipSurface ssipSurface : adaptedSurfaceMap.ssipSurfaces) {
			String surfaceLabel = MessageFormat.format("{0,number,0.000000}", ssipSurface.getElectronDensityIsosurface().getDensity());
			surfaceMap.put(surfaceLabel, ssipSurface);
		}
		return surfaceMap;
	}

	@Override
	public AdaptedSurfaceMap marshal(Map<String, SsipSurface> surfaceMap) throws Exception {
		AdaptedSurfaceMap adaptedSurfaceMap = new AdaptedSurfaceMap();
		ArrayList<String> surfaceKeys = new ArrayList<>(surfaceMap.keySet());
		Collections.sort(surfaceKeys);
		for (String string : surfaceKeys) {
			adaptedSurfaceMap.ssipSurfaces.add(surfaceMap.get(string));
		}
		return adaptedSurfaceMap;
	}
	
	
}
