/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

/**
 * Class for representing an Atom.
 * 
 * @author tn300
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "atom", namespace = "http://www.xml-cml.org/schema")
public class Atom extends Point3D {

	/**
	 * A constant associated to each atom depending on the functional group it
	 * belongs to.
	 */
	@XmlTransient
	private final double correctFactor;

	/**
	 * The chemical symbol of the atom as given in the periodic table.
	 */
	@XmlAttribute(name = "elementType", namespace = "http://www.xml-cml.org/schema")
	private final AtomDescriptor symbol;

	@XmlAttribute(name = "id", namespace = "http://www.xml-cml.org/schema")
	private final String atomId;

	@XmlAttribute(name = "formalCharge", namespace = "http://www.xml-cml.org/schema")
	private int formalCharge;
	
	/**
	 * This is null constructor required for JAXB marshaling.
	 */
	public Atom() {
		this.symbol = null;
		this.atomId = null;
		this.correctFactor = 1.0;
		this.formalCharge = 0;
	}

	/**
	 * Constructor.
	 * 
	 * @param symbol The chemical symbol of the atom as given in the periodic table.
	 * @param position Position of the Atom.
	 * @param atomId AtomID.
	 */
	public Atom(AtomDescriptor symbol, Point3D position, String atomId) {		
		if (symbol == null || position == null)
			throw new IllegalArgumentException("Null is not allowed!");
		this.x = position.getX();
		this.y = position.getY();
		this.z = position.getZ();
		this.symbol = symbol;
		this.atomId = atomId;
		this.correctFactor = 1.0; // default value
		this.formalCharge = 0; //default value
	}

	/**
	 * Constructor. Also sets correction factor associated with atom.
	 * 
	 * @param symbol The chemical symbol of the atom as given in the periodic table.
	 * @param position Position of the Atom.
	 * @param correctFactor Beta Correction factor; a constant associated to each atom depending on the functional group it
	 * belongs to.
	 * @param atomId AtomID.
	 */
	public Atom(AtomDescriptor symbol, Point3D position, double correctFactor,
			String atomId) {
		if (symbol == null || position == null)
			throw new IllegalArgumentException("Null is not allowed!");
		this.x = position.getX();
		this.y = position.getY();
		this.z = position.getZ();
		this.symbol = symbol;
		this.atomId = atomId;
		this.correctFactor = correctFactor;
		this.formalCharge = 0; //default value
	}

	public AtomDescriptor getSymbol() {
		return symbol;
	}

	public Point3D getPosition() {
		return new Point3D(getX(), getY(), getZ());
	}

	public double getCorrectFactor() {
		return correctFactor;
	}

	public String getAtomId() {
		return atomId;
	}

	public int getFormalCharge() {
		return formalCharge;
	}

	public void setFormalCharge(int formalCharge) {
		this.formalCharge = formalCharge;
	}

	/**
	 * Finds the atom closest to a point in 3D space.
	 * 
	 * @param atoms
	 *            Set of atoms
	 * @param point
	 *            Point in 3D space
	 * @return atom closest to the specified point
	 */
	public static Atom findAtomClosestToPoint(List<Atom> atoms, Point3D point) {
		if (point == null || atoms == null)
			throw new IllegalArgumentException("Null values are not allowed!");

		// Initial values
		double minimumDistance = point.distanceTo(atoms.get(0).getPosition());
		Atom closestAtom = atoms.get(0);

		Point3D atomPosition;

		// temporary variable
		double distance;

		for (Atom atom : atoms) {
			atomPosition = atom.getPosition();
			distance = point.distanceTo(atomPosition);
			if (minimumDistance > distance) {
				minimumDistance = distance;
				closestAtom = atom;
			}
		}

		return closestAtom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(correctFactor);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((getPosition() == null) ? 0 : getPosition().hashCode());
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atom other = (Atom) obj;
		if (Double.doubleToLongBits(correctFactor) != Double
				.doubleToLongBits(other.correctFactor))
			return false;
		if (getPosition() == null) {
			if (other.getPosition() != null)
				return false;
		} else if (!getPosition().equals(other.getPosition())) {
			return false;
		}
		return symbol.equals(other.symbol);
	}

	/**
	 * Example of an atom representation:
	 * 
	 * Chemical symbol: H Position: (1,2,3) Correction factor: 1
	 */
	@Override
	public String toString() {
		return "Chemical symbol: " + symbol.getName() + "\n" + "Position: "
				+ getPosition().toString() + "\n" + "Correction Factor: "
				+ getCorrectFactor() + "\n" + "Atom ID: " + getAtomId() + "\n";
	}
}