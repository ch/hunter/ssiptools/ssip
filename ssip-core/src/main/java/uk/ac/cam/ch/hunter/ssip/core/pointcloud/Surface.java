/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Extracts all disjoint alpha or beta surfaces from the molecular surface.
 * 
 * @author tn300
 */
public class Surface<T extends Point3D> {

	/**
	 * Specifies a radius for surface area estimation in
	 * {@link Surface#findArea()}.
	 */
	public static final double RADIUS = 0.5;

	/**
	 * Number of nearest neighbours used to incrementally construct the disjoint
	 * point cloud surfaces.
	 * 
	 * Note: using a number that is too large here might cause the points at the
	 * edge of the surface to have a nearest neighbour that do not belong to the
	 * surface. This is especially true if the density of points is very low or
	 * the non-uniformity in their distribution is quite large.
	 */
	private static final int NUMBER_OF_NEAREST_NEIGHBOURS = 10;

	/**
	 * Points in space defining the surface.
	 */
	private final ArrayList<T> pointsOnSurface;

	/**
	 * Space partitioning tree structure for efficient lookup of nearest
	 * neighbours.
	 */
	private final KDTree<T> space;

	/**
	 * Area of the surface. This variable is initialized on demand, NOT in
	 * constructor.
	 */
	private double area;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	private int hash;
	private boolean hashCodeFound = false;

	private static final Logger LOG = LogManager.getLogger(Surface.class);

	/**
	 * Constructs a surface defined by <code>pointsOnSurface</code>.
	 * 
	 * @param pointsOnSurface
	 *            points defining the surface.
	 */
	public Surface(ArrayList<T> pointsOnSurface) {

		if (pointsOnSurface == null)
			throw new IllegalArgumentException("Null is not allowed!");
		
		this.pointsOnSurface = pointsOnSurface;
		space = new KDTree<>(pointsOnSurface);

		for (T alphaPoint: pointsOnSurface ){
			LOG.debug("pointsOnSurface {}", alphaPoint);	
		}
	}

	/**
	 * Finds and retrieves disjoint surface patches of <code>this</code>
	 * surface.
	 * 
	 * @return Disjoint surface patches of <code>this</code> surface.
	 */
	public ArrayList<Surface<T>> findPatches() {

		// list of all disjoint patches of calling surface object
		ArrayList<Surface<T>> disjointSurfaces = new ArrayList<>();

		// disjoint patch of calling surface object
		Surface<T> patch;

		// points on a disjoint patch
		ArrayList<T> pointsOnPatch;

		// starting point of surface exploration on each patch
		T seed;

		// iterate over the list while there are still elements
		while (!pointsOnSurface.isEmpty()) {
			// take the first element of the list
			seed = pointsOnSurface.get(0);

			// initialize a patch
			pointsOnPatch = new ArrayList<>();

			// discover the patch 'seed' belongs to
			discoverPatch(seed, space, pointsOnPatch);

			// initializes a surface patch
			patch = new Surface<>(pointsOnPatch);

			// Adds the patch to the list of disjoint surfaces
			disjointSurfaces.add(patch);

			// remove all points belonging to the found patch
			pointsOnSurface.removeAll(pointsOnPatch);
		}

		return disjointSurfaces;
	}

	/**
	 * Implements a recursive algorithm to discover disjoint surface patches on
	 * the molecular surface.
	 * 
	 * @param startingPoint
	 *            serves as a starting point for the method
	 * @param space
	 *            kD tree structure embedded in the space occupied by the
	 *            molecule and its surface.
	 * @param patch
	 *            the surface patch being constructed
	 */
	private static <T extends Point3D> void discoverPatch(T startingPoint,
			final KDTree<T> space, final ArrayList<T> patch) {

		// Adds the point to the patch
		patch.add(startingPoint);

		// Retrieves all neighbours to the starting point
		ArrayList<T> neighbouringPoints = space.nearestNeighbours(
				startingPoint, NUMBER_OF_NEAREST_NEIGHBOURS);

		/*
		 * Iterates over all neighbouring points and recursively calls the
		 * method on those that are not yet matched.
		 */
		for (T neighbouringPoint : neighbouringPoints) {
			if (!patch.contains(neighbouringPoint))
				discoverPatch(neighbouringPoint, space, patch);
		}
	}

	/**
	 * Finds the area of the surface. The specified <code>radius</code> should
	 * be small enough so that the tangent circle defined by <code>radius</code>
	 * and centered at each point, can be thought of as approximately lying on
	 * the surface.
	 * 
	 * @param radius
	 *            the radius of a circle tangent to the surface
	 * @return area of <code>this</code> surface.
	 */
	private double findArea() {

		// number of points within the search radius
		int numberOfPointsWithinRadius;

		// area of a circle tangent to the surface. The circle is assumed to be
		// such that nearby points are assumed to lie on it.
		double areaOfTangentCircle = Math.PI * Math.PI * RADIUS;

		for (T point : pointsOnSurface) {

			// retrieves the number of points within the search radius
			numberOfPointsWithinRadius = space.ballSearch(RADIUS, point).size();

			// calculates the area occupied by the specified point and adds it
			// to the total surface
			area += areaOfTangentCircle / numberOfPointsWithinRadius;

		}

		return area;
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((space == null) ? 0 : space.hashCode());
			hash = result;
			hashCodeFound = true;
		}

		return hash;
	}

	public double getArea() {
		return area != 0 ? area : findArea();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Surface<?> other = (Surface<?>) obj;
		return space.equals(other.space);
	}

	public ArrayList<T> getPointsOnSurface() {
		return pointsOnSurface;
	}

	public KDTree<T> getSpace() {
		return space;
	}
}