/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import java.util.Comparator;
import static java.lang.Math.sqrt;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.ERROR_IN_POSITIONS;

/**
 * A Comparator class used to compare the coordinate of MEPs along a given axis.
 * 
 * @author tn300
 */
public class AxisDistanceComparator<T extends Point3D> implements Comparator<T> {

	/**
	 * This error constant was calculated based on the following assumptions:
	 * <p>
	 * 1) that errors in each coordinate are the same
	 * <p>
	 *  2) that errors in each coordinates are independent
	 * <p>
	 * Refer to wikipedia: <a href="https://en.wikipedia.org/wiki/Propagation_of_uncertainty">
	 * Propagation of uncertainty</a> for more information.
	 * 
	 */
	public static final double ERROR = sqrt(2) * ERROR_IN_POSITIONS;

	/**
	 * Axis along which the comparison is made.
	 */
	private final Axis axis;

	public AxisDistanceComparator(Axis axis) {
		this.axis = axis;
	}

	@Override
	public int compare(Point3D o1, Point3D o2) {
		double o1Coord = o1.getCoord(axis);
		double o2Coord = o2.getCoord(axis);

		return Double.compare(o1Coord, o2Coord);
	}

	public Axis getAxis() {
		return axis;
	}
}