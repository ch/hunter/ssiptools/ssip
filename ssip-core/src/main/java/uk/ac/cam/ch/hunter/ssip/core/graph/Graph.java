/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;


/**
 * Represents an undirected, simple graph structure. An example can be found
 * <a href="http://mathworld.wolfram.com/SimpleGraph.html">here</a>. The graph
 * is comprised of vertices which are connected by edges. To each vertex and
 * each edge can be assigned a label. An edge is uniquely identified, up to
 * permutation, by the two vertices spanning it.
 * 
 * @author tn300
 * 
 * @param <A>
 *            the type of label associated to a vertex
 * @param <B>
 *            the type of label associated to an edge
 */
public class Graph<A, B> {

	/**
	 * All vertices present in the graph.
	 */
	private final ArrayList<Vertex> vertices;

	public Graph() {
		vertices = new ArrayList<>();
	}

	/**
	 * @param v
	 *            a vertex in the graph
	 * @return number of adjacent vertices
	 */
	int findNumberOfAdjacentVertices(Vertex v) {
		// TODO: write a note of caution
		return v.edges.size();
	}

	/**
	 * The method returns an unmodifiable set.
	 * 
	 * @param setOfVertices
	 *            set of vertices in the graph
	 * @return all adjacent vertices of <code>setOfVertices</code> excluding
	 *         <code>setOfVertices</code> itself.
	 */
	Set<Vertex> findSetAdjacentVertices(Set<Vertex> setOfVertices) {
		// TODO: write a note of caution
		Set<Vertex> setAdjacent = new HashSet<>();

		for (Vertex v : setOfVertices) {
			setAdjacent.addAll(v.edges.keySet());
		}

		setAdjacent.removeAll(setOfVertices);
		return setAdjacent;
	}

	/**
	 * The method returns an unmodifiable set.
	 * 
	 * @param v
	 *            a vertex in the graph
	 * @return all adjacent vertices
	 */
	Set<Vertex> findAdjacentVertices(Vertex v) {
		return Collections.unmodifiableSet(v.edges.keySet());
	}

	/**
	 * @param v1
	 *            a vertex present in the graph
	 * @param v2
	 *            another vertex present in the graph different from
	 *            <code>v1</code>
	 * @return <code>true</code> if the <code>v1</code> and <code>v2</code> span
	 *         an edge.
	 */
	boolean hasEdge(Vertex v1, Vertex v2) {
		// TODO: write a note of caution: both are assumed to belong to this
		// graph!
		return v1.edges.containsKey(v2);
	}

	/**
	 * @param v1
	 *            a vertex present in the graph
	 * @param v2
	 *            another vertex present in the graph different from
	 *            <code>v1</code>
	 * @return label of vertex spanned by <code>v1</code> and <code>v2</code>.
	 */
	B getEdgeLabel(Vertex v1, Vertex v2) {
		// TODO: write a note of caution
		return v1.edges.get(v2);
	}

	/**
	 * @param v
	 *            a vertex present in the graph
	 * @return label of <code>v</code>
	 */
	A getVertexLabel(Vertex v) {
		// TODO: write a nato of caution
		return v.vertexLabel;
	}

	/**
	 * @return number of vertices in the graph
	 */
	int getNumberOfVertices() {
		return vertices.size();
	}

	/**
	 * @return all vertices present in the graph
	 */
	public ArrayList<Vertex> getVertices() {
		return vertices;
	}

	/**
	 * Returns a string representation of the graph. The returned string is
	 * closely related to adjacency list representation of a graph (which is
	 * equivalent to connectivity table in a pdb file). It has the general form
	 * illustrated below:
	 * 
	 * <p>
	 * [vertexIndex][vertexLabel] -&gt; [vertexIndex][edgeLabel] [...][...] ...
	 * [vertexIndex][vertexLabel] -&gt; [vertexIndex][edgeLabel] [...][...] ...
	 * etc.
	 * </p>
	 * 
	 */
	@Override
	public String toString() {
		String graphRepresentation = "";

		for (Vertex v : vertices) {
			graphRepresentation += (vertices.indexOf(v) + 1) + v.vertexLabel.toString() + " -> ";

			for (Vertex nv : new TreeSet<>(v.edges.keySet())) {
				graphRepresentation += (vertices.indexOf(nv) + 1) + v.edges.get(nv).toString() + " ";
			}
			graphRepresentation += "\n";
		}

		return graphRepresentation;
	}

	public Vertex addVertex(A vertexlabel) {
		if (vertexlabel == null)
			throw new IllegalArgumentException("'vertexLabel' is null.");
		Vertex v = new Vertex(vertexlabel);
		vertices.add(v);
		return v;
	}

	public void addEdge(Vertex v1, Vertex v2, B edgeLabel) {
		if (v1 == null)
			throw new IllegalArgumentException("'v1' is null");
		if (v2 == null)
			throw new IllegalArgumentException("'v2' is null");
		if (edgeLabel == null)
			throw new IllegalArgumentException("'edgeLabel' is null");
		if (!v1.edges.containsKey(v2)) {
			v1.edges.put(v2, edgeLabel);
			v2.edges.put(v1, edgeLabel);
		}
	}

	public class Vertex implements Comparable<Vertex> {

		private final A vertexLabel;

		private final HashMap<Vertex, B> edges;

		// TODO: must not be initialized explicitly
		Vertex(A vertexLabel) {
			edges = new HashMap<>();
			this.vertexLabel = vertexLabel;
		}

		/**
		 * Returns a representation of a vertex.
		 *
		 * <p>
		 * v1, ..., v7, ..., v12, etc.
		 * </p>
		 */
		@Override
		public String toString() {
			return "v" + (vertices.indexOf(this) + 1);
		}

		@Override
		public int compareTo(Vertex arg0) {
			if (this.equals(arg0))
				return 0;
			else
				return (vertices.indexOf(this) > vertices.indexOf(arg0)) ? 1 : -1;
		}

		public A getVertexLabel() {
			return vertexLabel;
		}
	}
}
