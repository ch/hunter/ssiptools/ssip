/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.X_AXIS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.Y_AXIS;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Axis.Z_AXIS;
import static java.lang.Math.abs;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 * Represents a 3D point.
 * 
 * @author tn300
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Point3D", namespace = "http://www.xml-cml.org/schema")
public class Point3D {

	/**
	 * Error for each point and in each coordinate.
	 */
	@XmlTransient
	public static final double ERROR_IN_POSITIONS = 1e-5;
	@XmlTransient
	private static final String NULL_ERROR_STRING = "Null values are not allowed.";

	@XmlAttribute(name = "x3", namespace="http://www.xml-cml.org/schema")
	protected double x;
	@XmlAttribute(name = "y3", namespace="http://www.xml-cml.org/schema")
	protected double y;
	@XmlAttribute(name = "z3", namespace="http://www.xml-cml.org/schema")
	protected double z;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;

	/**
	 * Constructs (0,0,0) point.
	 */
	public Point3D() {
		this(0, 0, 0);
	}

	/**
	 * Constructs a 3D point with the specified error along each coordinate. The
	 * constructed object is immutable.
	 * 
	 * <p>
	 * The following values for any of the components are not allowed: NAN,
	 * Double.NEGATIVE_INFINITY and Double.POSITIVE_INFINITY.
	 * 
	 * @param x X coordinate.
	 * @param y Y coordinate.
	 * @param z Z coordinate.
	 */
	public Point3D(double x, double y, double z) {
		if (Double.isNaN(x) || Double.isNaN(y) || Double.isNaN(z))
			throw new IllegalArgumentException("One of the arguments is NAN.");
		if (Double.isInfinite(x) || Double.isInfinite(y)
				|| Double.isInfinite(z))
			throw new IllegalArgumentException(
					"One of the arguments is infinite");

		this.x = x;
		this.y = y;
		this.z = z;
	}

	

	/**
	 * Distance squared from <code>this</code> point to <code>point</code>. Useful for
	 * faster computation.
	 * 
	 * @param point Other <code>point</code> to compare with
	 * @return Distance squared from <code>this</code> point to <code>point</code>
	 */
	public double distanceSquaredTo(Point3D point) {
		if (point == null)
			throw new IllegalArgumentException(NULL_ERROR_STRING);
		double diffX = x - point.x;
		double diffY = y - point.y;
		double diffZ = z - point.z;
		return diffX * diffX + diffY * diffY + diffZ * diffZ;
	}

	/**
	 * Distance from <code>this</code> point to <code>point</code>.
	 * 
	 * @param  point Other <code>point</code> to compare with
	 * @return Distance from <code>this</code> point to <code>point</code>
	 */
	public double distanceTo(Point3D point) {
		return Math.sqrt(distanceSquaredTo(point));
	}

	/**
	 * Returns the minimum coordinate.
	 * 
	 * @return Minimum Cartesian component of the vector.
	 */
	public double minCoord() {
		double minXY = x <= y ? x : y;
		return minXY <= z ? minXY : z;
	}

	/**
	 * Returns the maximum coordinate.
	 * 
	 * @return Maximum Cartesian component of the vector.
	 */
	public double maxCoord() {
		double maxXY = x >= y ? x : y;
		return maxXY >= z ? maxXY : z;
	}

	/**
	 * Returns the axis with maximum coordinate. If more than one such axis is
	 * present, returns the axis in order 'x', 'y' and 'z' depending on which of
	 * the axes contain the maximum coordinates. For example if 'x' and 'y' hold
	 * the maximum coordinate, the method returns 'x'. If 'y' and 'z' hold the
	 * maximum coordinate it returns 'y'.
	 * 
	 * @return Cartesian axis with maximum coordinate.
	 */
	public Axis maxCoordAxis() {
		double max = maxCoord();
		if (Math.abs(max - x) < ERROR_IN_POSITIONS)
			return X_AXIS;
		else if (Math.abs(max - y) < ERROR_IN_POSITIONS)
			return Y_AXIS;
		else
			return Z_AXIS;
	}

	/**
	 * Returns the axis with minimum coordinate. If more than one such axis is
	 * present, returns the axis in order 'x', 'y' and 'z' depending on which of
	 * the axes contain the minimum coordinates. For example if 'x' and 'y' hold
	 * the minimum coordinate, the method returns 'x'. If 'y' and 'z' hold the
	 * minimum coordinate it returns 'y'.
	 * 
	 * @return Cartesian axis with minimum coordinate.
	 */
	public Axis minCoordAxis() {
		double min = minCoord();
		if (Math.abs(min - x) < ERROR_IN_POSITIONS)
			return X_AXIS;
		else if (Math.abs(min - y) < ERROR_IN_POSITIONS)
			return Y_AXIS;
		else
			return Z_AXIS;
	}

	/**
	 * Returns the coordinate along the specified <code>axis</code>.
	 * 
	 * @param axis Axis required.
	 * @return Value of specified Cartesian axis within the vector.
	 */
	public double getCoord(Axis axis) {
		if (axis == null)
			throw new IllegalArgumentException(NULL_ERROR_STRING);

		switch (axis) {
		case X_AXIS:
			return x;
		case Y_AXIS:
			return y;
		default:
			return z;
		}
	}

	/**
	 * Returns the squared norm of <code>this</code> point (vector). Useful for
	 * faster computation.
	 * 
	 * @return Squared norm (length) of the vector.
	 */
	public double getNormSquared() {
		return x * x + y * y + z * z;
	}

	/**
	 * Returns the norm of <code>this</code> point(vector).
	 * 
	 * @return Norm (length) of the vector.
	 */
	public double getNorm() {
		return Math.sqrt(getNormSquared());
	}

	/**
	 * Returns a new normalized <code>Point3D</code> object from the coordinates
	 * of <code>this</code> object.
	 * 
	 * @return Normalised vector.
	 */
	public Point3D normalize() {
		double norm = getNorm();
		if (norm > ERROR_IN_POSITIONS)
			return new Point3D(x / norm, y / norm, z / norm);
		else
			throw new IllegalArgumentException(
					"The norm of the vector is too close to zero.");
	}

	/**
	 * Negates the coordinates of the point.
	 * 
	 * @return Negated form of the vector.
	 */
	public Point3D neg() {
		return new Point3D(-x, -y, -z);
	}

	/**
	 * <code>point1</code> - <code>point2</code> component-wise.
	 * 
	 * @param point1 <code>point1</code>
	 * @param point2 <code>point2</code>
	 * @return Vector subtraction of <code>point1</code> from <code>point2</code>. 
	 */
	public static Point3D sub(Point3D point1, Point3D point2) {
		if (point1 == null || point2 == null)
			throw new IllegalArgumentException(NULL_ERROR_STRING);

		return new Point3D(point1.x - point2.x, point1.y - point2.y, point1.z
				- point2.z);
	}

	/**
	 * <code>point1</code> + <code>point2</code> component-wise.
	 * 
	 * @param point1 <code>point1</code>
	 * @param point2 <code>point2</code>
	 * @return Vector addition of <code>point1</code> and <code>point2</code>.  
	 */
	public static Point3D add(Point3D point1, Point3D point2) {
		if (point1 == null || point2 == null)
			throw new IllegalArgumentException(NULL_ERROR_STRING);

		return new Point3D(point1.x + point2.x, point1.y + point2.y, point1.z
				+ point2.z);
	}

	/**
	 * Dot product between vectors corresponding to <code>point1</code> and
	 * <code>point2</code>.
	 * 
	 * @param point1 <code>point1</code>
	 * @param point2 <code>point2</code>
	 * @return Dot product between <code>point1</code> and <code>point2</code>. 
	 */
	public static double dot(Point3D point1, Point3D point2) {
		if (point1 == null || point2 == null)
			throw new IllegalArgumentException(NULL_ERROR_STRING);

		return point1.x * point2.x + point1.y * point2.y + point1.z * point2.z;
	}

	/**
	 * Cross product between vectors corresponding to <code>point1</code> and
	 * <code>point2</code>.
	 * 
	 * @param point1 <code>point1</code>
	 * @param point2 <code>point2</code>
	 * @return Cross product between <code>point1</code> and <code>point2</code>.
	 */
	public static Point3D cross(Point3D point1, Point3D point2) {
		if (point1 == null || point2 == null)
			throw new IllegalArgumentException(NULL_ERROR_STRING);

		return new Point3D(point1.y * point2.z - point1.z * point2.y, point1.z
				* point2.x - point1.x * point2.z, point1.x * point2.y
				- point1.y * point2.x);
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(x);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(y);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(z);
			result = prime * result + (int) (temp ^ (temp >>> 32));

			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point3D other = (Point3D) obj;
		return ((abs(this.x - other.x) < ERROR_IN_POSITIONS) && (abs(this.y - other.y) < ERROR_IN_POSITIONS) && (abs(this.z - other.z) < ERROR_IN_POSITIONS));
	}

	/**
	 * Test to see if distance in X direction is equal between <code>this</code> and <code>point</code>.
	 * 
	 * @param point Other <code>point</code>.
	 * @return x distances equal
	 */
	public boolean isEqualAlongXTo(Point3D point) {
		return abs(x - point.x) < ERROR_IN_POSITIONS;
	}

	/**
	 * Test to see if distance in Y direction is equal between <code>this</code> and <code>point</code>.
	 * 
	 * @param point Other <code>point</code>.
	 * @return y distance equal
	 */
	public boolean isEqualAlongYTo(Point3D point) {
		return abs(y - point.y) < ERROR_IN_POSITIONS;
	}

	/**
	 * Test to see if distance in Z direction is equal between <code>this</code> and <code>point</code>.
	 * 
	 * @param point Other <code>point</code>.
	 * @return z distance equal
	 */
	public boolean isEqualAlongZTo(Point3D point) {
		return abs(z - point.z) < ERROR_IN_POSITIONS;
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ", " + z + ")";
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}
}