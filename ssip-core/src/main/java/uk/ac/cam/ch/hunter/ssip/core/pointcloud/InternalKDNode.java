/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import static java.lang.Math.abs;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.areCollinearToAxis;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.GeometryUtils.areCoplanarToAxis;
import static uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D.sub;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Represents an internal node of a KDTree.
 * 
 * @author tn300
 */
public class InternalKDNode<T extends Point3D> extends KDNode<T> {

	/**
	 * Child node containing points with coordinate along splitDimension which
	 * is less than splitPosition. If parent node is a leaf node this is null.
	 */
	private final KDNode<T> left;

	/**
	 * Child node containing points with coordinate along splitDimension which
	 * is greater than splitPosition. If parent node is a leaf node this is
	 * null.
	 */
	private final KDNode<T> right;

	/**
	 * The axis orthogonal to the splitting plane which separates
	 * <code>left</code> and <code>right</code> nodes.
	 */
	private final Axis splitAxis;

	/**
	 * The intersection of <code>splitAxis</code> with the splitting plane.
	 */
	private final double splitPositionAlongSplitAxis;

	private boolean hashCodeFound = false;
	private int hash;

	/**
	 * Constructs an internal node representing a bounding box spanned by
	 * <code>nearEdgeOfBoundingBox</code> and <code>farEdgeOfBoundingBox</code>.
	 * The node holds references to its two child nodes: <code>left</code> and
	 * <code>right</code>. The <code>left</code> and <code>right</code> child
	 * nodes are disjoint 3D rectangular boxes that share a face and split the
	 * node across an axis-orthogonal plane containing the
	 * <code>farEdgeOfBoundingBox</code> of <code>left</code> and the
	 * <code>nearEdgeOfBoundingBox</code> of <code>right</code>. Thus points
	 * contained in this node are assigned to <code>left</code> or
	 * <code>right</code> bounding boxes depending on whether their coordinate
	 * along the axis which is orthogonal to the splitting plane (the other two
	 * axis lie in the plane) is smaller or bigger than the point of
	 * intersection between the axis and the plane. MEPs that lie on the face
	 * shared by <code>left</code> and <code>right</code> child nodes belong to
	 * both nodes.
	 * 
	 * For more information on KDNode <code>nearEdgeOfBoundingBox</code> and
	 * <code>farEdgeOfBoundingBox</code> check
	 * {@link KDNode#KDNode(Point3D, Point3D)}.
	 * 
	 * @param left
	 *            child node containing points with coordinate along the axis
	 *            orthogonal to the splitting plane smaller than the
	 *            intersection of the axis with that plane
	 * @param right
	 *            child node containing points with coordinate along the axis
	 *            orthogonal to the splitting plane smaller than the
	 *            intersection of the axis with that plane
	 */
	public InternalKDNode(KDNode<T> left, KDNode<T> right) {
		super(nearEdgeRetriever(left), farEdgeRetriever(right));
		if (!areCollinearToAxis(left.getFarEdgeOfBoundingBox(), right.getFarEdgeOfBoundingBox()))
			throw new IllegalArgumentException(
					"'left' and 'right' bounding boxes are incompatible.The far"
							+ " edge of the 'left' bounding box and the far edge of "
							+ "the 'right' bounding box must lie on line parallel "
							+ "to one of the axes.");
		if (!areCollinearToAxis(left.getNearEdgeOfBoundingBox(), right.getNearEdgeOfBoundingBox()))
			throw new IllegalArgumentException(
					"'left' and 'right' bounding boxes are incompatible.The near "
							+ "edge of the 'left' bounding box and the near edge of"
							+ " the 'right' bounding box must lie on line parallel "
							+ "to one of the axes.");

		if (!areCoplanarToAxis(left.getFarEdgeOfBoundingBox(), right.getNearEdgeOfBoundingBox()))
			throw new IllegalArgumentException("'left' and 'right' nodes have to "
					+ "share a plane orthogonal to one of the axes.");

		splitAxis = sub(right.getNearEdgeOfBoundingBox(), left.getNearEdgeOfBoundingBox())
				.maxCoordAxis();
		splitPositionAlongSplitAxis = left.getFarEdgeOfBoundingBox().getCoord(splitAxis);
		this.left = left;
		this.right = right;
	}

	/**
	 * Retrieves the near edge of the specified <code>node</code>. The method is
	 * used in the constructor to safely pass data by checking if
	 * <code>node</code> is null before retrieving the edge. This can't be done
	 * in the constructor as the call to the parent class constructor - super()
	 * has to be the first line in the constructor.
	 * 
	 * @param node
	 * @return
	 */
	private static <T extends Point3D> Point3D nearEdgeRetriever(KDNode<T> node) {
		if (node == null)
			throw new IllegalArgumentException("Null values are not allowed.");

		return node.getNearEdgeOfBoundingBox();
	}

	/**
	 * Retrieves the far edge of the specified <code>node</code>. The method is
	 * used in the constructor to safely pass data by checking if
	 * <code>node</code> is null before retrieving the edge. This can't be done
	 * in the constructor as the call to the parent class constructor - super()
	 * has to be the first line in the constructor.
	 * 
	 * @param node
	 * @return
	 */
	private static <T extends Point3D> Point3D farEdgeRetriever(KDNode<T> node) {
		if (node == null)
			throw new IllegalArgumentException("Null values are not allowed.");

		return node.getFarEdgeOfBoundingBox();
	}

	@Override
	protected void searchNode(TreeSet<T> closestPoints, Point3D queryPoint, int k) {

		// current 'k' closest points to queryMEP
		double queryMEPCoordinateAlongSplitAxis = queryPoint.getCoord(splitAxis);

		// The node neighbouring the one that has already been traversed. The
		// node does NOT contain 'queryMEP' but if 'queryMEP' is close to the
		// splitting plane, the node might contain MEPs closer than the one
		// found in the node containing 'queryMEP'.
		KDNode<T> neighbouringNode;

		// if 'queryMEP' is in 'left' node
		if (queryMEPCoordinateAlongSplitAxis < splitPositionAlongSplitAxis) {

			// search the left node for 'k' closest points
			left.searchNode(closestPoints, queryPoint, k);

			// store that the 'right' node is not searched
			neighbouringNode = right;

		} else { // if 'queryMEP' is in 'right' node

			// search the node
			right.searchNode(closestPoints, queryPoint, k);

			// store that 'left' node is not searched
			neighbouringNode = left;
		}

		// The distance between 'queryMEP' and the most distant member of
		// 'closestMEPs'. If the list is empty Double.Min_Value guarantees that
		// the neighbouring node will be searched.
		double distanceOfFurthestCurrentClosestMep = closestPoints.isEmpty() ? Double.MIN_VALUE
				: closestPoints.last().distanceTo(queryPoint);

		// The distance between 'queryMEP' and the splitting plane.
		// If 'queryMEP' is close to the splitting plane there might be points
		// in 'neigbouringNode' which are closer to 'queryMEP' than the one
		// found in the node containing 'queryMEP'.
		double distanceToSplittingPlane = abs(queryPoint.getCoord(splitAxis)
				- splitPositionAlongSplitAxis);

		// if there are not enough 'closestMEPs' or 'queryMEP' is close to
		// 'neighbouringNode'
		if (closestPoints.size() < k
				|| distanceToSplittingPlane < distanceOfFurthestCurrentClosestMep) {

			// search for MEPs in 'neighbouringNode'
			neighbouringNode.searchNode(closestPoints, queryPoint, k);
		}
	}

	@Override
	protected void searchNode(TreeSet<T> closestPoints, Point3D queryPoint,
			double ballradius) {

		// current 'k' closest points to queryMEP
		double queryMEPCoordinateAlongSplitAxis = queryPoint
				.getCoord(splitAxis);

		// The node neighbouring the one that has already been traversed. The
		// node does NOT contain 'queryMEP' but if 'queryMEP' is close to the
		// splitting plane, the node might contain MEPs closer than the one
		// found in the node containing 'queryMEP'.
		KDNode<T> neighbouringNode;

		// if 'queryMEP' is in 'left' node
		if (queryMEPCoordinateAlongSplitAxis < splitPositionAlongSplitAxis) {

			// search the left node for 'k' closest points
			left.searchNode(closestPoints, queryPoint, ballradius);

			// store that the 'right' node is not searched
			neighbouringNode = right;

		} else { // if 'queryMEP' is in 'right' node

			// search the node
			right.searchNode(closestPoints, queryPoint, ballradius);

			// store that 'left' node is not searched
			neighbouringNode = left;
		}

		// The distance between 'queryMEP' and the splitting plane.
		// If 'queryMEP' is close to the splitting plane there might be points
		// in 'neigbouringNode' which are closer to 'queryMEP' than the one
		// found in the node containing 'queryMEP'.
		double distanceToSplittingPlane = abs(queryPoint.getCoord(splitAxis)
				- splitPositionAlongSplitAxis);

		// 'queryMEP' is close to 'neighbouringNode'
		if (abs(ballradius) > distanceToSplittingPlane) {

			// search for MEPs in 'neighbouringNode'
			neighbouringNode.searchNode(closestPoints, queryPoint, ballradius);
		}

	}

	@Override
	public boolean belongsTo(Point3D queryPoint) {
		if (!containsPoint(queryPoint))
			return false;

		return left.belongsTo(queryPoint) || right.belongsTo(queryPoint);
	}

	@Override
	public int numberOfPointsInBoundingBox() {
		return left.numberOfPointsInBoundingBox() + right.numberOfPointsInBoundingBox();
	}

	@Override
	public ArrayList<T> getPointsInBoundingBox() {
		ArrayList<T> pointsInBoundingBox = new ArrayList<>();
		pointsInBoundingBox.addAll(left.getPointsInBoundingBox());
		pointsInBoundingBox.addAll(right.getPointsInBoundingBox());
		return pointsInBoundingBox;
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + left.hashCode();
			result = prime * result + right.hashCode();
			result = prime * result + splitAxis.hashCode();
			long temp;
			temp = Double.doubleToLongBits(splitPositionAlongSplitAxis);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			hash = result;
			hashCodeFound = true;
		}

		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		InternalKDNode<?> other = (InternalKDNode<?>) obj;
		if (!left.equals(other.left))
			return false;
		if (!right.equals(other.right))
			return false;
		return true;
	}

	public KDNode<T> getLeft() {
		return left;
	}

	public KDNode<T> getRight() {
		return right;
	}

	public Axis getSplitAxis() {
		return splitAxis;
	}

	public double getSplitPositionAlongSplitAxis() {
		return splitPositionAlongSplitAxis;
	}
}