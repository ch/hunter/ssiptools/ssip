/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.surfaceinformation;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.adapters.SsipSurfaceAdapter;

/**
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.NONE)
public class SsipSurfaceInformation {

	@XmlTransient
	private static final Logger LOG = LogManager.getLogger(SsipSurfaceInformation.class);
	
	@XmlTransient
	private static final String VDW_ISOSURFACE_DENSITY = "0.002000";
	
	@XmlElement(name = "Surfaces", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	@XmlJavaTypeAdapter(SsipSurfaceAdapter.class)
	private HashMap<String, SsipSurface> ssipSurfaces;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;
	
	public SsipSurfaceInformation() {
		/**
		 * JAXB constructor.
		 */
	}
	
	public SsipSurfaceInformation(List<SsipSurface> ssipSurfaces) {
		this.ssipSurfaces = new HashMap<>();
		for (SsipSurface ssipSurface : ssipSurfaces) {
			this.ssipSurfaces.put(surfaceLabel(ssipSurface), ssipSurface);
		}
	}
	
	public SsipSurfaceInformation(SsipSurface... ssipSurfaces) {
		this.ssipSurfaces = new HashMap<>();
		for (SsipSurface ssipSurface : ssipSurfaces) {
			this.ssipSurfaces.put(surfaceLabel(ssipSurface), ssipSurface);
		}
	}

	private String surfaceLabel(SsipSurface surface) {
		return MessageFormat.format("{0,number,0.000000}", surface.getElectronDensityIsosurface().getDensity());
	}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SsipSurfaceInformation other = (SsipSurfaceInformation) obj;
		return this.getSsipSurfaces().equals(other.getSsipSurfaces());
	}
	
	@Override
	public int hashCode() {
		return ssipSurfaces.hashCode();
	}

	/**
	 * Volume enclosed by the VdW surface, the 0.002 a.u. electron density isosurface. 
	 * 
	 * @return VdW volume
	 */
	public double getVdWVolume() {
		LOG.debug("keys: {}", getSsipSurfaces().keySet());
		return getSsipSurfaces().get(VDW_ISOSURFACE_DENSITY).getVolumeVdW().getVolume(); 
	}
	
	public double getTotalVdWSurfaceArea() {
		return getSsipSurfaces().get(VDW_ISOSURFACE_DENSITY).getTotalSurfaceArea().getSurfaceArea();
	}
	
	public double getNegativeVdWSurfaceArea() {
		return getSsipSurfaces().get(VDW_ISOSURFACE_DENSITY).getNegativeSurfaceArea().getSurfaceArea();
	}
	
	public double getPositiveVdWSurfaceArea() {
		return getSsipSurfaces().get(VDW_ISOSURFACE_DENSITY).getPositiveSurfaceArea().getSurfaceArea();
	}
	
	/**
	 * This calculates the total number of SSIPs expected, using given SSIP area.
	 * 
	 * @param areaOfSSIP surface area of single SSIP.
	 * @return total number of SSIPs
	 */
	public int getTotalNumberOfSSIPs(double areaOfSSIP) {
		return (int) (Math.round(getSsipSurfaces().get(VDW_ISOSURFACE_DENSITY).getTotalSurfaceArea().getSurfaceArea() / areaOfSSIP));
	}

	/**
	 * This calculates the number of negative SSIPs expected, using given SSIP area.
	 * 
	 * @param areaOfSSIP surface area of single SSIP.
	 * @return number of negative SSIPs
	 */
	public int getNumberOfNegativeSSIPs(double areaOfSSIP) {
		return (int) (Math.round(getSsipSurfaces().get(VDW_ISOSURFACE_DENSITY).getNegativeSurfaceArea().getSurfaceArea() / areaOfSSIP));
	}

	/**
	 * This calculates the number of positive SSIPs expected, using given SSIP area.
	 * This is the difference between the total number and the number of negative
	 * SSIPs.
	 * 
	 * @param areaOfSSIP surface area of single SSIP.
	 * @return number of positive SSIPs
	 */
	public int getNumberOfPositiveSSIPs(double areaOfSSIP) {
		return this.getTotalNumberOfSSIPs(areaOfSSIP) - this.getNumberOfNegativeSSIPs(areaOfSSIP);
	}
	
	public Map<String, SsipSurface> getSsipSurfaces() {
		return ssipSurfaces;
	}

	public void setSsipSurfaces(Map<String, SsipSurface> ssipSurfaces) {
		this.ssipSurfaces =  (HashMap<String, SsipSurface>) ssipSurfaces;
	}
	
	public void setSsipSurfaces(List<SsipSurface> ssipSurfaces) {
		this.ssipSurfaces = new HashMap<>();
		for (SsipSurface ssipSurface : ssipSurfaces) {
			this.ssipSurfaces.put(surfaceLabel(ssipSurface), ssipSurface);
		}
	}
}
