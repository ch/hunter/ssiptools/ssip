/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.Arrays;
import java.util.List;

import uk.ac.cam.ch.hunter.ssip.core.graph.LabelComparator;

/**
 * BondDescriptor class for describing degree of bonding between two atoms.
 * 
 * Based on the description of bond orders in <a href="http://wiki.jmol.org/index.php/Support_for_bond_orders">http://wiki.jmol.org/index.php/Support_for_bond_orders</a>
 * matching CML specified values only.
 * 
 * @author mdd31
 *
 */
public enum BondDescriptor implements LabelComparator<BondDescriptor> {

	WILDCARD {
		@Override
		public String toString() {
			return "W";
		}
	},

	HBOND {
		@Override
		public String toString() {
			return "HB";
		}
	},
	
	PARTIAL {
		@Override
		public String toString() {
			return "PS";
		}
	},
	
	SINGLE {
		@Override
		public String toString() {
			return "S";
		}
	},

	PARTIALDOUBLE {
		@Override
		public String toString() {
			return "PD";
		}
	},
	
	AROMATIC {
		@Override
		public String toString() {
			return "A";
		}
	},
	
	DOUBLE {
		@Override
		public String toString() {
			return "D";
		}
	},

	PARTIALTRIPLE {
		@Override
		public String toString() {
			return "PT";
		}
	},
	
	TRIPLE {
		@Override
		public String toString() {
			return "T";
		}
	};

	@Override
	public boolean consideredEqual(BondDescriptor other) {
		if (other == null)
			return false;

		if (this.equals(WILDCARD) || other.equals(WILDCARD)) {
			return true;
		} else if ((this.equals(AROMATIC) && other.equals(PARTIALDOUBLE)) || (this.equals(PARTIALDOUBLE) && other.equals(AROMATIC)) ) {
			return true;
		} else if ((this.equals(AROMATIC) && (other.equals(SINGLE) || other.equals(DOUBLE))) || ((this.equals(SINGLE) || this.equals(DOUBLE)) && other.equals(AROMATIC))) {
			return true;
		} else {
			return equals(other);
		}
	}

	/**
	 * Provides a list of BondDescriptor in order they are declared.
	 * 
	 * @return List of BondDescriptor in order they are declared.
	 */
	public static List<BondDescriptor> valuesAsList() {
		return Arrays.asList(BondDescriptor.values());
	}
}
