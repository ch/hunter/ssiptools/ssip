/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.pointcloud;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * The class provides a set of methods used to sort and partition lists.
 * 
 * @author tn300
 */
public final class ListUtils {

	private ListUtils() {

	}

	/**
	 * Finds the <code>k</code>-th smallest element in an unordered sublist of
	 * <code>list</code> specified by <code>left</code> and <code>right</code>
	 * indices inclusive. Implements quickselect algorithm. 
	 * 
	 * @see <a href="https://en.wikipedia.org/wiki/Quickselect">Quickselect</a>
	 * 
	 * @param <E> This is the type parameter.
	 * 
	 * @param list
	 *            the list whose median has to be found
	 * @param comparator
	 *            comparator object specifying how <code>list</code> entries are
	 *            compared.
	 * @param left
	 *            left boundary index
	 * @param right
	 *            right boundary index
	 * @param k
	 *            k-th smallest element to be found in <code>list</code>
	 * @return Value of k-th smallest element to be found in <code>list</code>
	 *           
	 */
	public static <E> E quickSelect(List<E> list, Comparator<E> comparator, int left, int right,
			int k) {
		if (list == null)
			throw new IllegalArgumentException(
					"The list is null. A non-null value must be provided.");
		if (list.isEmpty())
			throw new IllegalArgumentException(
					"The list is empty. The median of an empty list is not defined.");
		if (comparator == null)
			throw new IllegalArgumentException(
					"The comparator is null. A non-null value must be provided.");
		if (left > right)
			throw new IllegalArgumentException(
					"'left' index boundary must be smaller than 'right' boundary.");
		if (k < left || k > right)
			throw new IllegalArgumentException(
					"The k-th must be contained inside the boundaries specified by left and right.");
		if (right >= list.size())
			throw new IllegalArgumentException(
					"The right index is out of bounds. The specified list does not conain that many elements.");
		if (left == right)
			return list.get(k);

		// Generates random numbers
		Random randomGen = new Random();
		int pivotIndex;

		while (left != right) {

			// Generates a pivot between left and right
			pivotIndex = left + randomGen.nextInt(right - left);

			/*
			 * Sorts the pivot to its final position. All elements before the
			 * pivot are smaller, all elements after are bigger. Equal elements
			 * may end up on either side.
			 */
			pivotIndex = ListUtils.findPartition(list, comparator, left, right, pivotIndex);

			if (pivotIndex == k)
				/*
				 * if the pivot is sorted to the position of the median.
				 */
				return list.get(k);
			else if (k < pivotIndex)
				/*
				 * if the median is to the left of the pivot, move the right
				 * boundary just before the pivot.
				 */
				right = pivotIndex - 1;
			else
				/*
				 * if the median is to the right of the pivot, move the left
				 * boundary just after the pivot.
				 */
				left = pivotIndex + 1;
		}

		return list.get(k);
	}

	/**
	 * The method places the element at {@code pivotIndex}such that all
	 * elements of the list before that element are smaller in a sense specified
	 * by {@code comparator} and all elements of the list after are bigger
	 * in a sense specified by {@code comparator}. Elements that are
	 * considered equal may end up on either side . For more information on the
	 * algorithm, refer to the <a href="https://en.wikipedia.org/wiki/Quickselect#Algorithm">wiki</a>.
	 * 
	 * @param <E> This is the type parameter.
	 * 
	 * @param list
	 *            the list containing <code>pivotIndex</code>
	 * @param comparator
	 *            comparator object specifying how <code>list</code> entries are
	 *            compared.
	 * @param left
	 *            the index of the left boundary
	 * @param right
	 *            the index of the right boundary
	 * @param pivotIndex
	 *            the pivot to be placed at correct position.
	 * @return the final position of the pivot
	 */
	public static <E> int findPartition(List<E> list, Comparator<E> comparator, int left,
			int right, int pivotIndex) {
		if (list == null)
			throw new IllegalArgumentException(
					"The list is null. A non-null value must be provided.");
		if (list.isEmpty())
			throw new IllegalArgumentException(
					"The list is empty. The median of an empty list is not defined.");
		if (comparator == null)
			throw new IllegalArgumentException(
					"The comparator is null. A non-null value must be provided.");
		if (left > right)
			throw new IllegalArgumentException("The left boundary must be smaller than the right!");
		if (pivotIndex < left || pivotIndex > right)
			throw new IllegalArgumentException(
					"The pivot must be contained inside the boundaries specified by left and right.");
		if (right >= list.size())
			throw new IllegalArgumentException(
					"The right index is out of bounds. The specified list does not conain that many elements.");
		if (right == left)
			return pivotIndex;

		// Retrieves pivot value.
		E pivotValue = list.get(pivotIndex);

		// Swaps pivot with last element. The value of the pivot is now placed
		// at the last element.
		Collections.swap(list, pivotIndex, right);

		// Position of pivot set initially to left-most.
		int storeIndex = left;

		// Iterates over all elements from 'left' to 'right - 1'
		for (int i = left; i <= right - 1; i++) {

			// if pivotValue is greater than current element value
			if (comparator.compare(pivotValue, list.get(i)) == 1) {

				// place the element to the left of the pivot
				Collections.swap(list, storeIndex, i);
				storeIndex++;
			}
		}

		// Last swap placing pivot value at storeIndex, the final place of the
		// pivot
		Collections.swap(list, right, storeIndex);
		return storeIndex;
	}
}