/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.DefaultNamespacePrefixMapper;
import uk.ac.cam.ch.hunter.ssip.core.SsipContainer;
import uk.ac.cam.ch.hunter.ssip.core.SsipMolecule;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;

/**
 * Writer class for an {@link SsipMolecule} object. Provides both a simple output of the
 * SSIP values as a string, as well as output to an XML file.
 */
public class SsipMoleculeWriter {

	private static final Logger LOG = LogManager
			.getLogger(SsipMoleculeWriter.class);
	
	private SsipMoleculeWriter() {

	}

	/**
	 * This returns the values of the SSIPs in the SSIPLList in a simple format,
	 * with the
	 * 
	 * @param ssipList List of SSIPs.
	 * @return String ssipListString
	 */
	public static String writeSSIPList(SsipContainer ssipList) {
		String ssipListString = "";
		ArrayList<Ssip> sortedSSIPs = (ArrayList<Ssip>) ssipList.getSSIPs();
		for (Ssip ssip : sortedSSIPs) {
			String ssipString = SsipWriter.writeSSIP(ssip);
			ssipListString += ssipString + "\n";
		}
		return ssipListString;
	}

	/**
	 * This returns the formated output to the system console.
	 * 
	 * @param ssipList List of SSIPs.
	 */

	public static void returnSSIPListToConsole(SsipContainer ssipList) {
		String ssipListString = writeSSIPList(ssipList);
		System.out.print(ssipListString);
	}

	/**
	 * Method to Marshal an {@link SsipMolecule} to file using JAXB.
	 * 
	 * @param ssipMolecule Input SsipMolecule to marshal.
	 * @param fileName Output XML file.
	 * @throws JAXBException JAXBException
	 */
	public static void  marshal(SsipMolecule ssipMolecule, URI fileName) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(SsipMolecule.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		DefaultNamespacePrefixMapper defaultNamespacePrefixMapper = new DefaultNamespacePrefixMapper();
		Schema ssipSchema = SchemaMaker.createSsipSchema();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", defaultNamespacePrefixMapper);
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", DefaultNamespacePrefixMapper.getSsipXmlHeader());
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemalocationstringssip());
		jaxbMarshaller.setSchema(ssipSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(fileName);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(ssipMolecule, os);

		} catch (FileNotFoundException e) {
			LOG.warn(e);
		}
	}
}
