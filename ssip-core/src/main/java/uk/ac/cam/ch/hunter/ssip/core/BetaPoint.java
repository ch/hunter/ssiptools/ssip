/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

/**
 * A Surface Site Interaction Point (SSIP) representing an hydrogen bond
 * acceptor. This has a negative electrostatic potential and is coloured red by
 * convention.
 *
 * @author mw529
 */
public class BetaPoint extends MepPoint {

	private final double beta;

	public BetaPoint(){
		this.beta = 0;
	}

	public BetaPoint(double x, double y, double z, double ep, Atom closestAtom) {
		super(x, y, z, ep);
		if (ep >= 0)
			throw new IllegalArgumentException(
					"Beta point must have electrostatic potential strictly smaller than zero.");
		beta = calculateBeta(ep, closestAtom);
	}
	
	public BetaPoint(double x, double y, double z, String closestAtomID, double beta){
		super(x, y, z);
		this.beta = beta;
		setNearestAtomID(closestAtomID);
	}

   
	
	
	/**
	 * Calculate a Beta value from its molecular electrostatic potential
	 * value(kJ mol^(−1))
	 * <p>
	 * 
	 * See equation 16 in DOI:10.1039/C3CP53158A
	 * 
	 * Note these are the same values as in PARAMETERS_CUBIC_LO.DAT, but in different
	 * units.
	 * 
	 * # ax^3+bx^2+cx  parameters for conversion of MEP into alpha/beta in Footprint
     * a_beta 0.00
     * b_beta 951.48
     * c_beta -27.56
     * 
	 */
	private double calculateBeta(double mepValue, Atom closestAtom) {
		double result;
	
		result = 0.000138 * Math.pow(mepValue, 2);
		result -= 0.0105 * mepValue;

		return -1.0 * result * closestAtom.getCorrectFactor();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(beta);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BetaPoint other = (BetaPoint) obj;
		if (Math.abs(beta - other.getValue()) < Constants.ERROR_IN_ELECTROSTATIC_POTENTIAL) {
			return super.equals(other);
		} else {
			return false;
		}
	}

	@Override
	public double getValue() {
		return beta;
	}

	@Override
	public String toString() {
		return "Beta value: " + beta + super.toString();
	}
}
