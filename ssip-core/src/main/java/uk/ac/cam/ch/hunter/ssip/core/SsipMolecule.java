/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;

/**
 * Container class. Contains the {@link CmlMolecule}, along with the {@link SsipSurface} and {@link SsipContainer} for a molecule.
 * 
 * TODO: Use this object for visualisation with Jmol.
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SSIPMolecule", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
@XmlType(propOrder = { "cmlMolecule", "ssipSurfaceInformation", "ssipContainer" })
public class SsipMolecule {

	@XmlTransient
	private static final Logger LOG = LogManager
			.getLogger(SsipMolecule.class);
	
	@XmlAttribute(name = "ssipSoftwareVersion", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private final String softwareVersion;
	
	@XmlAttribute(name = "parameterVersion", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private final String parameterVersion;
	
	@XmlElement(name = "molecule", namespace = "http://www.xml-cml.org/schema")
	private final CmlMolecule cmlMolecule;
	
	@XmlElement(name = "SSIPs", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private final SsipContainer ssipContainer;
	@XmlElement(name = "SurfaceInformation", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private final SsipSurfaceInformation ssipSurfaceInformation;
	
	/**
	 * Null constructor for JAXB.
	 */
	public SsipMolecule(){
		this.cmlMolecule = null;
		this.ssipContainer = null;
		this.ssipSurfaceInformation = null;
		this.parameterVersion = null;
		this.softwareVersion = null;
	}
	
	/**
	 * Constructor to generate {@link SsipMolecule}.
	 * 
	 * @param cmlMolecule cmlMolecule.
	 * @param ssipContainer ssipContainer.
	 * @param ssipSurfaceInformation ssipSurfaceInformation.
	 */
	public SsipMolecule(CmlMolecule cmlMolecule, SsipContainer ssipContainer,
			SsipSurfaceInformation ssipSurfaceInformation) {
		this.cmlMolecule = cmlMolecule;
		this.ssipContainer = setnearestAtomID(getCmlMolecule(), ssipContainer);
		this.ssipSurfaceInformation = ssipSurfaceInformation;
		this.parameterVersion = "1.0.0";
		ProjectProperties projectProperties = new ProjectProperties();
		this.softwareVersion = projectProperties.getVersion();
	}

	/**
	 * Constructor to generate {@link SsipMolecule}.
	 * 
	 * @param cmlMolecule cmlMolecule.
	 * @param ssipContainer ssipContainer.
	 * @param ssipSurfaceInformation ssipSurfaceInformation.
	 * @param parameterVersion parameter version 1.0.0 is original, 2.0.0 is pi function
	 */
	public SsipMolecule(CmlMolecule cmlMolecule, SsipContainer ssipContainer,
			SsipSurfaceInformation ssipSurfaceInformation, String parameterVersion) {
		this.cmlMolecule = cmlMolecule;
		this.ssipContainer = setnearestAtomID(getCmlMolecule(), ssipContainer);
		this.ssipSurfaceInformation = ssipSurfaceInformation;
		this.parameterVersion = parameterVersion;
		ProjectProperties projectProperties = new ProjectProperties();
		this.softwareVersion = projectProperties.getVersion();
	}
	

	/**
	 * This ensures that the atomIDs in this match those given in the cml molecule. 
	 * 
	 * @param cmlMolecule cmlMolecule.
	 * @param ssipContainer ssipContainer.
	 */
	private SsipContainer setnearestAtomID(CmlMolecule cmlMolecule, SsipContainer ssipContainer){
		ArrayList<Ssip> ssipWithAtomIdList = new ArrayList<>();
		for (Ssip ssip : ssipContainer.getSSIPs()) {
			Atom nearestAtom = Atom.findAtomClosestToPoint(cmlMolecule.getAtomArrayList(), ssip);
			ssip.setNearestAtomID(nearestAtom.getAtomId());
			ssipWithAtomIdList.add(ssip);
		}
		return new SsipContainer(ssipWithAtomIdList);
	}
	
	public CmlMolecule getCmlMolecule() {
		return cmlMolecule;
	}


	public SsipContainer getSsipContainer() {
		return ssipContainer;
	}


	public SsipSurfaceInformation getSsipSurfaceInformation() {
		return ssipSurfaceInformation;
	}
	
	public String getSoftwareVersion() {
		return softwareVersion;
	}
	
}
