/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.io;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Event handler class for use during validation of XML against Schema.
 * 
 * @author mdd31
 *
 */
public class XmlValidationEventHandler implements ValidationEventHandler{

	private static final Logger LOG = LogManager.getLogger(XmlValidationEventHandler.class);
	/**
	 * Method for handling event when issue with XML is formed.
	 */
	@Override
	public boolean handleEvent(ValidationEvent event) {
        LOG.warn("EVENT");
        LOG.info("SEVERITY:  {}", event.getSeverity());
        LOG.debug("MESSAGE:  {}", event.getMessage());
        if (LOG.isDebugEnabled()) {
        	LOG.debug("LINKED EXCEPTION:  {}", String.valueOf(event.getLinkedException()));
		}
        LOG.trace("LOCATOR");
        LOG.trace("    LINE NUMBER:  {}", event.getLocator().getLineNumber());
        LOG.trace("    COLUMN NUMBER:  {}", event.getLocator().getColumnNumber());
        LOG.trace("    OFFSET:  {}", event.getLocator().getOffset());
        LOG.trace("    OBJECT:  {}", event.getLocator().getObject());
        LOG.trace("    NODE:  {}", event.getLocator().getNode());
        LOG.trace("    URL:  {}", event.getLocator().getURL());
        return true;
    }
}
