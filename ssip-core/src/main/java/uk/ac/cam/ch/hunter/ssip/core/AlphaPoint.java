/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;


/**
 * A Surface Site Interaction Point (SSIP) representing an hydrogen bond donor.
 * This has a positive electrostatic potential and is coloured blue by
 * convention.
 *
 * @author mw529
 */
public class AlphaPoint extends MepPoint {

	private final double alpha;

	public AlphaPoint(){
		this.alpha = 0;
	}

	public AlphaPoint(double x, double y, double z, double ep) {
		super(x, y, z, ep);
		if (ep < 0)
			throw new IllegalArgumentException(
					"Aplha must have electrostatic potential bigger or equal to zero");

		alpha = mepToAlpha(ep);
	}
	/**
	 * Constructor for use when the EP value is not known, i.e. when SSIP is read from a file.
	 * 
	 *TODO- make use of the closestAtom.
	 * 
	 * @param x X coordinate.
	 * @param y Y coordinate.
	 * @param z Z coordinate.
	 * @param closestAtomID ID of the closest Atom to this point.
	 * @param alpha Alpha value.
	 */
	public AlphaPoint(double x, double y, double z, String closestAtomID, double alpha){
		super(x, y, z);
		this.alpha = alpha;
		setNearestAtomID(closestAtomID);
	}
	
	/**
	 * Calculate an Alpha value from its molecular electrostatic potential
	 * value(kJ mol^(−1)).
	 * <p>
	 * 
	 * See equation 15 in DOI:10.1039/C3CP53158A
	 * 
	 * Note these are the same values as in PARAMETERS_CUBIC_LO.DAT, but in different
	 * units.
	 * 
	 * # ax^3+bx^2+cx  parameters for conversion of MEP into alpha/beta in Footprint
     * a_alpha 0.00
     * b_alpha 177.73
     * c_alpha 19.68
     * 
	 */
	private double mepToAlpha(double mepValue) {
		double result;

		result =  0.0000258 * Math.pow(mepValue, 2);
		result += 0.0075 * mepValue;

		return result;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(alpha);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlphaPoint other = (AlphaPoint) obj;
		if (Math.abs(alpha - other.getValue()) < Constants.ERROR_IN_ELECTROSTATIC_POTENTIAL) {
			return super.equals(other);
		} else {
			return false;
		}
		
	}

	@Override
	public double getValue() {
		return alpha;
	}

	@Override
	public String toString() {
		return "Alpha value: " + alpha + super.toString();
	}
}