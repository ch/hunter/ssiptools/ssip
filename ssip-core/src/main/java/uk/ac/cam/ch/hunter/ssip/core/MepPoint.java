/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;


import java.util.Formatter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import uk.ac.cam.ch.hunter.ssip.core.interfaces.INearestAtom;
import uk.ac.cam.ch.hunter.ssip.core.interfaces.IPointProperty;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;


/**
 * The class represents a point in 3D space on the molecular electrostatic
 * potential surface
 * 
 * @author tn300
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ AlphaPoint.class, BetaPoint.class})
public abstract class MepPoint extends Point3D implements INearestAtom, IPointProperty {

	/**
	 * Electrostatic potential in kJ/mol.
	 */
	@XmlTransient
	protected double ep;

	@XmlTransient
	protected double isosurfdensity;
	
	/**
	 * Area associated to that MEP.
	 */
	@XmlTransient
	private double area;

	/**
	 * This is an ID for the closest atom.
	 */
	@XmlAttribute(name = "nearestAtomID", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	protected String nearestAtomID;

	/**
	 * This creates the MEPPoint with an electrostatic potential.
	 * 
	 * @param x x component of the Cartesian.
	 * @param y y component of the Cartesian.
	 * @param z z component of the Cartesian.
	 * @param ep Electrostatic potential in kJ/mol.
	 */
	public MepPoint(double x, double y, double z, double ep) {
		super(x, y, z);
		this.ep = ep;
		this.isosurfdensity = 0.002;
	}

	/**
	 * constructor for when there is no electrostatic potential value associated
	 * with the point, for when the alpha or beta point is read from file.
	 * 
	 * @param x x component of the Cartesian.
	 * @param y y component of the Cartesian.
	 * @param z z component of the Cartesian.
	 */
	public MepPoint(double x, double y, double z) {
		super(x, y, z);
	}

	/**
	 * Null constructor for JAXB.
	 */
	public MepPoint() {
		super();
	}

	@Override
	public Point3D getPosition(){
		return new Point3D(getX(), getY(), getZ());
	}
	
	/**
	 * @return The electrostatic potential in kJ/mol.
	 */
	public double getEp() {
		return ep;
	}

	/**
	 * Provides the value associated to the MEPPoint. For AlphaPoint and
	 * BetaPoint this is alpha and beta values respectively.
	 * 
	 * @return Value of MEP
	 */
	public abstract double getValue();

	

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MepPoint other = (MepPoint) obj;
		return (Math.abs(ep - other.getEp()) <= Constants.ERROR_IN_ELECTROSTATIC_POTENTIAL);
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			int result = super.hashCode();
			final int prime = 31;
			
			long temp;
			temp = Double.doubleToLongBits(ep);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(isosurfdensity);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = nearestAtomID.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			hashCodeFound = true;
			hash = result;
		}
		return hash;
	}

	@Override
	public String toString() {
		return " [point=" + super.toString() + ", potential(Hartrees)=" + ep
				/ Constants.HARTREES_TO_KJMOL + "]";
	}

	/**
	 * Returns a truncated this.getValue() for use in Jmol visualisation.
	 *
	 * @return truncated this.getValue()
	 */
	public String toFormattedString() {
		
		try (Formatter formatter = new Formatter()) {
			return formatter.format("%.2f", this.getValue()).toString();
		}
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		if (area < 0)
			throw new IllegalArgumentException(
					"The assigned area must be positive.");
		this.area = area;
	}

	@Override
	public String getNearestAtomID() {
		return nearestAtomID;
	}

	@Override
	public void setNearestAtomID(String nearestAtomID) {
		this.nearestAtomID = nearestAtomID;
	}
}