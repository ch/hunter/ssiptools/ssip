/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.surfaceinformation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

/**
 * Class for storing the volume enclosed by the associated surface.
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SurfaceVolume {

	@XmlTransient
	static final double ERROR_IN_VOLUME = 1e-7;
	
	@XmlValue()
	private double volume;
	
	@XmlAttribute(name="unit", namespace="http://www-hunter.ch.cam.ac.uk/SSIP")
	private String unit;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;
	
	public SurfaceVolume() {
		
	}
	
	public SurfaceVolume(double volume) {
		this.volume = volume;
		this.unit = "Å^3";
	}
	
	
	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurfaceVolume otherSurfaceVolume =  (SurfaceVolume) obj;
		return Math.abs(this.volume - otherSurfaceVolume.getVolume()) < ERROR_IN_VOLUME && this.getUnit() == otherSurfaceVolume.getUnit();
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(volume);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = unit.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			hashCodeFound = true;
			hash = result;
		}

		return hash;

	}
	
}
