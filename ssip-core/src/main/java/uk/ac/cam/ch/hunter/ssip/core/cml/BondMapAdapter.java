/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.cml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * This Class is for converting the Map containing bond information into a List of POJOs.
 * This allows them to be marshaled by JAXB.   
 * 
 * @author mdd31
 *
 */
public class BondMapAdapter extends XmlAdapter<BondMapAdapter.AdaptedBondMap, Map<String, String>> {
	/**
	 * POJO for representing the BondMap as a list, to aid in marshaling with JAXB.
	 * 
	 * @author mdd31
	 *
	 */
	@XmlAccessorType(XmlAccessType.NONE)
	public static class AdaptedBondMap {
		
		/**
		 * The list of bonds.
		 */
		@XmlElement(name= "bond", namespace = "http://www.xml-cml.org/schema")
		public List<Bond> bondList = new ArrayList<>();

	}
	/**
	 * Bond POJO for representing the bond information, to aid in marshaling.
	 * 
	 * @author mdd31
	 *
	 */
	public static class Bond implements Comparable<Bond>{
		
		/**
		 * The atom ID references for the atoms involved in the bond.
		 */
		@XmlAttribute(name = "atomRefs2", namespace = "http://www.xml-cml.org/schema")
		public String atomrefs;
		/**
		 * Bond order. 
		 */
		@XmlAttribute(name = "order", namespace = "http://www.xml-cml.org/schema")
		public String order;
		
		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			Bond other = (Bond) obj;
			return this.atomrefs.equals(other.atomrefs) && this.order.equals(other.order);
		}
		
		@Override
		public int hashCode() {
			return this.atomrefs.hashCode() * this.order.hashCode();
		}
		
		@Override
		public int compareTo(Bond other){
			String atomRefsOne = atomrefs;
			String atomRefsTwo = other.atomrefs;
			return atomRefsOne.compareTo(atomRefsTwo);
		}
	}
	
	@Override
	public AdaptedBondMap marshal(Map<String, String> bondMap) throws Exception{
		AdaptedBondMap adaptedBondMap = new AdaptedBondMap();
		for (Map.Entry<String, String> bondEntry : bondMap.entrySet()) {
			Bond bond = new Bond();
			bond.atomrefs = bondEntry.getKey();
			bond.order = bondEntry.getValue();
			adaptedBondMap.bondList.add(bond);
		}
		Collections.sort(adaptedBondMap.bondList);
		return adaptedBondMap;
	}
	
	@Override
	public Map<String, String> unmarshal(AdaptedBondMap adaptedBondMap){
		Map<String, String> bondMap = new HashMap<>();
		for (Bond bond : adaptedBondMap.bondList) {
			bondMap.put(bond.atomrefs, bond.order);
		}
		return bondMap;
	}
}
