/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core.cml.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.io.StringWriter;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.DefaultNamespacePrefixMapper;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.io.SchemaMaker;
import uk.ac.cam.ch.hunter.ssip.core.io.XmlValidationEventHandler;

/**
 * Class for writing CML representation of molecule to file. Using JAXB.
 * 
 * @author mdd31
 *
 */
public class CmlWriter {

	private static final Logger LOG = LogManager.getLogger(CmlWriter.class);
	
	private CmlWriter(){

	}

	/**
	 * This marshals the {@link CmlMolecule} to file using JAXB.
	 * 
	 * @param cmlMolecule CmlMolecule
	 * @param fileName Filename to write to 
	 * @throws JAXBException JAXBException
	 */
	public static void marshal(CmlMolecule cmlMolecule, URI fileName) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(CmlMolecule.class);
		Schema cmlSchema = SchemaMaker.createCMLSchema();
		
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", DefaultNamespacePrefixMapper.getXmlHeader());
		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemalocationstringssip());
		jaxbMarshaller.setSchema(cmlSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		try {
			File outputFile = new File(fileName);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(cmlMolecule, os);

		} catch (FileNotFoundException e) {
			LOG.warn(e);
		}
	}

	/**
	 * This marshals the {@link CmlMolecule} to a string using JAXB.
	 *
	 * @param cmlMolecule molecule to write
	 * @throws JAXBException exception
	 * 
	 * @return string containing CML representation.
	 */
	public static String marshalToString(CmlMolecule cmlMolecule) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(CmlMolecule.class);
		Schema cmlSchema = SchemaMaker.createCMLSchema();

		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", DefaultNamespacePrefixMapper.getXmlHeader());
		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
				DefaultNamespacePrefixMapper.getSchemalocationstringssip());
		jaxbMarshaller.setSchema(cmlSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());

		StringWriter sw = new StringWriter();

		jaxbMarshaller.marshal(cmlMolecule, sw);

		return sw.toString();

	}

}
