/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.Sets;

import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;
import uk.ac.cam.ch.hunter.ssip.core.graph.VF2;

/**
 * Represents the chemical structure of the molecule. The structure is described
 * by a graph whose vertices (or nodes) are Atom objects and whose edges are
 * BondDescriptor objects. The class also holds information about all the
 * functional groups present in the molecule.
 * 
 * @author tn300
 * 
 */
public class ChemicalStructure {
	private static final Logger LOG = LogManager.getLogger(ChemicalStructure.class);

	/**
	 * Graph describing the structure of a molecule. The Atom objects contained
	 * in the graph provide a spatial representation of the molecule. The graph
	 * itself describes the topology. Thus the object provides a complete
	 * description of the molecule.
	 */
	private final Graph<AtomDescriptor, BondDescriptor> molTopology;

	/**
	 * Map between a functional group and its isomorphisms in the molecule. Each
	 * isomorphism is one-to-one map between atoms belonging to the functional
	 * group and atoms belonging to the molecule.
	 */
	private final EnumMap<FunctionalGroup, Set<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>>> molGroups;

	/**
	 * Map between vertices and correction factors.
	 */
	Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> cFactors;

	/**
	 * TODO
	 * 
	 * @param molTopology The graph describing the structure of a molecule.
	 */
	public ChemicalStructure(Graph<AtomDescriptor, BondDescriptor> molTopology) {
		if (molTopology == null)
			throw new IllegalArgumentException("Null is not valid input.");

		this.molTopology = molTopology;
		LOG.debug("molTopology is:\n{}", molTopology);

		molGroups = new EnumMap<>(FunctionalGroup.class);
		findFunctionalGroups();
		LOG.debug("molGroups found are: {}", molGroups);

		cFactors = new HashMap<>();
		// Default correction factor of 1 is assigned to all vertices initially.
		for (Graph<AtomDescriptor, BondDescriptor>.Vertex v : molTopology.getVertices()) {
			cFactors.put(v, 1.0);
		}
		refineCFactors();
	}

	/**
	 * Finds all the functional groups present in the graph.
	 */
	private void findFunctionalGroups() {

		initMolGroups();

		removeGroupPermutations();

		// Sorts groups in descending order of preference according to
		// 'FunctionalGroupComparator'.
		ArrayList<FunctionalGroup> funcGroups = new ArrayList<>(molGroups.keySet());

		// Iterators for functional groups and isomorphisms.
		ListIterator<FunctionalGroup> outerGroupIt;
		ListIterator<FunctionalGroup> innerGroupIt;
		Iterator<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>> outerIsomorphIt;
		Iterator<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>> internalIsomorpIt;

		FunctionalGroup outerGroup;
		FunctionalGroup innerGroup;
		HashSet<Graph<AtomDescriptor, BondDescriptor>.Vertex> outerVertices;

		outerGroupIt = funcGroups.listIterator();
		while (outerGroupIt.hasNext()) {
			outerGroup = outerGroupIt.next();

			// Collects all vertices to which the group is mapped.
			outerVertices = new HashSet<>();
			outerIsomorphIt = molGroups.get(outerGroup).iterator();
			while (outerIsomorphIt.hasNext()) {
				outerVertices.addAll(outerIsomorphIt.next().values());
			}

			// Iterates over isomorphisms of groups with lesser preference
			// to determine if there is overlap with 'outerGroup' and removes
			// the isomorphism from the lesser group if that is the case.
			innerGroupIt = funcGroups.listIterator(outerGroupIt.nextIndex());
			while (innerGroupIt.hasNext()) {
				innerGroup = innerGroupIt.next();
				if (!innerGroup.equals(outerGroup)) {
					internalIsomorpIt = molGroups.get(innerGroup).iterator();
					while (internalIsomorpIt.hasNext()) {
						if (!Sets.intersection(internalIsomorpIt.next().values(), outerVertices).isEmpty()) {
							internalIsomorpIt.remove();
						}
					}
				}
			}

			// Checks if a group has been eliminated from the molecule.
			if (molGroups.get(outerGroup).isEmpty()) {
				molGroups.remove(outerGroup);
			}
		}
	}

	/**
	 * Initializes 'molGroups' finding all isomorphisms of functional groups
	 * regardless of whether they overlap.
	 */
	private void initMolGroups() {
		// The algorithm used to find all functional groups.
		VF2<AtomDescriptor, BondDescriptor> algorithm;

		// Populates the map between functional groups and its isomorphisms in
		// the molecule.
		for (FunctionalGroup group : FunctionalGroup.values()) {
			LOG.debug("Checking for group: {}",group);
			algorithm = new VF2<>(group.getChemicalStructure(), molTopology);
			molGroups.put(group, algorithm.findIsomorphisms());
		}
	}

	/**
	 * Removes functional group permutations on the same set of atoms in
	 * molecule.
	 */
	private void removeGroupPermutations() {
		Iterator<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>> it;
		Set<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>> refinedIsomorphisms;

		HashSet<PermutationFilter> filters;
		for (FunctionalGroup group : FunctionalGroup.values()) {
			filters = new HashSet<>();
			refinedIsomorphisms = new HashSet<>();
			it = molGroups.get(group).iterator();
			while (it.hasNext()) {
				filters.add(new PermutationFilter(it.next()));
			}

			for (PermutationFilter p : filters) {
				refinedIsomorphisms.add(p.getIsomorphism());
			}
			molGroups.put(group, refinedIsomorphisms);
		}
	}

	/**
	 * Assigns a correction factor to vertices belonging to a functional group
	 * in the molecule.
	 */
	private void refineCFactors() {

		// Iterates over each group present in the molecule.
		for (FunctionalGroup group : molGroups.keySet()) {

			// Iterates over all isomorphisms of the functional group
			for (BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex> isomorphism : molGroups
					.get(group)) {

				// Iterates over all molecular vertices in the isomorphism.
				for (Graph<AtomDescriptor, BondDescriptor>.Vertex v : isomorphism.values()) {
					cFactors.put(v, group.getCorrectionFactors().get(isomorphism.inverse().get(v)));
				}
			}
		}
	}

	private class PermutationFilter {
		private final BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex> isomorphism;

		public PermutationFilter(
				BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex> isomorphism) {
			this.isomorphism = isomorphism;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((isomorphism == null) ? 0 : isomorphism.values().hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PermutationFilter other = (PermutationFilter) obj;
			if (isomorphism == null) {
				if (other.isomorphism != null)
					return false;
			} else if (!isomorphism.values().equals(other.isomorphism.values())) {
				return false;
			}
			return true;
		}

		public BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex> getIsomorphism() {
			return isomorphism;
		}
	}

	EnumMap<FunctionalGroup, Set<BiMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Graph<AtomDescriptor, BondDescriptor>.Vertex>>> getMolGroups() {
		return molGroups;
	}

	public Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> getCFactors() {
		return cFactors;
	}

	public Graph<AtomDescriptor, BondDescriptor> getMolTopology() {
		return molTopology;
	}
}