/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-core - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.core;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

/**
 * Class implements SSIP comparator. This uses magnitude and distance from the centroid to the molecule to order SSIPs. If the SSIPs still compare equal the Axis distances are used.
 * 
 * @author mdd31
 *
 */
public class SsipComparator implements Comparator<Ssip>{
	
	private static final Logger LOG = LogManager.getLogger(SsipComparator.class);
	
	
	private final Point3D centroid;
	
	/**
	 * Constructor for SSIPs in a molecule- this requires the molecule centroid.
	 * 
	 * @param centroid The molecule's centroid.
	 */
	public SsipComparator(Point3D centroid){
		this.centroid = centroid;
	}
	
	/**
	 * Orders SSIPs by the value of the SSIP, then by distance to centroid.
	 */
	@Override
	public int compare(Ssip o1, Ssip o2) {

		if (LOG.isDebugEnabled()) {
			LOG.debug("SSIP 1:");
			LOG.debug(o1.toString());
			LOG.debug("SSIP 2:");
			LOG.debug(o2.toString());
		}
		
		double o1Ep = o1.getValue();
		double o2Ep = o2.getValue();
		
		LOG.trace("VALUE 1: {}. VALUE 2: {}. ", o1Ep, o2Ep);
		
		if (o1.equals(o2) && o1.getPosition().equals(o2.getPosition())){
			LOG.trace("SSIPs are equal");
			return 0;
		}
		else if (Math.abs(o1Ep - o2Ep) < Constants.ERROR_IN_ELECTROSTATIC_POTENTIAL) {
			//If they have the same value then the one closest to the centroid is ranked higher.
			if (o1.distanceSquaredTo(centroid) < o2.distanceSquaredTo(centroid)) {
				return -1;
			}
			else if (o1.distanceSquaredTo(centroid) > o2.distanceSquaredTo(centroid)) {
				return 1;
			} else {
				if (o1.minCoordAxis().equals(o2.minCoordAxis())){
					if (o1.maxCoordAxis().equals(o2.maxCoordAxis())) {
						 if (o1.minCoord() < o2.minCoord()){
							 return -1;
						 } else {
							 return 1;
						 }
					} else {
						return o1.maxCoordAxis().compareTo(o2.maxCoordAxis());
					}
				} else{
					return o1.minCoordAxis().compareTo(o2.minCoordAxis());
				}
			}
		}
		else if (o1Ep > o2Ep){
			return -1;
		}
		else
			return 1;
		
	}
}
