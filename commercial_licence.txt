Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.

Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:

Cambridge Enterprise Ltd
University of Cambridge
Hauser Forum
3 Charles Babbage Rd
Cambridge CB3 0GT
United Kingdom
Tel: +44 (0)1223 760339
Email: software@enterprise.cam.ac.uk
