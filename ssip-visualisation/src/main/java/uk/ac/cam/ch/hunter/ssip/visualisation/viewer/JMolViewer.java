/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-visualisation - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.visualisation.viewer;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jmol.adapter.smarter.SmarterJmolAdapter;
import org.jmol.api.JmolViewer;

import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.SsipMolecule;
import uk.ac.cam.ch.hunter.ssip.core.cml.io.CmlWriter;

/**
 * Initial attempt at displaying both the molecule and calculated SSIPS using
 * the external Jmol viewer.
 *
 * It uses the simple "Inline String Method":
 * 
 * http://wiki.jmol.org/index.php/Jmol_Cdk_Integration
 *
 * @author mw529
 *
 */
public class JMolViewer {

	private static final Logger LOG = LogManager.getLogger(JMolViewer.class);
	
	String structureString = null;
	List<Ssip> ssipList = null;

	private final boolean originalView;
	
	/**
	 * Scaling facor of SSIPs value to sphere diameter.
	 * Larger value, smaller spheres.
	 */
	static final double SCALE_VALUE = 4;

	/**
	 * Translucency of the SSIPs
	 */
	static final double TRANSLUCENCY = 220;
	
	private static final String DRAW_SSIP = "DRAW ssip";

	public JMolViewer(SsipMolecule ssipMolecule, boolean originalView) {

		try {
			structureString = CmlWriter.marshalToString(ssipMolecule.getCmlMolecule());
		} catch (JAXBException e) {
			LOG.error("JAXB exception: {}", e);
		}

		ssipList = ssipMolecule.getSsipContainer().getSSIPs();
		
		this.originalView = originalView;

	}

	public void show() {

		showStandardSSIPs();

	}

	private void showStandardSSIPs() {
		System.out.println("************** Starting Jmol **************");

		JFrame frame = new JFrame("SSIP Visualiser");
		frame.addWindowListener(new ApplicationCloser());
		frame.setSize(640, 480);

		Container contentPane = frame.getContentPane();
		JmolPanel jmolPanel = new JmolPanel();
		jmolPanel.setPreferredSize(new Dimension(400, 400));

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(jmolPanel);

		jmolPanel.viewer.openStringInline(structureString);

		jmolPanel.viewer.evalString("wireframe 0.1");
		jmolPanel.viewer.evalString("font label 15 bold");
		jmolPanel.viewer.evalString("spacefill 0");
		jmolPanel.viewer.evalString("set useMinimizationThread false");
		jmolPanel.viewer.evalString("color background white");
		jmolPanel.viewer.evalString("set drawFontSize 20");


		int sSIPid = 0;
		for (Ssip sSIP : ssipList) {
			jmolPanel.viewer.evalString(drawSsipValueString(sSIPid, sSIP));
			if (originalView) {
				jmolPanel.viewer.evalString(drawSsipTranslucencyString(sSIPid));
				jmolPanel.viewer.evalString(drawSsipTextValueString(sSIPid, sSIP));
			}
			
			sSIPid++;
		}

		contentPane.add(panel);
		frame.setVisible(true);
	}

	private static String drawSsipValueString(int ssipID, Ssip ssip) {
		// Blue = +ve MEP = Hydrogen bond donor = Alpha
		if (ssip.getValue() > 0) {
			return DRAW_SSIP + ssipID + " TRANSLUCENT COLOR BLUE DIAMETER "
					+ scaleSSIP(ssip.getValue()) + "{" + ssip.getX() + "," + ssip.getY() + "," + ssip.getZ() + "}";
			// Red = -ve MEP = Hydrogen bond acceptor = Beta
		} else {
			return DRAW_SSIP + ssipID + " TRANSLUCENT COLOR RED DIAMETER "
					+ scaleSSIP(ssip.getValue()) + "{" + ssip.getX() + "," + ssip.getY() + "," + ssip.getZ() + "}";
		}
	}
	
	private static String drawSsipTranslucencyString(int ssipID) {
		return DRAW_SSIP + ssipID + " TRANSLUCENT " + TRANSLUCENCY + " ";
	}
	
	private static String drawSsipTextValueString(int ssipID, Ssip ssip) {
		return "DRAW ssipText" + ssipID + "TEXT " + '"' + ssip.toFormattedString() + '"' +  " {" + ssip.getX() + "," + ssip.getY() + "," + ssip.getZ() + "}";
	}
	
	static double scaleSSIP(double ssipValue) {
		return Math.abs(ssipValue) / SCALE_VALUE;
	}

	static class ApplicationCloser extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	}

	static class JmolPanel extends JPanel {
		private static final long serialVersionUID = 1L;
		transient JmolViewer viewer;
		private final Dimension currentSize = new Dimension();

		JmolPanel() {
			viewer = JmolViewer.allocateViewer(this, new SmarterJmolAdapter(), null, null, null, null, null);
		}

		@Override
		public void paint(Graphics g) {
			getSize(currentSize);
			viewer.renderScreenImage(g, currentSize.width, currentSize.height);
		}
	}

}
