/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-visualisation - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.visualisation.cli;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import uk.ac.cam.ch.hunter.ssip.core.GitRepositoryState;
import uk.ac.cam.ch.hunter.ssip.core.SsipMolecule;
import uk.ac.cam.ch.hunter.ssip.core.io.SsipMoleculeReader;
import uk.ac.cam.ch.hunter.ssip.visualisation.viewer.JMolViewer;


public class VisualiseCli {
	
	private static final Logger LOG = LogManager.getLogger(VisualiseCli.class);

	private static final String NAME = "ssip-vis";
	
	private static GitRepositoryState repositoryState = GitRepositoryState.getGitRepositoryState(VisualiseCli.class);
	
	private static final String TITLE = NAME + " - A tool for visualising SSIPs \n"
			+ "       http://dx.doi.org/10.1039/C3CP53158A\n" + "\n" + "       Git version: " + getGitBranch() + " "
			+ getGitVersion();

	private static final String EXAMPLES = ""
			+ "  # Path to the exported example files"
			+ "\n" + "  export ssip_vis_ex_dir=/usr/share/ssip-repo/visualisation/ExampleData" 
			+ "\n"
			+ "\n ssip-vis -r $ssip_vis_ex_dir/water_ssip.xml"
			+ "\n";
	
	
	private static OptionParser optionParser = null;
	private static OptionSet optionSet = null;
	
	private static SsipMolecule ssipMolecule = null;
	
	private VisualiseCli() {
		
	}
	
	/**
	 * main for class- run when compiled.
	 * 
	 * @param args Command line arguments.
	 */
	public static void main(String[] args) {
		// Populate the internal state of the object as directed by the command
		// line options
		parseArguments(args);
		
		displaySSIPs();

	}

	
	private static void parseArguments(String[] args) {

		optionParser = getOptionParser();

		if (args.length == 0) {
			printUsage();
			System.exit(0);
		}
		

		try {
			processArguments(args);
		} catch (IllegalArgumentException e) {
			LOG.info("IllegalArgumentException", e);
		}
	}
	
	/**
	 * Action the command line arguments
	 * 
	 * @param args Command line arguments
	 */
	private static void processArguments(String[] args) {

		optionSet = optionParser.parse(args);

		if (optionSet.has("h")) {
			printUsage();
			System.exit(0);
		}
		
		
		readSSIPs((String) optionSet.valueOf("r"));
		
	
		
	}
	
	private static void readSSIPs(String inputFileName) {
		try {
			File xmlFile = new File(inputFileName);
			
			ssipMolecule = SsipMoleculeReader.demarshal(xmlFile.toURI());
		} catch (JAXBException e) {
			LOG.error("JAXBException: {}", e);
		}

	}
	
	/**
	 * This displays the SSIPs using JMol on the screen.
	 * 
	 * If -v flag given default to old visualisation format of solid colours. If not present, it is shown with translucent spheres and values are displayed. 
	 * 
	 */
	private static void displaySSIPs() {

		JMolViewer viewer = new JMolViewer(ssipMolecule, !optionSet.has("-v"));

		viewer.show();
	}
	
	private static void printUsage() {
		LOG.info("");
		LOG.info(TITLE);
		LOG.info("");
	
		try {
			optionParser.printHelpOn(System.out);
		} catch (IOException e) {
			LOG.info("Cannot print help options to System.out", e);
		}
	
		LOG.info("");
		LOG.info("Example usage:");
		LOG.info(EXAMPLES);
		System.exit(0);
	}
	
	/**
	 * Sets up all the valid options for this command
	 * 
	 * @return OptionParser valid options
	 */
	private static OptionParser getOptionParser() {
		return new VisualiseOptionParser();
	}
	
	private static String getGitVersion() {
		return repositoryState.getCommitId().substring(0, 7);
	}

	private static String getGitBranch() {
		return repositoryState.getBranch();
	}
}
