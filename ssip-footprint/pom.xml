<?xml version="1.0"?>
<!--

    This product is dual-licensed under both the AGPL 3 and a Commercial License.

    ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
    Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses />.

    ======================================================================

    Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.

    Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:

    Cambridge Enterprise Ltd
    University of Cambridge
    Hauser Forum
    3 Charles Babbage Rd
    Cambridge CB3 0GT
    United Kingdom
    Tel: +44 (0)1223 760339
    Email: software@enterprise.cam.ac.uk

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>uk.ac.cam.ch.hunter.ssip</groupId>
		<artifactId>ssip</artifactId>
		<version>8.0.0-SNAPSHOT</version>
	</parent>

	<artifactId>ssip-footprint</artifactId>
	<name>ssip-footprint</name>

	<build>

		<plugins>

			<plugin>
				<groupId>pl.project13.maven</groupId>
				<artifactId>git-commit-id-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>revision</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<dotGitDirectory>${project.basedir}/.git</dotGitDirectory>
					<!-- this is false by default, forces the plugin to generate the git.properties 
						file -->
					<generateGitPropertiesFile>true</generateGitPropertiesFile>
					<!-- The path for the to be generated properties file, it's relative 
						to ${project.basedir} -->
					<generateGitPropertiesFilename>src/main/resources/git.properties</generateGitPropertiesFilename>
				</configuration>
			</plugin>


			<!-- standalone jar with deps; build with mvn clean compile assembly:single -->
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<archive>
						<manifest>
							<mainClass>uk.ac.cam.ch.hunter.ssip.footprint.cli.FootPrintCli</mainClass>
						</manifest>
					</archive>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
			</plugin>




			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>appassembler-maven-plugin</artifactId>
				<configuration>
					<platforms>
						<platform>unix</platform>
					</platforms>

					<programs>
						<program>
							<mainClass>uk.ac.cam.ch.hunter.ssip.footprint.cli.FootPrintCli</mainClass>
							<id>ssip</id>
						</program>
					</programs>

					<!-- Prevent long directories -->
					<repositoryLayout>flat</repositoryLayout>
					<!-- Tell appassembler that src/main/resources is the new src/main/config -->
					<configurationSourceDirectory>src/main/resources</configurationSourceDirectory>
					<!-- Bring across log4j.properties; without this log4j spews everything 
						at runtime -->
					<copyConfigurationDirectory>true</copyConfigurationDirectory>
				</configuration>



				<executions>
					<execution>
						<id>assembly</id>
						<phase>package</phase>
						<goals>
							<goal>assemble</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<!--Tool for building a Debian package (.deb) http://tech.ebuddy.com/2013/10/02/automating-debian-package-creation-and-management-with-mavenant/ 
					https://github.com/brndkfr/xmppbot/blob/master/xmppbot/pom.xml -->
				<artifactId>jdeb</artifactId>
				<groupId>org.vafer</groupId>
				<version>${jdeb.version}</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>jdeb</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<dataSet>
						<data>
							<src>${project.build.directory}/appassembler/</src>
							<type>directory</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/benzene_001_new.pdb</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/benzene_001_0.002_0.00003.isout</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/4-chloroacetophenone.extfix.isout</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/4-chloroacetophenone.infix050.isout</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/4-chloroacetophenone.infix104.isout</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/4-chloroacetophenone.pdb</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/XLYOFNOQVPJJNP-UHFFFAOYSA-N.cml</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0020_merged.cube</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N.cml</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>
						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0104_merged.cube</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>
						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0050_merged.cube</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>
						<data>
							<src>src/test/resources/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0020_merged.cube</src>
							<type>file</type>
							<mapper>
								<type>perm</type>
								<filemode>555</filemode>
								<prefix>/usr/share/ssip-repo/ssip/ExampleData</prefix>
							</mapper>
						</data>

						<data>
							<type>link</type>
							<symlink>true</symlink>
							<linkName>/usr/bin/ssip</linkName>
							<linkTarget>/usr/share/ssip-repo/ssip/bin/ssip</linkTarget>
						</data>

					</dataSet>
				</configuration>
			</plugin>


		</plugins>
	</build>
	<dependencies>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-api</artifactId>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
		</dependency>
		<dependency>
			<groupId>net.sf.jopt-simple</groupId>
			<artifactId>jopt-simple</artifactId>
		</dependency>
		<dependency>
			<groupId>name.mjw</groupId>
			<artifactId>fortranformat</artifactId>
		</dependency>
		<dependency>
			<groupId>${project.groupId}</groupId>
			<artifactId>ssip-core</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>jakarta.xml.bind</groupId>
			<artifactId>jakarta.xml.bind-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.xmlunit</groupId>
			<artifactId>xmlunit-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.xmlunit</groupId>
			<artifactId>xmlunit-matchers</artifactId>
		</dependency>
	</dependencies>
</project>
