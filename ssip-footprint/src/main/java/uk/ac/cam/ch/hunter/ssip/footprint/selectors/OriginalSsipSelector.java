/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint.selectors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import uk.ac.cam.ch.hunter.ssip.core.MepPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Surface;

/**
 * 
 * The original SSIP selection algorithm.
 * <p>
 * Please see pages 18266-7 of http://dx.doi.org/10.1039/c3cp53158a
 * <p>
 * 
 * 1) Read in all the MEP values.
 * <p>
 * 2) Convert values to alpha or beta, with corrections for beta terms. Note
 * their signs are carried over, i.e. betas are explicitly negative, and not
 * intrinsically as a function of their class.
 * <p>
 * 3) Split these into two lists, +ve MEPs are alphas, MEPs -ve are betas.
 * <p>
 * 4) Read in the surface area of the molecule that has already been calculated.
 * <p>
 * 5) The total number of SSIPs is the integer rounded value of the surface area
 * divided by 9.35 A^2 (Equation 1 in DOI:10.1039/C3CP53158A).
 * <p>
 * 6) The number of negative and positive SSIPS is then worked out as a ratio of
 * the number of points in the MEP list.
 * <p>
 * 7) For each MEP list in alpha, beta:
 * <p>
 * 7a) Sort the list by MEP absolute electrostatic potential value, largest to
 * smallest.
 * <p>
 * 7b) Take the first element (via surface.getPointsOnSurface()) and set this to
 * be the first SSIP.
 * <p>
 * 7c) Taking the *next* MEP in this list, calculate its distance from the
 * existing set of SSIPs. If it is greater than 3.2 A (lowerCutOffDistance), add
 * this as a new SSIP.
 * <p>
 * 7d) If the number of SSIPs is less than required, go to 7b, starting from the
 * next element, else, save this a new trialSsipConfiguration
 * <p>
 * 8) For all the trialSsipConfigurations, calculate the sum of alphas or betas.
 * Select the configuration with the highest value as bestSsipConfiguration
 * <p>
 * 9) Replace any existing SSIP with a MEP point that has a electrostatic
 * potential greater the SSIP's and is within a distance of 1.1 A.
 * 
 * @author mw529
 */
public class OriginalSsipSelector<T extends MepPoint> implements ISsipSelector{

	private static final Logger LOG = LogManager.getLogger(OriginalSsipSelector.class);

	/**
	 * The minimum distance (Angstrom) from a set of SSIPs, at which a MEP point can
	 * be considered to be a new SSIP element in this set.
	 */
	private final double lowerCutOffDistance;

	/**
	 * The maximum distance (Angstrom) a MEP, with a greater electrostatic
	 * potential, will dispace a SSIP from a existing set of bestSsipConfiguration.
	 */
	private final double occludedZoneDistance;

	/**
	 * Number of SSIPs to be selected on <code>surface</code>.
	 */
	private final int numberOfSSIPs;

	/**
	 * Represents the surface, i.e. the set of MEPs, on which this algorithm is
	 * executed.
	 */
	private final Surface<T> surface;

	/**
	 * Constructs a SSIP selector.
	 * 
	 * @param surface               Set of MEP points to work on.
	 * @param requiredNumberOfSSIPs Number of SSIPs to abstract from the MEP points.
	 */
	public OriginalSsipSelector(Surface<T> surface, int requiredNumberOfSSIPs, double lowerCutOffDistance, double occludedZoneDistance) {
		if (surface == null)
			throw new IllegalArgumentException("Null values are not allowed.");

		this.surface = surface;
		for (T point3d : this.surface.getPointsOnSurface()) {
			LOG.debug(point3d);
		}
		this.numberOfSSIPs = requiredNumberOfSSIPs;
		this.lowerCutOffDistance = lowerCutOffDistance;
		this.occludedZoneDistance = occludedZoneDistance;

	}

	/**
	 * Calculate the SSIPs.
	 * 
	 * @return List of SSIP for this list of MEPPoints.
	 */
	public List<Ssip> getSSIPs() {

		ArrayList<T> orderedMepsPoints = sortMepPoints();

		ArrayList<Ssip> bestSsipConfiguration = findBestSsipConfiguration(orderedMepsPoints);

		calculateOccludedZone(orderedMepsPoints, bestSsipConfiguration);

		LOG.debug("\nFinal bestSsipConfiguration value is : {}", calculateSSIPsValues(bestSsipConfiguration));
		LOG.debug("Required Number of SSIPs: {}\nActual number of SSIPs: {}", numberOfSSIPs,
				bestSsipConfiguration.size());
		LOG.debug(bestSsipConfiguration);
		LOG.debug("");

		return bestSsipConfiguration;
	}

	/**
	 * Returns the set of SSIPs given a starting index into a *sorted* MEP list and
	 * a required number of SSIPs to find. This is entirely deterministic.
	 * 
	 * @param meps          List of *sorted* MEP values.
	 * @param startMepIndex Starting index of MEP to process.
	 * @param requiredSSIPs Required number of SSIPs to find.
	 * @return The list of SSIPs.
	 */
	private ArrayList<Ssip> getSSIPsFromStartingMepIndex(List<T> meps, int startMepIndex,
			int requiredSSIPs) {

		// Work in distance squared to avoid expensive sqrt()
		double lowerCutOffDistanceSquared = this.lowerCutOffDistance * this.lowerCutOffDistance;

		// The final set of SSIPs
		ArrayList<Ssip> sSIPs = new ArrayList<>();

		// Add (and cast) the first MEP to be the first SSIP
		sSIPs.add(Ssip.fromMepPoint(meps.get(startMepIndex)));
		int foundSSIPs = 1;

		// Starting index in MEP list
		for (int j = startMepIndex + 1; j < meps.size(); j++) {
			if (foundSSIPs == requiredSSIPs) {
				LOG.trace("Found all of the required SSIPs ({})", requiredSSIPs);
				break;
			}

			// Is this current MEP outside of min distance (lowerCutOffDistance)
			if (meps.get(startMepIndex).distanceSquaredTo(meps.get(j)) > lowerCutOffDistanceSquared) {

				// Now check distance against ALL existing SSIPs
				boolean isOutsideDistanceRangeOfExistingSSIPs = true;
				for (int k = 0; k < sSIPs.size(); k++) {
					if (sSIPs.get(k).distanceSquaredTo(meps.get(j)) < lowerCutOffDistanceSquared) {
						isOutsideDistanceRangeOfExistingSSIPs = false;
					}
				}

				// OK, lets accept this MEP as a new SSIP into our existing set
				if (isOutsideDistanceRangeOfExistingSSIPs) {
					sSIPs.add(Ssip.fromMepPoint(meps.get(j)));
					foundSSIPs = foundSSIPs + 1;
				}
				// Move onto next MEP in list
				continue;
			}

		}

		if (LOG.isTraceEnabled() && foundSSIPs != requiredSSIPs) {
			LOG.trace("Only found {} of the required {} SSIPs ", foundSSIPs, requiredSSIPs);
		}
		LOG.debug("Found SSIPs: {} Required SSIPs {}", foundSSIPs, requiredSSIPs);
		LOG.trace("SSIPs are: {}", sSIPs);
		return sSIPs;
	}

	/**
	 * Given a list of SSIPs, calculate the sum of their alpha or beta values. This
	 * is a fitness function for deciding which specific set of SSIPs best describe
	 * the entire MEP surface.
	 * 
	 * @param ssips List of SSIPs to evaluate.
	 * @return Sum of alpha or beta values.
	 */
	private double calculateSSIPsValues(ArrayList<Ssip> ssips) {
		double value = 0.0;
		for (Ssip ssip : ssips) {
			value += ssip.getValue();
		}
		return value;
	}

	private ArrayList<Ssip> findBestSsipConfiguration(ArrayList<T> orderedMepsPoints) {
		ArrayList<Ssip> trialSsipConfiguration;
		ArrayList<Ssip> bestSsipConfiguration = new ArrayList<>();

		for (int i = 0; i < orderedMepsPoints.size(); i++) {

			LOG.trace("trialSsipConfiguration #{}", i);

			trialSsipConfiguration = getSSIPsFromStartingMepIndex(orderedMepsPoints, i, numberOfSSIPs);

			LOG.trace("trialSsipConfiguration #{} total value is : {}", i,
					calculateSSIPsValues(trialSsipConfiguration));

			if (Math.abs(calculateSSIPsValues(trialSsipConfiguration)) > Math
					.abs(calculateSSIPsValues(bestSsipConfiguration))) {
				bestSsipConfiguration = trialSsipConfiguration;
				LOG.debug(" new bestSsipConfiguration (total {}): {}", calculateSSIPsValues(bestSsipConfiguration),
						bestSsipConfiguration);
			}
		}
		return bestSsipConfiguration;
	}

	/**
	 * 
	 * Given a configuration of SSIPs, calculated from a parent set of MEPs, this
	 * will replace any existing SSIP with a MEP point that has a electrostatic
	 * potential greater the SSIP's and is within a distance of 1.1 A.
	 * 
	 * Please see second paragraph on page 18267 of
	 * http://dx.doi.org/10.1039/c3cp53158a
	 * 
	 * @param orderedMEPsPoints
	 * @param bestSsipConfiguration
	 */
	private void calculateOccludedZone(ArrayList<T> orderedMEPsPoints, ArrayList<Ssip> bestSsipConfiguration) {

		final double HARTREES_TO_KJMOL = 2625.49962d;
		final double epsilon = Math.ulp(HARTREES_TO_KJMOL);

		LOG.debug("\ncalculateOccludedZone()");

		for (int i = 0; i < bestSsipConfiguration.size(); i++) {
			for (int j = 0; j < orderedMEPsPoints.size(); j++) {

				if (Math.abs(orderedMEPsPoints.get(j).getEp()) < Math
						.abs(bestSsipConfiguration.get(i).getEp() + epsilon)) {
					break;
				}
				if (orderedMEPsPoints.get(j).distanceSquaredTo(
						bestSsipConfiguration.get(i)) > ((occludedZoneDistance * occludedZoneDistance) - epsilon)) {
					continue;
				}
				LOG.debug(" j.getEp(): {}", orderedMEPsPoints.get(j).getEp());
				LOG.debug(" i.getEp(): {}", bestSsipConfiguration.get(i).getEp());
				LOG.debug(" i <--> j distance: {}", orderedMEPsPoints.get(j).distanceTo(bestSsipConfiguration.get(i)));

				LOG.debug("calculateOccludedZone{neg,pos}(): removing (i) {}", bestSsipConfiguration.get(i));
				LOG.debug("calculateOccludedZone{neg,pos}(): adding (j) {}", orderedMEPsPoints.get(j));
				bestSsipConfiguration.set(i, Ssip.fromMepPoint(orderedMEPsPoints.get(j)));

				break;

			}
		}
	}

	/**
	 * Sort each MEP point so that the largest absolute value is first, and the
	 * smallest absolute is last.
	 * 
	 * @return sorted Ordered list of MEP points, largest absolute first.
	 */
	private ArrayList<T> sortMepPoints() {
		ArrayList<T> orderedMepsPoints = surface.getPointsOnSurface();

		Comparator<T> comparator = new Comparator<T>() {

			@Override
			public int compare(T o1, T o2) {

				double o1Ep = Math.abs((o1).getEp());
				double o2Ep = Math.abs((o2).getEp());

				if (o1Ep > o2Ep)
					return -1;

				if (o1Ep < o2Ep)
					return 1;

				else
					return 0;
			}
		};

		Collections.sort(orderedMepsPoints, comparator);
		return orderedMepsPoints;
	}
}