/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint.cli;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import uk.ac.cam.ch.hunter.ssip.core.ChemicalStructure;
import uk.ac.cam.ch.hunter.ssip.core.GitRepositoryState;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.SsipContainer;
import uk.ac.cam.ch.hunter.ssip.core.SsipMolecule;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.footprint.Molecule;
import uk.ac.cam.ch.hunter.ssip.core.cml.io.CmlReader;
import uk.ac.cam.ch.hunter.ssip.footprint.io.IsoOutReader;
import uk.ac.cam.ch.hunter.ssip.footprint.parameters.IFootprintParameters;
import uk.ac.cam.ch.hunter.ssip.footprint.parameters.OriginalParameters;
import uk.ac.cam.ch.hunter.ssip.footprint.parameters.Parameters;
import uk.ac.cam.ch.hunter.ssip.footprint.selectors.SelectorEnum;
import uk.ac.cam.ch.hunter.ssip.core.io.SsipMoleculeReader;
import uk.ac.cam.ch.hunter.ssip.core.io.SsipMoleculeWriter;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;

/**
 * Stand alone CLI tool for footprinting.
 * 
 * @author mjw
 * 
 */
public class FootPrintCli {

	private static final Logger LOG = LogManager.getLogger(FootPrintCli.class);

	private static final String NAME = "ssip";

	private static GitRepositoryState repositoryState = GitRepositoryState.getGitRepositoryState(FootPrintCli.class);
	
	private static final String TITLE = NAME + " - A tool for footprinting molecular electrostatic potential surfaces\n"
			+ "       http://dx.doi.org/10.1039/C3CP53158A\n" + "\n" + "       Git version: " + getGitBranch() + " "
			+ getGitVersion();

	private static final String EXAMPLES = ""
			+ "  # Path to the exported example files"
			+ "\n" + "  export ssip_ex_dir=/usr/share/ssip-repo/ssip/ExampleData" 
			+ "\n" + "\n" + "  # using the quadratic beta function" 
			+ "\n" + "  " + NAME
			+ " -m $ssip_ex_dir/XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0020_merged.cube -c $ssip_ex_dir/XLYOFNOQVPJJNP-UHFFFAOYSA-N.cml -w water.xml"
			+ "\n"
			+ "\n" + "\n" + "  # using the pi function"
			+ "\n" + "  " + NAME
			+ " --legacy -t -i $ssip_ex_dir/4-chloroacetophenone.extfix.isout -g $ssip_ex_dir/4-chloroacetophenone.infix050.isout -f $ssip_ex_dir/4-chloroacetophenone.infix104.isout -p $ssip_ex_dir/4-chloroacetophenone.pdb -w 4-chloroacetophenone.xml"
			+ "\n" + "  " + NAME
			+ " -t -m $ssip_ex_dir/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0020_merged.cube -g $ssip_ex_dir/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0050_merged.cube -f $ssip_ex_dir/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0104_merged.cube -c $ssip_ex_dir/UHOVQNZJYSORNB-UHFFFAOYSA-N.cml -w UHOVQNZJYSORNB-UHFFFAOYSA-N.xml"
			+ "\n";

	private static final double DEFAULT_ISOSURFACE_DENSITY = 0.002;
	private static final double GRAD_ISOSURFACE_DENSITY = 0.005;
	private static final double FOOTPRINT_ISOSURFACE_DENSITY = 0.0104;
	
	private static OptionParser optionParser = null;
	private static OptionSet optionSet = null;

	
	private static IFootprintParameters footprintParameters =null;
	private static IsoOutReader isoOut = null;
	private static IsoOutReader gradInnerIso = null;
	private static IsoOutReader fpInnerIso = null;
	private static CmlMolecule cmlMolecule = null;
	private static boolean usePi = false;
	private static SsipMolecule ssipMolecule = null;

	
	
	private FootPrintCli() {

	}

	/**
	 * main for class- run when compiled.
	 * 
	 * @param args Command line arguments.
	 */
	public static void main(String[] args) {
		// Populate the internal state of the object as directed by the command
		// line options
		parseArguments(args);
		
		if (optionSet.has("n")) {
			printSSIPsToConsole();
		} else if (optionSet.has("w")) {
			try {
				writeSSIPs((String) optionSet.valueOf("w"));
			} catch (JAXBException e) {
				LOG.error(e);
			}
		} else {
			LOG.info("no output option specified");
			System.exit(0);
		}

	}

	private static void parseArguments(String[] args) {

		optionParser = getOptionParser();

		if (args.length == 0) {
			printUsage();
			System.exit(0);
		}
		

		try {
			processArguments(args);
		} catch (IllegalArgumentException e) {
			LOG.info("IllegalArgumentException", e);
		}
	}

	/**
	 * Action the command line arguments
	 * 
	 * @param args Command line arguments
	 */
	private static void processArguments(String[] args) {

		optionSet = optionParser.parse(args);

		if (optionSet.has("h")) {
			printUsage();
			System.exit(0);
		}
		
		if (optionSet.has("r")) {
			readSSIPs((String) optionSet.valueOf("r"));
		} else {
			usePi = optionSet.has("t");
			LOG.trace("use pi: {}", usePi);
			parseParameters();
			parseFiles();
			
			calculateSSIPs();
		}
	
		
	}

	/**
	 * Sets up all the valid options for this command
	 * 
	 * @return OptionParser valid options
	 */
	private static OptionParser getOptionParser() {
		return new FootprintOptionParser();
	}

	private static void printUsage() {
		LOG.info("");
		LOG.info(TITLE);
		LOG.info("");
	
		try {
			optionParser.printHelpOn(System.out);
		} catch (IOException e) {
			LOG.info("Cannot print help options to System.out", e);
		}
	
		LOG.info("");
		LOG.info("Example usage:");
		LOG.info(EXAMPLES);
		System.exit(0);
	}

	private static void parseParameters() {
		if (optionSet.has("params")) {
			footprintParameters = parseParameterValues();
		} else {
			footprintParameters = new OriginalParameters();
		}
	}
	
	private static Parameters parseParameterValues() {
		OriginalParameters originalParameters = new OriginalParameters();
		double occludedPositiveZoneDistance;
		double occludedNegativeZoneDistance;
		
		double areaOfSSIP = (double) optionSet.valueOf("surfacearea");
		double lowerPositiveCutOffDistance = optionSet.has("dpos") ? (double) optionSet.valueOf("dpos"): (double) optionSet.valueOf("dtotal");
		double lowerNegativeCutOffDistance = optionSet.has("dneg") ? (double) optionSet.valueOf("dneg"): (double) optionSet.valueOf("dtotal");
		
		if (optionSet.has("rscale")) {
			occludedPositiveZoneDistance = lowerPositiveCutOffDistance * (originalParameters.getPositiveOccludedZoneDistance() / originalParameters.getPositiveLowerCutOffDistance());
			occludedNegativeZoneDistance = lowerNegativeCutOffDistance * (originalParameters.getNegativeOccludedZoneDistance() /originalParameters.getNegativeLowerCutOffDistance());
		} else {
			occludedPositiveZoneDistance = optionSet.has("rpos") ? (double) optionSet.valueOf("rpos"): (double) optionSet.valueOf("rtotal");
			occludedNegativeZoneDistance = optionSet.has("rneg") ? (double) optionSet.valueOf("rneg"): (double) optionSet.valueOf("rtotal");
		}
		
		return new Parameters(lowerPositiveCutOffDistance, lowerNegativeCutOffDistance, occludedPositiveZoneDistance, occludedNegativeZoneDistance, areaOfSSIP);
	}
	
	private static void parseFiles() {
		LOG.debug("supplied cube file name is: {}", optionSet.valueOf("m"));

		File cubeFile = new File((String) optionSet.valueOf("m"));
		URI cubeUri = cubeFile.toURI();

		LOG.debug("supplied cml file name is: {}", optionSet.valueOf("c"));

		File cmlFile = new File((String) optionSet.valueOf("c"));
		URI cmlUri = cmlFile.toURI();

		try {
			cmlMolecule = CmlReader.demarshal(cmlUri);
			isoOut = new IsoOutReader(cubeUri, DEFAULT_ISOSURFACE_DENSITY);
		} catch (JAXBException e) {
			LOG.info("JAXBException: ", e);
		}
		
		if (usePi){
			// If the differential MEPS and pi-function are being used, 
			// two additional files are required
			File gradInnerIsoFile = new File((String) optionSet.valueOf("g"));
			LOG.debug("gradient surface file: {}", optionSet.valueOf("g"));
			URI gradInnerIsoUri = gradInnerIsoFile.toURI();
			gradInnerIso = new IsoOutReader(gradInnerIsoUri, GRAD_ISOSURFACE_DENSITY);
			
			File fpInnerIsoFile = new File((String) optionSet.valueOf("f"));
			URI fpInnerIsoUri = fpInnerIsoFile.toURI();
			fpInnerIso = new IsoOutReader(fpInnerIsoUri, FOOTPRINT_ISOSURFACE_DENSITY);
		}

	}

	private static void readSSIPs(String inputFileName) {
		try {
			File xmlFile = new File(inputFileName);
			
			ssipMolecule = SsipMoleculeReader.demarshal(xmlFile.toURI());
		} catch (JAXBException e) {
			LOG.error("JAXBException: {}", e.getMessage());
		}

	}

	private static void calculateSSIPs() {
		SsipSurfaceInformation sSIPSurfaceInformation;

		Molecule molecule;
		
		ChemicalStructure topology = new ChemicalStructure(cmlMolecule.getMolTopology());
		if (usePi) {
				sSIPSurfaceInformation = new SsipSurfaceInformation(isoOut.getSurfaceInformation(),
						gradInnerIso.getSurfaceInformation(), fpInnerIso.getSurfaceInformation());
				molecule = new Molecule(topology, cmlMolecule.getMolPositions(), isoOut.getMepsData(),
						fpInnerIso.getMepsData(), gradInnerIso.getMepsData(), 
						sSIPSurfaceInformation, footprintParameters);
			} else {
				sSIPSurfaceInformation = new SsipSurfaceInformation(isoOut.getSurfaceInformation());
				molecule = new Molecule(topology, cmlMolecule.getMolPositions(), isoOut.getMepsData(),
						sSIPSurfaceInformation, footprintParameters);
			}
		

		SsipContainer ssipList = new SsipContainer((ArrayList<Ssip>) molecule.findSSIPs(SelectorEnum.valueOf((String) optionSet.valueOf("selector"))));

		if (usePi) {
			ssipMolecule = new SsipMolecule(cmlMolecule, ssipList, sSIPSurfaceInformation, "2.0.0");
		} else {
			ssipMolecule = new SsipMolecule(cmlMolecule, ssipList, sSIPSurfaceInformation);
		}
	}

	private static void printSSIPsToConsole() {
		SsipMoleculeWriter.returnSSIPListToConsole(ssipMolecule.getSsipContainer());
	}

	private static void writeSSIPs(String outputFilename) throws JAXBException {
		File outputFile = new File(outputFilename);
		URI xmlFilename = outputFile.toURI();
		SsipMoleculeWriter.marshal(ssipMolecule, xmlFilename);
	}

	private static String getGitVersion() {
		return repositoryState.getCommitId().substring(0, 7);
	}

	private static String getGitBranch() {
		return repositoryState.getBranch();
	}

}
