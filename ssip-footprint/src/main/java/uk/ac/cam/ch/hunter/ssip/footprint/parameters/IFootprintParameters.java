/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint.parameters;

public interface IFootprintParameters {

	/**
	 * This is the smallest separation between two positive SSIPs that is allowed. A SSIP contact blocks any further contacts within this radius.
	 * 
	 * @return positive lower cut off distance
	 */
	double getPositiveLowerCutOffDistance();

	/**
	 * This is the smallest separation between two negative SSIPs that is allowed. A SSIP contact blocks any further contacts within this radius.
	 * 
	 * @return negative lower cut off distance
	 */
	double getNegativeLowerCutOffDistance();
	
	/**
	 * This is the contact region on the surface of an SSIP. A SSIP contact occurs
	 * over a small region instead of a single point.
	 * 
	 * @return positive occluded zone radius
	 */
	double getPositiveOccludedZoneDistance();
	
	/**
	 * This is the contact region on the surface of an SSIP. A SSIP contact occurs
	 * over a small region instead of a single point.
	 * 
	 * @return positive occluded zone radius
	 */
	double getNegativeOccludedZoneDistance();

	/**
	 * This is the area of the SSIPs used to determine number of SSIPs to find.
	 * 
	 * @return
	 */
	double getAreaOfSSIP();

}