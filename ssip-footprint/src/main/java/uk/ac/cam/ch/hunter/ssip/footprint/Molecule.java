/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.BondDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.CentroidCalculator;
import uk.ac.cam.ch.hunter.ssip.core.ChemicalStructure;
import uk.ac.cam.ch.hunter.ssip.core.MepPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.SsipComparator;
import uk.ac.cam.ch.hunter.ssip.footprint.parameters.IFootprintParameters;
import uk.ac.cam.ch.hunter.ssip.footprint.selectors.ISsipSelector;
import uk.ac.cam.ch.hunter.ssip.footprint.selectors.OriginalSsipSelector;
import uk.ac.cam.ch.hunter.ssip.footprint.selectors.SelectorEnum;
import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.KDTree;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Surface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;

public class Molecule {

	private static final Logger LOG = LogManager.getLogger(Molecule.class);

	/**
	 * Equation 1 in DOI:10.1039/C3CP53158A in Angstroms squared.
	 */
	public final IFootprintParameters footprintParameters;

	/**
	 * Error in area estimate of the molecular surface.
	 */
	public static final double ERROR_IN_AREA = 1e-7;

	/**
	 * Error in estimated volume of the molecule.
	 */
	public static final double ERROR_IN_VOLUME = 1e-7;

	/**
	 * Error in estimated energy of the molecule.
	 */
	public static final double ERROR_IN_ENERGY = 1e-7;
	
	/**
	 * Absolute value of the minimum Esp value a beta point
	 * can have on the footprinting surface (not an actual Esp,
	 * but as a result of padding)
	 */	
	public static final double MIN_NEG_FP_ESP = 0.01;

		/**
	 * Represents the chemical structure of the molecule.
	 */
	private final ChemicalStructure topology;

	/**
	 * Contains information on all the areas and volume for the molecule corresponding to each surface. Also used to calculate the number of SSIPs of each type.
	 */
	private final SsipSurfaceInformation ssipSurfaceInformation;
	
	/**
	 * A list of points on the molecular surface with positive electrostatic
	 * potential and their corresponding alpha values.
	 */
	private final Surface<AlphaPoint> alphaSurface;

	/**
	 * A list of points on the molecular surface with negative electrostatic
	 * potential and their corresponding beta values.
	 */
	private final Surface<BetaPoint> betaSurface;

	/**
	 * A list of points on the molecular surface with negative electrostatic
	 * potential and their corresponding beta values for the tri surface (or Pi function) approach.
	 */
	private final Surface<BetaPiPoint> betaPiSurface;
	
	/**
	 * List of atoms in the molecule.
	 */
	private final ArrayList<Atom> atoms;

	/**
	 * Constructs the Molecule object.
	 * 
	 * @param topology Represents the chemical structure of the molecule.
	 * @param molPositions molPositions.
	 * @param mepsData Positions and values of MEPs in space.
	 * @param ssipSurfaceInformation SSIP surface information
	 */
	public Molecule(
			ChemicalStructure topology,
			Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> molPositions,
			LinkedHashMap<Point3D, Double> mepsData, SsipSurfaceInformation ssipSurfaceInformation, IFootprintParameters footprintParameters) {
		if (topology == null || mepsData == null || footprintParameters == null)
			throw new IllegalArgumentException("Null values are not allowed!");

		this.topology = topology;

		// Assigns Surface information- contains surface area and number of SSIPs.
		this.ssipSurfaceInformation = ssipSurfaceInformation;

		this.footprintParameters = footprintParameters;
		
		// Initialises 'atoms' list
		atoms = new ArrayList<>();
		initAtoms(molPositions);

		// Initialises alpha and beta surface points
		ArrayList<AlphaPoint> alphaSurfacePoints = new ArrayList<>();
		ArrayList<BetaPoint> betaSurfacePoints = new ArrayList<>();
		initAlphaBetaSurfacePoints(alphaSurfacePoints, betaSurfacePoints, mepsData);

		// Initialises alpha and beta surfaces
		// First check there are points in the lists.
				if (alphaSurfacePoints.size() > 1) {
					this.alphaSurface = new Surface<>(alphaSurfacePoints);
				} else {
					this.alphaSurface = null;
				}
				if (betaSurfacePoints.size() > 1) {
					betaSurface = new Surface<>(betaSurfacePoints);
				} else {
					this.betaSurface = null;
				}
				this.betaPiSurface=null;
	}

	/**
	 * Constructs Molecule object.
	 * 
	 * @param topology
	 * @param molPositions
	 * @param mepsData
	 * @param fpMepsData
	 * @param piMepsData
	 * @param ssipSurfaceInformation SSIP surface information
	 */
	public Molecule(
			ChemicalStructure topology,
			Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> molPositions,
			LinkedHashMap<Point3D, Double> mepsData,
			LinkedHashMap<Point3D, Double> fpMepsData,
			LinkedHashMap<Point3D, Double> piMepsData,
			SsipSurfaceInformation ssipSurfaceInformation,
			IFootprintParameters footprintParameters) {
		if (topology == null || mepsData == null || fpMepsData == null 
				|| piMepsData == null || footprintParameters == null)
			throw new IllegalArgumentException("Null values are not allowed!");

		this.topology = topology;


		// Assigns Surface information
		this.ssipSurfaceInformation = ssipSurfaceInformation;

		this.footprintParameters = footprintParameters;
		
		// Initialises 'atoms' list
		atoms = new ArrayList<>();
		initAtoms(molPositions);

		// Initialises alpha and beta surface points
		ArrayList<AlphaPoint> alphaSurfacePoints = new ArrayList<>();
		ArrayList<BetaPiPoint> betaSurfacePoints = new ArrayList<>();
		initPiSurfacePoints(alphaSurfacePoints, betaSurfacePoints, mepsData, fpMepsData, piMepsData);

		LOG.trace("number of beta points: {}", betaSurfacePoints.size());
		
		// Initialises alpha and beta surfaces
		// First check there are points in the lists.
				if (alphaSurfacePoints.size() > 1) {
					this.alphaSurface = new Surface<>(alphaSurfacePoints);
				} else {
					this.alphaSurface = null;
				}
				if (betaSurfacePoints.size() > 1) {
					LOG.debug("beta pi surface creation");
					LOG.debug("beta point 1: {}", betaSurfacePoints.get(0));
					this.betaPiSurface = new Surface<>(betaSurfacePoints);
				} else {
					this.betaPiSurface = null;
				}
				this.betaSurface = null;
	}

	/**
	 * Initialises 'atoms'.
	 */
	private void initAtoms(
			Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> molPositions) {
		List<Graph<AtomDescriptor, BondDescriptor>.Vertex> vertices = topology
				.getMolTopology().getVertices();
		Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> cFactors = topology
				.getCFactors();

		for (int i = 0; i < vertices.size(); i++) {
			Graph<AtomDescriptor, BondDescriptor>.Vertex v = vertices.get(i);
			String atomId = "a".concat(String.valueOf(i + 1));
			atoms.add(new Atom(v.getVertexLabel(), molPositions.get(v),
					cFactors.get(v), atomId));
		}
	}

	/**
	 * Sorts the surface points into Alpha or beta points, ready to initialise the alpha and beta surfaces.
	 * 
	 * @param alphaSurfacePoints AlphaPoint ArrayList to populate.
	 * @param betaSurfacePoints BetaPoint ArrayList to populate.
	 * @param mepsData Positions and values of MEPs in space.
	 */
	private void initAlphaBetaSurfacePoints(ArrayList<AlphaPoint> alphaSurfacePoints,
			ArrayList<BetaPoint> betaSurfacePoints,
			LinkedHashMap<Point3D, Double> mepsData) {

		// Closest atom to a given MEP
		Atom closestAtom;

		// Electrostatic potential of the MEP
		Double ep;

		// Location in space of the point
		Point3D pointInSpace;

		// Alpha point
		AlphaPoint alpha;

		// Beta point
		BetaPoint beta;

		// Iterates over positions of MEPs in space
		for (Entry<Point3D, Double> entry : mepsData.entrySet()) {

			// Retrieves the electrostatic potential of the given MEP
			ep = entry.getValue();
			pointInSpace = entry.getKey();

			if (ep >= 0) {

				// Initialises an alpha point
				alpha = new AlphaPoint(pointInSpace.getX(),
						pointInSpace.getY(), pointInSpace.getZ(), ep);

				// Adds it to the list of alpha points
				alphaSurfacePoints.add(alpha);

			} else {

				// Retrieves the closest atom to a beta point
				closestAtom = Atom.findAtomClosestToPoint(atoms, pointInSpace);

				// Initialises a beta point
				beta = new BetaPoint(pointInSpace.getX(), pointInSpace.getY(),
						pointInSpace.getZ(), ep, closestAtom);

				// Adds the beta point to the list of beta points
				betaSurfacePoints.add(beta);
			}
		}

	}

	/**
	 * Sorts the surface points into Alpha or beta points, ready to initialise the alpha and beta surfaces.
	 * 
	 * @param mepsData
	 * @param topology
	 */
	private void initPiSurfacePoints(ArrayList<AlphaPoint> alphaSurfacePoints,
			ArrayList<BetaPiPoint> betaSurfacePoints,
			LinkedHashMap<Point3D, Double> mepsData,
			LinkedHashMap<Point3D, Double> fpMepsData,
			LinkedHashMap<Point3D, Double> piMepsData
			) {

		// Electrostatic potential of the MEP
		Double ep;
		Double fpEp;
		Double piEp;
		Double maxFpEp;
		Double fpPad;

		ArrayList<Double> fpEps = new ArrayList<>();

		// Location in space of the point
		Point3D pointInSpace;
		
		// Inner surface points and inner surface kdTrees
		Point3D closestFpPoint;
		KDTree<Point3D> fpTree;
		Point3D closestPiPoint;
		KDTree<Point3D> piTree;
		
		// Alpha point
		AlphaPoint alpha;

		// Beta point
		BetaPiPoint beta;

		// Initializes KDtree for closest point search
		LOG.debug("fp tree creation");
		fpTree = new KDTree<>(new ArrayList<>(fpMepsData.keySet()));
		LOG.debug("pi tree creation");
		piTree = new KDTree<>(new ArrayList<>(piMepsData.keySet()));

		// Iterates over positions of MEPs in space
		for (Entry<Point3D, Double> entry : mepsData.entrySet()) {

			// Retrieves the electrostatic potential to the given MEP
			ep = entry.getValue();
			pointInSpace = entry.getKey();

			if (ep >= 0) {

				// Initialises an alpha point
				alpha = new AlphaPoint(pointInSpace.getX(),
						pointInSpace.getY(), pointInSpace.getZ(), ep);

				// Adds it to the list of alpha points
				alphaSurfacePoints.add(alpha);

			} else {

				closestPiPoint = piTree.nearestNeighbours(pointInSpace, 1).get(0);
				piEp = piMepsData.get(closestPiPoint);
				closestFpPoint = fpTree.nearestNeighbours(pointInSpace, 1).get(0);
				fpEp = fpMepsData.get(closestFpPoint);
				fpEps.add(fpEp);

				// Initialises a beta point
				beta = new BetaPiPoint(pointInSpace.getX(), pointInSpace.getY(),
						pointInSpace.getZ(), ep, fpEp, piEp);

				// Adds the beta point to the list of beta points
				betaSurfacePoints.add(beta);
			}
		}
		
		// Ensure the footprinting beta surface is all negative
		//First check there is a negative surface.
		if (!fpEps.isEmpty()) {
			maxFpEp = Collections.max(fpEps);
			fpPad = Math.min(0.0, -maxFpEp-MIN_NEG_FP_ESP);
			for (BetaPiPoint betaPoint : betaSurfacePoints) {
				betaPoint.setFootprintEp(betaPoint.getEp()+fpPad);
			}	
		}
		

	}
	
	/**
	 * Find all SSIPs for the molecule.
	 * 
	 * @return List of SSIPs
	 */
	public List<Ssip> findSSIPs(SelectorEnum selectorEnum) {

		ArrayList<Ssip> ssips = new ArrayList<>();
		
		switch (selectorEnum) {
		case ORIGINAL:
			ssips = (ArrayList<Ssip>) calcOriginalSSIPsonMolecule();
			break;
		default:
			break;
		}
		
		
		LOG.debug("Total number of SSIPs: {}\nRequired Number of SSIPs: {}", ssips.size(), getTotalNumberOfSSIPs());
		LOG.debug("Final Set of all SSIPs found on Surface:");
		LOG.debug(ssips);
		// Difference between total number of SSIPs for the given surface area and the number assigned.
		int differenceInSSIPNumber = getTotalNumberOfSSIPs() - ssips.size();
		if (differenceInSSIPNumber != 0) {
			LOG.debug("Adding {} null SSIPs.", differenceInSSIPNumber);
			Point3D moleculeCentroid = getMoleculeCentroid();
			LOG.trace("Molecule Centroid: {}", moleculeCentroid);
			for (int i = 0; i < differenceInSSIPNumber; i++) {
				ssips.add(Ssip.fromMepPoint(new AlphaPoint(moleculeCentroid.getX(), moleculeCentroid.getY(), moleculeCentroid.getZ(), null, 0.0)));
			}
		}
		LOG.debug("Final Set of all SSIPs including null SSIPs:");
		LOG.debug(ssips);
		Collections.sort(ssips, new SsipComparator(getMoleculeCentroid()));
		return ssips;
	}

	private List<Ssip> calcOriginalSSIPsonMolecule() {
		List<Ssip> ssips = new ArrayList<>();
		
		// Alpha SSIPs
		if (getNumberOfPositiveSSIPs() > 0) {
			ssips.addAll(calcOriginalSSIPsOnSurface(getNumberOfPositiveSSIPs(), alphaSurface, footprintParameters.getPositiveLowerCutOffDistance(), footprintParameters.getPositiveOccludedZoneDistance()));
		}

		// Beta SSIPs
		if (getNumberOfNegativeSSIPs() > 0) {
			if (betaPiSurface != null) {
				ssips.addAll(calcOriginalSSIPsOnSurface(getNumberOfNegativeSSIPs(), betaPiSurface, footprintParameters.getNegativeLowerCutOffDistance(), footprintParameters.getNegativeOccludedZoneDistance()));
			} else {
				ssips.addAll(calcOriginalSSIPsOnSurface(getNumberOfNegativeSSIPs(), betaSurface, footprintParameters.getNegativeLowerCutOffDistance(), footprintParameters.getNegativeOccludedZoneDistance()));
			}
			
		}
		return ssips;
	}

	public <T extends MepPoint> Set<Ssip> calcSSIPs(
			int requestedTotalNumberOfSSIPs, Surface<T> surface, double lowerCutOffDistance, double occludedZoneDistance) {

		// retrieves all patches on the surface
		ArrayList<Surface<T>> patches = surface.findPatches();

		// sorts elements in descending order such that the surface with
		// the highest fractional SSIP area is at the beginning of the list.
		Comparator<Surface<T>> comparator = new Comparator<Surface<T>>() {

			@Override
			public int compare(Surface<T> o1, Surface<T> o2) {

				// retrieves fractional SSIP area of surfaces
				double o1RemainingArea = o1.getArea() % ERROR_IN_AREA;
				double o2RemainingArea = o2.getArea() % ERROR_IN_AREA;

				if (o1.equals(o2))
					return 0;
				else if (o1RemainingArea > o2RemainingArea)
					return -1;
				else
					return 1;
			}
		};

		// Sorts 'patches' according to 'comparator'. It is important to
		// remember that from now on patches that have the most fractional SSIP
		// area are placed at the beginning of the list. This is very important
		// for the 'while' loop below.
		Collections.sort(patches, comparator);

		// number of SSIPs assigned to a given 'patch' at EACH iteration
		int numberOfSSIPsAtEachIteration;

		// Stores the number of SSIPs assigned to each surface. The position of
		// each element in the array corresponds to the position of a given
		// surface in the list of patches to which the number of SSIPs are
		// assigned.
		int[] numberOfSSIPsPerPatch = new int[patches.size()];

		// an element of 'patches'
		Surface<T> patch;

		// index used to access elements in 'patches' and
		// 'numberOfSSIPsPerPatch'.
		int i = 0;

		int totalNumberOfSSIPs = requestedTotalNumberOfSSIPs;

		// iterates until there are no SSIPs left to be assigned
		while (totalNumberOfSSIPs != 0) {

			// Retrieves a 'patch'. As 'i' may traverse some elements of the
			// list more than once it is important to use the '%' operator which
			// loops around preventing index out of bounds exceptions.
			patch = patches.get(i % patches.size());

			// This rather complicated statement ensures that while the index
			// 'i' traverses all elements of 'patches' for the FIRST time, the
			// number of SSIPs assigned to each surface is given by the
			// NON-fractional part of the ratio between the surface area of the
			// patch and the area of a SSIP. If however 'i' starts to traverse
			// elements for the SECOND time, the number of SSIPs assigned is
			// '1'. To understand this, recall that 'patches' is sorted as
			// described before, hence those surfaces having the largest
			// fractional SSIP area get one more SSIP to reflect that they have
			// the most remaining area.
			numberOfSSIPsAtEachIteration = patches.size() < i ? (int) (patch
					.getArea() / footprintParameters.getAreaOfSSIP()) : 1;

			// increments the number of SSIPs assigned to 'patch'
			numberOfSSIPsPerPatch[i % patches.size()] += numberOfSSIPsAtEachIteration;

			// decreases total number of SSIPs by the number of assigned SSIPs
			totalNumberOfSSIPs -= numberOfSSIPsAtEachIteration;

			// increases index
			i++;
		}

		// initializes a set containing all SSIPs in their final configuration
		HashSet<Ssip> ssips = new HashSet<>(requestedTotalNumberOfSSIPs);

		// declares 'SSIPSelector' used to generate SSIPs
		ISsipSelector selector;

		// iterates over all patches
		for (int j = 0; j < patches.size(); j++) {

			// initializes a 'SSIPSelector' providing the surface and the
			// number of SSIPs assigned
			selector = new OriginalSsipSelector<>(patches.get(j),
					numberOfSSIPsPerPatch[j], lowerCutOffDistance, occludedZoneDistance);

			// adds all SSIPs generated by 'selector' to the set.
			ssips.addAll(selector.getSSIPs());
		}

		return ssips;
	}

	/**
	 * This calculates the SSIPs that should be present on the given surface, using the original selection criteria, {@link OriginalSsipSelector}.
	 * 
	 * @param requiredNumberOfSSIPs
	 * @param surface of MEPS points
	 * @return SSIPs on surface 
	 */
	private <T extends MepPoint> HashSet<Ssip> calcOriginalSSIPsOnSurface(
			int requiredNumberOfSSIPs, Surface<T> surface, double lowerCutOffDistance, double occludedZoneDistance) {

		// initializes a set containing all SSIPs in their final configuration
		HashSet<Ssip> ssips = new HashSet<>();

		// declares 'SSIPSelector' used to generate SSIPs
		OriginalSsipSelector<T> selector;

		selector = new OriginalSsipSelector<>(surface, requiredNumberOfSSIPs, lowerCutOffDistance, occludedZoneDistance);

		// adds all SSIPs generated by 'selector' to the set.
		ssips.addAll(selector.getSSIPs());

		return ssips;
	}

	/**
	 * 
	 * @return the Centroid of the molecule.
	 */
	private Point3D getMoleculeCentroid(){
		CentroidCalculator centroidCalculator = new CentroidCalculator();
		return centroidCalculator.calculateCentroid(getAtomCoordinates());
	}
	
	/**
	 * @return The 3D coordinates of the atoms.
	 */
	private List<Point3D> getAtomCoordinates(){
		ArrayList<Point3D> atomCoordinates = new ArrayList<>();
		for (Atom atom : getAtoms()) {
			atomCoordinates.add(atom.getPosition());
		}
		return atomCoordinates;
	}
	
	
	
	public int getTotalNumberOfSSIPs() {
		return ssipSurfaceInformation.getTotalNumberOfSSIPs(footprintParameters.getAreaOfSSIP());
	}

	public int getNumberOfNegativeSSIPs() {
		return ssipSurfaceInformation.getNumberOfNegativeSSIPs(footprintParameters.getAreaOfSSIP());
	}

	public int getNumberOfPositiveSSIPs() {
		return ssipSurfaceInformation.getNumberOfPositiveSSIPs(footprintParameters.getAreaOfSSIP());
	}

	public double getArea() {
		return getSsipSurfaceInformation().getTotalVdWSurfaceArea();
	}

	public double getPosArea() {
		return getSsipSurfaceInformation().getPositiveVdWSurfaceArea();
	}

	public double getNegArea() {
		return getSsipSurfaceInformation().getNegativeVdWSurfaceArea();
	}

	public Surface<AlphaPoint> getAlphaSurface() {
		return alphaSurface;
	}

	public Surface<BetaPoint> getBetaSurface() {
		return betaSurface;
	}

	public Surface<BetaPiPoint> getBetaPiSurface() {
		return betaPiSurface;
	}

	public List<Atom> getAtoms() {
		return atoms;
	}

	public double getVolume() {
		return getSsipSurfaceInformation().getVdWVolume();
	}

	public ChemicalStructure getTopology() {
		return topology;
	}

	public SsipSurfaceInformation getSsipSurfaceInformation() {
		return ssipSurfaceInformation;
	}

}
