/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint.selectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import uk.ac.cam.ch.hunter.ssip.core.MepPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.KDTree;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Surface;

/**
 * An experimental simulated annealing algorithm. More information can be found <a
 * href="https://en.wikipedia.org/wiki/Simulated_annealing">here</a>.
 * 
 * Note, this has never been implemented or used.
 * 
 * @author tn300
 */
public class SimulatedAnnealingSsipSelector<T extends MepPoint> implements ISsipSelector{

	/**
	 * Specifies a factor determining the number of iterations performed on each
	 * SSIP. This factor represents the ratio between the number of iterations
	 * and the number of nearest neighbours explored at each change of the
	 * configuration. By increasing this factor the number of iterations
	 * performed is increased.
	 */
	private static final int ITERATIONS_FACTOR = 10;

	/**
	 * Number of SSIPs to be selected on <code>surface</code>.
	 */
	private final int numberOfSSIPs;

	/**
	 * Represents the surface on which the algorithm is executed.
	 */
	private final Surface<T> surface;

	/**
	 * Number of nearest neighbours iterated over by the algorithm.
	 * 
	 * Note: specifying too few neighbours leads to slow convergence, specifying
	 * too many slows down the selection process.
	 */
	private final int numberOfNearestNeighbours;

	/**
	 * Number of iterations performed by the selection algorithm for each SSIP.
	 * 
	 * Note: The number of iterations should allow for each SSIP to explore the
	 * entire space.
	 */
	private final int numberOfIterations;

	/**
	 * Effective SSIP surface area in Angstroms squared. Represents the
	 * available surface for each SSIP. This differs from the average SSIP area
	 * as SSIPs can be placed on a surface accommodating slightly more or
	 * slightly less SSIPs than the average suggests.
	 */
	private final double effectiveSSIPArea;

	/**
	 * Effective distance squared between SSIPs on the surface. The effective
	 * distance squared of an SSIP on the surface is equal to
	 * <code>effectiveSSIPArea</code> under the assumption that the underlying
	 * surface is a square. This is usually a good initial guess and the
	 * resulting distance is smaller than what it would have been if we had
	 * assumed that the surface is a circle. However issues may arise with
	 * surfaces whose curvature is high.
	 */
	private final double effectiveSSIPDistanceSquared;

	// Random number generator
	static Random randomGenerator;

	/**
	 * Constructs a SSIP selector.
	 * 
	 * @param surface The surface on which the algorithm is executed.
	 * @param numberOfSSIPs Number of SSIPs to produce.
	 */
	public SimulatedAnnealingSsipSelector(Surface<T> surface, int numberOfSSIPs, double areaOfSSIP) {
		if (surface == null)
			throw new IllegalArgumentException("Null values are not allowed.");
		if (numberOfSSIPs < 1)
			throw new IllegalArgumentException(
					"The number of SSIPs should be at least one.");

		this.surface = surface;
		this.numberOfSSIPs = numberOfSSIPs;

		// ---Calculates effective area and distance of an SSIP---

		// calculates an effective area of SSIP
		double tmpEffectiveSSIPsurface = surface.getArea() / numberOfSSIPs;

		// stores the smaller of the two values as effective area
		effectiveSSIPArea = tmpEffectiveSSIPsurface > areaOfSSIP ? areaOfSSIP
				: tmpEffectiveSSIPsurface;

		// effective 3D distance between SSIPs
		effectiveSSIPDistanceSquared = Math.sqrt(effectiveSSIPArea);

		// number of nearest neighbours explored at each move of an SSIP
		numberOfNearestNeighbours = Math.round((float) Math.sqrt(surface
				.getPointsOnSurface().size()));

		// number of iterations per configuration
		numberOfIterations = ITERATIONS_FACTOR * numberOfNearestNeighbours;

	}

	/**
	 * Provides a randomly generated initial distribution of SSIPs.
	 * 
	 * @param surface
	 *            molecular surface on which the SSIPs are generated
	 * @param numberOfSSIPs
	 *            the number of SSIPs present on the surface
	 * @return list of SSIPs assigned randomly to MEPPoints on the surface
	 */
	private static <T extends MepPoint> CopyOnWriteArrayList<Ssip> generateRandomInitialSSIPConfiguration(
			Surface<T> surface, int numberOfSSIPs) {

		// A distribution of SSIPs
		CopyOnWriteArrayList<Ssip> ssipDistribution = new CopyOnWriteArrayList<>();

		// Number of points on surface
		int numberOfPoints = surface.getPointsOnSurface().size();

		// Array of ints representing indices of points in 'surface'
		ArrayList<Integer> randomInts = new ArrayList<>();

		// Iterates until the required number of SSIPs is found
		for (int i = 0; i < numberOfSSIPs; ++i) {

			// Generates a random integer between zero and the length of
			// 'surface'
			int randomInt = randomGenerator.nextInt(numberOfPoints);

			// if index already selected
			if (randomInts.contains(randomInt)) {

				// decrease by one and loop back
				i--;
			} else {

				// add to list of random indices
				randomInts.add(randomInt);
			}
		}

		// Iterates over all randomly generated indices
		for (Integer index : randomInts) {

			// Adds a SSIP at the specified index
			ssipDistribution.add(Ssip.fromMepPoint(surface.getPointsOnSurface().get(
					index)));
		}

		return ssipDistribution;
	}

	/**
	 * Finds the MEP with highest electrostatic potential in <code>meps</code>.
	 * 
	 * @param meps
	 * 
	 * @return
	 */
	private static <T extends MepPoint> T findMaxMEP(List<T> meps) {

		T bestMep = meps.get(0);

		for (T mep : meps) {
			if (mep.getValue() > bestMep.getValue())
				bestMep = mep;
		}

		return bestMep;
	}

	public List<Ssip> getSSIPs() {

		// if numberOfSSIPs = 1 place SSIP at maximum
		if (numberOfSSIPs == 1) {
			return new CopyOnWriteArrayList<>(Arrays.asList(Ssip.fromMepPoint(
					findMaxMEP(surface.getPointsOnSurface()))));
		}

		KDTree<T> space = surface.getSpace();

		// Generates a random distribution of SSIPs
		CopyOnWriteArrayList<Ssip> ssipConfiguration = generateRandomInitialSSIPConfiguration(
				surface, numberOfSSIPs);

		// neighbouring MEPs
		ArrayList<T> neighbours;

		// current temperature of the algorithm
		double temperature;

		// energy of the SSIP being moved
		double currentEnergy;

		// energy of the candidate SSIP
		double candidateEnergy;

		// random generator of numbers
		Random randomGen = new Random();

		// Iterates until the specified number of iterations
		for (int j = 0; j < numberOfIterations; j++) {

			// initializing temperature
			temperature = 1.0 - j / numberOfIterations;

			// Iterates over all SSIPs
			for (Ssip ssip : ssipConfiguration) {

				// retrieves all neighbours
				neighbours = space.nearestNeighbours(ssip,
						numberOfNearestNeighbours);

				// selects a neighbour at random
				Ssip candidateSSIP = Ssip.fromMepPoint(neighbours.get(randomGen
						.nextInt(neighbours.size())));

				// retrieves the current energy
				currentEnergy = ssip.getEnergy(ssipConfiguration,
						effectiveSSIPDistanceSquared);

				// retrieves the candidate energy
				candidateEnergy = candidateSSIP.getEnergy(ssipConfiguration,
						effectiveSSIPDistanceSquared);

				// if candidate energy is greater than current energy or
				// if the current energy is greater than candidate energy with
				// the given probability
				if (Math.exp(candidateEnergy - currentEnergy / temperature) > Math
						.random()) {
					// removes the current SSIP
					ssipConfiguration.remove(ssip);

					ssipConfiguration.add(candidateSSIP);
				}
			}
		}

		return ssipConfiguration;
	}

	@Override
	public String toString() {
		return "Number of nearest neighbours = " + numberOfNearestNeighbours
				+ "\n Number of iterations = " + numberOfIterations
				+ "\n Effective SSIP surface = " + effectiveSSIPArea
				+ "\n Effective SSIP distance = "
				+ effectiveSSIPDistanceSquared;
	}
}