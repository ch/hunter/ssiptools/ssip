/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.KDTree;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

/**
 * Method used in the original footprinting code to calculate the surface area
 * of a molecule using the MEP points. There is no documentation or references to the
 * approach used in the original code.
 * 
 * @author mw529
 * 
 */
public class ApproximateSurfaceArea {
	private static final Logger LOG = LogManager
			.getLogger(ApproximateSurfaceArea.class);
	/**
	 * This is the constant ANG_CONVERSION_FAC in the original code. It seems to
	 * be a scaling factor which is a function of the basis set, in this case it
	 * is for 6-31G*. Its origin and derivation in the original code is
	 * undocumented and unknown.
	 */
	public static final double EMPIRICAL_PTS_TO_ANGSQUARED = 0.790;

	public static final double SURF_PROBE_RAD = 0.5;

	private double negativeSurfaceArea;
	private double positiveSurfaceArea;
	private double totalSurfaceArea;

	private KDTree<Point3D> kDtree;

	/**
	 * Calculate the surface area (in Ang^2) of a molecule using the MEP points.
	 * 
	 * @param mepsData
	 *            MEP points of the molecule.
	 */
	public ApproximateSurfaceArea(Map<Point3D, Double> mepsData) {
		LOG.debug("TotalNumber of MEPs: {}", mepsData.size());

		negativeSurfaceArea = 0.0;
		positiveSurfaceArea = 0.0;
		totalSurfaceArea = 0.0;

		kDtree = new KDTree<>(new ArrayList<>(mepsData.keySet()));

		computeSurfaceAreaFromMeps(mepsData);

		LOG.debug("TotalSurfaceArea: {}", totalSurfaceArea);
		LOG.debug("PositiveSurfaceArea: {}", getPositiveSurfaceArea());
		LOG.debug("NegativeSurfaceArea: {}", getNegativeSurfaceArea());
	}

	/**
	 * 
	 * @return The surface area (in Ang^2) of the negative MEPs of a molecule.
	 */
	public double getNegativeSurfaceArea() {
		return negativeSurfaceArea;
	}

	/**
	 * 
	 * @return The surface area (in Ang^2) of the positive MEPs of a molecule.
	 */
	public double getPositiveSurfaceArea() {
		return positiveSurfaceArea;
	}

	/**
	 * 
	 * @return The total surface area (in Ang^2) of a molecule.
	 */
	public double getTotalSurfaceArea() {
		return totalSurfaceArea;
	}

	/**
	 * Assigns a weight = 1/N to "point", N being the number of neighbouring
	 * points in mepsData.keySet().
	 * 
	 * TODO check unit of mepsData 3d space and the surf_probe_rad (Bohr?
	 * Angstrom? make consistent with AddSurfPtHeader.cpp)
	 */
	private double calcWeight(Point3D point) {
		int neighb;
		double weight;

		neighb = kDtree.ballSearch(SURF_PROBE_RAD, point).size();

		weight = 1.0 / ((double) neighb);
		return weight;
	}

	/**
	 * Estimates the three (neg, pos, tot) surface areas using a finite radius
	 * smoothing algorithm and an empirical conversion factor.
	 * 
	 */
	private void computeSurfaceAreaFromMeps(Map<Point3D, Double> mepsData) {
		double ep;
		Point3D pointInSpace;

		final double HARTREES_TO_KJMOL = 2625.49962d;
		final double epsilon = Math.ulp(HARTREES_TO_KJMOL);

		for (Entry<Point3D, Double> entry : mepsData.entrySet()) {

			pointInSpace = entry.getKey();
			ep = entry.getValue();

			double pointWeight = calcWeight(pointInSpace);
			if (ep < (0.0 - epsilon)) {
				negativeSurfaceArea += pointWeight;
			}
			if (ep > (0.0 + epsilon)) {
				positiveSurfaceArea += pointWeight;
			}

			totalSurfaceArea += pointWeight;
		}
		negativeSurfaceArea = negativeSurfaceArea * EMPIRICAL_PTS_TO_ANGSQUARED;
		positiveSurfaceArea = positiveSurfaceArea * EMPIRICAL_PTS_TO_ANGSQUARED;
		totalSurfaceArea = totalSurfaceArea * EMPIRICAL_PTS_TO_ANGSQUARED;

	}

}
