/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint.io;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.footprint.ApproximateSurfaceArea;

/**
 * Reads in a combined, unformatted cube file from NWChem; this contains the
 * atomic coordinates and electrostatic potential at various points around the
 * molecule on a predefined surface.
 * 
 * The Molecule volume is stored in the first line.
 * 
 * <pre>
 * {@code
 * Marat Enclosed Volume: 4.455011 atomic_units
 * Gaussian Cube file
 *    3   -4.841015   -3.779452   -4.821085
 *   62    0.164880    0.000000    0.000000
 *   47    0.000000    0.164324    0.000000
 *   57    0.000000    0.000000    0.165279
 *    8    0.000000   -0.373754    0.000000    0.655092
 *    1    0.000000    1.437206    0.000000    0.386541
 *    1    0.000000   -1.061563    0.000000   -1.041633
 *    -3.192216    -0.985944     0.137288    -0.009353
 *    -3.192216    -0.985944     0.963683    -0.035411
 *    -3.192216    -0.821620    -0.358549     0.011042
 * ..etc
 * }
 * </pre>
 */
public class IsoOutReader {
	private static final Logger LOG = LogManager.getLogger(IsoOutReader.class);
	public static final double HARTREES_TO_KJMOL = 2625.49962;
	public static final double BOHR_TO_ANGSTROMS = 0.529177249;

	private static final String TOKENS_FORMAT = "Tokens: {}";
	
	/**
	 * Value of the isosurface density. In a.u. Not contained in the file.
	 */
	private final double electronDensityIsosurface;
	
	/**
	 * Surface area of the molecule.
	 */
	private double surfaceAreaInAngSquared;

	/**
	 * Surface area of the molecule associated with negative electrostatic
	 * potential.
	 */
	private double negativeSurfaceAreaInAngSquared;

	/**
	 * Surface area of the molecule associated with positive electrostatic
	 * potential.
	 */
	private double positiveSurfaceAreaInAngSquared;

	/**
	 * Largest +ve MEP value
	 */
	private double largestPositiveMepValueInKjPerMol;

	/**
	 * Largest -ve MEP value
	 */
	private double largestNegativeMepValueInKjPerMol;

	/**
	 * Volume enclosed by the isosurface.This is in cubic Angstroms
	 */
	private double moleculeVolume;
	
	/**
	 * Molecular electrostatic points (MEPs) representing the molecular surface.
	 */
	private LinkedHashMap<Point3D, Double> mepsData;

	/**
	 * Constructs an isoOut file reader extracting data from the specified URI.
	 * 
	 * @param uri
	 *            The URI(universal resource identifier) of the isoOut file that
	 *            is being processed.
	 * @param eDensity The value of the electron density isosurface the MEPS data corresponds to.
	 */
	public IsoOutReader(URI uri, double eDensity) {

		this.electronDensityIsosurface = eDensity;
		
		mepsData = new LinkedHashMap<>();
		
		Path resource = Paths.get(uri);

		try (Scanner scan = new Scanner(Files.newBufferedReader(resource,
				StandardCharsets.UTF_8));) {

			processHeaderLines(scan);

			processMEPLines(scan);

		} catch (FileNotFoundException e) {
			LOG.error("File not found : ", e);
			System.exit(1);
		} catch (IOException e) {
			LOG.error("General I/O exception: ", e);
			System.exit(1);
		} catch (IllegalAccessException e) {
			LOG.error(e);
		}
	}

	private void processMEPLines(Scanner scan) {
		List<String> tokens;
		double x;
		double y;
		double z;
		double ep;

		double largestPositiveMepValueSeen = 0.0;
		double largestNegativeMepValueSeen = 0.0;
		// MEPPoints
		while (scan.hasNextLine()) {
			tokens = Arrays.asList(scan.nextLine().split("\\s+"));
			LOG.debug(TOKENS_FORMAT, tokens);
			x = Double.parseDouble(tokens.get(1)) * BOHR_TO_ANGSTROMS;
			y = Double.parseDouble(tokens.get(2)) * BOHR_TO_ANGSTROMS;
			z = Double.parseDouble(tokens.get(3)) * BOHR_TO_ANGSTROMS;
			Point3D point = new Point3D(x, y, z);
			ep = Double.parseDouble(tokens.get(4));

			if (ep > 0.0 && ep > largestPositiveMepValueSeen) {
				largestPositiveMepValueSeen = ep;
			}

			if (ep < 0.0 && ep < largestNegativeMepValueSeen) {
				largestNegativeMepValueSeen = ep;
			}

			largestPositiveMepValueInKjPerMol = largestPositiveMepValueSeen * HARTREES_TO_KJMOL;
			largestNegativeMepValueInKjPerMol = largestNegativeMepValueSeen * HARTREES_TO_KJMOL;

			mepsData.put(point, ep * HARTREES_TO_KJMOL);
		}


		// MEP Points have been read, now calculate surface areas
		ApproximateSurfaceArea molArea = new ApproximateSurfaceArea(mepsData);

		surfaceAreaInAngSquared = molArea.getTotalSurfaceArea();
		negativeSurfaceAreaInAngSquared = molArea.getNegativeSurfaceArea();
		positiveSurfaceAreaInAngSquared = molArea.getPositiveSurfaceArea();
	}

	private void processHeaderLines(Scanner scan) throws IllegalAccessException {
		// HEADER
		// skip the header lines.
		List<String> tokens = Arrays.asList(scan.nextLine().split("\\s+"));
		LOG.debug(TOKENS_FORMAT, tokens);
		if (tokens.size() > 2) {
			double volume = Double.parseDouble(tokens.get(4));
			if ("atomic_units".equals(tokens.get(5))) {
				double volInAngstroms = volume * BOHR_TO_ANGSTROMS * BOHR_TO_ANGSTROMS * BOHR_TO_ANGSTROMS;
				this.moleculeVolume = volInAngstroms;
			} else if ("angstroms".equals(tokens.get(5))) {
				this.moleculeVolume = volume;
			} else {
				throw new IllegalAccessException("Unit string not recognised.");
			}
		}
		scan.nextLine();
		// get the number of atoms, so we know how many lines to skip.
		tokens = Arrays.asList(scan.nextLine().split("\\s+"));
		LOG.debug(TOKENS_FORMAT, tokens);
		int numberOfAtoms = Integer.parseInt(tokens.get(1));
		// skip the grid coordinate lines.
		scan.nextLine();
		scan.nextLine();
		scan.nextLine();
		for (int i = 0; i < numberOfAtoms; i++) {
			scan.nextLine();
		}
	}

	/**
	 * This returns the surface information for the surface the reader has parsed.
	 * 
	 * @return surface information.
	 */
	public SsipSurface getSurfaceInformation() {
		return new SsipSurface(surfaceAreaInAngSquared, negativeSurfaceAreaInAngSquared,
				positiveSurfaceAreaInAngSquared, electronDensityIsosurface, mepsData.size(), moleculeVolume,
				largestPositiveMepValueInKjPerMol, largestNegativeMepValueInKjPerMol);
	}
	
	public double getSurfaceAreaInAngSquared() {
		return surfaceAreaInAngSquared;
	}

	public void setSurfaceAreaInAngSquared(double surfaceAreaInAngSquared) {
		this.surfaceAreaInAngSquared = surfaceAreaInAngSquared;
	}

	public double getNegativeSurfaceAreaInAngSquared() {
		return negativeSurfaceAreaInAngSquared;
	}

	public void setNegativeSurfaceAreaInAngSquared(
			double negativeSurfaceAreaInAngSquared) {
		this.negativeSurfaceAreaInAngSquared = negativeSurfaceAreaInAngSquared;
	}

	public double getPositiveSurfaceAreaInAngSquared() {
		return positiveSurfaceAreaInAngSquared;
	}

	public void setPositiveSurfaceAreaInAngSquared(
			double positiveSurfaceAreaInAngSquared) {
		this.positiveSurfaceAreaInAngSquared = positiveSurfaceAreaInAngSquared;
	}

	public double getMoleculeVolume() {
		return moleculeVolume;
	}

	public double getLargestPositiveMepValueInKjPerMol() {
		return largestPositiveMepValueInKjPerMol;
	}

	public double getLargestNegativeMepValueInKjPerMol() {
		return largestNegativeMepValueInKjPerMol;
	}

	public LinkedHashMap<Point3D, Double> getMepsData() {
		return mepsData;
	}

}