/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint.parameters;

/**
 * Generic class which acts as a container for the parameters required for footprinting.
 * 
 * @author mdd31
 *
 */
public class Parameters implements IFootprintParameters {

	private final double lowerPositiveCutOffDistance;
	
	private final double lowerNegativeCutOffDistance;
	
	private final double occludedPositiveZoneDistance;
	
	private final double occludedNegativeZoneDistance;
	
	private final double areaOfSSIP;
	
	/**
	 * Used when the positive and negative regions are treated with the same parameters.
	 * 
	 * @param lowerCutOffDistance
	 * @param occludedZoneDistance
	 * @param areaOfSSIP
	 */
	public Parameters(double lowerCutOffDistance, double occludedZoneDistance, double areaOfSSIP) {
		this.lowerPositiveCutOffDistance = lowerCutOffDistance;
		this.lowerNegativeCutOffDistance = lowerCutOffDistance;
		this.occludedPositiveZoneDistance = occludedZoneDistance;
		this.occludedNegativeZoneDistance = occludedZoneDistance;
		this.areaOfSSIP = areaOfSSIP;
	}

	/**
	 * @param lowerPositiveCutOffDistance
	 * @param lowerNegativeCutOffDistance
	 * @param occludedPositiveZoneDistance
	 * @param occludedNegativeZoneDistance
	 * @param areaOfSSIP
	 */
	public Parameters(double lowerPositiveCutOffDistance, double lowerNegativeCutOffDistance,
			double occludedPositiveZoneDistance, double occludedNegativeZoneDistance, double areaOfSSIP) {
		this.lowerPositiveCutOffDistance = lowerPositiveCutOffDistance;
		this.lowerNegativeCutOffDistance = lowerNegativeCutOffDistance;
		this.occludedPositiveZoneDistance = occludedPositiveZoneDistance;
		this.occludedNegativeZoneDistance = occludedNegativeZoneDistance;
		this.areaOfSSIP = areaOfSSIP;
	}



	@Override
	public double getPositiveLowerCutOffDistance() {
		return lowerPositiveCutOffDistance;
	}


	@Override
	public double getNegativeLowerCutOffDistance() {
		return lowerNegativeCutOffDistance;
	}
	
	@Override
	public double getPositiveOccludedZoneDistance() {
		return occludedPositiveZoneDistance;
	}


	@Override
	public double getNegativeOccludedZoneDistance() {
		return occludedNegativeZoneDistance;
	}


	@Override
	public double getAreaOfSSIP() {
		return areaOfSSIP;
	}
	
}
