/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint;

import uk.ac.cam.ch.hunter.ssip.core.Constants;
import uk.ac.cam.ch.hunter.ssip.core.MepPoint;

public class BetaPiPoint extends MepPoint {
	
	public static final double ZEROBETA = -0.01;
	
	private final double beta;
	
	private double betaEp;
	private double footprintEp;
	private double gradEp;
	
	/**
	* Constructor for pi-function differential method (depends on two ep values)
	* 
	* @param x
	* @param y
	* @param z
	* @param betaEp electrostatic potential on 0.002 a.u. isodensity surface used for pi function
	* @param fpEp: the 0.0104 a.u. isosurface used for the footprinting assignment
	* @param piEp: the 0.005 a.u. isosurface used for the pi function gradient
	*/
	public BetaPiPoint(double x, double y, double z, double betaEp, double fpEp, double piEp) {
		// this.ep is set to fpEp, which is the correct value for the purpose of
		// footprinting. XXX: maybe "ep" should be called "fpEp" but this refactor should
		// happen on a greater scope.
		super(x, y, z, fpEp);
		this.isosurfdensity = 0.0104;
		this.betaEp = betaEp;
		this.footprintEp = fpEp;
		this.gradEp = piEp;
		if (betaEp >= 0)
			throw new IllegalArgumentException(
					"Beta point must have outer electrostatic potential strictly smaller than zero.");
		beta = calculateBeta(betaEp, piEp);
	}

	/**
	 * Calculate the Beta using the pi function.
	 * 
	 * @param mepValue for surface- 0.002 a.u.
	 * @param innerEpValue for inner surface 0.005 a.u.
	 * @return beta value
	 */
	private double calculateBeta(double mepValue, double innerEpValue) {
		
		//gradient
		double dPhi = innerEpValue - mepValue;
		
		double b_a = -63.9536393/Constants.HARTREES_TO_KJMOL;
		double b_b = -4.72726144/Constants.HARTREES_TO_KJMOL;
		double b_c = -0.00610595816*Constants.HARTREES_TO_KJMOL;
		double b_d = 25.8334241/Constants.HARTREES_TO_KJMOL;
		double b_f = -0.00908829891 ;
	
		double quadratic  =   b_a * ( mepValue + b_b * Math.pow(mepValue, 2)); 
		double correction =  1.00 + b_d * Math.abs( (1-b_f) *dPhi + b_f * mepValue - b_c); 
        double result     = quadratic * correction;
		
		return Math.min(-1.0 * result, ZEROBETA);
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(beta);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(betaEp);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(footprintEp);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(gradEp);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BetaPiPoint other = (BetaPiPoint) obj;
		if (Math.abs(beta - other.getValue()) < Constants.ERROR_IN_ELECTROSTATIC_POTENTIAL) {
			return super.equals(other);
		} else {
			return false;
		}
	}

	@Override
	public double getValue() {
		return beta;
	}
	
	@Override
	public double getEp() {
		return footprintEp;
	}

	@Override
	public String toString() {
		return "Beta value: " + beta + super.toString();
	}
	
	public double getBetaEp() {
		return betaEp;
	}

	public void setBetaEp(double betaEp) {
		this.betaEp = betaEp;
	}

	public double getGradEp() {
		return gradEp;
	}

	public void setGradEp(double gradEp) {
		this.gradEp = gradEp;
	}

	public void setFootprintEp(double ep) {
		// XXX setting an ep after instantiation may sound strange: it is
		// actually needed when ep is not an actual measure but a
		// footprinting artifice. This is used by the Pi Surface initialisation
		// method in Molecule to add a set padding to the "patched" surface.
		// Changing the name ep to fpEp across relevant classes can help.
		this.footprintEp = ep;
		this.ep = ep;
	}
}
