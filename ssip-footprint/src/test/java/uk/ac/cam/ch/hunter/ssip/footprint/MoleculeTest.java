/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static uk.ac.cam.ch.hunter.ssip.footprint.Molecule.ERROR_IN_AREA;
import static uk.ac.cam.ch.hunter.ssip.footprint.Molecule.ERROR_IN_VOLUME;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.BondDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.ChemicalStructure;
import uk.ac.cam.ch.hunter.ssip.core.graph.Graph;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Surface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.footprint.parameters.OriginalParameters;

public class MoleculeTest {

	/*
	 * Specifies a distance which is much smaller (ie 10 times) than the
	 * distance between atoms. This is done to easily determine which MEPS are
	 * closest to which atoms.
	 */
	public static final double EPSILON = 0.1;

	static Molecule molecule;
	static Molecule piMolecule;

	static ChemicalStructure topology;
	static Map<Graph<AtomDescriptor, BondDescriptor>.Vertex, Point3D> molPositions;
	static HashMap<Graph<AtomDescriptor, BondDescriptor>.Vertex, Double> cFactors;
	static ArrayList<Graph<AtomDescriptor, BondDescriptor>.Vertex> vertices;
	static Graph<AtomDescriptor, BondDescriptor>.Vertex v1, v2, v3, v4;

	static String compound = "ethanol";
	static String basis = "Becke3LYP/6-31G(d)";
	static double volume = 35;

	static double area = 81.0;
	static double posArea = 60.0;
	static double negArea = 21.0;

	static LinkedHashMap<Point3D, Double> mepsData;

	// Atoms positions
	static Point3D p1, p2, p3, p4;

	@SuppressWarnings("unchecked")
	@BeforeClass
	public static void setUp() {

		// Mock of the topology of the molecule
		topology = Mockito.mock(ChemicalStructure.class);

		Graph<AtomDescriptor, BondDescriptor> molStruct = Mockito.mock(Graph.class);

		v1 = Mockito.mock(Graph.Vertex.class);
		Mockito.when(v1.getVertexLabel()).thenReturn(AtomDescriptor.C);
		v2 = Mockito.mock(Graph.Vertex.class);
		Mockito.when(v2.getVertexLabel()).thenReturn(AtomDescriptor.H);
		v3 = Mockito.mock(Graph.Vertex.class);
		Mockito.when(v3.getVertexLabel()).thenReturn(AtomDescriptor.O);
		v4 = Mockito.mock(Graph.Vertex.class);
		Mockito.when(v4.getVertexLabel()).thenReturn(AtomDescriptor.N);

		vertices = new ArrayList<>(Arrays.asList(v1, v2, v3, v4));

		cFactors = new HashMap<>();
		cFactors.put(v1, 1.0);
		cFactors.put(v2, 1.1);
		cFactors.put(v3, 1.2);
		cFactors.put(v4, 1.3);

		Mockito.when(topology.getCFactors()).thenReturn(cFactors);
		Mockito.when(topology.getMolTopology()).thenReturn(molStruct);
		Mockito.when(molStruct.getVertices()).thenReturn(vertices);

		// Atom positions
		p1 = new Point3D(0, 0, 0);
		p2 = new Point3D(1, 0, 0);
		p3 = new Point3D(0, 1, 0);
		p4 = new Point3D(0, 0, 1);

		molPositions = new HashMap<>();
		molPositions.put(v1, p1);
		molPositions.put(v2, p2);
		molPositions.put(v3, p3);
		molPositions.put(v4, p4);

		// MEPs data
		mepsData = new LinkedHashMap<>();
		mepsData.put(new Point3D(EPSILON + p1.getX(), p1.getY(), p1.getZ()), 1.0);
		mepsData.put(new Point3D(EPSILON + p2.getX(), p2.getY(), p2.getZ()), 1.1);
		mepsData.put(new Point3D(EPSILON + p3.getX(), p3.getY(), p3.getZ()), 1.2);
		mepsData.put(new Point3D(EPSILON + p4.getX(), p4.getY(), p4.getZ()), 1.3);
		mepsData.put(new Point3D(-EPSILON + p1.getX(), p1.getY(), p1.getZ()), -1.0);
		mepsData.put(new Point3D(-EPSILON + p2.getX(), p2.getY(), p2.getZ()), -1.1);
		mepsData.put(new Point3D(-EPSILON + p3.getX(), p3.getY(), p3.getZ()), -1.2);
		mepsData.put(new Point3D(-EPSILON + p4.getX(), p4.getY(), p4.getZ()), -1.3);

		LinkedHashMap<Point3D, Double> fpMepsData =new LinkedHashMap<>();
		fpMepsData.put(new Point3D(-EPSILON + p1.getX(), -EPSILON + p1.getY(), p1.getZ()), -2.0);
		fpMepsData.put(new Point3D(-EPSILON + p2.getX(), -EPSILON + p2.getY(), p2.getZ()), -2.0);
		fpMepsData.put(new Point3D(-EPSILON + p3.getX(), -EPSILON + p3.getY(), p3.getZ()), -2.0);
		fpMepsData.put(new Point3D(-EPSILON + p4.getX(), -EPSILON + p4.getY(), p4.getZ()), -1.5);
		LinkedHashMap<Point3D, Double> piMepsData =new LinkedHashMap<>();
		piMepsData.put(new Point3D(-EPSILON + p1.getX(), -EPSILON + p1.getY(), -EPSILON + p1.getZ()), -2.5);
		piMepsData.put(new Point3D(-EPSILON + p2.getX(), -EPSILON + p2.getY(), -EPSILON + p2.getZ()), -2.5);
		piMepsData.put(new Point3D(-EPSILON + p3.getX(), -EPSILON + p3.getY(), -EPSILON + p3.getZ()), -2.5);
		piMepsData.put(new Point3D(-EPSILON + p4.getX(), -EPSILON + p4.getY(), -EPSILON + p4.getZ()), -2.0);
		
		SsipSurface ssipSurface = new SsipSurface(area, negArea, posArea, 0.002, mepsData.size());
		ssipSurface.setVolumeVdW(volume);
		SsipSurfaceInformation ssipSurfaceInformation = new SsipSurfaceInformation(ssipSurface);
		
		molecule = new Molecule(topology, molPositions, mepsData, ssipSurfaceInformation, new OriginalParameters());
		
		piMolecule = new Molecule(topology, molPositions, mepsData, fpMepsData, piMepsData, ssipSurfaceInformation, new OriginalParameters());
	}

	@Test
	public void testConstruction() {
		assertNotNull(molecule);
		assertEquals(volume, molecule.getVolume(), ERROR_IN_VOLUME);
		assertEquals(9, molecule.getTotalNumberOfSSIPs());
		assertEquals(7, molecule.getNumberOfPositiveSSIPs());
		assertEquals(2, molecule.getNumberOfNegativeSSIPs());
		assertEquals(area, molecule.getArea(), ERROR_IN_AREA);
		assertEquals(posArea, molecule.getPosArea(), ERROR_IN_AREA);
		assertEquals(area - posArea, molecule.getNegArea(), ERROR_IN_AREA);
		assertNotNull(molecule.getAlphaSurface());
		assertNotNull(molecule.getBetaSurface());
		assertNotNull(molecule.getTopology());
		// assertEquals(2,
		// molecule.getAlphaSurface().getPointsOnSurface().size());
		// assertTrue(molecule.getBetaSurface().getPointsOnSurface().isEmpty());
	}

	@Test
	public void testAlphaSurface() {

		Point3D ap1, ap2, ap3, ap4;
		ap1 = new Point3D(EPSILON + p1.getX(), p1.getY(), p1.getZ());
		ap2 = new Point3D(EPSILON + p2.getX(), p2.getY(), p2.getZ());
		ap3 = new Point3D(EPSILON + p3.getX(), p3.getY(), p3.getZ());
		ap4 = new Point3D(EPSILON + p4.getX(), p4.getY(), p4.getZ());

		AlphaPoint alpha1, alpha2, alpha3, alpha4;
		alpha1 = new AlphaPoint(ap1.getX(), ap1.getY(), ap1.getZ(), mepsData.get(ap1));
		alpha2 = new AlphaPoint(ap2.getX(), ap2.getY(), ap2.getZ(), mepsData.get(ap2));
		alpha3 = new AlphaPoint(ap3.getX(), ap3.getY(), ap3.getZ(), mepsData.get(ap3));
		alpha4 = new AlphaPoint(ap4.getX(), ap4.getY(), ap4.getZ(), mepsData.get(ap4));
		ArrayList<AlphaPoint> alphaPoints = new ArrayList<>(Arrays.asList(alpha1, alpha2, alpha3, alpha4));

		Surface<AlphaPoint> expectedAlphaSurface = new Surface<>(alphaPoints);
		Surface<AlphaPoint> actualAlphaSurface = molecule.getAlphaSurface();
		assertEquals(expectedAlphaSurface, actualAlphaSurface);
	}

	@Test
	public void testBetaSurface() {
		Atom a1, a2, a3, a4;
		a1 = new Atom(vertices.get(0).getVertexLabel(), molPositions.get(v1), cFactors.get(v1), "a1");
		a2 = new Atom(vertices.get(1).getVertexLabel(), molPositions.get(v2), cFactors.get(v2), "a2");
		a3 = new Atom(vertices.get(2).getVertexLabel(), molPositions.get(v3), cFactors.get(v3), "a3");
		a4 = new Atom(vertices.get(3).getVertexLabel(), molPositions.get(v4), cFactors.get(v4), "a4");

		Point3D bp1, bp2, bp3, bp4;
		bp1 = new Point3D(-EPSILON + p1.getX(), p1.getY(), p1.getZ());
		bp2 = new Point3D(-EPSILON + p2.getX(), p2.getY(), p2.getZ());
		bp3 = new Point3D(-EPSILON + p3.getX(), p3.getY(), p3.getZ());
		bp4 = new Point3D(-EPSILON + p4.getX(), p4.getY(), p4.getZ());

		BetaPoint beta1, beta2, beta3, beta4;
		beta1 = new BetaPoint(bp1.getX(), bp1.getY(), bp1.getZ(), mepsData.get(bp1), a1);
		beta2 = new BetaPoint(bp2.getX(), bp2.getY(), bp2.getZ(), mepsData.get(bp2), a2);
		beta3 = new BetaPoint(bp3.getX(), bp3.getY(), bp3.getZ(), mepsData.get(bp3), a3);
		beta4 = new BetaPoint(bp4.getX(), bp4.getY(), bp4.getZ(), mepsData.get(bp4), a4);

		ArrayList<BetaPoint> betaPoints = new ArrayList<>(Arrays.asList(beta1, beta2, beta3, beta4));
		Surface<BetaPoint> expectedBetaSurface = new Surface<>(betaPoints);
		
		Surface<BetaPoint> actualBetaSurface = molecule.getBetaSurface();
		
		assertEquals(expectedBetaSurface, actualBetaSurface);
	}

	@Test
	public void testAtoms() {
		Atom a1, a2, a3, a4;
		a1 = new Atom(vertices.get(0).getVertexLabel(), molPositions.get(v1), cFactors.get(v1), "a1");
		a2 = new Atom(vertices.get(1).getVertexLabel(), molPositions.get(v2), cFactors.get(v2), "a2");
		a3 = new Atom(vertices.get(2).getVertexLabel(), molPositions.get(v3), cFactors.get(v3), "a3");
		a4 = new Atom(vertices.get(3).getVertexLabel(), molPositions.get(v4), cFactors.get(v4), "a4");
		List<Atom> expectedAtoms = Arrays.asList(a1, a2, a3, a4);
		
		List<Atom> actualAtoms = molecule.getAtoms();
		
		assertEquals(expectedAtoms, actualAtoms);
	}
	
	@Test
	public void testPiBetaSurface() {

		Point3D bp1, bp2, bp3, bp4;
		bp1 = new Point3D(-EPSILON + p1.getX(), p1.getY(), p1.getZ());
		bp2 = new Point3D(-EPSILON + p2.getX(), p2.getY(), p2.getZ());
		bp3 = new Point3D(-EPSILON + p3.getX(), p3.getY(), p3.getZ());
		bp4 = new Point3D(-EPSILON + p4.getX(), p4.getY(), p4.getZ());

		BetaPiPoint beta1, beta2, beta3, beta4;
		beta1 = new BetaPiPoint(bp1.getX(), bp1.getY(), bp1.getZ(), mepsData.get(bp1), -1.5, -2.5);
		beta2 = new BetaPiPoint(bp2.getX(), bp2.getY(), bp2.getZ(), mepsData.get(bp2), -1.5, -2.5);
		beta3 = new BetaPiPoint(bp3.getX(), bp3.getY(), bp3.getZ(), mepsData.get(bp3), -2.0, -2.0);
		beta4 = new BetaPiPoint(bp4.getX(), bp4.getY(), bp4.getZ(), mepsData.get(bp4), -1.5, -2.5);

		ArrayList<BetaPiPoint> betaPoints = new ArrayList<>(Arrays.asList(beta1, beta2, beta3, beta4));
		Surface<BetaPiPoint> expectedBetaSurface = new Surface<>(betaPoints);
		
		Surface<BetaPiPoint> actualBetaSurface = piMolecule.getBetaPiSurface();
		
		assertEquals(expectedBetaSurface, actualBetaSurface);
	}
}
