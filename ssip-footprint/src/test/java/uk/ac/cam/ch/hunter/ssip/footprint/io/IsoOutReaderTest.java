/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint.io;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;

public class IsoOutReaderTest {

	/**
	 * Output file only prints to 4 dp
	 */
	public static final double TOLERANCE = 1E-4;
	private final static Logger LOG = LogManager
			.getLogger(IsoOutReaderTest.class);
	static IsoOutReader isoOutReader;

	@BeforeClass
	public static void setUp() {
		URI uri = null;
		try {
			uri = IsoOutReaderTest.class
					.getResource(
							"/uk/ac/cam/ch/hunter/ssip/footprint/XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0020_merged.cube")
					.toURI();
		} catch (URISyntaxException e) {
			System.out.println("The specified URI is not valid: "
					+ e.getMessage());
			System.out.println("The reason is: " + e.getReason());
			System.exit(1);
		}

		isoOutReader = new IsoOutReader(uri, 0.002);
	}

	@Test
	public void testgetPositiveSurfaceAreaInAngSquared() {
		assertEquals(18.856491,
				isoOutReader.getPositiveSurfaceAreaInAngSquared(), TOLERANCE);
		LOG.debug("Pos Surf Area: {}",
				isoOutReader.getPositiveSurfaceAreaInAngSquared());
	}

	@Test
	public void testgetNegativeSurfaceAreaInAngSquared() {
		assertEquals(18.603324,
				isoOutReader.getNegativeSurfaceAreaInAngSquared(), TOLERANCE);
		LOG.debug("Neg Surf Area: {}",
				isoOutReader.getNegativeSurfaceAreaInAngSquared());
	}

	@Test
	public void testgetSurfaceAreaInAngSquared() {

		assertEquals(37.459815, isoOutReader.getSurfaceAreaInAngSquared(),
				TOLERANCE);
		LOG.debug("Surf Area: {}", isoOutReader.getSurfaceAreaInAngSquared());

	}

	@Test
	public void testgetMepsData() {

		// ---------------Checking MEPS----------------
		Map<Point3D, Double> meps = isoOutReader.getMepsData();

		assertEquals(2323, meps.size());

		// Check ordering
		assertEquals(new Point3D(-1.7913390726798597, -0.429370186420608, -0.219780011763676),
				meps.keySet().toArray()[0]);

		assertEquals(new Point3D(1.7890784274721319, -0.026922950897372996, 0.372314295433428),
				meps.keySet().toArray()[2322]);

	}
	
	@Test
	public void testGetMoleculeVolume(){
		assertEquals(21.082298727368553, isoOutReader.getMoleculeVolume(), TOLERANCE);
	}

	@Test
	public void getLargestPositiveMepValueInKjPerMol(){
		assertEquals(218.92991131332002, isoOutReader.getLargestPositiveMepValueInKjPerMol(), TOLERANCE);
	}

	@Test
	public void getLargestNegativeMepValueInKjPerMol(){
		assertEquals(-183.34388946384001, isoOutReader.getLargestNegativeMepValueInKjPerMol(), TOLERANCE);
	}
	
	@Test
	public void getSurfaceInformation() {
		SsipSurface expectedSurfaceInformation = new SsipSurface(37.45981484520736, 18.603323956396878, 18.856490888810047,  0.002, 2323, 21.082298727368553, 218.92991131332002, -183.34388946384001);
		SsipSurface actualSurfaceInformation = isoOutReader.getSurfaceInformation();
		assertEquals(expectedSurfaceInformation, actualSurfaceInformation);
		assertEquals(expectedSurfaceInformation.getMaxElectrostaticPotential(), actualSurfaceInformation.getMaxElectrostaticPotential());
		assertEquals(expectedSurfaceInformation.getMinElectrostaticPotential(), actualSurfaceInformation.getMinElectrostaticPotential());
	}
}
