/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;

public class ApproximateSurfaceAreaTest {

	static ApproximateSurfaceArea approximateSurfaceArea;

	@BeforeClass
	public static void setUp() {
		Point3D a = new Point3D(0.1, 0.1, 0.1);
		Point3D b = new Point3D(1.1, 1.1, 0);
		Point3D c = new Point3D(1, 2, 0);

		Map<Point3D, Double> mepsData = new HashMap<Point3D, Double>();

		mepsData.put(a, Double.valueOf(1));
		mepsData.put(b, Double.valueOf(0));
		mepsData.put(c, Double.valueOf(-1));

		approximateSurfaceArea = new ApproximateSurfaceArea(mepsData);

	}

	@Test
	public void testTotalSurfaceArea() {
		Assert.assertEquals(2.37, approximateSurfaceArea.getTotalSurfaceArea(), 0.01);
	}

	@Test
	public void testPositiveSurfaceArea() {
		Assert.assertEquals(0.79, approximateSurfaceArea.getPositiveSurfaceArea(), 0.01);
	}

	@Test
	public void testNegativeSurfaceArea() {

		Assert.assertEquals(0.79, approximateSurfaceArea.getNegativeSurfaceArea(), 0.01);
	}

}
