/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.util.ArrayList;

import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.ChemicalStructure;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.cml.io.CmlReader;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.footprint.io.IsoOutReader;
import uk.ac.cam.ch.hunter.ssip.footprint.parameters.OriginalParameters;
import uk.ac.cam.ch.hunter.ssip.footprint.selectors.SelectorEnum;

public class FootprintPiFunctionTest {
	
	private ArrayList<Ssip> actualBenzeneSSIPs;

	@Test
	public void testFindSSIPsBenzene() throws Exception {
		URI midBenzeneSurfUri = FootprintPiFunctionTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0050_merged.cube").toURI();
		URI innerBenzeneSurfUri = FootprintPiFunctionTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0104_merged.cube").toURI();
		URI outerBenzeneOutUri = FootprintPiFunctionTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0020_merged.cube").toURI();
		URI benzeneCMLUri = FootprintPiFunctionTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N.cml").toURI();
		
		IsoOutReader gradBenzeneInnerIso = new IsoOutReader(midBenzeneSurfUri, 0.0050);
		IsoOutReader fpBenzeneInnerIso = new IsoOutReader(innerBenzeneSurfUri, 0.0104);
		IsoOutReader outerBenzeneIsoOut = new IsoOutReader(outerBenzeneOutUri, 0.0020);
		CmlMolecule benzeneCml = CmlReader.demarshal(benzeneCMLUri);
		
		
		Molecule benzeneMolecule = new Molecule(new ChemicalStructure(benzeneCml.getMolTopology()), benzeneCml.getMolPositions(),
				outerBenzeneIsoOut.getMepsData(),
				fpBenzeneInnerIso.getMepsData(), gradBenzeneInnerIso.getMepsData(),
				new SsipSurfaceInformation(outerBenzeneIsoOut.getSurfaceInformation()), new OriginalParameters());
		actualBenzeneSSIPs = (ArrayList<Ssip>) benzeneMolecule.findSSIPs(SelectorEnum.ORIGINAL);
		
		ArrayList<Ssip> expectedSSIPs = new ArrayList<>();
		expectedSSIPs.add(new Ssip(29.099455334978252, 7.494864909304008, 2.229438037822723 , 0.6541466845154975 ));
		expectedSSIPs.add(new Ssip(24.43084091504615, 4.88322119490955, 7.26785792385074    , 0.654059062787588  ));
		expectedSSIPs.add(new Ssip(26.71319561941438, 4.754256465910258, 1.3680925253846798 , 0.6521030116277686 ));
		expectedSSIPs.add(new Ssip(26.816757723752673, 7.624281555673946, 8.128793853098056 , 0.6520154645950972 ));
		expectedSSIPs.add(new Ssip(29.16521037076174, 8.848722037513339, 5.5424580406370225 , 0.6519571016851077 ));
		expectedSSIPs.add(new Ssip(24.50465214526842, 3.482638773968017, 3.733878902361992  , 0.6516069541032659 ));
		expectedSSIPs.add(new Ssip(26.765166666658335, 6.189, 4.7485                        , 0.0                ));
		expectedSSIPs.add(new Ssip(27.61115649324248, 4.330281241543204, 4.584348763978587  , -2.4100857307330528));
		expectedSSIPs.add(new Ssip(24.82537165892435, 6.781514927271044, 4.5187106763671245 , -2.4286312236283623));
		expectedSSIPs.add(new Ssip(28.31039331804461, 5.5939194697477745, 5.897106282789834 , -2.4504132436675334));
		expectedSSIPs.add(new Ssip(25.85690216217254, 7.969110384794314, 4.912564602543848  , -2.457410732310986 ));
		expectedSSIPs.add(new Ssip(25.901162018101648, 7.375312656032679, 3.38390858709034  , -2.46267650651509  ));

		assertEquals(expectedSSIPs,actualBenzeneSSIPs);
	}
	
}
