/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-footprint - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.footprint.selectors;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.ChemicalStructure;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.cml.io.CmlReader;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.footprint.Molecule;
import uk.ac.cam.ch.hunter.ssip.footprint.io.IsoOutReader;
import uk.ac.cam.ch.hunter.ssip.footprint.parameters.OriginalParameters;

public class OriginalSsipSelectorTest {

	public static final double HARTREES_TO_KJMOL = 2625.49962;

	Atom dummy = new Atom(AtomDescriptor.O, new Point3D(0.0d, 0.0d, 0.0d), "a1");

	@Test
	public void testWaterIso() throws URISyntaxException, JAXBException {

		URI waterIsoUri = OriginalSsipSelectorTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/footprint/XLYOFNOQVPJJNP-UHFFFAOYSA-N_0.0020_merged.cube").toURI();
		URI waterCMLUri = OriginalSsipSelectorTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/footprint/XLYOFNOQVPJJNP-UHFFFAOYSA-N.cml").toURI();
		
		IsoOutReader waterIsoOut = new IsoOutReader(waterIsoUri, 0.0020);
		CmlMolecule waterCml = CmlReader.demarshal(waterCMLUri);
		
		Molecule waterMolecule = new Molecule(new ChemicalStructure(waterCml.getMolTopology()), waterCml.getMolPositions(),
				waterIsoOut.getMepsData(),
				new SsipSurfaceInformation(waterIsoOut.getSurfaceInformation()), new OriginalParameters());
		
		ArrayList<Ssip> actualwaterSSIPs = (ArrayList<Ssip>) waterMolecule.findSSIPs(SelectorEnum.ORIGINAL);
		
		
		ArrayList<Ssip> expectedSSIPs = new ArrayList<>();

		expectedSSIPs.add(Ssip.fromMepPoint(new AlphaPoint(-1.416984269774288, -1.043681471239728, 0.008290619960083,
				0.083386 * HARTREES_TO_KJMOL)));
		expectedSSIPs.add(Ssip.fromMepPoint(new AlphaPoint(1.439769054584481, -1.01976160123043, -0.12410476514447599,
				0.083362 * HARTREES_TO_KJMOL)));
		expectedSSIPs.add(Ssip.fromMepPoint(new BetaPoint(0.061383502529502, 1.6254933353115129, -1.09890111717287,
				-0.067086 * HARTREES_TO_KJMOL, dummy)));
		expectedSSIPs.add(Ssip.fromMepPoint(new BetaPoint(0.042533150565624, 1.7105442903025398, 0.992838650333057,
			        -0.067804 * HARTREES_TO_KJMOL, dummy)));
		
		assertEquals(
				expectedSSIPs, actualwaterSSIPs);
		
	}

	/**
	 * Values obtained from version 8a36cf4d49c688142 of the oldfootprintcreator
	 * C++ code
	 * 
	 * @throws URISyntaxException 
	 * @throws JAXBException 
	 */
	@Test
	public void testBenzene() throws URISyntaxException, JAXBException {

		URI outerBenzeneOutUri = OriginalSsipSelectorTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N_0.0020_merged.cube").toURI();
		URI benzeneCMLUri = OriginalSsipSelectorTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/footprint/UHOVQNZJYSORNB-UHFFFAOYSA-N.cml").toURI();
		
		IsoOutReader outerBenzeneIsoOut = new IsoOutReader(outerBenzeneOutUri, 0.0020);
		CmlMolecule benzeneCml = CmlReader.demarshal(benzeneCMLUri);
		
		Molecule benzeneMolecule = new Molecule(new ChemicalStructure(benzeneCml.getMolTopology()), benzeneCml.getMolPositions(),
				outerBenzeneIsoOut.getMepsData(),
				new SsipSurfaceInformation(outerBenzeneIsoOut.getSurfaceInformation()), new OriginalParameters());
		
		ArrayList<Ssip> actualBenzeneSSIPs = (ArrayList<Ssip>) benzeneMolecule.findSSIPs(SelectorEnum.ORIGINAL);
		
		ArrayList<Ssip> expectedSSIPs = new ArrayList<>();
		expectedSSIPs.add(new Ssip(29.099455334978252, 7.494864909304008, 2.229438037822723 , 0.6541466845154975 ));
		expectedSSIPs.add(new Ssip(24.43084091504615, 4.88322119490955, 7.26785792385074    , 0.654059062787588  ));
		expectedSSIPs.add(new Ssip(26.71319561941438, 4.754256465910258, 1.3680925253846798 , 0.6521030116277686 ));
		expectedSSIPs.add(new Ssip(26.816757723752673, 7.624281555673946, 8.128793853098056 , 0.6520154645950972 ));
		expectedSSIPs.add(new Ssip(29.16521037076174, 8.848722037513339, 5.5424580406370225 , 0.6519571016851077 ));
		expectedSSIPs.add(new Ssip(24.50465214526842, 3.482638773968017, 3.733878902361992  , 0.6516069541032659 ));
		expectedSSIPs.add(new Ssip(26.765166666658335, 6.189, 4.7485                        , 0.0                ));
		expectedSSIPs.add(new Ssip(26.765166666658335, 6.189, 4.7485                        , 0.0                ));
		expectedSSIPs.add(new Ssip(25.83298123380874, 7.771177984932852, 4.716137711955792, -1.5522195028948937));
		expectedSSIPs.add(new Ssip(27.4379980554702, 4.604256412085967, 5.386548664473148, -1.5523828013930507));
		expectedSSIPs.add(new Ssip(28.441903918788842, 5.451376581716391, 5.043901633150408, -1.5527094212198032));
		expectedSSIPs.add(new Ssip(25.351412474369525, 6.913470036904436, 3.796694358527288, -1.5528727425483981));

		
		assertEquals(12, actualBenzeneSSIPs.size());
		assertEquals(
				expectedSSIPs,
				actualBenzeneSSIPs);
		

	}

}
