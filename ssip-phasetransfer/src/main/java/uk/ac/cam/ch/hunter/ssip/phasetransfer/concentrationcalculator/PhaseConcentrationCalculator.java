/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;

/**
 * Container class extracts the concentrations from a Phase into arrays and
 * Matrices.
 * 
 * @author mdd31
 * 
 */
public class PhaseConcentrationCalculator {

	private static final Logger LOG = LogManager.getLogger(PhaseConcentrationCalculator.class);
	
	private Phase phase;

	private AssociationConstants associationConstants;

	private ArrayList<SsipConcentration> ssipConcentrationList;

	private boolean concentrationsCalculated;

	/**
	 * Initialise the PhaseConcentration with a given phase.
	 * 
	 * @param phase phase
	 */
	public PhaseConcentrationCalculator(Phase phase) {
		setPhase(phase);
		setAssociationConstants(phase.getAssociationConstants());
		setSsipConcentrationList(phase.getsortedSSIPConcentrations());
	}

	/**
	 * Initialise the associationConstants and the SSIPConcentration list,
	 * without a phase.
	 * 
	 * @param associationConstants association constants
	 * @param ssipConcentrationList SSIP concentrations
	 */
	public PhaseConcentrationCalculator(
			AssociationConstants associationConstants,
			List<SsipConcentration> ssipConcentrationList) {
		setAssociationConstants(associationConstants);
		setSsipConcentrationList(ssipConcentrationList);
	}
	
	/**
	 * This uses the given {@link ConcentrationCalculator} for the values of the free and bound concentrations, rather than calculating them.
	 * 
	 * @param phase phase
	 * @param concentrationCalculator concentration calculator
	 */
	public PhaseConcentrationCalculator(Phase phase, ConcentrationCalculator concentrationCalculator){
		setPhase(phase);
		setSsipConcentrationList(phase.getsortedSSIPConcentrations());
		ConcentrationMatcher concentrationMatcher = new ConcentrationMatcher(
				concentrationCalculator, getSsipConcentrationList());
		concentrationMatcher.setConcentrations();
		setSsipConcentrationList(concentrationMatcher.getSsipConcentrationList());
		setConcentrationsCalculated(true);
	}
	
	/**
	 * This gets the total concentrations from each SSIPConcentration in the
	 * list, creating a DoubleMatrix such that the ith value corresponds to the
	 * ith SSIPConcentration.
	 * 
	 * @return total concentrations
	 */
	public ArrayRealVector getTotalConcentrations() {
		ArrayRealVector totalConcentrations = new ArrayRealVector(
				getSsipConcentrationList().size());
		for (int i = 0; i < getSsipConcentrationList().size(); i++) {
			totalConcentrations.setEntry(i, getSsipConcentrationList().get(i)
					.getTotalConcentration());
		}
		return totalConcentrations;
	}

	/**
	 * This gets the free concentrations from each SSIPConcentration in the
	 * list, creating a DoubleMatrix such that the ith value corresponds to the
	 * ith SSIPConcentration.
	 * 
	 * @return free concentrations
	 */
	public ArrayRealVector getFreeConcentrations() {
		ArrayRealVector freeConcentrations = new ArrayRealVector(
				getSsipConcentrationList().size());
		for (int i = 0; i < getSsipConcentrationList().size(); i++) {
			freeConcentrations.setEntry(i, getSsipConcentrationList().get(i)
					.getFreeConcentration());
		}
		return freeConcentrations;
	}

	/**
	 * This gets the total bound concentrations from each SSIPConcentration in
	 * the list, creating a DoubleMatrix such that the ith value corresponds to
	 * the ith SSIPConcentration.
	 * 
	 * @return bound concentrations
	 */
	public ArrayRealVector getBoundConcentrations() {
		ArrayRealVector totalBoundConcentrations = new ArrayRealVector(
				getSsipConcentrationList().size());
		for (int i = 0; i < getSsipConcentrationList().size(); i++) {
			totalBoundConcentrations.setEntry(i, getSsipConcentrationList().get(i)
					.getBoundConcentration());
		}
		return totalBoundConcentrations;
	}

	/**
	 * This gets the bound concentrations from each SSIPConcentration in the
	 * list, creating a DoubleMatrix such that the (i,j)th value corresponds to
	 * the fraction of the ith SSIPConcentration bound to the jth
	 * SSIPConcentration.
	 * 
	 * @return bound concentrations
	 */
	public Array2DRowRealMatrix getBoundConcentrationsMatrix() {
		Array2DRowRealMatrix boundConcentrationsMatrix = new Array2DRowRealMatrix(
				getSsipConcentrationList().size(), getSsipConcentrationList()
						.size());
		for (int i = 0; i < getSsipConcentrationList().size(); i++) {
			boundConcentrationsMatrix.setRowVector(i,
					getSsipConcentrationList().get(i)
							.getBoundConcentrationList());
		}
		return boundConcentrationsMatrix;
	}

	/**
	 * Returns the ConcentrationCalculator with the calculated free and bound
	 * concentrations.
	 * 
	 * @return concentration calculator
	 */
	 ConcentrationCalculator calculateFreeAndBoundConcentrations() {
		ConcentrationCalculatorFactory concentrationCalculatorFactory = new ConcentrationCalculatorFactory();
		return concentrationCalculatorFactory.generateConcentrationCalculator(
				getAssociationConstantsMatrix(), getTotalConcentrations());
	}

	/**
	 * This returns a ConcentrationMatcher with the free and bound
	 * concentrations set on the list of SSIPConcentrations.
	 * 
	 * @return concentration matcher
	 */
	ConcentrationMatcher matchFreeAndBoundConcentrations() {
		ConcentrationMatcher concentrationMatcher = new ConcentrationMatcher(
				calculateFreeAndBoundConcentrations(), ssipConcentrationList);
		concentrationMatcher.setConcentrations();
		return concentrationMatcher;
	}

	/**
	 * This calculates the free and bound concentrations, if not already
	 * calculated, then sets these values for the SSIPConcnetrations in the
	 * List.
	 */
	public void calculateAndSetFreeAndBoundConcentrations() {
		// Only calculate if it hasn't already been calculated.
		if (!isConcentrationsCalculated()) {
			ConcentrationMatcher concentrationMatcher = matchFreeAndBoundConcentrations();
			setSsipConcentrationList(concentrationMatcher
					.getSsipConcentrationList());
			setConcentrationsCalculated(true);
		}
	}

	Set<String> getMoleculeIDs() {
		HashSet<String> moleculeIDs = new HashSet<>();
		for (int i = 0; i < getSsipConcentrationList().size(); i++) {
			String moleculeID = getSsipConcentrationList().get(i)
					.getMoleculeID();
			moleculeIDs.add(moleculeID);
		}
		return moleculeIDs;
	}

	/**
	 * This Sorts the ArrayList of the SSIPConcentrations, and then group them
	 * by moleculeID, returning a Map of moleculeID to a Set of
	 * SSIPConcentrations
	 * 
	 * @return ssipconcentrations
	 */
	public Map<String, ArrayList<SsipConcentration>> groupSSIPConcentrationsByMolecule() {
		HashMap<String, ArrayList<SsipConcentration>> ssipConcentrationsByMolecule = new HashMap<>();
		LOG.debug("Number of SSIPs in phase: {}", getSsipConcentrationList().size());
		LOG.trace("SSIPs in phase: {}", getSsipConcentrationList());
		HashSet<String> moleculeIDs = (HashSet<String>) getMoleculeIDs();
		for (String moleculeID : moleculeIDs) {
			ArrayList<SsipConcentration> ssipConcentrations = new ArrayList<>();
			LOG.trace("Mol ID for collection: {}", moleculeID);
			for (int i = 0; i < getSsipConcentrationList().size(); i++) {
				LOG.trace("Loop: pass number {}", i);
				LOG.trace("molecule ID: {}", getSsipConcentrationList().get(i).getMoleculeID());
				String ssipMolID = getSsipConcentrationList().get(i).getMoleculeID();
				LOG.trace("molecule ID: {}", ssipMolID);
				LOG.trace("comparison: {}", ssipMolID.equals(moleculeID));
				if (ssipMolID.equals(moleculeID)) {
					LOG.trace("molecule ID match: {}", i);
					ssipConcentrations.add(getSsipConcentrationList().get(i));
					LOG.trace("updated ssips in molecule: {}", ssipConcentrations);
				}
			}
			ssipConcentrationsByMolecule.put(moleculeID, ssipConcentrations);
		}
		return ssipConcentrationsByMolecule;
	}

	/**
	 * This gets the map of the SSIPConcentrationLists with the moleculeID as
	 * the key.
	 * 
	 * @return phase molecules
	 */
	Map<String, PhaseMolecule> getSSIPConcentrationListwithMoleculeID() {
		return getPhase().getPhaseMolecules();
	}

	/**
	 * This returns the set of SSIPConcentrationLists with the free and Bound
	 * concentrations assigned.
	 * 
	 * @return phase molecules
	 */
	Set<PhaseMolecule> setFreeAndBoundConcentrationsOfSSIPConcentrationLists() {
		if (!concentrationsCalculated) {
			calculateAndSetFreeAndBoundConcentrations();
		}
		HashMap<String, PhaseMolecule> ssipConcentrationListsInPhase = (HashMap<String, PhaseMolecule>) getSSIPConcentrationListwithMoleculeID();
		HashMap<String, ArrayList<SsipConcentration>> ssipConcentrationsByMolecule = (HashMap<String, ArrayList<SsipConcentration>>) groupSSIPConcentrationsByMolecule();
		LOG.debug("molecule ID set: {}", ssipConcentrationsByMolecule.keySet());
		HashSet<PhaseMolecule> ssipConcentrationListWithValuesSet = new HashSet<>();
		for (Map.Entry<String, PhaseMolecule> ssipConcentrationListEntry : ssipConcentrationListsInPhase
				.entrySet()) {
			ArrayList<SsipConcentration> ssipConcentrations = ssipConcentrationsByMolecule.get(ssipConcentrationListEntry.getKey());
			LOG.trace("SSIPs matched: {}", ssipConcentrations);
			SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ssipConcentrationListEntry.getValue().getSsipConcentrationContainer().getConcentrationUnit(), ssipConcentrations);
			PhaseMolecule phaseMoleculeConcSet = new PhaseMolecule(ssipConcentrationListEntry.getValue().getCmlMolecule(), ssipConcentrationListEntry.getValue().getSsipSurfaceInformation(), ssipConcentrationContainer, ssipConcentrationListEntry.getValue().getMoleculeID(), ssipConcentrationListEntry.getValue().getStdinchikey());
			try {
				phaseMoleculeConcSet.setMoleFraction(ssipConcentrationListEntry.getValue().getMoleFraction());
			} catch (NullPointerException e) {
				LOG.info("No Mole fraction found: ", e);
			}
			ssipConcentrationListWithValuesSet.add(phaseMoleculeConcSet);
		}
		return ssipConcentrationListWithValuesSet;
	}

	/**
	 * This returns a new Phase object with the Free and bound concentrations
	 * set in each SSIPConcentrationList.
	 * 
	 * @return phase
	 */
	public Phase setFreeAndBoundConcentrationsOfPhase() {
		return new Phase(
				(HashSet<PhaseMolecule>) setFreeAndBoundConcentrationsOfSSIPConcentrationLists(),
				getPhase().getTemperature(), getPhase().getConcentrationUnit());
	}

	public Array2DRowRealMatrix getAssociationConstantsMatrix() {
		return getAssociationConstants().getAssociationConstants();
	}

	public Phase getPhase() {
		return phase;
	}

	public void setPhase(Phase phase) {
		this.phase = phase;
	}

	public AssociationConstants getAssociationConstants() {
		return associationConstants;
	}

	public void setAssociationConstants(
			AssociationConstants associationConstants) {
		this.associationConstants = associationConstants;
	}

	public List<SsipConcentration> getSsipConcentrationList() {
		return ssipConcentrationList;
	}

	public void setSsipConcentrationList(
			List<SsipConcentration> ssipConcentrationList) {
		this.ssipConcentrationList = (ArrayList<SsipConcentration>) ssipConcentrationList;
	}

	public boolean isConcentrationsCalculated() {
		return concentrationsCalculated;
	}

	private void setConcentrationsCalculated(boolean concentrationsCalculated) {
		this.concentrationsCalculated = concentrationsCalculated;
	}
}
