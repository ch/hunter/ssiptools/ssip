/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.DefaultNamespacePrefixMapper;
import uk.ac.cam.ch.hunter.ssip.core.io.SchemaMaker;
import uk.ac.cam.ch.hunter.ssip.core.io.XmlValidationEventHandler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;

/**
 * class to write out information about a solvent system.
 * 
 * @author mdd31
 *
 */

public class SolventXmlWriter {

	private static final Logger LOG = LogManager
			.getLogger(SolventXmlWriter.class);

	private static final Schema phaseSchema = SchemaMaker.createPhaseSchema();
	
	private SolventXmlWriter(){
		/**
		 * Initialise writer.
		 */
	}
	
	/**
	 * Marshal {@link PhaseSolvent} to file using JAXB.
	 * 
	 * @param phaseSolvent solvent
	 * @param filename URI
	 * @throws JAXBException exception
	 */
	public static void  marshal(PhaseSolvent phaseSolvent, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSolvent.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(phaseSolvent, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
			throw new JAXBException(e);
		}

		
	}

	/**
	 * Marshal {@link SolventContainer} to XML using JAXB, to the file at the given {@link URI}.
	 * 
	 * @param solventContainer solvent container
	 * @param filename URI
	 * @throws JAXBException exception
	 */
	public static void marshalContainer(SolventContainer solventContainer, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(SolventContainer.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(solventContainer, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
			throw new JAXBException(e);
		}
	}
	
}
