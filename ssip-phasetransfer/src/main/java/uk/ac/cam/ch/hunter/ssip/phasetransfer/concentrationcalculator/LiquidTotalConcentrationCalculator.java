/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Constants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IPhase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

/**
 * Class for calculating the total concentration of the liquid phase, accounting for the material that is in the gas phase.
 * 
 * This extends the {@link TotalConcentrationCalculator} class, adding an 
 * extra variable to include the gas concentrations.
 * @author mdd31
 *
 */
public class LiquidTotalConcentrationCalculator extends
		TotalConcentrationCalculator {

	private static final Logger LOG = LogManager.getLogger(LiquidTotalConcentrationCalculator.class);
	
	private ArrayRealVector gasConcentrations;
	
	private ArrayRealVector phiBoundGas;
	
	private boolean gasConcentrationsUpdated;
	
	/**
	 * Constructor for calculator. Gas concentrations are set to zero on initialisation.
	 * 
	 * @param iPhase phase interface
	 * @param temperature temperature
	 */
	public LiquidTotalConcentrationCalculator(IPhase iPhase, double temperature) {
		super(iPhase, temperature);
		this.gasConcentrations = new ArrayRealVector(getSsipArrayList().size(), 0.0);
		this.phiBoundGas = new ArrayRealVector(getSsipArrayList().size(), 0.0);
		this.gasConcentrationsUpdated = false;
	}

	/**
	 * Calculation checks that the trial concentration calculated and the total concentration (including that in the gas phase) used are the same (there is self consistency). See gas outline outline for derivation.
	 * 
	 * @return concentrations match
	 */
	@Override
	public boolean totalConcentrationMatches(){
		return VectorUtils.compareVectorsEqualRelativeTolerance(calculateTrialConcentration(), getConcentrationCalculator().getTotalConcentrations().add(getGasConcentrations()), Constants.TOLERANCE);
	}
	
	/**
	 * Calculate the total concentrations by an iterative cycle. See gas calc and concentration outlines for derivation.
	 */
	@Override
	public void calculateTotalConcentrations(){
		boolean converged = totalConcentrationMatches();
		//This is to make sure it is recalculated if the gas concentrations are updated.
		if (gasConcentrationsUpdated) {
			converged = false;
		}
		while (!converged) {
			ArrayRealVector newTotalConcentrations =  (ArrayRealVector) calculateTrialConcentration().subtract(getGasConcentrations()).add(getConcentrationCalculator().getTotalConcentrations()).mapDivide(2.0);
			setConcentrationCalculator(new ConcentrationCalculator(getConcentrationCalculator().getAssociationConstants(), newTotalConcentrations));
			getConcentrationCalculator().calculateFreeAndBoundConcentrations();
			gasConcentrationsUpdated = false;
			converged = totalConcentrationMatches();
		}
		
	}
	

	/**
	 * Phi Bound used when gas is present is the total fraction bound.
	 * 
	 * It is (Phi^l_b *theta_l + Phi^g_b *theta_g)/(theta_l + theta_g)
	 * 
	 */
	@Override
	public ArrayRealVector calculatePhiBound() {
		ArrayRealVector totalConcs = getConcentrationCalculator().getTotalConcentrations().add(getGasConcentrations());
		ArrayRealVector phiBound =calculatePhiBoundLiquid().ebeMultiply(this.getConcentrationCalculator().getTotalConcentrations()).add(getGasConcentrations().ebeMultiply(getPhiBoundGas())).ebeDivide(totalConcs);
		LOG.debug("Total phi bound: {}", phiBound);
		LOG.trace("Liquid phi bound: {} liquid concentration: {}  gas phi bound: {} gas concentration: {}", calculatePhiBoundLiquid(), this.getConcentrationCalculator().getTotalConcentrations(), getPhiBoundGas(), getGasConcentrations());
		LOG.trace("Liquid psi f: {} Liquid psi_f VdW: {}", this.getPsiFree(), this.calculatePvdWFree());
		return phiBound; 
	}
	
	
	public ArrayRealVector calculatePhiBoundLiquid() {
		return super.calculatePhiBound();
	}
	

	public ArrayRealVector getPhiBoundGas() {
		return phiBoundGas;
	}

	public ArrayRealVector getGasConcentrations() {
		return gasConcentrations;
	}

	public void setGasConcentrations(ArrayRealVector gasConcentrations, ArrayRealVector phiBoundGas) {
		this.gasConcentrations = gasConcentrations;
		this.phiBoundGas = phiBoundGas;
		this.gasConcentrationsUpdated = true;
	}
	
}
