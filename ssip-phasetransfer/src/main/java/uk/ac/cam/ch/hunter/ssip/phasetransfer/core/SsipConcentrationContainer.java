/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import uk.ac.cam.ch.hunter.ssip.core.CentroidCalculator;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.SsipComparator;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;


/**
 * Represents a group of SSIPConcentration points for a molecule.
 * 
 * @author mdd31
 *
 */
@XmlRootElement(name = "SSIPs", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class SsipConcentrationContainer {
	
	/**
	 * Array list of SSIP concentrations related to the same molecule.
	 */
	@XmlElement(name = "SSIP", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ArrayList<SsipConcentration> ssipConcentrations;
	
	@XmlAttribute(name = "units", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final ConcentrationUnit concentrationUnit;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;

	/**
	 * Constructor for JAXB.
	 */
	public SsipConcentrationContainer(){
		this.ssipConcentrations = new ArrayList<>();
		this.concentrationUnit = null;
	}
	
	/**
	 * Initialise with a collection of {@link SsipConcentration}s. These are then stored in the canonical order.
	 * 
	 * @param concentrationUnit unit
	 * @param ssipConcentrations SSIP concentrations
	 */
	public SsipConcentrationContainer(ConcentrationUnit concentrationUnit, List<SsipConcentration> ssipConcentrations) {
		this.concentrationUnit = concentrationUnit;
		this.setSsipConcentrations(ssipConcentrations);
	}

	/**
	 * Constructor to create copy of Container.
	 * 
	 * @param ssipConcentrationContainer container
	 */
	public SsipConcentrationContainer(SsipConcentrationContainer ssipConcentrationContainer){
		this.concentrationUnit = ssipConcentrationContainer.getConcentrationUnit();
		ArrayList<SsipConcentration> ssipConcentrationArrayList = new ArrayList<>();
		for (SsipConcentration ssipConcentration : ssipConcentrationContainer.getSsipConcentrations()) {
			ssipConcentrationArrayList.add(new SsipConcentration(ssipConcentration, ssipConcentration.getMoleculeID()));
		}
		this.ssipConcentrations = ssipConcentrationArrayList;
	}
	
	private boolean assertConcentrationsMatch(ConcentrationUnit concentrationUnit, Collection<SsipConcentration> ssipConcentrations){
		for (SsipConcentration ssipConcentration : ssipConcentrations) {
			if (!concentrationUnit.equals(ssipConcentration.getConcentrationUnit())) {
				return false;
			}
		}
		return true;
	}
	/**
	 * Set the bound concentration DoubleMatrix from the bound concentration list.
	 */
	public void setBoundConcentrations(){
		for (SsipConcentration ssipConcentration : getSsipConcentrations()) {
			if (ssipConcentration.getBoundConcentrations() != null && ssipConcentration.getBoundConcentrationList() == null) {
				ssipConcentration.setBoundConcentrations(ssipConcentration.getBoundConcentrations());
			}
		}
	}
	/**
	 * convert concentrations to the given unit, returning a new {@link SsipConcentrationContainer}.
	 * 
	 * @param concentrationUnit unit
	 * @return ssip concentration container
	 */
	public SsipConcentrationContainer convertConcentrationsTo(ConcentrationUnit concentrationUnit){
		ArrayList<SsipConcentration> convertedSsipConcentrations = new ArrayList<>();
		for (SsipConcentration ssipConcentration : getSsipConcentrations()) {
			convertedSsipConcentrations.add(ssipConcentration.convertConcentrationsTo(concentrationUnit));
		}
		return new SsipConcentrationContainer(concentrationUnit, convertedSsipConcentrations);
	}
	
	/**
	 * Store the canonicalised List of SSIPConcentrations rather than a set. This ensures the same write order every time.
	 * 
	 * @param ssips SSIPs
	 * @return ssipconcentrations
	 */
	private List<SsipConcentration> getSortedSSIPS(Collection<SsipConcentration> ssipConcentrations) {
		ArrayList<SsipConcentration> sortedSsips = new ArrayList<>(ssipConcentrations);
		CentroidCalculator centroidCalculator = new CentroidCalculator();
		Point3D centroid = centroidCalculator
				.calculateSSIPCentroid(sortedSsips);
		Comparator<Ssip> comparator = new SsipComparator(centroid);
		Collections.sort(sortedSsips, comparator);
		return sortedSsips;
	}
	
	public ConcentrationUnit getConcentrationUnit() {
		return concentrationUnit;
	}

	public List<SsipConcentration> getSsipConcentrations() {
		return ssipConcentrations;
	}

	public void setSsipConcentrations(List<SsipConcentration> ssipConcentrations) {
		if (assertConcentrationsMatch(getConcentrationUnit(), ssipConcentrations)) {
			this.ssipConcentrations = (ArrayList<SsipConcentration>) getSortedSSIPS(ssipConcentrations);
		} else {
			throw new IllegalArgumentException("concentration units do not match");
		}
	}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SsipConcentrationContainer other = (SsipConcentrationContainer) obj;
		
		return this.getConcentrationUnit().equals(other.getConcentrationUnit()) && this.getSsipConcentrations().equals(other.getSsipConcentrations());
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = super.hashCode();
			long temp;
			temp = ssipConcentrations.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = concentrationUnit.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			
			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
}
