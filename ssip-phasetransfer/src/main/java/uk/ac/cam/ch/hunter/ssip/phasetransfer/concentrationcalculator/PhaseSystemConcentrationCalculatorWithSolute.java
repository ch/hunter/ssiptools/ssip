/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import java.util.HashSet;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolute;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;

/**
 * Calculator class containing methods to genreate a phase system with a solute in the condensed phase.
 * 
 * @author mdd31
 *
 */
public class PhaseSystemConcentrationCalculatorWithSolute {
	
	private static PhaseFactory phaseFactory = new PhaseFactory();
	
	private PhaseSystemConcentrationCalculatorWithSolute(){
	}

	/**
	 * This creates a phase system with the given mixture ID.
	 * 
	 * @param solventPhase solvent phase
	 * @param mixtureID mixture ID
	 * @return phase system
	 */
	public static PhaseSystem createPhaseSystem(Phase solventPhase, String mixtureID){
		PhaseSystemConcentrationCalculator phaseSystemConcentrationCalculator = new PhaseSystemConcentrationCalculator(solventPhase.convertConcentrationUnitTo(ConcentrationUnit.SSIPCONCENTRATION));
		return phaseSystemConcentrationCalculator.getPhaseSystem(mixtureID).convertConcentrationUnitTo(solventPhase.getConcentrationUnit());
	}
	
	/**
	 * This takes the input phase and creates a new phase with the solute in the output phase.
	 * 
	 * @param solventPhase solvent
	 * @param phaseSolute solute
	 * @return phase
	 */
	public static Phase generatePhaseWithSolute(Phase solventPhase, PhaseSolute phaseSolute){
		HashSet<PhaseMolecule> phaseMolecules = (HashSet<PhaseMolecule>) solventPhase.getPhaseMoleculesSet();
		phaseMolecules.add(phaseSolute.getPhaseMolecule());
		Phase phaseWithSolute = phaseFactory.generatePhase(phaseMolecules, solventPhase.getTemperature(), solventPhase.getConcentrationUnit());
		phaseWithSolute.setSolventID(solventPhase.getSolventID());
		return phaseWithSolute;
	}
	
	/**
	 * This creates a {@link PhaseSystem} from the input phase and then updates the condensed phase to include the solute given.
	 * 
	 * @param solventPhase solvent 
	 * @param phaseSolute solute
	 * @param mixtureID mixture ID
	 * @return phase system
	 */
	public static PhaseSystem generatePhaseSystemWithSolute(Phase solventPhase, PhaseSolute phaseSolute, String mixtureID){
		PhaseSystem phaseSystem = createPhaseSystem(solventPhase, mixtureID);
		Phase phaseWithSolute = generatePhaseWithSolute(phaseSystem.getCondensedPhase(), phaseSolute);
		phaseSystem.setCondensedPhase(phaseWithSolute);
		return phaseSystem;
	}
	
}
