/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;


/**
 * This class contains a value for a concentration and also the unit, and
 * provides methods for conversion to expression in another unit.
 * 
 * @author mdd31
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Concentration {

	@XmlValue
	private final double concentrationValue;
	@XmlAttribute(name = "units", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final ConcentrationUnit concentrationUnit;

	@XmlTransient
	private static final double ERROR = 1e-7;
	
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;

	/**
	 * Constructor for JAXB
	 */
	public Concentration(){
		this.concentrationUnit = null;
		this.concentrationValue = Double.NaN;
	}
	
	/**
	 * Constructor takes value and unit.
	 * 
	 * @param value Value of Concentration.
	 * @param unit Unit of Concentration.
	 */
	public Concentration(double value, ConcentrationUnit unit) {
		this.concentrationValue = value;
		this.concentrationUnit = unit;
	}

	@Override
	public String toString() {
		return getConcentrationValue()
				+ getConcentrationUnit().getConcentrationUnit();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Concentration other = (Concentration) obj;
		return this.getConcentrationUnit()
				.equals(other.getConcentrationUnit()) && Math.abs(this
				.getConcentrationValue() - other.getConcentrationValue()) < ERROR;

	}

	@Override
	public int hashCode(){
		if (!hashCodeFound) {

			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(getConcentrationValue());
			result = result * prime + (int) (temp ^ (temp >>> 32));
			result = result * prime + concentrationUnit.hashCode();
			hash = result;
			hashCodeFound = true;
		}
		
		return hash;
	}
	
	/**
	 * Method converts concentration to the given unit. 
	 * 
	 * @param unit Concentration Unit wanted.
	 * @return Concentration in that unit 
	 */
	public Concentration convertTo(ConcentrationUnit unit) {
		double convertedValue = getConcentrationValue()
				* getConcentrationUnit().getConversionFactorTo(unit);
		return new Concentration(convertedValue, unit);
	}

	public double getConcentrationValue() {
		return concentrationValue;
	}

	public ConcentrationUnit getConcentrationUnit() {
		return concentrationUnit;
	}

}
