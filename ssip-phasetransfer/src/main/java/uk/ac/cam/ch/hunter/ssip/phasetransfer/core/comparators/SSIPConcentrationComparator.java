/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.comparators;

import java.util.Comparator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.SsipComparator;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;

/**
 * Comparator for SSIPConcentrations.
 * 
 * @author mark
 * 
 */
public class SSIPConcentrationComparator implements
		Comparator<SsipConcentration> {

	private static final Logger LOG = LogManager.getLogger(SSIPConcentrationComparator.class);
	
	private final Map<String, Point3D> centroidMap;
	
	/**
	 * This takes a map of the moleculeID and centroid for the molecules.
	 * 
	 * @param centroidMap map of centroids
	 */
	public SSIPConcentrationComparator(Map<String, Point3D> centroidMap){
		this.centroidMap = centroidMap;
	}
	
	@Override
	public int compare(SsipConcentration ssipConcentration1,
			SsipConcentration ssipConcentration2) {

		String molID1 = ssipConcentration1.getMoleculeID();
		String molID2 = ssipConcentration2.getMoleculeID();

		if (ssipConcentration1.equals(ssipConcentration2) && ssipConcentration1.getPosition().equals(ssipConcentration2.getPosition())) {
			return 0;
		} else if (molID1.equals(molID2)) {
			LOG.debug("centroid: {}", this.getCentroidMap().get(molID1));
			SsipComparator ssipComparator = new SsipComparator(this.getCentroidMap().get(molID1));
			Ssip ssip1 = new Ssip(ssipConcentration1.getX(), ssipConcentration1.getY(), ssipConcentration1.getZ(), ssipConcentration1.getValue());
			Ssip ssip2 = new Ssip(ssipConcentration2.getX(), ssipConcentration2.getY(), ssipConcentration2.getZ(), ssipConcentration2.getValue());
			LOG.debug("SSIP 1: {}", ssip1);
			LOG.debug("SSIP 2: {}", ssip2);
			return ssipComparator.compare(ssip1, ssip2);
		} else if (molID1.compareTo(molID2) < 0) {
			return -1;
		} else {
			return 1;
		}
	}

	public Map<String, Point3D> getCentroidMap() {
		return centroidMap;
	}

	
}
