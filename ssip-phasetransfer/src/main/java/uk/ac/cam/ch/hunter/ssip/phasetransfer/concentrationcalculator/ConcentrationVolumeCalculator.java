/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

/**
 * Class contains methods for calculating concentrations from molecular volume.
 * 
 * @author mdd31
 *
 */
public class ConcentrationVolumeCalculator {

	private static final Logger LOG = LogManager.getLogger(ConcentrationVolumeCalculator.class);
	
	/**
	 * Volume of the zero point void for a liquid as described in {@link http://dx.doi.org/10.1039/c2sc21666c}.
	 * This is in cubic angstroms.
	 * 
	 */
	private static final double VOID_VOLUME = 5.0;
	
	/**
	 * Packing coefficient in the zero point liquid.
	 */
	private static final double PACKING_COEFFICIENT = 0.9;
	
	/**
	 * Avogadro's constant {@link https://en.wikipedia.org/wiki/Avogadro_constant}
	 */
	private static final double AVOGADROS_CONSTANT = 6.022140857e23;
	
	/**
	 * Expected volume available to a molecule in solution at standard concentration of 1M. In cubic angstroms.
	 */
	private static final double V_1MOLAR = Math.pow(10, 27)/AVOGADROS_CONSTANT;
	
	private ConcentrationVolumeCalculator(){
		/**
		 * private constructor.
		 */
	}
	
	/**
	 * Calculate the zero point liquid concentration. See concentration outline for definition.
	 * Volume in cubic angstroms.
	 * 
	 * @param volumeVdW VdW volume
	 * @return concentration
	 */
	public static Concentration calculateZeroPointConc(double volumeVdW){
		LOG.trace("V_1M: {}", V_1MOLAR);
		double numerator = V_1MOLAR * PACKING_COEFFICIENT;
		LOG.trace("V_1M * packing coeff: {}", numerator);
		double totalVolume = volumeVdW + VOID_VOLUME;
		LOG.trace("Total volume: {}", totalVolume);
		double unmodConc = V_1MOLAR/totalVolume;
		LOG.trace("V_1M/total vol: {}", unmodConc);
		LOG.trace("conc * packing: {}", unmodConc *PACKING_COEFFICIENT);
		double packConc = PACKING_COEFFICIENT/totalVolume;
		LOG.trace("pack/vol: {}", packConc);
		double concentrationValue = numerator/totalVolume;
		LOG.debug("Zero point concentration value: {}", concentrationValue);
		return new Concentration(concentrationValue, ConcentrationUnit.MOLAR);
	}
	
	/**
	 * Calculate the critical concentration. See concentration outline for definition.
	 * Volume in cubic angstroms.
	 * 
	 * @param volumeVdW VdW volume
	 * @return concentration
	 */
	public static Concentration calculateCriticalConc(double volumeVdW){
		double concentrationValue = (V_1MOLAR * PACKING_COEFFICIENT)/(2.0 * volumeVdW);
		LOG.debug("Critical Concentration: {}", concentrationValue);
		return new Concentration(concentrationValue, ConcentrationUnit.MOLAR);
	}
	
	/**
	 * Calculates the critical concentrations for all volumes in list. This is for each molecule. This is in Moles.
	 * 
	 * @param vdWVolumePerMolecule VdW volumes per molecule
	 * @return concentrations
	 */
	public static ArrayRealVector calculateCriticalConcs(ArrayRealVector vdWVolumePerMolecule){
		ArrayRealVector criticalConcs =  new ArrayRealVector(vdWVolumePerMolecule.getDimension());
		for (int i = 0; i < vdWVolumePerMolecule.getDimension(); i++) {
			criticalConcs.setEntry(i, calculateCriticalConc(vdWVolumePerMolecule.getEntry(i)).getConcentrationValue());
		}
		return criticalConcs;
	}
	
	/**
	 * This calculates the sum of the (mole fraction)/(critical concentration) of species.  
	 * 
	 * @param vdWVolumePerMolecule VdW volumes per molecule
	 * @param moleFraction mole fraction.
	 * @return concentration
	 */
	public static double calculatePhaseConcentration(ArrayRealVector vdWVolumePerMolecule, ArrayRealVector moleFraction){
		ArrayRealVector criticalConcs = calculateCriticalConcs(vdWVolumePerMolecule);
		LOG.debug("Critical concs: {}", criticalConcs);
		LOG.debug("molefractions: {}", moleFraction);
		double phaseConcentration = VectorUtils.sumVectorElements(moleFraction.ebeDivide(criticalConcs));
		LOG.debug("phase concentration: {}", phaseConcentration);
		return phaseConcentration;
	}
	
	/**
	 * This returns the initial concentrations in the SSIP normalised concentration scale, for all SSIPs.
	 * 
	 * @param moleFractionsBySSIP mole fractions by SSIP
	 * @param vdWVolumePerMolecule VdW volumes per molecule
	 * @param moleFractionByMolecule mole fractions by molecule
	 * @return concentrations
	 */
	public static ArrayRealVector calculateInitialConcentrationsSSIPNorm(ArrayRealVector moleFractionsBySSIP , ArrayRealVector vdWVolumePerMolecule, ArrayRealVector moleFractionByMolecule){
		double totalPhaseConcentration = calculatePhaseConcentration(vdWVolumePerMolecule, moleFractionByMolecule);
		ArrayRealVector initialConcs = (ArrayRealVector) moleFractionsBySSIP.mapDivide(totalPhaseConcentration).mapMultiply(ConcentrationUnit.MOLAR.getConversionFactorTo(ConcentrationUnit.SSIPCONCENTRATION)); 
		LOG.trace("molefractions by SSIP: {}", moleFractionsBySSIP);
		LOG.debug("Initial Concentrations: {}", initialConcs);
		return initialConcs;
	}
	
	/**
	 * Calculate the mole fraction weighted concentration based on the zero point concentrations of the species in the phase. The zero point concentrations need to be in the SSIP normalised scale. 
	 * 
	 * @param moleFractionBySSIP mole fractions by SSIP
	 * @param zeroPointConcentrationsByMolecule zero point concentrations by molecule
	 * @param moleFractionByMolecule mole fractions by molecule
	 * @return concentrations
	 */
	public static ArrayRealVector calculateZeroPointConcentrationFraction(ArrayRealVector moleFractionBySSIP, ArrayRealVector zeroPointConcentrationsByMolecule, ArrayRealVector moleFractionByMolecule){
		return (ArrayRealVector) moleFractionBySSIP.mapDivide(VectorUtils.sumVectorElements(moleFractionByMolecule.ebeDivide(zeroPointConcentrationsByMolecule)));
	}
	
	public static double getVoidVolume() {
		return VOID_VOLUME;
	}

	public static double getPackingCoefficient() {
		return PACKING_COEFFICIENT;
	}

	public static double getAvogadrosConstant() {
		return AVOGADROS_CONSTANT;
	}

	public static double getV1molar() {
		return V_1MOLAR;
	}
}
