/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Constants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.MatrixUtils;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;



/**
 * Class for calculating the free and bound concentrations.
 * 
 * @author mdd31
 *
 */
public class ConcentrationCalculator {
	
	private static final Logger LOG = LogManager.getLogger(ConcentrationCalculator.class);
	
	/**
	 * Association constants for each pairwise interaction between SSIPs.
	 */
	private final Array2DRowRealMatrix associationConstants;
	/**
	 * Total concentrations for each SSIP.
	 */
	private final ArrayRealVector totalConcentrations;
	/**
	 * The concentration of the SSIPs that are not bound to another SSIP.
	 */
	private ArrayRealVector freeConcentrations;
	/**
	 * The concentration of each SSIP that is bound to another SSIP. 
	 */
	private ArrayRealVector boundConcentrationsTotal;
	/**
	 * The concentrations of each SSIP bound to every other SSIP as a 2D matrix. 
	 */
	private Array2DRowRealMatrix boundConcentrations;
	
	/**
	 * Initialise with the association Constants and total concentrations for all SSIPs in the phase.
	 * 
	 * @param associationConstants Association constants.
	 * @param totalConcentrations Total concentrations
	 */
	public ConcentrationCalculator(Array2DRowRealMatrix associationConstants, ArrayRealVector totalConcentrations){
		this.associationConstants = associationConstants;
		this.totalConcentrations = totalConcentrations;
		//upon initialisation assume all SSIPs are unbound.
		setFreeConcentrations(totalConcentrations);
		//upon initialisation set all bound values and totals to zero.
		setBoundConcentrationsTotal(new ArrayRealVector(totalConcentrations.getDimension()));
		setBoundConcentrations(new Array2DRowRealMatrix(totalConcentrations.getDimension(), totalConcentrations.getDimension()));
	}
	
	/**
	 * This constructor is used when the free and bound concentrations have been read in from a file.
	 * 
	 * @param totalConcentrations total concentrations
	 * @param freeConcentrations free concentrations
	 * @param boundConcentrations bound concentrations
	 */
	public ConcentrationCalculator(ArrayRealVector totalConcentrations, ArrayRealVector freeConcentrations, Array2DRowRealMatrix boundConcentrations){
		this.associationConstants = null;
		this.totalConcentrations = totalConcentrations;
		setFreeConcentrations(freeConcentrations);
		setBoundConcentrations(boundConcentrations);
		setBoundConcentrationsTotal(MatrixUtils.calculateRowSums(boundConcentrations));
	}
	
	/**
	 * This calculates the bound concentrations, as described by eqn 6 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>
	 * 
	 * The bound concentrations and total are then set using eqn 7 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper"</a>
	 * In this equation the sum is over [ij] + [ji]. Since eqn 6 produces symmetric answers, this means that twice the row sum can be used.
	 * The bound concentrations and bound concentration totals returned, include this factor of 2.
	 */
	public void calculateBoundConcentrations(){
		//Matrix of [ifree][jfree]
		Array2DRowRealMatrix concentrationProduct = VectorUtils.calculateVectorVectorTMatrix(getFreeConcentrations(), getFreeConcentrations());
		//take the hadamard product with the association constants matrix.
		Array2DRowRealMatrix boundConcentrationValues = MatrixUtils.calculateHadamardProduct(getAssociationConstants(), concentrationProduct);
		//multiply by factor of 2 for summation as [ij] = [ji].
		boundConcentrationValues = (Array2DRowRealMatrix) boundConcentrationValues.scalarMultiply(2.0);
		setBoundConcentrations(boundConcentrationValues);
		LOG.debug("Bound concentrations: {}", boundConcentrationValues);
		LOG.debug("row sums: {}", MatrixUtils.calculateRowSums(boundConcentrationValues));
		setBoundConcentrationsTotal(MatrixUtils.calculateRowSums(boundConcentrationValues));
	}
	/**
	 * This sums the free and total bound concentrations.
	 * 
	 * @return total concentrations
	 */
	public ArrayRealVector calculateTotalConcentration(){
		return getFreeConcentrations().add(getBoundConcentrationsTotal());
	}
	
	/**
	 * Test to see if the sum of free and bound concentrations matches the total concentration.
	 * 
	 * @return concentrations match
	 */
	public boolean totalConcentrationMatches(){
		return VectorUtils.compareVectorsEqualRelativeTolerance(getTotalConcentrations(), calculateTotalConcentration(), Constants.TOLERANCE);
	}
	
	/**
	 * This carries out a normalisation on the Free concentration. This is designed to be used
	 * during the process of finding the bound fractions.
	 *  
	 */
	void normaliseFreeConcentration(){
			ArrayRealVector normalisationMatrix = calculateTotalConcentration().ebeDivide(getTotalConcentrations());
			//we use the square root for normalisation- this is slow, but is supposed to let it decrease
			//monotonically during convergence, instead of oscillate.
			for (int i = 0; i < normalisationMatrix.getDimension(); i++) {
				normalisationMatrix.setEntry(i, Math.pow(normalisationMatrix.getEntry(i), 0.5));
			}
			ArrayRealVector normalisedFreeConcentrations = getFreeConcentrations().ebeDivide(normalisationMatrix);
			
			setFreeConcentrations(normalisedFreeConcentrations);
	}

	/**
	 * This iterates through to get the equilibrium concentrations.
	 */
	public void calculateFreeAndBoundConcentrations(){
		calculateBoundConcentrations();
		boolean converged = totalConcentrationMatches();
		while (!converged) {
			normaliseFreeConcentration();
			calculateBoundConcentrations();
			LOG.debug("CalculatedTotalConcentration: {}", calculateTotalConcentration());
			LOG.debug("FreeConcentration: {}", getFreeConcentrations());
			LOG.debug("BoundConcentrationTotals: {}", getBoundConcentrationsTotal());
			converged = totalConcentrationMatches();
		}
	}
	
	/**
	 * Gets the sum of the total concentrations of each SSIP. This is the total concentration of the phase.
	 * 
	 * @return total concentration
	 */
	public double getTotalConcentration(){
		return VectorUtils.sumVectorElements(getTotalConcentrations());
	}
	
	/**
	 * Get Psi_{free}, the fraction free of the total phase. See mdd31 thesis for more information.
	 * 
	 * @return Psi free
	 */
	public double getPsiFree(){
		return VectorUtils.sumVectorElements(getFreeConcentrations()) / getTotalConcentration();
	}
	
	/**
	 * Get the Psi Free per molecule.
	 * 
	 * @param moleculeIds
	 * @return
	 */
	public ArrayRealVector getPsiFreePerMolecule(List<String> moleculeIds) {
		HashMap<String, Double> freeConcsByID = (HashMap<String, Double>) getFreeConcentrationByMolecule(moleculeIds);
		HashMap<String, Double> totalConcsByID = (HashMap<String, Double>) getTotalConcentrationByMolecule(moleculeIds);
		ArrayRealVector psiFree = new ArrayRealVector(moleculeIds.size());
		for (int i = 0; i < moleculeIds.size(); i++) {
			String molID = moleculeIds.get(i);
			psiFree.setEntry(i, freeConcsByID.get(molID)/totalConcsByID.get(molID));
		}
		return psiFree;
	}
	
	/**
	 * This get the sum of free concentrations of SSIPs by Molecule ID.
	 * 
	 * @param moleculeIds
	 * @return free concentrations by ID
	 */
	public Map<String, Double> getFreeConcentrationByMolecule(List<String> moleculeIds){
		HashMap<String, Double> freeConcsByID = new HashMap<>();
		ArrayRealVector freeConcs = getFreeConcentrations();
		for (int i = 0; i < freeConcs.getDimension(); i++) {
			String moleculeID = moleculeIds.get(i);
			if (freeConcsByID.containsKey(moleculeIds.get(i))) {
				freeConcsByID.put(moleculeID, freeConcsByID.get(moleculeID) + freeConcs.getEntry(i));
			} else {
				freeConcsByID.put(moleculeID, freeConcs.getEntry(i));
			}
		}
		return freeConcsByID;
	}
	
	public Map<String, Double> getTotalConcentrationByMolecule(List<String> moleculeIds){
		HashMap<String, Double> totalConcsByID = new HashMap<>();
		ArrayRealVector totalConcs = getTotalConcentrations();
		for (int i = 0; i < totalConcs.getDimension(); i++) {
			String moleculeID = moleculeIds.get(i);
			if (totalConcsByID.containsKey(moleculeIds.get(i))) {
				totalConcsByID.put(moleculeID, totalConcsByID.get(moleculeID) + totalConcs.getEntry(i));
			} else {
				totalConcsByID.put(moleculeID, totalConcs.getEntry(i));
			}
		}
		return totalConcsByID;
	}
	
	
	public Array2DRowRealMatrix getAssociationConstants() {
		return associationConstants;
	}

	public ArrayRealVector getTotalConcentrations() {
		return totalConcentrations;
	}

	public ArrayRealVector getFreeConcentrations() {
		return freeConcentrations;
	}

	public void setFreeConcentrations(ArrayRealVector freeConcentrations) {
		this.freeConcentrations = freeConcentrations;
	}

	public ArrayRealVector getBoundConcentrationsTotal() {
		return boundConcentrationsTotal;
	}

	public void setBoundConcentrationsTotal(ArrayRealVector boundConcentrationsTotal) {
		this.boundConcentrationsTotal = boundConcentrationsTotal;
	}

	public Array2DRowRealMatrix getBoundConcentrations() {
		return boundConcentrations;
	}

	public void setBoundConcentrations(Array2DRowRealMatrix boundConcentrations) {
		this.boundConcentrations = boundConcentrations;
	}
}
