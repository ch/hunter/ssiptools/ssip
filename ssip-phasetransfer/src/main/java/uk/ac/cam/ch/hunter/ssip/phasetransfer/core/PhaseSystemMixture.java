/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters.PhaseSystemMapAdapterForPhaseSystemMixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IEnergyDifferenceCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IPhaseSystemCollection;

/**
 * Class for storing {@link PhaseSystem}s with different solvents but the same solute in the Liquid (Condensed) phase.
 * 
 * @author mdd31
 *
 */
@XmlRootElement(name = "PhaseSystemMixture", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class PhaseSystemMixture implements IPhaseSystemCollection, IEnergyDifferenceCalculator{

	@XmlTransient
	private static final Logger LOG = LogManager.getLogger(PhaseSystemMixture.class);
	
	@XmlElement(name = "PhaseSystems", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlJavaTypeAdapter(PhaseSystemMapAdapterForPhaseSystemMixture.class)
	private HashMap<String, PhaseSystem> phaseSystemMap;
	
	@XmlAttribute(name = "soluteID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String soluteID;
	
	public PhaseSystemMixture(){
		/**
		 * JAXB constructor
		 */
	}
	
	/**
	 * Constructor for {@link PhaseSystemMixture} to initialise with map of {@link PhaseSystem}s.
	 * 
	 * @param phaseSystemMap phase system map
	 */
	public PhaseSystemMixture(Map<String, PhaseSystem> phaseSystemMap){
		this.phaseSystemMap = (HashMap<String, PhaseSystem>) phaseSystemMap;
	}

	/**
	 * Set Association Constants upon unmarshalling.
	 */
	@Override
	public void setAssociationConstants(){
		for (Map.Entry<String, PhaseSystem> mixtureMapEntry : getPhaseSystemMap().entrySet()) {
			mixtureMapEntry.getValue().setAssociationConstants();
		}
	}

	@Override
	public void setBoundConcentrations(){
		for (Map.Entry<String, PhaseSystem> mixtureMapEntry : getPhaseSystemMap().entrySet()) {
			mixtureMapEntry.getValue().setBoundConcentrations();
		}
	}
	
	@Override
	public double calculateConfinementEnergyDifference(String moleculeID, String fromPhase, String toPhase){
		double confinementEnergyPhase1 = getPhaseSystemMap().get(fromPhase).getCondensedPhase().calculateConfinementEnergy(moleculeID);
		double confinementEnergyPhase2 = getPhaseSystemMap().get(toPhase).getCondensedPhase().calculateConfinementEnergy(moleculeID);
		return confinementEnergyPhase2 - confinementEnergyPhase1;
	}
	
	@Override
	public ArrayRealVector calculateConfinementEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase){
		ArrayRealVector confinementEnergyMatrixFromPhase = getPhaseSystemMap().get(fromPhase).getCondensedPhase().calculateConfinementEnergyPerSSIPMatrix(moleculeID);
		ArrayRealVector confinementEnergyMatrixToPhase = getPhaseSystemMap().get(toPhase).getCondensedPhase().calculateConfinementEnergyPerSSIPMatrix(moleculeID);
		return confinementEnergyMatrixToPhase.subtract(confinementEnergyMatrixFromPhase);
	}
	

	@Override
	public EnergyValue calculateConfinementEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateConfinementEnergyDifference(moleculeID, fromPhase, toPhase), calculateConfinementEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLEFRACTION);
	}
	
	@Override
	public List<EnergyValue> calculateConfinementEnergyDifferenceToPhase(String moleculeID, String toPhase) {
		ArrayList<EnergyValue> confinementEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseSystemMap().keySet()) {
			confinementEnergyDifferenceList.add(calculateConfinementEnergyDifferenceValue(moleculeID, fromPhase, toPhase));
		}
		return confinementEnergyDifferenceList;
	}
	
	@Override
	public List<EnergyValue> calculateConfinementEnergyDifferenceBetweenAllPhases(String moleculeID){
		ArrayList<EnergyValue> completeConfinementEnergyDifferenceMap = new ArrayList<>();
		for (String toPhase : getPhaseSystemMap().keySet()) {
			completeConfinementEnergyDifferenceMap.addAll(calculateConfinementEnergyDifferenceToPhase(moleculeID, toPhase));
		}
		return completeConfinementEnergyDifferenceMap;
	}
	
	@Override
	public double calculateConfinementEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase){
		double confinementEnergy = calculateConfinementEnergyDifference(moleculeID, fromPhase, toPhase);
		double conversionFactor = calculateConversionFactorTo1MScale(toPhase, fromPhase);
		return confinementEnergy + conversionFactor;
	}
	
	@Override
	public ArrayRealVector calculateConfinementEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase){
		return calculateConfinementEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase);
	}
	
	@Override
	public EnergyValue calculateConfinementEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateConfinementEnergyDifferenceFor1M(moleculeID, fromPhase, toPhase), calculateConfinementEnergyDifferenceFor1MPerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLAR);
	}
	
	@Override
	public List<EnergyValue> calculateConfinementEnergyDifferenceToPhase1M(String moleculeID, String toPhase) {
		ArrayList<EnergyValue> confinementEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseSystemMap().keySet()) {
			confinementEnergyDifferenceList.add(calculateConfinementEnergyDifferenceValue1M(moleculeID, fromPhase, toPhase));
		}
		return confinementEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergyDifferenceBetweenAllPhases1M(String moleculeID){
		ArrayList<EnergyValue> completeConfinementEnergyDifferenceMap = new ArrayList<>();
		for (String toPhase : getPhaseSystemMap().keySet()) {
			completeConfinementEnergyDifferenceMap.addAll(calculateConfinementEnergyDifferenceToPhase1M(moleculeID, toPhase));
		}
		return completeConfinementEnergyDifferenceMap;
	}

	@Override
	public double calculateConfinementEnergySolvation(String moleculeID, String phaseID){
		return getPhaseSystemMap().get(phaseID).getCondensedPhase().calculateConfinementEnergy(moleculeID);
	}

	@Override
	public ArrayRealVector calculateConfinementEnergySolvationPerSSIP(String moleculeID, String phaseID){
		return getPhaseSystemMap().get(phaseID).getCondensedPhase().calculateConfinementEnergyPerSSIPMatrix(moleculeID);
	}

	@Override
	public EnergyValue calculateConfinementEnergySolvationEnergyValue(String moleculeID, String phaseID){
		return new EnergyValue(moleculeID, phaseID, "", calculateConfinementEnergySolvation(moleculeID, phaseID), calculateConfinementEnergySolvationPerSSIP(moleculeID, phaseID), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergySolvationAllPhases(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (String phaseID : getPhaseSystemMap().keySet()) {
			energyValues.add(calculateConfinementEnergySolvationEnergyValue(moleculeID, phaseID));
		}
		return energyValues;
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergyValuesAllPhasesAllValues(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		energyValues.addAll(calculateConfinementEnergySolvationAllPhases(moleculeID));
		energyValues.addAll(calculateConfinementEnergyDifferenceBetweenAllPhases(moleculeID));
		energyValues.addAll(calculateConfinementEnergyDifferenceBetweenAllPhases1M(moleculeID));
		return energyValues;
	}

	@Override
	public double calculateBindingEnergyDifference(String moleculeID, String fromPhase, String toPhase){
		double bindingEnergyPhase1 = getPhaseSystemMap().get(fromPhase).getCondensedPhase().calculateBindingEnergy(moleculeID);
		double bindingEnergyPhase2 = getPhaseSystemMap().get(toPhase).getCondensedPhase().calculateBindingEnergy(moleculeID);
		return bindingEnergyPhase2 - bindingEnergyPhase1;
	}

	@Override
	public ArrayRealVector calculateBindingEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase){
		ArrayRealVector bindingEnergyMatrixFromPhase = getPhaseSystemMap().get(fromPhase).getCondensedPhase().calculateBindingEnergyPerSSIP(moleculeID);
		ArrayRealVector bindingEnergyMatrixToPhase = getPhaseSystemMap().get(toPhase).getCondensedPhase().calculateBindingEnergyPerSSIP(moleculeID);
		return bindingEnergyMatrixToPhase.subtract(bindingEnergyMatrixFromPhase);
	}

	@Override
	public EnergyValue calculateBindingEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateBindingEnergyDifference(moleculeID, fromPhase, toPhase), calculateBindingEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyDifferenceToPhase(String moleculeID, String toPhase){
		ArrayList<EnergyValue> bindingEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseSystemMap().keySet()) {
			bindingEnergyDifferenceList.add(calculateBindingEnergyDifferenceValue(moleculeID, fromPhase, toPhase));
		}
		return bindingEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyDifferenceBetweenAllPhases(String moleculeID){
		ArrayList<EnergyValue> completeBindingEnergyDifferenceList = new ArrayList<>();
		for (String toPhase : getPhaseSystemMap().keySet()) {
			completeBindingEnergyDifferenceList.addAll(calculateBindingEnergyDifferenceToPhase(moleculeID, toPhase));
		}
		return completeBindingEnergyDifferenceList;
	}

	@Override
	public double calculateBindingEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase){
		double bindingEnergy = calculateBindingEnergyDifference(moleculeID, fromPhase, toPhase);
		double conversionFactor = calculateConversionFactorTo1MScale(toPhase, fromPhase);
		return bindingEnergy + conversionFactor;
	}

	@Override
	public ArrayRealVector calculateBindingEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase){
		return calculateBindingEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase);
	}

	@Override
	public EnergyValue calculateBindingEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateBindingEnergyDifferenceFor1M(moleculeID, fromPhase, toPhase), calculateBindingEnergyDifferenceFor1MPerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLAR);
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyDifferenceToPhase1M(String moleculeID, String toPhase){
		ArrayList<EnergyValue> bindingEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseSystemMap().keySet()) {
			bindingEnergyDifferenceList.add(calculateBindingEnergyDifferenceValue1M(moleculeID, fromPhase, toPhase));
		}
		return bindingEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyDifferenceBetweenAllPhases1M(String moleculeID){
		ArrayList<EnergyValue> completeBindingEnergyDifferenceList = new ArrayList<>();
		for (String toPhase : getPhaseSystemMap().keySet()) {
			completeBindingEnergyDifferenceList.addAll(calculateBindingEnergyDifferenceToPhase1M(moleculeID, toPhase));
		}
		return completeBindingEnergyDifferenceList;
	}

	@Override
	public double calculateBindingEnergySolvation(String moleculeID, String phaseID){
		return getPhaseSystemMap().get(phaseID).getCondensedPhase().calculateBindingEnergy(moleculeID);
	}

	@Override
	public ArrayRealVector calculateBindingEnergySolvationPerSSIP(String moleculeID, String phaseID){
		return getPhaseSystemMap().get(phaseID).getCondensedPhase().calculateBindingEnergyPerSSIP(moleculeID);
	}

	@Override
	public EnergyValue calculateBindingEnergySolvationEnergyValue(String moleculeID, String phaseID){
		return new EnergyValue(moleculeID, phaseID, "", calculateBindingEnergySolvation(moleculeID, phaseID), calculateBindingEnergySolvationPerSSIP(moleculeID, phaseID), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateBindingEnergySolvationAllPhases(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (String phaseID : getPhaseSystemMap().keySet()) {
			energyValues.add(calculateBindingEnergySolvationEnergyValue(moleculeID, phaseID));
		}
		return energyValues;
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyValuesAllPhasesAllValues(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		energyValues.addAll(calculateBindingEnergySolvationAllPhases(moleculeID));
		energyValues.addAll(calculateBindingEnergyDifferenceBetweenAllPhases(moleculeID));
		energyValues.addAll(calculateBindingEnergyDifferenceBetweenAllPhases1M(moleculeID));
		return energyValues;
	}

	@Override
	public double calculateFreeEnergyDifference(String moleculeID, String fromPhase1, String toPhase){
		double freeEnergyPhase1 = getPhaseSystemMap().get(fromPhase1).getCondensedPhase().calculateEnergyInPhase(moleculeID);
		double freeEnergyPhase2 = getPhaseSystemMap().get(toPhase).getCondensedPhase().calculateEnergyInPhase(moleculeID);
		return freeEnergyPhase2 - freeEnergyPhase1;
	}

	@Override
	public ArrayRealVector calculateFreeEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase){
		ArrayRealVector freeEnergyMatrixFromPhase = getPhaseSystemMap().get(fromPhase).getCondensedPhase().calculateEnergyInPhaseBySSIP(moleculeID);
		ArrayRealVector freeEnergyMatrixToPhase = getPhaseSystemMap().get(toPhase).getCondensedPhase().calculateEnergyInPhaseBySSIP(moleculeID);
		return freeEnergyMatrixToPhase.subtract(freeEnergyMatrixFromPhase);
	}

	@Override
	public EnergyValue calculateFreeEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateFreeEnergyDifference(moleculeID, fromPhase, toPhase), calculateFreeEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyDifferenceToPhase(String moleculeID, String toPhase){
		ArrayList<EnergyValue> freeEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseSystemMap().keySet()) {
			freeEnergyDifferenceList.add(calculateFreeEnergyDifferenceValue(moleculeID, fromPhase, toPhase));			
		}
		return freeEnergyDifferenceList;
	}
	
	@Override
	public List<EnergyValue> calculateFreeEnergyDifferenceBetweenAllPhases(String moleculeID){
		ArrayList<EnergyValue> completeFreeEnergyDifferenceList = new ArrayList<>();
		for (String toPhase : getPhaseSystemMap().keySet()) {
			completeFreeEnergyDifferenceList.addAll(calculateFreeEnergyDifferenceToPhase(moleculeID, toPhase));
		}
		return completeFreeEnergyDifferenceList;
	}

	@Override
	public double calculateFreeEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase){
		double freeEnergy = calculateFreeEnergyDifference(moleculeID, fromPhase, toPhase);
		double conversionFactor = calculateConversionFactorTo1MScale(toPhase, fromPhase);
		return freeEnergy + conversionFactor;
	}

	@Override
	public ArrayRealVector calculateFreeEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase){
		return calculateFreeEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase);
	}

	@Override
	public EnergyValue calculateFreeEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateFreeEnergyDifferenceFor1M(moleculeID, fromPhase, toPhase), calculateFreeEnergyDifferenceFor1MPerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLAR);
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyDifferenceToPhase1M(String moleculeID, String toPhase){
		ArrayList<EnergyValue> freeEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseSystemMap().keySet()) {
			freeEnergyDifferenceList.add(calculateFreeEnergyDifferenceValue1M(moleculeID, fromPhase, toPhase));			
		}
		return freeEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyDifferenceBetweenAllPhases1M(String moleculeID){
		ArrayList<EnergyValue> completeFreeEnergyDifferenceList = new ArrayList<>();
		for (String toPhase : getPhaseSystemMap().keySet()) {
			completeFreeEnergyDifferenceList.addAll(calculateFreeEnergyDifferenceToPhase1M(moleculeID, toPhase));
		}
		return completeFreeEnergyDifferenceList;
	}

	@Override
	public double calculateFreeEnergySolvation(String moleculeID, String phaseID){
		return getPhaseSystemMap().get(phaseID).getCondensedPhase().calculateEnergyInPhase(moleculeID);
	}

	@Override 
	public ArrayRealVector calculateFreeEnergySolvationPerSSIP(String moleculeID, String phaseID){
		return getPhaseSystemMap().get(phaseID).getCondensedPhase().calculateEnergyInPhaseBySSIP(moleculeID);
	}

	@Override
	public EnergyValue calculateFreeEnergySolvationEnergyValue(String moleculeID, String phaseID){
		LOG.debug("free energy per SSIP: {}", calculateFreeEnergySolvationPerSSIP(moleculeID, phaseID));
		return new EnergyValue(moleculeID, phaseID, "", calculateFreeEnergySolvation(moleculeID, phaseID), calculateFreeEnergySolvationPerSSIP(moleculeID, phaseID), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateFreeEnergySolvationAllPhases(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (String phaseID : getPhaseSystemMap().keySet()) {
			energyValues.add(calculateFreeEnergySolvationEnergyValue(moleculeID, phaseID));
		}
		return energyValues;
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyValuesAllPhasesAllValues(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		energyValues.addAll(calculateFreeEnergySolvationAllPhases(moleculeID));
		energyValues.addAll(calculateFreeEnergyDifferenceBetweenAllPhases(moleculeID));
		energyValues.addAll(calculateFreeEnergyDifferenceBetweenAllPhases1M(moleculeID));
		return energyValues;
	}
	
	@Override
	public List<AssociationEnergyValue> calculateAssociationEnergyValuesAllPhases() {
		ArrayList<AssociationEnergyValue> associationEnergyValues = new ArrayList<>();
		for (PhaseSystem phaseSystem : getPhaseSystemMap().values()) {
			LOG.debug("phaseSystem ID: {}", phaseSystem.getMixtureID());
			LOG.debug("phaseSystem Condensed ID: {}", phaseSystem.getCondensedPhase().getSolventID());
			associationEnergyValues.addAll(phaseSystem.getCondensedPhase().calculateMoleculeAssociationEnergyValues());
		}
		return associationEnergyValues;
	}

	@Override
	public List<EnergyValue> calculateLogPAllPhases(String moleculeID){
		ArrayList<EnergyValue> logPvalues = new ArrayList<>();
		for (String toPhase : getPhaseSystemMap().keySet()) {
			for (String fromPhase : getPhaseSystemMap().keySet()) {
				logPvalues.add(calculateLogPValue(moleculeID, fromPhase, toPhase));
			}
		}
		return logPvalues;
	}
	
	@Override
	public EnergyValue calculateLogPValue(String moleculeID, String fromPhase, String toPhase) {
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateLogP(moleculeID, fromPhase, toPhase), calculateFreeEnergyDifferenceFor1MPerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLAR);
	}
	
	@Override
	public double calculateLogP(String moleculeID, String fromPhase, String toPhase) {
		double energy1M = calculateFreeEnergyDifferenceFor1M(moleculeID, fromPhase, toPhase);
		double rT = Constants.GAS_CONSTANT * getPhaseSystemMap().get(fromPhase).getCondensedPhase().getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue();
		double logeto10conversion = Math.log10(Math.E);
		LOG.debug("conversion factor: {}", logeto10conversion);
		LOG.trace("value: {}", -(energy1M*logeto10conversion)/rT);
		return -(energy1M*logeto10conversion)/rT;
	}

	@Override
	public double calculateConversionFactorTo1MScale(String toPhase, String fromPhase){
		double factorToPhase = getPhaseSystemMap().get(toPhase).getCondensedPhase().calculateConversionTo1MScale();
		double factorFromPhase = getPhaseSystemMap().get(fromPhase).getCondensedPhase().calculateConversionTo1MScale();
		return factorToPhase - factorFromPhase;
	}
	
	@Override
	public Map<String, PhaseSystem> getPhaseSystemMap() {
		return phaseSystemMap;
	}

	@Override
	public void setPhaseSystemMap(Map<String, PhaseSystem> phaseSystemMap) {
		this.phaseSystemMap = (HashMap<String, PhaseSystem>) phaseSystemMap;
	}

	public String getSoluteID() {
		return soluteID;
	}

	public void setSoluteID(String soluteID) {
		this.soluteID = soluteID;
	}
}
