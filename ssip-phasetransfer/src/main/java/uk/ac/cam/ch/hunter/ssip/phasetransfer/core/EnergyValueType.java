/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * This enum is used to distinguish between the different types of energies that will be produced from calculation:
 * 
 * energies will either be expressed as free energy of transfer for a constant mole fraction, or as free energy change for 1M.
 * 
 * @author mdd31
 *
 */
@XmlType
@XmlEnum(String.class)
public enum EnergyValueType {
	@XmlEnumValue("MOLEFRACTION")
	MOLEFRACTION("MOLEFRACTION"){
		
	},
	@XmlEnumValue("MOLAR")
	MOLAR("MOLAR"){
		
	};
	
	private final String valueType;
	
	private EnergyValueType(String valueType){
		this.valueType = valueType;
	}
	
	public String getValueType(){
		return this.valueType;
	}
}
