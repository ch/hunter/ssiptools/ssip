/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Class for representing the information about a bound concentration for a
 * SSIP. This is to aid in marshalling of the information with JAXB.
 * 
 * @author mdd31
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class BoundConcentration extends Concentration implements
		Comparable<BoundConcentration> {

	@XmlAttribute(name = "ssipID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final int ssipRef;

	/**
	 * constructor for JAXB.
	 */
	public BoundConcentration() {
		this.ssipRef = Integer.MAX_VALUE;
	}

	/**
	 * Constructor for Bound Concentration. This requires the value of the concentration, the unit and also the id of the SSIP this is bound to.
	 * 
	 * @param value Value of concentration.
	 * @param unit Unit of concentration.
	 * @param ssipid ID of the SSIP this is bound to.
	 */
	public BoundConcentration(double value, ConcentrationUnit unit, int ssipid) {
		super(value, unit);
		this.ssipRef = ssipid;
	}

	@Override
	public int compareTo(BoundConcentration other) {
		int ssipIdConc1 = this.getSsipRef();
		int ssipIdConc2 = other.getSsipRef();
		if (ssipIdConc1 == ssipIdConc2) {
			return 0;
		} else if (ssipIdConc1 < ssipIdConc2) {
			return -1;
		} else {
			return 1;
		}
	}

	public int getSsipRef() {
		return ssipRef;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}
		BoundConcentration other = (BoundConcentration) obj;
		return this.getSsipRef() == other.getSsipRef();
	}
	
	@Override
	public int hashCode(){
		if (!hashCodeFound) {
			int prime = 37;
			int result = super.hashCode();
			result = result * prime + getSsipRef();
			hash = result;
			hashCodeFound = true;
		}
		return hash;
	}

}
