/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Enumeration of the units for the Concentration
 * 
 * @author mdd31
 *
 */
@XmlType
@XmlEnum(String.class)
public enum ConcentrationUnit {

	@XmlEnumValue("SSIPConcentrationNormalised")
	SSIPCONCENTRATION ("SSIPConcentrationNormalised") {

		@Override
		public String getConcentrationUnit() {
			return this.getUnit();
		}

		@Override
		public double getConversionFactorTo(ConcentrationUnit convertTo) {
			if (this == convertTo) {
				return 1.0;
			} else if (convertTo == MOLAR){
				return CONVERT_FROM_SSIPCONCENTRATION_TO_M;
			}else {
				return 0;
			}
		}
		
	},

	@XmlEnumValue("MOLAR")
	MOLAR("MOLAR") {
		
		@Override
		public String getConcentrationUnit() {
			return this.getUnit();
		}

		@Override
		public double getConversionFactorTo(ConcentrationUnit convertTo) {
			if (this == convertTo) {
				return 1.0;
			} else if (convertTo == SSIPCONCENTRATION){
				return CONVERT_FROM_M_TO_SSIPCONCENTRATION;
			}else {
				return 0;
			}
		}
	};
	
		
	private final String unit;
	
	private static final double CONVERT_FROM_SSIPCONCENTRATION_TO_M = 300.0;
	private static final double CONVERT_FROM_M_TO_SSIPCONCENTRATION = 1.0/300.0;
	
	private ConcentrationUnit(String unit){
		this.unit = unit;
	}
	
	public abstract String getConcentrationUnit();
	
	/**
	 * This returns the conversion factor for conversion between the current unit and the unit to convert to.
	 * 
	 * @param convertTo Contentration unit one is converting to.
	 * @return Conversion factor associated with convertTo unit.
	 */
	public abstract double getConversionFactorTo(ConcentrationUnit convertTo);

	public String getUnit() {
		return unit;
	}
	
}
