/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

/**
 *This class contains a value for the temperature, as well as a unit, and with methods for conversion.
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Temperature {

	@XmlValue
	private final double temperatureValue;
	@XmlAttribute(name = "units", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final TemperatureUnit temperatureUnit;

	@XmlTransient
	private static final double ERROR = 1e-7;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;

	/**
	 * Constructor for JAXB.
	 */
	public Temperature(){
		this.temperatureValue = Double.NaN;
		this.temperatureUnit = null;
	}
	
	/**
	 * Constructor takes value and unit.
	 * 
	 * @param temperatureValue value
	 * @param temperatureUnit unit
	 */
	public Temperature(double temperatureValue, TemperatureUnit temperatureUnit){
		this.temperatureValue = temperatureValue;
		this.temperatureUnit = temperatureUnit;
	}
	
	@Override
	public String toString(){
		return getTemperatureValue() + getTemperatureUnit().getTemperatureUnit();
	}
	
	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(temperatureValue);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = temperatureUnit.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));

			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Temperature other = (Temperature) obj;
		return this.getTemperatureUnit().equals(other.getTemperatureUnit()) && Math.abs(this.getTemperatureValue() - other.getTemperatureValue()) < ERROR;
	}
	
	/**
	 * Method converts Temperature to the given unit.
	 * 
	 * @param temperatureUnit unit
	 * @return temperature
	 */
	public Temperature convertTo(TemperatureUnit temperatureUnit){
		double convertedValue = getTemperatureValue() + getTemperatureUnit().getConversionFactorTo(temperatureUnit);
		return new Temperature(convertedValue, temperatureUnit);
	}
	public double getTemperatureValue() {
		return temperatureValue;
	}

	public TemperatureUnit getTemperatureUnit() {
		return temperatureUnit;
	}
}
