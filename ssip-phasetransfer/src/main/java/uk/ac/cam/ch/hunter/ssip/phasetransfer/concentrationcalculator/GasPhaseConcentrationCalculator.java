/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;

/**
 * Concentration matcher for a gas phase. extends {@link ConcentrationMatcher} This also has methods to get concentrations by molecule.
 * 
 * @author mdd31
 *
 */
public class GasPhaseConcentrationCalculator extends ConcentrationMatcher{

	/**
	 * Constructor uses super from {@link ConcentrationMatcher}.
	 * 
	 * @param concentrationCalculator concentration calculator
	 * @param ssipConcentrations SSIP concentrations
	 */
	public GasPhaseConcentrationCalculator(
			ConcentrationCalculator concentrationCalculator,
			List<SsipConcentration> ssipConcentrations) {
		super(concentrationCalculator, ssipConcentrations);
	}
	
	private void setTotalConcentrations(){
		ArrayList<SsipConcentration> ssipConcentrationsWithTotalConc = new ArrayList<>();
		ArrayRealVector totalConcentrations = getConcentrationCalculator().getTotalConcentrations();
		
		for (int i = 0; i < getSsipConcentrationList().size(); i++) {
			ssipConcentrationsWithTotalConc.add(new SsipConcentration(getSsipConcentrationList().get(i), new Concentration(totalConcentrations.getEntry(i), ConcentrationUnit.SSIPCONCENTRATION), getSsipConcentrationList().get(i).getMoleculeID()));
		}
		setSsipConcentrationList(ssipConcentrationsWithTotalConc);
	}
	
	/**
	 * Sets all concentrations (total, free and bound) for all SSIPs in this phase. First does speciation calculation.
	 */
	@Override
	public void setConcentrations(){
		getConcentrationCalculator().calculateFreeAndBoundConcentrations();
		setTotalConcentrations();
		setFreeConcentrations();
		setBoundConcentrationTotals();
		setBoundConcentrationLists();
	}
	
	/**
	 * This returns the {@link SsipConcentration} elements grouped by corresponding molecule.
	 * 
	 * @return concentrations
	 */
	public Map<String, ArrayList<SsipConcentration>> getConcentrationsByMolID(){
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getSsipConcentrationList();
		HashMap<String, ArrayList<SsipConcentration>> ssipConcsByMolId = new HashMap<>();
		for (SsipConcentration ssipConcentration : ssipConcentrations) {
			if (!ssipConcsByMolId.keySet().contains(ssipConcentration.getMoleculeID())) {
				ArrayList<SsipConcentration> molSsipConcentrations = new ArrayList<>();
				molSsipConcentrations.add(ssipConcentration);
				ssipConcsByMolId.put(ssipConcentration.getMoleculeID(), molSsipConcentrations);
			} else {
				ssipConcsByMolId.get(ssipConcentration.getMoleculeID()).add(ssipConcentration);
			}
		}
		return ssipConcsByMolId;
	}
}
