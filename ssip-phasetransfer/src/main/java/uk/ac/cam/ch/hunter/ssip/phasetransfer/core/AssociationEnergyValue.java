/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@XmlRootElement(name = "AssociationEnergyValue", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class AssociationEnergyValue {

	@XmlTransient
	private static final Logger LOG = LogManager.getLogger(AssociationEnergyValue.class);
	@XmlTransient
	private static final double ERROR = 1e-5;
	
	@XmlAttribute(name = "moleculeID1", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String moleculeID1;
	@XmlAttribute(name = "moleculeID2", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String moleculeID2;
	@XmlAttribute(name = "solventID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String solventID;
	@XmlAttribute(name ="value", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final double value;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	private int hash;
	@XmlTransient
	private boolean hashCodeFound = false;
	
	public AssociationEnergyValue() {
		this.moleculeID1 = null;
		this.moleculeID2 = null;
		this.solventID = null;
		this.value = Double.NaN;
	}
	
	public AssociationEnergyValue(String moleculeID1, String moleculeID2, String solventID, double value) {
		this.moleculeID1 = moleculeID1;
		this.moleculeID2 = moleculeID2;
		this.solventID = solventID;
		this.value = value;
	}
	
	public String getMoleculeID1() {
		return moleculeID1;
	}

	public String getMoleculeID2() {
		return moleculeID2;
	}

	public String getSolventID() {
		return solventID;
	}

	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		return String
				.format("moleculeID1: %s moleculeID2: %s solventID: %s value: %.9f",
						getMoleculeID1(), getMoleculeID2(), getSolventID(),
						getValue());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssociationEnergyValue other = (AssociationEnergyValue) obj;
		if (this.getMoleculeID1().equals(other.getMoleculeID1())
				&& this.getMoleculeID2().equals(other.getMoleculeID2())
				&& this.getSolventID().equals(other.getSolventID())) {
			LOG.debug("Descriptors equal");
			return Math.abs(this.getValue() - other.getValue()) < ERROR;
		} else {
			LOG.debug("descriptors not equal");
			LOG.debug(this);
			LOG.debug(other);
			LOG.debug("molID1 comparison: {}", this.getMoleculeID1() == other.getMoleculeID1());
			LOG.debug("molID2 comparison: {}", this.getMoleculeID2().equals(other.getMoleculeID2()));
			LOG.debug("solventID comparison: {}", this.getSolventID() == other.getSolventID());
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 23;
			int result = 1;
			result = prime * result + getMoleculeID1().hashCode()
					+ getMoleculeID2().hashCode() + getSolventID().hashCode();
			hash = result;
			hashCodeFound = true;
		}
		return hash;
	}
}
