/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.math3.analysis.function.Log;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstantsFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters.PhaseMoleculeMapAdapter;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.comparators.SSIPConcentrationComparator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IPhase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

/**
 * Class for storing the information about the SSIPs and concentrations for all
 * molecule which constitute the phase. From this the AssociationConstants and
 * then free and bound concentrations can be calculated.
 * 
 * @author mdd31
 * 
 */
@XmlRootElement(name = "Phase", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.NONE)
public class Phase implements IPhase{

	private static final Logger LOG = LogManager.getLogger(Phase.class);
	
	@XmlElement(name = "Molecules", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlJavaTypeAdapter(PhaseMoleculeMapAdapter.class)
	private HashMap<String, PhaseMolecule> phaseMoleculesMap;

	@XmlAttribute(name = "units", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final ConcentrationUnit concentrationUnit;
	@XmlElement(name = "Temperature", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final Temperature temperature;

	@XmlTransient
	private AssociationConstants associationConstants;
	
	@XmlAttribute(name = "solventID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String solventID;

	@XmlAttribute( name = "phaseType", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private PhaseType phaseType;
	/**
	 * Constructor for JAXB.
	 */
	public Phase(){
		this.concentrationUnit = null;
		this.temperature = null;
		this.phaseMoleculesMap = null;
		
	}
	
	/**
	 * Creates an instance of a Phase object with the given collection of
	 * SSIPConcentrationLists.
	 * 
	 * @param ssipConcentrationListByMolecule SSIPs by molecule
	 * @param temperature temperature
	 * @param concentrationUnit concentration unit
	 */
	public Phase(Collection<PhaseMolecule> ssipConcentrationListByMolecule,
			Temperature temperature, ConcentrationUnit concentrationUnit) {
		this.temperature = temperature;
		this.concentrationUnit = concentrationUnit;
		this.phaseMoleculesMap = (HashMap<String, PhaseMolecule>) getSSIPConcentrationListwithMoleculeID(ssipConcentrationListByMolecule);
		LOG.debug("molecule map: {}", this.phaseMoleculesMap);
		setAssociationConstants();
	}

	/**
	 * Constructor which takes the map of SSIPConcentrationLists by moleculeID.
	 * 
	 * @param ssipConcentrationListsByMoleculeID SSIPs by molecule
	 * @param temperature temperature
	 * @param concentrationUnit concentration unit
	 */
	public Phase(Map<String, PhaseMolecule> ssipConcentrationListsByMoleculeID, Temperature temperature, ConcentrationUnit concentrationUnit){
		this.temperature = temperature;
		this.concentrationUnit = concentrationUnit;
		this.phaseMoleculesMap =  (HashMap<String, PhaseMolecule>) ssipConcentrationListsByMoleculeID;
		setAssociationConstants();
		
	}
	
	/**
	 * Constructor to create a copy of a phase object.
	 * 
	 * @param phase phase
	 */
	public Phase(Phase phase) {
		this.temperature = phase.getTemperature();
		this.concentrationUnit = phase.getConcentrationUnit();
		HashSet<PhaseMolecule> phaseMoleculesSet = new HashSet<>();
		for (PhaseMolecule phaseMolecule : phase.getPhaseMoleculesSet()) {
			phaseMoleculesSet.add(new PhaseMolecule(phaseMolecule));
		}
		this.phaseMoleculesMap = (HashMap<String, PhaseMolecule>) getSSIPConcentrationListwithMoleculeID(phaseMoleculesSet);
		try {
			this.solventID = getSolventID();
		} catch (NullPointerException e) {
			LOG.info("No solventID: {}", e.getMessage());
		}
		try {
			this.phaseType = getPhaseType();
		} catch (NullPointerException e) {
			LOG.info("No phase type: {}", e.getMessage());
		}
	}
	
	/**
	 * Convert Concentration unit for the concentrations within the phase.
	 * 
	 * @param concentrationUnit concentration unit
	 * @return phase
	 */
	@Override
	public Phase convertConcentrationUnitTo(ConcentrationUnit concentrationUnit){
		HashMap<String, PhaseMolecule> convertedSSIPConcentrationListsByMoleculeID = new HashMap<>();
		for (Map.Entry<String, PhaseMolecule> mapEntry : getPhaseMolecules().entrySet()) {
			convertedSSIPConcentrationListsByMoleculeID.put(mapEntry.getKey(), mapEntry.getValue().convertConcentrationsTo(concentrationUnit));
		}
		Phase convertedPhase = new Phase(convertedSSIPConcentrationListsByMoleculeID, getTemperature(), concentrationUnit);
		try {
			convertedPhase.setSolventID(getSolventID());
		} catch (Exception e) {
			LOG.info("No solventID: {}", e.getMessage());
		}
		try {
			convertedPhase.setPhaseType(getPhaseType());
		} catch (NullPointerException e) {
			LOG.info("No phase type: {}", e.getMessage());
		}
		return convertedPhase;
	}
	
	private Map<String, PhaseMolecule> getSSIPConcentrationListwithMoleculeID(Collection<PhaseMolecule> ssipConcentrationListByMolecule) {
		HashMap<String, PhaseMolecule> moleculeIDSSIPConcentrationListMap = new HashMap<>();
		for (PhaseMolecule ssipConcentrationList : ssipConcentrationListByMolecule) {
			if (ssipConcentrationList.getMoleculeConcentrationUnit().equals(getConcentrationUnit())) {
				moleculeIDSSIPConcentrationListMap.put(
						ssipConcentrationList.getMoleculeID(),
						ssipConcentrationList);
			} else {
				throw new UnsupportedOperationException("Unit conversion not supported.");
			}
			
		}
		return moleculeIDSSIPConcentrationListMap;
	}
	
	private ArrayList<SsipConcentration> sortSSIPConcentrations(
			Set<PhaseMolecule> phaseMolecules) {
		ArrayList<SsipConcentration> sortedSSIPConcentrations = new ArrayList<>();
		HashMap<String, Point3D> centroidMap = new HashMap<>();
		for (PhaseMolecule phaseMolecule : phaseMolecules) {
			ArrayList<SsipConcentration> ssipConcentrationValues = (ArrayList<SsipConcentration>) phaseMolecule.getSSIPConcentrations();
			LOG.debug("SSIP Concentrations: {}", ssipConcentrationValues);
			sortedSSIPConcentrations.addAll(ssipConcentrationValues);
			centroidMap.put(phaseMolecule.getMoleculeID(), phaseMolecule.getMoleculeCentroid());
		}
		
		Comparator<SsipConcentration> comparator = new SSIPConcentrationComparator(centroidMap);
		Collections.sort(sortedSSIPConcentrations, comparator);
		return sortedSSIPConcentrations;
	}
	
	private AssociationConstants generateAssociationConstants(List<SsipConcentration> ssipConcentrations, Temperature temperature){
		AssociationConstantsFactory assocationConstantsFactory = new AssociationConstantsFactory(
				temperature.convertTo(TemperatureUnit.KELVIN).getTemperatureValue());
		return assocationConstantsFactory.getAssociationConstants(ssipConcentrations);
	}
	
	/**
	 * Method to calculate Association constants after unmarshalling from file.
	 */
	public void setAssociationConstants(){
		this.associationConstants = generateAssociationConstants(getsortedSSIPConcentrations(), getTemperature());
	}
	
	/**
	 * Set the bound concentration {@link ArrayRealVector} from the {@link ArrayList} of {@link BoundConcentration}s.
	 */
	public void setBoundConcentrations(){
		for (Map.Entry<String, PhaseMolecule> phaseMolEntry : getPhaseMolecules().entrySet()) {
			phaseMolEntry.getValue().setBoundConcentrations();
		}
	}
	/**
	 * This returns a sorted arrayList of the SSIPConcentrations.
	 * 
	 * @return sortedSSIPConcentrationList
	 */
	public List<SsipConcentration> getsortedSSIPConcentrations() {
		return sortSSIPConcentrations(getPhaseMoleculesSet());
	}

	/**
	 * This gets the moleculeID of each SSIPConcentrationList and uses it as the
	 * key in a map to the SSIPConcentrationLists.
	 * 
	 * @return phase molecule map
	 */
	@Override
	public Map<String, PhaseMolecule> getPhaseMolecules() {
		return this.phaseMoleculesMap;
	}

	/**
	 * This returns the total concentration of molecules in the phase, this is the sum of concentrations of all the molecules in the phase.
	 * 
	 * @return phase concentration
	 */
	public Concentration getPhaseConcentration() {
		double totalConcentration = 0.0;
		for (PhaseMolecule phaseMolecule : getPhaseMoleculesSet()) {
			totalConcentration += phaseMolecule.getTotalConcentrations().getEntry(0);
		}
		return new Concentration(totalConcentration, getConcentrationUnit());
	}
	
	/**
	 * This gets the total concentrations from each SSIPConcentration in the
	 * list, creating a DoubleMatrix such that the ith value corresponds to the
	 * ith SSIPConcentration.
	 * 
	 * @return total concentrations
	 */
	public ArrayRealVector getTotalConcentrations() {
		ArrayList<SsipConcentration> sortedSsipConcentrationList = (ArrayList<SsipConcentration>) getsortedSSIPConcentrations();
		ArrayRealVector totalConcentrations = new ArrayRealVector(
				sortedSsipConcentrationList.size());
		for (int i = 0; i < sortedSsipConcentrationList.size(); i++) {
			totalConcentrations.setEntry(i, sortedSsipConcentrationList.get(i)
					.getTotalConcentration());
		}
		return totalConcentrations;
	}

	/**
	 * This gets the free concentrations from each SSIPConcentration in the
	 * sorted list, creating a DoubleMatrix such that the ith value corresponds
	 * to the ith SSIPConcentration.
	 * 
	 * @return free concentrations
	 */
	public ArrayRealVector getFreeConcentrations() {
		ArrayList<SsipConcentration> sortedSsipConcentrationList = (ArrayList<SsipConcentration>) getsortedSSIPConcentrations();
		ArrayRealVector freeConcentrations = new ArrayRealVector(
				sortedSsipConcentrationList.size());
		for (int i = 0; i < sortedSsipConcentrationList.size(); i++) {
			freeConcentrations.setEntry(i, sortedSsipConcentrationList.get(i)
					.getFreeConcentration());
		}
		return freeConcentrations;
	}

	/**
	 * This gets the total bound concentrations from each SSIPConcentration in
	 * the sorted list, creating a DoubleMatrix such that the ith value
	 * corresponds to the ith SSIPConcentration.
	 * 
	 * @return bound concentrations
	 */
	public ArrayRealVector getBoundConcentrations() {
		ArrayList<SsipConcentration> sortedSsipConcentrationList = (ArrayList<SsipConcentration>) getsortedSSIPConcentrations();
		ArrayRealVector totalBoundConcentrations = new ArrayRealVector(
				sortedSsipConcentrationList.size());
		for (int i = 0; i < sortedSsipConcentrationList.size(); i++) {
			totalBoundConcentrations.setEntry(i, sortedSsipConcentrationList.get(i)
					.getBoundConcentration());
		}
		return totalBoundConcentrations;
	}

	/**
	 * This gets the bound concentrations from each SSIPConcentration in the
	 * sorted list, creating a DoubleMatrix such that the (i,j)th value
	 * corresponds to the fraction of the ith SSIPConcentration bound to the jth
	 * SSIPConcentration.
	 * 
	 * @return bound concentrations
	 */
	public Array2DRowRealMatrix getBoundConcentrationsMatrix() {
		ArrayList<SsipConcentration> sortedSsipConcentrationList = (ArrayList<SsipConcentration>) getsortedSSIPConcentrations();
		Array2DRowRealMatrix boundConcentrationsMatrix = new Array2DRowRealMatrix(
				sortedSsipConcentrationList.size(),
				sortedSsipConcentrationList.size());
		for (int i = 0; i < sortedSsipConcentrationList.size(); i++) {
			boundConcentrationsMatrix.setRowVector(i, sortedSsipConcentrationList
					.get(i).getBoundConcentrationList());
		}
		return boundConcentrationsMatrix;
	}

	/**
	 * This gets the indices of the SSIPs with the given moleculeID.
	 * 
	 * @param moleculeID molecule ID
	 * @return SSIP indices
	 */
	private List<Integer> getSSIPindicesBymoleculeID(String moleculeID){
		ArrayList<Integer> ssipIndices = new ArrayList<>();
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getsortedSsips();
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			if (ssipConcentrations.get(i).getMoleculeID().equals(moleculeID)) {
				ssipIndices.add(i);
			}
		}
		return ssipIndices;
	}

	/**
	 * This is the mean fraction of moleculeID1 bound to moleculeID2.
	 * 
	 * @param moleculeID1 ID 1
	 * @param moleculeID2 ID 2
	 * @return mean fraction of Mol 1 bound to Mol 2.
	 */
	public double calculateMeanBoundFractionOfMolecules(String moleculeID1, String moleculeID2) {
		return getPhaseMolecules().get(moleculeID1).calculateMeanFractionBoundToGivenSSIPs(getSSIPindicesBymoleculeID(moleculeID2));
	}
	
	/**
	 * This calculates the conversion factor required for this phase to convert to the 1M scale, from the molar fraction scale.
	 * 
	 * @return conversion factor
	 */
	public double calculateConversionTo1MScale(){
		double phaseConcentration = getPhaseConcentration().convertTo(ConcentrationUnit.MOLAR).getConcentrationValue();
		return -Constants.GAS_CONSTANT * getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue() * Math.log(phaseConcentration);
	}
	
	/**
	 * This returns a Double matrix of the solvation constants for each SSIP in
	 * the phase, with the order based on the order of the SSIPs.
	 * 
	 * From eqn 18 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * @return solvation constants
	 */
	public ArrayRealVector calculateSolvationConstants() {
		ArrayList<SsipConcentration> sortedSSIPs = (ArrayList<SsipConcentration>) getsortedSSIPConcentrations();
		ArrayRealVector solvationConstants = new ArrayRealVector(sortedSSIPs.size());
		for (int i = 0; i < sortedSSIPs.size(); i++) {
			solvationConstants.setEntry(i, sortedSSIPs.get(i)
					.calculateSolvationConstant());
		}
		return solvationConstants;
	}

	/**
	 * This returns a Double matrix of the fraction Free for each SSIP in the
	 * phase, with the order based on the order of the SSIPs.
	 * 
	 * From eqn 16 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * @return fraction free
	 */
	public ArrayRealVector calculateFractionFree() {
		ArrayList<SsipConcentration> sortedSSIPs = (ArrayList<SsipConcentration>) getsortedSSIPConcentrations();
		ArrayRealVector fractionFree = new ArrayRealVector(sortedSSIPs.size());
		for (int i = 0; i < sortedSSIPs.size(); i++) {
			fractionFree.setEntry(i, sortedSSIPs.get(i).calculateFractionFree());
		}
		return fractionFree;
	}
	
	/**
	 * This returns the matrix of the contribution to the binding energy from each SSIP.
	 * 
	 * @param moleculeID ID
	 * @return binding energy contributions
	 */
	public ArrayRealVector calculateBindingEnergyPerSSIP(String moleculeID){
		ArrayRealVector logFractionFree = getPhaseMolecules().get(moleculeID).calculateLogFractionFree();
		double prefactor = Constants.GAS_CONSTANT * getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue();
		return (ArrayRealVector) logFractionFree.mapMultiply(prefactor);
	}
	
	/**
	 * This returns the contribution to the free energy of binding for the molecule with the given ID in this phase.
	 * 
	 * Eqn 17 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a> described the free energy change of binding between two phases.
	 * This is the contribution from the current phase. 
	 * 
	 * Note that there was a sign error in the paper, so the expression here is the correct one.
	 * 
	 * @param moleculeID ID
	 * @return binding energy
	 */
	public double calculateBindingEnergy(String moleculeID){
		double logFractionFreeSumForMolecule = getPhaseMolecules().get(moleculeID).calculateLogFractionFreeSum();
		return Constants.GAS_CONSTANT * getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue() * logFractionFreeSumForMolecule;
	}
	
	/**
	 * This calculates the fractional occupancy of the phase. This should be
	 * between 0 and 1 if in the maximum SSIP Concentration normalised scale.
	 * 
	 * @return fractional occupancy
	 */
	public double calculateFractionalOccupancy() {
		ArrayRealVector totalConcentrations = getTotalConcentrations();
		return VectorUtils.sumVectorElements(totalConcentrations) * getConcentrationUnit().getConversionFactorTo(ConcentrationUnit.SSIPCONCENTRATION);
	}

	/**
	 * This calculates the confinement energy per SSIP for the phase.
	 * 
	 * This is described by eqn 22 in {@link http://dx.doi.org/10.1039/C3SC22124E}. 
	 * 
	 * @return confinement energy contributions
	 */
	double calculateConfinementEnergyPerSSIP(){
		double fractionalOccupancy = calculateFractionalOccupancy();
		double expressionToLog = (Math.sqrt(1.0 + (8.0 * fractionalOccupancy))- 1.0)/(4.0 * fractionalOccupancy);
		return -Constants.GAS_CONSTANT * getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue() *Math.log(expressionToLog);
	}
	
	/**
	 * This returns the number of SSIPs in a particular SSIPConcentrationLsit with the same moleculeID. 
	 * 
	 * @param moleculeID ID
	 * @return number of SSIPs
	 */
	int numberOfSSIPsInMolecule(String moleculeID){
		int numberOfSSIPs;
		Map<String, PhaseMolecule> ssipConcentrationMap = getPhaseMolecules();
		numberOfSSIPs = ssipConcentrationMap.get(moleculeID).getSSIPConcentrations().size();
		return numberOfSSIPs;
	}
	
	/**
	 * This returns a double matrix with the Confinement energy contribution from each SSIP in the molecule. 
	 * 
	 * @param moleculeID ID
	 * @return confinement energy contributions
	 */
	public ArrayRealVector calculateConfinementEnergyPerSSIPMatrix(String moleculeID){
		ArrayRealVector confinementEnergyMatrix = new ArrayRealVector(numberOfSSIPsInMolecule(moleculeID));
		return (ArrayRealVector) confinementEnergyMatrix.mapAdd(calculateConfinementEnergyPerSSIP());
	}
	
	/**
	 * This returns the confinement energy for the given molecule in the phase.
	 * 
	 * This is described by eqn 22 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * @param moleculeID ID
	 * @return confinement energy
	 */
	public double calculateConfinementEnergy(String moleculeID){
		double confinementEnergyPerSSIP = calculateConfinementEnergyPerSSIP();
		int numberOfSSIPs = numberOfSSIPsInMolecule(moleculeID);
		return (double) numberOfSSIPs * confinementEnergyPerSSIP;
	}
	
	/**
	 * This returns the free energy contribution from each SSIP in the given molecule.
	 * 
	 * @param moleculeID ID
	 * @return free energy contribution
	 */
	public ArrayRealVector calculateEnergyInPhaseBySSIP(String moleculeID){
		ArrayRealVector confinementEnergyMatrix = calculateConfinementEnergyPerSSIPMatrix(moleculeID);
		ArrayRealVector bindingEnergyMatrix = calculateBindingEnergyPerSSIP(moleculeID);
		return bindingEnergyMatrix.add(confinementEnergyMatrix);
	}
	
	/**
	 * This is the sum of the free energy of binding and free energy of confinement for the given molecule in the phase.
	 * 
	 * 
	 * @param moleculeID ID
	 * @return free energy 
	 */
	public double calculateEnergyInPhase(String moleculeID){
		double confinementEnergy = calculateConfinementEnergy(moleculeID);
		double bindingEnergy = calculateBindingEnergy(moleculeID);
		LOG.trace("binding: {} confinement: {}", bindingEnergy, confinementEnergy);
		return bindingEnergy + confinementEnergy;
	}

	/**
	 * This returns Association energies between all combinations of molecular species in the phase.
	 * 
	 * @return list of association energy values.
	 */
	public List<AssociationEnergyValue> calculateMoleculeAssociationEnergyValues(){
		ArrayList<AssociationEnergyValue> associationEnergyValues = new ArrayList<>();
		for (String moleculeID1 : getPhaseMolecules().keySet()) {
			for (String moleculeID2 : getPhaseMolecules().keySet()) {
				associationEnergyValues.add(calculateMoleculeAssociationEnergyValue(moleculeID1, moleculeID2));
			}
		}
		return associationEnergyValues;
	}
	
	/**
	 * This calculates Association energy value and returns a {@link AssociationEnergyValue} object. 
	 * 
	 * @param moleculeID1 ID 1
	 * @param moleculeID2 ID 2
	 * @return association energy value
	 */
	public AssociationEnergyValue calculateMoleculeAssociationEnergyValue(String moleculeID1, String moleculeID2) {
		double value = calculateMoleculeAssociationEnergy(moleculeID1, moleculeID2);
		return new AssociationEnergyValue(moleculeID1, moleculeID2, this.getSolventID(), value);
	}
	
	/**
	 * Calculates the Association energy between the molecules given.
	 * 
	 * @param moleculeID1 ID 1
	 * @param moleculeID2 ID 2
	 * @return Association energy
	 */
	public double calculateMoleculeAssociationEnergy(String moleculeID1, String moleculeID2) {
		ArrayRealVector molAssociationConstants = calculateMoleculeAssociationConstants(moleculeID1, moleculeID2);
		double rT = Constants.GAS_CONSTANT * getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue();
		double associationEnergy = 0.0;
		associationEnergy +=-rT * VectorUtils.sumVectorElements(molAssociationConstants.map(new Log()));
		return associationEnergy;
	}
	
	/**
	 * Calculates Association constants between the molecules given.
	 * 
	 * @param moleculeID1 ID 1
	 * @param moleculeID2 ID 2
	 * @return molecule association constants
	 */
	public ArrayRealVector calculateMoleculeAssociationConstants(String moleculeID1, String moleculeID2) {
		ArrayRealVector boundConcentrations = getPhaseMolecules().get(moleculeID1).getBoundConcentrationsToOtherSSIPs(getSSIPindicesBymoleculeID(moleculeID2));
		ArrayRealVector mol1FreeConcentrations = getPhaseMolecules().get(moleculeID1).getTotalConcentrations().add(boundConcentrations.mapMultiply(-1.0));
		ArrayRealVector mol2FreeConcentrations = getPhaseMolecules().get(moleculeID2).getTotalConcentrations().add(getPhaseMolecules().get(moleculeID2).getBoundConcentrationsToOtherSSIPs(getSSIPindicesBymoleculeID(moleculeID1)).mapMultiply(-1.0));
		double mol1Concs = mol1FreeConcentrations.getEntry(0);
		double mol2Concs = mol2FreeConcentrations.getEntry(0);
		return (ArrayRealVector) boundConcentrations.mapDivide((mol1Concs*mol2Concs));
	}
	
	/**
	 * Return {@link PhaseConcentrationFraction} information for the phase.
	 * 
	 * @return Phase Concentration Fraction.
	 */
	public PhaseConcentrationFraction calculatePhaseConcentrationFraction() {
		ArrayList<MoleculeConcentrationFraction> moleculeConcentrationFractions = (ArrayList<MoleculeConcentrationFraction>) calculateConcentrationFractionsAllMolecules();
		return new PhaseConcentrationFraction(moleculeConcentrationFractions, getTemperature(), getSolventID(), getPhaseType());
	}
	
	/**
	 * Calculate the {@link MoleculeConcentrationFraction} information for all {@link PhaseMolecule}s in the phase.
	 * 
	 * @return List of all molecule concentration fractions.
	 */
	public List<MoleculeConcentrationFraction> calculateConcentrationFractionsAllMolecules(){
		ArrayList<MoleculeConcentrationFraction> moleculeConcentrationFractions = new ArrayList<>();
		for (String moleculeID : getPhaseMolecules().keySet()) {
			moleculeConcentrationFractions.add(calculateConcentrationFractions(moleculeID));
		}
		return moleculeConcentrationFractions;
	}
	
	/**
	 * Calculate the {@link MoleculeConcentrationFraction} information for the {@link PhaseMolecule} with given moleculeID.
	 * 
	 * @param moleculeID ID 1
	 * @return molecule concentration fraction
	 */
	public MoleculeConcentrationFraction calculateConcentrationFractions(String moleculeID) {
		double freeFractionValue = getPhaseMolecules().get(moleculeID).calculateMeanFractionFree();
		FreeConcentrationFraction freeConcentrationFraction = new FreeConcentrationFraction(freeFractionValue, moleculeID);
		ArrayList<BoundConcentrationFraction> boundConcentrationFractions = new ArrayList<>();
		for (String moleculeIDValue : getPhaseMolecules().keySet()) {
			double boundFractionValue = calculateMeanBoundFractionOfMolecules(moleculeID, moleculeIDValue);
			boundConcentrationFractions.add(new BoundConcentrationFraction(boundFractionValue, moleculeID, moleculeIDValue));
		}
		return new MoleculeConcentrationFraction(moleculeID, freeConcentrationFraction, boundConcentrationFractions);
	}
	
	public Set<PhaseMolecule> getPhaseMoleculesSet() {
		return new HashSet<>(getPhaseMolecules().values());
	}

	public AssociationConstants getAssociationConstants() {
		return associationConstants;
	}

	public Temperature getTemperature() {
		return temperature;
	}

	@Override
	public ConcentrationUnit getConcentrationUnit() {
		return concentrationUnit;
	}

	public String getSolventID() {
		return solventID;
	}

	public void setSolventID(String solventID) {
		this.solventID = solventID;
	}

	public PhaseType getPhaseType() {
		return phaseType;
	}

	public void setPhaseType(PhaseType phaseType) {
		this.phaseType = phaseType;
	}

	private List<String> getMoleculeIDForSSIPs(){
		ArrayList<String> molIDs = new ArrayList<>(getPhaseMolecules().keySet());
		Collections.sort(molIDs);
		return molIDs;
	}
	
	@Override
	public ArrayRealVector getVdWVolumesByMolecule() {
		ArrayRealVector vdWVolumesByMolecule = new ArrayRealVector(getMoleculeIDForSSIPs().size());
		for (int i = 0; i < getMoleculeIDForSSIPs().size(); i++) {
			String molID = getMoleculeIDForSSIPs().get(i);
			vdWVolumesByMolecule.setEntry(i, getPhaseMolecules().get(molID).getSsipSurfaceInformation().getVdWVolume());
		}
		return vdWVolumesByMolecule;
	}

	@Override
	public ArrayRealVector getVdWVolumes() {
		ArrayRealVector volumes = new ArrayRealVector();
		for (String molId : getMoleculeIDForSSIPs()) {
			if (volumes.getDimension() == 0) {
				volumes = (ArrayRealVector) getPhaseMolecules().get(molId).getVdWVolumes();
			} else {
				volumes = (ArrayRealVector) volumes.append(getPhaseMolecules().get(molId).getVdWVolumes());
			}
		}
		return volumes;
	}

	@Override
	public Map<String, Double> getMoleFractionsByMoleculeID() {
		HashMap<String, Double> moleFractionsByMoleculeID = new HashMap<>();
		for (String moleculeID : getMoleculeIDForSSIPs()) {
			moleFractionsByMoleculeID.put(moleculeID, getPhaseMolecules().get(moleculeID).getMoleFraction());
		}
		return moleFractionsByMoleculeID;
	}
	
	@Override
	public ArrayRealVector getMoleFractionsByMolecule() {
		ArrayRealVector moleFractionsByMolecule = new ArrayRealVector(getMoleculeIDForSSIPs().size());
		for (int i = 0; i < getMoleculeIDForSSIPs().size(); i++) {
			String molID = getMoleculeIDForSSIPs().get(i);
			moleFractionsByMolecule.setEntry(i, getPhaseMolecules().get(molID).getMoleFraction());
		}
		return moleFractionsByMolecule;
	}

	@Override
	public ArrayRealVector getMoleFractions() {
		ArrayRealVector moleFractions = new ArrayRealVector();
		for (String molID : getMoleculeIDForSSIPs()) {
			if (moleFractions.getDimension() == 0) {
				moleFractions = (ArrayRealVector) getPhaseMolecules().get(molID).getMoleFractions();
			} else {
				moleFractions = (ArrayRealVector) moleFractions.append(getPhaseMolecules().get(molID).getMoleFractions());
			}
		}
		return moleFractions;
	}

	@Override
	public List<SsipConcentration> getsortedSsips() {
		return sortSSIPConcentrations(getPhaseMoleculesSet());
	}

	@Override
	public FractionalOccupancy getFractionalOccupancy(String soluteID) {
		return new FractionalOccupancy(calculateFractionalOccupancy(), getSolventID(), soluteID, getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue());
	}

}
