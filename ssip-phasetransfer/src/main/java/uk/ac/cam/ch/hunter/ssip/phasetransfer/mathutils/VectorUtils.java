/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.util.FastMath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Utils class containing static methods for vector operations required but not included in apache commons math. 
 * 
 * @author mdd31
 *
 */
public class VectorUtils {

	private static final Logger LOG = LogManager.getLogger(VectorUtils.class);
	
	private VectorUtils() {
	}
	
	/**
	 * Calculate the sum of elements in the vector.
	 * 
	 * @param arrayRealVector Input vector.
	 * @return Sum of elements in the vector.
	 */
	public static double sumVectorElements(ArrayRealVector arrayRealVector) {
		double sum =0.0;
		for (int i = 0; i < arrayRealVector.getDimension(); i++) {
			sum += arrayRealVector.getEntry(i);
		}
		return sum;
	}
	
	/**
	 * Compare the vectors element wise for equality, with given absolute tolerance.
	 * 
	 * @param vector1 Input vector1.
	 * @param vector2 Input vector2.
	 * @param tolerance Absolute tolerance for comparison.
	 * @return True if vectors are equal.
	 */
	public static boolean compareVectorsEqualAbsoluteTolerance(ArrayRealVector vector1, ArrayRealVector vector2, double tolerance) {
		LOG.debug("ulp for tolerance: {}", Math.ulp(tolerance));
		for (int i = 0; i < vector1.getDimension(); i++) {
			LOG.trace("difference: {} tolerance: {} ulp for diference: {}", FastMath.abs(vector1.getEntry(i) - vector2.getEntry(i)), tolerance, Math.ulp(Math.abs(vector1.getEntry(i) - vector2.getEntry(i))));
			LOG.trace("ulp value 1: {} ulp vector 2: {}", Math.ulp(vector1.getEntry(i)), Math.ulp(vector2.getEntry(i)));
			boolean almostEqual = (Math.abs(vector1.getEntry(i) - vector2.getEntry(i)) <= (tolerance + (Math.max(Math.ulp(vector1.getEntry(i)), Math.ulp(vector2.getEntry(i))/2.0))));
			LOG.trace("almost equal: {}", almostEqual);
			if (!almostEqual) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Compare the vectors element wise for equality, with given relative tolerance.
	 * 
	 * @param vector1 Input vector1.
	 * @param vector2 Input vector2.
	 * @param tolerance Relative tolerance for comparison.
	 * @return True if vectors are equal.
	 */
	public static boolean compareVectorsEqualRelativeTolerance(ArrayRealVector vector1, ArrayRealVector vector2, double tolerance) {
		LOG.debug("ulp for tolerance: {}", Math.ulp(tolerance));
		for (int i = 0; i < vector1.getDimension(); i++) {
			LOG.trace("absolute difference: {} relative difference: {} tolerance: {} ulp for diference: {}", FastMath.abs(vector1.getEntry(i) - vector2.getEntry(i)), (FastMath.abs(vector1.getEntry(i) - vector2.getEntry(i))/vector2.getEntry(i)), tolerance, Math.ulp(Math.abs(vector1.getEntry(i) - vector2.getEntry(i))));
			LOG.trace("ulp value 1: {} ulp vector 2: {}", Math.ulp(vector1.getEntry(i)), Math.ulp(vector2.getEntry(i)));
			boolean almostEqual = ((Math.abs(vector1.getEntry(i) - vector2.getEntry(i))/ vector2.getEntry(i)) <= (tolerance + (Math.max(Math.ulp(vector1.getEntry(i)), Math.ulp(vector2.getEntry(i))/2.0))));
			LOG.trace("almost equal: {}", almostEqual);
			if (!almostEqual) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Compare the vectors element wise for equality, with given absolute and relative tolerances.
	 * 
	 * @param vector1 Input vector1.
	 * @param vector2 Input vector2.
	 * @param absTolerance Absolute tolerance for comparison.
	 * @param relTolerance Relative tolerance for comparison.
	 * @return True if vectors are equal.
	 */
	public static boolean compareVectorsEqualCompositeTolerance(ArrayRealVector vector1, ArrayRealVector vector2, double absTolerance, double relTolerance) {
		LOG.debug("ulp for tolerance: {}", Math.ulp(absTolerance));
		for (int i = 0; i < vector1.getDimension(); i++) {
			double relativeTolerance = Math.abs(vector1.getEntry(i)) * relTolerance;
			LOG.trace("absolute difference: {} relative difference: {} absolute tolerance: {} relative_tolerance: {} ulp for diference: {}", FastMath.abs(vector1.getEntry(i) - vector2.getEntry(i)), (FastMath.abs(vector1.getEntry(i) - vector2.getEntry(i))/vector2.getEntry(i)), absTolerance, relativeTolerance, Math.ulp(Math.abs(vector1.getEntry(i) - vector2.getEntry(i))));
			LOG.trace("ulp value 1: {} ulp vector 2: {}", Math.ulp(vector1.getEntry(i)), Math.ulp(vector2.getEntry(i)));
			boolean almostEqual = ((Math.abs(vector1.getEntry(i) - vector2.getEntry(i))) <= (relativeTolerance + absTolerance + (Math.max(Math.ulp(vector1.getEntry(i)), Math.ulp(vector2.getEntry(i))/2.0))));
			if (!almostEqual) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * This performs the multiplication of vector1 with vector2 transpose.
	 *  
	 * @param vector1 Input vector1.
	 * @param vector2 Input vector2.
	 * @return Multiplication of vector1 with vector2 transpose
	 */
	public static Array2DRowRealMatrix calculateVectorVectorTMatrix(ArrayRealVector vector1, ArrayRealVector vector2) {
		Array2DRowRealMatrix productMatrix = new Array2DRowRealMatrix(vector1.getDimension(), vector2.getDimension());
		for (int i = 0; i < vector1.getDimension(); i++) {
			productMatrix.setRowVector(i, vector2.mapMultiply(vector1.getEntry(i)));
		}
		return productMatrix;
	}
	
}
