/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IPhase;

/**
 * This calculates and matches the total concentrations of SSIPs in solvents. 
 * 
 * @author md331
 *
 */
public class TotalConcentrationMatcher {
	
	private static final Logger LOG = LogManager.getLogger(TotalConcentrationMatcher.class);
	
	private TotalConcentrationMatcher(){
	}

	/**
	 * Calculates and sets total concentrations for the given {@link IPhase}. 
	 * 
	 * @param iPhase phase
	 * @param temperature temperature
	 * @return iPhase phase with concentrations
	 */
	public static IPhase setIPhaseTotalConcentrations(IPhase iPhase, double temperature){
		IPhase convertedIPhase = iPhase.convertConcentrationUnitTo(ConcentrationUnit.SSIPCONCENTRATION);
		LOG.trace("converted molefractions: {}", convertedIPhase);
		TotalConcentrationCalculator totalConcentrationCalculator = new TotalConcentrationCalculator(convertedIPhase, temperature);
		totalConcentrationCalculator.calculateTotalConcentrations();
		LOG.debug("Total concentrations match: {}", totalConcentrationCalculator.totalConcentrationMatches());
		LOG.debug("Total Concentrations: {}", totalConcentrationCalculator.getConcentrationCalculator().getTotalConcentrations());
		for (Map.Entry<String, ArrayList<SsipConcentration>> molSsipConcEntry : totalConcentrationCalculator.calculateTotalConcsByMolecule().entrySet()) {
			convertedIPhase.getPhaseMolecules().get(molSsipConcEntry.getKey()).getSsipConcentrationContainer().setSsipConcentrations(molSsipConcEntry.getValue());
		}
		return convertedIPhase.convertConcentrationUnitTo(iPhase.getConcentrationUnit());
	}
	
	/**
	 * Calculates total concentrations for SSIPs in each solvent, and then sets them. 
	 * 
	 * @param solventContainer solvent container
	 * @param temperature temperature
	 */
	public static void setSolventTotalConcentrations(SolventContainer solventContainer, double temperature){
		HashMap<String, PhaseSolvent> solventEntryMap = new HashMap<>();
		for (Map.Entry<String, PhaseSolvent> solventEntry : solventContainer.getSolventMap().entrySet()) {
			PhaseSolvent concSetPhaseSolvent = (PhaseSolvent) setIPhaseTotalConcentrations(solventEntry.getValue(), temperature);
			solventEntryMap.put(solventEntry.getKey(), concSetPhaseSolvent);
		}
		solventContainer.setSolventMap(solventEntryMap);
	}
	
	/**
	 * Calculates total concentrations for SSIPs in each phase, then sets them.
	 * 
	 * @param mixture mixture
	 */
	public static void setPhaseTotalConcentrations(Mixture mixture){
		HashMap<String, Phase> phaseEntryMap = new HashMap<>();
		for (Map.Entry<String, Phase> phaseEntry : mixture.getPhaseMap().entrySet()) {
			LOG.info("input phase solvent ID: {}", phaseEntry.getValue().getSolventID());
			Phase concSetPhase = (Phase) setIPhaseTotalConcentrations(phaseEntry.getValue(), phaseEntry.getValue().getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue());
			LOG.info("conc set phase solvent ID: {}", concSetPhase.getSolventID());
			phaseEntryMap.put(phaseEntry.getKey(), concSetPhase);
		}
		mixture.setPhaseMap(phaseEntryMap);
	}
	
}
