/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters.SolventMapAdapter;

/**
 * Container class for {@link PhaseSolvent} objects.
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "SolventList", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
public class SolventContainer {

	@XmlElement(name = "Solvents", namespace ="http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlJavaTypeAdapter(SolventMapAdapter.class)
	private HashMap<String, PhaseSolvent> solventMap;
	
	/**
	 * Constructor for JAXB
	 */
	public SolventContainer(){
		setSolventMap(null);
	}
	
	/**
	 * Constructor initialising with a map containing {@link PhaseSolvent} information. Key is solventID.
	 * 
	 * @param solventMap solvent map
	 */
	public SolventContainer(Map<String, PhaseSolvent> solventMap){
		this.setSolventMap(solventMap);
	}

	public Map<String, PhaseSolvent> getSolventMap() {
		return solventMap;
	}

	public void setSolventMap(Map<String, PhaseSolvent> solventMap) {
		this.solventMap = (HashMap<String, PhaseSolvent>) solventMap;
	}
}
