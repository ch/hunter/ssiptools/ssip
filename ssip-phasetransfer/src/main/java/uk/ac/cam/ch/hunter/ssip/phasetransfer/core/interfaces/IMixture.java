/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces;

import java.util.List;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.BoundConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.FractionalOccupancyCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseConcentrationFraction;

/**
 * Interface for mixture like objects. These represent a collection of phase objects, and define methods that they should all have.
 * 
 * @author mdd31
 *
 */
public interface IMixture {

	/**
	 * This is required to map the {@link BoundConcentration} objects to populate the {@link ArrayRealVector} representation of the information after unmarshalling from XML.
	 */
	public void setBoundConcentrations();

	/**
	 * Set Association Constants after unmarshalling from XML.
	 */
	public void setAssociationConstants();

	/**
	 * This returns the stored map containing the phases. 
	 * @return phase map
	 */
	public Map<String, Phase> getPhaseMap();
	
	/**
	 * Sets phase Map.
	 * 
	 * @param phaseMap phase map.
	 */
	public void setPhaseMap(Map<String, Phase> phaseMap);
	
	/**
	 * gets ID for mixture.
	 * 
	 * @return string
	 */
	public String getMixtureID();

	/**
	 * set ID for mixture.
	 * 
	 * @param key key.
	 */
	public void setMixtureID(String key);
	
	/**
	 * 
	 * This calculates the {@link PhaseConcentrationFraction}s for each phase in the mixture.
	 * 
	 * @return Phase concentration fractions
	 */
	public List<PhaseConcentrationFraction> calculatePhaseConcentrationFractions();
	
	public FractionalOccupancyCollection getFractionalOccupancyCollection();
}
