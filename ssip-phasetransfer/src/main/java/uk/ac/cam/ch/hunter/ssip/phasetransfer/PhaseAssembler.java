/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolute;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;

/**
 * Class creates Phases from the solvent and solute information given. 
 * 
 * @author mdd31
 *
 */
public class PhaseAssembler {

	private static final Logger LOG = LogManager.getLogger(PhaseAssembler.class);
	
	private static final PhaseFactory phaseFactory = new PhaseFactory();
	
	/**
	 *  Constructor
	 */
	public PhaseAssembler(){
		/**
		 * Initialise assembler
		 */
	}
	
	/**
	 * This generates a phase from the given solvent and solute information.
	 * 
	 * @param solute Solute.
	 * @param solventInformation Solvent information.
	 * @param temperature Temperature.
	 * @param concentrationUnit Concentration unit.
	 * @return phase
	 */
	public Phase generatePhaseFromSoluteAndSolvent(PhaseSolute solute, PhaseSolvent solventInformation, Temperature temperature, ConcentrationUnit concentrationUnit){
		HashSet<PhaseMolecule> solventAndSolute = new HashSet<>();
		solventAndSolute.addAll(solventInformation.getPhaseMolecules().values());
		solventAndSolute.add(solute.getPhaseMolecule());
		Phase phase = phaseFactory.generatePhase(solventAndSolute, temperature, concentrationUnit);
		phase.setSolventID(solventInformation.getSolventId());
		return phase;
	}
	
	/**
	 * This return a Map of the phase with the key being the PhaseID which equals SolventID for the solvent and solute information given. 
	 * 
	 * @param solute Solute.
	 * @param solventID Solvent ID.
	 * @param solventInformation solvent information.
	 * @param temperature temperature.
	 * @param concentrationUnit concentration unit.
	 * @return phase map
	 */
	public Map<String, Phase> generatePhaseFromSoluteAndSolventWithID(PhaseSolute solute, String solventID, PhaseSolvent solventInformation, Temperature temperature, ConcentrationUnit concentrationUnit) {
		HashMap<String, Phase> phaseMapwithphaseID = new HashMap<>();
		Phase phaseSoluteAndSolvent = generatePhaseFromSoluteAndSolvent(solute, solventInformation, temperature, concentrationUnit);
		phaseMapwithphaseID.put(solventID, phaseSoluteAndSolvent);
		return phaseMapwithphaseID;
	}
	
	/**
	 * This generates phases for the same solute with a range of different solvent systems.
	 * 
	 * @param solute Solute.
	 * @param solventInformationByID Solvent information.
	 * @param temperature Temperature.
	 * @param concentrationUnit Concentration unit.
	 * @return phase map
	 */
	public Map<String, Phase> generatePhasesFromSoluteWithSolvents(PhaseSolute solute, Map<String, PhaseSolvent> solventInformationByID, Temperature temperature, ConcentrationUnit concentrationUnit){
		HashMap<String, Phase> phaseMapwithphaseID = new HashMap<>();
		for (Map.Entry<String, PhaseSolvent> solventInformation : solventInformationByID.entrySet()) {
			LOG.debug("Solute ID: {}", solute.getSoluteID());
			LOG.debug("Solvent ID: {}", solventInformation.getKey());
			HashMap<String, Phase> phaseWithID = (HashMap<String, Phase>) generatePhaseFromSoluteAndSolventWithID(solute, solventInformation.getKey(), solventInformation.getValue(), temperature, concentrationUnit);
			phaseMapwithphaseID.putAll(phaseWithID);
		}
		return phaseMapwithphaseID;
	}
	
}
