/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.linear.ArrayRealVector;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;


/**
 * This class takes the results from the concentration calculation and matches them to the corresponding SSIPs.
 *  
 * @author mdd31
 *
 */
public class ConcentrationMatcher {

	private ConcentrationCalculator concentrationCalculator;
	
	private ArrayList<SsipConcentration> ssipConcentrationList;
	
	/**
	 * Constructor for the Concentration Matcher. Takes the concentrationCalculator used to generate the free and bound concentrations, and assigns the values to the given SSIPConcentrations in the list, assuming the ordering is the same.
	 * 
	 * @param concentrationCalculator calculator
	 * @param ssipConcentrations SSIPs
	 */
	public ConcentrationMatcher(ConcentrationCalculator concentrationCalculator, List<SsipConcentration> ssipConcentrations){
		setConcentrationCalculator(concentrationCalculator);

		setSsipConcentrationList(ssipConcentrations);
	}

	/**
	 * sets the free concentrations for all the SSIPConcentrations in the Array list, based on the values in the ConcentrationCalculator.
	 */
	protected void setFreeConcentrations(){
		for (int i = 0; i < getConcentrationCalculator().getFreeConcentrations().getDimension(); i++) {
			double freeConcentration = getConcentrationCalculator().getFreeConcentrations().getEntry(i);
			getSsipConcentrationList().get(i).setFreeConcentration(freeConcentration);
		}
	}
	
	/**
	 * sets the bound total concentrations for all the SSIPConcentrations in the Array list, based on the values in the ConcentrationCalculator.
	 */
	protected void setBoundConcentrationTotals(){
		for (int i = 0; i < getConcentrationCalculator().getBoundConcentrationsTotal().getDimension(); i++) {
			double boundConcentrationTotal = getConcentrationCalculator().getBoundConcentrationsTotal().getEntry(i);
			getSsipConcentrationList().get(i).setBoundConcentration(boundConcentrationTotal);
		}
	}
	
	/**
	 * sets the bound concentration lists for all the SSIPConcentrations in the Array list, based on the values in the ConcentrationCalculator.
	 */
	protected void setBoundConcentrationLists(){
		for (int i = 0; i < getConcentrationCalculator().getBoundConcentrations().getRowDimension(); i++) {
			ArrayRealVector boundConcentrations = (ArrayRealVector) getConcentrationCalculator().getBoundConcentrations().getRowVector(i);
			getSsipConcentrationList().get(i).setBoundConcentrationList(boundConcentrations);
			
		} 
	}
	
	/**
	 * sets the free concentration, bound concentration totals and bound concentration lists for each SSIPConcetration.
	 */
	public void setConcentrations(){
		setFreeConcentrations();
		setBoundConcentrationTotals();
		setBoundConcentrationLists();
	}
	

	
	public ConcentrationCalculator getConcentrationCalculator() {
		return concentrationCalculator;
	}

	public void setConcentrationCalculator(ConcentrationCalculator concentrationCalculator) {
		this.concentrationCalculator = concentrationCalculator;
	}

	public List<SsipConcentration> getSsipConcentrationList() {
		return ssipConcentrationList;
	}

	public void setSsipConcentrationList(List<SsipConcentration> ssipConcentrationList) {
		this.ssipConcentrationList = (ArrayList<SsipConcentration>) ssipConcentrationList;
	}
	
}
