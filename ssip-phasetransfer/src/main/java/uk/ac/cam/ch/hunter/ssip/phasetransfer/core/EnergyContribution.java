/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;

/**
 * Class for storing the information about the energy contribution from an SSIP to the total.
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class EnergyContribution implements Comparable<EnergyContribution>{

	private static final double ERROR = 1e-7;
	
	@XmlValue
	private final double value;
	@XmlAttribute(name = "ssipID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final int ssipID;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	private int hash;
	@XmlTransient
	private boolean hashCodeFound = false;
	
	/**
	 * Constructor for JAXB.
	 */
	public EnergyContribution(){
		this.value = Double.NaN;
		this.ssipID = Integer.MAX_VALUE;
	}
	
	/**
	 * Constructor for {@link EnergyContribution}, settingthe value and the SSIPID for the SSIP the energy corresponds to.
	 * 
	 * @param value Energy contribution from an SSIP to the total
	 * @param ssipID ID of the SSIP.
	 */
	public EnergyContribution(double value, int ssipID){
		this.value = value;
		this.ssipID = ssipID;
	}

	public double getValue() {
		return value;
	}

	public int getSsipID() {
		return ssipID;
	}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnergyContribution other = (EnergyContribution) obj;
		return this.getSsipID() == other.getSsipID() && Math.abs(this.getValue() - other.getValue()) < ERROR;
	}
	
	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(value);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = ssipID;
			result = prime * result + (int) (temp ^ (temp >>> 32));
			hash = result;
			hashCodeFound = true;
		}
		return hash;
	}
	
	@Override
	public int compareTo(EnergyContribution other){
		return Integer.compare(this.getSsipID(), other.getSsipID());
	}
}
