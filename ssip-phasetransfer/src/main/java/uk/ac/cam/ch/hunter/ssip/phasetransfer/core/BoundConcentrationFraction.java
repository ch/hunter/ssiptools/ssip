/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for storing bound concentration fraction information.
 * 
 * @author mdd31
 *
 */
@XmlRootElement(name="BoundConcentrationFraction", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class BoundConcentrationFraction extends ConcentrationFraction {

	@XmlAttribute(name="moleculeID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String moleculeID;
	
	@XmlAttribute(name="boundToMoleculeID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String boundToMoleculeID;
	
	
	public BoundConcentrationFraction() {
		/**
		 * JAXB constructor
		 */
		super();
	}
	
	public BoundConcentrationFraction(double concentrationFraction, String moleculeID, String boundToMoleculeID) {
		super(concentrationFraction);
		this.setMoleculeID(moleculeID);
		this.setBoundToMoleculeID(boundToMoleculeID);
	}

	public String getMoleculeID() {
		return moleculeID;
	}

	public void setMoleculeID(String moleculeID) {
		this.moleculeID = moleculeID;
	}

	public String getBoundToMoleculeID() {
		return boundToMoleculeID;
	}

	public void setBoundToMoleculeID(String boundToMoleculeID) {
		this.boundToMoleculeID = boundToMoleculeID;
	}
	
	@Override
	public String toString() {
		return String.format("Bound fraction: MoleculeID: %s Bound To: %s Fraction Bound: %f", getMoleculeID(), getBoundToMoleculeID(), getFraction());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoundConcentrationFraction other = (BoundConcentrationFraction) obj;
		if (this.moleculeID.equals(other.getMoleculeID()) && this.boundToMoleculeID.equals(other.getBoundToMoleculeID())){
			return super.equals(other);
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = super.hashCode();
			long temp;
			temp = moleculeID.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = boundToMoleculeID.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));

			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
}
