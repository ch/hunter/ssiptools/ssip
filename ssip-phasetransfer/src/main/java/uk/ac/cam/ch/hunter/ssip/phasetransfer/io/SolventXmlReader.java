/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import java.io.File;
import java.net.URI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;

import uk.ac.cam.ch.hunter.ssip.core.io.SchemaMaker;
import uk.ac.cam.ch.hunter.ssip.core.io.XmlValidationEventHandler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;

/**
 * Class for the reading in of information about a solvent from XML.
 * 
 * @author mdd31
 *
 */
public class SolventXmlReader {

	private static final Schema phaseSchema = SchemaMaker.createPhaseSchema();
		
	/**
	 * Constructor for a SolventXMLReader
	 */
	private SolventXmlReader(){
		/**
		 * Initialise reader.
		 */
	}
	/**
	 * unMarshal {@link PhaseSolvent} from given {@link URI}. 
	 * 
	 * @param uri filename
	 * @return phase solvent
	 * @throws JAXBException exception
	 */
	public static PhaseSolvent unmarshal(URI uri) throws JAXBException{
		File file = new File(uri);
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSolvent.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		return (PhaseSolvent) jaxbUnmarshaller.unmarshal(file);
	}
	
	/**
	 * Unmarshal {@link SolventContainer} from given {@link URI}.
	 * 
	 * @param uri filename
	 * @return solvent container
	 * @throws JAXBException exception
	 */
	public static SolventContainer unmarshalContainer(URI uri) throws JAXBException{
		File file = new File(uri);
		JAXBContext jaxbContext = JAXBContext.newInstance(SolventContainer.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		return (SolventContainer) jaxbUnmarshaller.unmarshal(file);
	}
	
}
