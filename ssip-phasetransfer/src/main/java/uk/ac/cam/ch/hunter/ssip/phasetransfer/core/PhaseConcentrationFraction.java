/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Class contains concentration fraction information for the
 * {@link PhaseMolecule}s in the given phase.
 * 
 * @author mdd31
 *
 */
@XmlRootElement(name = "PhaseConcentrationFraction", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class PhaseConcentrationFraction {

	@XmlElement(name = "MoleculeConcentrationFraction", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ArrayList<MoleculeConcentrationFraction> moleculeConcentrationFractions;

	@XmlElement(name = "Temperature", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private Temperature temperature;

	@XmlAttribute(name = "solventID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String solventID;

	@XmlAttribute(name = "phaseType", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private PhaseType phaseType;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;
	
	public PhaseConcentrationFraction() {
		/**
		 * JAXB Constructor.
		 */
	}

	public PhaseConcentrationFraction(List<MoleculeConcentrationFraction> moleculeConcentrationFractions,
			Temperature temperature, String solventID, PhaseType phaseType) {
		this.moleculeConcentrationFractions = (ArrayList<MoleculeConcentrationFraction>) moleculeConcentrationFractions;
		this.temperature = temperature;
		this.solventID = solventID;
		this.phaseType = phaseType;
	}

	public List<MoleculeConcentrationFraction> getMoleculeConcentrationFractions() {
		return moleculeConcentrationFractions;
	}

	public void setMoleculeConcentrationFractions(List<MoleculeConcentrationFraction> moleculeConcentrationFractions) {
		this.moleculeConcentrationFractions = (ArrayList<MoleculeConcentrationFraction>) moleculeConcentrationFractions;
	}

	public Temperature getTemperature() {
		return temperature;
	}

	public void setTemperature(Temperature temperature) {
		this.temperature = temperature;
	}

	public String getSolventID() {
		return solventID;
	}

	public void setSolventID(String solventID) {
		this.solventID = solventID;
	}

	public PhaseType getPhaseType() {
		return phaseType;
	}

	public void setPhaseType(PhaseType phaseType) {
		this.phaseType = phaseType;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhaseConcentrationFraction other = (PhaseConcentrationFraction) obj;
		return this.getMoleculeConcentrationFractions().equals(other.getMoleculeConcentrationFractions())
				&& this.getPhaseType() == other.getPhaseType() && this.getSolventID().equals(other.getSolventID())
				&& this.getTemperature().equals(other.getTemperature());
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = super.hashCode();
			long temp;
			temp = moleculeConcentrationFractions.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = temperature.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = solventID.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = phaseType.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));

			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
	
	@Override
	public String toString() {
		return String.format("Phase: ID: %s Temperature: %s Type:%s%n %s", getSolventID(), getTemperature().toString(),
				getPhaseType().toString(), getMoleculeConcentrationFractions());
	}
}
