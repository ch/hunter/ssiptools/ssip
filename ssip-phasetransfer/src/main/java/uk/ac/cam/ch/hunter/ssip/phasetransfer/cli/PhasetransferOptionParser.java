/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.cli;

import java.util.Arrays;
import java.util.List;

import joptsimple.OptionParser;
import joptsimple.OptionSpecBuilder;

public class PhasetransferOptionParser extends OptionParser {

	//Options for input files
	private static final String CONCENTRATION_UNIT_STRING = "concentrationUnit";
	private static final String PHASE_FILE_STRING = "phaseFile";
	private static final String CONC_CALC_STRING = "conccalc";
	private static final String TEMPERATURE_STRING = "temperature";
	private static final String TEMPERATURE_UNIT_STRING = "temperatureUnit";
	
	
	@SafeVarargs
	private static <T> List<T> asList(T... items) {
		return Arrays.asList(items);
	}
	public PhasetransferOptionParser(){
	OptionSpecBuilder phaseFile = acceptsAll(asList(PHASE_FILE_STRING), "phase input file name");
	phaseFile.withRequiredArg().ofType(String.class);
	OptionSpecBuilder phaseCollection = acceptsAll(asList("phaseCollection"), "phase collection in input file");
	phaseCollection.requiredIf(phaseFile);
	OptionSpecBuilder phaseCollectionList = acceptsAll(asList("phaseCollectionList"), "phase collection list in input file.");
	phaseCollectionList.requiredIf(phaseFile);
	OptionSpecBuilder phaseCalculation = acceptsAll(asList("calc"), "option sets it to calculate new concentrations for the phases, otherwise they are read in.");
	OptionSpecBuilder totalConcCalc = acceptsAll(asList(CONC_CALC_STRING), "calculate the total concentration for solvents based on molecular volumes and volume fraction of solvent.");
	totalConcCalc.availableIf(phaseCalculation);
	OptionSpecBuilder gasCalc = acceptsAll(asList("gascalc"), "option to do gas calculation calculation for input phases in mixture.");
	gasCalc.availableIf(phaseCalculation);
	OptionSpecBuilder solventFile = acceptsAll(asList("solvent"), "solvent file name");
	solventFile.withRequiredArg().ofType(String.class);
	solventFile.availableUnless(phaseFile);
	OptionSpecBuilder soluteFile = acceptsAll(asList("solute"), "solute file name");
	soluteFile.withRequiredArg().ofType(String.class);
	
	//Options for input temperature and concentration units
	OptionSpecBuilder temperature = acceptsAll(asList("t", TEMPERATURE_STRING), "temperature value");
	temperature.withRequiredArg().ofType(Double.class).defaultsTo(298.0);
	OptionSpecBuilder temperatureUnit = acceptsAll(asList(TEMPERATURE_UNIT_STRING), TEMPERATURE_UNIT_STRING);
	temperatureUnit.withRequiredArg().defaultsTo("KELVIN");
	OptionSpecBuilder concentrationUnit = acceptsAll(asList(CONCENTRATION_UNIT_STRING), CONCENTRATION_UNIT_STRING);
	concentrationUnit.withRequiredArg().defaultsTo("MOLAR");
	
	//Options for output
	
	OptionSpecBuilder outputFilename = acceptsAll(asList("o", "outputfile"), "output file name, required");
	outputFilename.withRequiredArg().ofType(String.class);
	
	OptionSpecBuilder completeSolvationOutput = acceptsAll(asList("solvation"), "Output solvation energies of all molecules in all phases.");
	
	OptionSpecBuilder energyValuesOutput = acceptsAll(asList("e", "energies"), "output the free energies");
	energyValuesOutput.withOptionalArg().defaultsTo("all");
	energyValuesOutput.requiredIf(completeSolvationOutput);
	OptionSpecBuilder associationEnergyValueOutput = acceptsAll(asList("association"), "output the Association energies between species.");
	associationEnergyValueOutput.availableUnless(energyValuesOutput);
	
	
	OptionSpecBuilder phaseOutput = acceptsAll(asList("p", "phaseOut"), "output the phase states from the calculation to a PhaseCollectionList");
	phaseOutput.availableUnless(energyValuesOutput, associationEnergyValueOutput);
	OptionSpecBuilder concFractionOutput = acceptsAll(asList("concfrac"), "output the concentration fraction information of each of the components.");
	concFractionOutput.availableUnless(energyValuesOutput, associationEnergyValueOutput, phaseOutput);
	OptionSpecBuilder fracOccOutput = acceptsAll(asList("f", "fracocc"), "Output the fractional occupancy of all phases");
	fracOccOutput.withRequiredArg().ofType(String.class);
	// help option.
	OptionSpecBuilder help = acceptsAll(asList("h", "?", "help"),
			"display this output");
	help.forHelp();
}
}
