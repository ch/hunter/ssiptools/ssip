/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.cli;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.GitRepositoryState;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.AssociationEnergyValue;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.AssociationEnergyValueCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.EnergyValue;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.EnergyValueCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.FractionalOccupancyCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.MixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseConcentrationFraction;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseConcentrationFractionCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IEnergyDifferenceCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.ISolvationEnergyCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.io.EnergyXmlWriter;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.io.FractionalOccupancyXmlWriter;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.io.MixtureXmlReader;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.io.MixtureXmlWriter;


/**
 * Class for the command line interface of the phase transfer code.
 * 
 * @author mdd31
 *
 */
public class PhaseTransferCli {

	private static final Logger LOG = LogManager.getLogger(PhaseTransferCli.class);

	private static final String NAME = "phasetransfer";

	private static GitRepositoryState repositoryState = GitRepositoryState.getGitRepositoryState(PhaseTransferCli.class);
	
	private static final String TITLE = NAME
			+ " - A tool for calculating free and bound concentations and free energies of transfer\n"
			+ "       Git version: " + getGitBranch()  + " "+ getGitVersion();

	private static final String EXAMPLES = "  "
		        + NAME
			+ " -p -o phaseOutFile.xml --solute /usr/share/ssip-repo/phasetransfer/ExampleData/watersolute.xml --solvent /usr/share/ssip-repo/phasetransfer/ExampleData/waterphasesolvent.xml"
			+ "\n"
			+ "  "
			+ NAME
			+ " -e -o energyOutFile.xml --solute /usr/share/ssip-repo/phasetransfer/ExampleData/watersolute.xml --solvent /usr/share/ssip-repo/phasetransfer/ExampleData/waterphasesolvent.xml"
			+ "\n";
	
	private static final String CONCENTRATION_UNIT_STRING = "concentrationUnit";
	private static final String PHASE_FILE_STRING = "phaseFile";
	private static final String BINDING_ENERGY_STRING = "BindingEnergy";
	private static final String CONFINEMENT_ENERGY_STRING = "ConfinementEnergy";
	private static final String TOTAL_ENERGY_STRING = "TotalEnergy";
	private static final String LOGP_STRING = "LogP";
	private static final String CONC_CALC_STRING = "conccalc";
	private static final String GAS_CALC_STRING = "gascalc";
	private static final String TEMPERATURE_STRING = "temperature";
	private static final String TEMPERATURE_UNIT_STRING = "temperatureUnit";
	
	private static OptionParser optionParser = null;
	private static OptionSet optionSet = null;
	
	private static MixtureContainer mixtureMap;
	private static Mixture mixture;
	private static PhaseSystemContainer phaseSystemContainer;
	private static PhaseSystemMixtureContainer phaseSystemMixtureContainer;
	
	private static URI solventFileURI;
	private static URI soluteFileURI;
	private static URI phaseFileURI;
	private static URI outputFileURI;
	private static URI fracOccFileURI;
	
	private PhaseTransferCli(){
		
	}
	
	/**
	 * main class run when called on the command line.
	 * 
	 * @param args CLI arguments.
	 */
	public static void main(String[] args) {
		// Populate the internal state of the object as directed by the command
		// line options
		parseArguments(args);
		LOG.info("Writing output to: {}", outputFileURI);
		try {
			if (optionSet.has("e")) {
				writeFreeEnergies();
			} else if (optionSet.has("p")) {
				writeMixtures();
			} else if (optionSet.has("association")) {
				writeAssociationEnergies();
			} else if (optionSet.has("concfrac")) {
				writeConcentrationFractions();
			}
			if (optionSet.has("f")) {
				writeFractionalOccupancies();
			}
		} catch (JAXBException e) {
			LOG.error("JAXBException: {}", e.getMessage());
		}
	}
	
	private static void parseArguments(String[] args) {

		optionParser = getOptionParser();

		if (args.length == 0) {
			printUsage();
			System.exit(0);
		}

		try {
			processArguments(args);
		} catch (Exception e) {
			LOG.error(e);
			LOG.error("Exception {}", e.getMessage());
			LOG.debug("Exception trace: {}", Arrays.toString(e.getStackTrace()));
		}
	}
	
	/**
	 * Action the command line arguments
	 * 
	 * @param args
	 *            Command line arguments
	 * @throws JAXBException 
	 */
	private static void processArguments(String[] args) throws JAXBException {

		optionSet = optionParser.parse(args);

		if (optionSet.has("h")) {
			printUsage();
			System.exit(0);
		}
		if (optionSet.has(PHASE_FILE_STRING)) {
			LOG.info("parsing phases");
			parsePhases();
		} else {
			LOG.info("parsing solvents and solutes");
			parseSolventAndSolutes();
		}
		setOutputfileURI();
	}

	/**
	 * Sets up all the valid options for this command
	 * 
	 * @return OptionParser valid options
	 */
	private static OptionParser getOptionParser() {
		return new PhasetransferOptionParser();
	}

	private static void setPhaseFileURI(){
		File phaseFile = new File((String) optionSet.valueOf(PHASE_FILE_STRING));
		phaseFileURI = phaseFile.toURI();
	}

	private static void parsePhases() throws JAXBException{
		setPhaseFileURI();
		if (optionSet.has("phaseCollection")) {
			parsePhaseCollection();
		} else if (optionSet.has("phaseCollectionList")) {
			parsePhaseCollectionList();
		}
		
	}

	private static void parsePhaseCollectionList() throws JAXBException{
		if (optionSet.has(GAS_CALC_STRING)) {
			phaseSystemContainer = MixtureXmlReader.calculatePhaseSystemsFromMixture(phaseFileURI, false);
			LOG.debug("phaseSystemcontainer: {}", phaseSystemContainer);
		} else if (optionSet.has("calc")) {
			mixtureMap = MixtureXmlReader.parsePhaseCollectionListFileAndCalculateConcentrations(phaseFileURI, optionSet.has(CONC_CALC_STRING));
		} else {
			mixtureMap = MixtureXmlReader.parsePhaseCollectionListFileAndReadConcentrations(phaseFileURI);
		}
	}

	private static void parsePhaseCollection() throws JAXBException{

		if (optionSet.has(GAS_CALC_STRING) && optionSet.has("solute")){
			phaseSystemMixtureContainer = MixtureXmlReader.calculatePhaseSystemsWithSolutes(phaseFileURI, soluteFileURI);
		} else if (optionSet.has(GAS_CALC_STRING)){
			phaseSystemContainer = MixtureXmlReader.calculatePhaseSystemsFromMixture(phaseFileURI, true);
			LOG.debug("phaseSystemcontainer: {}", phaseSystemContainer);
		} else if (optionSet.has("calc")) {
			mixture = MixtureXmlReader.parsePhaseCollectionFileAndCalculateConcentrations(phaseFileURI, optionSet.has(CONC_CALC_STRING));
		} else {
			mixture = MixtureXmlReader.parsePhaseCollectionFileAndReadConcentrations(phaseFileURI);
		}
	}

	private static void setSoluteAndSolventURIs(){
		File soluteFile = new File((String) optionSet.valueOf("solute"));
		soluteFileURI = soluteFile.toURI();
		File solventFile = new File((String) optionSet.valueOf("solvent"));
		solventFileURI = solventFile.toURI();
	}
	private static void parseSolventAndSolutes() throws JAXBException{
		LOG.debug("setting input URIs");
		setSoluteAndSolventURIs();
		LOG.debug("value of temperature: {}", optionSet.valueOf(TEMPERATURE_STRING));
		LOG.debug("value of temperatureUnit: {}", optionSet.valueOf(TEMPERATURE_UNIT_STRING));
		Temperature temperature = new Temperature((double) optionSet.valueOf(TEMPERATURE_STRING), TemperatureUnit.valueOf((String) optionSet.valueOf(TEMPERATURE_UNIT_STRING)));
		LOG.debug("value of concentrationUnit: {}", optionSet.valueOf(CONCENTRATION_UNIT_STRING));
		ConcentrationUnit concentrationUnit = ConcentrationUnit.valueOf((String) optionSet.valueOf(CONCENTRATION_UNIT_STRING));
		mixtureMap = MixtureXmlReader.generateMixturesForSolutesFromFiles(soluteFileURI, solventFileURI, temperature, concentrationUnit, optionSet.has(CONC_CALC_STRING));
		LOG.debug("Mixture map keys: {}", mixtureMap.getMixtureMap().keySet());
	}
	
	private static void setOutputfileURI(){
		outputFileURI = new File((String) optionSet.valueOf("o")).toURI();
		if (optionSet.has("f")) {
			fracOccFileURI = new File((String) optionSet.valueOf("f")).toURI();
		}
		
	}
	
	private static void writeMixtures() throws JAXBException{
		if (optionSet.has(GAS_CALC_STRING)) {
			MixtureXmlWriter.marshalPhaseSystemContainer(phaseSystemContainer, outputFileURI);
		} else {
			if (optionSet.has("phaseCollection")) {
				MixtureXmlWriter.marshal(mixture, outputFileURI);
			} else {
				MixtureXmlWriter.marshalContainer(mixtureMap, outputFileURI);
			}
		}		
	}
	
	private static Map<String, List<EnergyValue>> calculateSoluteFreeEnergies(String moleculeID, IEnergyDifferenceCalculator mixture){
		HashMap<String, List<EnergyValue>> energyValues = new HashMap<>();
		LOG.debug("value of e: {}", optionSet.valueOf("e"));
		switch ((String) optionSet.valueOf("e")) {
		case "all":
			LOG.trace("all branch");
			energyValues.put(BINDING_ENERGY_STRING, mixture.calculateBindingEnergyValuesAllPhasesAllValues(moleculeID));
			energyValues.put(CONFINEMENT_ENERGY_STRING, mixture.calculateConfinementEnergyValuesAllPhasesAllValues(moleculeID));
			energyValues.put(TOTAL_ENERGY_STRING, mixture.calculateFreeEnergyValuesAllPhasesAllValues(moleculeID));
			break;
		case "binding":
			LOG.trace("binding branch");
			energyValues.put(BINDING_ENERGY_STRING, mixture.calculateBindingEnergyValuesAllPhasesAllValues(moleculeID));
			break;
		case "confinement":
			LOG.trace("confinement branch");
			energyValues.put(CONFINEMENT_ENERGY_STRING, mixture.calculateConfinementEnergyValuesAllPhasesAllValues(moleculeID));
			break;
		case "total":
			LOG.trace("total branch");
			energyValues.put(TOTAL_ENERGY_STRING, mixture.calculateFreeEnergyValuesAllPhasesAllValues(moleculeID));
			break;
		case "solvationtotal":
			LOG.trace("solvation branch");
			energyValues.put(TOTAL_ENERGY_STRING, mixture.calculateFreeEnergySolvationAllPhases(moleculeID));
			break;
		case "solvationbinding":
			LOG.trace("solvation binding branch");
			energyValues.put(BINDING_ENERGY_STRING, mixture.calculateBindingEnergySolvationAllPhases(moleculeID));
			break;
		case "logp":
			LOG.trace("logP branch");
			energyValues.put(LOGP_STRING, mixture.calculateLogPAllPhases(moleculeID));
			break;
		default:
			break;
		}
		LOG.debug("energyValues: {}", energyValues);
		return energyValues;
	}
	
	private static Map<String, List<EnergyValue>> calculateMolecularSolvationFreeEnergies(ISolvationEnergyCalculator iSolvationEnergyCalculator){
		HashMap<String, List<EnergyValue>> energyValues = new HashMap<>();
		LOG.debug("value of e: {}", optionSet.valueOf("e"));
		switch ((String) optionSet.valueOf("e")) {
		case "all":
			LOG.trace("all branch");
			energyValues.put(BINDING_ENERGY_STRING, iSolvationEnergyCalculator.calculateBindingEnergySolvationAllMoleculesEveryPhase());
			energyValues.put(CONFINEMENT_ENERGY_STRING, iSolvationEnergyCalculator.calculateConfinementEnergySolvationAllMoleculesEveryPhase());
			energyValues.put(TOTAL_ENERGY_STRING, iSolvationEnergyCalculator.calculateFreeEnergySolvationAllMoleculesEveryPhase());
			break;
		case "solvationtotal":
			LOG.trace("solvation branch");
			
			energyValues.put(TOTAL_ENERGY_STRING, iSolvationEnergyCalculator.calculateFreeEnergySolvationAllMoleculesEveryPhase());
			break;
		case "solvationbinding":
			LOG.trace("solvation binding branch");
			energyValues.put(BINDING_ENERGY_STRING, iSolvationEnergyCalculator.calculateBindingEnergySolvationAllMoleculesEveryPhase());
			break;
		default:
			break;
		}
		return energyValues;
	}
	
	private static void writeFreeEnergies() throws JAXBException{
		HashMap<String, List<EnergyValue>> energyValues = new HashMap<>();
		LOG.debug("energy values before calculation: {}", energyValues);
		if (optionSet.has("solvation")) {
			energyValues = (HashMap<String, List<EnergyValue>>) calculateFreeEnergiesInPhase();
		} else if (optionSet.has(GAS_CALC_STRING)) {
			energyValues = (HashMap<String, List<EnergyValue>>) calculateSoluteFreeEnergiesInPhaseSystem();
		} else {
			energyValues = (HashMap<String, List<EnergyValue>>) calculateSoluteFreeEnergiesInPhase();
		}
		
		LOG.debug("energy values after calculation: {}", energyValues);
		EnergyValueCollection energyValueCollection = createEnergyValueCollection(energyValues);
		EnergyXmlWriter.marshalCollection(energyValueCollection, outputFileURI);
	}
	
	private static Map<String, List<EnergyValue>> calculateFreeEnergiesInPhase(){
		HashMap<String, List<EnergyValue>> energyValues = new HashMap<>();
		for (Map.Entry<String, Mixture> mixtureEntry : mixtureMap.getMixtureMap().entrySet()) {
			LOG.debug("mixture key: {}", mixtureEntry.getKey());
			HashMap<String, List<EnergyValue>> mixtureEnergyValues = (HashMap<String, List<EnergyValue>>) calculateMolecularSolvationFreeEnergies(mixtureEntry.getValue());
			LOG.debug("mixture energy values: {}", mixtureEnergyValues);
			for (Map.Entry<String, List<EnergyValue>> mapEntry : mixtureEnergyValues.entrySet()) {
				if (energyValues.containsKey(mapEntry.getKey())) {
					energyValues.get(mapEntry.getKey()).addAll(mapEntry.getValue());
				} else {
					energyValues.put(mapEntry.getKey(), mapEntry.getValue());
				}
			}
		}
		return energyValues;
	}
	
	private static Map<String, List<EnergyValue>> calculateSoluteFreeEnergiesInPhaseSystem(){
		HashMap<String, List<EnergyValue>> energyValues = new HashMap<>();
		LOG.debug("energy values before calculation: {}", energyValues);
		for (Map.Entry<String, PhaseSystemMixture> mixtureEntry : phaseSystemMixtureContainer.getPhaseSystemMixtureMap().entrySet()) {
			HashMap<String, List<EnergyValue>> mixtureEnergyValues = (HashMap<String, List<EnergyValue>>) calculateSoluteFreeEnergies(mixtureEntry.getKey(), mixtureEntry.getValue());
			LOG.debug("mixture energy values: {}", mixtureEnergyValues);
			for (Map.Entry<String, List<EnergyValue>> mapEntry : mixtureEnergyValues.entrySet()) {
				if (energyValues.containsKey(mapEntry.getKey())) {
					energyValues.get(mapEntry.getKey()).addAll(mapEntry.getValue());
				} else {
					energyValues.put(mapEntry.getKey(), mapEntry.getValue());
				}
			}
		}
		return energyValues;
	}
	
	private static Map<String, List<EnergyValue>> calculateSoluteFreeEnergiesInPhase(){
		HashMap<String, List<EnergyValue>> energyValues = new HashMap<>();
		LOG.debug("energy values before calculation: {}", energyValues);
		for (Map.Entry<String, Mixture> mixtureEntry : mixtureMap.getMixtureMap().entrySet()) {
			LOG.debug("mixture key: {}", mixtureEntry.getKey());
			HashMap<String, List<EnergyValue>> mixtureEnergyValues = (HashMap<String, List<EnergyValue>>) calculateSoluteFreeEnergies(mixtureEntry.getKey(), mixtureEntry.getValue());
			LOG.debug("mixture energy values: {}", mixtureEnergyValues);
			for (Map.Entry<String, List<EnergyValue>> mapEntry : mixtureEnergyValues.entrySet()) {
				if (energyValues.containsKey(mapEntry.getKey())) {
					energyValues.get(mapEntry.getKey()).addAll(mapEntry.getValue());
				} else {
					energyValues.put(mapEntry.getKey(), mapEntry.getValue());
				}
			}
		}
		return energyValues;
	}
	
	private static EnergyValueCollection createEnergyValueCollection(Map<String, List<EnergyValue>> energyValues) {
		EnergyValueCollection energyValueCollection = new EnergyValueCollection();
		if (energyValues.containsKey(BINDING_ENERGY_STRING)) {
			energyValueCollection.setBindingEnergies(energyValues.get(BINDING_ENERGY_STRING));
		} 
		if (energyValues.containsKey(CONFINEMENT_ENERGY_STRING)) {
			energyValueCollection.setConfinementEnergies(energyValues.get(CONFINEMENT_ENERGY_STRING));
		} 
		if (energyValues.containsKey(TOTAL_ENERGY_STRING)) {
			energyValueCollection.setTotalEnergies(energyValues.get(TOTAL_ENERGY_STRING));
		}
		if (energyValues.containsKey(LOGP_STRING)) {
			energyValueCollection.setPartitionCoefficients(energyValues.get(LOGP_STRING));
		}
		LOG.debug("energy values collection: {}", energyValueCollection);
		LOG.debug("energy values collection Binding Energy: {}", energyValueCollection.getBindingEnergies());
		LOG.debug("energy values collection Confinement Energy: {}", energyValueCollection.getConfinementEnergies());
		LOG.debug("energy values collection Total Energy: {}", energyValueCollection.getTotalEnergies());
		LOG.debug("energy values collection Partition Coefficients: {}", energyValueCollection.getPartitionCoefficients());
		return energyValueCollection;
	}
	
	private static void writeAssociationEnergies() throws JAXBException {
		ArrayList<AssociationEnergyValue> associationEnergyValues = new ArrayList<>();
		if (optionSet.has(GAS_CALC_STRING) && phaseSystemMixtureContainer != null) {
			for (Map.Entry<String, PhaseSystemMixture> mixtureEntry : phaseSystemMixtureContainer.getPhaseSystemMixtureMap().entrySet()) {
				associationEnergyValues.addAll(mixtureEntry.getValue().calculateAssociationEnergyValuesAllPhases());
			}
		} else if (optionSet.has(GAS_CALC_STRING) && phaseSystemContainer != null) {
			for (Map.Entry<String, PhaseSystem> phaseSystemEntry : phaseSystemContainer.getPhaseSystemMap().entrySet()) {
				associationEnergyValues.addAll(phaseSystemEntry.getValue().getCondensedPhase().calculateMoleculeAssociationEnergyValues());
			}
		} else {
			for (Map.Entry<String, Mixture> mixtureEntry : mixtureMap.getMixtureMap().entrySet()) {
				associationEnergyValues.addAll(mixtureEntry.getValue().calculateAssociationEnergyValuesAllPhases());
			}
		}
		EnergyXmlWriter.marshalAssociationCollection(new AssociationEnergyValueCollection(associationEnergyValues), outputFileURI);
	}
	
	private static void writeConcentrationFractions() throws JAXBException {
		ArrayList<PhaseConcentrationFraction> phaseConcentrationFractions = new ArrayList<>();
		if (optionSet.has(GAS_CALC_STRING) && phaseSystemMixtureContainer != null) {
			for (Map.Entry<String, PhaseSystemMixture> mixtureEntry : phaseSystemMixtureContainer.getPhaseSystemMixtureMap().entrySet()) {
				for (PhaseSystem phaseSystem : mixtureEntry.getValue().getPhaseSystemMap().values()) {
					phaseConcentrationFractions.addAll(phaseSystem.calculatePhaseConcentrationFractions());
				}
			}
		} else if (optionSet.has(GAS_CALC_STRING) && phaseSystemContainer != null) {
			for (Map.Entry<String, PhaseSystem> phaseSystemEntry : phaseSystemContainer.getPhaseSystemMap().entrySet()) {
				phaseConcentrationFractions.addAll(phaseSystemEntry.getValue().calculatePhaseConcentrationFractions());
			} 
		} else {
			for (Map.Entry<String, Mixture> mixtureEntry : mixtureMap.getMixtureMap().entrySet()) {
				phaseConcentrationFractions.addAll(mixtureEntry.getValue().calculatePhaseConcentrationFractions());
			}
		}
		MixtureXmlWriter.marshalPhaseConcentrationFractionCollection(new PhaseConcentrationFractionCollection(phaseConcentrationFractions), outputFileURI);
	}
	
	private static void writeFractionalOccupancies() throws JAXBException {
		FractionalOccupancyCollection fractionalOccupancyCollection = new FractionalOccupancyCollection(new HashSet<>());
		if (mixtureMap != null) {
			for (Map.Entry<String, Mixture> mixtureEntry : mixtureMap.getMixtureMap().entrySet()) {
				fractionalOccupancyCollection.addFractionalOccupancyCollection(mixtureEntry.getValue().getFractionalOccupancyCollection());
			}
		}
		FractionalOccupancyXmlWriter.marshalCollection(fractionalOccupancyCollection, fracOccFileURI);
	}
	
	private static void printUsage() {
		LOG.info("");
		LOG.info(TITLE);
		LOG.info("");
		try {
			optionParser.printHelpOn(System.out);
		} catch (IOException e) {
			LOG.info("Cannot print help options to System.out", e);
		}
		LOG.info("");
		LOG.info("Example usage:");
		LOG.info(EXAMPLES);
		System.exit(0);
	}
	
	private static String getGitVersion() {
		return repositoryState.getCommitId().substring(0, 7);
	}

	private static String getGitBranch() {
		return repositoryState.getBranch();
	}
}
