/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Class for storing the Energy values, collected in lists.
 * 
 * @author mdd31
 *
 */
@XmlRootElement(name = "EnergyValues", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnergyValueCollection {
	
	@XmlElementWrapper(name = "FreeEnergyCollection", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlElement(name = "FreeEnergy", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ArrayList<EnergyValue> totalEnergies;
	
	@XmlElementWrapper(name = "BindingEnergyCollection", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlElement(name = "BindingEnergy", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ArrayList<EnergyValue> bindingEnergies;
	
	@XmlElementWrapper(name = "ConfinementEnergyCollection", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlElement(name = "ConfinementEnergy", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ArrayList<EnergyValue> confinementEnergies;
	
	@XmlElementWrapper(name = "PartitionCoefficientCollection", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlElement(name = "PartitionCoefficient", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ArrayList<EnergyValue> partitionCoefficients;
	
	
	public EnergyValueCollection(){
		/**
		 * Constructor.
		 */
	}

	public List<EnergyValue> getTotalEnergies() {
		return totalEnergies;
	}

	public void setTotalEnergies(List<EnergyValue> totalEnergies) {
		this.totalEnergies = (ArrayList<EnergyValue>) totalEnergies;
	}

	public List<EnergyValue> getBindingEnergies() {
		return bindingEnergies;
	}

	public void setBindingEnergies(List<EnergyValue> bindingEnergies) {
		this.bindingEnergies = (ArrayList<EnergyValue>) bindingEnergies;
	}

	public List<EnergyValue> getConfinementEnergies() {
		return confinementEnergies;
	}

	public void setConfinementEnergies(List<EnergyValue> confinementEnergies) {
		this.confinementEnergies = (ArrayList<EnergyValue>) confinementEnergies;
	}

	public List<EnergyValue> getPartitionCoefficients() {
		return partitionCoefficients;
	}

	public void setPartitionCoefficients(List<EnergyValue> partitionCoefficients) {
		this.partitionCoefficients = (ArrayList<EnergyValue>) partitionCoefficients;
	}

}
