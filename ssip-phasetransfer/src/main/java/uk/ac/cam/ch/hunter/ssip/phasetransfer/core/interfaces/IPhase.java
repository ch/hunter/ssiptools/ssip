/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces;

import java.util.List;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.FractionalOccupancy;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;

/**
 * Interface for Phase like objects.
 * 
 * @author mdd31
 *
 */
public interface IPhase {

	/**
	 * VdW volumes of each molecule are returned.
	 * 
	 * N length {@link ArrayRealVector}, where N is total number of Molecules.
	 * 
	 * @return volumes
	 */
	public ArrayRealVector getVdWVolumesByMolecule();
	
	/**
	 * VdW volumes of corresponding molecule for each SSIP are returned.
	 * 
	 * N length {@link ArrayRealVector}, where N is total number of SSIPs. 
	 * 
	 * @return volumes
	 */
	public ArrayRealVector getVdWVolumes();
	
	/**
	 * {@link Map} of moleculeID to mole fraction.
	 * 
	 * @return mole fractions
	 */
	public Map<String, Double> getMoleFractionsByMoleculeID();
	
	/**
	 * Mole fraction of each molecule are returned. Order corresponds to lexicographic ordering of moleculeIDs.
	 * 
	 * N length {@link ArrayRealVector}, where N is total number of Molecules.
	 * 
	 * @return mole fractions
	 */
	public ArrayRealVector getMoleFractionsByMolecule();
	
	/**
	 * Mole fractions of corresponding molecule for each SSIP are returned.
	 * 
	 * N length {@link ArrayRealVector}, where N is total number of SSIPs.
	 * 
	 * @return mole fractions
	 */
	public ArrayRealVector getMoleFractions();
	
	/**
	 * Returns the list of all the SSIP in the {@link IPhase}. This is canonicalised. 
	 * 
	 * @return ssipconcentrations
	 */
	public List<SsipConcentration> getsortedSsips();
	
	/**
	 * This returns a {@link Map} of all the {@link PhaseMolecule}s labelled by MoleculeID.
	 * 
	 * @return phase molecules
	 */
	public Map<String, PhaseMolecule> getPhaseMolecules();
	
	/**
	 * This converts the concentrations in the IPhase to another {@link ConcentrationUnit} and returns a new {@link IPhase}.
	 * 
	 * @param concentrationUnit concentration unit.
	 * @return iPhase
	 */
	public IPhase convertConcentrationUnitTo(ConcentrationUnit concentrationUnit);
	
	/**
	 * This retrieves the {@link ConcentrationUnit} of the {@link IPhase}.
	 * 
	 * @return concentration unit
	 */
	public ConcentrationUnit getConcentrationUnit();
	
	/**
	 * This retrieves the fractional occupancy information of the {@link IPhase}.
	 * 
	 * @param soluteID solute ID
	 * @return fractional occupancy
	 */
	public FractionalOccupancy getFractionalOccupancy(String soluteID);
	
}
