/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer;

import java.util.HashMap;
import java.util.Map;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolute;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;

/**
 * Class creates mixtures where the solute is constant but the solvent changes between each phase in the mixture.
 * 
 * @author mdd31
 *
 */
public class MixtureAssembler {
	
	private static final PhaseAssembler phaseAssembler = new PhaseAssembler();
	
	/**
	 * Constructor
	 */
	private MixtureAssembler(){
		/**
		 * Initialise Assembler
		 */
	}
	
	/**
	 * Creates a Mixture object for phases with the same solute but different solvents.
	 * 
	 * @param solute Solute
	 * @param solventInformationByID Solvent Information.
	 * @param temperature Temperature
	 * @param concentrationUnit Concentration Unit.
	 * @return mixture.
	 */
	public static Mixture generateMixtureForSolute(PhaseSolute solute, Map<String, PhaseSolvent> solventInformationByID, Temperature temperature, ConcentrationUnit concentrationUnit){
		HashMap<String, Phase> phaseMapForSolute = (HashMap<String, Phase>) phaseAssembler.generatePhasesFromSoluteWithSolvents(solute, solventInformationByID, temperature, concentrationUnit);
		Mixture mixture = new Mixture(phaseMapForSolute);
		mixture.setSoluteID(solute.getSoluteID());
		return mixture;
	}
	
	/**
	 * This contains a key which is the soluteID, and the value is the Mixture object for the given solute in a range of solvent systems.
	 * 
	 * @param solute Solute
	 * @param solventInformationByID Solvent information.
	 * @param temperature Temperature.
	 * @param concentrationUnit concentration unit.
	 * @return mixture map
	 */
	public static Map<String, Mixture> generateMixtureForSoluteWithID(PhaseSolute solute, Map<String, PhaseSolvent> solventInformationByID, Temperature temperature, ConcentrationUnit concentrationUnit){
		Mixture mixtureForSolute = generateMixtureForSolute(solute, solventInformationByID, temperature, concentrationUnit);
		
		HashMap<String, Mixture> mixtureMap = new HashMap<>();
		mixtureMap.put(solute.getSoluteID(), mixtureForSolute);
		return mixtureMap;
	}
	
	/**
	 *
	 * This creates a map of key-value pairs where the keys are the solutes and the values are the corresponding Mixtures.
	 * 
	 * @param soluteInformation Solute map.
	 * @param solventInformationByID Solvent map.
	 * @param temperature Temperature.
	 * @param concentrationUnit Concentration unit.
	 * @return mixture map
	 */
	public static Map<String, Mixture> generateMixturesforSolutes(Map<String, PhaseSolute> soluteInformation, Map<String, PhaseSolvent> solventInformationByID, Temperature temperature, ConcentrationUnit concentrationUnit){
		HashMap<String, Mixture> mixturesForSolutesMap = new HashMap<>();
		for (Map.Entry<String, PhaseSolute> solute : soluteInformation.entrySet()) {
			HashMap<String, Mixture> mixtureMap = (HashMap<String, Mixture>) generateMixtureForSoluteWithID(solute.getValue(), solventInformationByID, temperature, concentrationUnit);
			mixturesForSolutesMap.putAll(mixtureMap);
		}
		return mixturesForSolutesMap;
	}
}
