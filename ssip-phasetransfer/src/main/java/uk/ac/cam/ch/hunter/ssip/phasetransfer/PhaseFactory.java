/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer;

import java.util.Set;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.ConcentrationCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.PhaseConcentrationCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;

/**
 * This provides methods to generate a phase object with the concentrations of
 * the free and bound concentrations set.
 * 
 * @author mdd31
 * 
 */
public class PhaseFactory {

	private static final Logger LOG = LogManager.getLogger(PhaseFactory.class);
	
	/**
	 * Constructor.
	 */
	public PhaseFactory() {
		/**
		 * Initialise factory.
		 */
	}

	/**
	 * This method creates a phase, calculates the concentrations of the free
	 * and bound SSIPs, and the return a new phase where the
	 * SSIPConcentrationLists in the phase have the free and bound
	 * concentrations set.
	 * 
	 * The free and bound concentrations are calculated in the SSIPConcentration normalised scale, and then the results are returned in the concentration unit specified.
	 * 
	 * @param ssipConcentrationListByMolecule SSIP Concentrations
	 * @param temperature Temperature.
	 * @param concentrationUnit Concentration unit.
	 * @return phase
	 */
	public Phase generatePhase(
			Set<PhaseMolecule> ssipConcentrationListByMolecule,
			Temperature temperature, ConcentrationUnit concentrationUnit) {
		LOG.debug("Phase molecules: {}", ssipConcentrationListByMolecule);
		Phase initialPhase = new Phase(ssipConcentrationListByMolecule,
				temperature, concentrationUnit);
		LOG.debug("initial phase mol IDs: {}", initialPhase.getPhaseMolecules().keySet());
		LOG.trace("SSIPs in initial phase: {}", initialPhase.getsortedSSIPConcentrations());
		PhaseConcentrationCalculator phaseConcentrationCalculator = new PhaseConcentrationCalculator(
				initialPhase.convertConcentrationUnitTo(ConcentrationUnit.SSIPCONCENTRATION));
		LOG.trace("SSIPs in phase calculator: {}", phaseConcentrationCalculator.getSsipConcentrationList());
		Phase phaseWithConcentrations = phaseConcentrationCalculator.setFreeAndBoundConcentrationsOfPhase();
		LOG.trace("SSIPs in new phase: {}", phaseWithConcentrations.getsortedSSIPConcentrations());
		return phaseWithConcentrations.convertConcentrationUnitTo(concentrationUnit);
	}

	/**
	 * This generates a Phase with concentrations from the given double matrices used for the free and bound concentrations.
	 * 
	 * @param ssipConcentrationListByMolecule SSIP concentrations by list.
	 * @param temperature Temperature.
	 * @param concentrationUnit Concentration unit.
	 * @param freeConcentrations free concentrations.
	 * @param boundConcentrations bound concentrations.
	 * @return phase
	 */
	public Phase generatePhase(Set<PhaseMolecule> ssipConcentrationListByMolecule, Temperature temperature, ConcentrationUnit concentrationUnit, ArrayRealVector freeConcentrations, Array2DRowRealMatrix boundConcentrations){
		Phase initialPhase = new Phase(ssipConcentrationListByMolecule, temperature, concentrationUnit);
		ConcentrationCalculator concentrationCalculator = new ConcentrationCalculator(initialPhase.getTotalConcentrations(), freeConcentrations, boundConcentrations);
		PhaseConcentrationCalculator phaseConcentrationCalculator = new PhaseConcentrationCalculator(initialPhase, concentrationCalculator);
		return phaseConcentrationCalculator.setFreeAndBoundConcentrationsOfPhase();
	}
}
