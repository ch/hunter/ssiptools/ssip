/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.io.SchemaMaker;
import uk.ac.cam.ch.hunter.ssip.core.io.XmlValidationEventHandler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.PhaseSystemConcentrationCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.PhaseSystemConcentrationCalculatorWithSolute;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.TotalConcentrationMatcher;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.MixtureAssembler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.MixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolute;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SoluteContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;

/**
 * Class to read in and create mixtures from solute and solvent information.
 * 
 * @author mdd31
 *
 */
public class MixtureXmlReader {

	private static final Logger LOG = LogManager
			.getLogger(MixtureXmlReader.class);
	
	private static final Schema phaseSchema = SchemaMaker.createPhaseSchema();
	private static final PhaseFactory PHASE_FACTORY = new PhaseFactory();
	
	/**
	 * Constructor
	 */
	private MixtureXmlReader(){
		/**
		 * initialise class
		 */
	}
	
	/**
	 * Unmarshal XML in file at given {@link URI} to a {@link Mixture}.
	 * 
	 * @param filename URI filename.
	 * @return mixture
	 * @throws JAXBException exception.
	 */
	public static Mixture unmarshal(URI filename) throws JAXBException{
		File file = new File(filename);
		JAXBContext jaxbContext = JAXBContext.newInstance(Mixture.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		Mixture mixture = (Mixture) jaxbUnmarshaller.unmarshal(file);
		mixture.setAssociationConstants();
		mixture.setBoundConcentrations();
		return mixture;
	}
	
	/**
	 * Unmarshal XML in file at given {@link URI} to a {@link MixtureContainer}.
	 * 
	 * @param filename URI.
	 * @return mixture container
	 * @throws JAXBException exception.
	 */
	public static MixtureContainer unmarshalContainer(URI filename) throws JAXBException {
		File file = new File(filename);
		JAXBContext jaxbContext = JAXBContext.newInstance(MixtureContainer.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		MixtureContainer mixtureContainer = (MixtureContainer) jaxbUnmarshaller.unmarshal(file);
		mixtureContainer.setAssociationConstants();
		mixtureContainer.setBoundConcentrations();
		return mixtureContainer;
	}
	
	/**
	 * Unmarshal XML in file at given {@link URI} to a {@link PhaseSystem}.
	 * 
	 * @param filename URI.
	 * @return phase system
	 * @throws JAXBException exception.
	 */
	public static PhaseSystem unmarshalPhaseSystem(URI filename) throws JAXBException {
		File file = new File(filename);
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSystem.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		PhaseSystem phaseSystem = (PhaseSystem) jaxbUnmarshaller.unmarshal(file);
		phaseSystem.setAssociationConstants();
		phaseSystem.setBoundConcentrations();
		return phaseSystem;
	}
	
	/**
	 * Unmarshal XML in file at given {@link URI} to a {@link PhaseSystemContainer}.
	 * 
	 * @param filename URI
	 * @return phase system container
	 * @throws JAXBException exception.
	 */
	public static PhaseSystemContainer unmarshalPhaseSystemContainer(URI filename) throws JAXBException {
		File file = new File(filename);
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSystemContainer.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		PhaseSystemContainer phaseSystemContainer = (PhaseSystemContainer) jaxbUnmarshaller.unmarshal(file);
		phaseSystemContainer.setAssociationConstants();
		phaseSystemContainer.setBoundConcentrations();
		return phaseSystemContainer;
	}
	
	/**
	 * Unmarshal a {@link PhaseSystemMixture} from file. 
	 * 
	 * @param filename URI
	 * @return phase system mixture
	 * @throws JAXBException exception.
	 */
	public static PhaseSystemMixture unmarshalPhaseSystemMixture(URI filename) throws JAXBException{
		File file = new File(filename);
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSystemMixture.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		PhaseSystemMixture phaseSystemContainer = (PhaseSystemMixture) jaxbUnmarshaller.unmarshal(file);
		phaseSystemContainer.setAssociationConstants();
		phaseSystemContainer.setBoundConcentrations();
		return phaseSystemContainer;
	}
	
	/**
	 * Unmarshal a {@link PhaseSystemMixtureContainer} from file.
	 * 
	 * @param filename URI.
	 * @return phase system mixture container
	 * @throws JAXBException exception.
	 */
	public static PhaseSystemMixtureContainer unmarshalPhaseSystemMixtureContainer(URI filename) throws JAXBException{
		File file = new File(filename);
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSystemMixtureContainer.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		PhaseSystemMixtureContainer phaseSystemContainer = (PhaseSystemMixtureContainer) jaxbUnmarshaller.unmarshal(file);
		phaseSystemContainer.setAssociationConstants();
		phaseSystemContainer.setBoundConcentrations();
		return phaseSystemContainer;
	}
	
	/**
	 * Parses the solute XML file and returns the solute information by soluteID.
	 *  
	 * @param soluteURI solute URI.
	 * @return solute container
	 * @throws JAXBException exception.
	 */
	static SoluteContainer parseSoluteXMLFile(URI soluteURI) throws JAXBException{
		return SoluteXmlReader.unmarshalContainer(soluteURI);
	}
	
	/**
	 * Parses the solvent XML file and returns the solvent information by solventID.
	 * 
	 * @param solventURI URI.
	 * @return solvent container
	 * @throws JAXBException exception.
	 */
	static SolventContainer parseSolventXMLFile(URI solventURI) throws JAXBException{
		return SolventXmlReader.unmarshalContainer(solventURI);
	}
	
	/**
	 * This generates the Mixture map from the given information.
	 * 
	 * @param soluteInformation solute container
	 * @param solventInformationByID solvent information
	 * @param temperature Temperature
	 * @param concentrationUnit Concentration unit.
	 * @return mixture map
	 */
	static Map<String, Mixture> generateMixtureForSolutes(SoluteContainer soluteInformation, SolventContainer solventInformationByID, Temperature temperature, ConcentrationUnit concentrationUnit){
		return MixtureAssembler.generateMixturesforSolutes(soluteInformation.getPhaseSolutes(), solventInformationByID.getSolventMap(), temperature, concentrationUnit);
	}
	
	/**
	 * This calculates the concentrations for the phases in the given mixture.
	 * 
	 * @param mixture Mixture.
	 */
	private static Mixture calculateConcentrationsForPhasesInMixture(Mixture mixture){
		HashMap<String, Phase> calculatedPhaseMap = new HashMap<>();
		for (Map.Entry<String, Phase> phaseMapEntry : mixture.getPhaseMap().entrySet()) {
			LOG.debug("SSIP values from read: {}", phaseMapEntry.getValue().getsortedSSIPConcentrations());
			HashSet<PhaseMolecule> phaseMolecules = (HashSet<PhaseMolecule>) phaseMapEntry.getValue().getPhaseMoleculesSet();

			Phase calculatedPhase = PHASE_FACTORY.generatePhase(phaseMolecules, phaseMapEntry.getValue().getTemperature(), phaseMapEntry.getValue().getConcentrationUnit());
			LOG.debug("SSIPValues after calculation: {}", calculatedPhase.getsortedSSIPConcentrations());
			LOG.info("phase solvent ID: {}", calculatedPhase.getSolventID());
			calculatedPhase.setSolventID(phaseMapEntry.getKey());
			calculatedPhaseMap.put(phaseMapEntry.getKey(), calculatedPhase);
		}
		return new Mixture(calculatedPhaseMap);
	}
	
	/**
	 * This generates a Map of the Mixtures from the information in the given solute and solvent files, and the temperature and units given.
	 * 
	 * @param soluteURI URI
	 * @param solventURI URI
	 * @param temperature Temperature
	 * @param concentrationUnit Concentration unit.
	 * @param totConcCalc set to calc total concentration.
	 * @return mixture container
	 * @throws JAXBException exception 
	 */
	public static MixtureContainer generateMixturesForSolutesFromFiles(URI soluteURI, URI solventURI, Temperature temperature, ConcentrationUnit concentrationUnit, boolean totConcCalc) throws JAXBException{
		SoluteContainer soluteInformation =  parseSoluteXMLFile(soluteURI);
		LOG.debug("soluteInformation entries: {}", soluteInformation.getPhaseSolutes().size());
		SolventContainer solventInformation = parseSolventXMLFile(solventURI);
		if (totConcCalc) {
			TotalConcentrationMatcher.setSolventTotalConcentrations(solventInformation, temperature.convertTo(TemperatureUnit.KELVIN).getTemperatureValue());
		}
		LOG.debug("solventInformation entries: {}", solventInformation.getSolventMap().size());
		return new MixtureContainer(generateMixtureForSolutes(soluteInformation, solventInformation, temperature, concentrationUnit));
	}
	
	/**
	 * This reads a PhaseCollectionList element from a file and calculates concentrations.
	 * 
	 * @param xmlFilename URI
	 * @param totConcCalc  set to calc total concentration.
	 * @return mixture container
	 * @throws JAXBException exception.
	 */
	public static MixtureContainer parsePhaseCollectionListFileAndCalculateConcentrations(URI xmlFilename, boolean totConcCalc) throws JAXBException{
		MixtureContainer mixtureContainer = unmarshalContainer(xmlFilename);
		HashMap<String, Mixture> conccalcMixtureMap = new HashMap<>();
		for (Map.Entry<String, Mixture> mixtureEntry : mixtureContainer.getMixtureMap().entrySet()) {
			if (totConcCalc) {
				TotalConcentrationMatcher.setPhaseTotalConcentrations(mixtureEntry.getValue());
			}
			Mixture mixtureWithConcs = calculateConcentrationsForPhasesInMixture(mixtureEntry.getValue());
			conccalcMixtureMap.put(mixtureEntry.getKey(), mixtureWithConcs);
		}
		mixtureContainer.setMixtureMap(conccalcMixtureMap);
		return mixtureContainer;
	}
	
	/**
	 * This reads a PhaseCollection from a file and calculates concentrations.
	 * 
	 * @param xmlFilename URI.
	 * @param totConcCalc set to calc total concentration.
	 * @return mixture
	 * @throws JAXBException exception
	 */
	public static Mixture parsePhaseCollectionFileAndCalculateConcentrations(URI xmlFilename, boolean totConcCalc) throws JAXBException{
		Mixture readInMixture = unmarshal(xmlFilename);
		if (totConcCalc) {
			TotalConcentrationMatcher.setPhaseTotalConcentrations(readInMixture);
		}
		return calculateConcentrationsForPhasesInMixture(readInMixture);
		
	}
	
	/**
	 * This calculates the Concentrations in a phase system for each phase in the input mixture or mixture container.
	 * 
	 *   If a single mixture is read in 
	 * 
	 * @param xmlFilename URI
	 * @param singleMixture boolean
	 * @return phase system container
	 * @throws JAXBException exception.
	 */
	public static PhaseSystemContainer calculatePhaseSystemsFromMixture(URI xmlFilename, boolean singleMixture) throws JAXBException{
		HashMap<String, PhaseSystem> phaseSystemMap = new HashMap<>();
		if (singleMixture) {
			Mixture readInMixture = unmarshal(xmlFilename);
			phaseSystemMap.putAll(generatePhaseSystemsFromPhases(readInMixture.getPhaseMap()));
		} else {
			MixtureContainer readInMixtureContainer = unmarshalContainer(xmlFilename);
			for (Map.Entry<String, Mixture> mixtureEntry : readInMixtureContainer.getMixtureMap().entrySet()) {
				LOG.info("Mixture entry: {}", mixtureEntry.getKey());
				phaseSystemMap.putAll(generatePhaseSystemsFromPhases(mixtureEntry.getValue().getPhaseMap()));
			}
		}
		return new PhaseSystemContainer(phaseSystemMap);
	}
	
	private static Map<String, PhaseSystem> generatePhaseSystemsFromPhases(Map<String, Phase> phaseMap){
		HashMap<String, PhaseSystem> phaseSystemMap = new HashMap<>();
		for (Map.Entry<String, Phase> phaseMapEntry : phaseMap.entrySet()) {
			LOG.debug("Phase key: {}", phaseMapEntry.getKey());
			LOG.debug("Phase SolventID: {}", phaseMapEntry.getValue().getSolventID());
			PhaseSystemConcentrationCalculator phaseConcentrationCalculator = new PhaseSystemConcentrationCalculator(phaseMapEntry.getValue().convertConcentrationUnitTo(ConcentrationUnit.SSIPCONCENTRATION));
			phaseSystemMap.put(phaseMapEntry.getKey(), phaseConcentrationCalculator.getPhaseSystem(phaseMapEntry.getKey()).convertConcentrationUnitTo(phaseMapEntry.getValue().getConcentrationUnit()));
			if (LOG.isTraceEnabled()) {
				LOG.trace("PhaseSystem gas phase: {}", phaseSystemMap.get(phaseMapEntry.getKey()).getGasPhase().getSolventID());
				LOG.trace("PhaseSystem condensed phase: {}", phaseSystemMap.get(phaseMapEntry.getKey()).getCondensedPhase().getSolventID());
			}
			
		}
		LOG.info("phase system map:");
		LOG.info(phaseSystemMap);
		return phaseSystemMap;
	}
	
	/**
	 * Reads in solute information and phase information and generates phase systems with the solutes contained within.
	 * 
	 * @param phaseFilename URI
	 * @param soluteFilename URI
	 * @return phase system mixture container
	 * @throws JAXBException exception.
	 */
	public static PhaseSystemMixtureContainer calculatePhaseSystemsWithSolutes(URI phaseFilename, URI soluteFilename) throws JAXBException{
		SoluteContainer soluteContainer = parseSoluteXMLFile(soluteFilename);
		Mixture inputPhases = unmarshal(phaseFilename);
		HashMap<String, PhaseSystemMixture> phaseSystemMixtureMap = new HashMap<>();
		for (Map.Entry<String, PhaseSolute> soluteEntry : soluteContainer.getPhaseSolutes().entrySet()) {
			HashMap<String, PhaseSystem> phaseSystemMap = new HashMap<>();
			PhaseSolute phaseSolute = soluteEntry.getValue();
			for (Map.Entry<String, Phase> phaseEntry : inputPhases.getPhaseMap().entrySet()) {
				phaseSystemMap.put(phaseEntry.getKey(), PhaseSystemConcentrationCalculatorWithSolute.generatePhaseSystemWithSolute(phaseEntry.getValue(), phaseSolute, phaseEntry.getKey()));
			}
			phaseSystemMixtureMap.put(phaseSolute.getSoluteID(), new PhaseSystemMixture(phaseSystemMap));
		}
		return new PhaseSystemMixtureContainer(phaseSystemMixtureMap);
	}
	
	/**
	 * This reads a PhaseCollectionList element from a file and reads concentrations.
	 * 
	 * @param xmlFilename URI
	 * @return mixture container
	 * @throws JAXBException exception.
	 */
	public static MixtureContainer parsePhaseCollectionListFileAndReadConcentrations(URI xmlFilename) throws JAXBException{
		return unmarshalContainer(xmlFilename);
	}
	
	/**
	 * This reads a PhaseCollection from a file and read concentrations.
	 * 
	 * @param xmlFilename URI
	 * @return mixture
	 * @throws JAXBException exception 
	 */
	public static Mixture parsePhaseCollectionFileAndReadConcentrations(URI xmlFilename) throws JAXBException{
		return unmarshal(xmlFilename);
	}
	
}
