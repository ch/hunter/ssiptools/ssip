/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import java.io.File;
import java.net.URI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;

import uk.ac.cam.ch.hunter.ssip.core.io.SchemaMaker;
import uk.ac.cam.ch.hunter.ssip.core.io.XmlValidationEventHandler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.PhaseSystemConcentrationCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;


/**
 * Class for reading in information from XML to a Phase object.
 * 
 * @author mdd31
 *
 */
public class PhaseXmlReader {

	private static final Schema phaseSchema = SchemaMaker.createPhaseSchema();
	
	/**
	 * Constructor
	 */
	private PhaseXmlReader(){
		/**
		 * Initialise reader.
		 */
	}
	
	/**
	 * Unmarshal XML in file at given {@link URI} to {@link Phase} object.
	 * 
	 * @param filename URI
	 * @return phase
	 * @throws JAXBException exception
	 */
	public static Phase unmarshal(URI filename) throws JAXBException{
		File file = new File(filename);
		JAXBContext jaxbContext = JAXBContext.newInstance(Phase.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());

		Phase phase = (Phase) jaxbUnmarshaller.unmarshal(file);
		phase.setAssociationConstants();
		phase.setBoundConcentrations();
		return phase;
	}
	
	/**
	 * Read in phase and then calculate speciation in gas and liquid phases.
	 * 
	 * @param filename URI
	 * @param concentrationUnit concentration unit.
	 * @return phase system
	 * @throws JAXBException exception
	 */
	public static PhaseSystem createPhaseSystemFromPhase(URI filename, ConcentrationUnit concentrationUnit) throws JAXBException{
		Phase phase = unmarshal(filename);
		PhaseSystemConcentrationCalculator phaseSystemConcentrationCalculator = new PhaseSystemConcentrationCalculator(phase.convertConcentrationUnitTo(ConcentrationUnit.SSIPCONCENTRATION));
		return phaseSystemConcentrationCalculator.getPhaseSystem(phase.getSolventID()).convertConcentrationUnitTo(concentrationUnit);
	}
}
