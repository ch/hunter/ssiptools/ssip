/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
/**
 * 
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer;


import java.util.List;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Constants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.temperature.InteractionEnergyCalculator;

/**
 * Factory class for creating instance of the AssociationConstants class. This
 * 
 * @author mdd31
 * 
 */
public class AssociationConstantsFactory {

	private static final Logger LOG = LogManager.getLogger(AssociationConstantsFactory.class);
	
	/**
	 * temperature for the association constants- this must be in Kelvin, otherwise the maths is not valid.
	 */
	private double temperature;

	/**
	 * Generates an AssociationConstantsFactory with the given temperature in Kelvin.
	 * 
	 * @param temperature Temperature in Kelvin.
	 */
	public AssociationConstantsFactory(double temperature) {
		this.temperature = temperature;
	}

	/**
	 * This calculates the interaction energy between two SSIPs and includes the temperature dependence on the value, if the energy is less than zero.
	 * 
	 * @param ssip1 SSIP 1
	 * @param ssip2 SSIP 2
	 * @return interaction energy.
	 */
	private double calculateSsipInteractionEnergy(Ssip ssip1, Ssip ssip2){
		double interactionEnergy = ssip1.getValue()* ssip2.getValue();
		if (interactionEnergy < 0.0) {
			interactionEnergy = InteractionEnergyCalculator.calculateInteractionEnergyUsingSpline(interactionEnergy, temperature);
		} else {
			interactionEnergy = 0.0;
		}
		return interactionEnergy;
	}
	
	/**
	 * This calculates the association constant when given the interaction energy, using
	 * equation 5 from this <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * Here the interaction energy is ssip1*ssip2 + EVdW, in kJ/mol.
	 * 
	 * @param interactionEnergy Interaction Energy.
	 * @param temperature Temperature in Kelvin.
	 * @return association constant.
	 */
	private static double calculateAssociationConstant(double interactionEnergy, double temperature){
		double exponent = -(interactionEnergy) / (Constants.GAS_CONSTANT * temperature);
		LOG.trace("Exponent: {}", exponent);
		return 0.5 * Math.exp(exponent);
	}
	
	/**
	 * This calculates the value of the association constant, when there is only Van der Waals interactions between the SSIPs.
	 * i.e. when \epsilon_{i}\epsilon_{j} = 0 in equation 5 from this <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * @param temperature Temperature in Kelvin.
	 * @return VdW association constant
	 */
	public static double calculateKVdW(double temperature){
		return calculateAssociationConstant(Constants.EVDW, temperature);
	}
	
	/**
	 * This calculates the association constant for a pair of SSIPs, using
	 * equation 5 from this <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>
	 * 
	 * @param ssip1 SSIP 1
	 * @param ssip2 SSIP 2
	 * @return associationConstant Association Constant.
	 */
	private double calculateAssociationConstant(Ssip ssip1, Ssip ssip2) {
		//use calculateSsipInteractionEnergy to include temperature dependence.
		double interactionEnergy = calculateSsipInteractionEnergy(ssip1, ssip2) + Constants.EVDW;
		return calculateAssociationConstant(interactionEnergy, temperature);
	}
	
	/**
	 * This generates the matrix for the association constants for all pairwise sets of SSIPs.
	 * 
	 * @param orderedSSIPList SSIP List.
	 * @return association constant matrix.
	 */
	private Array2DRowRealMatrix createAssociationConstantMatrix(List<? extends Ssip> orderedSSIPList){
		Array2DRowRealMatrix associationConstants = new Array2DRowRealMatrix(orderedSSIPList.size(), orderedSSIPList.size());
		//iterate over row
		for (int i = 0; i < orderedSSIPList.size(); i++) {
			//iterate over column, using the fact that matrix is symmetric, so only have to traverse indices where j<=i.
			for (int j = 0; j <=i; j++) {
				//calculate the value of Kij for the SSIPs in the list.
				double valueKij = calculateAssociationConstant(orderedSSIPList.get(i), orderedSSIPList.get(j));
				if (i == j) {
					//set the value on the diagonal only
					associationConstants.setEntry(i, j, valueKij);
				} else {
					//set the value for the i,j and j,i position in the matrix, as they should be equal.
					associationConstants.setEntry(i, j, valueKij);
					associationConstants.setEntry(j, i, valueKij);
				}
			}
			
		}
		return associationConstants;
	}
	
	/**
	 * This generates the AssociationConstants from the given SSIP list, using the temperature for the Factory.
	 * 
	 * @param orderedSSIPList SSIP List.
	 * @return association constants.
	 */
	public AssociationConstants getAssociationConstants(List<? extends Ssip> orderedSSIPList){
		Array2DRowRealMatrix associationConstantMatrix = createAssociationConstantMatrix(orderedSSIPList);
		return new AssociationConstants(associationConstantMatrix);
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
}
