/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters.PhaseMapAdapterForPhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IMixture;

/**
 * Container like class to store information about phase information.
 * 
 * @author mdd31
 *
 */
@XmlRootElement(name = "Mixture", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class PhaseSystem implements IMixture {

	@XmlElement(name = "Phases", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlJavaTypeAdapter(PhaseMapAdapterForPhaseSystem.class)
	private HashMap<String, Phase> phaseMap;
	
	@XmlAttribute(name = "mixtureID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String mixtureID;
	
	
	public PhaseSystem(){
		/**
		 * JAXB Constructor
		 */
	}
	
	/**
	 * Constructor assembles a phase system object from the given gas and condensed phases.
	 * 
	 * @param gasPhase gas phase
	 * @param condensedPhase condensed phase
	 * @param mixtureID mixture ID
	 */
	public PhaseSystem(Phase gasPhase, Phase condensedPhase, String mixtureID){
		this.mixtureID = mixtureID;
		this.phaseMap = new HashMap<>();
		gasPhase.setPhaseType(PhaseType.GAS);
		phaseMap.put(PhaseType.GAS.getPhaseType(), gasPhase);
		condensedPhase.setPhaseType(PhaseType.CONDENSED);
		phaseMap.put(PhaseType.CONDENSED.getPhaseType(), condensedPhase);
	}
	
	@Override
	public void setBoundConcentrations() {
		for (Map.Entry<String, Phase> phaseMapEntry : getPhaseMap().entrySet()) {
			phaseMapEntry.getValue().setBoundConcentrations();
		}
	}

	/**
	 * Set Association Constants upon unmarshalling.
	 */
	@Override
	public void setAssociationConstants(){
		for (Map.Entry<String, Phase> phaseMapEntry : getPhaseMap().entrySet()) {
			phaseMapEntry.getValue().setAssociationConstants();
		}
	}
	
	@Override
	public List<PhaseConcentrationFraction> calculatePhaseConcentrationFractions() {
		ArrayList<PhaseConcentrationFraction> phaseConcentrationFractions = new ArrayList<>();
		for (Phase phase : getPhaseMap().values()) {
			phaseConcentrationFractions.add(phase.calculatePhaseConcentrationFraction());
		}
		return phaseConcentrationFractions;
	}

	public Phase getGasPhase() {
		return getPhaseMap().get(PhaseType.GAS.getPhaseType());
	}

	public void setGasPhase(Phase gasPhase) {
		getPhaseMap().put(PhaseType.GAS.getPhaseType(), gasPhase);
	}

	public Phase getCondensedPhase() {
		return getPhaseMap().get(PhaseType.CONDENSED.getPhaseType());
	}

	public void setCondensedPhase(Phase condensedPhase) {
		getPhaseMap().put(PhaseType.CONDENSED.getPhaseType(), condensedPhase);
	}

	/**
	 * Convert concentrations to new unit. 
	 * 
	 * @param concentrationUnit unit
	 * @return Phase system
	 */
	public PhaseSystem convertConcentrationUnitTo(ConcentrationUnit concentrationUnit){
		Phase convertedGasPhase = getGasPhase().convertConcentrationUnitTo(concentrationUnit);
		Phase convertedCondensedPhase = getCondensedPhase().convertConcentrationUnitTo(concentrationUnit);
		return new PhaseSystem(convertedGasPhase, convertedCondensedPhase, getMixtureID());
	}
	
	@Override
	public Map<String, Phase> getPhaseMap() {
		return phaseMap;
	}

	@Override
	public void setPhaseMap(Map<String, Phase> phaseMap) {
		this.phaseMap = (HashMap<String, Phase>) phaseMap;
	}

	@Override
	public String getMixtureID() {
		return mixtureID;
	}

	@Override
	public void setMixtureID(String mixtureID) {
		this.mixtureID = mixtureID;
	}

	@Override
	public FractionalOccupancyCollection getFractionalOccupancyCollection() {
		FractionalOccupancyCollection fractionalOccupancyCollection = new FractionalOccupancyCollection(new HashSet<>());
		for (Phase phase : getPhaseMap().values()) {
			fractionalOccupancyCollection.addFractionalOccupancy(phase.getFractionalOccupancy(getMixtureID()));
		}
		return fractionalOccupancyCollection;
	}
}
