/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;

/**
 * Utils class containing static methods for matrix operations required but not included in apache commons math.
 * 
 * @author mdd31
 *
 */
public class MatrixUtils {
	
	private MatrixUtils() {

	}
	
	/**
	 * This calculates the sum of the rows and returns the resultant vector.
	 * 
	 * @param matrix Input matrix.
	 * @return row sums
	 */
	public static ArrayRealVector calculateRowSums(Array2DRowRealMatrix matrix) {
		ArrayRealVector rowSums = new ArrayRealVector(matrix.getRowDimension());
		for (int i = 0; i < rowSums.getDimension(); i++) {
			rowSums.setEntry(i, VectorUtils.sumVectorElements((ArrayRealVector) matrix.getRowVector(i)));
		}
		return rowSums;
	}
	
	/**
	 * This calculates the Hadamard product of the two input matrices, and return the resultant product matrix.
	 * 
	 * @param matrix1 Input matrix 1.
	 * @param matrix2 Input matrix 2.
	 * @return Hadamard product of the two input matrices.
	 */
	public static Array2DRowRealMatrix calculateHadamardProduct(Array2DRowRealMatrix matrix1, Array2DRowRealMatrix matrix2) {
		Array2DRowRealMatrix resultsMatrix = new Array2DRowRealMatrix(matrix1.getRowDimension(), matrix1.getColumnDimension());
		for (int i = 0; i < matrix1.getRowDimension(); i++) {
			resultsMatrix.setRowVector(i, matrix1.getRowVector(i).ebeMultiply(matrix2.getRowVector(i)));
		}
		return resultsMatrix;
	}
	
}
