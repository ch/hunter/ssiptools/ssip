/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.commons.math3.linear.ArrayRealVector;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.EnergyContribution;

/**
 * Adapter for converting a {@link ArrayRealVector} of energy contributions to an {@link ArrayList} of {@link EnergyContribution}s.
 * 
 * @author mdd31
 *
 */
public class EnergyContributionsAdapter extends
		XmlAdapter<EnergyContributionsAdapter.AdaptedEnergyContributions, ArrayRealVector> {

	/**
	 * Class for storing the {@link EnergyContribution}s in an {@link ArrayList}
	 * 
	 * @author mdd31
	 *
	 */
	@XmlAccessorType(XmlAccessType.NONE)
	public static class AdaptedEnergyContributions{
		
		/**
		 * The {@link ArrayList} of {@link EnergyContribution} values.
		 */
		@XmlElement(name = "EnergyContribution", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
		public List<EnergyContribution> energyContributions = new ArrayList<>();
	}
	
	@Override
	public AdaptedEnergyContributions marshal(ArrayRealVector doubleMatrix){
		AdaptedEnergyContributions adaptedEnergyContributions = new AdaptedEnergyContributions();
		for (int i = 0; i < doubleMatrix.getDimension(); i++) {
			adaptedEnergyContributions.energyContributions.add(new EnergyContribution(doubleMatrix.getEntry(i), i + 1));
		}
		Collections.sort(adaptedEnergyContributions.energyContributions);
		return adaptedEnergyContributions;
	}
	
	@Override
	public ArrayRealVector unmarshal(AdaptedEnergyContributions adaptedEnergyContributions){
		ArrayRealVector doubleMatrix = new ArrayRealVector(adaptedEnergyContributions.energyContributions.size());
		for (int i = 0; i < adaptedEnergyContributions.energyContributions.size(); i++) {
			doubleMatrix.setEntry(i, adaptedEnergyContributions.energyContributions.get(i).getValue());
		}
		return doubleMatrix;
	}
}
