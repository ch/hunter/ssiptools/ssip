/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;

/**
 * Adapter for converting maps of Phase Molecules to a list, to give consistent write order.
 * 
 * @author mdd31
 *
 */
public class PhaseMoleculeMapAdapter extends XmlAdapter<PhaseMoleculeMapAdapter.AdaptedPhaseMoleculeMap, Map<String, PhaseMolecule>> {

	/**
	 * Adapted molecule map stores the information as an {@link ArrayList} of {@link PhaseMolecule} objects. Ordering is based on moleculeID.
	 * 
	 * @author mdd31
	 *
	 */
	@XmlAccessorType(XmlAccessType.NONE)
	public static class AdaptedPhaseMoleculeMap {
	
		/**
		 * {@link ArrayList} of {@link PhaseMolecule} entries.
		 */
		@XmlElement(name = "Molecule", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
		public List<PhaseMolecule> phaseMolecules = new ArrayList<>();
	}
	
	@Override
	public AdaptedPhaseMoleculeMap marshal(Map<String, PhaseMolecule> phaseMoleculeMap){
		AdaptedPhaseMoleculeMap adaptedPhaseMoleculeMap = new AdaptedPhaseMoleculeMap();
		for (Map.Entry<String, PhaseMolecule> phaseMolEntry : phaseMoleculeMap.entrySet()) {
			adaptedPhaseMoleculeMap.phaseMolecules.add(phaseMolEntry.getValue());
		}
		Collections.sort(adaptedPhaseMoleculeMap.phaseMolecules);
		return adaptedPhaseMoleculeMap;
	}
	
	@Override
	public Map<String, PhaseMolecule> unmarshal(AdaptedPhaseMoleculeMap adaptedPhaseMoleculeMap){
		HashMap<String, PhaseMolecule> phaseMoleculeMap = new HashMap<>();
		for (PhaseMolecule phaseMolecule : adaptedPhaseMoleculeMap.phaseMolecules) {
			phaseMoleculeMap.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		}
		return phaseMoleculeMap;
	}
}
