/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.DefaultNamespacePrefixMapper;
import uk.ac.cam.ch.hunter.ssip.core.io.SchemaMaker;
import uk.ac.cam.ch.hunter.ssip.core.io.XmlValidationEventHandler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolute;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SoluteContainer;

/**
 * Writer class for Solute information. Marshals information using JAXB. 
 * 
 * @author mdd31
 *
 */
public class SoluteXmlWriter {
	
	private static final Logger LOG = LogManager
			.getLogger(SoluteXmlWriter.class);

	private static final Schema phaseSchema = SchemaMaker.createPhaseSchema();
	
	private SoluteXmlWriter(){
		/**
		 * Constructor
		 */
	}
	
	/**
	 * Marshal {@link PhaseSolute} to file using JAXB.
	 * 
	 * @param phaseSolute solute
	 * @param filename URI
	 * @throws JAXBException exception
	 */
	public static void marshal(PhaseSolute phaseSolute, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSolute.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(phaseSolute, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
			throw new JAXBException(e);
		}
	}
	/**
	 * Marshal {@link SoluteContainer} to {@link URI} of file given using JAXB.
	 * 
	 * @param soluteContainer solute container
	 * @param filename URI
	 * @throws JAXBException exception
	 */
	public static void marshalSolutecontainer(SoluteContainer soluteContainer, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(SoluteContainer.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(soluteContainer, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
			throw new JAXBException(e);
		}
	}

}
