/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Class for representing a Solute. This is used to aid in the marshal/demarshal of information using JAXB.
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Solute", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
public class PhaseSolute implements Comparable<PhaseSolute>{
	@XmlElement(name = "Molecule", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private PhaseMolecule phaseMolecule;
	
	@XmlAttribute(name = "soluteID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String soluteID;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;
	
	public PhaseSolute() {
		/**
		 * Constructor for JAXB.
		 */
	}
	/**
	 * Create solute using a PhaseMolecule- this also generate a moleculeID if the molecule does not possess one.
	 * 
	 * @param phaseMolecule molecule
	 */
	public PhaseSolute(PhaseMolecule phaseMolecule){
		
		/**
		 * This logic is used so that the solute molecule can be distinguished from the solvent version of it, upon read in. 
		 */
		String soluteId = generateSoluteIDFromMolecule(phaseMolecule);
		this.setSoluteID(soluteId);
		this.phaseMolecule = phaseMolecule;
	}

	private String generateSoluteIDFromMolecule(PhaseMolecule phaseMolecule){
		String moleculeId;
		if (phaseMolecule.getMoleculeID() == null) {
			moleculeId = phaseMolecule.getStdinchikey() + "solute";
			phaseMolecule.setMoleculeID(moleculeId);
			
		} else {
			moleculeId = phaseMolecule.getMoleculeID();
		}
		return moleculeId;
	}
	
	public PhaseMolecule getPhaseMolecule() {
		return phaseMolecule;
	}

	public void setPhaseMolecule(PhaseMolecule phaseMolecule) {
		String soluteId = generateSoluteIDFromMolecule(phaseMolecule);
		this.phaseMolecule = phaseMolecule;
		// Update soluteID to match the input molecule.
		setSoluteID(soluteId);
		
	}

	public String getSoluteID() {
		return soluteID;
	}

	public void setSoluteID(String soluteID) {
		this.soluteID = soluteID;
		/**
		 * Update phase molecule ID so they are consistent with the solute ID.
		 */
		if (phaseMolecule != null) {
			phaseMolecule.setMoleculeID(soluteID);
		}
	}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhaseSolute other = (PhaseSolute) obj;
		
		return this.getSoluteID().equals(other.getSoluteID()) && this.getPhaseMolecule().equals(other.getPhaseMolecule());
	}
	
	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = super.hashCode();
			long temp;
			temp = phaseMolecule.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = soluteID.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));

			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
	@Override
	public int compareTo(PhaseSolute other){
		if (this.equals(other)){
			return 0;
		}
		else {
			return this.getSoluteID().compareTo(other.getSoluteID());
		}
	}
}
