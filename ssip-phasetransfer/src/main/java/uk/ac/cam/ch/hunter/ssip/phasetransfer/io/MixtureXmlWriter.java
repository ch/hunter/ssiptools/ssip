/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.DefaultNamespacePrefixMapper;
import uk.ac.cam.ch.hunter.ssip.core.io.SchemaMaker;
import uk.ac.cam.ch.hunter.ssip.core.io.XmlValidationEventHandler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.MixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseConcentrationFractionCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixtureContainer;

/**
 * Class provides methods for writing a {@link Mixture} or collection of Mixtures to XML.
 * 
 * @author mdd31
 *
 */
public class MixtureXmlWriter {

	private static final Logger LOG = LogManager
			.getLogger(MixtureXmlWriter.class);
	
	private static final String NAMESPACE_MAPPER = "com.sun.xml.bind.namespacePrefixMapper";
	
	private static final String XML_HEADERS = "com.sun.xml.bind.xmlHeaders"; 
	
	private static final Schema phaseSchema = SchemaMaker.createPhaseSchema();
	
	/**
	 * Constructor
	 */
	private MixtureXmlWriter(){
		/**
		 * Constructor
		 */
	}
	
	/**
	 * Marshal {@link Mixture} to XML using JAXB to file at given {@link URI}.
	 * 
	 * @param mixture Mixture to marshal.
	 * @param filename URI
	 * @throws JAXBException exception.
	 */
	public static void marshal(Mixture mixture, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(Mixture.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty(NAMESPACE_MAPPER, new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty(XML_HEADERS, DefaultNamespacePrefixMapper.getPhaseXmlHeader());
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(mixture, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}
	
	/**
	 * Marshal {@link MixtureContainer} to XML using JAXB to file at given {@link URI}.
	 * 
	 * @param mixtureContainer Conatiner to marshal.
	 * @param filename URI
	 * @throws JAXBException exception
	 */
	public static void marshalContainer(MixtureContainer mixtureContainer, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(MixtureContainer.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty(NAMESPACE_MAPPER, new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty(XML_HEADERS, DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(mixtureContainer, os);
		} catch (FileNotFoundException e) {
			LOG.error(e);
			throw new JAXBException(e);
		}
	}

	/**
	 * Marshal {@link PhaseSystem} to XML using JAXB to file at given {@link URI}.
	 * 
	 * @param phaseSystem Phase system.
	 * @param filename URI
	 * @throws JAXBException exception.
	 */
	public static void marshalPhaseSystem(PhaseSystem phaseSystem, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSystem.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty(NAMESPACE_MAPPER, new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty(XML_HEADERS, DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(phaseSystem, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}
	
	/**
	 * Marshal {@link PhaseSystemContainer} to XML using JAXB to file at given {@link URI}.
	 * 
	 * @param phaseSystemContainer container
	 * @param filename URI
	 * @throws JAXBException exception
	 */
	public static void marshalPhaseSystemContainer(PhaseSystemContainer phaseSystemContainer, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSystemContainer.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty(NAMESPACE_MAPPER, new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty(XML_HEADERS, DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(phaseSystemContainer, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}
	
	/**
	 * Marshals {@link PhaseSystemMixture} to file.
	 * 
	 * @param phaseSystemMixture mixture
	 * @param filename URI.
	 * @throws JAXBException exception.
	 */
	public static void marshalPhaseSystemMixture(PhaseSystemMixture phaseSystemMixture, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSystemMixture.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty(NAMESPACE_MAPPER, new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty(XML_HEADERS, DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(phaseSystemMixture, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}
	
	/**
	 * Marshals {@link PhaseSystemMixtureContainer} to file.
	 * 
	 * @param phaseSystemMixtureContainer container
	 * @param filename URI.
	 * @throws JAXBException exception.
	 */
	public static void marshalPhaseSystemMixtureContainer(PhaseSystemMixtureContainer phaseSystemMixtureContainer, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseSystemMixtureContainer.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty(NAMESPACE_MAPPER, new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty(XML_HEADERS, DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(phaseSystemMixtureContainer, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}
	
	/**
	 * This marshals a {@link PhaseConcentrationFractionCollection} to file.
	 * 
	 * @param phaseConcentrationFractionCollection Collection.
	 * @param filename URI
	 * @throws JAXBException exception.
	 */
	public static void marshalPhaseConcentrationFractionCollection(PhaseConcentrationFractionCollection phaseConcentrationFractionCollection, URI filename) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(PhaseConcentrationFractionCollection.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.setProperty(NAMESPACE_MAPPER, new DefaultNamespacePrefixMapper());
		jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		jaxbMarshaller.setProperty(XML_HEADERS, DefaultNamespacePrefixMapper.getPhaseXmlHeader());

		jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, DefaultNamespacePrefixMapper.getSchemaLocationPhase());
		jaxbMarshaller.setSchema(phaseSchema);
		jaxbMarshaller.setEventHandler(new XmlValidationEventHandler());
		
		try {
			File outputFile = new File(filename);
			OutputStream os = new FileOutputStream(outputFile);
			jaxbMarshaller.marshal(phaseConcentrationFractionCollection, os);

		} catch (FileNotFoundException e) {
			LOG.error(e);
		}
	}
}
