/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstantsFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Constants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IPhase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

/**
 * Class for calculating the total concentration of each SSIP based on the volume fraction of the molecules.
 * 
 * @author mdd31
 *
 */
public class TotalConcentrationCalculator {

	private static final Logger LOG = LogManager.getLogger(TotalConcentrationCalculator.class);
	
	private final ArrayRealVector moleculeVdWVolumes;
	
	private final ArrayRealVector moleFraction;
	
	private final ArrayList<SsipConcentration> ssipArrayList;
	
	private final ArrayRealVector weightedZeroPointConcentrations;
	
	private final ExpansionEnergyCalculator expansionEnergyCalculator;
	
	private final double temperature;
	
	private final double kVdW;
	
	private ConcentrationCalculator concentrationCalculator;
	
	/**
	 * Constructor requires information on the molecule volumes, molecule volume fraction of the phase, the SSIPs and temperature. This is provided via a class that implements the {@link IPhase} interface.
	 * 
	 * @param iPhase phase interface
	 * @param temperature temperature
	 */
	public TotalConcentrationCalculator(IPhase iPhase, double temperature){
		this.moleculeVdWVolumes = iPhase.getVdWVolumes();
		this.moleFraction = iPhase.getMoleFractions();
		this.ssipArrayList = (ArrayList<SsipConcentration>) iPhase.getsortedSsips();
		this.expansionEnergyCalculator = new ExpansionEnergyCalculator(iPhase.getPhaseMolecules());
		ArrayRealVector zeropointConcentrations = calculateZeroPointConcentrations(iPhase.getVdWVolumesByMolecule());
		this.weightedZeroPointConcentrations = ConcentrationVolumeCalculator.calculateZeroPointConcentrationFraction(iPhase.getMoleFractions(), zeropointConcentrations, iPhase.getMoleFractionsByMolecule());
		this.temperature = temperature;
		this.kVdW = AssociationConstantsFactory.calculateKVdW(temperature);
		LOG.debug("phase molefractions: {}", iPhase.getMoleFractions());
		LOG.debug("phase vdW volumes by molecule: {}", iPhase.getVdWVolumesByMolecule());
		LOG.debug("phase molefractions by molecule: {}", iPhase.getMoleFractionsByMolecule());
		ArrayRealVector initialTotalConcentrations = calculateInitialConcentrations(iPhase.getMoleFractions(), iPhase.getVdWVolumesByMolecule(), iPhase.getMoleFractionsByMolecule());
		LOG.debug("Initial Total concentrations: {}", initialTotalConcentrations.toArray());
		AssociationConstantsFactory associationConstantsFactory = new AssociationConstantsFactory(temperature);
		AssociationConstants associationConstants = associationConstantsFactory.getAssociationConstants(iPhase.getsortedSsips());
		this.concentrationCalculator = new ConcentrationCalculator(associationConstants.getAssociationConstants(), initialTotalConcentrations);
		this.concentrationCalculator.calculateFreeAndBoundConcentrations();
	}
	
	/**
	 * Calculates the concentration for the zero point liquid. Values are in the SSIP normalised scale.
	 * See concentration outline for derivation. 
	 * 
	 * @param moleculeVolumes molecule volumes
	 * @return zero point concentrations
	 */
	private ArrayRealVector calculateZeroPointConcentrations(ArrayRealVector moleculeVolumes){
		ArrayRealVector zeroPointConcs = new ArrayRealVector(moleculeVolumes.getDimension());
		for (int i = 0; i < moleculeVolumes.getDimension(); i++) {
			zeroPointConcs.setEntry(i, ConcentrationVolumeCalculator.calculateZeroPointConc(moleculeVolumes.getEntry(i)).convertTo(ConcentrationUnit.SSIPCONCENTRATION).getConcentrationValue());
		}
		return zeroPointConcs;
	}
	
	/**
	 * Calculate the initial total concentrations to use. See concentration outline for derivation. 
	 * 
	 * @param moleculeVolumes molecule volumes
	 * @param volumeFractions volume fractions
	 * @return concentrations
	 */
	private ArrayRealVector calculateInitialConcentrations(ArrayRealVector moleFractionsBySSIP , ArrayRealVector vdWVolumePerMolecule, ArrayRealVector moleFractionByMolecule){
		return ConcentrationVolumeCalculator.calculateInitialConcentrationsSSIPNorm(moleFractionsBySSIP, vdWVolumePerMolecule, moleFractionByMolecule);
	}

	/**
	 * Calculate the expansion energies for each SSIP. See concentration outline for derivation.
	 * 
	 * @return expansion energies
	 */
	private ArrayRealVector calculateExpansionEnergies(){
		return getExpansionEnergyCalculator().getExpansionEnergies(calculatePhiBound());
	}
	
	/**
	 * This calculates the trial concentration which is compared to the current total concentration. See concentration outline for derivation. 
	 * @return concentrations
	 */
	protected ArrayRealVector calculateTrialConcentration(){
		LOG.info("calculating trial concentration.");
		ArrayRealVector expansionEnergies = calculateExpansionEnergies();
		LOG.trace("expansionEnergies: {}", expansionEnergies);
		LOG.trace("zero point concs: {}", getWeightedZeroPointConcentrations());
		LOG.trace("mole fractions: {}", getMoleFraction());
		ArrayRealVector phiBound = calculatePhiBound();
		LOG.trace("phi bound: {}", phiBound);
		double minustwoRT = -2 * Constants.GAS_CONSTANT * temperature;
		ArrayRealVector energyMultiplicationFactor = (ArrayRealVector) phiBound.mapMultiply(-1.0).mapAdd(1.0).mapMultiply(minustwoRT);
		LOG.trace("-2RT(1-phi_b): {}", energyMultiplicationFactor);
		ArrayRealVector energyterm = new ArrayRealVector(getWeightedZeroPointConcentrations().getDimension(), 1.0).ebeMultiply(energyMultiplicationFactor).ebeDivide(expansionEnergies);
		LOG.trace("-2RT(1-phi_b)/energies: {}", energyterm);
		ArrayRealVector rightHandBracket = new ArrayRealVector(getWeightedZeroPointConcentrations().getDimension(), 1.0).add(energyterm);
		LOG.trace("right hand bracket: {}", rightHandBracket);
		ArrayRealVector trialConcentrations = getWeightedZeroPointConcentrations().ebeMultiply(rightHandBracket); 
		LOG.debug("Trial Conc: {}", trialConcentrations);
		return trialConcentrations;
	}
	
	/**
	 * Calculates the probability of an SSIP being free if only van der Waals interactions are present.
	 * See mdd thesis.
	 * 
	 * @return concentration
	 */
	public double calculatePvdWFree(){
		return (Math.sqrt(1.0 + 8.0 * getKVdW() * getTotalConcentration()) - 1.0)/(4.0 * getKVdW() * getTotalConcentration());
	}
	
	/**
	 * Calculates the total population of SSIPs that are bound. See concentration outline for derivation.
	 * 
	 * @return total population
	 */
	public ArrayRealVector calculatePhiBound(){
		double pvdWFree = calculatePvdWFree();
		ArrayRealVector psiFree = getPsiFree();
		LOG.trace("PvdWFree: {}", pvdWFree);
		LOG.trace("PsiFree: {}", psiFree);
		return psiFree.map(new PhiBoundCalculator(pvdWFree));
		
	}

	/**
	 * Calculation checks that the trial concentration calculated and the total concentration used are the same (there is self consistency). See concentration outline for derivation.
	 * 
	 * @return concentrations match
	 */
	public boolean totalConcentrationMatches(){
		return VectorUtils.compareVectorsEqualRelativeTolerance(calculateTrialConcentration(), getConcentrationCalculator().getTotalConcentrations(), Constants.TOLERANCE);
	}
	
	/**
	 * Calculate the total concentrations by an iterative cycle. See concentration outline for derivation.
	 */
	public void calculateTotalConcentrations(){
		boolean converged = totalConcentrationMatches();
		while (!converged) {
			ArrayRealVector newTotalConcentrations =  (ArrayRealVector) calculateTrialConcentration().add(getConcentrationCalculator().getTotalConcentrations()).mapDivide(2.0);
			setConcentrationCalculator(new ConcentrationCalculator(getConcentrationCalculator().getAssociationConstants(), newTotalConcentrations));
			getConcentrationCalculator().calculateFreeAndBoundConcentrations();
			converged = totalConcentrationMatches();
		}
	}
	
	/**
	 * This returns the SSIPs with the new total concentrations set.
	 * 
	 * @return ssipconcentrations
	 */
	public List<SsipConcentration> calculateTotalConcsForSSIPs(){
		if (!totalConcentrationMatches()) {
			calculateTotalConcentrations();
		}
		ArrayList<SsipConcentration> ssipConcentrationsWithTotalConc = new ArrayList<>();
		ArrayRealVector totalConcentrations = getConcentrationCalculator().getTotalConcentrations();
		for (int i = 0; i < getSsipArrayList().size(); i++) {
			ssipConcentrationsWithTotalConc.add(new SsipConcentration(getSsipArrayList().get(i), new Concentration(totalConcentrations.getEntry(i), ConcentrationUnit.SSIPCONCENTRATION), getSsipArrayList().get(i).getMoleculeID()));
			
		}
		ConcentrationMatcher concentrationMatcher = new ConcentrationMatcher(getConcentrationCalculator(), ssipConcentrationsWithTotalConc);
		concentrationMatcher.setConcentrations();
		return concentrationMatcher.getSsipConcentrationList();
	}
	
	/**
	 * This returns the SSIPs grouped by corresponding molecule.
	 * 
	 * @return ssipconcentrations
	 */
	public Map<String, ArrayList<SsipConcentration>> calculateTotalConcsByMolecule(){
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) calculateTotalConcsForSSIPs();
		HashMap<String, ArrayList<SsipConcentration>> ssipConcsByMolId = new HashMap<>();
		for (SsipConcentration ssipConcentration : ssipConcentrations) {
			if (!ssipConcsByMolId.keySet().contains(ssipConcentration.getMoleculeID())) {
				ArrayList<SsipConcentration> molSsipConcentrations = new ArrayList<>();
				molSsipConcentrations.add(ssipConcentration);
				ssipConcsByMolId.put(ssipConcentration.getMoleculeID(), molSsipConcentrations);
			} else {
				ssipConcsByMolId.get(ssipConcentration.getMoleculeID()).add(ssipConcentration);
			}
		}
		return ssipConcsByMolId;
	}

	public double getTotalConcentration(){
		return this.concentrationCalculator.getTotalConcentration();
	}
	
	public ArrayRealVector getPsiFree(){
		return this.concentrationCalculator.getPsiFreePerMolecule(getMoleculeIDs());
	}
	
	public List<String> getMoleculeIDs(){
		ArrayList<String> moleculeIDs = new ArrayList<>();
		for (int i = 0; i < ssipArrayList.size(); i++) {
			moleculeIDs.add(ssipArrayList.get(i).getMoleculeID());
		}
		return moleculeIDs;
	}
	
	public ArrayRealVector getMoleculeVdWVolumes() {
		return moleculeVdWVolumes;
	}

	public ArrayRealVector getMoleFraction() {
		return moleFraction;
	}

	public ArrayRealVector getWeightedZeroPointConcentrations() {
		return weightedZeroPointConcentrations;
	}

	public double getTemperature() {
		return temperature;
	}

	public double getKVdW() {
		return kVdW;
	}

	public List<SsipConcentration> getSsipArrayList() {
		return ssipArrayList;
	}

	public ExpansionEnergyCalculator getExpansionEnergyCalculator() {
		return expansionEnergyCalculator;
	}

	public ConcentrationCalculator getConcentrationCalculator() {
		return concentrationCalculator;
	}

	public void setConcentrationCalculator(
			ConcentrationCalculator concentrationCalculator) {
		this.concentrationCalculator = concentrationCalculator;
	}
}
