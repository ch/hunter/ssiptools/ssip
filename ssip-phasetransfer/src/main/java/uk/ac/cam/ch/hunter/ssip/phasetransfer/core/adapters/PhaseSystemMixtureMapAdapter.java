/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.comparators.PhaseSystemMixtureCompartor;

/**
 * {@link XmlAdapter} for a {@link Map} of {@link PhaseSystemMixture} values, where the key is the SoluteID. 
 * 
 * @author mdd31
 *
 */
public class PhaseSystemMixtureMapAdapter extends XmlAdapter<PhaseSystemMixtureMapAdapter.AdaptedPhaseSystemMixtureMap, Map<String, PhaseSystemMixture>> {

	/**
	 * {@link AdaptedPhaseSystemMixtureMap} contains {@link ArrayList} of {@link Mixture} objects. Order is based on SoluteID.
	 * 
	 * @author mdd31
	 *
	 */
	@XmlAccessorType(XmlAccessType.NONE)
	public static class AdaptedPhaseSystemMixtureMap {
		/**
		 * {@link ArrayList} of {@link Mixture} objects, ordered by SoluteID.
		 */
		@XmlElement(name = "PhaseSystemMixture", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
		public List<PhaseSystemMixture> mixtures = new ArrayList<>();
	}
	
	@Override
	public AdaptedPhaseSystemMixtureMap marshal(Map<String, PhaseSystemMixture> mixtureMap) {
		AdaptedPhaseSystemMixtureMap adaptedMixtureMap = new AdaptedPhaseSystemMixtureMap();
		for (Map.Entry<String, PhaseSystemMixture> mixtureMapEntry : mixtureMap.entrySet()) {
			mixtureMapEntry.getValue().setSoluteID(mixtureMapEntry.getKey());
			adaptedMixtureMap.mixtures.add(mixtureMapEntry.getValue());
		}
		Collections.sort(adaptedMixtureMap.mixtures, new PhaseSystemMixtureCompartor());
		return adaptedMixtureMap;
	}
	
	@Override
	public Map<String, PhaseSystemMixture> unmarshal(AdaptedPhaseSystemMixtureMap adaptedMixtureMap){
		HashMap<String, PhaseSystemMixture> mixtureMap = new HashMap<>();
		for (PhaseSystemMixture mixture : adaptedMixtureMap.mixtures) {
			mixtureMap.put(mixture.getSoluteID(), mixture);
		}
		return mixtureMap;
	}
}
