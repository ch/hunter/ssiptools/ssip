/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.MepPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;

/**
 * Class for storing the information about the concentrations related to a
 * single SSIP.
 * 
 * @author mdd31
 * 
 */
@XmlRootElement(name = "SSIP", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"totalConcentration", "freeConcentration", "boundConcentration", "boundConcentrations"})
public class SsipConcentration extends Ssip {

	@XmlTransient
	private static final Logger LOG = LogManager
			.getLogger(SsipConcentration.class);
	@XmlTransient
	public static final double ERROR_IN_CONCENTRATION = 1e-6;

	@XmlAttribute(name = "moleculeID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String moleculeID;
	
	@XmlElement(name = "TotalConcentration", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final Concentration totalConcentration;
	@XmlElement(name = "FreeConcentration", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private Concentration freeConcentration;
	@XmlElement(name = "BoundConcentrationSum", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private Concentration boundConcentration;

	
	/**
	 * the boundConcentrationList lists the concentration of the current SSIP
	 * bound to each other {@link SsipConcentration} in the
	 * {@link SSIPConcentrationList} in the same order.
	 * 
	 */
	@XmlTransient
	private ArrayRealVector boundConcentrationMatrix;
	
	/**
	 * This is an array list of the bound concentrations, This provides a route for easy marshalling with JAXB.
	 */
	@XmlElement(name = "BoundConcentration", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ArrayList<BoundConcentration> boundConcentrations;

	@XmlAttribute(name ="units", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ConcentrationUnit concentrationUnit;
	
	/**
	 * null constructor for JAXB.
	 */
	public SsipConcentration(){
		this.totalConcentration = null;
		this.moleculeID = null;
	}
	/**
	 * Constructor creates a new SSIPConcentration object using an MEPPoint, the
	 * totalConcentration and the moleculeID of the corresponding molecule.
	 * 
	 * @param mep MEP
	 * @param totalConcentration total concentration
	 * @param concentrationUnit concentration unit
	 * @param moleculeID molecule ID
	 */
	public SsipConcentration(MepPoint mep, double totalConcentration, ConcentrationUnit concentrationUnit,
			String moleculeID) {
		super(Ssip.fromMepPoint(mep));
		this.moleculeID = moleculeID;
		this.totalConcentration = new Concentration(totalConcentration, concentrationUnit);
		this.concentrationUnit = concentrationUnit;
		this.freeConcentration = null;
		this.boundConcentration = null;
		this.boundConcentrationMatrix = null;
		this.boundConcentrations = null;
	}
	
	/**
	 * Constructor creates a new SSIPConcentration object using MEPPoint, Concentration and moleculeID.
	 * 
	 * @param mep MEP
	 * @param totalConcentration total concentration
	 * @param moleculeID molecule ID
	 */
	public SsipConcentration(MepPoint mep, Concentration totalConcentration,
			String moleculeID) {
		super(Ssip.fromMepPoint(mep));
		this.moleculeID = moleculeID;
		this.totalConcentration = totalConcentration;
		this.concentrationUnit = totalConcentration.getConcentrationUnit();
		this.freeConcentration = null;
		this.boundConcentration = null;
		this.boundConcentrationMatrix = null;
		this.boundConcentrations = null;
	}

	/**
	 * Constructor creates a new SSIPConcentration object using an SSIP, the
	 * totalConcentration and the moleculeID of the corresponding molecule.
	 * 
	 * @param ssip SSIP
	 * @param totalConcentration total concentration
	 * @param concentrationUnit unit
	 * @param moleculeID molecule ID
	 */
	public SsipConcentration(Ssip ssip, double totalConcentration, ConcentrationUnit concentrationUnit,
			String moleculeID) {
		super(ssip);
		this.moleculeID = moleculeID;
		this.totalConcentration = new Concentration(totalConcentration, concentrationUnit);
		this.concentrationUnit = concentrationUnit;
		this.freeConcentration = null;
		this.boundConcentration = null;
		this.boundConcentrationMatrix = null;
		this.boundConcentrations = null;
	}
	
	/**
	 * Constructor creates a new SSIPConcentration using an SSIP, and also the Concentration and moleculeID.
	 * 
	 * @param ssip SSIP
	 * @param totalConcentration total concentration
	 * @param moleculeID molecule ID
	 */
	public SsipConcentration(Ssip ssip, Concentration totalConcentration, String moleculeID){
		super(ssip);
		this.moleculeID = moleculeID;
		this.totalConcentration = totalConcentration;
		this.concentrationUnit = totalConcentration.getConcentrationUnit();
		this.freeConcentration = null;
		this.boundConcentration = null;
		this.boundConcentrationMatrix = null;
		this.boundConcentrations = null;
	}

	/**
	 * Creates a new SSIPConcnetration using information from an existing SSIPConcentration instance but with a new moleculeID.
	 * @param ssipConcentration SSIP Concentration
	 * @param moleculeID molecule ID
	 */
	public SsipConcentration(SsipConcentration ssipConcentration, String moleculeID){
		super(ssipConcentration);
		this.moleculeID = moleculeID;
		this.totalConcentration =  new Concentration(ssipConcentration.getTotalConcentration(), ssipConcentration.getConcentrationUnit());
		this.concentrationUnit = ssipConcentration.getConcentrationUnit();
		this.freeConcentration = null;
		this.boundConcentration = null;
		this.boundConcentrationMatrix = null;
		this.boundConcentrations = null;
	}
	
	/**
	 * Method returns a new SSIPConcentration in the unit given.
	 * @param concentrationUnit unit
	 * @return SSIP concentration
	 */
	public SsipConcentration convertConcentrationsTo(ConcentrationUnit concentrationUnit){
		LOG.debug("starting conversion of SSIPConcentration");
		LOG.trace("converting from: {} to: {}", getConcentrationUnit(), concentrationUnit);
		double conversionFactor = getConcentrationUnit().getConversionFactorTo(concentrationUnit);
		double convertedTotalConcentration = getTotalConcentration() * conversionFactor;
		LOG.trace("Creating new SSIPConcentration object");
		SsipConcentration convertedSSIPConcentration = new SsipConcentration(this, convertedTotalConcentration, concentrationUnit, getMoleculeID());
		LOG.trace("checking free concentration: {}", this.freeConcentration);
		if (this.freeConcentration != null) {
			convertedSSIPConcentration.setFreeConcentration(conversionFactor * getFreeConcentration());
		}
		LOG.trace("checking bound concentration: {}", this.boundConcentration);
		if (this.boundConcentration != null) {
			convertedSSIPConcentration.setBoundConcentration(conversionFactor * getBoundConcentration());
		}
		LOG.trace("checking bound concentration list: {}", getBoundConcentrationList());
		if (getBoundConcentrationList() != null) {
			convertedSSIPConcentration.setBoundConcentrationList((ArrayRealVector) getBoundConcentrationList().mapMultiply(conversionFactor));
		}
		LOG.debug("converted SSIP: {}", convertedSSIPConcentration);
		return convertedSSIPConcentration;
	}
	
	/**
	 * Returns the solvation constant, as from eqn 18 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * This is the bound over free concentration.
	 * 
	 * @return solvation constant
	 */
	public double calculateSolvationConstant() {
		return getBoundConcentration() / getFreeConcentration();
	}

	/**
	 * Returns the fraction that is free, [ifree]/[i] which is equivalent to
	 * 1/(1 + Solvation constant).
	 * 
	 * This is as stated in eqn 16 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * @return fraction free
	 */
	public double calculateFractionFree() {
		return getFreeConcentration()/getTotalConcentration();
	}

	/**
	 * This returns ln(FractionFree). Fraction free is defined in: eqn 16 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>
	 * 
	 * @return ln(fraction free)
	 */
	public double calculateLogFractionFree(){
		return Math.log(calculateFractionFree());
	}
	
	public double getTotalConcentration() {
		return totalConcentration.getConcentrationValue();
	}

	public String getMoleculeID() {
		return moleculeID;
	}

	public double getFreeConcentration() {
		if (this.freeConcentration != null) {
			return freeConcentration.getConcentrationValue();
		} else {
			return Double.NaN;
		}
	}
	
	public void setFreeConcentration(double freeConcentration) {
		this.freeConcentration = new Concentration(freeConcentration, getConcentrationUnit());
	}

	/**
	 * Returns the bound concentrations to the other SSIPs selected.
	 * 
	 * @param ssipIndices ssip indices
	 * @return array vector of concentrations
	 */
	public ArrayRealVector getBoundConcentrationsToOtherSSIPs(List<Integer> ssipIndices) {
		ArrayRealVector boundConcentrationsSelection = new ArrayRealVector();
		for (Integer integer : ssipIndices) {
			LOG.trace("bound concentration: {}", getBoundConcentrationList().getEntry(integer));
			boundConcentrationsSelection = new ArrayRealVector(boundConcentrationsSelection, new double[] {getBoundConcentrationList().getEntry(integer)});
			LOG.trace("Bound concs: {}", boundConcentrationsSelection);
		}
		LOG.trace("bound concs for mol: {}", getBoundConcentrationList());
		LOG.trace("bound concs: {}", boundConcentrationsSelection);
		return boundConcentrationsSelection;
	}
	
	/**
	 * This gets the amount of the SSIP which is bound to the subset of SSIPs given. 
	 * 
	 * @param ssipIndices ssip Indices
	 * @return concentration bound to selected SSIPs.
	 */
	public Concentration getBoundConcentrationToOtherSSIPs(List<Integer> ssipIndices) {
		double boundConcentrationTotal = 0.0;
		for (Integer index : ssipIndices) {
			boundConcentrationTotal += getBoundConcentrationList().getEntry(index);
		}
		return new Concentration(boundConcentrationTotal, concentrationUnit);
	}
	
	public double getBoundConcentration() {
		if (this.boundConcentration != null) {
			return boundConcentration.getConcentrationValue();
		} else {
			return Double.NaN;
		}
		
	}

	public void setBoundConcentration(double boundConcentration) {
		this.boundConcentration = new Concentration(boundConcentration, concentrationUnit);
	}

	public ArrayRealVector getBoundConcentrationList() {
		return boundConcentrationMatrix;
	}

	private List<BoundConcentration> createBoundConcentrationsFromMatrix(ArrayRealVector boundConcentrationMatrix){
		ArrayList<BoundConcentration> boundConcentrationList = new ArrayList<>();
		for (int i = 0; i < boundConcentrationMatrix.getDimension(); i++) {
			boundConcentrationList.add(new BoundConcentration(boundConcentrationMatrix.getEntry(i), getConcentrationUnit(), i + 1));
		}
		return boundConcentrationList;
	}
	
	public void setBoundConcentrationList(ArrayRealVector boundConcentrationMatrix) {
		this.boundConcentrationMatrix = boundConcentrationMatrix;
		this.boundConcentrations = (ArrayList<BoundConcentration>) createBoundConcentrationsFromMatrix(boundConcentrationMatrix);
	}


	
	public List<BoundConcentration> getBoundConcentrations() {
		return boundConcentrations;
	}

	private ArrayRealVector createBoundConcentrationMatrixFromList(List<BoundConcentration> boundConcentrations){
		ArrayRealVector boundConcentrationsMatrix = new ArrayRealVector(boundConcentrations.size());
		for (int i = 0; i < boundConcentrations.size(); i++) {
			boundConcentrationsMatrix.setEntry(boundConcentrations.get(i).getSsipRef() -1, boundConcentrations.get(i).getConcentrationValue());
		}
		return boundConcentrationsMatrix;
	}
	
	public void setBoundConcentrations(List<BoundConcentration> boundConcentrations) {
		this.boundConcentrations = (ArrayList<BoundConcentration>) boundConcentrations;
		this.boundConcentrationMatrix = createBoundConcentrationMatrixFromList(boundConcentrations);
	}
	
	@Override
	public String toString(){
		return getMoleculeID() + " " +super.toString() + " concentration " + getTotalConcentration();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (super.equals(obj)) {
			SsipConcentration other = (SsipConcentration) obj;
			return this.moleculeID.equals(other.getMoleculeID())
					&& (Math.abs(this.getTotalConcentration()
							- other.getTotalConcentration()) < ERROR_IN_CONCENTRATION) && this.concentrationUnit.equals(other.concentrationUnit);
		}
		return false;

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = result + getMoleculeID().hashCode();
		return result;
	}

	public ConcentrationUnit getConcentrationUnit() {
		return concentrationUnit;
	}

}
