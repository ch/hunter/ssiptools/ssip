/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.math3.linear.ArrayRealVector;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters.PhaseMoleculeMapAdapter;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.comparators.SSIPConcentrationComparator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IPhase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

/**
 * Class for representing a solvent. This is used to aid in the marshal/demarshal of information using JAXB.
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Solvent", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
public class PhaseSolvent implements Comparable<PhaseSolvent>, IPhase{

	@XmlElement(name = "Molecules", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlJavaTypeAdapter(PhaseMoleculeMapAdapter.class)
	private final HashMap<String, PhaseMolecule> solventMolecules;

	@XmlAttribute(name = "solventID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String solventId;
	@XmlAttribute(name = "solventName", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String solventName;
	@XmlAttribute(name = "units", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final ConcentrationUnit concentrationUnit;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;
	
	/**
	 * Constructor for JAXB.
	 */
	public PhaseSolvent(){
		solventMolecules = null;
		solventId = null;
		solventName = null;
		concentrationUnit = null;
	}
	
	/**
	 * Create a Phase Solvent object.
	 * 
	 * @param solventMolecules solvent molecules
	 * @param solventId ID
	 * @param solventName name
	 * @param concentrationUnit unit
	 */
	public PhaseSolvent(Map<String, PhaseMolecule> solventMolecules, String solventId, String solventName, ConcentrationUnit concentrationUnit){
		this.solventMolecules = (HashMap<String, PhaseMolecule>) solventMolecules;
		this.solventId = solventId;
		this.solventName = solventName;
		this.concentrationUnit = concentrationUnit;
	}
	
	@Override
	public Map<String, PhaseMolecule> getPhaseMolecules() {
		return solventMolecules;
	}
	public String getSolventId() {
		return solventId;
	}
	public String getSolventName() {
		return solventName;
	}
	
	@Override
	public ConcentrationUnit getConcentrationUnit(){
		return concentrationUnit;
	}
	
	private List<String> getMoleculeIDForSSIPs(){
		ArrayList<String> molIDs = new ArrayList<>(getPhaseMolecules().keySet());
		Collections.sort(molIDs);
		return molIDs;
	}
	
	/**
	 * Gets the volume fractions for each SSIP.
	 * 
	 * @return mole fractions
	 */
	@Override
	public ArrayRealVector getMoleFractions(){
		ArrayRealVector moleFractions = new ArrayRealVector();
		for (String molID : getMoleculeIDForSSIPs()) {
			if (moleFractions.getDimension() == 0) {
				moleFractions = (ArrayRealVector) getPhaseMolecules().get(molID).getMoleFractions();
			} else {
				moleFractions = (ArrayRealVector) moleFractions.append(getPhaseMolecules().get(molID).getMoleFractions());
			}
			
		}
		return moleFractions;
	}
	
	@Override
	public Map<String, Double> getMoleFractionsByMoleculeID() {
		HashMap<String, Double> moleFractionsByMoleculeID = new HashMap<>();
		for (String moleculeID : getMoleculeIDForSSIPs()) {
			moleFractionsByMoleculeID.put(moleculeID, getPhaseMolecules().get(moleculeID).getMoleFraction());
		}
		return moleFractionsByMoleculeID;
	}
	
	/**
	 * This returns the molefractions by molecule in the solvent.
	 * 
	 * @return molefractions
	 */
	@Override
	public ArrayRealVector getMoleFractionsByMolecule(){
		ArrayRealVector moleFractionsByMolecule = new ArrayRealVector(getMoleculeIDForSSIPs().size());
		for (int i = 0; i < getMoleculeIDForSSIPs().size(); i++) {
			String molID = getMoleculeIDForSSIPs().get(i);
			moleFractionsByMolecule.setEntry(i, getPhaseMolecules().get(molID).getMoleFraction());
		}
		return moleFractionsByMolecule;
	}
	
	/**
	 * returns the associated molecule volume for each SSIP.
	 * 
	 * @return volumes
	 */
	@Override
	public ArrayRealVector getVdWVolumes(){
		ArrayRealVector volumes = new ArrayRealVector();
		for (String molId : getMoleculeIDForSSIPs()) {
			if (volumes.getDimension() == 0) {
				volumes = (ArrayRealVector) getPhaseMolecules().get(molId).getVdWVolumes();
			} else {
				volumes = (ArrayRealVector) volumes.append(getPhaseMolecules().get(molId).getVdWVolumes());
			}
		}
		return volumes;
	}
	
	@Override
	public ArrayRealVector getVdWVolumesByMolecule(){
		ArrayRealVector vdWVolumesByMolecule = new ArrayRealVector(getMoleculeIDForSSIPs().size());
		for (int i = 0; i < getMoleculeIDForSSIPs().size(); i++) {
			String molID = getMoleculeIDForSSIPs().get(i);
			vdWVolumesByMolecule.setEntry(i, getPhaseMolecules().get(molID).getSsipSurfaceInformation().getVdWVolume());
		}
		return vdWVolumesByMolecule;
	}
	
	/**
	 * This returns the sorted SSIPs in the solvent.
	 * 
	 * @return SSIPs
	 */
	@Override
	public List<SsipConcentration> getsortedSsips(){
		ArrayList<SsipConcentration> sortedSSIPConcentrations = new ArrayList<>();
		HashMap<String, Point3D> centroidMap = new HashMap<>();
		for (PhaseMolecule phaseMolecule : getPhaseMolecules().values()) {
			ArrayList<SsipConcentration> ssipConcentrationValues = (ArrayList<SsipConcentration>) phaseMolecule.getSSIPConcentrations();
			sortedSSIPConcentrations.addAll(ssipConcentrationValues);
			centroidMap.put(phaseMolecule.getMoleculeID(), phaseMolecule.getMoleculeCentroid());
		}
		Comparator<SsipConcentration> comparator = new SSIPConcentrationComparator(centroidMap);
		Collections.sort(sortedSSIPConcentrations, comparator);
		return sortedSSIPConcentrations;
	}
	
	@Override
	public PhaseSolvent convertConcentrationUnitTo(ConcentrationUnit concentrationUnit){
		HashMap<String, PhaseMolecule> convertedSSIPConcentrationListsByMoleculeID = new HashMap<>();
		for (Map.Entry<String, PhaseMolecule> mapEntry : getPhaseMolecules().entrySet()) {
			convertedSSIPConcentrationListsByMoleculeID.put(mapEntry.getKey(), mapEntry.getValue().convertConcentrationsTo(concentrationUnit));
		}
		return new PhaseSolvent(convertedSSIPConcentrationListsByMoleculeID, getSolventId(), getSolventName(), getConcentrationUnit());
	}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhaseSolvent other = (PhaseSolvent) obj;
		return this.getSolventId().equals(other.getSolventId()) && this.getSolventName().equals(other.getSolventName());
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = super.hashCode();
			long temp;
			temp = solventMolecules.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = solventId.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = solventName.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = concentrationUnit.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));



			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
	@Override
	public int compareTo(PhaseSolvent other){
		if (this.equals(other)) {
			return 0;
		} else {
			return this.getSolventId().compareTo(other.getSolventId());
		}
	}

	@Override
	public FractionalOccupancy getFractionalOccupancy(String soluteID) {
		return new FractionalOccupancy(calculateFractionalOccupancy(),getSolventId(), soluteID, Double.NaN);
	}

	private double calculateFractionalOccupancy() {
		ArrayList<SsipConcentration> sortedSsipConcentrationList = (ArrayList<SsipConcentration>) getsortedSSIPConcentrations();
		ArrayRealVector totalConcentrations = new ArrayRealVector(
				sortedSsipConcentrationList.size());
		for (int i = 0; i < sortedSsipConcentrationList.size(); i++) {
			totalConcentrations.setEntry(i, sortedSsipConcentrationList.get(i)
					.getTotalConcentration());
		}
		return VectorUtils.sumVectorElements(totalConcentrations) * getConcentrationUnit().getConversionFactorTo(ConcentrationUnit.SSIPCONCENTRATION);
	}

	private List<SsipConcentration> getsortedSSIPConcentrations() {
		ArrayList<SsipConcentration> sortedSSIPConcentrations = new ArrayList<>();
		HashSet<PhaseMolecule> phaseMolecules = (HashSet<PhaseMolecule>) getPhaseMolecules().values();
		HashMap<String, Point3D> centroidMap = new HashMap<>();
		for (PhaseMolecule phaseMolecule : phaseMolecules) {
			ArrayList<SsipConcentration> ssipConcentrationValues = (ArrayList<SsipConcentration>) phaseMolecule.getSSIPConcentrations();
			sortedSSIPConcentrations.addAll(ssipConcentrationValues);
			centroidMap.put(phaseMolecule.getMoleculeID(), phaseMolecule.getMoleculeCentroid());
		}
		
		Comparator<SsipConcentration> comparator = new SSIPConcentrationComparator(centroidMap);
		Collections.sort(sortedSSIPConcentrations, comparator);
		return sortedSSIPConcentrations;
	}
}