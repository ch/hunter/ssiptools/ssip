/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Constants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.temperature.InteractionEnergyCalculator;

/**
 * Class for calculating the Expansion energy for a collection of SSIPs that represent a solvent.
 * 
 * @author mdd31
 *
 */
public class ExpansionEnergyCalculator {
	
	private static final Logger LOG = LogManager.getLogger(ExpansionEnergyCalculator.class);
	
	private static final double TOLERANCE = 1e-7;
	
	private static final String POSITIVE_STRING = "positive";
	private static final String NEGATIVE_STRING = "negative";
	
	private final HashMap<String, PhaseMolecule> phaseMoleculeMap;
	
	private final ArrayList<SsipConcentration> ssipConcentrations;
	
	private final HashMap<String, Double> moleFractionByMoleculeID;
	
	private final ArrayRealVector vdWExpEnergies;
	
	private final ArrayRealVector interactionExpansionEnergies;
	
	/**
	 * Constructor requires the phase Molecules in a phase.
	 * 
	 * @param phaseMoleculeMap molecule map
	 */
	public ExpansionEnergyCalculator(Map<String, PhaseMolecule> phaseMoleculeMap){
		this.phaseMoleculeMap = (HashMap<String, PhaseMolecule>) phaseMoleculeMap;
		this.ssipConcentrations = (ArrayList<SsipConcentration>) getSortedSSIPs();
		LOG.trace("SSIP concentrations: {}", ssipConcentrations);
		this.moleFractionByMoleculeID = (HashMap<String, Double>) getMoleFractions();
		LOG.trace("mole fractions by molecule: {}", moleFractionByMoleculeID);
		LOG.trace("number SSIPs by molecule: {}", getNumberOfSSIPsbyMolecule());
		this.vdWExpEnergies = calculateVdWEnergies((HashMap<String, Integer>) getNumberOfSSIPsbyMolecule());
		
		this.interactionExpansionEnergies = calculateInteractionExpansionEnergies();
	}

	private HashMap<String, ArrayList<SsipConcentration>> getSSIPsByMolecule(){
		HashMap<String, ArrayList<SsipConcentration>> ssipByMoleculeMap = new HashMap<>();
		for (Map.Entry<String, PhaseMolecule> phaseMoleculeEntry : getPhaseMoleculeMap().entrySet()) {
			ssipByMoleculeMap.put(phaseMoleculeEntry.getKey(), (ArrayList<SsipConcentration>) phaseMoleculeEntry.getValue().getSSIPConcentrations());
		}
		return ssipByMoleculeMap;
	}
	
	private Map<String, Double> getMoleFractions(){
		HashMap<String, Double> moleFractions = new HashMap<>();
		for (Map.Entry<String, PhaseMolecule> phaseMoleculeEntry : getPhaseMoleculeMap().entrySet()) {
			moleFractions.put(phaseMoleculeEntry.getKey(), phaseMoleculeEntry.getValue().getMoleFraction());
		}
		return moleFractions;
	}
	
	private List<SsipConcentration> getSortedSSIPs(){
		HashMap<String, Point3D> centroidMap = (HashMap<String, Point3D>) calculateCentroidMap();
		uk.ac.cam.ch.hunter.ssip.phasetransfer.core.comparators.SSIPConcentrationComparator ssipComparator = new uk.ac.cam.ch.hunter.ssip.phasetransfer.core.comparators.SSIPConcentrationComparator(centroidMap);
		ArrayList<SsipConcentration> ssipConcentrationList = new ArrayList<>();
		LOG.debug("SSIPs by molecule: {}", getSSIPsByMolecule());
		for (ArrayList<SsipConcentration> ssipConcList : getSSIPsByMolecule().values()) {
			ssipConcentrationList.addAll(ssipConcList);
		}
		Collections.sort(ssipConcentrationList, ssipComparator);
		LOG.trace("Total SSIPs: {}", ssipConcentrationList);
		return ssipConcentrationList;
	}
	
	private Map<String, Integer> getNumberOfSSIPsbyMolecule(){
		HashMap<String, Integer> numberSsipsByMolecule = new HashMap<>();
		for (Map.Entry<String, ArrayList<SsipConcentration>> moleculeEntry : getSSIPsByMolecule().entrySet()) {
			numberSsipsByMolecule.put(moleculeEntry.getKey(), moleculeEntry.getValue().size());
		}
		return numberSsipsByMolecule;
	}
	
	private double calculateVdWEnergy(int nVdW){
		return -Constants.EVDW * ((double) nVdW ) / (4.0*(1.0 + ((double) nVdW / 12.0)));
	}
	
	private ArrayRealVector calculateVdWEnergies(HashMap<String, Integer> numberSsipsByMolecule){
		ArrayRealVector vdWEnergies = new ArrayRealVector(getSsipConcentrations().size());
		for (int i = 0; i < vdWEnergies.getDimension(); i++) {
			vdWEnergies.setEntry(i, calculateVdWEnergy(numberSsipsByMolecule.get(getSsipConcentrations().get(i).getMoleculeID())));
		}
		return vdWEnergies;
	}
	
	private Map<String, ArrayList<SsipConcentration>> partitionSSIPs(List<SsipConcentration> ssipConcentrations, Map<String, Point3D> centroidMap){
		HashMap<String, ArrayList<SsipConcentration>> ssipConcentrationsMap = new HashMap<>();
		ArrayList<SsipConcentration> positiveSsipConcentrations = new ArrayList<>();
		ArrayList<SsipConcentration> negativeSsipConcentrations = new ArrayList<>();
		for (SsipConcentration ssipConcentration : ssipConcentrations) {
			if (0.0 < ssipConcentration.getValue()) {
				positiveSsipConcentrations.add(ssipConcentration);
			} else if (0.0 > ssipConcentration.getValue()) {
				negativeSsipConcentrations.add(ssipConcentration);
			}
		}
		SsipConcentrationComparator ssipConcentrationComparator = new SsipConcentrationComparator(centroidMap);
		Collections.sort(positiveSsipConcentrations, ssipConcentrationComparator);
		Collections.sort(negativeSsipConcentrations, ssipConcentrationComparator);
		ssipConcentrationsMap.put(POSITIVE_STRING, positiveSsipConcentrations);
		ssipConcentrationsMap.put(NEGATIVE_STRING, negativeSsipConcentrations);
		return ssipConcentrationsMap;
	}
	
	private Map<String, Point3D> calculateCentroidMap(){
		HashMap<String, Point3D> centroidMap = new HashMap<>();
		for (Map.Entry<String, PhaseMolecule> phaseMoleculeEntry : getPhaseMoleculeMap().entrySet()) {
			centroidMap.put(phaseMoleculeEntry.getKey(), phaseMoleculeEntry.getValue().getMoleculeCentroid());
		}
		return centroidMap;
	}
	
	private Map<String, Double> calculateInteractionExpansionEnergy(){
		HashMap<String, Double> interactionEnergyByMoleculeID = new HashMap<>();
		HashMap<String, ArrayList<SsipConcentration>> ssipsByType = (HashMap<String, ArrayList<SsipConcentration>>) partitionSSIPs(getSsipConcentrations(), calculateCentroidMap());
		LOG.trace("Positive SSIPs: {}", ssipsByType.get(POSITIVE_STRING));
		LOG.trace("Negative SSIPs: {}", ssipsByType.get(NEGATIVE_STRING));
		HashMap<String, Integer> numberSSIPsByMolecule = (HashMap<String, Integer>) getNumberOfSSIPsbyMolecule();
		HashMap<String, Double> moleFractionByMolecule = (HashMap<String, Double>) getMoleFractionByMoleculeID();
		//Initialise map so all values are set to zero.
		for (String moleculeID : numberSSIPsByMolecule.keySet()) {
			interactionEnergyByMoleculeID.put(moleculeID, 0.0);
		}
		LOG.trace("Interaction energies by Molecule: {}", interactionEnergyByMoleculeID);
		// i is iterator value for positive SSIPs
		int i = 0;
		// j is iterator value for negative SSIPs
		int j = 0;
		//conditional loop for calculating total interaction energies for each molecule
		LOG.trace("Number of Positive SSIPs: {}", ssipsByType.get(POSITIVE_STRING).size());
		LOG.trace("Number of Negative SSIPs: {}", ssipsByType.get(NEGATIVE_STRING).size());
		int currentalpha = 0;

		
		double chialpharemaining;
		int currentbeta = 0;
		double chibetaremaining;
		
		if (ssipsByType.get(POSITIVE_STRING).size() > 0 && ssipsByType.get(NEGATIVE_STRING).size() > 0) {
			//mole fraction remaining for alpha set to total at start for first alpha.
			chialpharemaining = moleFractionByMolecule.get(ssipsByType.get(POSITIVE_STRING).get(i).getMoleculeID());
			//mole fraction remaining for beta set to total at start for first beta.
			chibetaremaining = moleFractionByMolecule.get(ssipsByType.get(NEGATIVE_STRING).get(j).getMoleculeID());
		} else {
			chialpharemaining = 0.0;
			chibetaremaining = 0.0;
		}
		
		
		while (i < ssipsByType.get(POSITIVE_STRING).size() && j < ssipsByType.get(NEGATIVE_STRING).size()) {
			//check to see if alpha index has been incremented, then update mole fraction available.
			//Done now to avoid index out of bounds error.
			if (i != currentalpha) {
				chialpharemaining = moleFractionByMolecule.get(ssipsByType.get(POSITIVE_STRING).get(i).getMoleculeID());
				currentalpha = i;
			}
			//check to see if beta index has been incremented, then update mole fraction available.
			//Done now to avoid index out of bounds error.
			if (j != currentbeta) {
				chibetaremaining = moleFractionByMolecule.get(ssipsByType.get(NEGATIVE_STRING).get(j).getMoleculeID());
				currentbeta = j;
			}
			//get SSIPs
			SsipConcentration alphaSSIP = ssipsByType.get(POSITIVE_STRING).get(i);
			SsipConcentration betaSSIP = ssipsByType.get(NEGATIVE_STRING).get(j);
			LOG.trace("alpha molecule: {} beta molecule: {}", alphaSSIP.getMoleculeID(), betaSSIP.getMoleculeID());
			//calculate interaction energy for SSIPs at zero kelvin.
			double interactionenergy = InteractionEnergyCalculator.calculateInteractionEnergyAZ(alphaSSIP.getValue() * betaSSIP.getValue());
			//calculate mole fraction of SSIPs that is interacting. Assumes all available will interact.
			double molefractioninteracting = Math.min(chialpharemaining, chibetaremaining);
			//contribution to total from each SSIP is half total (chi*E_{0}) for the interaction.
			//This is to properly partition contribution to total for each molecule.
			double intEnergyContribution = 0.5 * molefractioninteracting * interactionenergy;
			LOG.trace("chialprem: {} chibetrem: {} alpha value: {} beta value: {} ", chialpharemaining, chibetaremaining, alphaSSIP.getValue(), betaSSIP.getValue());
			
			LOG.trace("interactionenergy: {} mole frac interacting: {} interaction energy contribution: {}", interactionenergy, molefractioninteracting, intEnergyContribution);
			//update contribution from alpha point
			interactionEnergyByMoleculeID.put(alphaSSIP.getMoleculeID(), interactionEnergyByMoleculeID.get(alphaSSIP.getMoleculeID()) + intEnergyContribution);
			//update contribution from beta point
			interactionEnergyByMoleculeID.put(betaSSIP.getMoleculeID(), interactionEnergyByMoleculeID.get(betaSSIP.getMoleculeID()) + intEnergyContribution);
			// now to update amount available to interact.
			chialpharemaining -= molefractioninteracting;
			chibetaremaining -= molefractioninteracting;
			LOG.trace("interaction energy by mol: {}", interactionEnergyByMoleculeID);
			LOG.trace(" updated chialprem: {} chibetrem: {}", chialpharemaining, chibetaremaining);
			//check to see if alpha fraction is zero (below tolerance)
			if (chialpharemaining < TOLERANCE) {
				i += 1;
			}
			//check to see if beta fraction is zero (below tolerance)
			if (chibetaremaining < TOLERANCE) {
				j += 1;
			}
			LOG.trace("current alpha: {} current beta: {}, i: {} j:{}", currentalpha, currentbeta, i, j);
		}
		LOG.debug("Interaction energies by Molecule: {}", interactionEnergyByMoleculeID);
		//loop for converting to per SSIP energy, and weighting by molefraction.
		HashMap<String, Double> intEnergyByMoleculeIDPerSSIP = new HashMap<>();
		for (Map.Entry<String, Double> interactionEnergyEntry : interactionEnergyByMoleculeID.entrySet()) {
			double interactionEnergyPerSSIP = -interactionEnergyEntry.getValue()/(double) numberSSIPsByMolecule.get(interactionEnergyEntry.getKey());
			double weightedIntEnergyPerSSIP = interactionEnergyPerSSIP/getMoleFractions().get(interactionEnergyEntry.getKey());
			LOG.debug("moleculeID: {} molefraction: {} interaction energy per SSIP:{} weighted energy: {}", interactionEnergyEntry.getKey(), getMoleFractions().get(interactionEnergyEntry.getKey()), interactionEnergyPerSSIP, weightedIntEnergyPerSSIP);
			intEnergyByMoleculeIDPerSSIP.put(interactionEnergyEntry.getKey(), weightedIntEnergyPerSSIP);
		}
		LOG.debug("int energy per SSIP: {}", intEnergyByMoleculeIDPerSSIP);
		return intEnergyByMoleculeIDPerSSIP;
	}
	
	private ArrayRealVector calculateInteractionExpansionEnergies(){
		ArrayList<SsipConcentration> ssipConcs = (ArrayList<SsipConcentration>) getSsipConcentrations();
		HashMap<String, Double> interactionEnergyByMoleculeID = (HashMap<String, Double>) calculateInteractionExpansionEnergy();
		ArrayRealVector interactionExpansionEnergiesMatrix =new ArrayRealVector(ssipConcs.size());
		for (int i = 0; i < interactionExpansionEnergiesMatrix.getDimension(); i++) {
			interactionExpansionEnergiesMatrix.setEntry(i, interactionEnergyByMoleculeID.get(ssipConcs.get(i).getMoleculeID()));
		}
		return interactionExpansionEnergiesMatrix;
	}

	/**
	 * This calculates the expansion energies for each SSIP in the phase, for a given phi_{bound}, 
	 * the fraction bound due to polar interactions.
	 * 
	 * @param phibound population that is bound due to polar interactions
	 * @return energies
	 */
	public ArrayRealVector getExpansionEnergies(ArrayRealVector phibound){
		
		ArrayRealVector expansionEnergies = getVdWExpEnergies().add(getInteractionExpansionEnergies().ebeMultiply(phibound));
		LOG.info("E_exp VdW: {}", getVdWExpEnergies());
		LOG.info("E_exp polar: {}", getInteractionExpansionEnergies());
		LOG.info("Phi bound: {}", phibound);
		LOG.info("E_exp total: {}", expansionEnergies);
		return expansionEnergies;  
	}
	
	public List<SsipConcentration> getSsipConcentrations() {
		return ssipConcentrations;
	}

	public Map<String, PhaseMolecule> getPhaseMoleculeMap() {
		return phaseMoleculeMap;
	}

	public Map<String, Double> getMoleFractionByMoleculeID() {
		return moleFractionByMoleculeID;
	}

	public ArrayRealVector getVdWExpEnergies() {
		return vdWExpEnergies;
	}

	public ArrayRealVector getInteractionExpansionEnergies() {
		return interactionExpansionEnergies;
	}
	
}
