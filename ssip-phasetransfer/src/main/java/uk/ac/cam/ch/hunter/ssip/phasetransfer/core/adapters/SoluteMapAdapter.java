/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolute;

/**
 * {@link XmlAdapter} for maps of {@link PhaseSolute} objects, where the key is soluteID. 
 * 
 * @author mdd31
 *
 */
public class SoluteMapAdapter extends XmlAdapter<SoluteMapAdapter.AdaptedSoluteMap, Map<String, PhaseSolute>> {
	/**
	 * Adapted Solute map stores the information as an {@link ArrayList} of {@link PhaseSolute} objects. Ordering is based on soluteID.
	 * 
	 * @author mdd31
	 *
	 */
	@XmlAccessorType(XmlAccessType.NONE)
	public static class AdaptedSoluteMap{
		/**
		 * {@link ArrayList} of {@link PhaseSolute} entries.
		 */
		@XmlElement(name = "Solute", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
		public List<PhaseSolute> phaseSolutes = new ArrayList<>();
	}

@Override
public AdaptedSoluteMap marshal(Map<String, PhaseSolute> phaseSoluteMap) throws Exception {
	AdaptedSoluteMap adaptedSoluteMap = new AdaptedSoluteMap();
	for (Map.Entry<String, PhaseSolute> soluteEntry : phaseSoluteMap.entrySet()) {
		adaptedSoluteMap.phaseSolutes.add(soluteEntry.getValue());
	}
	Collections.sort(adaptedSoluteMap.phaseSolutes);
	return adaptedSoluteMap;
}

@Override
public Map<String, PhaseSolute> unmarshal(AdaptedSoluteMap adaptedSoluteMap) throws Exception {
	HashMap<String, PhaseSolute> phaseSoluteMap = new HashMap<>();
	for (PhaseSolute phaseSolute : adaptedSoluteMap.phaseSolutes) {
		phaseSoluteMap.put(phaseSolute.getSoluteID(), phaseSolute);
	}
	return phaseSoluteMap;
	
}

}
