/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.math3.analysis.function.Log;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.ProjectProperties;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

/**
 * Class containing information about the given molecule in the phase.
 * 
 * @author mdd31
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Molecule", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlType(propOrder = { "cmlMolecule", "ssipSurfaceInformation", "ssipConcentrationContainer" })
public class PhaseMolecule implements Comparable<PhaseMolecule>{

	@XmlTransient
	private static final Logger LOG = LogManager
			.getLogger(PhaseMolecule.class);
	
	@XmlElement(name = "molecule", namespace = "http://www.xml-cml.org/schema")
	private final CmlMolecule cmlMolecule;
	
	@XmlElement(name = "SSIPs", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final SsipConcentrationContainer ssipConcentrationContainer;
	
	@XmlElement(name = "SurfaceInformation", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private final SsipSurfaceInformation ssipSurfaceInformation;
	
	@XmlAttribute(name = "ssipSoftwareVersion", namespace = "http://www-hunter.ch.cam.ac.uk/SSIP")
	private final String softwareVersion;
	
	@XmlAttribute(name = "moleculeID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String moleculeID;
	@XmlAttribute(name = "stdInChIKey", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String stdinchikey;
	@XmlAttribute(name = "moleFraction", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private Double moleFraction;
	
	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;

	
	/**
	 * Constructor for JAXB.
	 */
	public PhaseMolecule(){
		this.cmlMolecule = null;
		this.ssipConcentrationContainer = null;
		this.ssipSurfaceInformation = null;
		this.softwareVersion = null;
		this.moleculeID = null;
		this.stdinchikey = null;
	}
	
	/**
	 * Constructor for Phase Molecule.
	 * 
	 * @param cmlMolecule cml
	 * @param ssipSurfaceInformation surface information
	 * @param ssipConcentrationContainer SSIPs
	 * @param moleculeID ID
	 * @param stdinchikey StdInChIKey
	 */
	public PhaseMolecule(CmlMolecule cmlMolecule, SsipSurfaceInformation ssipSurfaceInformation, SsipConcentrationContainer ssipConcentrationContainer, String moleculeID, String stdinchikey){
		this.cmlMolecule = cmlMolecule;
		this.ssipSurfaceInformation = ssipSurfaceInformation;
		this.ssipConcentrationContainer = ssipConcentrationContainer;
		ProjectProperties projectProperties = new ProjectProperties();
		this.softwareVersion = projectProperties.getVersion();
		this.moleculeID = moleculeID;
		this.stdinchikey = stdinchikey;
	}
	
	/**
	 * Constructor to create new copy of a PhaseMolecule.
	 * 
	 * @param phaseMolecule molecule
	 */
	public PhaseMolecule(PhaseMolecule phaseMolecule){
		this.cmlMolecule = phaseMolecule.getCmlMolecule();
		this.ssipSurfaceInformation = phaseMolecule.getSsipSurfaceInformation();
		this.ssipConcentrationContainer = new SsipConcentrationContainer(phaseMolecule.getSsipConcentrationContainer());
		ProjectProperties projectProperties = new ProjectProperties();
		this.softwareVersion = projectProperties.getVersion();
		this.moleculeID = phaseMolecule.getMoleculeID();
		this.stdinchikey = phaseMolecule.getStdinchikey();
		try {
			this.moleFraction = phaseMolecule.getMoleFraction();
		} catch (NullPointerException e){
			LOG.info("No molefraction: {}", e);
		}
	}
	
	public List<SsipConcentration> getSSIPConcentrations(){
		return getSsipConcentrationContainer().getSsipConcentrations();
	}
	
	public ArrayRealVector getTotalConcentrations(){
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getSSIPConcentrations();
		ArrayRealVector totalConcentrations = new ArrayRealVector(
				ssipConcentrations.size());
		for (int i = 0; i < totalConcentrations.getDimension(); i++) {
			totalConcentrations.setEntry(i, ssipConcentrations.get(i).getTotalConcentration());
		}
		return totalConcentrations;
	}
	
	public ArrayRealVector getFreeConcentrations(){
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getSSIPConcentrations();
		ArrayRealVector freeConcentrations = new ArrayRealVector(
				ssipConcentrations.size());
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			freeConcentrations
					.setEntry(i, ssipConcentrations.get(i).getFreeConcentration());
		}
		return freeConcentrations;
	}
	
	/**
	 * Get bound concentrations for each SSIP in molecule, bound to the given SSIPs, in the same order.
	 * 
	 * @param ssipIndices indices
	 * @return array of bound concentrations.
	 */
	public Array2DRowRealMatrix getBoundConcentrationValuesToOtherSSIPs(List<Integer> ssipIndices) {
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getSSIPConcentrations();
		Array2DRowRealMatrix boundConcs = new Array2DRowRealMatrix(ssipConcentrations.size(), ssipIndices.size());
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			boundConcs.setRowVector(i, ssipConcentrations.get(i).getBoundConcentrationsToOtherSSIPs(ssipIndices));
		}
		return boundConcs;
	}
	
	/**
	 * This gets the concentration of the SSIPs which are bound to the subset of SSIPs given.
	 * 
	 * @param ssipIndices indices
	 * @return Concentration bound to selected SSIPs in phase for each SSIP in molecule. 
	 */
	public ArrayRealVector getBoundConcentrationsToOtherSSIPs(List<Integer> ssipIndices) {
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getSSIPConcentrations();
		ArrayRealVector boundConcentrations = new ArrayRealVector(
				ssipConcentrations.size());
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			boundConcentrations.setEntry(i, ssipConcentrations.get(i)
					.getBoundConcentrationToOtherSSIPs(ssipIndices).getConcentrationValue());
		}
		return boundConcentrations;
	}
	
	public ArrayRealVector getBoundConcentrations(){
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getSSIPConcentrations();
		ArrayRealVector boundConcentrations = new ArrayRealVector(
				ssipConcentrations.size());
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			boundConcentrations.setEntry(i, ssipConcentrations.get(i)
					.getBoundConcentration());
		}
		return boundConcentrations;
	}
	
	/**
	 * Set the bound concentration {@link ArrayRealVector} from the {@link ArrayList} of {@link BoundConcentration}s.
	 */
	public void setBoundConcentrations(){
		this.getSsipConcentrationContainer().setBoundConcentrations();
	}
	
	public Array2DRowRealMatrix getBoundConcentrationsMatrix(){
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getSSIPConcentrations();
		Array2DRowRealMatrix boundConcentrationsMatrix = new Array2DRowRealMatrix(
				ssipConcentrations.size(),
				getNumberOfOtherSSIPsInPhase());
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			ArrayRealVector boundValues = ssipConcentrations.get(i)
					.getBoundConcentrationList();
			LOG.debug("boundValues dimension: {}", boundValues.getDimension());
			LOG.debug("boundValues: {}", boundValues);
			boundConcentrationsMatrix.setRowVector(i, boundValues);
			LOG.debug("boundConcentrationMatrix: {}", boundConcentrationsMatrix);
		}
		return boundConcentrationsMatrix;
	}
	
	/**
	 * This returns a new {@link PhaseMolecule} with the concentrations in the {@link ConcentrationUnit} given.
	 * 
	 * @param concentrationUnit unit
	 * @return phase molecule
	 */
	public PhaseMolecule convertConcentrationsTo(ConcentrationUnit concentrationUnit){
		LOG.debug("Starting SSIPs: {}", getSSIPConcentrations());
		SsipConcentrationContainer convertedContainer = getSsipConcentrationContainer().convertConcentrationsTo(concentrationUnit);
		LOG.debug("Converted SSIPs: {}", convertedContainer.getSsipConcentrations());
		PhaseMolecule convertedPhaseMolecule = new PhaseMolecule(getCmlMolecule(), getSsipSurfaceInformation(), convertedContainer, getMoleculeID(), getStdinchikey());
		try {
			convertedPhaseMolecule.setMoleFraction(getMoleFraction());
		} catch (NullPointerException e) {
			LOG.debug("No Mole fraction found: ", e);
		}
		return convertedPhaseMolecule;
	}
	
	int getNumberOfOtherSSIPsInPhase(){
		int numberOfSSIPsInPhase;

		numberOfSSIPsInPhase = getSSIPConcentrations().get(0)
				.getBoundConcentrationList().getDimension();
		return numberOfSSIPsInPhase;
	}
	
	/**
	 * This returns a Double matrix of the fraction Free for each SSIP in the
	 * SSIPConcentrationList, with the order based on the order of the SSIPs.
	 * 
	 * From eqn 16 in <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * @return fraction free
	 */
	public ArrayRealVector calculateFractionFree(){
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getSSIPConcentrations();
		ArrayRealVector fractionFree = new ArrayRealVector(ssipConcentrations.size());
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			fractionFree.setEntry(i, ssipConcentrations.get(i).calculateFractionFree());
		}
		return fractionFree;
	}
	/**
	 * This returns a Double matrix of ln(FractionFree) for each SSIP in the
	 * SSIPConcentrationList, with the order based on the order of the SSIPs.
	 * 
	 * Fraction Free as defined in eqn 16 in 
	 * <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * @return ln(fraction free)
	 */
	public ArrayRealVector calculateLogFractionFree(){
		ArrayRealVector logFractionFree = calculateFractionFree();
		return logFractionFree.map(new Log());
	}
	/**
	 * This return the sum of ln(FractionFree) for all SSIPs in the
	 * SSIPConcnetrationList.
	 * 
	 * Fraction Free as defined in eqn 16 in
	 * <a href="http://dx.doi.org/10.1039/C3SC22124E">paper</a>.
	 * 
	 * @return ln(fraction free) sum
	 */
	public double calculateLogFractionFreeSum(){
		return VectorUtils.sumVectorElements(calculateLogFractionFree());
	}
	
	/**
	 * This calculates the mean fraction of Molecule that is free.
	 * 
	 * @return mean free fraction
	 */
	public double calculateMeanFractionFree() {
		ArrayRealVector fractionsFree = calculateFractionFree();
		return VectorUtils.sumVectorElements(fractionsFree)/(double) fractionsFree.getDimension();
	}
	
	/**
	 * This Calculates the mean fraction of the molecule bound to the given set of SSIPs.
	 * 
	 * @param ssipIndices indices
	 * @return mean bound fraction.
	 */
	public double calculateMeanFractionBoundToGivenSSIPs(List<Integer> ssipIndices) {
		ArrayRealVector fractionsBound = calculateFractionBoundToGivenSSIPs(ssipIndices);
		return VectorUtils.sumVectorElements(fractionsBound)/(double) fractionsBound.getDimension();
	}
	
	/**
	 * This calculates the fraction of each SSIP in the molecule bound to the given SSIPs in the phase.
	 * 
	 * @param ssipIndices indices
	 * @return fraction bound
	 */
	public ArrayRealVector calculateFractionBoundToGivenSSIPs(List<Integer> ssipIndices) {
		ArrayRealVector boundConcentrations = getBoundConcentrationsToOtherSSIPs(ssipIndices);
		ArrayRealVector totalConcentrations = getTotalConcentrations();
		return boundConcentrations.ebeDivide(totalConcentrations);
	}
	
	public ConcentrationUnit getMoleculeConcentrationUnit(){
		return getSsipConcentrationContainer().getConcentrationUnit();
	}
	
	public Point3D getMoleculeCentroid(){
		return getCmlMolecule().getMoleculeCentroid();
	}
	
	public CmlMolecule getCmlMolecule() {
		return cmlMolecule;
	}

	public SsipConcentrationContainer getSsipConcentrationContainer() {
		return ssipConcentrationContainer;
	}

	public SsipSurfaceInformation getSsipSurfaceInformation() {
		return ssipSurfaceInformation;
	}

	public String getSoftwareVersion() {
		return softwareVersion;
	}

	public String getMoleculeID() {
		return moleculeID;
	}

	public void setMoleculeID(String moleculeID) {
		this.moleculeID = moleculeID;
	}

	public String getStdinchikey() {
		return stdinchikey;
	}
	
	public double getMoleFraction() {
		return moleFraction;
	}

	public void setMoleFraction(double moleFraction) {
		this.moleFraction = moleFraction;
	}

	/**
	 * This returns the mole fraction of each SSIP in the molecule. 
	 * 
	 * @return mole fractions
	 */
	public RealVector getMoleFractions(){
		ArrayRealVector moleFractions = new ArrayRealVector(getSSIPConcentrations().size());
		return moleFractions.mapAdd(getMoleFraction());
	}
	
	/**
	 * This returns the volumes corresponding to each SSIP.
	 * 
	 * @return volumes
	 */
	public RealVector getVdWVolumes(){
		ArrayRealVector volumes = new ArrayRealVector(getSSIPConcentrations().size());
		return volumes.mapAdd(getSsipSurfaceInformation().getVdWVolume());
	}
	
	@Override
	public boolean equals(Object obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhaseMolecule other = (PhaseMolecule) obj;
		LOG.trace("MolID1: {} MolID2: {}", this.getMoleculeID(), other.getMoleculeID());
		LOG.trace("stdinchikey1: {} stdinchikey2: {}", this.getStdinchikey(), other.getStdinchikey());
		if (this.getMoleculeID() == other.getMoleculeID() && this.getStdinchikey() == other.getStdinchikey()) {
			return this.getCmlMolecule().equals(other.getCmlMolecule()) && this.getSsipSurfaceInformation().equals(other.getSsipSurfaceInformation()) && this.getSsipConcentrationContainer().equals(other.getSsipConcentrationContainer()); 
		} else {
			return false;
		}
		 
	}
	
	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = super.hashCode();
			long temp;
			temp = cmlMolecule.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = ssipConcentrationContainer.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = ssipSurfaceInformation.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = moleculeID.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = stdinchikey.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			
			if (moleFraction != null) {
				temp = moleFraction.hashCode();
				result = prime * result + (int) (temp ^ (temp >>> 32));
			}
			
			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
	@Override
	public String toString(){
		return getMoleculeID();
	}
	
	@Override
	public int compareTo(PhaseMolecule phaseMolecule){
		return getMoleculeID().compareTo(phaseMolecule.getMoleculeID());
	}
}
