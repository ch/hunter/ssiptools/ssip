/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * This stores Concentration fraction information for a molecule.
 * 
 * @author mdd31
 *
 */
@XmlRootElement(name = "MoleculeConcentrationFraction", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class MoleculeConcentrationFraction {
	
	@XmlAttribute(name="moleculeID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String moleculeID;
	
	@XmlElement(name = "FreeConcentrationFraction", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private FreeConcentrationFraction freeConcentrationFraction;

	@XmlElement(name = "BoundConcentrationFraction", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private ArrayList<BoundConcentrationFraction> boundConcentrationFractions;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	protected int hash;
	@XmlTransient
	protected boolean hashCodeFound = false;	
	
	public MoleculeConcentrationFraction() {
		/**
		 * Constructor for JAXB.
		 */
	}

	public MoleculeConcentrationFraction(String moleculeID, FreeConcentrationFraction freeConcentrationFraction,
			List<BoundConcentrationFraction> boundConcentrationFractions) {
		this.moleculeID = moleculeID;
		this.freeConcentrationFraction = freeConcentrationFraction;
		this.boundConcentrationFractions = (ArrayList<BoundConcentrationFraction>) boundConcentrationFractions;
	}

	public String getMoleculeID() {
		return moleculeID;
	}

	public void setMoleculeID(String moleculeID) {
		this.moleculeID = moleculeID;
	}

	public FreeConcentrationFraction getFreeConcentrationFraction() {
		return freeConcentrationFraction;
	}

	public void setFreeConcentrationFraction(FreeConcentrationFraction freeConcentrationFraction) {
		this.freeConcentrationFraction = freeConcentrationFraction;
	}

	public List<BoundConcentrationFraction> getBoundConcentrationFractions() {
		return boundConcentrationFractions;
	}

	public void setBoundConcentrationFractions(List<BoundConcentrationFraction> boundConcentrationFractions) {
		this.boundConcentrationFractions = (ArrayList<BoundConcentrationFraction>) boundConcentrationFractions;
	}

	@Override
	public String toString() {
		return String.format("Molecule Concentration MoleculeID:%s%n fractions:%n%s%n%s", getMoleculeID(), getFreeConcentrationFraction().toString(), getBoundConcentrationFractions().toString());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoleculeConcentrationFraction other = (MoleculeConcentrationFraction) obj;
		return this.getMoleculeID().equals(other.getMoleculeID())
				&& this.getFreeConcentrationFraction().equals(other.getFreeConcentrationFraction())
				&& this.getBoundConcentrationFractions().equals(other.getBoundConcentrationFractions());
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {

			final int prime = 31;
			int result = super.hashCode();
			long temp;
			temp = moleculeID.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = freeConcentrationFraction.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = boundConcentrationFractions.hashCode();
			result = prime * result + (int) (temp ^ (temp >>> 32));

			hashCodeFound = true;
			hash = result;
		}

		return hash;
	}
	
}
