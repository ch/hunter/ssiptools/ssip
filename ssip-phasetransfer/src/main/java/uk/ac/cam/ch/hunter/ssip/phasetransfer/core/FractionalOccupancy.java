/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Class for Storing information on fractional occupancy (theta) of a phase.
 * 
 * @author Mark D
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class FractionalOccupancy {
	
	@XmlTransient
	private static final double ERROR = 1e-7;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	private int hash;
	@XmlTransient
	private boolean hashCodeFound = false;
	
	@XmlAttribute(name = "value", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final double value;
	
	@XmlAttribute(name = "solventID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String solventID;
	
	@XmlAttribute(name = "soluteID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String soluteID;
	
	@XmlAttribute(name = "temperature", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final double temperature;
	
	/**
	 * Constructor for JAXB.
	 */
	public FractionalOccupancy() {
		this.value = 0.0;
		this.solventID = "";
		this.soluteID = "";
		this.temperature = 0.0;
	}

	/**
	 * Constructor
	 * 
	 * @param value value of fractional occupancy
	 * @param solventID solvent ID
	 * @param soluteID solute ID
	 * @param temperature temperature in Kelvin.
	 */
	public FractionalOccupancy(double value, String solventID, String soluteID, double temperature) {
		this.value = value;
		this.solventID = solventID;
		this.soluteID = soluteID;
		this.temperature = temperature;
	}

	@Override
	public String toString() {
		return String
				.format("solvent ID: %s solute ID: %s value: %f temperature/K: %f",
						getSolventID(), getSoluteID(),
						getValue(), getTemperature());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FractionalOccupancy other = (FractionalOccupancy) obj;
		if (this.getSolventID() == other.getSolventID()
				&& this.getSoluteID() == other.getSoluteID()) {
			return Math.abs(this.getValue() - other.getValue()) < ERROR
					&& Math.abs(this.getTemperature() - other.getTemperature()) < ERROR;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 23;
			int result = 1;
			result = prime * result + getSoluteID().hashCode()
					+ getSolventID().hashCode() + Double.hashCode(getTemperature());
			hash = result;
			hashCodeFound = true;
		}
		return hash;
	}


	public double getValue() {
		return value;
	}

	public String getSolventID() {
		return solventID;
	}

	public String getSoluteID() {
		return soluteID;
	}

	public double getTemperature() {
		return temperature;
	}

}
