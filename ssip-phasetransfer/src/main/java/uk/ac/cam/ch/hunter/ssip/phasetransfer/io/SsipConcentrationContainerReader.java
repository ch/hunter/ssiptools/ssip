/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import java.io.File;
import java.net.URI;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.io.SchemaMaker;
import uk.ac.cam.ch.hunter.ssip.core.io.XmlValidationEventHandler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;

/**
 * Class for reading {@link SsipConcentration} and {@link SsipConcentrationContainer} from file. Unmarshalling is done by JAXB.
 * 
 * @author mdd31
 *
 */
public class SsipConcentrationContainerReader {

	private static final Logger LOG = LogManager.getLogger(SsipConcentrationContainerReader.class);
	
	private static final Schema phaseSchema = SchemaMaker.createPhaseSchema();
	
	private SsipConcentrationContainerReader(){
		/**
		 * Constructor
		 */
	}
	
	/**
	 * Unmarshal {@link SsipConcentrationContainer} from file using JAXB.
	 * 
	 * @param uri File URI.
	 * @return ssip concentration container
	 * @throws JAXBException exception.
	 */
	public static SsipConcentrationContainer unmarshal(URI uri) throws JAXBException{
		File file = new File(uri);
		JAXBContext jaxbContext = JAXBContext.newInstance(SsipConcentrationContainer.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		return (SsipConcentrationContainer) jaxbUnmarshaller.unmarshal(file);
	}
	/**
	 * Unmarshal {@link SsipConcentration} from file using JAXB.
	 * 
	 * @param uri File URI.
	 * @return ssip concentration
	 * @throws JAXBException exception.
	 */
	public static SsipConcentration unmarshalConcentration(URI uri) throws JAXBException{
		File file = new File(uri);
		JAXBContext jaxbContext = JAXBContext.newInstance(Ssip.class, SsipConcentration.class);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
 
		jaxbUnmarshaller.setSchema(phaseSchema);
		jaxbUnmarshaller.setEventHandler(new XmlValidationEventHandler());
		
		Object value = jaxbUnmarshaller.unmarshal(file);
		LOG.debug("object: {}", value);
		LOG.debug("value class: {}", value.getClass());
		return (SsipConcentration) value;
	}
}
