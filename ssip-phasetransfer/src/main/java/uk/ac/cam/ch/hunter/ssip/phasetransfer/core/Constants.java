/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

/**
 * Class containing constants used in calculations.
 * 
 * @author mdd31
 *
 */
public class Constants {
	
	private Constants() {
		/*
		 * private constructor.
		 */
	}
	
	/**
	 * Conversion factor 
	 */
	public static final double J_TO_KJ_CONVERSION = 1e-3;
	
	/**
	 * value for the gas constant, R in JK^-1mol^-1.
	 * Converted to kJK^-1mol^-1.
	 * 
	 * see <a href="https://en.wikipedia.org/wiki/Gas_constant">https://en.wikipedia.org/wiki/Gas_constant</a>
	 */
	public static final double GAS_CONSTANT = 8.3144598 * J_TO_KJ_CONVERSION;

	/**
	 * value of the van der Waals interaction of 2 SSIPs interacting in a
	 * pairwise manner. As detailed in 
	 * <a href="http://dx.doi.org/10.1039/c3sc22124e">paper</a>.
	 */
	public static final double EVDW = -5.6;

	/**
	 * Value for room temperature in Kelvin.
	 */
	public static final double ROOM_TEMPERATURE = 298.0;
	
	/**
	 * Value for tolerance to be used in cycles.
	 */
	public static final double TOLERANCE = 1e-14;
	
	/**
	 * Tolerance to be used in gas concentration cycles.
	 */
	public static final double GAS_TOLERANCE = 1e-14;
}
