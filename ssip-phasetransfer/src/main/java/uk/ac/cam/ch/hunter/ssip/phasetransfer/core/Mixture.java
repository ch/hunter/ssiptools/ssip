/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters.PhaseMapAdapter;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IEnergyDifferenceCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.IMixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces.ISolvationEnergyCalculator;



/**
 * Mixture class is a container for a collection of Phases. This is used to
 * calculate the free energy of transfer for a given species between phases.
 * 
 * @author mdd31
 * 
 */
@XmlRootElement(name = "Mixture", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class Mixture implements IMixture, IEnergyDifferenceCalculator, ISolvationEnergyCalculator {

	@XmlTransient
	private static final Logger LOG = LogManager.getLogger(Mixture.class);
	/**
	 * This Map contains all the phases with the key acting as a label for the corresponding phase.
	 */
	@XmlElement(name = "Phases", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlJavaTypeAdapter(PhaseMapAdapter.class)
	private HashMap<String, Phase> phaseMap;
	
	@XmlAttribute(name = "soluteID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private String soluteID;
	
	public Mixture(){
		/**
		 * Constructor for JAXB.
		 */
	}
	
	/**
	 * This initialises the Mixture with a collection of named phases.
	 * 
	 * @param phaseMap phase map
	 */
	public Mixture(Map<String, Phase> phaseMap){
		setPhaseMap(phaseMap);
	}

	@Override
	public double calculateConfinementEnergyDifference(String moleculeID, String fromPhase, String toPhase){
		double confinementEnergyPhase1 = getPhaseMap().get(fromPhase).calculateConfinementEnergy(moleculeID);
		double confinementEnergyPhase2 = getPhaseMap().get(toPhase).calculateConfinementEnergy(moleculeID);
		return confinementEnergyPhase2 - confinementEnergyPhase1;
	}

	@Override
	public ArrayRealVector calculateConfinementEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase){
		ArrayRealVector confinementEnergyMatrixFromPhase = getPhaseMap().get(fromPhase).calculateConfinementEnergyPerSSIPMatrix(moleculeID);
		ArrayRealVector confinementEnergyMatrixToPhase = getPhaseMap().get(toPhase).calculateConfinementEnergyPerSSIPMatrix(moleculeID);
		return confinementEnergyMatrixToPhase.subtract(confinementEnergyMatrixFromPhase);
	}

	@Override
	public EnergyValue calculateConfinementEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateConfinementEnergyDifference(moleculeID, fromPhase, toPhase), calculateConfinementEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergyDifferenceToPhase(String moleculeID, String toPhase) {
		ArrayList<EnergyValue> confinementEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseMap().keySet()) {
			confinementEnergyDifferenceList.add(calculateConfinementEnergyDifferenceValue(moleculeID, fromPhase, toPhase));
		}
		return confinementEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergyDifferenceBetweenAllPhases(String moleculeID){
		ArrayList<EnergyValue> completeConfinementEnergyDifferenceMap = new ArrayList<>();
		for (String toPhase : getPhaseMap().keySet()) {
			completeConfinementEnergyDifferenceMap.addAll(calculateConfinementEnergyDifferenceToPhase(moleculeID, toPhase));
		}
		return completeConfinementEnergyDifferenceMap;
	}

	@Override
	public double calculateConfinementEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase){
		double confinementEnergy = calculateConfinementEnergyDifference(moleculeID, fromPhase, toPhase);
		double conversionFactor = calculateConversionFactorTo1MScale(toPhase, fromPhase);
		return confinementEnergy + conversionFactor;
	}

	@Override
	public ArrayRealVector calculateConfinementEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase){
		return calculateConfinementEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase);
	}

	@Override
	public EnergyValue calculateConfinementEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateConfinementEnergyDifferenceFor1M(moleculeID, fromPhase, toPhase), calculateConfinementEnergyDifferenceFor1MPerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLAR);
	}
	
	@Override
	public List<EnergyValue> calculateConfinementEnergyDifferenceToPhase1M(String moleculeID, String toPhase) {
		ArrayList<EnergyValue> confinementEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseMap().keySet()) {
			confinementEnergyDifferenceList.add(calculateConfinementEnergyDifferenceValue1M(moleculeID, fromPhase, toPhase));
		}
		return confinementEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergyDifferenceBetweenAllPhases1M(String moleculeID){
		ArrayList<EnergyValue> completeConfinementEnergyDifferenceMap = new ArrayList<>();
		for (String toPhase : getPhaseMap().keySet()) {
			completeConfinementEnergyDifferenceMap.addAll(calculateConfinementEnergyDifferenceToPhase1M(moleculeID, toPhase));
		}
		return completeConfinementEnergyDifferenceMap;
	}
	
	@Override
	public double calculateConfinementEnergySolvation(String moleculeID, String phaseID){
		return getPhaseMap().get(phaseID).calculateConfinementEnergy(moleculeID);
	}

	@Override
	public ArrayRealVector calculateConfinementEnergySolvationPerSSIP(String moleculeID, String phaseID){
		return getPhaseMap().get(phaseID).calculateConfinementEnergyPerSSIPMatrix(moleculeID);
	}

	@Override
	public EnergyValue calculateConfinementEnergySolvationEnergyValue(String moleculeID, String phaseID){
		return new EnergyValue(moleculeID, phaseID, "", calculateConfinementEnergySolvation(moleculeID, phaseID), calculateConfinementEnergySolvationPerSSIP(moleculeID, phaseID), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergySolvationAllPhases(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (String phaseID : getPhaseMap().keySet()) {
			energyValues.add(calculateConfinementEnergySolvationEnergyValue(moleculeID, phaseID));
		}
		return energyValues;
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergyValuesAllPhasesAllValues(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		energyValues.addAll(calculateConfinementEnergySolvationAllPhases(moleculeID));
		energyValues.addAll(calculateConfinementEnergyDifferenceBetweenAllPhases(moleculeID));
		energyValues.addAll(calculateConfinementEnergyDifferenceBetweenAllPhases1M(moleculeID));
		return energyValues;
	}
	
	@Override
	public double calculateBindingEnergyDifference(String moleculeID, String fromPhase, String toPhase){
		double bindingEnergyPhase1 = getPhaseMap().get(fromPhase).calculateBindingEnergy(moleculeID);
		double bindingEnergyPhase2 = getPhaseMap().get(toPhase).calculateBindingEnergy(moleculeID);
		return bindingEnergyPhase2 - bindingEnergyPhase1;
	}

	@Override
	public ArrayRealVector calculateBindingEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase){
		ArrayRealVector bindingEnergyMatrixFromPhase = getPhaseMap().get(fromPhase).calculateBindingEnergyPerSSIP(moleculeID);
		ArrayRealVector bindingEnergyMatrixToPhase = getPhaseMap().get(toPhase).calculateBindingEnergyPerSSIP(moleculeID);
		return bindingEnergyMatrixToPhase.subtract(bindingEnergyMatrixFromPhase);
	}

	@Override
	public EnergyValue calculateBindingEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateBindingEnergyDifference(moleculeID, fromPhase, toPhase), calculateBindingEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyDifferenceToPhase(String moleculeID, String toPhase){
		ArrayList<EnergyValue> bindingEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseMap().keySet()) {
			bindingEnergyDifferenceList.add(calculateBindingEnergyDifferenceValue(moleculeID, fromPhase, toPhase));
		}
		return bindingEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyDifferenceBetweenAllPhases(String moleculeID){
		ArrayList<EnergyValue> completeBindingEnergyDifferenceList = new ArrayList<>();
		for (String toPhase : getPhaseMap().keySet()) {
			completeBindingEnergyDifferenceList.addAll(calculateBindingEnergyDifferenceToPhase(moleculeID, toPhase));
		}
		return completeBindingEnergyDifferenceList;
	}

	@Override
	public double calculateBindingEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase){
		double bindingEnergy = calculateBindingEnergyDifference(moleculeID, fromPhase, toPhase);
		double conversionFactor = calculateConversionFactorTo1MScale(toPhase, fromPhase);
		return bindingEnergy + conversionFactor;
	}
	
	@Override
	public ArrayRealVector calculateBindingEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase){
		return calculateBindingEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase);
	}

	@Override
	public EnergyValue calculateBindingEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateBindingEnergyDifferenceFor1M(moleculeID, fromPhase, toPhase), calculateBindingEnergyDifferenceFor1MPerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLAR);
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyDifferenceToPhase1M(String moleculeID, String toPhase){
		ArrayList<EnergyValue> bindingEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseMap().keySet()) {
			bindingEnergyDifferenceList.add(calculateBindingEnergyDifferenceValue1M(moleculeID, fromPhase, toPhase));
		}
		return bindingEnergyDifferenceList;
	}
	
	@Override
	public List<EnergyValue> calculateBindingEnergyDifferenceBetweenAllPhases1M(String moleculeID){
		ArrayList<EnergyValue> completeBindingEnergyDifferenceList = new ArrayList<>();
		for (String toPhase : getPhaseMap().keySet()) {
			completeBindingEnergyDifferenceList.addAll(calculateBindingEnergyDifferenceToPhase1M(moleculeID, toPhase));
		}
		return completeBindingEnergyDifferenceList;
	}
	
	@Override
	public double calculateBindingEnergySolvation(String moleculeID, String phaseID){
		return getPhaseMap().get(phaseID).calculateBindingEnergy(moleculeID);
	}
	
	@Override
	public ArrayRealVector calculateBindingEnergySolvationPerSSIP(String moleculeID, String phaseID){
		return getPhaseMap().get(phaseID).calculateBindingEnergyPerSSIP(moleculeID);
	}

	@Override
	public EnergyValue calculateBindingEnergySolvationEnergyValue(String moleculeID, String phaseID){
		return new EnergyValue(moleculeID, phaseID, "", calculateBindingEnergySolvation(moleculeID, phaseID), calculateBindingEnergySolvationPerSSIP(moleculeID, phaseID), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateBindingEnergySolvationAllPhases(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (String phaseID : getPhaseMap().keySet()) {
			energyValues.add(calculateBindingEnergySolvationEnergyValue(moleculeID, phaseID));
		}
		return energyValues;
	}

	@Override
	public List<EnergyValue> calculateBindingEnergyValuesAllPhasesAllValues(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		energyValues.addAll(calculateBindingEnergySolvationAllPhases(moleculeID));
		energyValues.addAll(calculateBindingEnergyDifferenceBetweenAllPhases(moleculeID));
		energyValues.addAll(calculateBindingEnergyDifferenceBetweenAllPhases1M(moleculeID));
		return energyValues;
	}
	
	@Override
	public double calculateFreeEnergyDifference(String moleculeID, String fromPhase1, String toPhase){
		double freeEnergyPhase1 = getPhaseMap().get(fromPhase1).calculateEnergyInPhase(moleculeID);
		double freeEnergyPhase2 = getPhaseMap().get(toPhase).calculateEnergyInPhase(moleculeID);
		return freeEnergyPhase2 - freeEnergyPhase1;
	}
	
	@Override
	public ArrayRealVector calculateFreeEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase){
		ArrayRealVector freeEnergyMatrixFromPhase = getPhaseMap().get(fromPhase).calculateEnergyInPhaseBySSIP(moleculeID);
		ArrayRealVector freeEnergyMatrixToPhase = getPhaseMap().get(toPhase).calculateEnergyInPhaseBySSIP(moleculeID);
		return freeEnergyMatrixToPhase.subtract(freeEnergyMatrixFromPhase);
	}

	@Override
	public EnergyValue calculateFreeEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateFreeEnergyDifference(moleculeID, fromPhase, toPhase), calculateFreeEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyDifferenceToPhase(String moleculeID, String toPhase){
		ArrayList<EnergyValue> freeEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseMap().keySet()) {
			freeEnergyDifferenceList.add(calculateFreeEnergyDifferenceValue(moleculeID, fromPhase, toPhase));			
		}
		return freeEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyDifferenceBetweenAllPhases(String moleculeID){
		ArrayList<EnergyValue> completeFreeEnergyDifferenceList = new ArrayList<>();
		for (String toPhase : getPhaseMap().keySet()) {
			completeFreeEnergyDifferenceList.addAll(calculateFreeEnergyDifferenceToPhase(moleculeID, toPhase));
		}
		return completeFreeEnergyDifferenceList;
	}

	@Override
	public double calculateFreeEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase){
		double freeEnergy = calculateFreeEnergyDifference(moleculeID, fromPhase, toPhase);
		double conversionFactor = calculateConversionFactorTo1MScale(toPhase, fromPhase);
		return freeEnergy + conversionFactor;
	}

	@Override
	public ArrayRealVector calculateFreeEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase){
		return calculateFreeEnergyDifferencePerSSIP(moleculeID, fromPhase, toPhase);
	}

	@Override
	public EnergyValue calculateFreeEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase){
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateFreeEnergyDifferenceFor1M(moleculeID, fromPhase, toPhase), calculateFreeEnergyDifferenceFor1MPerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLAR);
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyDifferenceToPhase1M(String moleculeID, String toPhase){
		ArrayList<EnergyValue> freeEnergyDifferenceList = new ArrayList<>();
		for (String fromPhase : getPhaseMap().keySet()) {
			freeEnergyDifferenceList.add(calculateFreeEnergyDifferenceValue1M(moleculeID, fromPhase, toPhase));			
		}
		return freeEnergyDifferenceList;
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyDifferenceBetweenAllPhases1M(String moleculeID){
		ArrayList<EnergyValue> completeFreeEnergyDifferenceList = new ArrayList<>();
		for (String toPhase : getPhaseMap().keySet()) {
			completeFreeEnergyDifferenceList.addAll(calculateFreeEnergyDifferenceToPhase1M(moleculeID, toPhase));
		}
		return completeFreeEnergyDifferenceList;
	}

	@Override
	public double calculateFreeEnergySolvation(String moleculeID, String phaseID){
		return getPhaseMap().get(phaseID).calculateEnergyInPhase(moleculeID);
	}

	@Override 
	public ArrayRealVector calculateFreeEnergySolvationPerSSIP(String moleculeID, String phaseID){
		return getPhaseMap().get(phaseID).calculateEnergyInPhaseBySSIP(moleculeID);
	}

	@Override
	public EnergyValue calculateFreeEnergySolvationEnergyValue(String moleculeID, String phaseID){
		LOG.debug("free energy per SSIP: {}", calculateFreeEnergySolvationPerSSIP(moleculeID, phaseID));
		return new EnergyValue(moleculeID, phaseID, "", calculateFreeEnergySolvation(moleculeID, phaseID), calculateFreeEnergySolvationPerSSIP(moleculeID, phaseID), EnergyValueType.MOLEFRACTION);
	}

	@Override
	public List<EnergyValue> calculateFreeEnergySolvationAllPhases(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (String phaseID : getPhaseMap().keySet()) {
			energyValues.add(calculateFreeEnergySolvationEnergyValue(moleculeID, phaseID));
		}
		return energyValues;
	}

	@Override
	public List<EnergyValue> calculateFreeEnergyValuesAllPhasesAllValues(String moleculeID){
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		energyValues.addAll(calculateFreeEnergySolvationAllPhases(moleculeID));
		energyValues.addAll(calculateFreeEnergyDifferenceBetweenAllPhases(moleculeID));
		energyValues.addAll(calculateFreeEnergyDifferenceBetweenAllPhases1M(moleculeID));
		return energyValues;
	}
	
	@Override
	public List<EnergyValue> calculateLogPAllPhases(String moleculeID){
		ArrayList<EnergyValue> logPvalues = new ArrayList<>();
		for (String toPhase : getPhaseMap().keySet()) {
			for (String fromPhase : getPhaseMap().keySet()) {
				logPvalues.add(calculateLogPValue(moleculeID, fromPhase, toPhase));
			}
		}
		return logPvalues;
	}
	
	@Override
	public EnergyValue calculateLogPValue(String moleculeID, String fromPhase, String toPhase) {
		return new EnergyValue(moleculeID, toPhase, fromPhase, calculateLogP(moleculeID, fromPhase, toPhase), calculateFreeEnergyDifferenceFor1MPerSSIP(moleculeID, fromPhase, toPhase), EnergyValueType.MOLAR);
	}
	
	@Override
	public double calculateLogP(String moleculeID, String fromPhase, String toPhase) {
		double energy1M = calculateFreeEnergyDifferenceFor1M(moleculeID, fromPhase, toPhase);
		double rT = Constants.GAS_CONSTANT * getPhaseMap().get(fromPhase).getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue();
		double logeto10conversion = Math.log10(Math.E);
		LOG.debug("conversion factor: {}", logeto10conversion);
		return -(energy1M*logeto10conversion)/rT;
	}

	@Override
	public double calculateConversionFactorTo1MScale(String toPhase, String fromPhase){
		double factorToPhase = getPhaseMap().get(toPhase).calculateConversionTo1MScale();
		double factorFromPhase = getPhaseMap().get(fromPhase).calculateConversionTo1MScale();
		return factorToPhase - factorFromPhase;
	}
	@Override
	public List<AssociationEnergyValue> calculateAssociationEnergyValuesAllPhases() {
		ArrayList<AssociationEnergyValue> associationEnergyValues = new ArrayList<>();
		LOG.info("calculating association energies");
		for (Map.Entry<String, Phase> phaseEntry : getPhaseMap().entrySet()) {
			Phase phase = phaseEntry.getValue();
			LOG.debug("phase Entry String: {}", phaseEntry.getKey());
			LOG.debug("phase solvent ID: {}", phase.getSolventID());
			associationEnergyValues.addAll(phase.calculateMoleculeAssociationEnergyValues());
		}
		return associationEnergyValues;
	}
	
	@Override
	public List<PhaseConcentrationFraction> calculatePhaseConcentrationFractions() {
		ArrayList<PhaseConcentrationFraction> phaseConcentrationFractions = new ArrayList<>();
		for (Phase phase : getPhaseMap().values()) {
			phaseConcentrationFractions.add(phase.calculatePhaseConcentrationFraction());
		}
		return phaseConcentrationFractions;
	}

	/**
	 * Set Association Constants upon unmarshalling.
	 */
	@Override
	public void setAssociationConstants(){
		for (Map.Entry<String, Phase> phaseMapEntry : getPhaseMap().entrySet()) {
			phaseMapEntry.getValue().setAssociationConstants();
		}
	}
	/**
	 * Set bound concentration {@link ArrayRealVector} upon unmarshalling, using the {@link ArrayList} of {@link BoundConcentration}s.
	 */
	@Override
	public void setBoundConcentrations(){
		for (Map.Entry<String, Phase> phaseMapEntry : getPhaseMap().entrySet()) {
			phaseMapEntry.getValue().setBoundConcentrations();
		}
	}
	
	@Override
	public Map<String, Phase> getPhaseMap() {
		return phaseMap;
	}

	@Override
	public void setPhaseMap(Map<String, Phase> phaseMap) {
		this.phaseMap = (HashMap<String, Phase>) phaseMap;
	}

	public String getSoluteID() {
		return soluteID;
	}

	public void setSoluteID(String soluteID) {
		this.soluteID = soluteID;
	}
	
	@Override
	public String getMixtureID(){
		return getSoluteID();		
	}
	
	@Override
	public void setMixtureID(String mixtureID){
		setSoluteID(mixtureID);
	}

	@Override
	public FractionalOccupancyCollection getFractionalOccupancyCollection() {
		HashSet<FractionalOccupancy> fractionalOccupancyCollection = new HashSet<>();
		String solID = getSoluteID() != null ? getSoluteID() : "";
		for (Phase phase : getPhaseMap().values()) {
			LOG.info("phase solventID: {}", phase.getSolventID());
			LOG.info("phase fractionalOccupancy: {}", phase.getFractionalOccupancy(solID));
			fractionalOccupancyCollection.add(phase.getFractionalOccupancy(solID));
		}
		return new FractionalOccupancyCollection(fractionalOccupancyCollection);
	}

	@Override
	public List<EnergyValue> calculateFreeEnergySolvationAllMoleculesEveryPhase() {
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (Map.Entry<String, Phase> phaseEntry : getPhaseMap().entrySet()) {
			LOG.debug("phase molecule keys: {}");
			for (String moleculeID : phaseEntry.getValue().getPhaseMolecules().keySet()) {
				energyValues.add(calculateFreeEnergySolvationEnergyValue(moleculeID, phaseEntry.getKey()));
			}
		}
		return energyValues;
	}

	@Override
	public List<EnergyValue> calculateBindingEnergySolvationAllMoleculesEveryPhase() {
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (Map.Entry<String, Phase> phaseEntry : getPhaseMap().entrySet()) {
			for (String moleculeID : phaseEntry.getValue().getPhaseMolecules().keySet()) {
				energyValues.add(calculateBindingEnergySolvationEnergyValue(moleculeID, phaseEntry.getKey()));
			}
		}
		return energyValues;
	}

	@Override
	public List<EnergyValue> calculateConfinementEnergySolvationAllMoleculesEveryPhase() {
		ArrayList<EnergyValue> energyValues = new ArrayList<>();
		for (Map.Entry<String, Phase> phaseEntry : getPhaseMap().entrySet()) {
			for (String moleculeID : phaseEntry.getValue().getPhaseMolecules().keySet()) {
				energyValues.add(calculateConfinementEnergySolvationEnergyValue(moleculeID, phaseEntry.getKey()));
			}
		}
		return energyValues;
	}
}
