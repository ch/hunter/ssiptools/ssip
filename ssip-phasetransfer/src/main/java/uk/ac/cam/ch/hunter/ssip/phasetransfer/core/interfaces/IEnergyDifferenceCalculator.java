/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.interfaces;

import java.util.List;

import org.apache.commons.math3.linear.ArrayRealVector;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.AssociationEnergyValue;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.EnergyValue;

/**
 * Interface for calculating the solvation or transfer energies of molecules between phases within the given class.
 * 
 * @author mdd31
 *
 */
public interface IEnergyDifferenceCalculator {

	/**
	 * This calculates the confinement energy difference between the two phases for the given molecule: this is the change going from fromPhase to toPhase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return confinement energy difference
	 */
	public double calculateConfinementEnergyDifference(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the contribution to the confinement energy difference between the two phases for the given molecule from each SSIP: this is the change going from fromPhase to toPhase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return confinement energy difference contribution
	 */
	public ArrayRealVector calculateConfinementEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the confinement energy difference between the two phases for the given molecule: this is the change going from fromPhase to toPhase, and returning the EnergyValue object of it.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return confinement energy difference
	 */
	public EnergyValue calculateConfinementEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * List containing the confinement energy differences, as EnergyValue entries.
	 * 
	 * @param moleculeID molecule ID.
	 * @param toPhase to phase.
	 * @return confinement energy differences
	 */
	public List<EnergyValue> calculateConfinementEnergyDifferenceToPhase(String moleculeID, String toPhase);
	
	/**
	 * List where the energy value entries are the confinement energy change of the given molecule between the given phases.
	 * 
	 * @param moleculeID molecule ID.
	 * @return confinement energy differences
	 */
	public List<EnergyValue> calculateConfinementEnergyDifferenceBetweenAllPhases(String moleculeID);
	
	/**
	 * This calculates the change in Confinement Energy upon transfer in the 1M scale.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return confinement energy difference
	 */
	public double calculateConfinementEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the change in Confinement Energy upon transfer in the 1M scale per SSIP.
	 * This is the same as in the molefraction scale.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return confinement energy difference contribution
	 */
	public ArrayRealVector calculateConfinementEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the confinement energy difference for 1M between the two phases for the given molecule: this is the change going from fromPhase to toPhase, and returning the EnergyValue object of it.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return confinement energy difference
	 */
	public EnergyValue calculateConfinementEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * List containing the confinement energy differences for 1M, as EnergyValue entries.
	 * 
	 * @param moleculeID molecule ID.
	 * @param toPhase to phase.
	 * @return confinement energy differences
	 */
	public List<EnergyValue> calculateConfinementEnergyDifferenceToPhase1M(String moleculeID, String toPhase);
	
	/**
	 * List where the energy value entries are the confinement energy change for 1M of the given molecule between the given phases.
	 * 
	 * @param moleculeID molecule ID.
	 * @return confinement energy differences
	 */
	public List<EnergyValue> calculateConfinementEnergyDifferenceBetweenAllPhases1M(String moleculeID);
	
	/**
	 * This is the confinement energy contribution to the solvation energy for the given molecule in the given phase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return confinement energy
	 */
	public double calculateConfinementEnergySolvation(String moleculeID, String phaseID);
	
	/**
	 * This is the solvation confinement energy contribution from each SSIP for the given molecule in the given phase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return confinement energy contributions
	 */
	public ArrayRealVector calculateConfinementEnergySolvationPerSSIP(String moleculeID, String phaseID);
	
	/**
	 * This is the confinement energy contribution to the solvation energy for the given molecule in the given phase as an {@link EnergyValue}.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return confinement energy
	 */
	public EnergyValue calculateConfinementEnergySolvationEnergyValue(String moleculeID, String phaseID);
	
	/**
	 * This is the confinement energy contribution to the solvation energy for the given molecule in all phases in the mixture.
	 * 
	 * @param moleculeID molecule ID.
	 * @return confinement energies
	 */
	public List<EnergyValue> calculateConfinementEnergySolvationAllPhases(String moleculeID);
	
	/**
	 * This calculates the Confinement energy values for all solvations, and all phase transfer in the mole fraction and molar scales, appending them to the outputted list in the given order.
	 * 
	 * @param moleculeID molecule ID.
	 * @return confinement energies
	 */
	public List<EnergyValue> calculateConfinementEnergyValuesAllPhasesAllValues(String moleculeID);
	
	/**
	 * This calculates the free energy of binding difference between the two phases for the given molecule: this is the change going from fromPhase to toPhase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return binding energy difference
	 */
	public double calculateBindingEnergyDifference(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the contribution to the binding energy difference between the two phases for the given molecule from each SSIP: this is the change going from fromPhase to toPhase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return binding energy difference contribution
	 */
	public ArrayRealVector calculateBindingEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the binding energy difference between the two phases for the given molecule: this is the change going from fromPhase to toPhase, and returning the EnergyValue object of it.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return binding energy difference
	 */
	public EnergyValue calculateBindingEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * List containing the binding energy differences, as EnergyValue entries.
	 * 
	 * @param moleculeID molecule ID.
	 * @param toPhase to phase.
	 * @return binding energy differences
	 */
	public List<EnergyValue> calculateBindingEnergyDifferenceToPhase(String moleculeID, String toPhase);
	
	/**
	 * List where the energy value entries are the binding energy change of the given molecule between the given phases.
	 * 
	 * @param moleculeID molecule ID.
	 * @return binding energy differences
	 */
	public List<EnergyValue> calculateBindingEnergyDifferenceBetweenAllPhases(String moleculeID);
	
	/**
	 * This calculates the change in Binding Energy upon transfer in the 1M scale.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return binding energy difference
	 */
	public double calculateBindingEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the change in Binding Energy upon transfer in the 1M scale per SSIP.
	 * This is the same as in the molefraction scale.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return binding energy difference contributions
	 */
	public ArrayRealVector calculateBindingEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the binding energy difference for 1M between the two phases for the given molecule: this is the change going from fromPhase to toPhase, and returning the EnergyValue object of it.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return binding energy difference
	 */
	public EnergyValue calculateBindingEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * List containing the binding energy differences for 1M, as EnergyValue entries.
	 * 
	 * @param moleculeID molecule ID.
	 * @param toPhase to phase.
	 * @return binding energy differences
	 */
	public List<EnergyValue> calculateBindingEnergyDifferenceToPhase1M(String moleculeID, String toPhase);
	
	/**
	 * List where the energy value entries are the binding energy change for 1M of the given molecule between the given phases.
	 * 
	 * @param moleculeID molecule ID.
	 * @return binding energy differences
	 */
	public List<EnergyValue> calculateBindingEnergyDifferenceBetweenAllPhases1M(String moleculeID);
	
	/**
	 * This is the binding energy contribution to the solvation energy for the given molecule in the given phase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return binding energy
	 */
	public double calculateBindingEnergySolvation(String moleculeID, String phaseID);
	
	/**
	 * This is the solvation binding energy contribution from each SSIP for the given molecule in the given phase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return binding energy contributions
	 */
	public ArrayRealVector calculateBindingEnergySolvationPerSSIP(String moleculeID, String phaseID);
	
	/**
	 * This is the binding energy contribution to the solvation energy for the given molecule in the given phase as an {@link EnergyValue}.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return binding energy
	 */
	public EnergyValue calculateBindingEnergySolvationEnergyValue(String moleculeID, String phaseID);
	
	/**
	 * This is the binding energy contribution to the solvation energy for the given molecule in all phases in the mixture.
	 * 
	 * @param moleculeID molecule ID.
	 * @return binding energies
	 */
	public List<EnergyValue> calculateBindingEnergySolvationAllPhases(String moleculeID);
	
	/**
	 * This calculates the Binding energy values for all solvations, and all phase transfer in the mole fraction and molar scales, appending them to the outputted list in the given order.
	 * 
	 * @param moleculeID molecule ID.
	 * @return binding energies
	 */
	public List<EnergyValue> calculateBindingEnergyValuesAllPhasesAllValues(String moleculeID);
	
	/**
	 * This calculates the free energy difference between the two phases for the given molecule: this is the change going from phase 1 to phase2.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return free energy difference
	 */
	public double calculateFreeEnergyDifference(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the contribution to the free energy difference between the two phases for the given molecule from each SSIP: this is the change going from fromPhase to toPhase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return free energy difference contributions
	 */
	public ArrayRealVector calculateFreeEnergyDifferencePerSSIP(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the free energy difference between the two phases for the given molecule: this is the change going from fromPhase to toPhase, and returning the EnergyValue object of it.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return free energy difference
	 */
	public EnergyValue calculateFreeEnergyDifferenceValue(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * List containing the free energy differences, as EnergyValue entries.
	 * 
	 * @param moleculeID molecule ID.
	 * @param toPhase to phase.
	 * @return free energy differences
	 */
	public List<EnergyValue> calculateFreeEnergyDifferenceToPhase(String moleculeID, String toPhase);
	
	/**
	 * List where the energy value entries are the free energy change of the given molecule between the given phases.  
	 * 
	 * @param moleculeID molecule ID.
	 * @return free energy differences
	 */
	public List<EnergyValue> calculateFreeEnergyDifferenceBetweenAllPhases(String moleculeID);
	
	/**
	 * This calculates the change in Free Energy upon transfer in the 1M scale.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return free energy difference
	 */
	public double calculateFreeEnergyDifferenceFor1M(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the change in Free Energy upon transfer in the 1M scale per SSIP.
	 * This is the same as in the molefraction scale.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return free energy difference contributions
	 */
	public ArrayRealVector calculateFreeEnergyDifferenceFor1MPerSSIP(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * This calculates the free energy difference for 1M between the two phases for the given molecule: this is the change going from fromPhase to toPhase, and returning the EnergyValue object of it.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return free energy difference
	 */
	public EnergyValue calculateFreeEnergyDifferenceValue1M(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * List containing the free energy differences for 1M, as EnergyValue entries.
	 * 
	 * @param moleculeID molecule ID.
	 * @param toPhase to phase.
	 * @return free energy differences
	 */
	public List<EnergyValue> calculateFreeEnergyDifferenceToPhase1M(String moleculeID, String toPhase);
	
	/**
	 * List where the energy value entries are the free energy change for 1M of the given molecule between the given phases.  
	 * 
	 * @param moleculeID molecule ID.
	 * @return free energy differences
	 */
	public List<EnergyValue> calculateFreeEnergyDifferenceBetweenAllPhases1M(String moleculeID);
	
	/**
	 * This is the solvation energy for the given molecule in the given phase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return free energy
	 */
	public double calculateFreeEnergySolvation(String moleculeID, String phaseID);
	
	/**
	 * This is the solvation energy contribution from each SSIP for the given molecule in the given phase.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return free energy contributions
	 */
	public ArrayRealVector calculateFreeEnergySolvationPerSSIP(String moleculeID, String phaseID);
	
	/**
	 * This is the solvation energy for the given molecule in the given phase as an {@link EnergyValue}.
	 * 
	 * @param moleculeID molecule ID.
	 * @param phaseID phase ID.
	 * @return free energy
	 */
	public EnergyValue calculateFreeEnergySolvationEnergyValue(String moleculeID, String phaseID);
	
	/**
	 * This is the solvation energy for the given molecule in all phases in the mixture.
	 * 
	 * @param moleculeID molecule ID.
	 * @return free energies
	 */
	public List<EnergyValue> calculateFreeEnergySolvationAllPhases(String moleculeID);
	
	/**
	 * This calculates the free energy values for all solvations, and all phase transfer in the mole fraction and molar scales, appending them to the outputted list in the given order.
	 * 
	 * @param moleculeID molecule ID.
	 * @return free energies
	 */
	public List<EnergyValue> calculateFreeEnergyValuesAllPhasesAllValues(String moleculeID);
	
	/**
	 * Calculate LogP between all phases.
	 * 
	 * @param moleculeID molecule ID.
	 * @return logP values.
	 */
	public List<EnergyValue> calculateLogPAllPhases(String moleculeID);
	
	/**
	 * Calculate log of partition coefficient.
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return logP value.
	 */
	public EnergyValue calculateLogPValue(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * Calculate log of partition coefficient. 
	 * 
	 * @param moleculeID molecule ID.
	 * @param fromPhase from phase.
	 * @param toPhase to phase.
	 * @return logP value.
	 */
	public double calculateLogP(String moleculeID, String fromPhase, String toPhase);
	
	/**
	 * calculate the ConversionFactor between the two phases.
	 * 
	 * @param toPhase to phase.
	 * @param fromPhase from phase.
	 * @return conversion factor
	 */
	public double calculateConversionFactorTo1MScale(String toPhase, String fromPhase);

	/**
	 * This returns the association energy values for all the possible molecular associations in all phases.
	 * 
	 * @return List of {@link AssociationEnergyValue}
	 */
	public List<AssociationEnergyValue> calculateAssociationEnergyValuesAllPhases();
}
