/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.math3.linear.ArrayRealVector;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.adapters.EnergyContributionsAdapter;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

/**
 * Class for storing the information about an energy of transfer, with the
 * molecule it corresponds to, the phase it has come from and the phase it is
 * going to.
 * 
 * @author mdd31
 * 
 */
@XmlRootElement(name = "EnergyValue", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnergyValue {

	@XmlTransient
	private static final double ERROR = 1e-7;

	@XmlAttribute(name = "moleculeID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String moleculeID;
	@XmlAttribute(name = "toSolventID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String toPhaseID;
	@XmlAttribute(name = "fromSolventID", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final String fromPhaseID;
	@XmlElement(name ="TotalEnergy", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final double value;
	@XmlAttribute(name ="valueType", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	private final EnergyValueType energyValueType;
	@XmlElement(name = "EnergyContributions", namespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema")
	@XmlJavaTypeAdapter(EnergyContributionsAdapter.class)
	private final ArrayRealVector contributionBySSIP;

	/**
	 * Hash value of the object. Used in HashMaps and HashSets.
	 */
	@XmlTransient
	private int hash;
	@XmlTransient
	private boolean hashCodeFound = false;

	/**
	 * constructor for JAXB
	 */
	public EnergyValue(){
		this.moleculeID = null;
		this.toPhaseID = null;
		this.fromPhaseID = null;
		this.value = Double.NaN;
		this.contributionBySSIP = null;
		this.energyValueType = null;
	}
	
	/**
	 * Constructor
	 * 
	 * @param moleculeID Molecule ID.
	 * @param toPhaseID Phase ID going to.
	 * @param fromPhaseID Phase ID coming from.
	 * @param value Value.
	 * @param contributionBySSIP Contribution By SSIP.
	 * @param energyValueType Energy value type.
	 */
	public EnergyValue(String moleculeID, String toPhaseID, String fromPhaseID,
			double value, ArrayRealVector contributionBySSIP,
			EnergyValueType energyValueType) {
		this.moleculeID = moleculeID;
		this.toPhaseID = toPhaseID;
		this.fromPhaseID = fromPhaseID;
		this.value = value;
		this.contributionBySSIP = contributionBySSIP;
		this.energyValueType = energyValueType;
	}
	
	@Override
	public String toString() {
		return String
				.format("moleculeID: %s to phase: %s from phase: %s value: %f type: %s",
						getMoleculeID(), getToPhaseID(), getFromPhaseID(),
						getValue(), getEnergyValueType().getValueType());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnergyValue other = (EnergyValue) obj;
		if (this.getMoleculeID() == other.getMoleculeID()
				&& this.getToPhaseID() == other.getToPhaseID()
				&& this.getFromPhaseID() == other.getFromPhaseID()) {
			return VectorUtils.compareVectorsEqualAbsoluteTolerance(this.getContributionBySSIP(), other.getContributionBySSIP(), ERROR) && Math.abs(this.getValue() - other.getValue()) < ERROR
					&& this.getEnergyValueType() == other.getEnergyValueType();
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		if (!hashCodeFound) {
			final int prime = 23;
			int result = 1;
			result = prime * result + getFromPhaseID().hashCode()
					+ getMoleculeID().hashCode() + getToPhaseID().hashCode()
					+ getEnergyValueType().hashCode();
			hash = result;
			hashCodeFound = true;
		}
		return hash;
	}

	public String getMoleculeID() {
		return moleculeID;
	}

	public String getToPhaseID() {
		return toPhaseID;
	}

	public String getFromPhaseID() {
		return fromPhaseID;
	}

	public double getValue() {
		return value;
	}

	public EnergyValueType getEnergyValueType() {
		return energyValueType;
	}

	public ArrayRealVector getContributionBySSIP() {
		return contributionBySSIP;
	}
}
