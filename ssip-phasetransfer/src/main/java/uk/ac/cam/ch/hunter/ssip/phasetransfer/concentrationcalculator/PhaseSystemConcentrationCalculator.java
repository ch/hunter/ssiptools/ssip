/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstantsFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Constants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseType;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

/**
 * Calculator class to generate the speciation of the liquid and gas phase.
 * 
 * @author mdd31
 *
 */
public class PhaseSystemConcentrationCalculator {

	private static final Logger LOG = LogManager.getLogger(PhaseSystemConcentrationCalculator.class);
	
	private final double temperature; 

	private boolean speciationCalculated;
	
	private LiquidTotalConcentrationCalculator liquidTotalConcentrationCalculator;
	
	private GasPhaseConcentrationCalculator gasConcentrationMatcher;
	
	private Phase liquidPhase;
	
	private Phase gasPhase;
	
	/**
	 * Constructor takes information from liquid phase, creating a gas phase, and extracts the 
	 * 
	 * @param liquidPhase liquid phase
	 */
	public PhaseSystemConcentrationCalculator(Phase liquidPhase){
		this.temperature = liquidPhase.getTemperature().convertTo(TemperatureUnit.KELVIN).getTemperatureValue();
		this.liquidPhase = liquidPhase;
		speciationCalculated = false;
		
		initialiseLiquidPhase();
		initialiseGasPhase();
	}
	
	private void initialiseGasPhase(){
		this.gasPhase = new Phase(liquidPhase);
		this.gasPhase.setPhaseType(PhaseType.GAS);
		this.gasPhase.setSolventID(liquidPhase.getSolventID());
		AssociationConstantsFactory associationConstantsFactory = new AssociationConstantsFactory(temperature);
		AssociationConstants associationConstants = associationConstantsFactory.getAssociationConstants(gasPhase.getsortedSsips());
		ArrayRealVector initialConcentrations = calculateGasTotalConcentrations((ArrayRealVector) getLiquidEnergyBySSIP().mapMultiply(-1.0));
		LOG.trace("initial gas conc: {}", initialConcentrations.toArray());
		ConcentrationCalculator concentrationCalculator = new ConcentrationCalculator(associationConstants.getAssociationConstants(), initialConcentrations);
		concentrationCalculator.calculateFreeAndBoundConcentrations();
		this.gasConcentrationMatcher = new GasPhaseConcentrationCalculator(concentrationCalculator, gasPhase.getsortedSsips());
		this.gasConcentrationMatcher.setConcentrations();
		LOG.trace("total gas conc matcher: {}", getGasConcentrationMatcher().getConcentrationCalculator().getTotalConcentrations().toArray());
		LOG.trace("gas conc matcher SSIPs: {}", getGasConcentrationMatcher().getConcentrationsByMolID());
		LOG.trace("total gas concs before setting: {}", getGasPhase().getTotalConcentrations().toArray());
		LOG.trace("free gas concs before setting: {}", getGasPhase().getFreeConcentrations().toArray());
		setGasPhaseSsipConcentrations();
		LOG.trace("total gas concs after setting: {}", getGasPhase().getTotalConcentrations().toArray());
		LOG.trace("free gas concs: {}", getGasPhase().getFreeConcentrations().toArray());
		LOG.trace("bound gas concs: {}", getGasPhase().getBoundConcentrations().toArray());
	}
	
	private void initialiseLiquidPhase(){
		liquidPhase.setPhaseType(PhaseType.CONDENSED);
		liquidTotalConcentrationCalculator = new LiquidTotalConcentrationCalculator(liquidPhase, temperature);
		liquidTotalConcentrationCalculator.calculateTotalConcentrations();
		calculateLiquidSpeciationSetPhase();
	}
	
	/**
	 * This returns a {@link PhaseSystem} with speciation calculated for both phases.
	 * 
	 * @param mixtureID mixture ID
	 * 
	 * @return phase system
	 */
	public PhaseSystem getPhaseSystem(String mixtureID){
		calculateSpeciation();
		return new PhaseSystem(getGasPhase(), getLiquidPhase(), mixtureID);
	}
	
	/**
	 * This calculates the speciation in the gas and liquid phases, such that the total concentrations are self consistent.
	 */
	public void calculateSpeciation(){
		ArrayRealVector trialGasConcs = gasPhase.getTotalConcentrations();
		ArrayRealVector phiBoundGas;
		if (!speciationCalculated) {
			calculateLiquidSpeciationSetPhase();
			//Gas phase is set equal to tolerance in first step.
			trialGasConcs = calculateGasTotalConcentrations(calculateEnergyDifferenceBetweenPhases());
			LOG.trace("trial gas concs init: {}", trialGasConcs.toArray());
			speciationCalculated = concentrationsMatchGas(trialGasConcs);
		}
		int speciationLoop = 0;
		//after initial calculation of gas phase concentrations we then iterate to get self consistent concentration, reevaluating liquid concentration at each step.
		while (!speciationCalculated) {
			speciationLoop+=1;
			LOG.info("speciation loop: {}", speciationLoop);
			gasConcentrationMatcher.setConcentrationCalculator(new ConcentrationCalculator(gasConcentrationMatcher.getConcentrationCalculator().getAssociationConstants(), trialGasConcs));
			calculateGasSpeciationSetPhase();
			phiBoundGas = calculatePhiBoundGas();
			LOG.trace("trial gas concs1: {}", trialGasConcs.toArray());
			trialGasConcs = calculateGasTotalConcentrations(calculateEnergyDifferenceBetweenPhases());
			LOG.trace("trial gas concs2: {}", trialGasConcs.toArray());
			liquidTotalConcentrationCalculator.setGasConcentrations(trialGasConcs, phiBoundGas);
			calculateLiquidSpeciationSetPhase();
			trialGasConcs = calculateGasTotalConcentrations(calculateEnergyDifferenceBetweenPhases());
			LOG.trace("trial gas concs3: {}", trialGasConcs.toArray());
			speciationCalculated = concentrationsMatchGas(trialGasConcs);
		}
	}
	
	private ArrayRealVector calculatePhiBoundGas() {
		double pvdWFreeGas = calculatePvdWFreeGas();
		ArrayRealVector psiFreeGas = gasConcentrationMatcher.getConcentrationCalculator().getPsiFreePerMolecule(getLiquidTotalConcentrationCalculator().getMoleculeIDs());
		LOG.trace("PvdWFree: {}", pvdWFreeGas);
		LOG.trace("PsiFree: {}", psiFreeGas);
		return psiFreeGas.map(new PhiBoundCalculator(pvdWFreeGas));
	}

	private double calculatePvdWFreeGas() {
		return (Math.sqrt(1.0 + 8.0 * liquidTotalConcentrationCalculator.getKVdW() * gasConcentrationMatcher.getConcentrationCalculator().getTotalConcentration()) - 1.0)/(4.0 * liquidTotalConcentrationCalculator.getKVdW() * gasConcentrationMatcher.getConcentrationCalculator().getTotalConcentration());
	}

	private void setGasPhaseSsipConcentrations(){
		HashMap<String, ArrayList<SsipConcentration>> gasConcsByID = (HashMap<String, ArrayList<SsipConcentration>>) gasConcentrationMatcher.getConcentrationsByMolID();
		LOG.debug("gasConcsbyMolID: {}", gasConcsByID);
		for (Map.Entry<String, ArrayList<SsipConcentration>> molMapEntry : gasConcsByID.entrySet()) {
			gasPhase.getPhaseMolecules().get(molMapEntry.getKey()).getSsipConcentrationContainer().setSsipConcentrations(molMapEntry.getValue());
		}
	}
	
	public Map<String, Double> getGasEnergyByMolID(){
		ArrayList<String> molIDs = new ArrayList<>(gasPhase.getPhaseMolecules().keySet());
		HashMap<String, Double> gasEnergyByMolIDs = new HashMap<>();
		for (String moleculeID : molIDs) {
			LOG.trace("gas moleculeID: {} binding: {} confinement: {} total: {} theta:{}", moleculeID, gasPhase.calculateBindingEnergy(moleculeID), gasPhase.calculateConfinementEnergy(moleculeID), gasPhase.calculateEnergyInPhase(moleculeID), gasPhase.calculateFractionalOccupancy());
			gasEnergyByMolIDs.put(moleculeID, gasPhase.calculateEnergyInPhase(moleculeID));
		}
		return gasEnergyByMolIDs;
	}
	
	public ArrayRealVector getGasEnergyBySSIP(){
		HashMap<String, Double> gasEnergyByMolIDs = (HashMap<String, Double>) getGasEnergyByMolID();
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getGasPhase().getsortedSsips();
		ArrayRealVector gasEnergies = new ArrayRealVector(ssipConcentrations.size());
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			gasEnergies.setEntry(i, gasEnergyByMolIDs.get(ssipConcentrations.get(i).getMoleculeID()));
		}
		return gasEnergies;
	}

	private void setLiquidPhaseSsipConcentrations(){
		HashMap<String, ArrayList<SsipConcentration>> liquidConcsByID = (HashMap<String, ArrayList<SsipConcentration>>) liquidTotalConcentrationCalculator.calculateTotalConcsByMolecule();
		for (Map.Entry<String, ArrayList<SsipConcentration>> molMapEntry : liquidConcsByID.entrySet()) {
			liquidPhase.getPhaseMolecules().get(molMapEntry.getKey()).getSsipConcentrationContainer().setSsipConcentrations(molMapEntry.getValue());
		}
	}
	
	public Map<String, Double> getLiquidEnergyByMolID(){
		ArrayList<String> molIDs = new ArrayList<>(liquidPhase.getPhaseMolecules().keySet());
		HashMap<String, Double> liquidEnergyByMolIDs = new HashMap<>();
		for (String moleculeID : molIDs) {
			LOG.trace("Liquid moleculeID: {} binding: {} confinement: {} total: {}", moleculeID, liquidPhase.calculateBindingEnergy(moleculeID), liquidPhase.calculateConfinementEnergy(moleculeID), liquidPhase.calculateEnergyInPhase(moleculeID));
			liquidEnergyByMolIDs.put(moleculeID, liquidPhase.calculateEnergyInPhase(moleculeID));
		}
		return liquidEnergyByMolIDs;
	}
	
	public ArrayRealVector getLiquidEnergyBySSIP(){
		HashMap<String, Double> liquidEnergyByMolIDs = (HashMap<String, Double>) getLiquidEnergyByMolID();
		ArrayList<SsipConcentration> ssipConcentrations = (ArrayList<SsipConcentration>) getLiquidPhase().getsortedSsips();
		ArrayRealVector liquidEnergies = new ArrayRealVector(ssipConcentrations.size());
		for (int i = 0; i < ssipConcentrations.size(); i++) {
			liquidEnergies.setEntry(i, liquidEnergyByMolIDs.get(ssipConcentrations.get(i).getMoleculeID()));
		}
		return liquidEnergies;
	}
	
	/**
	 * Transfer energy to gas phase.
	 * 
	 * @return energy differences
	 */
	public ArrayRealVector calculateEnergyDifferenceBetweenPhases(){
		ArrayRealVector gasEnergies = getGasEnergyBySSIP();
		ArrayRealVector liquidEnergies = getLiquidEnergyBySSIP();
		LOG.trace("gas energy: {}", gasEnergies);
		LOG.trace("liquid energy: {}",  liquidEnergies);
		return gasEnergies.subtract(liquidEnergies);
	}
	
	
	/**
	 * Check to see if concentrations match those as expected.
	 * 
	 * @param trialConcentrations trial concentrations
	 * @return gas concentrations match
	 */
	private boolean concentrationsMatchGas(ArrayRealVector trialConcentrations){
		return VectorUtils.compareVectorsEqualAbsoluteTolerance(trialConcentrations, gasPhase.getTotalConcentrations(), Constants.GAS_TOLERANCE);
	}
	
	ArrayRealVector calculateGasTotalConcentrations(ArrayRealVector energydifferences){
		ArrayRealVector exponent = (ArrayRealVector) energydifferences.mapDivide(Constants.GAS_CONSTANT * getTemperature());
		ArrayRealVector exponential = new ArrayRealVector(exponent.getDimension());
		for (int i = 0; i < exponent.getDimension(); i++) {
			exponential.setEntry(i, Math.exp(-exponent.getEntry(i)));
		}
		ArrayRealVector totalSystemConcentrations = liquidTotalConcentrationCalculator.calculateTrialConcentration();
		return (ArrayRealVector) totalSystemConcentrations.ebeMultiply(exponential).mapMultiply(0.5);
	}
	
	void calculateLiquidSpeciationSetPhase(){
		liquidTotalConcentrationCalculator.calculateTotalConcentrations();
		setLiquidPhaseSsipConcentrations();
	}
	
	void calculateGasSpeciationSetPhase(){
		gasConcentrationMatcher.setConcentrations();
		setGasPhaseSsipConcentrations();
	}
	public double getTemperature() {
		return temperature;
	}

	public LiquidTotalConcentrationCalculator getLiquidTotalConcentrationCalculator() {
		return liquidTotalConcentrationCalculator;
	}

	public void setLiquidTotalConcentrationCalculator(
			LiquidTotalConcentrationCalculator liquidTotalConcentrationCalculator) {
		this.liquidTotalConcentrationCalculator = liquidTotalConcentrationCalculator;
	}

	public GasPhaseConcentrationCalculator getGasConcentrationMatcher() {
		return gasConcentrationMatcher;
	}

	public void setGasConcentrationMatcher(
			GasPhaseConcentrationCalculator gasConcentrationMatcher) {
		this.gasConcentrationMatcher = gasConcentrationMatcher;
	}

	public Phase getLiquidPhase() {
		return liquidPhase;
	}

	public void setLiquidPhase(Phase liquidPhase) {
		this.liquidPhase = liquidPhase;
	}

	public Phase getGasPhase() {
		return gasPhase;
	}

	public void setGasPhase(Phase gasPhase) {
		this.gasPhase = gasPhase;
	}
}
