/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.temperature;

import java.text.DecimalFormat;

import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVectorFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Constants;

/**
 * Class containing methods for the calculation of Interaction energies between SSIPs, including the temperature dependence.
 * 
 * @author mdd31
 *
 */
public class InteractionEnergyCalculator {

	private static final Logger LOG = LogManager.getLogger(InteractionEnergyCalculator.class);
	
	private static final RealVectorFormat VECTOR_FORMAT = new RealVectorFormat("{", "}", ",", new DecimalFormat("+#,##0.000;-#"));
	
	static final SplineInterpolator SPLINE_INTERPOLATOR = new SplineInterpolator();
	
	private static final PolynomialSplineFunction INTERACTION_ENERGY_SPLINE = calculateInteractionEnergySpline();
	
	private InteractionEnergyCalculator(){
		/**
		 * Private Constructor as class possesses only static methods.
		 */
	}
	
	/**
	 * Calculate Interaction energy at desired temperature when the interaction energy at zero kelvin, e0 is given with the temperature of interest.
	 * 
	 * This is in kJmol^{-1}
	 * 
	 * @param e0 Energy at 0K.
	 * @param temperature temperature in Kelvin.
	 * @return interaction energy
	 */
	public static double calculateInteractionEnergy(double e0, double temperature){
		return new InteractionEnergyFunction().value(e0, temperature);
	}
	
	/**
	 * Calculate interaction energy at room temperature.
	 * 
	 * @param e0 energy at 0K.
	 * @return interaction energy RT
	 */
	public static double calculateInteractionEnergyRT(double e0){
		return new InteractionEnergyFunctionRT().value(e0) ;
	}
	
	/**
	 * This calculates the interaction energy at the given temperature when given the interaction energy at 298K, by finding the energy at absolute zero. 
	 * 
	 * @param e298 Energy at 298K.
	 * @param temperature Temperature in Kelvin.
	 * @return interaction energy
	 */
	public static double calculateInteractionEnergyUsingSpline(double e298, double temperature){
		double interactionEnergyAZ = calculateInteractionEnergyAZ(e298);
		return calculateInteractionEnergy(interactionEnergyAZ, temperature);
	}
	
	/**
	 * This calculates the interaction energy at absolute zero given the interaction energy at 298K. 
	 * 
	 * @param e298 Energy at 298K.
	 * @return interaction energy
	 */
	public static double calculateInteractionEnergyAZ(double e298){
		return INTERACTION_ENERGY_SPLINE.value(e298);
	}
	
	/**
	 * This calculates the spline function for the mapping of the interaction energy at 298K to interaction energy at absolute zero. 
	 * 
	 * @return spline function
	 */
	private static PolynomialSplineFunction calculateInteractionEnergySpline(){
		ArrayRealVector interactionEnergiesForFit = interactionEnergyValuesForFit();
		if (LOG.isDebugEnabled()) {
			LOG.debug("interaction energies for fit: {}", VECTOR_FORMAT.format(interactionEnergiesForFit));
		}
		ArrayRealVector interactionEnergies298K = interactionEnergyArrayRT(interactionEnergiesForFit);
		if (LOG.isDebugEnabled()) {
			LOG.debug("interactions energies RT after fit: {}", VECTOR_FORMAT.format(interactionEnergies298K));
		}
		
		//we want the spline function to return the value of E0 so we use the interaction energies at 298K as x value, and E0 value as 
		return calculateSplineFunction(interactionEnergies298K, interactionEnergiesForFit);
	}
	
	/**
	 * This calculates the cubic spline function for the given values.
	 * 
	 * @param x X
	 * @param y Y
	 * @return spline function
	 */
	private static PolynomialSplineFunction calculateSplineFunction(ArrayRealVector x, ArrayRealVector y){
		return SPLINE_INTERPOLATOR.interpolate(x.toArray(), y.toArray());
	}
	
	/**
	 * This calculates the interaction energies at room temperature for the interaction energies at zero Kelvin, for the polynomial spline.
	 * 
	 * @param energyZeroK Energy
	 * @return interaction energies
	 */
	private static ArrayRealVector interactionEnergyArrayRT(ArrayRealVector energyZeroK){
		return energyZeroK.map(new InteractionEnergyFunctionRT());
	}
	
	/**
	 * This generates interaction energies at absolute zero over the range of -400 to 0.
	 * 
	 * @return interaction energies
	 */
	private static ArrayRealVector interactionEnergyValuesForFit(){
		ArrayRealVector interactionEnergies = new ArrayRealVector(801);
		//The range of the value of E_{298} for negative interactions -200 to 0.
		//This corresponds to range of -400 to 0 in
		
		for (int i = 0; i < 801; i++) {
			interactionEnergies.addToEntry(i, ((double) i - 800.0)/2.0);
		}
		LOG.debug("Interactionenergies: {}", interactionEnergies);
		return interactionEnergies;
	}

	public static double getGasConstant() {
		return Constants.GAS_CONSTANT;
	}

	public static double getRoomTemperature() {
		return Constants.ROOM_TEMPERATURE;
	}

	public static PolynomialSplineFunction getInteractionEnergySpline() {
		return INTERACTION_ENERGY_SPLINE;
	}
	
	
}
