/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;


public class ConcentrationCalculatorTest {
	
	private ConcentrationCalculator concentrationCalculator1SSIPSystem;
	private ConcentrationCalculator concentrationCalculator2SSIPSystem;
	private ConcentrationCalculator concentrationCalculator4SSIPSystem;
	private Array2DRowRealMatrix associationConstantValues4SSIPSystem;
	private ArrayRealVector expectedTotalConcentrations4SSIPSystem;
	private static final double ERROR = 1e-14;
	
	@Before
	public void setUp() throws Exception {
		Array2DRowRealMatrix associationConstantValues1SSIPSystem = new Array2DRowRealMatrix(new double[][]{{1.0}});
		ArrayRealVector expectedTotalConcentrations1SSIPSystem = new ArrayRealVector(new double[]{1.0});
		concentrationCalculator1SSIPSystem = new ConcentrationCalculator(associationConstantValues1SSIPSystem, expectedTotalConcentrations1SSIPSystem);
		
		Array2DRowRealMatrix associationConstantValues2SSIPSystem = new Array2DRowRealMatrix(new double[][]{{1.0, 1.0}, {1.0, 1.0}});
		ArrayRealVector expectedTotalConcentrations2SSIPSystem = new ArrayRealVector(new double[]{1.0, 1.0});
		concentrationCalculator2SSIPSystem = new ConcentrationCalculator(associationConstantValues2SSIPSystem, expectedTotalConcentrations2SSIPSystem);
		
		associationConstantValues4SSIPSystem = new Array2DRowRealMatrix(new double[][]{{0.9537159952872262, 2.137865575311679, 121.00046313114248, 121.00046313114248},
				{2.137865575311679, 3.200818570079262, 24.080437731838927, 24.080437731838927},
				{121.00046313114248, 24.080437731838927, 7.517159672e-3, 7.517159672e-3},
				{121.00046313114248, 24.080437731838927, 7.517159672e-3, 7.517159672e-3}});
		
		expectedTotalConcentrations4SSIPSystem = new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0});
		
		concentrationCalculator4SSIPSystem = new ConcentrationCalculator(associationConstantValues4SSIPSystem, expectedTotalConcentrations4SSIPSystem);
		
	}

	@Test
	public void testConcentrationCalculator() {
		ArrayRealVector expectedBoundConcentrationTotals = new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0});
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][]{{0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}});
		assertNotNull(concentrationCalculator4SSIPSystem);
		
		assertArrayEquals(expectedTotalConcentrations4SSIPSystem.toArray(), concentrationCalculator4SSIPSystem.getTotalConcentrations().toArray(), ERROR);
		
		assertEquals(4, concentrationCalculator4SSIPSystem.getAssociationConstants().getRowDimension());
		assertEquals(4, concentrationCalculator4SSIPSystem.getAssociationConstants().getColumnDimension());
		for (int i = 0; i < associationConstantValues4SSIPSystem.getRowDimension(); i++) {
			assertArrayEquals(associationConstantValues4SSIPSystem.getRow(i), concentrationCalculator4SSIPSystem.getAssociationConstants().getRow(i), ERROR);
		}
		
		assertArrayEquals(expectedTotalConcentrations4SSIPSystem.toArray(), concentrationCalculator4SSIPSystem.getFreeConcentrations().toArray(), ERROR);
		
		assertArrayEquals(expectedBoundConcentrationTotals.toArray(), concentrationCalculator4SSIPSystem.getBoundConcentrationsTotal().toArray(), ERROR);
		
		assertEquals(4, concentrationCalculator4SSIPSystem.getBoundConcentrations().getRowDimension());
		assertEquals(4, concentrationCalculator4SSIPSystem.getBoundConcentrations().getColumnDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i), concentrationCalculator4SSIPSystem.getBoundConcentrations().getRow(i), ERROR);
		}
		
	}
	
	@Test
	public void testConcentrationCalculator2(){
		ArrayRealVector expectedFreeConcentration = new ArrayRealVector(new double[]{0.05186667943949074, 0.24548649357294183, 0.3939105639154974, 0.3939105639154974});
		ArrayRealVector expectedBoundConcentrationTotal = new ArrayRealVector(new double[]{9.948133320560622, 9.754513506427159, 9.606089436084389, 9.606089436084389});
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][]{{0.005131282816098672, 0.0544410430503873, 4.944280497347068, 4.944280497347068},
			{0.0544410430503873, 0.3857858185611114, 4.657143322407831, 4.657143322407831},
			{4.944280497347068, 4.657143322407831, 0.002332808164745528, 0.002332808164745528},
			{4.944280497347068, 4.657143322407831, 0.002332808164745528, 0.002332808164745528}});
		ConcentrationCalculator actualConcentrationCalculator = new ConcentrationCalculator(expectedTotalConcentrations4SSIPSystem, expectedFreeConcentration, expectedBoundConcentration);
		
		assertArrayEquals(expectedFreeConcentration.toArray(), actualConcentrationCalculator.getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(expectedBoundConcentrationTotal.toArray(), actualConcentrationCalculator.getBoundConcentrationsTotal().toArray(), ERROR);
		
		assertEquals(4, actualConcentrationCalculator.getBoundConcentrations().getRowDimension());
		assertEquals(4, actualConcentrationCalculator.getBoundConcentrations().getColumnDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i), actualConcentrationCalculator.getBoundConcentrations().getRow(i), ERROR);
		}
		assertTrue(actualConcentrationCalculator.totalConcentrationMatches());
	}
	
	@Test
	public void testGetTotalConcentration(){
		concentrationCalculator4SSIPSystem.calculateFreeAndBoundConcentrations();
		assertEquals(40.0, concentrationCalculator4SSIPSystem.getTotalConcentration(), ERROR);
	}
	
	@Test
	public void testGetPsiFree(){
		concentrationCalculator4SSIPSystem.calculateFreeAndBoundConcentrations();
		assertEquals(0.027129357521085685, concentrationCalculator4SSIPSystem.getPsiFree(), ERROR);
	}
	
	@Test
	public void testCalculateBoundConcentrations(){
		ArrayRealVector expectedBoundConcentrationTotals = new ArrayRealVector(new double[]{49018.50156657677, 10699.91192181376, 29019.187036465082, 29019.187036465082});
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][]{{190.74319905744522, 427.5731150623358, 24200.092626228496, 24200.092626228496},
				{427.5731150623358, 640.1637140158524, 4816.087546367786, 4816.087546367786},
				{24200.092626228496, 4816.087546367786, 1.5034319344, 1.5034319344},
				{24200.092626228496, 4816.087546367786, 1.5034319344, 1.5034319344}});
		concentrationCalculator4SSIPSystem.calculateBoundConcentrations();
		assertEquals(4, concentrationCalculator4SSIPSystem.getBoundConcentrations().getRowDimension());
		assertEquals(4, concentrationCalculator4SSIPSystem.getBoundConcentrations().getColumnDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i), concentrationCalculator4SSIPSystem.getBoundConcentrations().getRow(i), ERROR);
		}
		assertArrayEquals(expectedBoundConcentrationTotals.toArray(), concentrationCalculator4SSIPSystem.getBoundConcentrationsTotal().toArray(), ERROR);
	}
	
	@Test
	public void testCalculateTotalConcentrations(){
		ArrayRealVector expectedTotalConcentrations = new ArrayRealVector(new double[]{49028.50156657677, 10709.91192181376, 29029.187036465082, 29029.187036465082});
		concentrationCalculator4SSIPSystem.calculateBoundConcentrations();
		ArrayRealVector actualTotalConcentrations = concentrationCalculator4SSIPSystem.calculateTotalConcentration();
		assertArrayEquals(expectedTotalConcentrations.toArray(), actualTotalConcentrations.toArray(), ERROR);
	}

	@Test
	public void testTotalConcentrationMatches(){
		assertTrue(concentrationCalculator4SSIPSystem.totalConcentrationMatches());
		concentrationCalculator4SSIPSystem.calculateBoundConcentrations();
		assertFalse(concentrationCalculator4SSIPSystem.totalConcentrationMatches());
	}
	
	@Test
	public void testNormaliseFreeConcentration() throws Exception{
		ArrayRealVector expectedTotalConcentrations = new ArrayRealVector(new double[]{49028.50156657677, 10709.91192181376, 29029.187036465082, 29029.187036465082});
		concentrationCalculator4SSIPSystem.calculateBoundConcentrations();
		ArrayRealVector actualTotalConcentrations = concentrationCalculator4SSIPSystem.calculateTotalConcentration();
		assertArrayEquals(expectedTotalConcentrations.toArray(), actualTotalConcentrations.toArray(), ERROR);		
		concentrationCalculator4SSIPSystem.normaliseFreeConcentration();
		ArrayRealVector expectedNormalisedFreeConcentrations = new ArrayRealVector(new double[]{0.14281561350023927, 0.3055674221597144, 0.1856019621616085, 0.1856019621616085});
		assertArrayEquals(expectedNormalisedFreeConcentrations.toArray(), concentrationCalculator4SSIPSystem.getFreeConcentrations().toArray(), ERROR);
		assertFalse(concentrationCalculator4SSIPSystem.totalConcentrationMatches());
	}
	
	@Test
	public void testCalculateFreeAndBoundConcentrations1SSIPSystem(){
		ArrayRealVector expectedBoundConcentrationTotal = new ArrayRealVector(new double[]{0.500000000000001});
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][]{{0.500000000000001}});
		ArrayRealVector expectedFreeConcentration = new ArrayRealVector(new double[]{0.500000000000001});
		
		concentrationCalculator1SSIPSystem.calculateFreeAndBoundConcentrations();
		assertTrue(concentrationCalculator1SSIPSystem.totalConcentrationMatches());
		
		assertArrayEquals(expectedFreeConcentration.toArray(), concentrationCalculator1SSIPSystem.getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(expectedBoundConcentrationTotal.toArray(), concentrationCalculator1SSIPSystem.getBoundConcentrationsTotal().toArray(), ERROR);
		
		assertEquals(1, concentrationCalculator1SSIPSystem.getBoundConcentrations().getRowDimension());
		assertEquals(1, concentrationCalculator1SSIPSystem.getBoundConcentrations().getRowDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i), concentrationCalculator1SSIPSystem.getBoundConcentrations().getRow(i), ERROR);
		}
		
	}
	@Test
	public void testCalculateFreeAndBoundConcentrations2SSIPSystem(){
		ArrayRealVector expectedBoundConcentrationTotal = new ArrayRealVector(new double[]{0.6096117967977974, 0.6096117967977974});
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][]{{0.3048058983988987, 0.3048058983988987}, {0.3048058983988987, 0.3048058983988987}});
		ArrayRealVector expectedFreeConcentration = new ArrayRealVector(new double[]{0.3903882032022092, 0.3903882032022092});
		
		concentrationCalculator2SSIPSystem.calculateFreeAndBoundConcentrations();
		assertTrue(concentrationCalculator2SSIPSystem.totalConcentrationMatches());
		
		assertArrayEquals(expectedFreeConcentration.toArray(), concentrationCalculator2SSIPSystem.getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(expectedBoundConcentrationTotal.toArray(), concentrationCalculator2SSIPSystem.getBoundConcentrationsTotal().toArray(), ERROR);
		
		assertEquals(2, concentrationCalculator2SSIPSystem.getBoundConcentrations().getRowDimension());
		assertEquals(2, concentrationCalculator2SSIPSystem.getBoundConcentrations().getColumnDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i), concentrationCalculator2SSIPSystem.getBoundConcentrations().getRow(i), ERROR);
		}
	}
	
	@Test
	public void testCalculateFreeAndBoundConcentrations4SSIPSystem(){
		ArrayRealVector expectedFreeConcentration = new ArrayRealVector(new double[]{0.05186667943949074, 0.24548649357294183, 0.3939105639154974, 0.3939105639154974});
		ArrayRealVector expectedBoundConcentrationTotal = new ArrayRealVector(new double[]{9.948133320560622, 9.754513506427159, 9.606089436084389, 9.606089436084389});
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][]{{0.005131282816098672, 0.0544410430503873, 4.944280497347068, 4.944280497347068},
			{0.0544410430503873, 0.3857858185611114, 4.657143322407831, 4.657143322407831},
			{4.944280497347068, 4.657143322407831, 0.002332808164745528, 0.002332808164745528},
			{4.944280497347068, 4.657143322407831, 0.002332808164745528, 0.002332808164745528}});
		
		concentrationCalculator4SSIPSystem.calculateFreeAndBoundConcentrations();
		assertTrue(concentrationCalculator4SSIPSystem.totalConcentrationMatches());
		
		assertArrayEquals(expectedFreeConcentration.toArray(), concentrationCalculator4SSIPSystem.getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(expectedBoundConcentrationTotal.toArray(), concentrationCalculator4SSIPSystem.getBoundConcentrationsTotal().toArray(), ERROR);
		
		assertEquals(4, concentrationCalculator4SSIPSystem.getBoundConcentrations().getRowDimension());
		assertEquals(4, concentrationCalculator4SSIPSystem.getBoundConcentrations().getColumnDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i), concentrationCalculator4SSIPSystem.getBoundConcentrations().getRow(i), ERROR);
		}
	}
}
