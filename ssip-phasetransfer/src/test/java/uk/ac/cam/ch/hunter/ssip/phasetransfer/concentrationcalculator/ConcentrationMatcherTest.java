/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;


import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;

public class ConcentrationMatcherTest {

	private ConcentrationMatcher concentrationMatcher4SSIPSystem;
	private ConcentrationCalculator concentrationCalculator4SSIPSystem;
	private ArrayList<SsipConcentration> ssipConcentrationList;
	private HashMap<String, HashSet<SsipConcentration>> ssipConcentrationsByMoleculeID;
	
	private static final double ERROR = 1e-7;
	
	@Before
	public void setUp() throws Exception {
		ConcentrationCalculatorFactory concentrationCalculatorFactory = new ConcentrationCalculatorFactory();
		
		Array2DRowRealMatrix associationConstantValues4SSIPSystem = new Array2DRowRealMatrix(new double[][]{{0.9537159952872262, 2.137865575311679, 121.00046313114248, 121.00046313114248},
				{2.137865575311679, 3.200818570079262, 24.080437731838927, 24.080437731838927},
				{121.00046313114248, 24.080437731838927, 7.517159672e-3, 7.517159672e-3},
				{121.00046313114248, 24.080437731838927, 7.517159672e-3, 7.517159672e-3}});
		
		ArrayRealVector expectedTotalConcentrations4SSIPSystem = new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0});
		
		concentrationCalculator4SSIPSystem = concentrationCalculatorFactory.generateConcentrationCalculator(associationConstantValues4SSIPSystem, expectedTotalConcentrations4SSIPSystem);
		
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "a", 2.0));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "a", -4.0));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(1.0, 0.0, 1.0, "a", -4.0));
		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1, new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2, new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3, new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4, new Concentration(10.0, ConcentrationUnit.MOLAR), "mol2");
		ssipConcentrationList = new ArrayList<>();
		ssipConcentrationList.add(ssipConcentration2);
		ssipConcentrationList.add(ssipConcentration1);
		ssipConcentrationList.add(ssipConcentration3);
		ssipConcentrationList.add(ssipConcentration4);
		
		HashSet<SsipConcentration> ssipConcentrationsMol1 = new HashSet<>();
		ssipConcentrationsMol1.add(ssipConcentration1);
		ssipConcentrationsMol1.add(ssipConcentration2);
		ssipConcentrationsMol1.add(ssipConcentration3);
		HashSet<SsipConcentration> ssipConcentrationsMol2 = new HashSet<>();
		ssipConcentrationsMol2.add(ssipConcentration4);
		
		ssipConcentrationsByMoleculeID = new HashMap<>();
		ssipConcentrationsByMoleculeID.put("mol1", ssipConcentrationsMol1);
		ssipConcentrationsByMoleculeID.put("mol2", ssipConcentrationsMol2);
		
		concentrationMatcher4SSIPSystem = new ConcentrationMatcher(concentrationCalculator4SSIPSystem, ssipConcentrationList);
	}

	@Test
	public void testConcentrationMatcher() {
		assertNotNull(concentrationMatcher4SSIPSystem);
		assertEquals(concentrationMatcher4SSIPSystem.getSsipConcentrationList(), ssipConcentrationList);
		assertEquals(concentrationMatcher4SSIPSystem.getConcentrationCalculator(), concentrationCalculator4SSIPSystem);
		
	}
	
	@Test
	public void testSetFreeConcentrations() throws Exception{		
		concentrationMatcher4SSIPSystem.setFreeConcentrations();
		ArrayList<SsipConcentration> actualSSIPConcentrationList = (ArrayList<SsipConcentration>) concentrationMatcher4SSIPSystem.getSsipConcentrationList();
		double[] expectedFreeConcentration = new double[]{0.05186668339004207, 0.24548651049082884, 0.3939105349663359, 0.3939105349663359};
		for (int i = 0; i < actualSSIPConcentrationList.size(); i++) {
			assertEquals(expectedFreeConcentration[i], actualSSIPConcentrationList.get(i).getFreeConcentration(), ERROR);
		}
	}

	@Test
	public void testSetBoundConcentrationTotals() throws Exception{				
		concentrationMatcher4SSIPSystem.setBoundConcentrationTotals();
		ArrayList<SsipConcentration> actualSSIPConcentrationList = (ArrayList<SsipConcentration>) concentrationMatcher4SSIPSystem.getSsipConcentrationList();
		double[] expectedBoundConcentrationTotal = new double[]{9.94813335569966, 9.754513524877266, 9.606089427317157, 9.606089427317157};
		for (int i = 0; i < actualSSIPConcentrationList.size(); i++) {
			assertEquals(expectedBoundConcentrationTotal[i], actualSSIPConcentrationList.get(i).getBoundConcentration(), ERROR);
		}
	}
	
	@Test
	public void testSetBoundConcentrationLists() throws Exception{		
		concentrationMatcher4SSIPSystem.setBoundConcentrationLists();
		ArrayList<SsipConcentration> actualSSIPConcentrationList = (ArrayList<SsipConcentration>) concentrationMatcher4SSIPSystem.getSsipConcentrationList();
		double[][] expectedBoundConcentration = new double[][]{{0.005131283597771884, 0.054441050948867135, 4.944280510576511, 4.944280510576511},
				{0.054441050948867135, 0.385785871734555, 4.657143301096922, 4.657143301096922},
				{4.944280510576511, 4.657143301096922, 0.0023328078218614115, 0.0023328078218614115},
				{4.944280510576511, 4.657143301096922, 0.0023328078218614115, 0.0023328078218614115}};
		for (int i = 0; i < actualSSIPConcentrationList.size(); i++) {
			assertArrayEquals(expectedBoundConcentration[i], actualSSIPConcentrationList.get(i).getBoundConcentrationList().toArray(), ERROR);
		}
	}
	
	@Test
	public void testSetConcentrations() {
		concentrationMatcher4SSIPSystem.setConcentrations();
		ArrayList<SsipConcentration> actualSSIPConcentrationList = (ArrayList<SsipConcentration>) concentrationMatcher4SSIPSystem.getSsipConcentrationList();
		double[] expectedFreeConcentration = new double[]{0.05186668339004207, 0.24548651049082884, 0.3939105349663359, 0.3939105349663359};
		double[] expectedBoundConcentrationTotal = new double[]{9.94813335569966, 9.754513524877266, 9.606089427317157, 9.606089427317157};
		double[][] expectedBoundConcentration = new double[][]{{0.005131283597771884, 0.054441050948867135, 4.944280510576511, 4.944280510576511},
				{0.054441050948867135, 0.385785871734555, 4.657143301096922, 4.657143301096922},
				{4.944280510576511, 4.657143301096922, 0.0023328078218614115, 0.0023328078218614115},
				{4.944280510576511, 4.657143301096922, 0.0023328078218614115, 0.0023328078218614115}};
		for (int i = 0; i < actualSSIPConcentrationList.size(); i++) {
			assertEquals(expectedFreeConcentration[i], actualSSIPConcentrationList.get(i).getFreeConcentration(), ERROR);
			assertEquals(expectedBoundConcentrationTotal[i], actualSSIPConcentrationList.get(i).getBoundConcentration(), ERROR);
			assertArrayEquals(expectedBoundConcentration[i], actualSSIPConcentrationList.get(i).getBoundConcentrationList().toArray(), ERROR);
		}
	}
}
