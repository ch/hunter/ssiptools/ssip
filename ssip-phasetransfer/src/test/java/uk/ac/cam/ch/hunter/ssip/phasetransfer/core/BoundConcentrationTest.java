/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

public class BoundConcentrationTest {

	static final double ERROR = 1e-7;
	
	private BoundConcentration boundConcentration;
	
	@Before
	public void setUp() throws Exception {
		boundConcentration = new BoundConcentration(1.0, ConcentrationUnit.MOLAR, 1);
	}

	@Test
	public void testBoundConcentration() {
		assertNotNull(boundConcentration);
		assertEquals(1.0, boundConcentration.getConcentrationValue(), ERROR);
		assertEquals(ConcentrationUnit.MOLAR, boundConcentration.getConcentrationUnit());
		assertEquals(1, boundConcentration.getSsipRef());
	}
	
	@Test
	public void testCompareTo(){
		BoundConcentration boundConcentration2 = new BoundConcentration(1.0, ConcentrationUnit.MOLAR, 2);
		ArrayList<BoundConcentration> boundConcentrations = new ArrayList<>();
		boundConcentrations.add(boundConcentration2);
		boundConcentrations.add(boundConcentration);
		Collections.sort(boundConcentrations);
		assertEquals(1, boundConcentrations.get(0).getSsipRef());
		assertEquals(-1, boundConcentration.compareTo(boundConcentration2));
	}

	@Test
	public void testEquals(){
		BoundConcentration boundConcentration2 = new BoundConcentration(1.0, ConcentrationUnit.MOLAR, 1);
		BoundConcentration boundConcentration3 = new BoundConcentration(1.0, ConcentrationUnit.MOLAR, 2);
		BoundConcentration boundConcentration4 = new BoundConcentration(4.0, ConcentrationUnit.MOLAR, 2);
		assertEquals(boundConcentration2, boundConcentration);
		assertNotEquals(boundConcentration3, boundConcentration);
		assertNotEquals(boundConcentration4, boundConcentration3);
	}
}
