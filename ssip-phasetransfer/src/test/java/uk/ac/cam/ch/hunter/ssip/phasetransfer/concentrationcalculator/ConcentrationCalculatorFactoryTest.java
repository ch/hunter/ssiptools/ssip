/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.*;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

public class ConcentrationCalculatorFactoryTest {

	private ConcentrationCalculatorFactory concentrationCalculatorFactory;
	
	private Array2DRowRealMatrix associationConstantValues4SSIPSystem;
	private ArrayRealVector expectedTotalConcentrations4SSIPSystem;
	private ConcentrationCalculator concentrationCalculator4SSIPSystem;
	
	private static final double ERROR = 1e-7;
	
	@Before
	public void setUp() throws Exception {
		concentrationCalculatorFactory = new ConcentrationCalculatorFactory();
		
		associationConstantValues4SSIPSystem = new Array2DRowRealMatrix(new double[][]{{0.9537159952872262, 2.137865575311679, 121.00046313114248, 121.00046313114248},
				{2.137865575311679, 3.200818570079262, 24.080437731838927, 24.080437731838927},
				{121.00046313114248, 24.080437731838927, 7.517159672e-3, 7.517159672e-3},
				{121.00046313114248, 24.080437731838927, 7.517159672e-3, 7.517159672e-3}});
		
		expectedTotalConcentrations4SSIPSystem = new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0});
		
		concentrationCalculator4SSIPSystem = concentrationCalculatorFactory.generateConcentrationCalculator(associationConstantValues4SSIPSystem, expectedTotalConcentrations4SSIPSystem);
	}

	@Test
	public void testConcentrationCalculatorFactory() {
		assertNotNull(concentrationCalculatorFactory);
	}

	@Test
	public void testGenerateConcentrationCalculator() {
		ArrayRealVector expectedFreeConcentration = new ArrayRealVector(new double[]{0.05186668339004207, 0.24548651049082884, 0.3939105349663359, 0.3939105349663359});
		ArrayRealVector expectedBoundConcentrationTotal = new ArrayRealVector(new double[]{9.94813335569966, 9.754513524877266, 9.606089427317157, 9.606089427317157});
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][]{{0.005131283597771884, 0.054441050948867135, 4.944280510576511, 4.944280510576511},
				{0.054441050948867135, 0.385785871734555, 4.657143301096922, 4.657143301096922},
				{4.944280510576511, 4.657143301096922, 0.0023328078218614115, 0.0023328078218614115},
				{4.944280510576511, 4.657143301096922, 0.0023328078218614115, 0.0023328078218614115}});
				
		concentrationCalculator4SSIPSystem.calculateFreeAndBoundConcentrations();
		assertTrue(concentrationCalculator4SSIPSystem.totalConcentrationMatches());
		
		assertArrayEquals(expectedFreeConcentration.toArray(), concentrationCalculator4SSIPSystem.getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(expectedBoundConcentrationTotal.toArray(), concentrationCalculator4SSIPSystem.getBoundConcentrationsTotal().toArray(), ERROR);
		
		assertEquals(4, concentrationCalculator4SSIPSystem.getBoundConcentrations().getRowDimension());
		assertEquals(4, concentrationCalculator4SSIPSystem.getBoundConcentrations().getColumnDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i), concentrationCalculator4SSIPSystem.getBoundConcentrations().getRow(i), ERROR);
		}
		

	}

}
