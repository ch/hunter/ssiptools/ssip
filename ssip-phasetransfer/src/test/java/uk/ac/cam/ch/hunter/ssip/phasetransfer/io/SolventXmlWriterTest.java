/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.w3c.dom.Attr;
import org.xmlunit.matchers.CompareMatcher;
import org.xmlunit.util.Nodes;
import org.xmlunit.util.Predicate;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;

public class SolventXmlWriterTest {

	private static final Logger LOG = LogManager.getLogger(SolventXmlWriterTest.class);

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private URI outputURI;
	private URI expectedSolventURI;
	private URI outputContainerURI;
	private URI expectedSolventContainerURI;
	private PhaseSolvent phaseSolvent;
	private String solventId;
	private String solventName;
	private HashMap<String, PhaseMolecule> solventMolecules;
	private PhaseMolecule phaseMolecule;
	private HashMap<String, PhaseSolvent> solventMap;
	private SolventContainer solventContainer;
	
	// Ignore the "dynamic" value of ssipSoftwareVersion in the testing
    final Predicate<Attr> attributeFilter = new Predicate<Attr>(){
        @Override
        public boolean test(Attr attrName) {
        	return (!Nodes.getQName(attrName).toString().contains("ssipSoftwareVersion"));
        }
    };
	
	@Before
	public void setUp() throws Exception {
		outputURI = tempFolder.newFile("solvent.xml").toURI();
		expectedSolventURI = SolventXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/phasesolvent.xml").toURI();
		outputContainerURI = tempFolder.newFile("solventcontainer.xml").toURI();
		expectedSolventContainerURI = SolventXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedsolventcontainer.xml").toURI();
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000, 0.0);
		
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "a", 2.0));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "a", -4.0));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(1.0, 0.0, 1.0, "a", -4.0));
		
		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ArrayList<SsipConcentration> expectedSSIPConcentrationList = new ArrayList<>();
		expectedSSIPConcentrationList.add(ssipConcentration2);
		expectedSSIPConcentrationList.add(ssipConcentration1);
		expectedSSIPConcentrationList.add(ssipConcentration3);
		expectedSSIPConcentrationList.add(ssipConcentration4);
		SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, expectedSSIPConcentrationList);
		
		phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		
		solventMolecules = new HashMap<>();
		solventMolecules.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		
		solventId = "water";
		solventName = "water";
		phaseSolvent = new PhaseSolvent(solventMolecules, solventId, solventName, ConcentrationUnit.MOLAR);
		solventMap = new HashMap<>();
		solventMap.put(phaseSolvent.getSolventId(), phaseSolvent);
		solventContainer = new SolventContainer(solventMap);
	}

	@Test
	public void testMarshal() {
		try {
			SolventXmlWriter.marshal(phaseSolvent, outputURI);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			LOG.error(e);
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(outputURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedSolventURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}

	@Test
	public void testMarshalContainer() {
		try {
			SolventXmlWriter.marshalContainer(solventContainer, outputContainerURI);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			LOG.error(e);
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(outputContainerURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedSolventContainerURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
}
