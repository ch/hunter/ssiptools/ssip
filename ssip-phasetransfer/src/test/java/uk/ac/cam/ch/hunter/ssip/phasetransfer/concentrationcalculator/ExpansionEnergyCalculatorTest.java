/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;

public class ExpansionEnergyCalculatorTest {

	private ArrayList<SsipConcentration> ssipConcentrations;
	private ExpansionEnergyCalculator expansionEnergyCalculator;
	private ExpansionEnergyCalculator expansionEnergyCalculator2;
	private ExpansionEnergyCalculator expansionEnergyCalculator3;
	
	private static final double ERROR = 1.0e-7;
	
	@Before
	public void setUp() throws Exception {
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "water";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		CmlMolecule cmlMolecule2 = new CmlMolecule(stdinchikey, "nullwater", atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		//volumes in cubic angstroms. taken from DOI: 10.1039/c2sc21666c
		ssipSurfaceInformation.setVolumeVdW(21.4);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		Concentration moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, "water");
		ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipWaterAlpha2Conc);
		ssipConcentrations.add(ssipWaterAlpha1Conc);
		ssipConcentrations.add(ssipWaterBeta1Conc);
		ssipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrations);
		
		PhaseMolecule phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		
		phaseMolecule.setMoleFraction(1.0);
		HashMap<String, PhaseMolecule> phaseMoleculeMap = new HashMap<>();
		phaseMoleculeMap.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		expansionEnergyCalculator = new ExpansionEnergyCalculator(phaseMoleculeMap);
		
		PhaseMolecule phaseMoleculeWater2 = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		phaseMoleculeWater2.setMoleFraction(0.25);
		
		SsipConcentration nullWaterSSIP = new SsipConcentration(ssipWaterAlpha1, moleculeConcentration, "nullwater");
		ArrayList<SsipConcentration> nullWaterSSIPs = new ArrayList<>();
		nullWaterSSIPs.add(nullWaterSSIP);
		SsipConcentrationContainer nullWaterContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, nullWaterSSIPs);
		PhaseMolecule phaseMoleculenullwater = new PhaseMolecule(cmlMolecule2, new SsipSurfaceInformation(ssipSurfaceInformation), nullWaterContainer, "nullwater", stdinchikey);
		phaseMoleculenullwater.setMoleFraction(0.75);
		HashMap<String, PhaseMolecule> nullWaterMixture = new HashMap<>();
		nullWaterMixture.put("water", phaseMoleculeWater2);
		nullWaterMixture.put("nullwater", phaseMoleculenullwater);
		
		expansionEnergyCalculator2 = new ExpansionEnergyCalculator(nullWaterMixture);
		
		ArrayList<SsipConcentration> posOnlyWater = new ArrayList<>();
		posOnlyWater.add(ssipWaterAlpha1Conc);
		posOnlyWater.add(ssipWaterAlpha2Conc);
		SsipConcentrationContainer posWaterContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, posOnlyWater);
		PhaseMolecule phaseMoleculeposwater = new PhaseMolecule(cmlMolecule2, new SsipSurfaceInformation(ssipSurfaceInformation), posWaterContainer, "poswater", stdinchikey);
		phaseMoleculeposwater.setMoleFraction(1.0);
		HashMap<String, PhaseMolecule> posWaterMixture = new HashMap<>();
		posWaterMixture.put("water", phaseMoleculeposwater);
		expansionEnergyCalculator3 = new ExpansionEnergyCalculator(posWaterMixture);
	}

	@Test
	public void testExpansionEnergyCalculator() {
		assertNotNull(expansionEnergyCalculator);
		assertEquals(ssipConcentrations, expansionEnergyCalculator.getSsipConcentrations());
	}

	@Test
	public void testGetExpansionEnergies() {
		double[] expectedValues = new double[]{7.368902576034264, 7.368902576034264, 7.368902576034264, 7.368902576034264};
		assertArrayEquals(expectedValues, expansionEnergyCalculator.getExpansionEnergies(new ArrayRealVector(new double[] {0.5, 0.5, 0.5, 0.5})).toArray(), ERROR);
	}

	@Test
	public void testGetVdWExpEnergies() {
		double[] expectedValues = new double[]{4.2, 4.2, 4.2, 4.2};
		assertArrayEquals(expectedValues, expansionEnergyCalculator.getVdWExpEnergies().toArray(), ERROR);
		double[] expectedValues2 = new double[]{1.2923076923076924, 4.2, 4.2, 4.2, 4.2};
		assertArrayEquals(expectedValues2, expansionEnergyCalculator2.getVdWExpEnergies().toArray(), ERROR);
	}

	@Test
	public void testGetInteractionExpansionEnergies() {
		double[] expectedValues = new double[]{6.337805152068531, 6.337805152068531, 6.337805152068531, 6.337805152068531};
		assertArrayEquals(expectedValues, expansionEnergyCalculator.getInteractionExpansionEnergies().toArray(), ERROR);
		double[] expectedValues2 = new double[]{4.225203434712354, 3.1689025760342653, 3.1689025760342653, 3.1689025760342653, 3.1689025760342653};
		assertArrayEquals(expectedValues2, expansionEnergyCalculator2.getInteractionExpansionEnergies().toArray(), ERROR);
	}
	
	@Test
	public void testGetInteractionExpansionEnergiesNoNegSsips() {
		double[] expectedValues = new double[]{0.0, 0.0};
		assertArrayEquals(expectedValues, expansionEnergyCalculator3.getInteractionExpansionEnergies().toArray(), ERROR);
	}

}
