/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;

public class AssociationConstantsFactoryTest {

	private static final double ERROR = 1e-7;
	
	private AssociationConstantsFactory associationFactory;
	private ArrayList<Ssip> ssipList;
	private AssociationConstants expectedAssociationConstants;
	
	@Before
	public void setUp() throws Exception {
		associationFactory = new AssociationConstantsFactory(298.0);
		AlphaPoint mep1 = new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0);
		Ssip ssip1 = Ssip.fromMepPoint(mep1);
		AlphaPoint mep2 = new AlphaPoint(0.0, 1.0, 0.0, "b", 2.0);
		Ssip ssip2 = Ssip.fromMepPoint(mep2);
		BetaPoint mep3 = new BetaPoint(0.0, 0.0, 1.0, "c", -4.0);
		Ssip ssip3 = Ssip.fromMepPoint(mep3);
		BetaPoint mep4 = new BetaPoint(1.0, 1.0, 1.0, "c", -4.0);
		Ssip ssip4 = Ssip.fromMepPoint(mep4);
		ssipList = new ArrayList<>();
		ssipList.add(ssip2);
		ssipList.add(ssip1);
		ssipList.add(ssip3);
		ssipList.add(ssip4);
		Array2DRowRealMatrix associationConstantValues = new Array2DRowRealMatrix(new double[][]{{4.79227489177873, 4.792274891778739, 121.00048466786859, 121.00048466786859},
				{4.79227489177873, 4.79227489177873, 24.080452950492628, 24.080452950492628},
				{121.00048466786859, 24.080452950492628, 4.79227489177873, 4.79227489177873},
				{121.00048466786859, 24.080452950492628, 4.79227489177873, 4.79227489177873}});
		expectedAssociationConstants = new AssociationConstants(associationConstantValues);
	}

	@Test
	public void testAssociationConstantsFactoryConstructor() {
		assertNotNull(associationFactory);
		assertEquals(298.0, associationFactory.getTemperature(), ERROR);
	}

	@Test
	public void testCalculateKVdW(){
		assertEquals(4.79227489177873, AssociationConstantsFactory.calculateKVdW(298.0), ERROR);
	}
	
	@Test
	public void testGetAssociationConstants() {
		AssociationConstants actualAssociationConstants = associationFactory.getAssociationConstants(ssipList);
		for (int i = 0; i < actualAssociationConstants.getAssociationConstants().getRowDimension(); i++) {
			assertArrayEquals(expectedAssociationConstants.getAssociationConstants().getRow(i), actualAssociationConstants.getAssociationConstants().getRow(i), ERROR);
		}
	}

}
