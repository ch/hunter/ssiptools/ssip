/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.comparators;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;

public class SsipConcentrationComparatorTest {

	private static final Logger LOG = LogManager.getLogger(SsipConcentrationComparatorTest.class);
	
	private SsipConcentration ssipConcentration1;
	private SsipConcentration ssipConcentration2;
	private SsipConcentration ssipConcentration3;
	private SsipConcentration ssipConcentration4;
	private SsipConcentration ssipConcentration5;
	private SsipConcentration ssipConcentration6;
	private SSIPConcentrationComparator ssipConcentrationComparator;

	@Before
	public void setUp() throws Exception {
		ssipConcentration1 = new SsipConcentration(new AlphaPoint(1.0, 0.0,
				0.0, "a", 1.0), 10.0, ConcentrationUnit.MOLAR, "mol1");
		ssipConcentration2 = new SsipConcentration(new AlphaPoint(0.0, 1.0,
				0.0, "a", 2.0), 10.0, ConcentrationUnit.MOLAR, "mol1");
		ssipConcentration3 = new SsipConcentration(new AlphaPoint(0.0, 0.0,
				1.0, "a", 2.0), 10.0, ConcentrationUnit.MOLAR, "mol2");
		ssipConcentration4 = new SsipConcentration(new BetaPoint(1.0, 0.0, 1.0,
				"a", -2.0), 10.0, ConcentrationUnit.MOLAR, "mol2");
		ssipConcentration5 = new SsipConcentration(new BetaPoint(0.0, 0.0, 1.0,
				"a", -2.0), 10.0, ConcentrationUnit.MOLAR, "mol2");
		ssipConcentration6 = new SsipConcentration(new BetaPoint(0.0, 0.0, -1.0,
				"a", -2.0), 10.0, ConcentrationUnit.MOLAR, "mol2");
		HashMap<String, Point3D> centroidMap = new HashMap<>();
		centroidMap.put("mol1", new Point3D(0.0, 0.0, 0.0));
		centroidMap.put("mol2", new Point3D(0.0, 0.0, 0.0));
		ssipConcentrationComparator = new SSIPConcentrationComparator(centroidMap);
	}

	@Test
	public void testCompare() {
		assertEquals(0, ssipConcentrationComparator.compare(ssipConcentration1,
				ssipConcentration1));
		LOG.debug("ssipConcentration1: {} " +
				"ssipConcentration2: {}", ssipConcentration1.toString(), ssipConcentration2.toString());
		LOG.debug("equality: {}", ssipConcentration1.equals(ssipConcentration2));
		assertEquals(1, ssipConcentrationComparator.compare(ssipConcentration1,
				ssipConcentration2));
		assertEquals(-1, ssipConcentrationComparator.compare(
				ssipConcentration1, ssipConcentration3));
		assertEquals(-1, ssipConcentrationComparator.compare(
				ssipConcentration1, ssipConcentration4));
		assertEquals(-1, ssipConcentrationComparator.compare(
				ssipConcentration3, ssipConcentration4));
		assertEquals(-1, ssipConcentrationComparator.compare(
				ssipConcentration2, ssipConcentration3));
		LOG.debug("ssipConcentration4: {} " +
				"ssipConcentration5: {}", ssipConcentration4.toString(), ssipConcentration5.toString());
		LOG.debug("equality: {}", ssipConcentration4.equals(ssipConcentration5));
		assertEquals(1, ssipConcentrationComparator.compare(
				ssipConcentration4, ssipConcentration5));
		assertEquals(-1, ssipConcentrationComparator.compare(ssipConcentration5, ssipConcentration4));
		assertEquals(-2, ssipConcentrationComparator.compare(ssipConcentration5, ssipConcentration6));
		assertEquals(2, ssipConcentrationComparator.compare(ssipConcentration6, ssipConcentration5));
	}

}
