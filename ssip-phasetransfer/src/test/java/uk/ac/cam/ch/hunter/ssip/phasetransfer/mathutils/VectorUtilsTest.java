/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Test;

public class VectorUtilsTest {

	private static final double ERROR = 1e-7;
	
	@Test
	public void testSumVectorElements() {
		assertEquals(5.0, VectorUtils.sumVectorElements(new ArrayRealVector(new double[] {1.0, 2.0, 0.5, 0.0, 1.5})), ERROR);
	}

	@Test
	public void testCompareVectorsEqualAbsoluteTolerance() {
		assertTrue(VectorUtils.compareVectorsEqualAbsoluteTolerance(new ArrayRealVector(new double[]{1.0, 2.0, 3.0}), new ArrayRealVector(new double[]{1.0, 2.0, 3.0}), ERROR));
		assertTrue(VectorUtils.compareVectorsEqualAbsoluteTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), ERROR));
		assertTrue(VectorUtils.compareVectorsEqualAbsoluteTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), 1e-9));
		assertTrue(VectorUtils.compareVectorsEqualAbsoluteTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), 1e-10));
		}

	@Test
	public void testCompareVectorsEqualRelativeTolerance() {
		assertTrue(VectorUtils.compareVectorsEqualRelativeTolerance(new ArrayRealVector(new double[]{1.0, 2.0, 3.0}), new ArrayRealVector(new double[]{1.0, 2.0, 3.0}), ERROR));
		assertTrue(VectorUtils.compareVectorsEqualRelativeTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), ERROR));
		assertTrue(VectorUtils.compareVectorsEqualRelativeTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), 1e-9));
		assertTrue(VectorUtils.compareVectorsEqualRelativeTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), 1e-10));
		}
	
	@Test
	public void testCompareVectorsEqualCompositeTolerance() {
		assertTrue(VectorUtils.compareVectorsEqualCompositeTolerance(new ArrayRealVector(new double[]{1.0, 2.0, 3.0}), new ArrayRealVector(new double[]{1.0, 2.0, 3.0}), ERROR, ERROR));
		assertTrue(VectorUtils.compareVectorsEqualCompositeTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), ERROR, ERROR));
		assertTrue(VectorUtils.compareVectorsEqualCompositeTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), 1e-9, 1e-9));
		assertTrue(VectorUtils.compareVectorsEqualCompositeTolerance(new ArrayRealVector(new double[]{10.0000000001, 10.0000000001, 9.9999999999, 9.9999999999}), new ArrayRealVector(new double[]{10.0, 10.0, 10.0, 10.0}), 1e-10, 1e-10));
	}
	
	@Test
	public void testCalculateVectorVectorTMatrix() {
		Array2DRowRealMatrix outputMatrix = VectorUtils.calculateVectorVectorTMatrix(new ArrayRealVector(new double[]{1.0, 2.0, 3.0}), new ArrayRealVector(new double[]{1.0, 4.0, 5.0}));
		Array2DRowRealMatrix expectedMatrix = new Array2DRowRealMatrix(new double[][] {{1.0, 4.0, 5.0},
																					   {2.0, 8.0, 10.0},
																					   {3.0, 12.0, 15.0}});
		for (int i = 0; i < expectedMatrix.getRowDimension(); i++) {
			assertArrayEquals(expectedMatrix.getRow(i), outputMatrix.getRow(i), ERROR);
		}
	}

}
