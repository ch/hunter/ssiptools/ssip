/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;

public class PhaseFactoryTest {

	private PhaseFactory phaseFactory;
	private Phase testPhase;
	private Phase waterPhase;
	private Phase waterPhaseGivenConcentrations;

	static final double ERROR = 1e-7;

	@Before
	public void setUp() throws Exception {
		phaseFactory = new PhaseFactory();

		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "a", 2.0));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "a", -4.0));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(1.0, 0.0, 1.0, "a", -4.0));
		
		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1,
				new Concentration(10.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2,
				new Concentration(10.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3,
				new Concentration(10.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4,
				new Concentration(10.0, ConcentrationUnit.SSIPCONCENTRATION), "mol2");
		
		ArrayList<SsipConcentration> expectedSSIPConcentrationList = new ArrayList<>();
		expectedSSIPConcentrationList.add(ssipConcentration2);
		expectedSSIPConcentrationList.add(ssipConcentration1);
		expectedSSIPConcentrationList.add(ssipConcentration3);
		expectedSSIPConcentrationList.add(ssipConcentration4);
		Concentration moleculeConcentration = new Concentration(10.0, ConcentrationUnit.SSIPCONCENTRATION);
		String moleculeID1 = "mol1";
		String moleculeID2 = "mol2";
		ArrayList<SsipConcentration> ssipConcentrationList1 = new ArrayList<>();
		ssipConcentrationList1.add(ssipConcentration2);
		ssipConcentrationList1.add(ssipConcentration1);
		ssipConcentrationList1.add(ssipConcentration3);
		ArrayList<SsipConcentration> ssipConcentrationList2 = new ArrayList<>();
		ssipConcentrationList2.add(ssipConcentration4);
		SsipConcentrationContainer ssipConcentrationContainer1 = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrationList1);
		SsipConcentrationContainer ssipConcentrationContainer2 = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrationList2);
		PhaseMolecule phaseMolecule1 = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer1, moleculeID1, stdinchikey);
		PhaseMolecule phaseMolecule2 = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer2, moleculeID2, stdinchikey);
		HashSet<PhaseMolecule> ssipConcListSet = new HashSet<>();
		ssipConcListSet.add(phaseMolecule1);
		ssipConcListSet.add(phaseMolecule2);
		Temperature expectedTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		testPhase = phaseFactory.generatePhase(ssipConcListSet, expectedTemperature, ConcentrationUnit.SSIPCONCENTRATION);

		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		String moleculeWater = "water";
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, "water");
		ArrayList<SsipConcentration> waterSsipConcentrations = new ArrayList<>();
		waterSsipConcentrations.add(ssipWaterAlpha1Conc);
		waterSsipConcentrations.add(ssipWaterAlpha2Conc);
		waterSsipConcentrations.add(ssipWaterBeta1Conc);
		waterSsipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer waterContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, waterSsipConcentrations);
		SsipSurface waterSurfaceInformation = new SsipSurface(37.34, 18.46, 18.879, 0.002, 2200); 
		
		PhaseMolecule waterMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(waterSurfaceInformation), waterContainer, moleculeWater, moleculeWater);
		HashSet<PhaseMolecule> waterPhaseMolecules = new HashSet<>();
		waterPhaseMolecules.add(waterMolecule);
		PhaseFactory phaseFactory = new PhaseFactory();
		
		waterPhase = phaseFactory.generatePhase(waterPhaseMolecules, expectedTemperature, ConcentrationUnit.SSIPCONCENTRATION);
		
		ArrayRealVector freeConcentrations = new ArrayRealVector(new double[] {
				0.007533732191334883, 0.007533732191334883,
				0.00757930731166302, 0.00757930731166302 });
		Array2DRowRealMatrix boundConcentrations = new Array2DRowRealMatrix(
				new double[][] {{2.2982203651390878E-5, 2.2982203651390878E-5,
							0.08846017133076653, 0.08846017133076653},
							{2.2982203651390878E-5, 2.2982203651390878E-5,
							0.08846017133076653, 0.08846017133076653},
							{0.08846017133076653, 0.08846017133076653,
							1.553807836657748E-7, 1.553807836657748E-7},
							{0.08846017133076653, 0.08846017133076653,
							1.553807836657748E-7, 1.553807836657748E-7 }});
		
		waterPhaseGivenConcentrations = phaseFactory.generatePhase(waterPhaseMolecules, expectedTemperature, ConcentrationUnit.SSIPCONCENTRATION, freeConcentrations, boundConcentrations);
	}

	@Test
	public void testPhaseFactory() {
		assertNotNull(phaseFactory);
	}

	@Test
	public void testGeneratePhase() {
		HashSet<PhaseMolecule> ssipConcentrationListsWithConcentrations = (HashSet<PhaseMolecule>) testPhase
				.getPhaseMoleculesSet();
		assertEquals(2, ssipConcentrationListsWithConcentrations.size());
		for (PhaseMolecule phaseMolecule : ssipConcentrationListsWithConcentrations) {
			if (phaseMolecule.getMoleculeID() == "mol1") {
				double[] expectedFreeConcentration = new double[] {
						0.08676986748399186, 0.36733966024767906,
						0.22705476384913603 };
				double[] expectedBoundConcentrationTotal = new double[] {
						9.913230132610302, 9.632660339819983, 9.772945236069777 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
						0.07216217023801696, 0.3054980705468841,
						4.767784945912701, 4.767784945912701},
						{0.3054980705468841, 1.2933240616244759,
						4.0169191038243115, 4.0169191038243115},
						{4.767784945912701, 4.0169191038243115,
							0.4941205931663821, 0.4941205931663821 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				Array2DRowRealMatrix actualBoundConcentrationMatrix = phaseMolecule
						.getBoundConcentrationsMatrix();
				assertEquals(3, actualBoundConcentrationMatrix.getRowDimension());
				assertEquals(4, actualBoundConcentrationMatrix.getColumnDimension());
				// 1D serialisation goes down rows first, so transpose is
				// required.
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							actualBoundConcentrationMatrix.getRow(i),
							ERROR);
				}
				
			} else if (phaseMolecule.getMoleculeID() == "mol2") {
				double[] expectedFreeConcentration = new double[] { 0.22705476384913603 };
				double[] expectedBoundConcentrationTotal = new double[] { 9.772945236069777};
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
					4.767784945912701, 4.0169191038243115,
					0.4941205931663821, 0.4941205931663821 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				assertEquals(
						1,
						phaseMolecule.getBoundConcentrationsMatrix().getRowDimension());
				assertEquals(
						4,
						phaseMolecule.getBoundConcentrationsMatrix().getColumnDimension());
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							phaseMolecule.getBoundConcentrationsMatrix().getRow(i),
							ERROR);
				}
			} else {
				fail("detected a different SSIPConcentrationList");
			}
		}
	}

	@Test
	public void testGenerateWaterPhase() {
		HashSet<PhaseMolecule> ssipConcentrationListsWithConcentrations = (HashSet<PhaseMolecule>) waterPhase
				.getPhaseMoleculesSet();
		assertEquals(1, ssipConcentrationListsWithConcentrations.size());
		for (PhaseMolecule phaseMolecule : ssipConcentrationListsWithConcentrations) {
			if (phaseMolecule.getMoleculeID() == "water") {
				double[] expectedFreeConcentration = new double[] {
						0.007534186253796742, 0.007534186253796742,
						0.007534186253796742, 0.007534186253796742 };
				double[] expectedBoundConcentrationTotal = new double[] {
						0.17696581377157944, 0.17696581377157944,
						0.17696581377157944, 0.17696581377157944 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
						5.440570245593702E-4, 5.440570245593702E-4,
						0.08793884986123035, 0.08793884986123035},
						{5.440570245593702E-4, 5.440570245593702E-4,
							0.08793884986123035, 0.08793884986123035},
						{0.08793884986123035, 0.08793884986123035,
						5.440570245593702E-4, 5.440570245593702E-4},
						{0.08793884986123035, 0.08793884986123035,
						5.440570245593702E-4, 5.440570245593702E-4 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				Array2DRowRealMatrix actualBoundConcentrationMatrix = phaseMolecule
						.getBoundConcentrationsMatrix();
				assertEquals(4, actualBoundConcentrationMatrix.getRowDimension());
				assertEquals(4, actualBoundConcentrationMatrix.getColumnDimension());
				// 1D serialisation goes down rows first, so transpose is
				// required.
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							actualBoundConcentrationMatrix.getRow(i),
							ERROR);
				}
			} else {
				fail("detected a different SSIPConcentrationList");
			}
		}
	}

	@Test
	public void testGeneratePhaseFromGivenConcentrations(){
		HashSet<PhaseMolecule> ssipConcentrationListsWithConcentrations = (HashSet<PhaseMolecule>) waterPhaseGivenConcentrations.getPhaseMoleculesSet();
		assertEquals(1, ssipConcentrationListsWithConcentrations.size());
		for (PhaseMolecule phaseMolecule : ssipConcentrationListsWithConcentrations) {
			if (phaseMolecule.getMoleculeID() == "water") {
				double[] expectedFreeConcentration = new double[] {
						0.007533732191334883, 0.007533732191334883,
						0.00757930731166302, 0.00757930731166302 };
				double[] expectedBoundConcentrationTotal = new double[] {
						0.17696630706883584, 0.17696630706883584,
						0.17692065342310037, 0.17692065342310037 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
						2.2982203651390878E-5, 2.2982203651390878E-5,
						0.08846017133076653, 0.08846017133076653},
						{2.2982203651390878E-5, 2.2982203651390878E-5,
						0.08846017133076653, 0.08846017133076653},
						{0.08846017133076653, 0.08846017133076653,
						1.553807836657748E-7, 1.553807836657748E-7},
						{0.08846017133076653, 0.08846017133076653,
						1.553807836657748E-7, 1.553807836657748E-7 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				Array2DRowRealMatrix actualBoundConcentrationMatrix = phaseMolecule
						.getBoundConcentrationsMatrix();
				assertEquals(4, actualBoundConcentrationMatrix.getRowDimension());
				assertEquals(4, actualBoundConcentrationMatrix.getColumnDimension());
				// 1D serialisation goes down rows first, so transpose is
				// required.
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							actualBoundConcentrationMatrix.getRow(i),
							ERROR);
				}
			} else {
				fail("detected a different SSIPConcentrationList");
			}
		}
	}
}
