/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils.VectorUtils;

public class PhaseTest {

	private Phase phase;
	private ArrayList<SsipConcentration> expectedSSIPConcentrationList;
	private HashSet<PhaseMolecule> ssipConcListSet;
	private AssociationConstants expectedAssociationConstants;
	private Temperature expectedTemperature;
	private HashMap<String, PhaseMolecule> expectedSSIPConcentrationListsByMoleculeID;
	private Phase waterPhase;
	private SsipSurface ssipSurfaceInformation;
	private CmlMolecule cmlMolecule;
	private String stdinchikey;
	private String moleculeid;
	private PhaseMolecule phaseMolecule1;
	private PhaseMolecule phaseMolecule2;
	
	static final double ERROR = 1e-9;
	static final double ERROR_IN_DENSITY = 1e-5;

	@Before
	public void setUp() throws Exception {

		stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "a", 2.0));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "a", -4.0));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(1.0, 0.0, 1.0, "a", -4.0));
		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1,
				new Concentration(55.35, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2,
				new Concentration(55.35, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3,
				new Concentration(55.35, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4,
				new Concentration(55.35, ConcentrationUnit.MOLAR), "mol2");
		expectedSSIPConcentrationList = new ArrayList<>();
		expectedSSIPConcentrationList.add(ssipConcentration2);
		expectedSSIPConcentrationList.add(ssipConcentration1);
		expectedSSIPConcentrationList.add(ssipConcentration3);
		expectedSSIPConcentrationList.add(ssipConcentration4);
		Concentration moleculeConcentration = new Concentration(55.35, ConcentrationUnit.MOLAR);
		String moleculeID1 = "mol1";
		String moleculeID2 = "mol2";
		ArrayList<SsipConcentration> ssipConcentrationList1 = new ArrayList<>();
		ssipConcentrationList1.add(ssipConcentration2);
		ssipConcentrationList1.add(ssipConcentration1);
		ssipConcentrationList1.add(ssipConcentration3);
		ArrayList<SsipConcentration> ssipConcentrationList2 = new ArrayList<>();
		ssipConcentrationList2.add(ssipConcentration4);
		SsipConcentrationContainer ssipConcentrationContainer1 = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, ssipConcentrationList1);
		SsipConcentrationContainer ssipConcentrationContainer2 = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, ssipConcentrationList2);
		phaseMolecule1 = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer1, moleculeID1, stdinchikey);
		phaseMolecule2 = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer2, moleculeID2, stdinchikey);
		ssipConcListSet = new HashSet<>();
		ssipConcListSet.add(phaseMolecule1);
		ssipConcListSet.add(phaseMolecule2);
		expectedSSIPConcentrationListsByMoleculeID = new HashMap<>();
		expectedSSIPConcentrationListsByMoleculeID.put(moleculeID1,
				phaseMolecule1);
		expectedSSIPConcentrationListsByMoleculeID.put(moleculeID2,
				phaseMolecule2);
		expectedTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		phase = new Phase(ssipConcListSet, expectedTemperature, ConcentrationUnit.MOLAR);

		Array2DRowRealMatrix associationConstantValues = new Array2DRowRealMatrix(new double[][]{{4.79227489177873, 4.79227489177873, 121.00048466786859, 121.00048466786859},
				{4.79227489177873, 4.79227489177873, 24.080452950492628, 24.080452950492628},
				{121.00048466786859, 24.080452950492628, 4.79227489177873, 4.79227489177873},
				{121.00048466786859, 24.080452950492628, 4.79227489177873, 4.79227489177873}});
		expectedAssociationConstants = new AssociationConstants(
				associationConstantValues);

		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		String moleculeWater = "water";
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, "water");
		ArrayList<SsipConcentration> waterSsipConcentrations = new ArrayList<>();
		waterSsipConcentrations.add(ssipWaterAlpha1Conc);
		waterSsipConcentrations.add(ssipWaterAlpha2Conc);
		waterSsipConcentrations.add(ssipWaterBeta1Conc);
		waterSsipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer waterContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, waterSsipConcentrations);
		SsipSurface waterSurfaceInformation = new SsipSurface(37.34, 18.46, 18.879, 0.002, 2200); 
		
		PhaseMolecule waterMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(waterSurfaceInformation), waterContainer, moleculeWater, moleculeWater);
		HashSet<PhaseMolecule> waterPhaseMolecules = new HashSet<>();
		waterPhaseMolecules.add(waterMolecule);
		PhaseFactory phaseFactory = new PhaseFactory();
		waterPhase = phaseFactory.generatePhase(waterPhaseMolecules, expectedTemperature, ConcentrationUnit.SSIPCONCENTRATION);
		waterPhase.setSolventID("water");
		waterPhase.setPhaseType(PhaseType.CONDENSED);
	}

	@Test
	public void testPhaseConstructor() {
		assertNotNull(phase);
		assertEquals(expectedTemperature, phase.getTemperature());

	}

	@Test
	public void testGetsortedSSIPConcentrations() {
		ArrayList<SsipConcentration> actualSSIPConcentrationList = (ArrayList<SsipConcentration>) phase
				.getsortedSSIPConcentrations();
		assertEquals(expectedSSIPConcentrationList, actualSSIPConcentrationList);
	}

	@Test
	public void testGetSsipConcentrationListByMolecule() {
		HashSet<PhaseMolecule> actualConcListSet = (HashSet<PhaseMolecule>) phase
				.getPhaseMoleculesSet();
		assertEquals(ssipConcListSet, actualConcListSet);
	}

	@Test
	public void testGetAssociationConstants() {
		AssociationConstants actualAssociationConstants = phase
				.getAssociationConstants();
		for (int i = 0; i < expectedAssociationConstants.getAssociationConstants().getRowDimension(); i++) {
			assertArrayEquals(expectedAssociationConstants
					.getAssociationConstants().getRow(i),
					actualAssociationConstants.getAssociationConstants().getRow(i),
					ERROR);
		}
		
	}

	@Test
	public void testGetSSIPConcentrationListwithMoleculeID() {
		HashMap<String, PhaseMolecule> actualSSIPConcentratrionsByMoleculeID = (HashMap<String, PhaseMolecule>) phase
				.getPhaseMolecules();
		assertEquals(expectedSSIPConcentrationListsByMoleculeID,
				actualSSIPConcentratrionsByMoleculeID);
	}

	@Test
	public void testGetPhaseConcentration(){
		assertEquals(new Concentration(55.35/300.00, ConcentrationUnit.SSIPCONCENTRATION), waterPhase.getPhaseConcentration());
	}
	
	@Test
	public void testGetTotalConcentrations() {
		double[] expectedtotalConcentrations = new double[] { 0.1845, 0.1845,
				0.1845, 0.1845 };
		assertArrayEquals(expectedtotalConcentrations, waterPhase
				.getTotalConcentrations().toArray(), ERROR);
	}

	@Test
	public void testGetFreeConcentrations() {
		double[] expectedFreeConcentration = new double[] {
				0.007534186253796742, 0.007534186253796742,
				0.007534186253796742, 0.007534186253796742};
		assertArrayEquals(expectedFreeConcentration, waterPhase
				.getFreeConcentrations().toArray(), ERROR);
	}

	@Test
	public void testGetBoundConcentrations() {
		double[] expectedBoundConcentrationTotal = new double[] {
				0.17696581377157944, 0.17696581377157944, 0.17696581377157944,
				0.17696581377157944 };
		assertArrayEquals(expectedBoundConcentrationTotal, waterPhase
				.getBoundConcentrations().toArray(), ERROR);
	}

	@Test
	public void testGetBoundConcentrationsMatrix() {
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
			5.440570245593702E-4, 5.440570245593702E-4,
			0.08793884986123035, 0.08793884986123035},
				{5.440570245593702E-4, 5.440570245593702E-4,
				0.08793884986123035, 0.08793884986123035},
				{0.08793884986123035, 0.08793884986123035, 
					5.440570245593702E-4, 5.440570245593702E-4},
				{0.08793884986123035, 0.087938849861230358,
						5.440570245593702E-4, 5.440570245593702E-4 }});
		Array2DRowRealMatrix actualBoundConcentrationMatrix = waterPhase
				.getBoundConcentrationsMatrix();
		assertEquals(4, actualBoundConcentrationMatrix.getRowDimension());
		assertEquals(4, actualBoundConcentrationMatrix.getColumnDimension());
		// 1D serialisation goes down rows first, so transpose is required.
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i),
					actualBoundConcentrationMatrix.getRow(i), ERROR);
		}
	}

	@Test
	public void testCalculateConversionTo1MScale(){
		assertEquals(-9.944722863356403, waterPhase.calculateConversionTo1MScale(), ERROR);
	}
	
	@Test
	public void testCalculatesolvationConstants() {
		double[] expectedSolvationConstants = new double[] {
				23.488377881583453, 23.488377881583453, 23.488377881583453,
				23.488377881583453 };
		ArrayRealVector actualSolvationConstants = waterPhase
				.calculateSolvationConstants();
		assertArrayEquals(expectedSolvationConstants,
				actualSolvationConstants.toArray(), ERROR);
	}

	@Test
	public void testCalculateFractionFree() {
		double[] expectedFractionFree = new double[] { 0.04083569785255687,
				0.04083569785255687, 0.04083569785255687, 0.04083569785255687 };
		ArrayRealVector actualFractionFree = waterPhase.calculateFractionFree();
		assertArrayEquals(expectedFractionFree, actualFractionFree.toArray(),
				ERROR);
	}

	@Test
	public void testCalculateFractionalOccupancy() {
		assertEquals(0.738, waterPhase.calculateFractionalOccupancy(), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergyPerSSIP() throws Exception{
		double confinementEnergyPerSSIP = waterPhase.calculateConfinementEnergyPerSSIP();
		assertEquals(1.475251022921089, confinementEnergyPerSSIP, ERROR);
	}
	
	@Test
	public void testNumberOfSSIPsInMolecule() throws Exception{		
		int numberOfSSIPs = waterPhase.numberOfSSIPsInMolecule("water");
		assertEquals(4, numberOfSSIPs);
	}
	
	@Test
	public void testCalculateConfinementEnergyPerSSIPMatrix(){
		double[] expectedArray = new double[]{1.475251022921089, 1.475251022921089, 1.475251022921089, 1.475251022921089};
		ArrayRealVector actualMatrix = waterPhase.calculateConfinementEnergyPerSSIPMatrix("water");
		assertArrayEquals(expectedArray, actualMatrix.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergy(){
		assertEquals(5.901004091684356, waterPhase.calculateConfinementEnergy("water"), ERROR);
	}
	
	@Test
	public void testCalculateBindingEnergyPerSSIP(){
		double[] expectedArray = new double[]{-7.924205601392543, -7.924205601392543, -7.924205601392543, -7.924205601392543};
		ArrayRealVector actualMatrix = waterPhase.calculateBindingEnergyPerSSIP("water");
		assertArrayEquals(expectedArray, actualMatrix.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateBindingEnergy(){
		assertEquals(-31.69682240557017, waterPhase.calculateBindingEnergy("water"), ERROR);
	}
	
	@Test
	public void testCalculateEnergyInPhaseBySSIP(){
		double[] expectedArray = new double[]{-6.4489545784714535, -6.4489545784714535, -6.4489545784714535, -6.4489545784714535};
		ArrayRealVector actualMatrix = waterPhase.calculateEnergyInPhaseBySSIP("water");
		assertArrayEquals(expectedArray, actualMatrix.toArray(), ERROR);
		assertEquals(-25.795818313885814, VectorUtils.sumVectorElements(actualMatrix), ERROR);
	}
	
	@Test
	public void testCalculateEnergyInPhase(){
		assertEquals(-25.795818313885814, waterPhase.calculateEnergyInPhase("water"), ERROR);
	}

	@Test
	public void testCalculateMoleculeAssociationEnergyValues() {
		ArrayList<AssociationEnergyValue> expectedAssociationEnergyValues = new ArrayList<>();
		expectedAssociationEnergyValues.add(new AssociationEnergyValue("water", "water", "water", -79.730795157));
		ArrayList<AssociationEnergyValue> actualAssociationEnergyValues = (ArrayList<AssociationEnergyValue>) waterPhase.calculateMoleculeAssociationEnergyValues();
		assertEquals(expectedAssociationEnergyValues, actualAssociationEnergyValues);
	}
	
	@Test
	public void testCalculateMoleculeAssociationEnergyValue() {
		AssociationEnergyValue expectedValue = new AssociationEnergyValue("water", "water", "water", -79.730795157);
		AssociationEnergyValue actualValue = waterPhase.calculateMoleculeAssociationEnergyValue("water", "water");
		assertEquals(expectedValue, actualValue);
	}
	@Test
	public void testCalculateMoleculeAssociationEnergy() {
		assertEquals(-79.730795089818, waterPhase.calculateMoleculeAssociationEnergy("water", "water"), ERROR);
	}
	@Test
	public void testCalculateMoleculeAssociationConstants() {
		double[] expectedValues = new double[] { 3117.5732974962393, 3117.5732974962393, 3117.5732974962393, 3117.5732974962393};
		ArrayRealVector actualValues = waterPhase.calculateMoleculeAssociationConstants("water", "water");
		assertArrayEquals(expectedValues, actualValues.toArray(), ERROR);
	}
	
	@Test
	public void testCalculatePhaseConcentrationFraction() {
		ArrayList<MoleculeConcentrationFraction> expectedMolConcFractions = new ArrayList<>();
		ArrayList<BoundConcentrationFraction> boundFractions = new ArrayList<>();
		boundFractions.add(new BoundConcentrationFraction(0.959164, "water", "water"));
		expectedMolConcFractions.add(new MoleculeConcentrationFraction("water", new FreeConcentrationFraction(0.040836, "water"), boundFractions));
		PhaseConcentrationFraction expectedPhaseFraction = new PhaseConcentrationFraction(expectedMolConcFractions, expectedTemperature, "water", PhaseType.CONDENSED);
		PhaseConcentrationFraction actualPhaseFraction = waterPhase.calculatePhaseConcentrationFraction();
		assertEquals(expectedPhaseFraction, actualPhaseFraction);
	}
	
	@Test
	public void testCalculateConcentrationFractionsAllMolecules() {
		ArrayList<MoleculeConcentrationFraction> expectedMolConcFractions = new ArrayList<>();
		ArrayList<BoundConcentrationFraction> boundFractions = new ArrayList<>();
		boundFractions.add(new BoundConcentrationFraction(0.959164, "water", "water"));
		expectedMolConcFractions.add(new MoleculeConcentrationFraction("water", new FreeConcentrationFraction(0.040836, "water"), boundFractions));
		ArrayList<MoleculeConcentrationFraction> actualMolConcFractions = (ArrayList<MoleculeConcentrationFraction>) waterPhase.calculateConcentrationFractionsAllMolecules();
		assertEquals(expectedMolConcFractions, actualMolConcFractions);
	}
	
	@Test
	public void testCalculateConcentrationFractions() {
		ArrayList<BoundConcentrationFraction> boundFractions = new ArrayList<>();
		boundFractions.add(new BoundConcentrationFraction(0.959164, "water", "water"));
		MoleculeConcentrationFraction expectedMolConcFraction = new MoleculeConcentrationFraction("water", new FreeConcentrationFraction(0.040836, "water"), boundFractions);
		MoleculeConcentrationFraction actualMolConcFraction = waterPhase.calculateConcentrationFractions("water");
		assertEquals(expectedMolConcFraction, actualMolConcFraction);
	}
	
	@Test
	public void testConvertConcentrationUnitTo(){
		Phase convertedPhase = waterPhase.convertConcentrationUnitTo(ConcentrationUnit.MOLAR);
		assertEquals(ConcentrationUnit.MOLAR, convertedPhase.getConcentrationUnit());
		assertEquals(expectedTemperature, convertedPhase.getTemperature());
		assertEquals(5.901004091684357, convertedPhase.calculateConfinementEnergy("water"), ERROR);
		assertEquals(-31.696822405570174, convertedPhase.calculateBindingEnergy("water"), ERROR);
		double[] expectedtotalConcentrations = new double[] { 55.35, 55.35, 55.35, 55.35 };
		assertArrayEquals(expectedtotalConcentrations, convertedPhase.getTotalConcentrations().toArray(), ERROR);
	}
	
	@Test
	public void testGetFractionalOccupancy() {
		FractionalOccupancy fractionalOccupancy = waterPhase.getFractionalOccupancy("");
		assertEquals(new FractionalOccupancy(0.738, "water", "", 298.0), fractionalOccupancy);
	}
}
