/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertArrayEquals;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.junit.Test;


public class AssociationConstantsTest {

	private static final double ERROR = 1e-7;
	
	@Test
	public void testAssociationConstantConstructor() {
		Array2DRowRealMatrix associationConstantValues = new Array2DRowRealMatrix(new double[][]{{0.9537159952872262, 2.137865575311679, 121.00046313114248, 121.00046313114248},
				{2.137865575311679, 3.200818570079262, 24.080437731838927, 24.080437731838927},
				{121.00046313114248, 24.080437731838927, 7.517159672e-3, 7.517159672e-3},
				{121.00046313114248, 24.080437731838927, 7.517159672e-3, 7.517159672e-3}});
		AssociationConstants actualAssociationConstants = new AssociationConstants(associationConstantValues);
		assertNotNull(actualAssociationConstants);
		for (int i = 0; i < associationConstantValues.getRowDimension(); i++) {
			assertArrayEquals(associationConstantValues.getRow(i), actualAssociationConstants.getAssociationConstants().getRow(i), ERROR);
		}
		assertEquals(associationConstantValues, actualAssociationConstants.getAssociationConstants());
		
		
	}

}
