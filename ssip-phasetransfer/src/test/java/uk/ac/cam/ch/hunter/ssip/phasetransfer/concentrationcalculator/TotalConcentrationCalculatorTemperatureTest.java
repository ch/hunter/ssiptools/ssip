/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.HashMap;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;

@RunWith(JUnitParamsRunner.class)
public class TotalConcentrationCalculatorTemperatureTest {

	private static final Logger LOG = LogManager.getLogger(TotalConcentrationCalculatorTemperatureTest.class);
	
	private PhaseSolvent phaseSolvent;
	private HashMap<String, PhaseMolecule> solventMolecules;
	private ArrayList<SsipConcentration> ssipConcentrations;
	
	private static final double ERROR = 1.0e-7;
	
	@Before
	public void setUp() throws Exception {
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		//Use volume that matches that the Gaussian volume output from the old implementation.
		ssipSurfaceInformation.setVolumeVdW(21.06);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		Concentration moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, moleculeid);
		ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipWaterAlpha2Conc);
		ssipConcentrations.add(ssipWaterAlpha1Conc);
		ssipConcentrations.add(ssipWaterBeta1Conc);
		ssipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrations);
		
		PhaseMolecule phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		LOG.debug("volumes: {}", phaseMolecule.getVdWVolumes());
		phaseMolecule.setMoleFraction(1.0);
		solventMolecules = new HashMap<>();
		solventMolecules.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		
		String solventId = "water";
		String solventName = "water";
		phaseSolvent = new PhaseSolvent(solventMolecules, solventId, solventName, ConcentrationUnit.SSIPCONCENTRATION);
		
	}

	@Test
	@Parameters({
		"273.15, 0.18682295842827495",
		"283.15, 0.18614713102570735",
		"293.15, 0.18541268116305276",
		"303.15, 0.18461891104837125",
		"313.15, 0.18376530428701765",
		"323.15, 0.18285151499659752",
		"333.15, 0.18187735760030055",
		"343.15, 0.18084279718687984",
		"353.15, 0.17974794025657703",
		"363.15, 0.17859302575893954",
		"373.15, 0.1773784163119196",
		"383.15, 0.17610458953348224",
		"393.15, 0.1747721293941099",
		"403.15, 0.1733817176374198",
		"413.15, 0.17193412515528844",
		"423.15, 0.17043020338320705",
		"433.15, 0.16887087570254458",
		"443.15, 0.16725712888592442",
		"453.15, 0.1655900045464483",
		"463.15, 0.16387059076869748",
		"473.15, 0.16210001377223632",
		"483.15, 0.16027942976277343",
		"502.55, 0.15660931224130226",
		"533.15, 0.15046798296742808",
		"552.55, 0.1463643732354742",
		"583.15, 0.13958368820894312",
		"602.55, 0.13510136707281245",
		"633.15, 0.12776098595693944",
		"644.25, 0.12502020543162007",
		"647.02, 0.12432995144151773"
	})
	public void testCalculateTotalConcentrations(double temperature, double expectedConcentration) {
		TotalConcentrationCalculator totalConcentrationCalculator = new TotalConcentrationCalculator(phaseSolvent, temperature);
		totalConcentrationCalculator.calculateTotalConcentrations();
		ArrayRealVector actualConcentrations = totalConcentrationCalculator.getConcentrationCalculator().getTotalConcentrations();
		double[] expectedConcentrations = new double[]{expectedConcentration, expectedConcentration, expectedConcentration, expectedConcentration};
		assertArrayEquals(expectedConcentrations, actualConcentrations.toArray(), ERROR);
	}

}
