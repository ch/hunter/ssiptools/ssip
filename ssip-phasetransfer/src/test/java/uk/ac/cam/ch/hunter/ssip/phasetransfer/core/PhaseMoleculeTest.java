/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;

public class PhaseMoleculeTest {

	private PhaseMolecule phaseMolecule;
	private ArrayList<SsipConcentration> expectedSSIPConcentrationList;
	private ArrayList<SsipConcentration> expectedSsipConcentrationsNormalised;
	private SsipConcentrationContainer ssipConcentrationContainer;
	private SsipSurfaceInformation ssipSurfaceInformation;
	private CmlMolecule cmlMolecule;
	private String stdinchikey;
	private String moleculeid;
	private ArrayList<Integer> ssipIndices;
	
	static final double ERROR = 1e-7;
	
	@Before
	public void setUp() throws Exception {
		stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurface = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		ssipSurface.setVolumeVdW(21.4);
		ssipSurfaceInformation = new SsipSurfaceInformation(ssipSurface);
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "a", 2.0));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "a", -4.0));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(1.0, 0.0, 1.0, "a", -4.0));
		
		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ssipConcentration1.setFreeConcentration(0.24548651049082884);
		ssipConcentration1.setBoundConcentration(9.754513524877266);
		ssipConcentration1.setBoundConcentrationList(new ArrayRealVector(
				new double[] { 0.054441050948867135, 0.385785871734555,
						4.657143301096922, 4.657143301096922 }));
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ssipConcentration2.setFreeConcentration(0.05186668339004207);
		ssipConcentration2.setBoundConcentration(9.94813335569966);
		ssipConcentration2.setBoundConcentrationList(new ArrayRealVector(
				new double[] { 0.005131283597771884, 0.054441050948867135,
						4.944280510576511, 4.944280510576511 }));
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ssipConcentration3.setFreeConcentration(0.3939105349663359);
		ssipConcentration3.setBoundConcentration(9.606089427317157);
		ssipConcentration3.setBoundConcentrationList(new ArrayRealVector(
				new double[] { 4.944280510576511, 4.657143301096922,
						0.0023328078218614115, 0.0023328078218614115 }));
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ssipConcentration4.setFreeConcentration(0.3939105349663359);
		ssipConcentration4.setBoundConcentration(9.606089427317157);
		ssipConcentration4.setBoundConcentrationList(new ArrayRealVector(
				new double[] { 4.944280510576511, 4.657143301096922,
						0.0023328078218614115, 0.0023328078218614115 }));
		expectedSSIPConcentrationList = new ArrayList<>();
		expectedSSIPConcentrationList.add(ssipConcentration2);
		expectedSSIPConcentrationList.add(ssipConcentration1);
		expectedSSIPConcentrationList.add(ssipConcentration3);
		expectedSSIPConcentrationList.add(ssipConcentration4);
		ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, expectedSSIPConcentrationList);
		
		SsipConcentration ssipConcentration1norm = new SsipConcentration(ssip1,
				new Concentration(10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration2norm = new SsipConcentration(ssip2,
				new Concentration(10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration3norm = new SsipConcentration(ssip3,
				new Concentration(10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration4norm = new SsipConcentration(ssip4,
				new Concentration(10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		expectedSsipConcentrationsNormalised = new ArrayList<>();
		expectedSsipConcentrationsNormalised.add(ssipConcentration2norm);
		expectedSsipConcentrationsNormalised.add(ssipConcentration1norm);
		expectedSsipConcentrationsNormalised.add(ssipConcentration3norm);
		expectedSsipConcentrationsNormalised.add(ssipConcentration4norm);
		
		phaseMolecule = new PhaseMolecule(cmlMolecule, ssipSurfaceInformation, ssipConcentrationContainer, moleculeid, stdinchikey);
		
		ssipIndices = new ArrayList<>();
		ssipIndices.add(0);
		ssipIndices.add(1);
	}

	@Test
	public void testPhaseMolecule() {
		assertNotNull(phaseMolecule);
	}

	@Test
	public void testGetSSIPConcentrations(){
		assertEquals(expectedSSIPConcentrationList, phaseMolecule.getSSIPConcentrations());
	}
	
	@Test
	public void testGetTotalConcentrations(){
		double[] totalConcentrations = new double[]{10.0, 10.0, 10.0, 10.0};
		assertArrayEquals(totalConcentrations, phaseMolecule.getTotalConcentrations().toArray(), ERROR);
	}
	
	@Test
	public void testGetFreeConcentrations(){
		double[] expectedFreeConcentration = new double[] {0.05186668339004207, 0.24548651049082884, 0.3939105349663359,
				0.3939105349663359 };
		assertArrayEquals(expectedFreeConcentration, phaseMolecule.getFreeConcentrations().toArray(), ERROR);
	}
	
	@Test
	public void testGetBoundConcentrationValuesToOtherSSIPs() {
		double[][] expectedConcentrations = new double[][] {{0.0051312836, 0.0544410509},
			{0.0544410509, 0.3857858717},
			{4.9442805106, 4.6571433011},
			{4.9442805106, 4.6571433011}};
		Array2DRowRealMatrix actualConcentrations = phaseMolecule.getBoundConcentrationValuesToOtherSSIPs(ssipIndices);
		for (int i = 0; i < expectedConcentrations.length; i++) {
			assertArrayEquals(expectedConcentrations[i], actualConcentrations.getRow(i), ERROR);
		}
		
	}
	
	@Test
	public void testGetBoundConcentrationsToOtherSSIPs() {
		double[] expectedBoundConcentrations = new double[] {
				0.059572334546639016, 0.4402269226834221, 9.601423811673433, 9.601423811673433 };
		assertArrayEquals(expectedBoundConcentrations, phaseMolecule.getBoundConcentrationsToOtherSSIPs(ssipIndices).toArray(), ERROR);
	}
	
	@Test
	public void testGetBoundConcentrations(){
		double[] expectedBoundConcentrationTotal = new double[] {
				9.94813335569966, 9.754513524877266, 9.606089427317157,
				9.606089427317157 };
		assertArrayEquals(expectedBoundConcentrationTotal, phaseMolecule.getBoundConcentrations().toArray(), ERROR);
	}
	
	@Test
	public void testGetBoundConcentrationsMatrix(){
		Array2DRowRealMatrix expectedBoundConcentrationMatrix = new Array2DRowRealMatrix(new double[][] {{
				0.005131283597771884, 0.054441050948867135, 4.944280510576511,
				4.944280510576511}, {0.054441050948867135, 0.385785871734555,
				4.657143301096922, 4.657143301096922}, {4.944280510576511,
				4.657143301096922, 0.0023328078218614115,
				0.0023328078218614115}, {4.944280510576511, 4.657143301096922,
				0.0023328078218614115, 0.0023328078218614115 }});
		Array2DRowRealMatrix actualBoundConcentrationMatrix = phaseMolecule.getBoundConcentrationsMatrix();
		assertEquals(4, actualBoundConcentrationMatrix.getColumnDimension());
		assertEquals(4, actualBoundConcentrationMatrix.getRowDimension());
		for (int i = 0; i < expectedBoundConcentrationMatrix.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentrationMatrix.getRow(i),
					actualBoundConcentrationMatrix.getRow(i),
					ERROR);
		}
	}
	
	@Test
	public void testConvertConcentrationsTo(){
		PhaseMolecule convertedMolecule = phaseMolecule.convertConcentrationsTo(ConcentrationUnit.SSIPCONCENTRATION);
		assertEquals(expectedSsipConcentrationsNormalised, convertedMolecule.getSSIPConcentrations());
	}
	
	@Test
	public void testGetNumberOfOtherSSIPsInPhase() throws Exception{
		int numberOfssipsInPhase = phaseMolecule.getNumberOfOtherSSIPsInPhase();
		assertEquals(4, numberOfssipsInPhase);
	}
	
	@Test
	public void testCalculateFractionFree(){
		double[] expectedFractionFree = new double[]{0.005186668318729676, 0.024548650962258983, 0.039391053645202886, 0.039391053645202886};
		ArrayRealVector actualFractionFree = phaseMolecule.calculateFractionFree();
		assertArrayEquals(expectedFractionFree, actualFractionFree.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateLogFractionFree(){
		double[] expectedLogFractionFree = new double[]{-5.261663730378906, -3.7070983768722634, -3.2342165533039395, -3.2342165533039395};
		ArrayRealVector actualLogFractionFree = phaseMolecule.calculateLogFractionFree();
		assertArrayEquals(expectedLogFractionFree, actualLogFractionFree.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateLogFractionFreeSum(){
		assertEquals(-15.437195213859049, phaseMolecule.calculateLogFractionFreeSum(), ERROR);
	}
	
	@Test
	public void testCalculateMeanFractionFree() {
		double expectedFraction = 0.027129356595338567;
		assertEquals(expectedFraction, phaseMolecule.calculateMeanFractionFree(), ERROR);
	}
	
	@Test
	public void testCalculateMeanFractionBoundToGivenSSIPs() {
		double expectedFraction = 0.49256617201442315;
		assertEquals(expectedFraction, phaseMolecule.calculateMeanFractionBoundToGivenSSIPs(ssipIndices), ERROR);
	}
	
	@Test
	public void testCalculateFractionBoundToGivenSSIPs() {
		ArrayRealVector expectedValues = new ArrayRealVector(new double[] {0.005957233454663901, 0.04402269226834221, 0.9601423811673433, 0.9601423811673433});
		ArrayRealVector actualValues = phaseMolecule.calculateFractionBoundToGivenSSIPs(ssipIndices);
		assertArrayEquals(expectedValues.toArray(), actualValues.toArray(), ERROR);
	}
	
	@Test
	public void testGetMoleculeCentroid(){
		Point3D expectedCentroid = new Point3D(0.0, 1.0, 0.0);
		assertEquals(expectedCentroid, phaseMolecule.getMoleculeCentroid());
	}
	
	@Test
	public void testGetCmlMolecule() {
		assertEquals(moleculeid, phaseMolecule.getCmlMolecule().getMoleculeid());
		assertEquals(stdinchikey, phaseMolecule.getCmlMolecule().getStdinchikey());
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		assertEquals(atomlist, phaseMolecule.getCmlMolecule().getAtomArrayList());
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		HashMap<Integer, HashMap<Integer, String>> adjacenyRepresentation = new HashMap<>();
		HashMap<Integer, String> oxyMap = new HashMap<>();
		oxyMap.put(1, "1");
		oxyMap.put(2, "1");
		HashMap<Integer, String> h1Map = new HashMap<>();
		h1Map.put(0, "1");
		HashMap<Integer, String> h2Map = new HashMap<>();
		h2Map.put(0, "1");
		adjacenyRepresentation.put(0, oxyMap);
		adjacenyRepresentation.put(1, h1Map);
		adjacenyRepresentation.put(2, h2Map);
		assertEquals(bondMap, phaseMolecule.getCmlMolecule().getBondMap());
		assertEquals(adjacenyRepresentation, phaseMolecule.getCmlMolecule().getAdjacencyRepresentation());
	}

	@Test
	public void testGetSsipConcentrationContainer() {
		
		assertEquals(expectedSSIPConcentrationList, ssipConcentrationContainer.getSsipConcentrations());
	}
	
	@Test
	public void testGetSsipSurfaceInformation() {
		assertEquals(ssipSurfaceInformation, phaseMolecule.getSsipSurfaceInformation());
	}

	@Test
	public void testGetSoftwareVersion() {
		assertTrue(phaseMolecule.getSoftwareVersion().matches("^(?:(\\d+)\\.)?(?:(\\d+)\\.)?(.*)$"));
	}

	@Test
	public void testGetMoleculeID() {
		assertEquals(moleculeid, phaseMolecule.getMoleculeID());
	}

	@Test
	public void testGetStdinchikey() {
		assertEquals(stdinchikey, phaseMolecule.getStdinchikey());
	}
	
	@Test
	public void testGetVolumeFraction(){
		phaseMolecule.setMoleFraction(1.0);
		assertEquals(1.0, phaseMolecule.getMoleFraction(), ERROR);
		assertArrayEquals(new double[]{1.0, 1.0, 1.0, 1.0}, phaseMolecule.getMoleFractions().toArray(), ERROR);
	}

	@Test
	public void testGetVdWVolumes(){
		
		assertArrayEquals(new double[]{21.4, 21.4, 21.4, 21.4}, phaseMolecule.getVdWVolumes().toArray(), ERROR);
	}
}
