/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.w3c.dom.Attr;
import org.xmlunit.matchers.CompareMatcher;
import org.xmlunit.util.Nodes;
import org.xmlunit.util.Predicate;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.PhaseSystemConcentrationCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseAssembler;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.MixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SoluteContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;

public class MixtureXmlReaderTest {

	private static final Logger LOG = LogManager.getLogger(MixtureXmlReaderTest.class);

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	private URI solventFileURI;
	private URI soluteFileURI;
	
	private URI phaseCollectionURI;
	private URI phaseCollectionListURI;
	private URI expectedPhaseSystemURI;
	private URI expectedPhaseSystemContainerURI;
	private URI expectedContainerCalculatedURI;
	private URI expectedPhaseSystemMixtureURI;
	private URI expectedPhaseSystemMixtureContainerURI;
	private URI expectedPhaseSystemMixtureContainerWithSoluteURI;
	private URI actualPhaseSystemContainerFileURI;
	
	private Temperature expectedTemperature;
	private Phase expectedPhase;
	private Phase waterPhase;
	private Mixture waterMixture;
	private HashMap<String, Mixture> mixtureMap;
	private SoluteContainer expectedSoluteInformation;
	private SolventContainer expectedSolventInformation;
	private PhaseSystem phaseSystem;

	static final double ERROR = 1e-7;
	
	// Ignore the "dynamic" value of ssipSoftwareVersion in the testing
    final Predicate<Attr> attributeFilter = new Predicate<Attr>(){
        @Override
        public boolean test(Attr attrName) {
        	return (!Nodes.getQName(attrName).toString().contains("ssipSoftwareVersion"));
        }
    };
	
	@Before
	public void setUp() throws Exception {
		solventFileURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/waterphasesolvent.xml").toURI();
		soluteFileURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/watersolute.xml").toURI();
		
		phaseCollectionURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseCollection.xml").toURI();
		phaseCollectionListURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseCollectionList.xml").toURI();
		
		expectedPhaseSystemURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystem.xml").toURI();
		expectedPhaseSystemContainerURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystemContainer.xml").toURI();
		expectedContainerCalculatedURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystemContainerCalc.xml").toURI();
		expectedPhaseSystemMixtureURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystemMixture.xml").toURI();
		expectedPhaseSystemMixtureContainerURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystemMixtureContainer.xml").toURI();
		expectedPhaseSystemMixtureContainerWithSoluteURI = MixtureXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystemMixtureContainerWithSolute.xml").toURI();
		
		actualPhaseSystemContainerFileURI = tempFolder.newFile("phaseSystem.xml").toURI();

		expectedSoluteInformation = SoluteXmlReader.unmarshalContainer(soluteFileURI);
		expectedSolventInformation = SolventXmlReader.unmarshalContainer(solventFileURI);
		expectedTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		PhaseAssembler phaseAssembler = new PhaseAssembler();
		expectedPhase = phaseAssembler.generatePhaseFromSoluteAndSolvent(expectedSoluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute"), expectedSolventInformation.getSolventMap().get("water"), expectedTemperature, ConcentrationUnit.MOLAR);
		
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		
		String moleculeID = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		Concentration moleculeConcentration = new Concentration(55.35, ConcentrationUnit.MOLAR);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		String moleculeWater = "water";
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, "water");
		ArrayList<SsipConcentration> waterSsipConcentrations = new ArrayList<>();
		waterSsipConcentrations.add(ssipWaterAlpha1Conc);
		waterSsipConcentrations.add(ssipWaterAlpha2Conc);
		waterSsipConcentrations.add(ssipWaterBeta1Conc);
		waterSsipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer waterContainer = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, waterSsipConcentrations);
		SsipSurface waterSurfaceInformation = new SsipSurface(37.34, 18.46, 18.879, 0.002, 2200); 
		waterSurfaceInformation.setVolumeVdW(21.4);
		PhaseMolecule waterMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(waterSurfaceInformation), waterContainer, moleculeWater, moleculeWater);
		waterMolecule.setMoleFraction(1.0);
		HashSet<PhaseMolecule> waterPhaseMolecules = new HashSet<>();
		waterPhaseMolecules.add(waterMolecule);
		 
		Temperature phaseTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		PhaseFactory phaseFactory = new PhaseFactory();
		
		waterPhase = phaseFactory.generatePhase(waterPhaseMolecules, phaseTemperature, ConcentrationUnit.MOLAR);
		HashMap<String, Phase> phaseMap = new HashMap<>();
		phaseMap.put(moleculeID, waterPhase);
		waterMixture = new Mixture(phaseMap);
		mixtureMap = new HashMap<>();
		mixtureMap.put(moleculeID + "solute", waterMixture);
		
		Phase liquidPhase = waterPhase.convertConcentrationUnitTo(ConcentrationUnit.SSIPCONCENTRATION);
		LOG.debug("liquid phase: {}", liquidPhase.toString());
		LOG.trace("Liquid molecules: {}", liquidPhase.getPhaseMolecules());
		LOG.trace("Water molecule: {}", liquidPhase.getPhaseMolecules().get("water").toString());
		LOG.trace("Water molecule: {}", liquidPhase.getPhaseMolecules().get("water").getMoleFraction());
		LOG.trace("Water molecule: {}", liquidPhase.getPhaseMolecules().get("water").getSsipConcentrationContainer().getConcentrationUnit());
		LOG.trace("Water molecule: {}", liquidPhase.getPhaseMolecules().get("water").getSsipConcentrationContainer().getSsipConcentrations());
		PhaseSystemConcentrationCalculator phaseSysCalculator = new PhaseSystemConcentrationCalculator(liquidPhase);
		phaseSysCalculator.calculateSpeciation();
		phaseSystem = new PhaseSystem(phaseSysCalculator.getGasPhase(), phaseSysCalculator.getLiquidPhase(), "water");
	}

	@Test
	public void testUnmarshal(){
		try {
			Mixture readInMixture = MixtureXmlReader.unmarshal(phaseCollectionURI);
			assertEquals(waterMixture.getPhaseMap().keySet(), readInMixture.getPhaseMap().keySet());
			assertEquals(waterPhase.getTemperature(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getTemperature());
			for (int i = 0; i < waterPhase.getAssociationConstants().getAssociationConstants().getRowDimension(); i++) {
				assertArrayEquals(waterPhase.getAssociationConstants().getAssociationConstants().getRow(i), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getAssociationConstants().getAssociationConstants().getRow(i), ERROR);
			}
			assertArrayEquals(waterPhase.getFreeConcentrations().toArray(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getFreeConcentrations().toArray(), ERROR);
			assertArrayEquals(waterPhase.getBoundConcentrations().toArray(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getBoundConcentrations().toArray(), ERROR);
			for (int i = 0; i < waterPhase.getBoundConcentrationsMatrix().getRowDimension(); i++) {
				assertArrayEquals(waterPhase.getBoundConcentrationsMatrix().getRow(i), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getBoundConcentrationsMatrix().getRow(i), ERROR);
			}
			assertArrayEquals(waterPhase.getTotalConcentrations().toArray(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getTotalConcentrations().toArray(), ERROR);
			assertEquals(waterPhase.getsortedSSIPConcentrations(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getsortedSSIPConcentrations());
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
	@Test
	public void testUnmarshalContainer(){
		try {
			MixtureContainer readInMixtureContainer = MixtureXmlReader.unmarshalContainer(phaseCollectionListURI);
			LOG.debug("expected key set: {}", mixtureMap.keySet());
			LOG.debug("actual key set: {}", readInMixtureContainer.getMixtureMap().keySet());
			assertEquals(mixtureMap.keySet(), readInMixtureContainer.getMixtureMap().keySet());
			Mixture readInMixture = readInMixtureContainer.getMixtureMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute");
			assertEquals(waterMixture.getPhaseMap().keySet(), readInMixture.getPhaseMap().keySet());
			assertEquals(waterPhase.getTemperature(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getTemperature());
			for (int i = 0; i < waterPhase.getAssociationConstants().getAssociationConstants().getRowDimension(); i++) {
				assertArrayEquals(waterPhase.getAssociationConstants().getAssociationConstants().getRow(i), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getAssociationConstants().getAssociationConstants().getRow(i), ERROR);
			}
			
			assertArrayEquals(waterPhase.getFreeConcentrations().toArray(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getFreeConcentrations().toArray(), ERROR);
			assertArrayEquals(waterPhase.getBoundConcentrations().toArray(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getBoundConcentrations().toArray(), ERROR);
			for (int i = 0; i < waterPhase.getBoundConcentrationsMatrix().getRowDimension(); i++) {
				assertArrayEquals(waterPhase.getBoundConcentrationsMatrix().getRow(i), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getBoundConcentrationsMatrix().getRow(i), ERROR);
			}
			
			assertArrayEquals(waterPhase.getTotalConcentrations().toArray(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getTotalConcentrations().toArray(), ERROR);
			assertEquals(waterPhase.getsortedSSIPConcentrations(), readInMixture.getPhaseMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getsortedSSIPConcentrations());
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
	@Test
	public void testUnmarshalPhaseSystem(){
		try {
			PhaseSystem actualPhaseSystem = MixtureXmlReader.unmarshalPhaseSystem(expectedPhaseSystemURI);
			assertEquals(phaseSystem.getGasPhase().getPhaseMolecules().keySet(), actualPhaseSystem.getGasPhase().getPhaseMolecules().keySet());
			assertArrayEquals(phaseSystem.getGasPhase().getTotalConcentrations().toArray(), actualPhaseSystem.getGasPhase().getTotalConcentrations().toArray(), ERROR);
			assertEquals(phaseSystem.getCondensedPhase().getPhaseMolecules().keySet(), actualPhaseSystem.getCondensedPhase().getPhaseMolecules().keySet());
			assertArrayEquals(phaseSystem.getCondensedPhase().getTotalConcentrations().toArray(), actualPhaseSystem.getCondensedPhase().getTotalConcentrations().toArray(), ERROR);
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
	@Test
	public void testUnmarshalPhaseSystemContainer(){
		try {
			PhaseSystemContainer actualPhaseSystemContainer = MixtureXmlReader.unmarshalPhaseSystemContainer(expectedPhaseSystemContainerURI);
			PhaseSystem actualPhaseSystem = actualPhaseSystemContainer.getPhaseSystemMap().get("water");
			assertEquals(phaseSystem.getGasPhase().getPhaseMolecules().keySet(), actualPhaseSystem.getGasPhase().getPhaseMolecules().keySet());
			assertArrayEquals(phaseSystem.getGasPhase().getTotalConcentrations().toArray(), actualPhaseSystem.getGasPhase().getTotalConcentrations().toArray(), ERROR);
			assertEquals(phaseSystem.getCondensedPhase().getPhaseMolecules().keySet(), actualPhaseSystem.getCondensedPhase().getPhaseMolecules().keySet());
			assertArrayEquals(phaseSystem.getCondensedPhase().getTotalConcentrations().toArray(), actualPhaseSystem.getCondensedPhase().getTotalConcentrations().toArray(), ERROR);
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
	@Test
	public void testUnMarshalPhaseSystemMixture(){
		try {
			PhaseSystemMixture container = MixtureXmlReader.unmarshalPhaseSystemMixture(expectedPhaseSystemMixtureURI);

			MixtureXmlWriter.marshalPhaseSystemMixture(container, actualPhaseSystemContainerFileURI);
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualPhaseSystemContainerFileURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedPhaseSystemMixtureURI)));
			LOG.trace(actualFileContents);
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		} catch (IOException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
	@Test
	public void testUnMarshalPhaseSystemMixtureContainer(){
		try {
			PhaseSystemMixtureContainer container = MixtureXmlReader.unmarshalPhaseSystemMixtureContainer(expectedPhaseSystemMixtureContainerURI);

			MixtureXmlWriter.marshalPhaseSystemMixtureContainer(container, actualPhaseSystemContainerFileURI);
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualPhaseSystemContainerFileURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedPhaseSystemMixtureContainerURI)));
			LOG.trace(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		} catch (IOException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
	@Test
	public void testParseSoluteXMLFile() throws Exception{
		SoluteContainer actualSoluteInformation =  MixtureXmlReader.parseSoluteXMLFile(soluteFileURI);
		assertEquals(expectedSoluteInformation.getPhaseSolutes().keySet(), actualSoluteInformation.getPhaseSolutes().keySet());
		assertEquals(expectedSoluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute").getSoluteID(), actualSoluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute").getSoluteID());
		assertEquals(expectedSoluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute").getPhaseMolecule().getSSIPConcentrations(), actualSoluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute").getPhaseMolecule().getSSIPConcentrations());
	}
	
	@Test
	public void testParseSolventXMLFile() throws Exception{
		SolventContainer actualSolventInformation = MixtureXmlReader.parseSolventXMLFile(solventFileURI);
		assertEquals(expectedSolventInformation.getSolventMap().keySet(), actualSolventInformation.getSolventMap().keySet());
		assertEquals(expectedSolventInformation.getSolventMap().get("water").getSolventId(), actualSolventInformation.getSolventMap().get("water").getSolventId());
		assertEquals(expectedSolventInformation.getSolventMap().get("water").getSolventName(), actualSolventInformation.getSolventMap().get("water").getSolventName());
		assertEquals(expectedSolventInformation.getSolventMap().get("water").getPhaseMolecules().keySet(), actualSolventInformation.getSolventMap().get("water").getPhaseMolecules().keySet());
	}
	
	@Test
	public void testGenerateMixtureForSolutes() throws Exception{
		Map<String, Mixture> actualMixtureMap = MixtureXmlReader.generateMixtureForSolutes(expectedSoluteInformation, expectedSolventInformation, expectedTemperature, ConcentrationUnit.MOLAR);
		
		for (Map.Entry<String, Mixture> mixtureMapEntry : actualMixtureMap.entrySet()) {
			assertEquals("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", mixtureMapEntry.getKey());
			HashMap<String, Phase> actualPhaseMap = (HashMap<String, Phase>) mixtureMapEntry.getValue().getPhaseMap();
			for (Map.Entry<String, Phase> phaseMapEntry : actualPhaseMap.entrySet()) {
				assertEquals("water", phaseMapEntry.getKey());
				Phase actualPhase = phaseMapEntry.getValue();
				assertEquals(expectedPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
				assertEquals(expectedPhase.getTemperature(), actualPhase.getTemperature());
				assertEquals(expectedPhase.getsortedSSIPConcentrations(), actualPhase.getsortedSSIPConcentrations());
			}
		}
	}
	
	@Test
	public void testGenerateMixturesForSolutesFromFiles() {
		try {
			MixtureContainer actualMixtureContainer = MixtureXmlReader.generateMixturesForSolutesFromFiles(soluteFileURI, solventFileURI, expectedTemperature, ConcentrationUnit.MOLAR, false);
			for (Map.Entry<String, Mixture> mixtureMapEntry : actualMixtureContainer.getMixtureMap().entrySet()) {
				assertEquals("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", mixtureMapEntry.getKey());
				HashMap<String, Phase> actualPhaseMap = (HashMap<String, Phase>) mixtureMapEntry.getValue().getPhaseMap();
				for (Map.Entry<String, Phase> phaseMapEntry : actualPhaseMap.entrySet()) {
					assertEquals("water", phaseMapEntry.getKey());
					Phase actualPhase = phaseMapEntry.getValue();
					assertEquals(expectedPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
					assertEquals(expectedPhase.getTemperature(), actualPhase.getTemperature());
					assertEquals(expectedPhase.getsortedSSIPConcentrations(), actualPhase.getsortedSSIPConcentrations());
					for (int i = 0; i < expectedPhase.getAssociationConstants().getAssociationConstants().getRowDimension(); i++) {
						assertArrayEquals(expectedPhase.getAssociationConstants().getAssociationConstants().getRow(i), actualPhase.getAssociationConstants().getAssociationConstants().getRow(i), ERROR);
					}
					
					assertArrayEquals(expectedPhase.getFreeConcentrations().toArray(), actualPhase.getFreeConcentrations().toArray(), ERROR);
					assertArrayEquals(expectedPhase.getBoundConcentrations().toArray(), actualPhase.getBoundConcentrations().toArray(), ERROR);
					for (int i = 0; i < expectedPhase.getBoundConcentrationsMatrix().getRowDimension(); i++) {
						assertArrayEquals(expectedPhase.getBoundConcentrationsMatrix().getRow(i), actualPhase.getBoundConcentrationsMatrix().getRow(i), ERROR);
					}
					assertArrayEquals(expectedPhase.getTotalConcentrations().toArray(), actualPhase.getTotalConcentrations().toArray(), ERROR);
				}
			}
		} catch (JAXBException e) {
			LOG.error("error: {}", e);
			fail("Should not go here");
		}
		
		
	}

	@Test
	public void testParsePhaseCollectionListFileAndCalculateConcentrations(){
		MixtureContainer actualMixtureContainer;
		try {
			actualMixtureContainer = MixtureXmlReader.parsePhaseCollectionListFileAndCalculateConcentrations(phaseCollectionListURI, false);
			for (Map.Entry<String, Mixture> mixtureMapEntry : actualMixtureContainer.getMixtureMap().entrySet()) {
				assertEquals("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", mixtureMapEntry.getKey());
				Mixture actualMixture = mixtureMapEntry.getValue();
				HashMap<String, Phase> actualPhaseMap = (HashMap<String, Phase>) actualMixture.getPhaseMap();
				for (Map.Entry<String, Phase> mapEntry : actualPhaseMap.entrySet()) {
					assertEquals("XLYOFNOQVPJJNP-UHFFFAOYSA-N", mapEntry.getKey());
					Phase actualPhase = mapEntry.getValue();
					assertEquals(waterPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
					assertEquals(waterPhase.getTemperature(), actualPhase.getTemperature());
					for (int i = 0; i < waterPhase.getAssociationConstants().getAssociationConstants().getRowDimension(); i++) {
						assertArrayEquals(waterPhase.getAssociationConstants().getAssociationConstants().getRow(i), actualPhase.getAssociationConstants().getAssociationConstants().getRow(i), ERROR);
					}
					assertArrayEquals(waterPhase.getFreeConcentrations().toArray(), actualPhase.getFreeConcentrations().toArray(), ERROR);
					assertArrayEquals(waterPhase.getBoundConcentrations().toArray(), actualPhase.getBoundConcentrations().toArray(), ERROR);
					for (int i = 0; i < waterPhase.getBoundConcentrationsMatrix().getRowDimension(); i++) {
						assertArrayEquals(waterPhase.getBoundConcentrationsMatrix().getRow(i), actualPhase.getBoundConcentrationsMatrix().getRow(i), ERROR);
					}
					assertArrayEquals(waterPhase.getTotalConcentrations().toArray(), actualPhase.getTotalConcentrations().toArray(), ERROR);
					assertEquals(waterPhase.getsortedSSIPConcentrations(), actualPhase.getsortedSSIPConcentrations());
				}
			}
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}

	@Test
	public void testParsePhaseCollectionFileAndCalculateConcentrations(){
		Mixture actualMixture;
		try {
			actualMixture = MixtureXmlReader.parsePhaseCollectionFileAndCalculateConcentrations(phaseCollectionURI, false);
			HashMap<String, Phase> actualPhaseMap = (HashMap<String, Phase>) actualMixture.getPhaseMap();
			for (Map.Entry<String, Phase> mapEntry : actualPhaseMap.entrySet()) {
				assertEquals("XLYOFNOQVPJJNP-UHFFFAOYSA-N", mapEntry.getKey());
				Phase actualPhase = mapEntry.getValue();
				assertEquals(waterPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
				assertEquals(waterPhase.getTemperature(), actualPhase.getTemperature());
				assertEquals(waterPhase.getsortedSSIPConcentrations(), actualPhase.getsortedSSIPConcentrations());
				for (int i = 0; i < waterPhase.getAssociationConstants().getAssociationConstants().getRowDimension(); i++) {
					assertArrayEquals(waterPhase.getAssociationConstants().getAssociationConstants().getRow(i), actualPhase.getAssociationConstants().getAssociationConstants().getRow(i), ERROR);
				}
				assertArrayEquals(waterPhase.getFreeConcentrations().toArray(), actualPhase.getFreeConcentrations().toArray(), ERROR);
				assertArrayEquals(waterPhase.getBoundConcentrations().toArray(), actualPhase.getBoundConcentrations().toArray(), ERROR);
				for (int i = 0; i < waterPhase.getBoundConcentrationsMatrix().getRowDimension(); i++) {
					assertArrayEquals(waterPhase.getBoundConcentrationsMatrix().getRow(i), actualPhase.getBoundConcentrationsMatrix().getRow(i), ERROR);
				}
				assertArrayEquals(waterPhase.getTotalConcentrations().toArray(), actualPhase.getTotalConcentrations().toArray(), ERROR);
			}
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}	
	}
	
	@Test
	public void testCalculatePhaseSystemsFromMixture(){
		try {
			PhaseSystemContainer container = MixtureXmlReader.calculatePhaseSystemsFromMixture(phaseCollectionURI, true);
			LOG.debug("PhaseSystems: {}", container.getPhaseSystemMap().keySet());
			LOG.debug("PhaseSystem phases: {}", container.getPhaseSystemMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getPhaseMap().keySet());
			LOG.debug("PhaseSystem Gas phase solventID: {}", container.getPhaseSystemMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getGasPhase().getSolventID());
			LOG.debug("PhaseSystem Condensed phase solventID: {}", container.getPhaseSystemMap().get("XLYOFNOQVPJJNP-UHFFFAOYSA-N").getCondensedPhase().getSolventID());
			MixtureXmlWriter.marshalPhaseSystemContainer(container, actualPhaseSystemContainerFileURI);
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualPhaseSystemContainerFileURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedContainerCalculatedURI)));
			LOG.trace(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		} catch (IOException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
	@Test
	public void testCalculatePhaseSystemsWithSolutes(){
		try {
			PhaseSystemMixtureContainer container = MixtureXmlReader.calculatePhaseSystemsWithSolutes(phaseCollectionURI, soluteFileURI);

			MixtureXmlWriter.marshalPhaseSystemMixtureContainer(container, actualPhaseSystemContainerFileURI);
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualPhaseSystemContainerFileURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedPhaseSystemMixtureContainerWithSoluteURI)));
			LOG.trace(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		} catch (IOException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
	
	@Test
	public void testParsePhaseCollectionListFileAndReadConcentrations(){
		MixtureContainer actualMixtureContainer;
		try {
			actualMixtureContainer = MixtureXmlReader.parsePhaseCollectionListFileAndReadConcentrations(phaseCollectionListURI);
			for (Map.Entry<String, Mixture> mixtureMapEntry : actualMixtureContainer.getMixtureMap().entrySet()) {
				assertEquals("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", mixtureMapEntry.getKey());
				Mixture actualMixture = mixtureMapEntry.getValue();
				HashMap<String, Phase> actualPhaseMap = (HashMap<String, Phase>) actualMixture.getPhaseMap();
				for (Map.Entry<String, Phase> mapEntry : actualPhaseMap.entrySet()) {
					assertEquals("XLYOFNOQVPJJNP-UHFFFAOYSA-N", mapEntry.getKey());
					Phase actualPhase = mapEntry.getValue();
					assertEquals(waterPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
					assertEquals(waterPhase.getTemperature(), actualPhase.getTemperature());
					assertEquals(waterPhase.getsortedSSIPConcentrations(), actualPhase.getsortedSSIPConcentrations());
					for (int i = 0; i < waterPhase.getAssociationConstants().getAssociationConstants().getRowDimension(); i++) {
						assertArrayEquals(waterPhase.getAssociationConstants().getAssociationConstants().getRow(i), actualPhase.getAssociationConstants().getAssociationConstants().getRow(i), ERROR);
					}
					assertArrayEquals(waterPhase.getFreeConcentrations().toArray(), actualPhase.getFreeConcentrations().toArray(), ERROR);
					assertArrayEquals(waterPhase.getBoundConcentrations().toArray(), actualPhase.getBoundConcentrations().toArray(), ERROR);
					for (int i = 0; i < waterPhase.getBoundConcentrationsMatrix().getRowDimension(); i++) {
						assertArrayEquals(waterPhase.getBoundConcentrationsMatrix().getRow(i), actualPhase.getBoundConcentrationsMatrix().getRow(i), ERROR);
					}
					assertArrayEquals(waterPhase.getTotalConcentrations().toArray(), actualPhase.getTotalConcentrations().toArray(), ERROR);
				}
			}
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}
		
	}
	
	@Test
	public void testParsePhaseCollectionFileAndReadConcentrations(){
		Mixture actualMixture;
		try {
			actualMixture = MixtureXmlReader.parsePhaseCollectionFileAndReadConcentrations(phaseCollectionURI);
			HashMap<String, Phase> actualPhaseMap = (HashMap<String, Phase>) actualMixture.getPhaseMap();
			for (Map.Entry<String, Phase> mapEntry : actualPhaseMap.entrySet()) {
				assertEquals("XLYOFNOQVPJJNP-UHFFFAOYSA-N", mapEntry.getKey());
				Phase actualPhase = mapEntry.getValue();
				assertEquals(waterPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
				assertEquals(waterPhase.getTemperature(), actualPhase.getTemperature());
				assertEquals(waterPhase.getsortedSSIPConcentrations(), actualPhase.getsortedSSIPConcentrations());
				for (int i = 0; i < waterPhase.getAssociationConstants().getAssociationConstants().getRowDimension(); i++) {
					assertArrayEquals(waterPhase.getAssociationConstants().getAssociationConstants().getRow(i), actualPhase.getAssociationConstants().getAssociationConstants().getRow(i), ERROR);
				}
				assertArrayEquals(waterPhase.getFreeConcentrations().toArray(), actualPhase.getFreeConcentrations().toArray(), ERROR);
				assertArrayEquals(waterPhase.getBoundConcentrations().toArray(), actualPhase.getBoundConcentrations().toArray(), ERROR);
				for (int i = 0; i < waterPhase.getBoundConcentrationsMatrix().getRowDimension(); i++) {
					assertArrayEquals(waterPhase.getBoundConcentrationsMatrix().getRow(i), actualPhase.getBoundConcentrationsMatrix().getRow(i), ERROR);
				}
				assertArrayEquals(waterPhase.getTotalConcentrations().toArray(), actualPhase.getTotalConcentrations().toArray(), ERROR);
			}
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}
			
	}
}
