/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolute;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseType;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.io.SoluteXmlReader;

public class PhaseSystemConcentrationCalculatorWithSoluteTest {

	private static final Logger LOG = LogManager.getLogger(PhaseSystemConcentrationCalculatorWithSoluteTest.class);
	
	private PhaseSolute soluteInformation;
	private PhaseSystemConcentrationCalculator waterSystemCalculator; 
	private Phase liquidPhase;
	private PhaseSystem waterPhaseSystem;
	private HashMap<String, PhaseMolecule> phaseMolecules;
	private ArrayList<SsipConcentration> ssipConcentrations;
	private double temperature;
	
	private static final double ERROR = 1e-10;
	
	@Before
	public void setUp() throws Exception {
		URI soluteFileURI = PhaseSystemConcentrationCalculatorWithSoluteTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/watersolute.xml").toURI();

		soluteInformation =  SoluteXmlReader.unmarshalContainer(soluteFileURI).getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute");
		
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "water";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		//volumes in cubic angstroms. taken from DOI: 10.1039/c2sc21666c
		ssipSurfaceInformation.setVolumeVdW(21.4);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		Concentration moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, moleculeid);
		ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipWaterAlpha2Conc);
		ssipConcentrations.add(ssipWaterAlpha1Conc);
		ssipConcentrations.add(ssipWaterBeta1Conc);
		ssipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrations);
		
		PhaseMolecule phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		LOG.debug("volumes: {}", phaseMolecule.getVdWVolumes());
		phaseMolecule.setMoleFraction(1.0);
		phaseMolecules = new HashMap<>();
		phaseMolecules.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		
		String solventId = "water";
		//temperature in kelvin.
		temperature = 298.0;
		
		liquidPhase = new Phase(phaseMolecules, new Temperature(temperature, TemperatureUnit.KELVIN), ConcentrationUnit.SSIPCONCENTRATION);
		liquidPhase.setSolventID(solventId);
		liquidPhase.setPhaseType(PhaseType.CONDENSED);
		
		waterSystemCalculator = new PhaseSystemConcentrationCalculator(liquidPhase);
		waterPhaseSystem = waterSystemCalculator.getPhaseSystem(solventId).convertConcentrationUnitTo(ConcentrationUnit.MOLAR);
	}

	@Test
	public void testCreatePhaseSystem() {
		PhaseSystem actualPhaseSystem = PhaseSystemConcentrationCalculatorWithSolute.createPhaseSystem(liquidPhase, "water");
		actualPhaseSystem = actualPhaseSystem.convertConcentrationUnitTo(ConcentrationUnit.MOLAR);
		assertArrayEquals(waterPhaseSystem.getCondensedPhase().getBoundConcentrations().toArray(), actualPhaseSystem.getCondensedPhase().getBoundConcentrations().toArray(), ERROR);
		assertArrayEquals(waterPhaseSystem.getCondensedPhase().getFreeConcentrations().toArray(), actualPhaseSystem.getCondensedPhase().getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(waterPhaseSystem.getCondensedPhase().getTotalConcentrations().toArray(), actualPhaseSystem.getCondensedPhase().getTotalConcentrations().toArray(), ERROR);
		assertArrayEquals(waterPhaseSystem.getGasPhase().getBoundConcentrations().toArray(), actualPhaseSystem.getGasPhase().getBoundConcentrations().toArray(), ERROR);
		assertArrayEquals(waterPhaseSystem.getGasPhase().getFreeConcentrations().toArray(), actualPhaseSystem.getGasPhase().getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(waterPhaseSystem.getGasPhase().getTotalConcentrations().toArray(), actualPhaseSystem.getGasPhase().getTotalConcentrations().toArray(), ERROR);
	}

	@Test
	public void testGeneratePhaseWithSolute() {
		Phase phaseWithSolute = PhaseSystemConcentrationCalculatorWithSolute.generatePhaseWithSolute(waterPhaseSystem.getCondensedPhase(), soluteInformation);
		double[] boundConcentrations = new double[]{9.589618137438365E-4, 9.589618137438365E-4, 9.589618137438365E-4, 9.589618137438365E-4, 52.54397634971279, 52.54397634971279, 52.54397634971279, 52.54397634971279};
		double[] freeConcentrations = new double[]{4.103818639780965E-5, 4.103818639780965E-5, 4.103818639780965E-5, 4.103818639780965E-5, 2.2485875184304374, 2.2485875184304374, 2.248587518430437, 2.248587518430437};
		double[] totalConcentrations = new double[]{0.001, 0.001, 0.001, 0.001, 54.79256386813412, 54.79256386813412, 54.79256386813412, 54.79256386813412};
		assertArrayEquals(boundConcentrations, phaseWithSolute.getBoundConcentrations().toArray(), ERROR);
		assertArrayEquals(freeConcentrations, phaseWithSolute.getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(totalConcentrations, phaseWithSolute.getTotalConcentrations().toArray(), ERROR);
	}

	@Test
	public void testGeneratePhaseSystemWithSolute() {
		PhaseSystem actualPhaseSystem = PhaseSystemConcentrationCalculatorWithSolute.generatePhaseSystemWithSolute(liquidPhase.convertConcentrationUnitTo(ConcentrationUnit.MOLAR), soluteInformation, "water");
		double[] boundConcentrations = new double[]{9.589618137438365E-4, 9.589618137438365E-4, 9.589618137438365E-4, 9.589618137438365E-4, 52.54397634971279, 52.54397634971279, 52.54397634971279, 52.54397634971279};
		double[] freeConcentrations = new double[]{4.103818639780965E-5, 4.103818639780965E-5, 4.103818639780965E-5, 4.103818639780965E-5, 2.2485875184304374, 2.2485875184304374, 2.248587518430437, 2.248587518430437};
		double[] totalConcentrations = new double[]{0.001, 0.001, 0.001, 0.001, 54.79256386813412, 54.79256386813412, 54.79256386813412, 54.79256386813412};
		assertArrayEquals(boundConcentrations, actualPhaseSystem.getCondensedPhase().getBoundConcentrations().toArray(), ERROR);
		assertArrayEquals(freeConcentrations, actualPhaseSystem.getCondensedPhase().getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(totalConcentrations, actualPhaseSystem.getCondensedPhase().getTotalConcentrations().toArray(), ERROR);
		assertArrayEquals(waterPhaseSystem.getGasPhase().getBoundConcentrations().toArray(), actualPhaseSystem.getGasPhase().getBoundConcentrations().toArray(), ERROR);
		assertArrayEquals(waterPhaseSystem.getGasPhase().getFreeConcentrations().toArray(), actualPhaseSystem.getGasPhase().getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(waterPhaseSystem.getGasPhase().getTotalConcentrations().toArray(), actualPhaseSystem.getGasPhase().getTotalConcentrations().toArray(), ERROR);
	}

}
