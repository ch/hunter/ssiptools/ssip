/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Node;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.ComparisonType;
import org.xmlunit.diff.DifferenceEvaluator;

public class ConcentrationDifferenceEvaluator implements DifferenceEvaluator {

	private static final Logger LOG = LogManager
			.getLogger(ConcentrationDifferenceEvaluator.class);
	
	private static final double ERROR = 1e-8;
	
	@Override
	public ComparisonResult evaluate(Comparison comparison, ComparisonResult outcome) {
		if (outcome == ComparisonResult.EQUAL) return outcome;
		LOG.debug("comparison: {}", comparison.toString());
		LOG.debug("comparison type: {}", comparison.getType().toString());
		if (comparison.getType().equals(ComparisonType.TEXT_VALUE) || comparison.getType().equals(ComparisonType.ATTR_VALUE)) {
			final Node controlNode = comparison.getControlDetails().getTarget();
			final Node testNode = comparison.getTestDetails().getTarget();
			double controlValue = Double.parseDouble(controlNode.getTextContent());
			double testValue = Double.parseDouble(testNode.getTextContent());
			LOG.trace("controlValue: {}  testValue: {}", controlNode, testValue);
			if (Math.abs(controlValue - testValue) < ERROR) {
				return ComparisonResult.EQUAL;
			}
			else {
				return ComparisonResult.DIFFERENT;
			}
		}
		return ComparisonResult.DIFFERENT;
	}

}
