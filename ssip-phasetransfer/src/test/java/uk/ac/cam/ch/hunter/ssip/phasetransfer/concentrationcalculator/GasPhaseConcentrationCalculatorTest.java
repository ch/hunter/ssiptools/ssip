/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstantsFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;

public class GasPhaseConcentrationCalculatorTest {

	private static final Logger LOG = LogManager.getLogger(GasPhaseConcentrationCalculatorTest.class);
	
	private ArrayList<SsipConcentration> ssipConcentrations;
	private ArrayList<SsipConcentration> gasSsipConcentrations;
	private HashMap<String, ArrayList<SsipConcentration>> ssipConcsByID;
	private ArrayRealVector totalConcentrations;
	private double temperature;
	private GasPhaseConcentrationCalculator gasConcentrationCalculator;
	
	private static final double ERROR = 1.0e-14;
	
	@Before
	public void setUp() throws Exception {
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		Concentration moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		Concentration gasConcentration = new Concentration(0.0001/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, moleculeid);
		ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipWaterAlpha2Conc);
		ssipConcentrations.add(ssipWaterAlpha1Conc);
		ssipConcentrations.add(ssipWaterBeta1Conc);
		ssipConcentrations.add(ssipWaterBeta2Conc);
		ssipConcsByID = new HashMap<>();
		ssipConcsByID.put(moleculeid, ssipConcentrations);
		SsipConcentration ssipGasAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				gasConcentration, moleculeid);
		SsipConcentration ssipGasAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				gasConcentration, moleculeid);
		SsipConcentration ssipGasBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				gasConcentration, moleculeid);
		SsipConcentration ssipGasBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				gasConcentration, moleculeid);
		gasSsipConcentrations = new ArrayList<>();
		gasSsipConcentrations.add(ssipGasAlpha2Conc);
		gasSsipConcentrations.add(ssipGasAlpha1Conc);
		gasSsipConcentrations.add(ssipGasBeta1Conc);
		gasSsipConcentrations.add(ssipGasBeta2Conc);
		temperature = 298.0;
		
		totalConcentrations = new ArrayRealVector(new double[]{0.0001/300, 0.0001/300, 0.0001/300, 0.0001/300});
		
		AssociationConstantsFactory associationConstantsFactory = new AssociationConstantsFactory(temperature);
		AssociationConstants associationConstants = associationConstantsFactory.getAssociationConstants(ssipConcentrations);
		ConcentrationCalculator concentrationCalculator = new ConcentrationCalculator(associationConstants.getAssociationConstants(), totalConcentrations);
		concentrationCalculator.calculateFreeAndBoundConcentrations();
		gasConcentrationCalculator = new GasPhaseConcentrationCalculator(concentrationCalculator, ssipConcentrations);
	}

	@Test
	public void testSetConcentrations() {
		gasConcentrationCalculator.setConcentrations();
		double[] boundConcentrationTotals = new double[]{3.456789526465048E-10, 3.456789526465048E-10, 3.4567895264650483E-10, 3.4567895264650483E-10};
		double[] freeConcentrations = new double[]{3.3298765440081544E-7, 3.3298765440081544E-7, 3.3298765440081544E-7, 3.3298765440081544E-7};
		double[][] boundconcentrations = new double[][]{{1.0632926729875642E-12, 1.0632926729875642E-12, 1.7177673398663133E-10, 1.7177673398663133E-10},
				{1.0632926729875642E-12, 1.0632926729875642E-12, 1.7177673398663133E-10, 1.7177673398663133E-10},
				{1.7177673398663133E-10, 1.7177673398663133E-10, 1.0632926729875642E-12, 1.0632926729875642E-12},
				{1.7177673398663133E-10, 1.7177673398663133E-10, 1.0632926729875642E-12, 1.0632926729875642E-12}};
		assertArrayEquals(freeConcentrations, gasConcentrationCalculator.getConcentrationCalculator().getFreeConcentrations().toArray(), ERROR);
		for (int i = 0; i < gasConcentrationCalculator.getSsipConcentrationList().size(); i++) {
			LOG.debug("i: {}", i);
			LOG.debug("array: {}", boundconcentrations[i]);
			SsipConcentration ssipConcentration = gasConcentrationCalculator.getSsipConcentrationList().get(i);
			LOG.debug("free conc: {}", ssipConcentration.getFreeConcentration());
			assertEquals(totalConcentrations.getEntry(i), ssipConcentration.getTotalConcentration(), ERROR);
			assertEquals(freeConcentrations[i], ssipConcentration.getFreeConcentration(), ERROR);
			assertEquals(boundConcentrationTotals[i], ssipConcentration.getBoundConcentration(), ERROR);
			assertArrayEquals(boundconcentrations[i], ssipConcentration.getBoundConcentrationList().toArray(), ERROR);
		assertEquals(gasSsipConcentrations, gasConcentrationCalculator.getSsipConcentrationList());
		}
	}

	@Test
	public void testGasPhaseConcentrationCalculator() {
		assertNotNull(gasConcentrationCalculator);
		assertArrayEquals(totalConcentrations.toArray(), gasConcentrationCalculator.getConcentrationCalculator().getTotalConcentrations().toArray(), ERROR);
		assertEquals(ssipConcentrations, gasConcentrationCalculator.getSsipConcentrationList());
	}

	@Test
	public void testGetConcentrationsByMolID() {
		assertEquals(ssipConcsByID, gasConcentrationCalculator.getConcentrationsByMolID());
	}

}
