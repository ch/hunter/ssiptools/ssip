/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;

public class SolventXmlReaderTest {

	private static final Logger LOG = LogManager
			.getLogger(SolventXmlReaderTest.class);

	static final double ERROR = 1e-7;

	private URI solventURI;
	private URI solventContainerURI;
	private PhaseSolvent phaseSolvent;
	private String solventId;
	private String solventName;
	private HashMap<String, PhaseMolecule> solventMolecules;
	private PhaseMolecule phaseMolecule;
	private ArrayList<SsipConcentration> expectedSSIPConcentrationList;
	private SsipConcentrationContainer ssipConcentrationContainer;
	private SsipSurface ssipSurfaceInformation;
	private CmlMolecule cmlMolecule;
	private String stdinchikey;
	private String moleculeid;
	private HashMap<String, PhaseSolvent> phaseSolventMap;
	private SolventContainer solventContainer;

	@Before
	public void setUp() throws Exception {
		solventURI = SolventXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/phasesolvent.xml").toURI();
		solventContainerURI = SolventXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedsolventcontainer.xml").toURI();
		
		HashSet<Ssip> ssipList = new HashSet<Ssip>();
		Ssip ssip1, ssip2, ssip3, ssip4;
		
		AlphaPoint mep1 = new AlphaPoint(1.0, 1.0, 1.0, "a1", 2.8);
		ssip1 = Ssip.fromMepPoint(mep1);
		AlphaPoint mep2 = new AlphaPoint(1.0, 0.0, 0.0, "a1", 2.8);
		ssip2 = Ssip.fromMepPoint(mep2);
		BetaPoint mep3 = new BetaPoint(0.0, 1.0, 1.0, "a1", -4.5);
		ssip3 = Ssip.fromMepPoint(mep3);
		BetaPoint mep4 = new BetaPoint(0.0, 1.0, 0.0, "a1", -4.5);
		ssip4 = Ssip.fromMepPoint(mep4);
		ssipList.add(ssip1);
		ssipList.add(ssip2);
		ssipList.add(ssip3);
		ssipList.add(ssip4);
		
		stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		
		Ssip ssip1a = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 2.8));
		Ssip ssip2a = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "a", 2.8));
		Ssip ssip3a = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "a", -4.5));
		Ssip ssip4a = Ssip.fromMepPoint(new BetaPoint(1.0, 0.0, 1.0, "a", -4.5));
		
		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1a,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2a,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3a,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4a,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		expectedSSIPConcentrationList = new ArrayList<>();
		expectedSSIPConcentrationList.add(ssipConcentration2);
		expectedSSIPConcentrationList.add(ssipConcentration1);
		expectedSSIPConcentrationList.add(ssipConcentration3);
		expectedSSIPConcentrationList.add(ssipConcentration4);
		ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, expectedSSIPConcentrationList);
		
		phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		
		solventMolecules = new HashMap<>();
		solventMolecules.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		
		solventId = "water";
		solventName = "water";
		phaseSolvent = new PhaseSolvent(solventMolecules, solventId, solventName, ConcentrationUnit.MOLAR);
		
		phaseSolventMap = new HashMap<>();
		phaseSolventMap.put(solventId, phaseSolvent);
		solventContainer = new SolventContainer(phaseSolventMap);
	}

	@Test
	public void testUnmarshal(){
		try {
			PhaseSolvent readInSolvent = SolventXmlReader.unmarshal(solventURI);
			assertEquals(phaseSolvent.getSolventId(), readInSolvent.getSolventId());
			assertEquals(phaseSolvent.getSolventName(), readInSolvent.getSolventName());
			assertEquals(phaseSolvent.getPhaseMolecules().keySet(), readInSolvent.getPhaseMolecules().keySet());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("shouldn't go here");
		}
	}
	
	@Test
	public void testUnmarshalContainer(){
		try {
			SolventContainer readInSolventContainer = SolventXmlReader.unmarshalContainer(solventContainerURI);
			assertEquals(solventContainer.getSolventMap().keySet(), readInSolventContainer.getSolventMap().keySet());
			assertEquals(phaseSolvent.getSolventId(), readInSolventContainer.getSolventMap().get(solventId).getSolventId());
			assertEquals(phaseSolvent.getSolventName(), readInSolventContainer.getSolventMap().get(solventId).getSolventName());
			assertEquals(phaseSolvent.getPhaseMolecules().keySet(), readInSolventContainer.getSolventMap().get(solventId).getPhaseMolecules().keySet());
		} catch (JAXBException e) {
			LOG.error(e);
			fail("should not go here");
		}
	}
}
