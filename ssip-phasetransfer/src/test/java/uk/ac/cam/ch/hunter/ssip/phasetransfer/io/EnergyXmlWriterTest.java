/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.xmlunit.matchers.CompareMatcher;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.AssociationEnergyValue;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.AssociationEnergyValueCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.EnergyValue;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.EnergyValueCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.EnergyValueType;

public class EnergyXmlWriterTest {

	private static final Logger LOG = LogManager.getLogger(EnergyXmlWriterTest.class);

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private URI energyValueURI;
	private URI expectedEnergyValueURI;
	private URI energyValuesFileURI;
	private URI expectedEnergyValueCollectionURI;
	private URI expectedAssociationCollectionURI;
	private static final String moleculeID = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
	private static final String toSolventID = "water";
	private static final String fromSolventID = "octanol";
	private EnergyValue energyValue;
	private HashMap<String, List<EnergyValue>> energyValuesInformation;
	private ArrayList<EnergyValue> energyValueList;
	private EnergyValueCollection energyValueCollection;
	
	@Before
	public void setUp() throws Exception {
		energyValuesFileURI = tempFolder.newFile("EnergyValues.xml").toURI();
		energyValueURI = tempFolder.newFile("energyvalue.xml").toURI();
		expectedEnergyValueURI = EnergyXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedEnergyValue.xml").toURI();
		expectedEnergyValueCollectionURI = EnergyXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedenergyvaluescollection.xml").toURI();
		expectedAssociationCollectionURI = EnergyXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedAssociationEnergyCollection.xml").toURI();
		energyValuesInformation = new HashMap<>();
		energyValue = new EnergyValue(moleculeID, toSolventID, fromSolventID, 0.2, new ArrayRealVector(new double[]{0.1, 0.0, 0.1, 0.0}), EnergyValueType.MOLEFRACTION);
		energyValueList = new ArrayList<>();
		energyValueList.add(energyValue);
		energyValuesInformation = new HashMap<>();
		energyValuesInformation.put("FreeEnergy", energyValueList);
		energyValuesInformation.put("BindingEnergy", energyValueList);
		energyValuesInformation.put("ConfinementEnergy", energyValueList);
		
		energyValueCollection = new EnergyValueCollection();
		energyValueCollection.setBindingEnergies(energyValueList);
		energyValueCollection.setConfinementEnergies(energyValueList);
		energyValueCollection.setTotalEnergies(energyValueList);

	}

	@After
	public void tearDown() throws Exception {
		//use this to flush file contents.
		new FileOutputStream(new File(energyValuesFileURI)).close();
	}

	@Test
	public void testMarshal(){
		try {
			EnergyXmlWriter.marshal(energyValue, energyValueURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(energyValueURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedEnergyValueURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
	@Test
	public void testMarshalCollection(){
		try {
			EnergyXmlWriter.marshalCollection(energyValueCollection, energyValuesFileURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(energyValuesFileURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedEnergyValueCollectionURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
	@Test
	public void testMarshalAssociationCollection(){
		ArrayList<AssociationEnergyValue> expectedAssociationEnergyValues = new ArrayList<>();
		expectedAssociationEnergyValues.add(new AssociationEnergyValue("water", "water", "water", -23.141710812));
		AssociationEnergyValueCollection associationEnergyValueCollection = new AssociationEnergyValueCollection(expectedAssociationEnergyValues);
		try {
			EnergyXmlWriter.marshalAssociationCollection(associationEnergyValueCollection, energyValuesFileURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(energyValuesFileURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedAssociationCollectionURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
}
