/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;

public class TotalConcentrationCalculatorTest {

	private static final Logger LOG = LogManager.getLogger(TotalConcentrationCalculatorTest.class);
	
	private PhaseSolvent phaseSolvent;
	private HashMap<String, PhaseMolecule> solventMolecules;
	private ArrayList<SsipConcentration> ssipConcentrations;
	private double temperature;
	private TotalConcentrationCalculator totalConcentrationCalculator;
	private HashMap<String, ArrayList<SsipConcentration>> totalConcSetByMolecule;
	
	private static final double ERROR = 1.0e-7;
	
	@Before
	public void setUp() throws Exception {
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		//volumes in cubic angstroms. taken from DOI: 10.1039/c2sc21666c
		ssipSurfaceInformation.setVolumeVdW(21.4);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		Concentration moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, moleculeid);
		ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipWaterAlpha2Conc);
		ssipConcentrations.add(ssipWaterAlpha1Conc);
		ssipConcentrations.add(ssipWaterBeta1Conc);
		ssipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrations);
		
		PhaseMolecule phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		LOG.debug("volumes: {}", phaseMolecule.getVdWVolumes());
		phaseMolecule.setMoleFraction(1.0);
		solventMolecules = new HashMap<>();
		solventMolecules.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		
		String solventId = "water";
		String solventName = "water";
		phaseSolvent = new PhaseSolvent(solventMolecules, solventId, solventName, ConcentrationUnit.SSIPCONCENTRATION);
		
		
		
		//temperature in kelvin.
		temperature = 298.0;
		totalConcentrationCalculator = new TotalConcentrationCalculator(phaseSolvent, temperature);
		
		Concentration totalCalculatedConcentration = new Concentration(0.18264539823230322, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssip1 = new SsipConcentration(ssipWaterAlpha1,
				totalCalculatedConcentration, moleculeid);
		SsipConcentration ssip2 = new SsipConcentration(ssipWaterAlpha2,
				totalCalculatedConcentration, moleculeid);
		SsipConcentration ssip3 = new SsipConcentration(ssipWaterBeta1,
				totalCalculatedConcentration, moleculeid);
		SsipConcentration ssip4 = new SsipConcentration(ssipWaterBeta2,
				totalCalculatedConcentration, moleculeid);
		ArrayList<SsipConcentration> ssipTotalConcentrations = new ArrayList<>();
		ssipTotalConcentrations.add(ssip1);
		ssipTotalConcentrations.add(ssip2);
		ssipTotalConcentrations.add(ssip3);
		ssipTotalConcentrations.add(ssip4);
		totalConcSetByMolecule = new HashMap<>();
		totalConcSetByMolecule.put(moleculeid, ssipTotalConcentrations);
	}

	@Test
	public void testTotalConcentrationCalculator() {
		assertNotNull(totalConcentrationCalculator);
		assertArrayEquals(new double[]{56.609285469107874/300.0, 56.609285469107874/300.0, 56.609285469107874/300.0, 56.609285469107874/300.0}, totalConcentrationCalculator.getWeightedZeroPointConcentrations().toArray(), ERROR);
		assertArrayEquals(new double[]{0.1163929015, 0.1163929015, 0.1163929015, 0.1163929015}, totalConcentrationCalculator.getConcentrationCalculator().getTotalConcentrations().toArray(), ERROR);
	}

	@Test
	public void testCalculatePvdWFree() {
		assertEquals(0.37442215414360613, totalConcentrationCalculator.calculatePvdWFree(), ERROR);
	}

	@Test
	public void testCalculatePhiBound() {
		assertArrayEquals(new double[] {0.9317128901120382, 0.9317128901120382, 0.9317128901120382, 0.9317128901120382}, totalConcentrationCalculator.calculatePhiBound().toArray(), ERROR);
	}

	@Test
	public void testCalculateTrialConcentration() throws Exception{
		ArrayRealVector trialConcentrations =  totalConcentrationCalculator.calculateTrialConcentration();				
		assertArrayEquals(new double[]{0.18237861617607995, 0.18237861617607995, 0.18237861617607995, 0.18237861617607995}, trialConcentrations.toArray(), ERROR);
	}
	
	@Test
	public void testTotalConcentrationMatches() {
		assertFalse(totalConcentrationCalculator.totalConcentrationMatches());
		totalConcentrationCalculator.calculateTotalConcentrations();
		assertTrue(totalConcentrationCalculator.totalConcentrationMatches());
	}
	
	@Test
	public void testCalculateTotalConcentrations() {
		totalConcentrationCalculator.calculateTotalConcentrations();
		assertArrayEquals(new double[]{0.18264539823230322, 0.18264539823230322, 0.18264539823230322, 0.18264539823230322}, totalConcentrationCalculator.getConcentrationCalculator().getTotalConcentrations().toArray(), ERROR);
	}

	@Test
	public void testCalculateTotalConcsForSSIPs() {
		assertEquals(totalConcSetByMolecule.get("XLYOFNOQVPJJNP-UHFFFAOYSA-N"), totalConcentrationCalculator.calculateTotalConcsForSSIPs());
	}

	@Test
	public void testCalculateTotalConcsByMolecule(){
		assertEquals(totalConcSetByMolecule, totalConcentrationCalculator.calculateTotalConcsByMolecule());
	}
	
	@Test
	public void testGetTotalConcentration() {
		assertArrayEquals(new double[]{0.1163929015, 0.1163929015, 0.1163929015, 0.1163929015}, totalConcentrationCalculator.getConcentrationCalculator().getTotalConcentrations().toArray(), ERROR);
		assertEquals(4 * 0.1163929015, totalConcentrationCalculator.getTotalConcentration(), ERROR);
	}

	@Test
	public void testGetPsiFree() {
		assertArrayEquals(new double[] {0.05113641356898364, 0.05113641356898364, 0.05113641356898364, 0.05113641356898364}, totalConcentrationCalculator.getPsiFree().toArray(), ERROR);
	}

}
