/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class TemperatureTest {

	private Temperature temperatureKelvin;
	private Temperature temperatureCelsius;
	private static final double ERROR = 1e-7;
	
	@Before
	public void setUp() throws Exception {
		temperatureKelvin = new Temperature(298.0, TemperatureUnit.KELVIN);
		temperatureCelsius = new Temperature(24.85, TemperatureUnit.CELSIUS);
	}
	@Test
	public void testTemperature() {
		assertNotNull(temperatureCelsius);
		assertNotNull(temperatureKelvin);
	}

	@Test
	public void testToString() {
		assertEquals("298.0KELVIN", temperatureKelvin.toString());
		assertEquals("24.85CELSIUS", temperatureCelsius.toString());
	}

	@Test
	public void testEqualsObject() {
		assertNotEquals(temperatureCelsius, temperatureKelvin);
	}

	@Test
	public void testConvertTo() {
		assertEquals(temperatureKelvin, temperatureKelvin.convertTo(TemperatureUnit.KELVIN));
		assertEquals(temperatureKelvin, temperatureCelsius.convertTo(TemperatureUnit.KELVIN));
		assertEquals(temperatureCelsius, temperatureCelsius.convertTo(TemperatureUnit.CELSIUS));
		assertEquals(temperatureCelsius, temperatureKelvin.convertTo(TemperatureUnit.CELSIUS));
	}

	@Test
	public void testGetTemperatureValue() {
		assertEquals(298.0, temperatureKelvin.getTemperatureValue(), ERROR);
		assertEquals(24.85, temperatureCelsius.getTemperatureValue(), ERROR);
	}

	@Test
	public void testGetTemperatureUnit() {
		assertEquals(TemperatureUnit.CELSIUS, temperatureCelsius.getTemperatureUnit());
		assertEquals(TemperatureUnit.KELVIN, temperatureKelvin.getTemperatureUnit());
	}

}
