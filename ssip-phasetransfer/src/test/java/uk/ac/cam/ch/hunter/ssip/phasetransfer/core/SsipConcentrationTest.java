/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;

public class SsipConcentrationTest {

	private static final double ERROR = 1e-7;

	private SsipConcentration ssipConcentration1;
	private SsipConcentration ssipConcentration2;
	private SsipConcentration ssipConcentration1SSIPNormalised;

	@Before
	public void setUp() throws Exception {
		ssipConcentration1 = new SsipConcentration(new AlphaPoint(1.0, 0.0,
				0.0, "a", 1.0), 10.0, ConcentrationUnit.MOLAR, "mol1");
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		ssipConcentration2 = new SsipConcentration(ssip1, 10.0, ConcentrationUnit.MOLAR, "mol1");
		ssipConcentration1SSIPNormalised = new SsipConcentration(new AlphaPoint(1.0, 0.0,
				0.0, "a", 1.0), 10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION, "mol1");
	}

	@Test
	public void testSSIPConcentrationConstructor1() {
		Ssip testSSIP = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		assertEquals(testSSIP.getValue(), ssipConcentration1.getValue(), ERROR);
		assertEquals(testSSIP.getPosition(), ssipConcentration1.getPosition());
		assertEquals("mol1", ssipConcentration1.getMoleculeID());
		assertEquals(10.0, ssipConcentration1.getTotalConcentration(), ERROR);
		assertEquals(ConcentrationUnit.MOLAR, ssipConcentration1.getConcentrationUnit());
	}

	@Test
	public void testSSIPConcentrationConstructor2() {
		Ssip testSSIP = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		assertEquals(testSSIP.getValue(), ssipConcentration2.getValue(), ERROR);
		assertEquals(testSSIP.getPosition(), ssipConcentration2.getPosition());
		assertEquals("mol1", ssipConcentration2.getMoleculeID());
		assertEquals(10.0, ssipConcentration2.getTotalConcentration(), ERROR);
		assertEquals(ConcentrationUnit.MOLAR, ssipConcentration2.getConcentrationUnit());
	}

	@Test
	public void testSSIPConcentrationEquals() {
		assertEquals(ssipConcentration1, ssipConcentration2);
	}

	@Test
	public void testGetSolvationConstant() {
		ssipConcentration1.setFreeConcentration(2.0);
		ssipConcentration1.setBoundConcentration(8.0);
		assertEquals(4.0, ssipConcentration1.calculateSolvationConstant(),
				ERROR);
	}

	@Test
	public void testCalculateFractionFree() {
		ssipConcentration1.setFreeConcentration(2.0);
		ssipConcentration1.setBoundConcentration(8.0);
		assertEquals(0.2, ssipConcentration1.calculateFractionFree(), ERROR);
	}
	
	@Test
	public void testCalculateLogFractionFree(){
		ssipConcentration1.setFreeConcentration(2.0);
		ssipConcentration1.setBoundConcentration(8.0);
		assertEquals(-1.609437912, ssipConcentration1.calculateLogFractionFree(), ERROR);
	}
	
	@Test
	public void testConvertConcentrationsTo(){
		SsipConcentration actualSSIPConcentrationMOLAR = ssipConcentration1SSIPNormalised.convertConcentrationsTo(ConcentrationUnit.MOLAR);
		assertEquals(ssipConcentration1, actualSSIPConcentrationMOLAR);
		assertEquals(ssipConcentration1.getFreeConcentration(), actualSSIPConcentrationMOLAR.getFreeConcentration(), ERROR);
		assertEquals(ssipConcentration1.getBoundConcentration(), actualSSIPConcentrationMOLAR.getBoundConcentration(), ERROR);
		assertEquals(ssipConcentration1.getBoundConcentrationList(), actualSSIPConcentrationMOLAR.getBoundConcentrationList());
		
		ssipConcentration1.setFreeConcentration(2.0);
		ssipConcentration1.setBoundConcentration(8.0);
		ssipConcentration1.setBoundConcentrationList(new ArrayRealVector(new double[]{1.0, 2.0, 2.0, 3.0}));
		
		SsipConcentration actualSSIPConcentrationSSIPNormalised = ssipConcentration1.convertConcentrationsTo(ConcentrationUnit.SSIPCONCENTRATION);
		assertEquals(ssipConcentration1SSIPNormalised, actualSSIPConcentrationSSIPNormalised);
		assertEquals(2.0/300, actualSSIPConcentrationSSIPNormalised.getFreeConcentration(), ERROR);
		assertEquals(8.0/300, actualSSIPConcentrationSSIPNormalised.getBoundConcentration(), ERROR);
		assertArrayEquals(new double[]{1.0/300.0, 2.0/300.0, 2.0/300.0, 3.0/300.0}, actualSSIPConcentrationSSIPNormalised.getBoundConcentrationList().toArray(), ERROR);
	}
	
	@Test
	public void testBoundConcentrationListSetting(){
		ssipConcentration1.setFreeConcentration(2.0);
		ssipConcentration1.setBoundConcentration(8.0);
		ssipConcentration1.setBoundConcentrationList(new ArrayRealVector(new double[]{1.0, 2.0, 2.0, 3.0}));
		
		ArrayList<BoundConcentration> boundConcentrations = new ArrayList<>();
		boundConcentrations.add(new BoundConcentration(1.0, ConcentrationUnit.MOLAR, 1));
		boundConcentrations.add(new BoundConcentration(2.0, ConcentrationUnit.MOLAR, 2));
		boundConcentrations.add(new BoundConcentration(2.0, ConcentrationUnit.MOLAR, 3));
		boundConcentrations.add(new BoundConcentration(3.0, ConcentrationUnit.MOLAR, 4));
		assertEquals(boundConcentrations, ssipConcentration1.getBoundConcentrations());
	}
	
	@Test
	public void testBoundConcentrationsSetting(){
		ArrayList<BoundConcentration> boundConcentrations = new ArrayList<>();
		boundConcentrations.add(new BoundConcentration(1.0, ConcentrationUnit.MOLAR, 1));
		boundConcentrations.add(new BoundConcentration(2.0, ConcentrationUnit.MOLAR, 2));
		boundConcentrations.add(new BoundConcentration(2.0, ConcentrationUnit.MOLAR, 3));
		boundConcentrations.add(new BoundConcentration(3.0, ConcentrationUnit.MOLAR, 4));
		ssipConcentration1.setBoundConcentrations(boundConcentrations);
		assertArrayEquals(new double[]{1.0, 2.0, 2.0, 3.0}, ssipConcentration1.getBoundConcentrationList().toArray(), ERROR);
	}
	
	@Test
	public void testGetBoundConcentrationsToOtherSSIPs() {
		ArrayList<BoundConcentration> boundConcentrations = new ArrayList<>();
		boundConcentrations.add(new BoundConcentration(1.0, ConcentrationUnit.MOLAR, 1));
		boundConcentrations.add(new BoundConcentration(2.0, ConcentrationUnit.MOLAR, 2));
		boundConcentrations.add(new BoundConcentration(2.0, ConcentrationUnit.MOLAR, 3));
		boundConcentrations.add(new BoundConcentration(3.0, ConcentrationUnit.MOLAR, 4));
		ssipConcentration1.setBoundConcentrations(boundConcentrations);
		ArrayList<Integer> ssipIndices = new ArrayList<>();
		ssipIndices.add(0);
		ssipIndices.add(1);
		assertArrayEquals(new double[] {1.0, 2.0}, ssipConcentration1.getBoundConcentrationsToOtherSSIPs(ssipIndices).toArray(), ERROR);
	}
	
	@Test
	public void testGetBoundConcentrationToOtherSSIPs() {
		ArrayList<BoundConcentration> boundConcentrations = new ArrayList<>();
		boundConcentrations.add(new BoundConcentration(1.0, ConcentrationUnit.MOLAR, 1));
		boundConcentrations.add(new BoundConcentration(2.0, ConcentrationUnit.MOLAR, 2));
		boundConcentrations.add(new BoundConcentration(2.0, ConcentrationUnit.MOLAR, 3));
		boundConcentrations.add(new BoundConcentration(3.0, ConcentrationUnit.MOLAR, 4));
		ssipConcentration1.setBoundConcentrations(boundConcentrations);
		ArrayList<Integer> ssipIndices = new ArrayList<>();
		ssipIndices.add(0);
		ssipIndices.add(1);
		assertEquals(3.0, ssipConcentration1.getBoundConcentrationToOtherSSIPs(ssipIndices).getConcentrationValue(), ERROR);
	}
	
}
