/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseAssemblerTest;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.PhaseSystemConcentrationCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.PhaseSystemConcentrationCalculatorWithSolute;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.io.SoluteXmlReader;

public class PhaseSystemMixtureTest {

	private static final Logger LOG = LogManager.getLogger(PhaseSystemMixtureTest.class);
	
	private PhaseSolute soluteInformation;
	private PhaseSystemConcentrationCalculator waterSystemCalculator; 
	private Phase liquidPhase;
	private PhaseSystem waterPhaseSystem;
	private PhaseSystem phaseSystemWithSolute;
	private HashMap<String, PhaseSystem> phaseSystemMap;
	private PhaseSystemMixture phaseSystemMixture;
	private HashMap<String, PhaseMolecule> phaseMolecules;
	private ArrayList<SsipConcentration> ssipConcentrations;
	private ArrayList<EnergyValue> expectedEnergyValues;
	private String solventId;
	private String solventIdWithSolute;
	private double temperature;
	
	private static final double ERROR = 1e-8;
	
	@Before
	public void setUp() throws Exception {
		URI soluteFileURI = PhaseAssemblerTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/watersolute.xml").toURI();

		soluteInformation =  SoluteXmlReader.unmarshalContainer(soluteFileURI).getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute");
		
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "water";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		//volumes in cubic angstroms. taken from DOI: 10.1039/c2sc21666c
		ssipSurfaceInformation.setVolumeVdW(21.4);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		Concentration moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, moleculeid);
		ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipWaterAlpha2Conc);
		ssipConcentrations.add(ssipWaterAlpha1Conc);
		ssipConcentrations.add(ssipWaterBeta1Conc);
		ssipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrations);
		
		PhaseMolecule phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		LOG.debug("volumes: {}", phaseMolecule.getVdWVolumes());
		phaseMolecule.setMoleFraction(1.0);
		phaseMolecules = new HashMap<>();
		phaseMolecules.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		
		solventId = "water";
		//temperature in kelvin.
		temperature = 298.0;
		
		liquidPhase = new Phase(phaseMolecules, new Temperature(temperature, TemperatureUnit.KELVIN), ConcentrationUnit.SSIPCONCENTRATION);
		liquidPhase.setSolventID(solventId);
		liquidPhase.setPhaseType(PhaseType.CONDENSED);
		
		waterSystemCalculator = new PhaseSystemConcentrationCalculator(liquidPhase);
		waterSystemCalculator.calculateSpeciation();
		
		waterPhaseSystem = waterSystemCalculator.getPhaseSystem(solventId).convertConcentrationUnitTo(ConcentrationUnit.MOLAR);
		phaseSystemMap = new HashMap<>();
		phaseSystemMap.put(solventId, waterPhaseSystem);
		solventIdWithSolute = "waterSolute";
		phaseSystemWithSolute = PhaseSystemConcentrationCalculatorWithSolute.generatePhaseSystemWithSolute(liquidPhase.convertConcentrationUnitTo(ConcentrationUnit.MOLAR), soluteInformation, solventIdWithSolute);
		phaseSystemMap.put(solventIdWithSolute, phaseSystemWithSolute);
		
		phaseSystemMixture = new PhaseSystemMixture(phaseSystemMap);
		
		expectedEnergyValues = new ArrayList<>();
	}

	@Test
	public void testPhaseSystemMixtureMapOfStringPhaseSystem() {
		assertNotNull(phaseSystemMixture);
		assertEquals(phaseSystemMap, phaseSystemMixture.getPhaseSystemMap());
	}

	@Test
	public void testCalculateConfinementEnergyDifference() {
		assertEquals(5.587025326381223E-5, phaseSystemMixture.calculateConfinementEnergyDifference("water", solventId, solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateConfinementEnergyDifferencePerSSIP() {
		assertArrayEquals(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}, phaseSystemMixture.calculateConfinementEnergyDifferencePerSSIP("water", solventId, solventIdWithSolute).toArray(), ERROR);

	}

	@Test
	public void testCalculateConfinementEnergyDifferenceValue() {
		assertEquals(new EnergyValue("water", solventIdWithSolute, solventId, 5.587025326381223E-5, new ArrayRealVector(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}), EnergyValueType.MOLEFRACTION), phaseSystemMixture.calculateConfinementEnergyDifferenceValue("water", solventId, solventIdWithSolute));
	}

	@Test
	public void testCalculateConfinementEnergyDifferenceToPhase() {
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, 5.587025326381223E-5, new ArrayRealVector(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateConfinementEnergyDifferenceToPhase("water", solventIdWithSolute));
	}

	@Test
	public void testCalculateConfinementEnergyDifferenceBetweenAllPhases() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, -5.587025326381223E-5, new ArrayRealVector(new double[]{-1.3967563315953058E-5, -1.3967563315953058E-5, -1.3967563315953058E-5, -1.3967563315953058E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, 5.587025326381223E-5, new ArrayRealVector(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateConfinementEnergyDifferenceBetweenAllPhases("water"));
	}

	@Test
	public void testCalculateConfinementEnergyDifferenceFor1M() {
		assertEquals(1.0650864356698264E-5, phaseSystemMixture.calculateConfinementEnergyDifferenceFor1M("water", solventId, solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateConfinementEnergyDifferenceFor1MPerSSIP() {
		assertArrayEquals(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}, phaseSystemMixture.calculateConfinementEnergyDifferenceFor1MPerSSIP("water", solventId, solventIdWithSolute).toArray(), ERROR);
	}

	@Test
	public void testCalculateConfinementEnergyDifferenceValue1M() {
		LOG.debug("value: {}", phaseSystemMixture.calculateConfinementEnergyDifferenceValue1M("water", solventId, solventIdWithSolute).getValue());
		LOG.debug("value by SSIP: {}", phaseSystemMixture.calculateConfinementEnergyDifferenceValue1M("water", solventId, solventIdWithSolute).getContributionBySSIP());
		assertEquals(new EnergyValue("water", solventIdWithSolute, solventId, 1.0650864356698264E-5, new ArrayRealVector(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}), EnergyValueType.MOLAR), phaseSystemMixture.calculateConfinementEnergyDifferenceValue1M("water", solventId, solventIdWithSolute));
	}

	@Test
	public void testCalculateConfinementEnergyDifferenceToPhase1M() {
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, 1.0650864356698264E-5, new ArrayRealVector(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateConfinementEnergyDifferenceToPhase1M("water", solventIdWithSolute));
	}

	@Test
	public void testCalculateConfinementEnergyDifferenceBetweenAllPhases1M() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, -1.0650864356698264E-5, new ArrayRealVector(new double[]{-1.3967563315953058E-5, -1.3967563315953058E-5, -1.3967563315953058E-5, -1.3967563315953058E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, 1.0650864356698264E-5, new ArrayRealVector(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateConfinementEnergyDifferenceBetweenAllPhases1M("water"));
	}

	@Test
	public void testCalculateConfinementEnergySolvation() {
		assertEquals(5.869975760214229, phaseSystemMixture.calculateConfinementEnergySolvation("water", solventId), ERROR);
		assertEquals(5.870031630469972, phaseSystemMixture.calculateConfinementEnergySolvation("water", solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateConfinementEnergySolvationPerSSIP() {
		assertArrayEquals(new double[]{1.4674939400535572, 1.4674939400535572, 1.4674939400535572, 1.4674939400535572}, phaseSystemMixture.calculateConfinementEnergySolvationPerSSIP("water", solventId).toArray(), ERROR);
		assertArrayEquals(new double[]{1.467507907617493, 1.467507907617493, 1.467507907617493, 1.467507907617493}, phaseSystemMixture.calculateConfinementEnergySolvationPerSSIP("water", solventIdWithSolute).toArray(), ERROR);
	}

	@Test
	public void testCalculateConfinementEnergySolvationEnergyValue() {
		assertEquals(new EnergyValue("water", solventId, "", 5.869975760214229, new ArrayRealVector(new double[]{1.4674939400535572, 1.4674939400535572, 1.4674939400535572, 1.4674939400535572}), EnergyValueType.MOLEFRACTION), phaseSystemMixture.calculateConfinementEnergySolvationEnergyValue("water", solventId));
	}

	@Test
	public void testCalculateConfinementEnergySolvationAllPhases() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, "", 5.869975760214229, new ArrayRealVector(new double[] {1.4674939400535572, 1.4674939400535572, 1.4674939400535572, 1.4674939400535572}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, "", 5.870031630469972, new ArrayRealVector(new double[]{1.467507907617493, 1.467507907617493, 1.467507907617493, 1.467507907617493}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateConfinementEnergySolvationAllPhases("water"));
	}

	@Test
	public void testCalculateConfinementEnergyValuesAllPhasesAllValues() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, "", 5.869975760214229, new ArrayRealVector(new double[] {1.4674939400535572, 1.4674939400535572, 1.4674939400535572, 1.4674939400535572}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, "", 5.870031630469972, new ArrayRealVector(new double[]{1.467507907617493, 1.467507907617493, 1.467507907617493, 1.467507907617493}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, -5.587025326381223E-5, new ArrayRealVector(new double[]{-1.3967563315953058E-5, -1.3967563315953058E-5, -1.3967563315953058E-5, -1.3967563315953058E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, 5.587025326381223E-5, new ArrayRealVector(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, -1.0650864356698264E-5, new ArrayRealVector(new double[]{-1.3967563315953058E-5, -1.3967563315953058E-5, -1.3967563315953058E-5, -1.3967563315953058E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, 1.0650864356698264E-5, new ArrayRealVector(new double[]{1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5, 1.3967563315953058E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertArrayEquals(expectedEnergyValues.toArray(), phaseSystemMixture.calculateConfinementEnergyValuesAllPhasesAllValues("water").toArray());
	}

	@Test
	public void testCalculateBindingEnergyDifference() {
		assertEquals(-8.854649026801553E-5, phaseSystemMixture.calculateBindingEnergyDifference("water", solventId, solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateBindingEnergyDifferencePerSSIP() {
		assertArrayEquals(new double[]{-2.2136043020815066E-5, -2.2136043020815066E-5, -2.2136043019926888E-5, -2.2136043019926888E-5}, phaseSystemMixture.calculateBindingEnergyDifferencePerSSIP("water", solventId, solventIdWithSolute).toArray(), ERROR);
	}

	@Test
	public void testCalculateBindingEnergyDifferenceValue() {
		assertEquals(new EnergyValue("water", solventIdWithSolute, solventId, -8.854649026801553E-5, new ArrayRealVector(new double[]{-2.2136043020815066E-5, -2.2136043020815066E-5, -2.2136043019926888E-5, -2.2136043019926888E-5}), EnergyValueType.MOLEFRACTION), phaseSystemMixture.calculateBindingEnergyDifferenceValue("water", solventId, solventIdWithSolute));
	}

	@Test
	public void testCalculateBindingEnergyDifferenceToPhase() {
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -8.854649026801553E-5, new ArrayRealVector(new double[]{-2.2136043020815066E-5, -2.2136043020815066E-5, -2.2136043019926888E-5, -2.2136043019926888E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateBindingEnergyDifferenceToPhase("water", solventIdWithSolute));
	}

	@Test
	public void testCalculateBindingEnergyDifferenceBetweenAllPhases() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, 8.854649026801553E-5, new ArrayRealVector(new double[]{2.2136043020815066E-5, 2.2136043020815066E-5, 2.2136043019926888E-5, 2.2136043019926888E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -8.854649026801553E-5, new ArrayRealVector(new double[]{-2.2136043020815066E-5, -2.2136043020815066E-5, -2.2136043019926888E-5, -2.2136043019926888E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateBindingEnergyDifferenceBetweenAllPhases("water"));
	}

	@Test
	public void testCalculateBindingEnergyDifferenceFor1M() {
		assertEquals(-1.3377004502856948E-4, phaseSystemMixture.calculateBindingEnergyDifferenceFor1M("water", solventId, solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateBindingEnergyDifferenceFor1MPerSSIP() {
		assertArrayEquals(new double[]{-2.2136043020815066E-5, -2.2136043020815066E-5, -2.2136043019926888E-5, -2.2136043019926888E-5}, phaseSystemMixture.calculateBindingEnergyDifferenceFor1MPerSSIP("water", solventId, solventIdWithSolute).toArray(), ERROR);
	}

	@Test
	public void testCalculateBindingEnergyDifferenceValue1M() {
		assertEquals(new EnergyValue("water", solventIdWithSolute, solventId, -1.3377004502856948E-4, new ArrayRealVector(new double[]{-2.2136043020815066E-5, -2.2136043020815066E-5, -2.2136043019926888E-5, -2.2136043019926888E-5}), EnergyValueType.MOLAR), phaseSystemMixture.calculateBindingEnergyDifferenceValue1M("water", solventId, solventIdWithSolute));
	}

	@Test
	public void testCalculateBindingEnergyDifferenceToPhase1M() {
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -1.3377004502856948E-4, new ArrayRealVector(new double[]{-2.2136043020815066E-5, -2.2136043020815066E-5, -2.2136043019926888E-5, -2.2136043019926888E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateBindingEnergyDifferenceToPhase1M("water", solventIdWithSolute));
	}

	@Test
	public void testCalculateBindingEnergyDifferenceBetweenAllPhases1M() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, 1.3377004502856948E-4, new ArrayRealVector(new double[]{2.2136043020815066E-5, 2.2136043020815066E-5, 2.2136043019926888E-5, 2.2136043019926888E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -1.3377004502856948E-4, new ArrayRealVector(new double[]{-2.2136043020815066E-5, -2.2136043020815066E-5, -2.2136043019926888E-5, -2.2136043019926888E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateBindingEnergyDifferenceBetweenAllPhases1M("water"));
	}

	@Test
	public void testCalculateBindingEnergySolvation() {
		assertEquals(-31.647710967861023, phaseSystemMixture.calculateBindingEnergySolvation("water", solventId), ERROR);
		assertEquals(-31.64779951203859, phaseSystemMixture.calculateBindingEnergySolvation("water", solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateBindingEnergySolvationPerSSIP() {
		assertArrayEquals(new double[]{-7.9119277450748156, -7.9119277450748156, -7.9119277450748156, -7.9119277450748156}, phaseSystemMixture.calculateBindingEnergySolvationPerSSIP("water", solventId).toArray(), ERROR);
		
		assertArrayEquals(new double[]{-7.911949881119157, -7.911949881119157, -7.911949881119157, -7.911949881119157}, phaseSystemMixture.calculateBindingEnergySolvationPerSSIP("water", solventIdWithSolute).toArray(), ERROR);
	}

	@Test
	public void testCalculateBindingEnergySolvationEnergyValue() {
		assertEquals(new EnergyValue("water", solventId, "", -31.647710980299262, new ArrayRealVector(new double[]{-7.9119277450748156, -7.9119277450748156, -7.9119277450748156, -7.9119277450748156}), EnergyValueType.MOLEFRACTION), phaseSystemMixture.calculateBindingEnergySolvationEnergyValue("water", solventId));
	}

	@Test
	public void testCalculateBindingEnergySolvationAllPhases() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, "", -31.647710980299262, new ArrayRealVector(new double[]{-7.9119277450748156, -7.9119277450748156, -7.9119277450748156, -7.9119277450748156}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, "", -31.64779952447663, new ArrayRealVector(new double[]{-7.911949881119157, -7.911949881119157, -7.911949881119157, -7.911949881119157}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateBindingEnergySolvationAllPhases("water"));
	}

	@Test
	public void testCalculateBindingEnergyValuesAllPhasesAllValues() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, "", -31.647710980299262, new ArrayRealVector(new double[]{-7.9119277450748156, -7.9119277450748156, -7.9119277450748156, -7.9119277450748156}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, "", -31.64779952447663, new ArrayRealVector(new double[]{-7.911949881119157, -7.911949881119157, -7.911949881119157, -7.911949881119157}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, 8.854649026801553E-5, new ArrayRealVector(new double[]{2.2205578933842673E-5, 2.2205578933842673E-5, 2.2067666198388736E-5, 2.2067666198388736E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -8.854649026801553E-5, new ArrayRealVector(new double[]{-2.2205578933842673E-5, -2.2205578933842673E-5, -2.2067666198388736E-5, -2.2067666198388736E-5}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, 1.3377004502856948E-4, new ArrayRealVector(new double[]{2.2205578933842673E-5, 2.2205578933842673E-5, 2.2067666198388736E-5, 2.2067666198388736E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -1.3377004502856948E-4, new ArrayRealVector(new double[]{-2.2205578933842673E-5, -2.2205578933842673E-5, -2.2067666198388736E-5, -2.2067666198388736E-5}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertArrayEquals(expectedEnergyValues.toArray(), phaseSystemMixture.calculateBindingEnergyValuesAllPhasesAllValues("water").toArray());
	}

	@Test
	public void testCalculateFreeEnergyDifference() {
		assertEquals(-3.26739188167835E-5, phaseSystemMixture.calculateFreeEnergyDifference("water", solventId, solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateFreeEnergyDifferencePerSSIP() {
		assertArrayEquals(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}, phaseSystemMixture.calculateFreeEnergyDifferencePerSSIP("water", solventId, solventIdWithSolute).toArray(), ERROR);
	}

	@Test
	public void testCalculateFreeEnergyDifferenceValue() {
		assertEquals(new EnergyValue("water", solventIdWithSolute, solventId, -3.26739188167835E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLEFRACTION), phaseSystemMixture.calculateFreeEnergyDifferenceValue("water", solventId, solventIdWithSolute));
	}

	@Test
	public void testCalculateFreeEnergyDifferenceToPhase() {
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -3.26739188167835E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateFreeEnergyDifferenceToPhase("water", solventIdWithSolute));
	}

	@Test
	public void testCalculateFreeEnergyDifferenceBetweenAllPhases() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, 3.26739188167835E-5, new ArrayRealVector(new double[] {8.168479705084053E-6, 8.168479705084053E-6, 8.168479704195875E-6, 8.168479704195875E-6}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -3.26739188167835E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateFreeEnergyDifferenceBetweenAllPhases("water"));
	}

	@Test
	public void testCalculateFreeEnergyDifferenceFor1M() {
		assertEquals(-7.789330772389746E-5, phaseSystemMixture.calculateFreeEnergyDifferenceFor1M("water", solventId, solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateFreeEnergyDifferenceFor1MPerSSIP() {
		assertArrayEquals(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}, phaseSystemMixture.calculateFreeEnergyDifferenceFor1MPerSSIP("water", solventId, solventIdWithSolute).toArray(), ERROR);
	}

	@Test
	public void testCalculateFreeEnergyDifferenceValue1M() {
		assertEquals(new EnergyValue("water", solventIdWithSolute, solventId, -7.789330772389746E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLAR), phaseSystemMixture.calculateFreeEnergyDifferenceValue1M("water", solventId, solventIdWithSolute));
	}

	@Test
	public void testCalculateFreeEnergyDifferenceToPhase1M() {
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -7.789330772389746E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateFreeEnergyDifferenceToPhase1M("water", solventIdWithSolute));
	}

	@Test
	public void testCalculateFreeEnergyDifferenceBetweenAllPhases1M() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, 7.789330772389746E-5, new ArrayRealVector(new double[]{8.168479705084053E-6, 8.168479705084053E-6, 8.168479704195875E-6, 8.168479704195875E-6}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -7.789330772389746E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateFreeEnergyDifferenceBetweenAllPhases1M("water"));
	}

	@Test
	public void testCalculateFreeEnergySolvation() {
		assertEquals(-25.777735220085034, phaseSystemMixture.calculateFreeEnergySolvation("water", solventId), ERROR);
		assertEquals(-25.777767894006658, phaseSystemMixture.calculateFreeEnergySolvation("water", solventIdWithSolute), ERROR);
	}

	@Test
	public void testCalculateFreeEnergySolvationPerSSIP() {
		assertArrayEquals(new double[]{-6.444433805021259, -6.444433805021259, -6.444433805021259, -6.444433805021259}, phaseSystemMixture.calculateFreeEnergySolvationPerSSIP("water", solventId).toArray(), ERROR);
		assertArrayEquals(new double[]{-6.4444419735016645, -6.4444419735016645, -6.4444419735016645, -6.4444419735016645}, phaseSystemMixture.calculateFreeEnergySolvationPerSSIP("water", solventIdWithSolute).toArray(), ERROR);
	}

	@Test
	public void testCalculateFreeEnergySolvationEnergyValue() {
		assertEquals(new EnergyValue("water", solventId, "", -25.777735220085034, new ArrayRealVector(new double[]{-6.444433805021259, -6.444433805021259, -6.444433805021259, -6.444433805021259}), EnergyValueType.MOLEFRACTION), phaseSystemMixture.calculateFreeEnergySolvationEnergyValue("water", solventId));
		assertEquals(new EnergyValue("water", solventIdWithSolute, "", -25.777767894006658, new ArrayRealVector(new double[]{-6.4444419735016645, -6.4444419735016645, -6.4444419735016645, -6.4444419735016645}), EnergyValueType.MOLEFRACTION), phaseSystemMixture.calculateFreeEnergySolvationEnergyValue("water", solventIdWithSolute));
	}

	@Test
	public void testCalculateFreeEnergySolvationAllPhases() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, "", -25.777735220085034, new ArrayRealVector(new double[]{-6.444433805021259, -6.444433805021259, -6.444433805021259, -6.444433805021259}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, "", -25.777767894006658, new ArrayRealVector(new double[]{-6.4444419735016645, -6.4444419735016645, -6.4444419735016645, -6.4444419735016645}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedEnergyValues, phaseSystemMixture.calculateFreeEnergySolvationAllPhases("water"));
	}

	@Test
	public void testCalculateFreeEnergyValuesAllPhasesAllValues() {
		expectedEnergyValues.add(new EnergyValue("water", solventId, "", -25.777735220085034, new ArrayRealVector(new double[]{-6.444433805021259, -6.444433805021259, -6.444433805021259, -6.444433805021259}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, "", -25.777767894006658, new ArrayRealVector(new double[]{-6.4444419735016645, -6.4444419735016645, -6.4444419735016645, -6.4444419735016645}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, 3.26739188167835E-5, new ArrayRealVector(new double[] {8.168479705084053E-6, 8.168479705084053E-6, 8.168479704195875E-6, 8.168479704195875E-6}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -3.26739188167835E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLEFRACTION));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventId, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventId, solventIdWithSolute, 7.789330772389746E-5, new ArrayRealVector(new double[]{8.168479705084053E-6, 8.168479705084053E-6, 8.168479704195875E-6, 8.168479704195875E-6}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventId, -7.789330772389746E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLAR));
		expectedEnergyValues.add(new EnergyValue("water", solventIdWithSolute, solventIdWithSolute, 0.0, new ArrayRealVector(new double[]{0.0,0.0,0.0,0.0}), EnergyValueType.MOLAR));
		assertArrayEquals(expectedEnergyValues.toArray(), phaseSystemMixture.calculateFreeEnergyValuesAllPhasesAllValues("water").toArray());
	}

	@Test
	public void testCalculateAssociationEnergyValuesAllPhases() {
		ArrayList<AssociationEnergyValue> expectedAssociationEnergyValues = new ArrayList<>();
		expectedAssociationEnergyValues.add(new AssociationEnergyValue("water", "water", "water", -23.201542007));
		expectedAssociationEnergyValues.add(new AssociationEnergyValue("water", "water", "water", -23.193090556));
		expectedAssociationEnergyValues.add(new AssociationEnergyValue("water", "XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", solventId, 8.450310115));
		expectedAssociationEnergyValues.add(new AssociationEnergyValue("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", "water", solventId, 8.450310115));
		expectedAssociationEnergyValues.add(new AssociationEnergyValue("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", "XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute", solventId, 40.093710786));
		ArrayList<AssociationEnergyValue> actualAssociationEnergyValues = (ArrayList<AssociationEnergyValue>) phaseSystemMixture.calculateAssociationEnergyValuesAllPhases();
		assertArrayEquals(expectedAssociationEnergyValues.toArray(), actualAssociationEnergyValues.toArray());
	}
	
	@Test
	public void testCalculateLogPAllPhases() {
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR));
		expectedList.add(new EnergyValue("water", "water", "waterSolute", -1.365319068670111E-5, new ArrayRealVector(new double[]{8.168479705084053E-6, 8.168479705084053E-6, 8.168479704195875E-6, 8.168479704195875E-6}), EnergyValueType.MOLAR));
		expectedList.add(new EnergyValue("water", "waterSolute", "water", 1.365319068670111E-5, new ArrayRealVector(new double[]{-8.168479705084053E-6, -8.168479705084053E-6, -8.168479704195875E-6, -8.168479704195875E-6}), EnergyValueType.MOLAR));
		expectedList.add(new EnergyValue("water", "waterSolute", "waterSolute", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR));
		ArrayList<EnergyValue> actualList = (ArrayList<EnergyValue>) phaseSystemMixture.calculateLogPAllPhases("water");
		assertArrayEquals(expectedList.toArray(), actualList.toArray());
	}
	
	@Test
	public void testCalculateLogPValue() {
		EnergyValue expectedValue = new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR);
		assertEquals(expectedValue, phaseSystemMixture.calculateLogPValue("water", "water", "water"));
	}
	
	@Test
	public void testCalculateLogP() {
		assertEquals(0.0, phaseSystemMixture.calculateLogP("water", "water", "water"), ERROR);
	}
	@Test
	public void testCalculateConversionFactorTo1MScale() {
		assertEquals(4.522355476055395E-5, phaseSystemMixture.calculateConversionFactorTo1MScale(solventId, solventIdWithSolute), ERROR);
	}

}
