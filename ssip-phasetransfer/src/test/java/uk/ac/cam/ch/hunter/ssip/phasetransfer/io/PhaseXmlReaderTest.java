/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;


import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.w3c.dom.Attr;
import org.xmlunit.matchers.CompareMatcher;
import org.xmlunit.util.Nodes;
import org.xmlunit.util.Predicate;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;

public class PhaseXmlReaderTest {

	private static final Logger LOG = LogManager.getLogger(PhaseXmlReaderTest.class);

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private Phase waterPhase;
	private URI phaseFileURI;
	private URI phaseSystemURI;
	private URI actualPhaseSystemFileURI;
	private Temperature phaseTemperature;
	private HashSet<PhaseMolecule> waterPhaseMolecules;
	static final double ERROR = 1e-7;
	
	// Ignore the "dynamic" value of ssipSoftwareVersion in the testing
    final Predicate<Attr> attributeFilter = new Predicate<Attr>(){
        @Override
        public boolean test(Attr attrName) {
        	return (!Nodes.getQName(attrName).toString().contains("ssipSoftwareVersion"));
        }
    };
	
	@Before
	public void setUp() throws Exception {
		phaseFileURI = PhaseXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/WaterPhase.xml").toURI();
		phaseSystemURI = PhaseXmlReaderTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystem.xml").toURI();
		actualPhaseSystemFileURI = tempFolder.newFile("phaseSystem.xml").toURI();
		
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		
		Concentration moleculeConcentration = new Concentration(55.35, ConcentrationUnit.MOLAR);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		String moleculeWater = "water";
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, "water");
		ArrayList<SsipConcentration> waterSsipConcentrations = new ArrayList<>();
		waterSsipConcentrations.add(ssipWaterAlpha1Conc);
		waterSsipConcentrations.add(ssipWaterAlpha2Conc);
		waterSsipConcentrations.add(ssipWaterBeta1Conc);
		waterSsipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer waterContainer = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, waterSsipConcentrations);
		SsipSurface waterSurfaceInformation = new SsipSurface(37.34, 18.46, 18.879, 0.002, 2200); 
		
		PhaseMolecule waterMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(waterSurfaceInformation), waterContainer, moleculeWater, stdinchikey);
		waterPhaseMolecules = new HashSet<>();
		waterPhaseMolecules.add(waterMolecule);
		 
		phaseTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		PhaseFactory phaseFactory = new PhaseFactory();
		waterPhase = phaseFactory.generatePhase(waterPhaseMolecules, phaseTemperature, ConcentrationUnit.MOLAR);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testUnMarshal(){
		try {
			Phase readInPhase = PhaseXmlReader.unmarshal(phaseFileURI);
			assertEquals(waterPhase.getConcentrationUnit(), readInPhase.getConcentrationUnit());
			assertEquals(waterPhase.getTemperature(), readInPhase.getTemperature());
			assertEquals(waterPhase.getsortedSSIPConcentrations(), readInPhase.getsortedSSIPConcentrations());
			assertArrayEquals(waterPhase.getBoundConcentrations().toArray(), readInPhase.getBoundConcentrations().toArray(), ERROR);
			assertArrayEquals(waterPhase.getFreeConcentrations().toArray(), waterPhase.getFreeConcentrations().toArray(), ERROR);
			assertArrayEquals(waterPhase.getTotalConcentrations().toArray(), readInPhase.getTotalConcentrations().toArray(), ERROR);
			for (int i = 0; i < waterPhase.getBoundConcentrationsMatrix().getRowDimension(); i++) {
				assertArrayEquals(waterPhase.getBoundConcentrationsMatrix().getRow(i), readInPhase.getBoundConcentrationsMatrix().getRow(i), ERROR);
			}
			LOG.debug("readInPhase mol IDs: {}", readInPhase.getPhaseMolecules().keySet());
			LOG.debug("waterPhase mol IDs: {}", waterPhase.getPhaseMolecules().keySet());
			assertEquals(waterPhase.getPhaseMolecules().keySet(), readInPhase.getPhaseMolecules().keySet());
			assertEquals(waterPhase.getPhaseMolecules().get("water").getCmlMolecule(), readInPhase.getPhaseMolecules().get("water").getCmlMolecule());
			assertEquals(waterPhase.getPhaseMolecules().get("water").getMoleculeConcentrationUnit(), readInPhase.getPhaseMolecules().get("water").getMoleculeConcentrationUnit());
			assertEquals(waterPhase.getPhaseMolecules().get("water").getMoleculeID(), readInPhase.getPhaseMolecules().get("water").getMoleculeID());
			assertEquals(waterPhase.getPhaseMolecules().get("water").getSsipConcentrationContainer(), readInPhase.getPhaseMolecules().get("water").getSsipConcentrationContainer());
			assertEquals(waterPhase.getPhaseMolecules().get("water").getSsipSurfaceInformation(), readInPhase.getPhaseMolecules().get("water").getSsipSurfaceInformation());
			assertEquals(waterPhase.getPhaseMolecules().get("water").getStdinchikey(), readInPhase.getPhaseMolecules().get("water").getStdinchikey());
		} catch (JAXBException e) {
			LOG.error(e);
			e.printStackTrace();
			fail("should not go here");
		}
	}
	
	@Test
	public void testCreatePhaseSystemFromPhase(){
		try {
			PhaseSystem phaseSystem = PhaseXmlReader.createPhaseSystemFromPhase(phaseFileURI, ConcentrationUnit.SSIPCONCENTRATION);

			MixtureXmlWriter.marshalPhaseSystem(phaseSystem, actualPhaseSystemFileURI);
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualPhaseSystemFileURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(phaseSystemURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (JAXBException e) {
			LOG.error(e);
			e.printStackTrace();
			fail("should not go here");
		} catch (IOException e) {
			LOG.error("IOEXPCEPTION: {}", e);
			fail("should not go here");
		}
	}
}
