/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.AssociationConstants;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;

public class PhaseConcentrationCalculatorTest {

	private PhaseConcentrationCalculator phaseConcentration1;
	private PhaseConcentrationCalculator phaseConcentration2;
	private Phase phase;
	private ArrayList<SsipConcentration> expectedSSIPConcentrationList;
	private HashSet<PhaseMolecule> ssipConcListSet;
	private HashMap<String, PhaseMolecule> ssipConcListMap;
	private AssociationConstants expectedAssociationConstants;
	private HashMap<String, ArrayList<SsipConcentration>> ssipConcentrationsByMoleculeID;
	private ArrayRealVector expectedFreeConcentration;
	private ArrayRealVector expectedBoundConcentrationTotal;
	private Array2DRowRealMatrix expectedBoundConcentration;
	
	
	static final double ERROR = 1e-7;
	static final double ERROR_IN_DENSITY = 1e-5;

	@Before
	public void setUp() throws Exception {
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "a", 2.0));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "a", -4.0));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(1.0, 0.0, 1.0, "a", -4.0));

		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol2");
		expectedSSIPConcentrationList = new ArrayList<>();
		expectedSSIPConcentrationList.add(ssipConcentration2);
		expectedSSIPConcentrationList.add(ssipConcentration1);
		expectedSSIPConcentrationList.add(ssipConcentration3);
		expectedSSIPConcentrationList.add(ssipConcentration4);
		ArrayList<SsipConcentration> ssipConcentrations1 = new ArrayList<>();
		ssipConcentrations1.add(ssipConcentration1);
		ssipConcentrations1.add(ssipConcentration2);
		ssipConcentrations1.add(ssipConcentration3);
		SsipConcentrationContainer ssipConcentrationContainer1 = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, ssipConcentrations1);
		ArrayList<SsipConcentration> ssipConcentrations2 = new ArrayList<>();
		ssipConcentrations2.add(ssipConcentration4);
		SsipConcentrationContainer ssipConcentrationContainer2 = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, ssipConcentrations2);
		SsipSurface surfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.002, 1000);
		
		String moleculeID1 = "mol1";
		String moleculeID2 = "mol2";
		
		PhaseMolecule phaseMolecule1 = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(surfaceInformation), ssipConcentrationContainer1, moleculeID1, stdinchikey);
		PhaseMolecule phaseMolecule2 = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(surfaceInformation), ssipConcentrationContainer2, moleculeID2, stdinchikey);
		
		ssipConcListSet = new HashSet<>();
		ssipConcListSet.add(phaseMolecule1);
		ssipConcListSet.add(phaseMolecule2);

		ssipConcListMap = new HashMap<>();
		ssipConcListMap.put(moleculeID1, phaseMolecule1);
		ssipConcListMap.put(moleculeID2, phaseMolecule2);

		ssipConcentrationsByMoleculeID = new HashMap<>();
		ssipConcentrationsByMoleculeID.put("mol1",
				(ArrayList<SsipConcentration>) phaseMolecule1.getSSIPConcentrations());
		ssipConcentrationsByMoleculeID.put("mol2",
				(ArrayList<SsipConcentration>) phaseMolecule2.getSSIPConcentrations());
		Temperature expectedTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		phase = new Phase(ssipConcListSet, expectedTemperature, ConcentrationUnit.MOLAR);

		Array2DRowRealMatrix associationConstantValues = new Array2DRowRealMatrix(new double[][]{{4.79227489177873, 4.79227489177873, 121.00048466786859, 121.00048466786859},
				{4.79227489177873, 4.79227489177873, 24.080452950492628, 24.080452950492628},
				{121.00048466786859, 24.080452950492628, 4.79227489177873, 4.79227489177873},
				{121.00048466786859, 24.080452950492628, 4.79227489177873, 4.79227489177873}});
		expectedAssociationConstants = new AssociationConstants(
				associationConstantValues);
		phaseConcentration1 = new PhaseConcentrationCalculator(phase);
		phaseConcentration2 = new PhaseConcentrationCalculator(
				phase.getAssociationConstants(),
				phase.getsortedSSIPConcentrations());
		
		expectedFreeConcentration = new ArrayRealVector(new double[] {
				0.08676986748399186, 0.36733966024767906,
				0.22705476384913603, 0.22705476384913603 });
		expectedBoundConcentrationTotal = new ArrayRealVector(
				new double[] { 9.913230132610302, 9.632660339819983,
						9.772945236069777, 9.772945236069777 });
		expectedBoundConcentration = new Array2DRowRealMatrix(
				new double[][] {
						{ 0.07216217023801696, 0.3054980705468841,
							4.767784945912701, 4.767784945912701 },
						{ 0.3054980705468841, 1.2933240616244759,
								4.0169191038243115, 4.0169191038243115 },
						{ 4.767784945912701,4.0169191038243115,
									0.4941205931663821, 0.4941205931663821 },
						{ 4.767784945912701, 4.0169191038243115,
										0.4941205931663821, 0.4941205931663821 } });
		
	}

	@Test
	public void testPhaseConcentrationPhase() {
		assertNotNull(phaseConcentration1);
		for (int i = 0; i < expectedAssociationConstants.getAssociationConstants().getRowDimension(); i++) {
			assertArrayEquals(expectedAssociationConstants
					.getAssociationConstants().getRow(i), phaseConcentration1
					.getAssociationConstantsMatrix().getRow(i), ERROR);
		}
		
		assertEquals(expectedSSIPConcentrationList,
				phaseConcentration1.getSsipConcentrationList());
	}

	@Test
	public void testPhaseConcentrationAssociationConstantsListOfSSIPConcentration() {
		assertNotNull(phaseConcentration2);
		for (int i = 0; i < expectedAssociationConstants.getAssociationConstants().getRowDimension(); i++) {
			assertArrayEquals(expectedAssociationConstants
					.getAssociationConstants().getRow(i), phaseConcentration2
					.getAssociationConstantsMatrix().getRow(i), ERROR);
		}
		
		assertEquals(expectedSSIPConcentrationList,
				phaseConcentration2.getSsipConcentrationList());
	}

	@Test
	public void testGetTotalConcentrations() {
		ArrayRealVector expectedTotalConcentrations = new ArrayRealVector(
				new double[] { 10.0, 10.0, 10.0, 10.0 });
		assertArrayEquals(expectedTotalConcentrations.toArray(),
				phaseConcentration1.getTotalConcentrations().toArray(), ERROR);
	}

	@Test
	public void testGetAssociationConstantsMatrix() {
		for (int i = 0; i < expectedAssociationConstants.getAssociationConstants().getRowDimension(); i++) {
			assertArrayEquals(expectedAssociationConstants
					.getAssociationConstants().getRow(i), phaseConcentration1
					.getAssociationConstantsMatrix().getRow(i), ERROR);
		}
	}

	@Test
	public void testCalculateFreeAndBoundConcentrations() throws Exception {
		ConcentrationCalculator concentrationCalculator = phaseConcentration1.calculateFreeAndBoundConcentrations();
				

		concentrationCalculator.calculateFreeAndBoundConcentrations();
		assertTrue(concentrationCalculator.totalConcentrationMatches());

		assertArrayEquals(expectedFreeConcentration.toArray(),
				concentrationCalculator.getFreeConcentrations().toArray(),
				ERROR);
		assertArrayEquals(
				expectedBoundConcentrationTotal.toArray(),
				concentrationCalculator.getBoundConcentrationsTotal().toArray(),
				ERROR);

		assertEquals(4, concentrationCalculator.getBoundConcentrations().getRowDimension());
		assertEquals(4,
				concentrationCalculator.getBoundConcentrations().getColumnDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i),
					concentrationCalculator.getBoundConcentrations().getRow(i),
					ERROR);
		}
		
	}

	@Test
	public void testMatchFreeAndBoundConcentrations() throws Exception {
		ConcentrationMatcher concentrationMatcher = phaseConcentration1.matchFreeAndBoundConcentrations(); 
		ArrayList<SsipConcentration> actualSSIPConcentrationList = (ArrayList<SsipConcentration>) concentrationMatcher
				.getSsipConcentrationList();
		for (int i = 0; i < actualSSIPConcentrationList.size(); i++) {
			assertEquals(expectedFreeConcentration.getEntry(i),
					actualSSIPConcentrationList.get(i).getFreeConcentration(),
					ERROR);
			assertEquals(expectedBoundConcentrationTotal.getEntry(i),
					actualSSIPConcentrationList.get(i).getBoundConcentration(),
					ERROR);
			assertArrayEquals(expectedBoundConcentration.getRowVector(i).toArray(),
					actualSSIPConcentrationList.get(i)
							.getBoundConcentrationList().toArray(), ERROR);
		}
	}

	@Test
	public void testCalculateAndSetFreeAndBoundConcentrations() {
		phaseConcentration1.calculateAndSetFreeAndBoundConcentrations();
		assertTrue(phaseConcentration1.isConcentrationsCalculated());
		Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
			 0.07216217023801696, 0.3054980705468841,
				4.767784945912701, 4.767784945912701 },
			{ 0.3054980705468841, 1.2933240616244759,
					4.0169191038243115, 4.0169191038243115 },
			{ 4.767784945912701,4.0169191038243115,
						0.4941205931663821, 0.4941205931663821 },
			{ 4.767784945912701, 4.0169191038243115,
							0.4941205931663821, 0.4941205931663821 } });
		assertArrayEquals(expectedFreeConcentration.toArray(), phaseConcentration1
				.getFreeConcentrations().toArray(), ERROR);
		assertArrayEquals(expectedBoundConcentrationTotal.toArray(), phaseConcentration1
				.getBoundConcentrations().toArray(), ERROR);
		assertEquals(4,
				phaseConcentration1.getAssociationConstantsMatrix().getRowDimension());
		assertEquals(4,
				phaseConcentration1.getAssociationConstantsMatrix().getColumnDimension());
		for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
			assertArrayEquals(expectedBoundConcentration.getRow(i),
					phaseConcentration1.getBoundConcentrationsMatrix().getRow(i),
					ERROR);
		}
	}

	@Test
	public void testGetMoleculeIDs() throws Exception {
		HashSet<String> expectedMoleculeIDs = new HashSet<>();
		expectedMoleculeIDs.add("mol1");
		expectedMoleculeIDs.add("mol2");
		Set<String> actualMoleculeIDs = phaseConcentration1.getMoleculeIDs();
		assertEquals(expectedMoleculeIDs, actualMoleculeIDs);
	}

	@Test
	public void testGroupSSIPConcentrationsByMolecule() {
		HashMap<String, ArrayList<SsipConcentration>> actualSSIPConcentratrionsByMoleculeID = (HashMap<String, ArrayList<SsipConcentration>>) phaseConcentration1
				.groupSSIPConcentrationsByMolecule();
		assertEquals(ssipConcentrationsByMoleculeID,
				actualSSIPConcentratrionsByMoleculeID);
	}

	@Test
	public void testGetSSIPConcentrationListwithMoleculeID() throws Exception {
		Map<String, PhaseMolecule> actualMap = phaseConcentration1.getSSIPConcentrationListwithMoleculeID(); 
		assertEquals(ssipConcListMap, actualMap);
	}

	@Test
	public void testSetFreeAndBoundConcentrationsOfSSIPConcentrationLists()
			throws Exception {
		Set<PhaseMolecule> ssipConcentrationListsWithConcentrations = phaseConcentration1.setFreeAndBoundConcentrationsOfSSIPConcentrationLists();
		assertEquals(2, ssipConcentrationListsWithConcentrations.size());
		for (PhaseMolecule phaseMolecule : ssipConcentrationListsWithConcentrations) {
			if (phaseMolecule.getMoleculeID() == "mol1") {
				double[] expectedFreeConcentration = new double[] {
						0.08676986748399186, 0.36733966024767906,
						0.22705476384913603 };
				double[] expectedBoundConcentrationTotal = new double[] {
						9.913230132610302, 9.632660339819983,
						9.772945236069777 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
					 0.07216217023801696, 0.3054980705468841,
						4.767784945912701, 4.767784945912701 },
					{ 0.3054980705468841, 1.2933240616244759,
							4.0169191038243115, 4.0169191038243115 },
					{ 4.767784945912701,4.0169191038243115,
								0.4941205931663821, 0.4941205931663821 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				Array2DRowRealMatrix actualBoundConcentrationMatrix = phaseMolecule
						.getBoundConcentrationsMatrix();
				assertEquals(3, actualBoundConcentrationMatrix.getRowDimension());
				assertEquals(4, actualBoundConcentrationMatrix.getColumnDimension());
				// 1D serialisation goes down rows first, so transpose is
				// required.
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							actualBoundConcentrationMatrix.getRow(i),
							ERROR);
				}
			} else if (phaseMolecule.getMoleculeID() == "mol2") {
				double[] expectedFreeConcentration = new double[] { 0.22705476384913603 };
				double[] expectedBoundConcentrationTotal = new double[] { 9.772945236069777 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
					 4.767784945912701,4.0169191038243115,
						0.4941205931663821, 0.4941205931663821 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				assertEquals(
						1,
						phaseMolecule.getBoundConcentrationsMatrix().getRowDimension());
				assertEquals(
						4,
						phaseMolecule.getBoundConcentrationsMatrix().getColumnDimension());
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							phaseMolecule.getBoundConcentrationsMatrix().getRow(i),
							ERROR);
				}
			} else {
				fail("detected a different SSIPConcentrationList");
			}
		}
	}

	@Test
	public void testSetFreeAndBoundConcentrationsOfPhase() {
		Phase phaseWithConcentrations = phaseConcentration1
				.setFreeAndBoundConcentrationsOfPhase();
		HashSet<PhaseMolecule> ssipConcentrationListsWithConcentrations = (HashSet<PhaseMolecule>) phaseWithConcentrations
				.getPhaseMoleculesSet();
		assertEquals(2, ssipConcentrationListsWithConcentrations.size());
		for (PhaseMolecule phaseMolecule : ssipConcentrationListsWithConcentrations) {
			if (phaseMolecule.getMoleculeID() == "mol1") {
				double[] expectedFreeConcentration = new double[] {
						0.08676986748399186, 0.36733966024767906,
						0.22705476384913603 };
				double[] expectedBoundConcentrationTotal = new double[] {
						9.913230132610302, 9.632660339819983,
						9.772945236069777 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
					 0.07216217023801696, 0.3054980705468841,
						4.767784945912701, 4.767784945912701 },
					{ 0.3054980705468841, 1.2933240616244759,
							4.0169191038243115, 4.0169191038243115 },
					{ 4.767784945912701,4.0169191038243115,
								0.4941205931663821, 0.4941205931663821 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				Array2DRowRealMatrix actualBoundConcentrationMatrix = phaseMolecule
						.getBoundConcentrationsMatrix();
				assertEquals(3, actualBoundConcentrationMatrix.getRowDimension());
				assertEquals(4, actualBoundConcentrationMatrix.getColumnDimension());
				// 1D serialisation goes down rows first, so transpose is
				// required.
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							actualBoundConcentrationMatrix.getRow(i),
							ERROR);
				}
			} else if (phaseMolecule.getMoleculeID() == "mol2") {
				double[] expectedFreeConcentration = new double[] { 0.22705476384913603 };
				double[] expectedBoundConcentrationTotal = new double[] { 9.772945236069777 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
					 4.767784945912701,4.0169191038243115,
						0.4941205931663821, 0.4941205931663821 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				assertEquals(
						1,
						phaseMolecule.getBoundConcentrationsMatrix().getRowDimension());
				assertEquals(
						4,
						phaseMolecule.getBoundConcentrationsMatrix().getColumnDimension());
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							phaseMolecule.getBoundConcentrationsMatrix().getRow(i),
							ERROR);
				}
			} else {
				fail("detected a different SSIPConcentrationList");
			}
		}
	}
	
	@Test
	public void testPhaseConcentrationCalculator(){
		ArrayRealVector FreeConcentrations = new ArrayRealVector(new double[] {
				0.05186668339004207, 0.24548651049082884, 0.3939105349663359,
				0.3939105349663359 });
		Array2DRowRealMatrix BoundConcentrations = new Array2DRowRealMatrix(
				new double[][] {
						{ 0.005131283597771884, 0.054441050948867135,
								4.944280510576511, 4.944280510576511 },
						{ 0.054441050948867135, 0.385785871734555,
								4.657143301096922, 4.657143301096922 },
						{ 4.944280510576511, 4.657143301096922,
								0.0023328078218614115, 0.0023328078218614115 },
						{ 4.944280510576511, 4.657143301096922,
								0.0023328078218614115, 0.0023328078218614115 } });
		ConcentrationCalculator concentrationCalculator = new ConcentrationCalculator(phase.getTotalConcentrations(), FreeConcentrations, BoundConcentrations);
		PhaseConcentrationCalculator phaseConcentrationCalculator = new PhaseConcentrationCalculator(phase, concentrationCalculator);
		Phase phaseWithConcentrations = phaseConcentrationCalculator.setFreeAndBoundConcentrationsOfPhase();
		HashSet<PhaseMolecule> ssipConcentrationListsWithConcentrations = (HashSet<PhaseMolecule>) phaseWithConcentrations
				.getPhaseMoleculesSet();
		assertEquals(2, ssipConcentrationListsWithConcentrations.size());
		for (PhaseMolecule phaseMolecule : ssipConcentrationListsWithConcentrations) {
			if (phaseMolecule.getMoleculeID() == "mol1") {
				double[] expectedFreeConcentration = new double[] {
						0.05186668339004207, 0.24548651049082884,
						0.3939105349663359 };
				double[] expectedBoundConcentrationTotal = new double[] {
						9.94813335569966, 9.754513524877266, 9.606089427317157 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
						0.005131283597771884, 0.054441050948867135,
						4.944280510576511, 4.944280510576511},
						{0.054441050948867135, 0.385785871734555,
						4.657143301096922, 4.657143301096922},
						{4.944280510576511, 4.657143301096922,
						0.0023328078218614115, 0.0023328078218614115 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				Array2DRowRealMatrix actualBoundConcentrationMatrix = phaseMolecule
						.getBoundConcentrationsMatrix();
				assertEquals(3, actualBoundConcentrationMatrix.getRowDimension());
				assertEquals(4, actualBoundConcentrationMatrix.getColumnDimension());
				// 1D serialisation goes down rows first, so transpose is
				// required.
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							actualBoundConcentrationMatrix.getRow(i),
							ERROR);
				}
			} else if (phaseMolecule.getMoleculeID() == "mol2") {
				double[] expectedFreeConcentration = new double[] { 0.3939105349663359 };
				double[] expectedBoundConcentrationTotal = new double[] { 9.606089427317157 };
				Array2DRowRealMatrix expectedBoundConcentration = new Array2DRowRealMatrix(new double[][] {{
						4.944280510576511, 4.657143301096922,
						0.0023328078218614115, 0.0023328078218614115 }});
				assertArrayEquals(
						expectedFreeConcentration,
						phaseMolecule.getFreeConcentrations().toArray(),
						ERROR);
				assertArrayEquals(expectedBoundConcentrationTotal,
						phaseMolecule.getBoundConcentrations()
								.toArray(), ERROR);
				assertEquals(
						1,
						phaseMolecule.getBoundConcentrationsMatrix().getRowDimension());
				assertEquals(
						4,
						phaseMolecule.getBoundConcentrationsMatrix().getColumnDimension());
				for (int i = 0; i < expectedBoundConcentration.getRowDimension(); i++) {
					assertArrayEquals(expectedBoundConcentration.getRow(i),
							phaseMolecule.getBoundConcentrationsMatrix().getRow(i),
							ERROR);
				}
			} else {
				fail("detected a different SSIPConcentrationList");
			}
		}
	}
}
