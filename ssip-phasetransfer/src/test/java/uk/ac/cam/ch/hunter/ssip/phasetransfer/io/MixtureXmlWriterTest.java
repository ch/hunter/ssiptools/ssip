/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.w3c.dom.Attr;
import org.xmlunit.matchers.CompareMatcher;
import org.xmlunit.util.Nodes;
import org.xmlunit.util.Predicate;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator.PhaseSystemConcentrationCalculator;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseConcentrationFraction;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseConcentrationFractionCollection;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseFactory;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.MixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystem;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSystemMixtureContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;

public class MixtureXmlWriterTest {

	private static final Logger LOG = LogManager.getLogger(MixtureXmlWriterTest.class);
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private URI expectedMixtureURI;
	private URI expectedMixtureListURI;
	private URI expectedPhaseSystemURI;
	private URI expectedPhaseSystemContainerURI;
	private URI expectedPhaseSystemMixtureURI;
	private URI expectedPhaseSystemMixtureContainerURI;
	private URI expectedPhaseConcentrationFractionCollectionURI;
	private URI actualMixtureURI;
	private Phase waterPhase;
	private Mixture waterMixture;
	private HashMap<String, Mixture> mixtureMap;
	private MixtureContainer mixtureContainer;
	private PhaseSystem phaseSystem;
	private PhaseSystemContainer phaseSystemContainer;
	private PhaseSystemMixture phaseSystemMixture;
	private PhaseSystemMixtureContainer phaseSystemMixtureContainer;
	
	// Ignore the "dynamic" value of ssipSoftwareVersion in the testing
    final Predicate<Attr> attributeFilter = new Predicate<Attr>(){
        @Override
        public boolean test(Attr attrName) {
        	return (!Nodes.getQName(attrName).toString().contains("ssipSoftwareVersion"));
        }
    };

	static final double ERROR = 1e-7;
	
	@Before
	public void setUp() throws Exception {
		actualMixtureURI = tempFolder.newFile("actualMixture.xml").toURI(); 
		expectedMixtureURI = MixtureXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseCollection.xml").toURI();
		expectedMixtureListURI = MixtureXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseCollectionList.xml").toURI();
		expectedPhaseSystemURI = MixtureXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystem.xml").toURI();
		expectedPhaseSystemContainerURI = MixtureXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystemContainer.xml").toURI();
		expectedPhaseSystemMixtureURI = MixtureXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystemMixture.xml").toURI();
		expectedPhaseSystemMixtureContainerURI = MixtureXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseSystemMixtureContainer.xml").toURI();
		expectedPhaseConcentrationFractionCollectionURI = MixtureXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedPhaseConcFracCollection.xml").toURI();
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		
		String moleculeID = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		Concentration moleculeConcentration = new Concentration(55.35, ConcentrationUnit.MOLAR);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		String moleculeWater = "water";
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, "water");
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, "water");
		ArrayList<SsipConcentration> waterSsipConcentrations = new ArrayList<>();
		waterSsipConcentrations.add(ssipWaterAlpha1Conc);
		waterSsipConcentrations.add(ssipWaterAlpha2Conc);
		waterSsipConcentrations.add(ssipWaterBeta1Conc);
		waterSsipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer waterContainer = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, waterSsipConcentrations);
		SsipSurface waterSurfaceInformation = new SsipSurface(37.34, 18.46, 18.879, 0.002, 2200); 
		waterSurfaceInformation.setVolumeVdW(21.4);
		PhaseMolecule waterMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(waterSurfaceInformation), waterContainer, moleculeWater, stdinchikey);
		waterMolecule.setMoleFraction(1.0);
		LOG.trace("Water molecule: {}", waterMolecule.getMoleFraction());
		HashSet<PhaseMolecule> waterPhaseMolecules = new HashSet<>();
		waterPhaseMolecules.add(waterMolecule);
		
		Temperature phaseTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		PhaseFactory phaseFactory = new PhaseFactory();
		
		waterPhase = phaseFactory.generatePhase(waterPhaseMolecules, phaseTemperature, ConcentrationUnit.MOLAR);
		waterPhase.setSolventID(moleculeWater);
		HashMap<String, Phase> phaseMap = new HashMap<>();
		phaseMap.put(moleculeID, waterPhase);
		waterMixture = new Mixture(phaseMap);
		mixtureMap = new HashMap<>();
		mixtureMap.put(moleculeID + "solute", waterMixture);
		mixtureContainer = new MixtureContainer(mixtureMap);
		
		Phase liquidPhase = waterPhase.convertConcentrationUnitTo(ConcentrationUnit.SSIPCONCENTRATION);
		LOG.debug("liquid phase: {}", liquidPhase.toString());
		LOG.trace("Liquid molecules: {}", liquidPhase.getPhaseMolecules());
		LOG.trace("Water molecule: {}", liquidPhase.getPhaseMolecules().get("water").toString());
		LOG.trace("Water molecule: {}", liquidPhase.getPhaseMolecules().get("water").getMoleFraction());
		LOG.trace("Water molecule: {}", liquidPhase.getPhaseMolecules().get("water").getSsipConcentrationContainer().getConcentrationUnit());
		LOG.trace("Water molecule: {}", liquidPhase.getPhaseMolecules().get("water").getSsipConcentrationContainer().getSsipConcentrations());
		PhaseSystemConcentrationCalculator phaseSysCalculator = new PhaseSystemConcentrationCalculator(liquidPhase);
		phaseSysCalculator.calculateSpeciation();
		phaseSystem = new PhaseSystem(phaseSysCalculator.getGasPhase(), phaseSysCalculator.getLiquidPhase(), "water");
		phaseSystem.setMixtureID(moleculeWater);
		HashMap<String, PhaseSystem> phaseSystemMap = new HashMap<>();
		phaseSystemMap.put("water", phaseSystem);
		phaseSystemContainer = new PhaseSystemContainer(phaseSystemMap);
		
		phaseSystemMixture = new PhaseSystemMixture(phaseSystemMap);
		phaseSystemMixture.setSoluteID("water");
		
		HashMap<String, PhaseSystemMixture> phaseMixtureMap = new HashMap<>();
		phaseMixtureMap.put("water", phaseSystemMixture);
		phaseSystemMixtureContainer = new PhaseSystemMixtureContainer(phaseMixtureMap);
	}

	@After
	public void tearDown() throws Exception {
		//use this to flush file contents.
		new FileOutputStream(new File(actualMixtureURI)).close();
	}

	@Test
	public void testMarshal(){
		try {
			MixtureXmlWriter.marshal(waterMixture, actualMixtureURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualMixtureURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedMixtureURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
	@Test
	public void testMarshalContainer(){
		try {
			MixtureXmlWriter.marshalContainer(mixtureContainer, actualMixtureURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualMixtureURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedMixtureListURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
	@Test
	public void testMarshalPhaseSystem(){
		try {
			MixtureXmlWriter.marshalPhaseSystem(phaseSystem, actualMixtureURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualMixtureURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedPhaseSystemURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
	@Test
	public void testMarshalPhaseSystemContainer(){
		try {
			MixtureXmlWriter.marshalPhaseSystemContainer(phaseSystemContainer, actualMixtureURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualMixtureURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedPhaseSystemContainerURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
	@Test
	public void testMarshalPhaseSystemMixture(){
		try {
			MixtureXmlWriter.marshalPhaseSystemMixture(phaseSystemMixture, actualMixtureURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualMixtureURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedPhaseSystemMixtureURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
	@Test
	public void testMarshalPhaseSystemMixtureContainer(){
		try {
			MixtureXmlWriter.marshalPhaseSystemMixtureContainer(phaseSystemMixtureContainer, actualMixtureURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualMixtureURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedPhaseSystemMixtureContainerURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
	
	@Test
	public void testMarshalPhaseConcentrationFractionCollection(){
		ArrayList<PhaseConcentrationFraction> phaseConcentrationFractions = (ArrayList<PhaseConcentrationFraction>) waterMixture.calculatePhaseConcentrationFractions();
		PhaseConcentrationFractionCollection phaseConcFracCollection = new PhaseConcentrationFractionCollection(phaseConcentrationFractions);
		try {
			MixtureXmlWriter.marshalPhaseConcentrationFractionCollection(phaseConcFracCollection, actualMixtureURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(actualMixtureURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedPhaseConcentrationFractionCollectionURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter).withDifferenceEvaluator(new ConcentrationDifferenceEvaluator()));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}

}
