/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Mixture;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseSolvent;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;

public class TotalConcentrationMatcherTest {

	private static final Logger LOG = LogManager.getLogger(TotalConcentrationMatcherTest.class);
	
	private Mixture mixture;
	private SolventContainer solventContainer;
	private PhaseSolvent phaseSolvent;
	private HashMap<String, PhaseMolecule> solventMolecules;
	private ArrayList<SsipConcentration> ssipTotalConcentrations;
	private HashMap<String, ArrayList<SsipConcentration>> totalConcSetByMolecule;
	
	@Before
	public void setUp() throws Exception {
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		ssipSurfaceInformation.setVolumeVdW(21.4);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		Concentration moleculeConcentration = new Concentration(50.0/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, moleculeid);
		ArrayList<SsipConcentration> ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipWaterAlpha2Conc);
		ssipConcentrations.add(ssipWaterAlpha1Conc);
		ssipConcentrations.add(ssipWaterBeta1Conc);
		ssipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrations);
		
		PhaseMolecule phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		LOG.debug("volumes: {}", phaseMolecule.getVdWVolumes());
		phaseMolecule.setMoleFraction(1.0);
		solventMolecules = new HashMap<>();
		solventMolecules.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		
		String solventId = "water";
		String solventName = "water";
		phaseSolvent = new PhaseSolvent(solventMolecules, solventId, solventName, ConcentrationUnit.SSIPCONCENTRATION);
		HashMap<String, PhaseSolvent> solventMap = new HashMap<>();
		solventMap.put(solventId, phaseSolvent);
		solventContainer = new SolventContainer(solventMap);
		
		Phase phase = new Phase(solventMolecules, new Temperature(298.0, TemperatureUnit.KELVIN), ConcentrationUnit.SSIPCONCENTRATION);
		HashMap<String, Phase> phaseMap = new HashMap<>();
		phaseMap.put("water", phase);
		mixture = new Mixture(phaseMap);
		
		Concentration totalCalculatedConcentration = new Concentration(0.18264539823230322, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssip1 = new SsipConcentration(ssipWaterAlpha1,
				totalCalculatedConcentration, moleculeid);
		SsipConcentration ssip2 = new SsipConcentration(ssipWaterAlpha2,
				totalCalculatedConcentration, moleculeid);
		SsipConcentration ssip3 = new SsipConcentration(ssipWaterBeta1,
				totalCalculatedConcentration, moleculeid);
		SsipConcentration ssip4 = new SsipConcentration(ssipWaterBeta2,
				totalCalculatedConcentration, moleculeid);
		ssipTotalConcentrations = new ArrayList<>();
		ssipTotalConcentrations.add(ssip1);
		ssipTotalConcentrations.add(ssip2);
		ssipTotalConcentrations.add(ssip3);
		ssipTotalConcentrations.add(ssip4);
		totalConcSetByMolecule = new HashMap<>();
		totalConcSetByMolecule.put(moleculeid, ssipTotalConcentrations);
	}

	@Test
	public void testSetSolventTotalConcentrationsPhaseSolventDouble() {
		PhaseSolvent newPhaseSolvent = (PhaseSolvent) TotalConcentrationMatcher.setIPhaseTotalConcentrations(phaseSolvent, 298.0);
		assertEquals(ssipTotalConcentrations, newPhaseSolvent.getsortedSsips());
	}

	@Test
	public void testSetSolventTotalConcentrationsSolventContainerDouble() {
		TotalConcentrationMatcher.setSolventTotalConcentrations(solventContainer, 298.0);
		assertEquals(ssipTotalConcentrations, solventContainer.getSolventMap().get("water").getsortedSsips());
	}

	@Test
	public void testSetPhaseTotalConcentrations(){
		
		TotalConcentrationMatcher.setPhaseTotalConcentrations(mixture);
		assertEquals(ssipTotalConcentrations, mixture.getPhaseMap().get("water").getsortedSsips());
	}
	
}
