/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;


import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseType;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;

public class PhaseSystemConcentrationCalculatorTest {

	private static final Logger LOG = LogManager.getLogger(PhaseSystemConcentrationCalculatorTest.class);

	private PhaseSystemConcentrationCalculator waterSystemCalculator;
	private PhaseSystemConcentrationCalculator hotWaterSystemCalculator;
	private Phase liquidPhase;
	private Phase hotLiquidPhase;
	private HashMap<String, PhaseMolecule> phaseMolecules;
	private ArrayList<SsipConcentration> ssipConcentrations;
	private ArrayList<SsipConcentration> gasSsipConcentrations;
	private double temperature;
	
	private static final double ERROR = 1.0e-7;
	
	@Before
	public void setUp() throws Exception {
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "water";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000);
		//volumes in cubic angstroms. taken from DOI: 10.1039/c2sc21666c
		ssipSurfaceInformation.setVolumeVdW(21.4);
		
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		Concentration moleculeConcentration = new Concentration(55.35/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				moleculeConcentration, moleculeid);
		SsipConcentration ssipWaterBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				moleculeConcentration, moleculeid);
		ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipWaterAlpha2Conc);
		ssipConcentrations.add(ssipWaterAlpha1Conc);
		ssipConcentrations.add(ssipWaterBeta1Conc);
		ssipConcentrations.add(ssipWaterBeta2Conc);
		SsipConcentrationContainer ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrations);
		
		Concentration gasConcentration = new Concentration(2.768433896064454E-6, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipGasAlpha1Conc = new SsipConcentration(ssipWaterAlpha1,
				gasConcentration, moleculeid);
		SsipConcentration ssipGasAlpha2Conc = new SsipConcentration(ssipWaterAlpha2,
				gasConcentration, moleculeid);
		SsipConcentration ssipGasBeta1Conc = new SsipConcentration(ssipWaterBeta1,
				gasConcentration, moleculeid);
		SsipConcentration ssipGasBeta2Conc = new SsipConcentration(ssipWaterBeta2,
				gasConcentration, moleculeid);
		gasSsipConcentrations = new ArrayList<>();
		gasSsipConcentrations.add(ssipGasAlpha2Conc);
		gasSsipConcentrations.add(ssipGasAlpha1Conc);
		gasSsipConcentrations.add(ssipGasBeta1Conc);
		gasSsipConcentrations.add(ssipGasBeta2Conc);
		
		PhaseMolecule phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		LOG.debug("volumes: {}", phaseMolecule.getVdWVolumes());
		phaseMolecule.setMoleFraction(1.0);
		phaseMolecules = new HashMap<>();
		phaseMolecules.put(phaseMolecule.getMoleculeID(), phaseMolecule);
		
		String solventId = "water";
		//temperature in kelvin.
		temperature = 298.0;
		
		liquidPhase = new Phase(phaseMolecules, new Temperature(temperature, TemperatureUnit.KELVIN), ConcentrationUnit.SSIPCONCENTRATION);
		liquidPhase.setSolventID(solventId);
		liquidPhase.setPhaseType(PhaseType.CONDENSED);
		
		waterSystemCalculator = new PhaseSystemConcentrationCalculator(liquidPhase);
		
		hotLiquidPhase = new Phase(phaseMolecules, new Temperature(600.0, TemperatureUnit.KELVIN), ConcentrationUnit.SSIPCONCENTRATION);
		hotLiquidPhase.setSolventID(solventId);
		hotLiquidPhase.setPhaseType(PhaseType.CONDENSED);
		
		hotWaterSystemCalculator = new PhaseSystemConcentrationCalculator(hotLiquidPhase);
	}

	@Test
	public void testPhaseSystemConcentrationCalculator() {
		assertNotNull(waterSystemCalculator);
	}

	@Test
	public void testCalculateSpeciation() {
		waterSystemCalculator.calculateSpeciation();
		double[] expectedLiquidtotals = new double[]{0.18264189106758083, 0.18264189106758083, 0.18264189106758083, 0.18264189106758083};
		double[] expectedGasTotals = new double[]{2.768433988823446E-6, 2.768433988823446E-6, 2.768433988823446E-6, 2.768433988823446E-6};
		assertArrayEquals(expectedLiquidtotals, waterSystemCalculator.getLiquidPhase().getTotalConcentrations().toArray(), ERROR);
		assertArrayEquals(expectedGasTotals, waterSystemCalculator.getGasPhase().getTotalConcentrations().toArray(), ERROR);
	}

	@Test
	public void testCalculateSpeciationHot() {
		hotWaterSystemCalculator.calculateSpeciation();
		double[] expectedLiquidtotals = new double[]{0.1163083075587531, 0.1163083075587531, 0.1163083075587531, 0.1163083075587531};
		double[] expectedGasTotals = new double[]{0.014050289636520639, 0.014050289636520639, 0.014050289636520639, 0.014050289636520639};
		assertArrayEquals(expectedLiquidtotals, hotWaterSystemCalculator.getLiquidPhase().getTotalConcentrations().toArray(), ERROR);
		assertArrayEquals(expectedGasTotals, hotWaterSystemCalculator.getGasPhase().getTotalConcentrations().toArray(), ERROR);
	}
	
	@Test
	public void testGetGasEnergyBySSIP() {
		double[] expectedEnergies = new double[]{-0.08423228035881981, -0.08423228035881981, -0.08423228035881981, -0.08423228035881981};
		LOG.trace("total gas conc matcher: {}", waterSystemCalculator.getGasConcentrationMatcher().getConcentrationCalculator().getTotalConcentrations().toArray());
		LOG.trace("gas conc matcher SSIPs: {}", waterSystemCalculator.getGasConcentrationMatcher().getConcentrationsByMolID());
		LOG.trace("total gas concs: {}", waterSystemCalculator.getGasPhase().getTotalConcentrations());
		LOG.trace("free gas concs: {}", waterSystemCalculator.getGasPhase().getFreeConcentrations());
		LOG.trace("water energy: {}", waterSystemCalculator.getGasPhase().calculateEnergyInPhase("water"));
		assertArrayEquals(expectedEnergies, waterSystemCalculator.getGasEnergyBySSIP().toArray(), ERROR);
	}

	@Test
	public void testGetLiquidEnergyBySSIP() {
		double[] expectedEnergies = new double[] {-25.777735220085034, -25.777735220085034, -25.777735220085034, -25.777735220085034};
		LOG.trace("total liquid concs: {}", waterSystemCalculator.getLiquidPhase().getTotalConcentrations());
		LOG.trace("free liquid concs: {}", waterSystemCalculator.getLiquidPhase().getFreeConcentrations());
		LOG.trace("water energy: {}", waterSystemCalculator.getLiquidPhase().calculateEnergyInPhase("water"));
		waterSystemCalculator.calculateSpeciation();
		assertArrayEquals(expectedEnergies, waterSystemCalculator.getLiquidEnergyBySSIP().toArray(), ERROR);
	}

	@Test
	public void testCalculateEnergyDifferenceBetweenPhases() {
		double[] energyDifferences = new double[]{7.71110182229545, 7.71110182229545, 7.71110182229545, 7.71110182229545};
		assertArrayEquals(energyDifferences, waterSystemCalculator.calculateEnergyDifferenceBetweenPhases().toArray(), ERROR);
		waterSystemCalculator.calculateSpeciation();
		double[] energyDifferences2 = new double[]{25.690523954895035, 25.690523954895035, 25.690523954895035, 25.690523954895035};
		assertArrayEquals(energyDifferences2, waterSystemCalculator.calculateEnergyDifferenceBetweenPhases().toArray(), ERROR);
	}

	@Test
	public void testCalculateGasTotalConcentrations() throws Exception {
		ArrayRealVector energyDifferences = new ArrayRealVector(new double[]{1.0, 1.0, 1.0, 1.0});
		double[] expectedConcentrations = new double[]{0.060995542051273506, 0.060995542051273506, 0.060995542051273506, 0.060995542051273506};
		ArrayRealVector gasConcs =  waterSystemCalculator.calculateGasTotalConcentrations(energyDifferences);
		assertArrayEquals(expectedConcentrations, gasConcs.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateLiquidSpeciationSetPhase() throws Exception {
		waterSystemCalculator.calculateLiquidSpeciationSetPhase();
		double[] totalConcs = new double[]{0.18264539823230322, 0.18264539823230322, 0.18264539823230322, 0.18264539823230322};
		assertArrayEquals(totalConcs, waterSystemCalculator.getLiquidPhase().getTotalConcentrations().toArray(), ERROR);
		
	}
	
	@Test
	public void testCalculateGasSpeciationSetPhase() throws Exception {		
		waterSystemCalculator.calculateGasSpeciationSetPhase();
		double[] totalConcs = new double[]{2.768433896064454E-6, 2.768433896064454E-6, 2.768433896064454E-6, 2.768433896064454E-6};
		assertArrayEquals(totalConcs, waterSystemCalculator.getGasPhase().getTotalConcentrations().toArray(), ERROR);
	}
	
	@Test
	public void testGetTemperature() {
		assertEquals(298.0, waterSystemCalculator.getTemperature(), ERROR);
	}

	@Test
	public void testGetLiquidPhase() {
		assertEquals(liquidPhase, waterSystemCalculator.getLiquidPhase());
	}

	@Test
	public void testGetGasPhase() {
		assertEquals(PhaseType.GAS, waterSystemCalculator.getGasPhase().getPhaseType());
		assertEquals(gasSsipConcentrations, waterSystemCalculator.getGasPhase().getsortedSsips());
	}

}
