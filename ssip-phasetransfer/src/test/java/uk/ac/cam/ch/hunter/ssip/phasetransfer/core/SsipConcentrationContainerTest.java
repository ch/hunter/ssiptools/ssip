/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;

public class SsipConcentrationContainerTest {

	private ArrayList<SsipConcentration> expectedSsipConcentrationList;
	private ArrayList<SsipConcentration> expectedSsipConcentrationsNormalised;
	private ArrayList<SsipConcentration> ssipConcentrations;
	private SsipConcentrationContainer ssipConcentrationContainer;
	
	@Before
	public void setUp() throws Exception {
		Ssip ssip1 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "a", 1.0));
		Ssip ssip2 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "a", 2.0));
		Ssip ssip3 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "a", -4.0));
		Ssip ssip4 = Ssip.fromMepPoint(new BetaPoint(1.0, 0.0, 1.0, "a", -4.0));
		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		expectedSsipConcentrationList = new ArrayList<>();
		expectedSsipConcentrationList.add(ssipConcentration2);
		expectedSsipConcentrationList.add(ssipConcentration1);
		expectedSsipConcentrationList.add(ssipConcentration3);
		expectedSsipConcentrationList.add(ssipConcentration4);
		ssipConcentrations = new ArrayList<>();
		ssipConcentrations.add(ssipConcentration1);
		ssipConcentrations.add(ssipConcentration2);
		ssipConcentrations.add(ssipConcentration3);
		ssipConcentrations.add(ssipConcentration4);
		SsipConcentration ssipConcentration1norm = new SsipConcentration(ssip1,
				new Concentration(10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration2norm = new SsipConcentration(ssip2,
				new Concentration(10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration3norm = new SsipConcentration(ssip3,
				new Concentration(10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		SsipConcentration ssipConcentration4norm = new SsipConcentration(ssip4,
				new Concentration(10.0/300.0, ConcentrationUnit.SSIPCONCENTRATION), "mol1");
		expectedSsipConcentrationsNormalised = new ArrayList<>();
		expectedSsipConcentrationsNormalised.add(ssipConcentration2norm);
		expectedSsipConcentrationsNormalised.add(ssipConcentration1norm);
		expectedSsipConcentrationsNormalised.add(ssipConcentration3norm);
		expectedSsipConcentrationsNormalised.add(ssipConcentration4norm);
		
		
		ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, ssipConcentrations);
	}

	@Test
	public void testSsipConcentrationContainerCollectionOfSSIPConcentration() {
		assertNotNull(ssipConcentrationContainer);
	}

	@Test
	public void testGetSsipConcentrations() {
		assertEquals(expectedSsipConcentrationList, ssipConcentrationContainer.getSsipConcentrations());
	}

	@Test
	public void testConvertConcentrationsTo(){
		SsipConcentrationContainer convertedContainer = ssipConcentrationContainer.convertConcentrationsTo(ConcentrationUnit.SSIPCONCENTRATION);
		assertEquals(expectedSsipConcentrationsNormalised, convertedContainer.getSsipConcentrations());
		SsipConcentrationContainer expectedContainer = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, expectedSsipConcentrationsNormalised);
		assertEquals(expectedContainer, convertedContainer);
	}
	
}
