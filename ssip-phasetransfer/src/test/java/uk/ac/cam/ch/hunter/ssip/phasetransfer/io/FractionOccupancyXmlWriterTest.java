/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.xmlunit.matchers.CompareMatcher;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.FractionalOccupancy;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.FractionalOccupancyCollection;

public class FractionOccupancyXmlWriterTest {
	
	private static final Logger LOG = LogManager.getLogger(FractionOccupancyXmlWriterTest.class);

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private URI expectedFracOccCollectionURI;
	private URI fracOccCollectionURI;
	private FractionalOccupancyCollection fractionalOccupancyCollection;
	
	@Before
	public void setUp() throws Exception {
		expectedFracOccCollectionURI = FractionOccupancyXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/fracOccCollection.xml").toURI();
		fracOccCollectionURI = tempFolder.newFile("actualFracOccCollection.xml").toURI();
		FractionalOccupancy fractionalOccupancy = new FractionalOccupancy(0.738, "water", "", 298.0);
		fractionalOccupancyCollection = new FractionalOccupancyCollection(new HashSet<FractionalOccupancy>());
		fractionalOccupancyCollection.addFractionalOccupancy(fractionalOccupancy);
	}
	
	@After
	public void tearDown() throws Exception {
		//use this to flush file contents.
		new FileOutputStream(new File(fracOccCollectionURI)).close();
	}
	
	@Test
	public void testMarshalCollection() {
		try {
			FractionalOccupancyXmlWriter.marshalCollection(fractionalOccupancyCollection, fracOccCollectionURI);;
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			fail("should not go here");
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(fracOccCollectionURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(expectedFracOccCollectionURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}

}
