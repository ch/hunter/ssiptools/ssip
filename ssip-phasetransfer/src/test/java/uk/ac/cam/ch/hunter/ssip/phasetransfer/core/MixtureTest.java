/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.PhaseFactory;

public class MixtureTest {

	private static final Logger LOG = LogManager.getLogger(MixtureTest.class);
	
	private Mixture mixture1;
	private Mixture mixture2;
	private Phase waterPhase;
	private Phase waterPhaseWithSolute;
	private ArrayList<EnergyValue> freeEnergyListMoleFraction;
	private ArrayList<EnergyValue> freeEnergyListMolar;
	static final double ERROR = 1e-10;

	
	@Before
	public void setUp() throws Exception {
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(37.34, 18.46, 18.879, 0.002, 2200);
		Ssip ssipWaterAlpha1 = Ssip.fromMepPoint(new AlphaPoint(0.0, 1.0, 0.0, "H", 2.8));
		Ssip ssipWaterAlpha2 = Ssip.fromMepPoint(new AlphaPoint(1.0, 0.0, 0.0, "H", 2.8));
		Ssip ssipWaterBeta1 = Ssip.fromMepPoint(new BetaPoint(0.0, 0.0, 1.0, "O", -4.5));
		Ssip ssipWaterBeta2 = Ssip.fromMepPoint(new BetaPoint(0.0, 1.0, 1.0, "O", -4.5));
		HashSet<Ssip> ssipListWater = new HashSet<>();
		ssipListWater.add(ssipWaterAlpha1);
		ssipListWater.add(ssipWaterAlpha2);
		ssipListWater.add(ssipWaterBeta1);
		ssipListWater.add(ssipWaterBeta2);
		
		Concentration waterMoleculeConcentration =  new Concentration(55.35 / 300, ConcentrationUnit.SSIPCONCENTRATION);
		Concentration soluteConcentration =  new Concentration(0.001/300, ConcentrationUnit.SSIPCONCENTRATION);
		SsipConcentration ssipWaterAlpha1ConcSolv = new SsipConcentration(ssipWaterAlpha1,
				waterMoleculeConcentration, "water");
		SsipConcentration ssipWaterAlpha2ConcSolv = new SsipConcentration(ssipWaterAlpha2,
				waterMoleculeConcentration, "water");
		SsipConcentration ssipWaterBeta1ConcSolv = new SsipConcentration(ssipWaterBeta1,
				waterMoleculeConcentration, "water");
		SsipConcentration ssipWaterBeta2ConcSolv = new SsipConcentration(ssipWaterBeta2,
				waterMoleculeConcentration, "water");
		ArrayList<SsipConcentration> ssipConcentrationsSolvent = new ArrayList<>();
		ssipConcentrationsSolvent.add(ssipWaterAlpha1ConcSolv);
		ssipConcentrationsSolvent.add(ssipWaterAlpha2ConcSolv);
		ssipConcentrationsSolvent.add(ssipWaterBeta1ConcSolv);
		ssipConcentrationsSolvent.add(ssipWaterBeta2ConcSolv);
		SsipConcentrationContainer concentrationContainerSolvent = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrationsSolvent);
		SsipConcentration ssipWaterAlpha1ConcSolute = new SsipConcentration(ssipWaterAlpha1,
				soluteConcentration, "SoluteWater");
		SsipConcentration ssipWaterAlpha2ConcSolute = new SsipConcentration(ssipWaterAlpha2,
				soluteConcentration, "SoluteWater");
		SsipConcentration ssipWaterBeta1ConcSolute = new SsipConcentration(ssipWaterBeta1,
				soluteConcentration, "SoluteWater");
		SsipConcentration ssipWaterBeta2ConcSolute = new SsipConcentration(ssipWaterBeta2,
				soluteConcentration, "SoluteWater");
		ArrayList<SsipConcentration> ssipConcentrationsSolute = new ArrayList<>();
		ssipConcentrationsSolute.add(ssipWaterAlpha1ConcSolute);
		ssipConcentrationsSolute.add(ssipWaterAlpha2ConcSolute);
		ssipConcentrationsSolute.add(ssipWaterBeta1ConcSolute);
		ssipConcentrationsSolute.add(ssipWaterBeta2ConcSolute);
		LOG.debug("solute conc list: {}", ssipConcentrationsSolute);
		SsipConcentrationContainer concentrationContainerSolute = new SsipConcentrationContainer(ConcentrationUnit.SSIPCONCENTRATION, ssipConcentrationsSolute);
		String moleculeWater = "water";
		String moleculeWaterSolute = "SoluteWater";
		
		PhaseMolecule phaseMoleculeWater = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), concentrationContainerSolvent, moleculeWater, stdinchikey);
		PhaseMolecule phaseMoleculeSolute = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), concentrationContainerSolute, moleculeWaterSolute, stdinchikey);

		HashSet<PhaseMolecule> phaseMoleculesWater = new HashSet<>();
		phaseMoleculesWater.add(phaseMoleculeWater);
		HashSet<PhaseMolecule> phaseMoleculesWaterWithSolute = new HashSet<>();
		phaseMoleculesWaterWithSolute.add(phaseMoleculeWater);
		phaseMoleculesWaterWithSolute.add(phaseMoleculeSolute);
		LOG.debug("phase mols with solute: {}", phaseMoleculesWaterWithSolute);
		LOG.debug("solute mol ID: {} Solvent mol ID: {}", phaseMoleculeSolute.getMoleculeID(), phaseMoleculeWater.getMoleculeID());
		PhaseFactory phaseFactory = new PhaseFactory();
		Temperature expectedTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		waterPhase = phaseFactory.generatePhase(phaseMoleculesWater, expectedTemperature, ConcentrationUnit.SSIPCONCENTRATION);
		waterPhase.setSolventID("water");
		waterPhase.setPhaseType(PhaseType.CONDENSED);
		waterPhaseWithSolute = phaseFactory.generatePhase(phaseMoleculesWaterWithSolute, expectedTemperature, ConcentrationUnit.SSIPCONCENTRATION); 
		HashMap<String, Phase> phaseMap1 = new HashMap<>();
		phaseMap1.put("water", waterPhase);
		HashMap<String, Phase> phaseMap2 = new HashMap<>();
		phaseMap2.put("water", waterPhase);
		phaseMap2.put("waterSolute", waterPhaseWithSolute);
		mixture1 = new Mixture(phaseMap1);
		mixture2 = new Mixture(phaseMap2);

		freeEnergyListMoleFraction = new ArrayList<>();
		freeEnergyListMoleFraction.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLEFRACTION));
		freeEnergyListMolar = new ArrayList<>();
		freeEnergyListMolar.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR));
	}

	@Test
	public void testMixture() {
		assertNotNull(mixture1);
		HashMap<String, Phase> phaseMap1 = new HashMap<>();
		phaseMap1.put("water", waterPhase);
		assertEquals(phaseMap1, mixture1.getPhaseMap());
	}

	@Test
	public void testCalculateConfinementEnergyDifference(){
		assertEquals(0.0, mixture2.calculateConfinementEnergyDifference("SoluteWater", "waterSolute", "waterSolute"), ERROR);
		assertEquals(5.5455262206294265E-5, mixture2.calculateConfinementEnergyDifference("water", "water", "waterSolute"), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferencePerSSIP(){
		ArrayRealVector actualValues = mixture2.calculateConfinementEnergyDifferencePerSSIP("water", "water", "waterSolute");
		double[] expectedValues = new double[]{1.3863815551573566E-5, 1.3863815551573566E-5, 1.3863815551573566E-5, 1.3863815551573566E-5};
		assertArrayEquals(expectedValues, actualValues.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferenceValue(){
		EnergyValue actualValue = mixture1.calculateConfinementEnergyDifferenceValue("water", "water", "water");
		assertEquals(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLEFRACTION), actualValue);
		assertEquals(0.0, actualValue.getValue(), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferenceToPhase(){
		ArrayList<EnergyValue> actualConfinementEnergyList = (ArrayList<EnergyValue>) mixture1.calculateConfinementEnergyDifferenceToPhase("water", "water");
		assertEquals(freeEnergyListMoleFraction, actualConfinementEnergyList);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferenceBetweenAllPhases(){
		ArrayList<EnergyValue> actualConfinementEnergyList = (ArrayList<EnergyValue>) mixture1.calculateConfinementEnergyDifferenceBetweenAllPhases("water");
		assertEquals(freeEnergyListMoleFraction, actualConfinementEnergyList);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferenceFor1M(){
		assertEquals(1.0691275964092029E-5, mixture2.calculateConfinementEnergyDifferenceFor1M("water", "water", "waterSolute"), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferenceFor1MPerSSIP(){
		ArrayRealVector actualMatrix = mixture2.calculateConfinementEnergyDifferenceFor1MPerSSIP("water", "water", "waterSolute");
		double[] expectedArray = new double[]{1.3863815551573566E-5, 1.3863815551573566E-5, 1.3863815551573566E-5, 1.3863815551573566E-5};
		assertArrayEquals(expectedArray, actualMatrix.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferenceValue1M(){
		EnergyValue actualValue = mixture1.calculateConfinementEnergyDifferenceValue1M("water", "water", "water");
		assertEquals(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR), actualValue);
		assertEquals(0.0, actualValue.getValue(), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferenceToPhase1M(){
		ArrayList<EnergyValue> actualFreeEnergyList = (ArrayList<EnergyValue>) mixture1.calculateConfinementEnergyDifferenceToPhase1M("water", "water");
		assertEquals(freeEnergyListMolar, actualFreeEnergyList);
	}
	
	@Test
	public void testCalculateConfinementEnergyDifferenceBetweenAllPhases1M(){
		ArrayList<EnergyValue> actualFreeEnergyList = (ArrayList<EnergyValue>) mixture1.calculateConfinementEnergyDifferenceBetweenAllPhases1M("water");
		assertEquals(freeEnergyListMolar, actualFreeEnergyList);
	}

	@Test
	public void testCalculateConfinementEnergySolvation(){
		assertEquals(5.901004091684356, mixture1.calculateConfinementEnergySolvation("water", "water"), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergySolvationPerSSIP(){
		ArrayRealVector actualArray = mixture2.calculateConfinementEnergySolvationPerSSIP("water", "water");
		double[] expectedArray = new double[]{1.475251022921089, 1.475251022921089, 1.475251022921089, 1.475251022921089};
		assertArrayEquals(expectedArray, actualArray.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateConfinementEnergySolvationEnergyValue(){
		assertEquals(new EnergyValue("water", "water", "", 5.901004091684356, new ArrayRealVector(new double[]{1.475251022921089, 1.475251022921089, 1.475251022921089, 1.475251022921089}), EnergyValueType.MOLEFRACTION), mixture1.calculateConfinementEnergySolvationEnergyValue("water", "water"));
	}
	
	@Test
	public void testCalculateConfinementEnergySolvationAllPhases(){
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", 5.901004091684356, new ArrayRealVector(new double[]{1.475251022921089, 1.475251022921089, 1.475251022921089, 1.475251022921089}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedList, mixture1.calculateConfinementEnergySolvationAllPhases("water"));
	}
	
	@Test
	public void testCalculateConfinementEnergyValuesAllPhasesAllValues(){
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", 5.901004091684356, new ArrayRealVector(new double[]{1.475251022921089, 1.475251022921089, 1.475251022921089, 1.475251022921089}), EnergyValueType.MOLEFRACTION));
		expectedList.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLEFRACTION));
		expectedList.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR));
		ArrayList<EnergyValue> actualList = (ArrayList<EnergyValue>) mixture1.calculateConfinementEnergyValuesAllPhasesAllValues("water");
		assertEquals(expectedList, actualList);
	}
	
	@Test
	public void testCalculateBindingEnergyDifference(){
		assertEquals(0.0, mixture2.calculateBindingEnergyDifference("SoluteWater", "waterSolute", "waterSolute"), ERROR);
		assertEquals(-8.766191120201938E-5, mixture2.calculateBindingEnergyDifference("water", "water", "waterSolute"), ERROR);
	}
	
	@Test
	public void testCalculateBindingEnergyDifferencePerSSIP(){
		ArrayRealVector actualValues = mixture2.calculateBindingEnergyDifferencePerSSIP("water", "water", "waterSolute");
		double[] expectedValues = new double[]{-2.1915477800504846E-5, -2.1915477800504846E-5, -2.1915477800504846E-5, -2.1915477800504846E-5};
		assertArrayEquals(expectedValues, actualValues.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateBindingEnergyDifferenceValue(){
		EnergyValue actualValue = mixture1.calculateBindingEnergyDifferenceValue("water", "water", "water");
		assertEquals(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLEFRACTION), actualValue);
		assertEquals(0.0, actualValue.getValue(), ERROR);
	}
	
	@Test
	public void testCalculateBindingEnergyDifferenceToPhase(){
		ArrayList<EnergyValue> actualBindingEnergyList = (ArrayList<EnergyValue>) mixture1.calculateBindingEnergyDifferenceToPhase("water", "water");
		assertEquals(freeEnergyListMoleFraction, actualBindingEnergyList);
	}
	
	@Test
	public void testCalculateBindingEnergyDifferenceBetweenAllPhases(){
		ArrayList<EnergyValue> actualBindingEnergyList = (ArrayList<EnergyValue>) mixture1.calculateBindingEnergyDifferenceBetweenAllPhases("water");
		assertEquals(freeEnergyListMoleFraction, actualBindingEnergyList);
	}
	
	@Test
	public void testCalculateBindingEnergyDifferenceFor1M(){
		assertEquals(-1.3242589744422162E-4, mixture2.calculateBindingEnergyDifferenceFor1M("water", "water", "waterSolute"), ERROR);
	}

	@Test
	public void testCalculateBindingEnergyDifferenceFor1MPerSSIP(){
		ArrayRealVector actualMatrix = mixture2.calculateBindingEnergyDifferenceFor1MPerSSIP("water", "water", "waterSolute");
		double[] expectedArray = new double[]{-2.1915477800504846E-5, -2.1915477800504846E-5, -2.1915477800504846E-5, -2.1915477800504846E-5};
		assertArrayEquals(expectedArray, actualMatrix.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateBindingEnergyDifferenceValue1M(){
		EnergyValue actualValue = mixture1.calculateBindingEnergyDifferenceValue1M("water", "water", "water");
		assertEquals(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR), actualValue);
		assertEquals(0.0, actualValue.getValue(), ERROR);
	}
	
	@Test
	public void testCalculatebindingEnergyDifferenceToPhase1M(){
		ArrayList<EnergyValue> actualFreeEnergyList = (ArrayList<EnergyValue>) mixture1.calculateBindingEnergyDifferenceToPhase1M("water", "water");
		assertEquals(freeEnergyListMolar, actualFreeEnergyList);
	}
	
	@Test
	public void testCalculateBindingEnergyDifferenceBetweenAllPhases1M(){
		ArrayList<EnergyValue> actualFreeEnergyList = (ArrayList<EnergyValue>) mixture1.calculateBindingEnergyDifferenceBetweenAllPhases1M("water");
		assertEquals(freeEnergyListMolar, actualFreeEnergyList);
	}
	@Test
	public void testCalculateBindingEnergySolvation(){
		assertEquals(-31.696822406251744, mixture1.calculateBindingEnergySolvation("water", "water"), ERROR);
	}
	
	@Test
	public void testCalculateBindingEnergySolvationPerSSIP(){
		ArrayRealVector actualArray = mixture2.calculateBindingEnergySolvationPerSSIP("water", "water");
		double[] expectedArray = new double[]{-7.924205601562936, -7.924205601562936, -7.924205601562936, -7.924205601562936};
		assertArrayEquals(expectedArray, actualArray.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateBindingEnergySolvationEnergyValue(){
		assertEquals(new EnergyValue("water", "water", "", -31.69682240557017, new ArrayRealVector(new double[]{-7.924205601392543, -7.924205601392543, -7.924205601392543, -7.924205601392543}), EnergyValueType.MOLEFRACTION), mixture1.calculateBindingEnergySolvationEnergyValue("water", "water"));
	}
	
	@Test
	public void testCalculateBindingEnergySolvationAllPhases(){
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", -31.69682240557017, new ArrayRealVector(new double[]{-7.924205601392543, -7.924205601392543, -7.924205601392543, -7.924205601392543}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedList, mixture1.calculateBindingEnergySolvationAllPhases("water"));
	}
	
	@Test
	public void testCalculateBindingEnergyValuesAllPhasesAllValues(){
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", -31.69682240557017, new ArrayRealVector(new double[]{-7.924205601392543, -7.924205601392543, -7.924205601392543, -7.924205601392543}), EnergyValueType.MOLEFRACTION));
		expectedList.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLEFRACTION));
		expectedList.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR));
		ArrayList<EnergyValue> actualList = (ArrayList<EnergyValue>) mixture1.calculateBindingEnergyValuesAllPhasesAllValues("water");
		assertEquals(expectedList, actualList);
	}
	
	@Test
	public void testCalculateFreeEnergyDifference(){
		assertEquals(0.0, mixture2.calculateFreeEnergyDifference("SoluteWater", "waterSolute", "waterSolute"), ERROR);
		assertEquals(-3.220664899572512E-5, mixture2.calculateFreeEnergyDifference("water", "water", "waterSolute"), ERROR);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferencePerSSIP(){
		ArrayRealVector actualValues = mixture2.calculateFreeEnergyDifferencePerSSIP("water", "water", "waterSolute");
		double[] expectedValues = new double[]{-8.05166224893128E-6, -8.05166224893128E-6, -8.05166224893128E-6, -8.05166224893128E-6};
		assertArrayEquals(expectedValues, actualValues.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferenceValue(){
		EnergyValue actualValue = mixture1.calculateFreeEnergyDifferenceValue("water", "water", "water");
		assertEquals(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLEFRACTION), actualValue);
		assertEquals(0.0, actualValue.getValue(), ERROR);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferenceToPhase(){
		ArrayList<EnergyValue> actualFreeEnergyList = (ArrayList<EnergyValue>) mixture1.calculateFreeEnergyDifferenceToPhase("water", "water");
		assertEquals(freeEnergyListMoleFraction, actualFreeEnergyList);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferenceBetweenAllPhases(){
		ArrayList<EnergyValue> actualFreeEnergyList = (ArrayList<EnergyValue>) mixture1.calculateFreeEnergyDifferenceBetweenAllPhases("water");
		assertEquals(freeEnergyListMoleFraction, actualFreeEnergyList);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferenceFor1M(){
		assertEquals(-7.697063523792735E-5, mixture2.calculateFreeEnergyDifferenceFor1M("water", "water", "waterSolute"), ERROR);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferenceFor1MPerSSIP(){
		ArrayRealVector actualMatrix = mixture2.calculateFreeEnergyDifferenceFor1MPerSSIP("water", "water", "waterSolute");
		double[] expectedArray = new double[]{-8.05166224893128E-6, -8.05166224893128E-6, -8.05166224893128E-6, -8.05166224893128E-6};
		assertArrayEquals(expectedArray, actualMatrix.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferenceValue1M(){
		EnergyValue actualValue = mixture1.calculateFreeEnergyDifferenceValue1M("water", "water", "water");
		assertEquals(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR), actualValue);
		assertEquals(0.0, actualValue.getValue(), ERROR);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferenceToPhase1M(){
		ArrayList<EnergyValue> actualFreeEnergyList = (ArrayList<EnergyValue>) mixture1.calculateFreeEnergyDifferenceToPhase1M("water", "water");
		assertEquals(freeEnergyListMolar, actualFreeEnergyList);
	}
	
	@Test
	public void testCalculateFreeEnergyDifferenceBetweenAllPhases1M(){
		ArrayList<EnergyValue> actualFreeEnergyList = (ArrayList<EnergyValue>) mixture1.calculateFreeEnergyDifferenceBetweenAllPhases1M("water");
		assertEquals(freeEnergyListMolar, actualFreeEnergyList);
	}
	
	@Test
	public void testCalculateFreeEnergySolvation(){
		assertEquals(-25.795818314567388, mixture1.calculateFreeEnergySolvation("water", "water"), ERROR);
		assertEquals(-25.79585052121634, mixture2.calculateFreeEnergySolvation("SoluteWater", "waterSolute"), ERROR);
	}
	
	@Test
	public void testCalculateFreeEnergySolvationPerSSIP(){
		ArrayRealVector actualArray = mixture2.calculateFreeEnergySolvationPerSSIP("water", "water");
		double[] expectedArray = new double[]{-6.448954578641847, -6.448954578641847, -6.448954578641847, -6.448954578641847};
		assertArrayEquals(expectedArray, actualArray.toArray(), ERROR);
	}
	
	@Test
	public void testCalculateFreeEnergySolvationEnergyValue(){
		assertArrayEquals(new double[]{-6.448954578641847, -6.448954578641847, -6.448954578641847, -6.448954578641847}, mixture1.calculateFreeEnergySolvationEnergyValue("water", "water").getContributionBySSIP().toArray(), ERROR);
		assertEquals(new EnergyValue("water", "water", "", -25.795818313885814, new ArrayRealVector(new double[]{-6.4489545784714535, -6.4489545784714535, -6.4489545784714535, -6.4489545784714535}), EnergyValueType.MOLEFRACTION), mixture1.calculateFreeEnergySolvationEnergyValue("water", "water"));
	}
	
	@Test
	public void testCalculateFreeEnergySolvationAllPhases(){
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", -25.795818313885814, new ArrayRealVector(new double[]{-6.4489545784714535, -6.4489545784714535, -6.4489545784714535, -6.4489545784714535}), EnergyValueType.MOLEFRACTION));
		assertEquals(expectedList, mixture1.calculateFreeEnergySolvationAllPhases("water"));
	}
	
	@Test
	public void testCalculateFreeEnergyValuesAllPhasesAllValues(){
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", -25.795818313885814, new ArrayRealVector(new double[]{-6.4489545784714535, -6.4489545784714535, -6.4489545784714535, -6.4489545784714535}), EnergyValueType.MOLEFRACTION));
		expectedList.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLEFRACTION));
		expectedList.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR));
		ArrayList<EnergyValue> actualList = (ArrayList<EnergyValue>) mixture1.calculateFreeEnergyValuesAllPhasesAllValues("water");
		assertEquals(expectedList, actualList);
	}
	
	@Test
	public void testCalculateLogPAllPhases() {
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR));
		ArrayList<EnergyValue> actualList = (ArrayList<EnergyValue>) mixture1.calculateLogPAllPhases("water");
		assertEquals(expectedList, actualList);
	}
	
	@Test
	public void testCalculateLogPValue() {
		EnergyValue expectedValue = new EnergyValue("water", "water", "water", 0.0, new ArrayRealVector(new double[]{0.0, 0.0, 0.0, 0.0}), EnergyValueType.MOLAR);
		assertEquals(expectedValue, mixture1.calculateLogPValue("water", "water", "water"));
	}
	
	@Test
	public void testCalculateLogP() {
		assertEquals(0.0, mixture1.calculateLogP("water", "water", "water"), ERROR);
	}
	
	@Test
	public void testCalculatePhaseConcnetrationFractions() {
		ArrayList<PhaseConcentrationFraction> expectedPhaseConcentrationFractions = new ArrayList<>();
		ArrayList<MoleculeConcentrationFraction> expectedMolConcFractions = new ArrayList<>();
		ArrayList<BoundConcentrationFraction> boundFractions = new ArrayList<>();
		boundFractions.add(new BoundConcentrationFraction(0.959164, "water", "water"));
		expectedMolConcFractions.add(new MoleculeConcentrationFraction("water", new FreeConcentrationFraction(0.040836, "water"), boundFractions));
		Temperature expectedTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		PhaseConcentrationFraction expectedPhaseFraction = new PhaseConcentrationFraction(expectedMolConcFractions, expectedTemperature, "water", PhaseType.CONDENSED);
		expectedPhaseConcentrationFractions.add(expectedPhaseFraction);
		ArrayList<PhaseConcentrationFraction> phaseConcentrationFractions = (ArrayList<PhaseConcentrationFraction>) mixture1.calculatePhaseConcentrationFractions();
		assertEquals(expectedPhaseConcentrationFractions, phaseConcentrationFractions);
	}
	
	@Test
	public void testCalculateConversionFactorTo1MScale() throws Exception{
		double actualConversionfactor = mixture1.calculateConversionFactorTo1MScale("water", "water");
		assertEquals(0.0, actualConversionfactor, ERROR);
	}
	
	@Test
	public void testGetFractionalOccupancyCollection() {
		HashSet<FractionalOccupancy> expectedSet = new HashSet<FractionalOccupancy>();
		expectedSet.add(new FractionalOccupancy(0.738, "water", "", 298.0));
		assertEquals(new FractionalOccupancy(0.738, "water", "", 298.0), mixture1.getPhaseMap().get("water").getFractionalOccupancy(""));
		assertEquals(1, mixture1.getFractionalOccupancyCollection().getFractionalOccupancies().size());
	}
	
	@Test
	public void testCalculateFreeEnergySolvationAllMoleculesEveryPhase() {
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", -25.795818313885814, new ArrayRealVector(new double[]{-6.4489545784714535, -6.4489545784714535, -6.4489545784714535, -6.4489545784714535}), EnergyValueType.MOLEFRACTION));
		ArrayList<EnergyValue> actualList = (ArrayList<EnergyValue>) mixture1.calculateFreeEnergySolvationAllMoleculesEveryPhase();
		assertEquals(expectedList, actualList);
	}
	
	@Test
	public void testCalculateBindingEnergySolvationAllMoleculesEveryPhase() {
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", -31.69682240557017, new ArrayRealVector(new double[]{-7.924205601392543, -7.924205601392543, -7.924205601392543, -7.924205601392543}), EnergyValueType.MOLEFRACTION));
		ArrayList<EnergyValue> actualList = (ArrayList<EnergyValue>) mixture1.calculateBindingEnergySolvationAllMoleculesEveryPhase();
		assertEquals(expectedList, actualList);
	}
	
	@Test
	public void testCalculateConfinementEnergySolvationAllMoleculesEveryPhase() {
		ArrayList<EnergyValue> expectedList = new ArrayList<>();
		expectedList.add(new EnergyValue("water", "water", "", 5.901004091684356, new ArrayRealVector(new double[]{1.475251022921089, 1.475251022921089, 1.475251022921089, 1.475251022921089}), EnergyValueType.MOLEFRACTION));
		ArrayList<EnergyValue> actualList = (ArrayList<EnergyValue>) mixture1.calculateConfinementEnergySolvationAllMoleculesEveryPhase();
		assertEquals(expectedList, actualList);
	}
}
