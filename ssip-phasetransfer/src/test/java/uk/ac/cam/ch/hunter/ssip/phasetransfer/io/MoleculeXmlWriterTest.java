/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.io;

import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.bind.JAXBException;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.w3c.dom.Attr;
import org.xmlunit.matchers.CompareMatcher;
import org.xmlunit.util.Nodes;
import org.xmlunit.util.Predicate;

import uk.ac.cam.ch.hunter.ssip.core.AlphaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Atom;
import uk.ac.cam.ch.hunter.ssip.core.AtomDescriptor;
import uk.ac.cam.ch.hunter.ssip.core.BetaPoint;
import uk.ac.cam.ch.hunter.ssip.core.Ssip;
import uk.ac.cam.ch.hunter.ssip.core.cml.CmlMolecule;
import uk.ac.cam.ch.hunter.ssip.core.pointcloud.Point3D;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurface;
import uk.ac.cam.ch.hunter.ssip.core.surfaceinformation.SsipSurfaceInformation;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SsipConcentrationContainer;

public class MoleculeXmlWriterTest {

	private static final Logger LOG = LogManager.getLogger(MoleculeXmlWriterTest.class);

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	static final String ssipNamespace = "http://www-hunter.ch.cam.ac.uk/SSIP";
	static final String phaseNamespace = "http://www-hunter.ch.cam.ac.uk/PhaseSchema";
	static final double ERROR = 1e-7;
	static final double ERROR_IN_DENSITY = 1e-5;
	private PhaseMolecule phaseMolecule;
	private SsipConcentrationContainer ssipConcentrationContainer;
	private URI outputFileURI;
	private URI comparisonFileURI;

	// Ignore the "dynamic" value of ssipSoftwareVersion in the testing
    final Predicate<Attr> attributeFilter = new Predicate<Attr>(){
        @Override
        public boolean test(Attr attrName) {
        	return (!Nodes.getQName(attrName).toString().contains("ssipSoftwareVersion"));
        }
    };
	
	@Before
	public void setUp() throws Exception {
		outputFileURI = tempFolder.newFile("phasemolecule.xml").toURI();
		comparisonFileURI = MoleculeXmlWriterTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/expectedphasemolecule.xml").toURI();
		HashSet<Ssip> ssipList = new HashSet<Ssip>();
		Ssip ssip1, ssip2, ssip3, ssip4;
		AlphaPoint mep1 = new AlphaPoint(1.0, 1.0, 1.0, "a1", 2.8);
		ssip1 = Ssip.fromMepPoint(mep1);
		AlphaPoint mep2 = new AlphaPoint(1.0, 0.0, 0.0, "a1", 2.8);
		ssip2 = Ssip.fromMepPoint(mep2);
		BetaPoint mep3 = new BetaPoint(0.0, 1.0, 1.0, "a1", -4.5);
		ssip3 = Ssip.fromMepPoint(mep3);
		BetaPoint mep4 = new BetaPoint(0.0, 1.0, 0.0, "a1", -4.5);
		ssip4 = Ssip.fromMepPoint(mep4);
		ssipList.add(ssip1);
		ssipList.add(ssip2);
		ssipList.add(ssip3);
		ssipList.add(ssip4);
		
		ArrayList<Atom> atomlist = new ArrayList<>();
		atomlist.add(new Atom(AtomDescriptor.O, new Point3D(0.0, 1.0, 0.0), "a1"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(1.0, 1.0, 0.0), "a2"));
		atomlist.add(new Atom(AtomDescriptor.H, new Point3D(-1.0, 1.0, 0.0), "a3"));
		HashMap<String, String> bondMap = new HashMap<>();
		bondMap.put("a1 a2", "1");
		bondMap.put("a1 a3", "1");
		String stdinchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
		String moleculeid = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";	
		
		CmlMolecule cmlMolecule = new CmlMolecule(stdinchikey, moleculeid, atomlist, bondMap);
		SsipSurface ssipSurfaceInformation = new SsipSurface(10.0, 5.0, 5.0, 0.0020, 1000, 0.0);
		SsipConcentration ssipConcentration1 = new SsipConcentration(ssip1,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ssipConcentration1.setFreeConcentration(0.24548651049082884);
		ssipConcentration1.setBoundConcentration(9.754513524877266);
		ssipConcentration1.setBoundConcentrationList(new ArrayRealVector(
				new double[] { 0.054441050948867135, 0.385785871734555,
						4.657143301096922, 4.657143301096922 }));
		SsipConcentration ssipConcentration2 = new SsipConcentration(ssip2,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ssipConcentration2.setFreeConcentration(0.05186668339004207);
		ssipConcentration2.setBoundConcentration(9.94813335569966);
		ssipConcentration2.setBoundConcentrationList(new ArrayRealVector(
				new double[] { 0.005131283597771884, 0.054441050948867135,
						4.944280510576511, 4.944280510576511 }));
		SsipConcentration ssipConcentration3 = new SsipConcentration(ssip3,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ssipConcentration3.setFreeConcentration(0.3939105349663359);
		ssipConcentration3.setBoundConcentration(9.606089427317157);
		ssipConcentration3.setBoundConcentrationList(new ArrayRealVector(
				new double[] { 4.944280510576511, 4.657143301096922,
						0.0023328078218614115, 0.0023328078218614115 }));
		SsipConcentration ssipConcentration4 = new SsipConcentration(ssip4,
				new Concentration(10.0, ConcentrationUnit.MOLAR), "mol1");
		ssipConcentration4.setFreeConcentration(0.3939105349663359);
		ssipConcentration4.setBoundConcentration(9.606089427317157);
		ssipConcentration4.setBoundConcentrationList(new ArrayRealVector(
				new double[] { 4.944280510576511, 4.657143301096922,
						0.0023328078218614115, 0.0023328078218614115 }));
		ArrayList<SsipConcentration> expectedSSIPConcentrationList = new ArrayList<>();
		expectedSSIPConcentrationList.add(ssipConcentration2);
		expectedSSIPConcentrationList.add(ssipConcentration1);
		expectedSSIPConcentrationList.add(ssipConcentration3);
		expectedSSIPConcentrationList.add(ssipConcentration4);
		ssipConcentrationContainer = new SsipConcentrationContainer(ConcentrationUnit.MOLAR, expectedSSIPConcentrationList);
		
		phaseMolecule = new PhaseMolecule(cmlMolecule, new SsipSurfaceInformation(ssipSurfaceInformation), ssipConcentrationContainer, moleculeid, stdinchikey);
		
	}

	@Test
	public void testMarshal() throws JAXBException{
		try {
			MoleculeXmlWriter.marshal(phaseMolecule, outputFileURI);
		} catch (JAXBException e1) {
			LOG.error(e1);
			e1.printStackTrace();
			throw e1;
		}
		
		try {
			String actualFileContents = new String(Files.readAllBytes(Paths.get(outputFileURI)));
			String expectedFileContents = new String(Files.readAllBytes(Paths.get(comparisonFileURI)));
			LOG.debug(actualFileContents);
			
			assertThat(actualFileContents, CompareMatcher.isSimilarTo(expectedFileContents).withAttributeFilter(attributeFilter));
		} catch (IOException e) {
			LOG.debug(e);
		}
	}
}
