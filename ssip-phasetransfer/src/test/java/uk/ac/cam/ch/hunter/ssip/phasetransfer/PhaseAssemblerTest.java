/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Phase;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.PhaseMolecule;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SoluteContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.SolventContainer;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Temperature;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.TemperatureUnit;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.io.SoluteXmlReader;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.io.SolventXmlReader;

public class PhaseAssemblerTest {

	private PhaseAssembler phaseAssembler;
	private Phase expectedPhase;
	private SoluteContainer soluteInformation;
	private SolventContainer solventInformation;
	private Temperature expectedTemperature;

	
	@Before
	public void setUp() throws Exception {
		URI solventFileURI = PhaseAssemblerTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/waterphasesolvent.xml").toURI();
		URI soluteFileURI = PhaseAssemblerTest.class.getResource("/uk/ac/cam/ch/hunter/ssip/phasetransfer/watersolute.xml").toURI();

		soluteInformation =  SoluteXmlReader.unmarshalContainer(soluteFileURI);
		solventInformation = SolventXmlReader.unmarshalContainer(solventFileURI);
		
		PhaseFactory phaseFactory = new PhaseFactory();
		HashSet<PhaseMolecule> solventAndSoluteLists = new HashSet<>(solventInformation.getSolventMap().get("water").getPhaseMolecules().values());
		solventAndSoluteLists.add(soluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute").getPhaseMolecule());
		expectedTemperature = new Temperature(298.0, TemperatureUnit.KELVIN);
		expectedPhase = phaseFactory.generatePhase(solventAndSoluteLists, expectedTemperature, ConcentrationUnit.MOLAR);
		
		phaseAssembler = new PhaseAssembler();
		
	}

	@Test
	public void testPhaseAssembler() {
		assertNotNull(phaseAssembler);
	}

	@Test
	public void testGeneratePhaseFromSoluteAndSolvent() {
		Phase actualPhase = phaseAssembler.generatePhaseFromSoluteAndSolvent(soluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute"), solventInformation.getSolventMap().get("water"), expectedTemperature, ConcentrationUnit.MOLAR);
		assertEquals(expectedPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
		assertEquals(expectedPhase.getTemperature(), actualPhase.getTemperature());
		assertEquals(expectedPhase.getPhaseMolecules(), actualPhase.getPhaseMolecules());
	}

	@Test
	public void testGeneratePhaseFromSoluteAndSolventWithID() {
		HashMap<String, Phase> actualPhaseMap = (HashMap<String, Phase>) phaseAssembler.generatePhaseFromSoluteAndSolventWithID(soluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute"), "water", solventInformation.getSolventMap().get("water"), expectedTemperature, ConcentrationUnit.MOLAR);
		for (Map.Entry<String, Phase> phaseMapEntry : actualPhaseMap.entrySet()) {
			assertEquals("water", phaseMapEntry.getKey());
			Phase actualPhase = phaseMapEntry.getValue();
			assertEquals(expectedPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
			assertEquals(expectedPhase.getTemperature(), actualPhase.getTemperature());
			assertEquals(expectedPhase.getPhaseMolecules(), actualPhase.getPhaseMolecules());
		}
	}

	@Test
	public void testGeneratePhasesFromSoluteWithSolvents() {
		HashMap<String, Phase> actualPhaseMap = (HashMap<String, Phase>) phaseAssembler.generatePhasesFromSoluteWithSolvents(soluteInformation.getPhaseSolutes().get("XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute"), solventInformation.getSolventMap(), expectedTemperature, ConcentrationUnit.MOLAR);
		for (Map.Entry<String, Phase> phaseMapEntry : actualPhaseMap.entrySet()) {
			assertEquals("water", phaseMapEntry.getKey());
			Phase actualPhase = phaseMapEntry.getValue();
			assertEquals(expectedPhase.getConcentrationUnit(), actualPhase.getConcentrationUnit());
			assertEquals(expectedPhase.getTemperature(), actualPhase.getTemperature());
			assertEquals(expectedPhase.getPhaseMolecules(), actualPhase.getPhaseMolecules());
		}
	}

}
