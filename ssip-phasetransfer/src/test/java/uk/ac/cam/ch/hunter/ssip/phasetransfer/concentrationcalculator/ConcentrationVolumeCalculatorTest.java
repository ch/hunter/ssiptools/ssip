/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.concentrationcalculator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.Concentration;
import uk.ac.cam.ch.hunter.ssip.phasetransfer.core.ConcentrationUnit;

public class ConcentrationVolumeCalculatorTest {

	private ArrayRealVector moleFractionsBySSIP;
	private ArrayRealVector vdWVolumePerMolecule;
	private ArrayRealVector moleFractionByMolecule;
	
	private static double ERROR = 1e-7;
	
	@Before
	public void setUp() throws Exception {
		moleFractionByMolecule = new ArrayRealVector(new double[]{1.0});
		moleFractionsBySSIP = new ArrayRealVector(new double[]{1.0, 1.0, 1.0, 1.0});
		vdWVolumePerMolecule = new ArrayRealVector(new double[]{21.4});
	}

	@Test
	public void testCalculateZeroPointConc() {
		assertEquals(new Concentration(56.609285469107874, ConcentrationUnit.MOLAR), ConcentrationVolumeCalculator.calculateZeroPointConc(21.4));
	}

	@Test
	public void testCalculateCriticalConc() {
		assertEquals(new Concentration(34.91787701832822, ConcentrationUnit.MOLAR), ConcentrationVolumeCalculator.calculateCriticalConc(21.4));
	}

	@Test
	public void testCalculateCriticalConcs(){
		assertArrayEquals(new double[]{34.91787701832822}, ConcentrationVolumeCalculator.calculateCriticalConcs(vdWVolumePerMolecule).toArray(), ERROR);
	}
	
	@Test
	public void testCalculatePhaseConcentration(){
		assertEquals(0.028638625408844442, ConcentrationVolumeCalculator.calculatePhaseConcentration(vdWVolumePerMolecule, moleFractionByMolecule), ERROR);
	}
	
	@Test
	public void testCalculateInitialConcentrationsSSIPNorm(){
		assertArrayEquals(new double[]{34.91787701832822/300.0, 34.91787701832822/300.0, 34.91787701832822/300.0, 34.91787701832822/300.0}, ConcentrationVolumeCalculator.calculateInitialConcentrationsSSIPNorm(moleFractionsBySSIP, vdWVolumePerMolecule, moleFractionByMolecule).toArray(), ERROR);
	}

	@Test
	public void testCalculateZeroPointConcentrationFraction(){
		assertArrayEquals(new double[]{56.609285469107874/300.0, 56.609285469107874/300.0, 56.609285469107874/300.0, 56.609285469107874/300.0}, ConcentrationVolumeCalculator.calculateZeroPointConcentrationFraction(moleFractionsBySSIP, new ArrayRealVector(new double[]{56.609285469107874/300.0}), moleFractionByMolecule).toArray(), ERROR);
	}
	
	@Test
	public void testGetV1molar(){
		assertEquals(1660.539040427164, ConcentrationVolumeCalculator.getV1molar(), 1e-7);
	}
}
