/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TemperatureUnitTest {

	private static final double ERROR = 1e-7;
	
	@Test
	public void testKelvin() {
		assertEquals("KELVIN", TemperatureUnit.KELVIN.getTemperatureUnit());
		assertEquals(0.0, TemperatureUnit.KELVIN.getConversionFactorTo(TemperatureUnit.KELVIN), ERROR);
		assertEquals(-273.15, TemperatureUnit.KELVIN.getConversionFactorTo(TemperatureUnit.CELSIUS), ERROR);
	}
	
	@Test
	public void testCelsius(){
		assertEquals("CELSIUS", TemperatureUnit.CELSIUS.getTemperatureUnit());
		assertEquals(0.0, TemperatureUnit.CELSIUS.getConversionFactorTo(TemperatureUnit.CELSIUS), ERROR);
		assertEquals(273.15, TemperatureUnit.CELSIUS.getConversionFactorTo(TemperatureUnit.KELVIN), ERROR);
	}

}
