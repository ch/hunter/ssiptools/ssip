/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core.temperature;

import static org.junit.Assert.assertEquals;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class InteractionEnergyCalculatorTest {

	private static final double ERROR = 1e-5;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	@Parameters({
		"-1.0, 298.0, -0.5995519650366715",
		"-10.0, 298.0, -9.826385042109182",
		"-50.0, 298.0, -49.99999991391315",
		"-175.0, 298.0, -175.0",
		"-395.0, 298.0, -395.0",
		"-151.0, 298.0, -151.0",
		"-72.0, 298.0, -71.99999999998273",
		"-60.5, 298.0, -60.49999999849591",
		"-50.8, 298.0, -50.79999993667051",
		"-50.8, 500.0, -50.79974943294584",
		"-60.5, 500.0, -60.49997106090202"
	})
	public void testCalculateInteractionEnergy(double e0, double temperature, double expectedEint) {
		double actualEint = InteractionEnergyCalculator.calculateInteractionEnergy(e0, temperature);
		assertEquals(expectedEint, actualEint, ERROR);
	}
	
	@Test
	@Parameters({
		"-1.0, -0.5995519650366715",
		"-10.0, -9.826385042109182",
		"-50.0, -49.99999991391315",
		"-175.0, -175.0",
		"-395.0, -395.0",
		"-151.0, -151.0",
		"-72.0, -71.99999999998273",
		"-60.5, -60.49999999849591",
		"-50.8, -50.79999993667051"
	})
	public void testCalculateInteractionEnergyRT(double e0, double expectedEint) {
		double actualEint = InteractionEnergyCalculator.calculateInteractionEnergyRT(e0);
		assertEquals(expectedEint, actualEint, ERROR);
	}

	@Test
	@Parameters({
		"-0.5001008995, 298.0, -0.5009721020551884",
		"-10.0, 298.0, -10.0",
		"-50.0, 298.0, -50.0",
		"-90.58876365, 298.0, -90.58876365",
		"-213.2096117643254, 298.0, -213.2096117643254",
		"-77.799898, 298.0, -77.799898",
		"-36.52302627, 298.0, -36.52302627",
		"-30.61929909, 298.0, -30.61929909",
		"-25.6603762, 298.0, -25.6603762",
		"-30.61929909, 500.0, -30.60006666053905",
		"-25.6603762, 500.0, -25.607782197269387"
	})
	public void testCalculateInteractionEnergyUsingSpline(double e298, double temperature, double expectedEint) {
		double actualEint = InteractionEnergyCalculator.calculateInteractionEnergyUsingSpline(e298, temperature);
		assertEquals(expectedEint, actualEint, ERROR);
	}

	@Test
	@Parameters({
		"-0.5001008995, -0.8556503685018941",
		"-5.010089938, -5.544628921197275",
		"-25.25224022, -25.25318623150945",
		"-90.58876365, -90.58876365000002",
		"-213.2096117643254, -213.2096117643254",
		"-77.799898, -77.7998980000018",
		"-36.52302627, -36.52304074979894",
		"-30.61929909, -30.61943060708467",
		"-25.6603762, -25.66119153559956"
	})
	public void testCalculateInteractionEnergyAZ(double eInt, double expectedE0) {
		double actualE0 = InteractionEnergyCalculator.calculateInteractionEnergyAZ(eInt);
		assertEquals(expectedE0, actualE0, ERROR);
	}

}
