/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.mathutils;

import static org.junit.Assert.assertArrayEquals;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Test;

public class MatrixUtilsTest {

	private static final double ERROR = 1e-7;
	
	@Test
	public void testCalculateRowSums() {
		ArrayRealVector outputVector = MatrixUtils.calculateRowSums(new Array2DRowRealMatrix(new double[][] {{1.0, 2.0},{3.0, 4.0}}));
		assertArrayEquals(new double[] {3.0, 7.0}, outputVector.toArray(), ERROR);
	}

	@Test
	public void testCalculateHadamardProduct() {
		Array2DRowRealMatrix outputMatrix = MatrixUtils.calculateHadamardProduct(new Array2DRowRealMatrix(new double[][] {{1.0, 2.0},{3.0, 4.0}}), new Array2DRowRealMatrix(new double[][] {{1.0, 4.0},{1.0, 5.0}}));
		Array2DRowRealMatrix expectedMatrix = new Array2DRowRealMatrix(new double[][] {{1.0, 8.0},{3.0, 20.0}});
		for (int i = 0; i < outputMatrix.getRowDimension(); i++) {
			assertArrayEquals(expectedMatrix.getRow(i), outputMatrix.getRow(i), ERROR);
		}
	}

}
