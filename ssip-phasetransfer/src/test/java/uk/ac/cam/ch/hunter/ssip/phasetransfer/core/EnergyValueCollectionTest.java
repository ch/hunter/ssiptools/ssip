/*
 * This product is dual-licensed under both the AGPL 3 and a Commercial License.
 *
 * ssip-phasetransfer - A tool for footprinting molecular electrostatic potential surfaces for calculation of solvation energies.
 * Copyright © 2015 Christopher Hunter (ch664@cam.ac.uk)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ======================================================================
 *
 * Copyright Mark Driver, Christopher Hunter, Teodor Nikolov at the University of Cambridge.
 *
 * Enquiries for any non-academic use of SSIPTools including commercial use should be directed to Cambridge Enterprise:
 *
 * Cambridge Enterprise Ltd
 * University of Cambridge
 * Hauser Forum
 * 3 Charles Babbage Rd
 * Cambridge CB3 0GT
 * United Kingdom
 * Tel: +44 (0)1223 760339
 * Email: software@enterprise.cam.ac.uk
 */
package uk.ac.cam.ch.hunter.ssip.phasetransfer.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Before;
import org.junit.Test;

public class EnergyValueCollectionTest {

	private EnergyValue energyValue;
	private ArrayList<EnergyValue> energyValueList;
	private EnergyValueCollection energyValueCollection;
	private static final String moleculeID = "XLYOFNOQVPJJNP-UHFFFAOYSA-N";
	private static final String toSolventID = "water";
	private static final String fromSolventID = "octanol";
	
	@Before
	public void setUp() throws Exception {
		energyValue = new EnergyValue(moleculeID, toSolventID, fromSolventID, 0.2, new ArrayRealVector(new double[]{0.1, 0.0, 0.1, 0.0}), EnergyValueType.MOLEFRACTION);
		energyValueList = new ArrayList<>();
		energyValueList.add(energyValue);
		energyValueCollection = new EnergyValueCollection();
		energyValueCollection.setBindingEnergies(energyValueList);
		energyValueCollection.setConfinementEnergies(energyValueList);
		energyValueCollection.setTotalEnergies(energyValueList);
	}

	@Test
	public void testEnergyValueCollection() {
		assertNotNull(energyValueCollection);
	}

	@Test
	public void testGetTotalEnergies() {
		assertEquals(energyValueList, energyValueCollection.getTotalEnergies());
	}

	@Test
	public void testGetBindingEnergies() {
		assertEquals(energyValueList, energyValueCollection.getBindingEnergies());
	}

	@Test
	public void testGetConfinementEnergies() {
		assertEquals(energyValueList, energyValueCollection.getConfinementEnergies());
	}

}
