# Introduction

This is an initial attempt at a Java software implementation of the SSIP method as described in the paper ["Footprinting molecular electrostatic potential surfaces for calculation of solvation energies"](http://dx.doi.org/10.1039/C3CP53158A).

Specifically, the footprinting aspect of the calculation will be initially worked upon here since a Java QM solution does not exist at the moment. For the moment, the QM aspects will be dealt with by NWChem; please see [this](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/mepsgen/nwchemcmlutils) repository *first* for the setup of the QM calculation.

# Binary releases

The latest Java binary releases can be found [here](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/ssip/-/releases) and, for example, the footprinting tool can be run as follows:

```
   java -jar ssip-footprint-7.0.0-jar-with-dependencies.jar
```

To footprint using the output the NWChem MEP calculation:

```
   java -jar ssip-footprint-7.0.0-jar-with-dependencies.jar -m water_0.0020_merged.cube -c water.cml -w water_ssip.xml
```

Then visualise it with:

```
   java -jar ssip-footprint-7.0.0-jar-with-dependencies.jar -r water_ssip.xml
```





For compilation from source, please see the [developer readme](DEV.md).

